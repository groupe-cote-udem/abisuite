#!/bin/bash


MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/pw.x"
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/masslaunchers/qe_mass_launcher/ecutwfc5/ecutwfc5.in
LOG=/home/fgoudreault/Workspace/abilaunch/examples/masslaunchers/qe_mass_launcher/ecutwfc5/ecutwfc5.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/masslaunchers/qe_mass_launcher/ecutwfc5/ecutwfc5.stderr

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
