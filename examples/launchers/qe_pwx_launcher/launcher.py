from abilaunch import QEPWLauncher as Launcher


# very simple example
qe_input_vars = {"calculation": "scf",
                 "ibrav": 2,
                 "celldm(1)": 10.28,
                 "nat": 2,
                 "ntyp": 1,
                 "ecutwfc": 18.0,
                 "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                                     "pseudo": "Si.pz-vbc.UPF"}],
                 "atomic_positions": {"parameter": "alat",
                                      "positions": {"Si": [[0.00, 0.00, 0.00],
                                                           [0.25, 0.25, 0.25]],
                                                    }},
                 "k_points": {"parameter": "automatic",
                              "k_points": [4, 4, 4, 1, 1, 1]}
                 }

launcher = Launcher("silicon")
launcher.command = "pw.x"
launcher.workdir = "."
launcher.pseudo_dir = "../../pseudos"
launcher.input_variables = qe_input_vars
launcher.write()
launcher.run()
