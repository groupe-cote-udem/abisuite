#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/ph.x"
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_phx_launcher/silicon.in
LOG=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_phx_launcher/silicon.log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/launchers/qe_phx_launcher/silicon.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
