from abilaunch import OpticLauncher as Launcher


# toptic1_2 variables
variables = {"ddkfile_1": "../../example_data/odat_example_1WF7",
             "ddkfile_2": "../../example_data/odat_example_1WF8",
             "ddkfile_3": "../../example_data/odat_example_1WF9",
             "wfkfile": "../../example_data/odat_example_WFK",
             "broadening": 0.002,
             "maxomega": 0.3,
             "domega": 0.0003,
             "tolerance": 0.002,
             "num_lin_comp": 1,
             "lin_comp": 11,
             "num_nonlin_comp": 2,
             "nonlin_comp": [123, 222],
             "num_linel_comp": 0,
             "num_nonlin2_comp": 0,
             }

l = Launcher("example")
l.workdir = "."
l.input_variables = variables
l.command = "/home/fgoudreault/Workspace/abinit/build/master/vanilla/src/98_main/optic"
l.write(overwrite=True)
l.run()
