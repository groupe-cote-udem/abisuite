from abilaunch.writers import PBSFileWriter


path = "example.sh"
writer = PBSFileWriter(
                       allow_cmd_invisible=True  # just for the sake of this example, no error will be raised since abinit script doesnt exist
        )
writer.path = path
writer.input_file_path = "example.files"
writer.log_file_path = "example.log"
writer.stderr_file_path = "stderr"
writer.jobname = "example"
writer.walltime = "1:00:00"
writer.nodes = 1
writer.ppn = 12
writer.mpi_command = "mpirun -npernode=12"
writer.command = "abinit"
writer.modules = ["MPI/Gnu/gcc4.9.2/openmpi/1.8.8"]
writer.lines_before = "cd $PBS_O_WORKDIR"
writer.write(overwrite=True)
