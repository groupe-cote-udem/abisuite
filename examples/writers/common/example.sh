#!/bin/bash

#PBS -N example
#PBS -l walltime=1:00:00
#PBS -l nodes=1:ppn=12

MPIRUN="mpirun -npernode=12"
EXECUTABLE=/home/fgoudreault/Workspace/abilaunch/examples/writers/common/abinit
INPUT=/home/fgoudreault/Workspace/abilaunch/examples/writers/common/example.files
LOG=/home/fgoudreault/Workspace/abilaunch/examples/writers/common/log
STDERR=/home/fgoudreault/Workspace/abilaunch/examples/writers/common/stderr

module load MPI/Gnu/gcc4.9.2/openmpi/1.8.8

cd $PBS_O_WORKDIR

$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
