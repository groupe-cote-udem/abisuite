.. image:: logo/large_logo.png
   :alt: Welcome to abisuite!
   :target: https://gitlab.com/groupe-cote-udem/abisuite
.. image:: https://img.shields.io/badge/licence-MIT-blue.svg
   :alt: Licence
   :target: https://gitlab.com/groupe-cote-udem/abisuite/blob/master/LICENSE
.. image:: https://img.shields.io/gitlab/pipeline/groupe-cote-udem/abisuite/master.svg
   :alt: Build status
   :target: https://gitlab.com/groupe-cote-udem/abisuite/pipelines/
.. image:: https://gitlab.com/groupe-cote-udem/abisuite/badges/master/coverage.svg
   :alt: Coverage percentage.
   :target: https://gitlab.com/groupe-cote-udem/abisuite/pipelines/
.. image:: https://img.shields.io/badge/docs-click%20here-lightgrey
   :alt: Click here for docs.
   :target: https://groupe-cote-udem.gitlab.io/abisuite

====================
Welcome to AbiSuite!
====================

This is a python module to easily write input files, launch calculations,
parse output files, post-process data and plot data from quantum abinitio softwares.

Installation
------------

Download repository using git::

 git clone git@github.com:abiproject/abisuite.git
 cd abisuite


It is preferable to use this library using a virtual environment. You
can create one using::

 python -m venv venv/abisuite
 source venv/abisuite/bin/activate

Call `deactivate` command to exit the venv. Then, execute `pip` to install::
  
 pip install .

For a development installation (to modify the code without having to execute
the setup script each time). Use the ``-e`` flag for the ``pip install``
command.

To execute tests, one needs the testing libraries which can be installed using::

 pip install .[tests]

To build docs, one needs to install using the `docs` option::

 pip install .[docs]

To read netCDF4 files, one needs to install netCDF4 and the corresponding
python interface. Good luck with that! In any case, the import will be
tried only where needed thus the major part of the code won't be affected
if netCDF4 is not installed. You can try to install the optional dependencies
like this one using::

 pip install .[optional]


Scripts
-------

When installing, an entrypoint is created
addendum is added to the ``~/.bashrc`` file that adds
the ``abisuite/scripts`` directory to the ``$PATH`` environment variable (UNIX only).
This enables the abisuite scripts to be available from everywhere on the
command line. You can execute ``abisuite --help`` to see all available options.

Tests
-----

Before running the tests, you need to install the requirements::

  pip install .[tests]
  
Then, just use pytest in main repository::

 pytest -n 4

The ``-n 4`` option asks to run tests in 4 threads. Indeed, the tests can be
parallelized this way.
 

Documentation
-------------

There is documentation available for abisuite
`here <https://groupe-cote-udem.gitlab.io/abisuite>`__ but it's probably
not *up-to-date* anymore as the gitlab CI tools are not unlimited anymore...

You can build the documentation locally::

  $ pip install .[docs]
  $ cd doc
  $ make clean html

After compilation terminates, the documentation is stored in html
format into the ``build`` subdirectory. You can view it with
your favorite internet browser::

  $ firefox build/html/index.html


Configuration File
------------------

abisuite requires a config file. A default one is created upon installation at:
`~/.config/abisuite`. It contains default values that probably needs to be
edited. If any entry in the config file is deleted, the next time the package
needs the config file, the default parameter will be put back. Also, if there
is an update of the software where new config file entries have been inserted,
their default values will be automatically set.

The ``abisuite config`` script have some options to configure / print config
options for quick reference/edits.

Contributors
------------

Ordered by largest to smallest contributions:

- `Felix Goudreault <https://gitlab.com/fgoudreault>`__
- `Olivier Gingras <https://gitlab.com/gingras.ol>`__
- `Daniel Gendron <https://gitlab.com/dgendron>`__
