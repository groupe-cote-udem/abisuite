from .bases import BaseQEPackager


class QEMatdynPackager(BaseQEPackager):
    """Packager class for a matdyn.x calculatio."""

    _calctype = "qe_matdyn"
    _loggername = "QEMatdynPackager"

    def keep_file(self, handler, *args, **kwargs):
        """Return True if we package the file."""
        if handler.basename.endswith(".eig"):
            return True
        if handler.basename.endswith(".freq"):
            return True
        return super().keep_file(handler, *args, **kwargs)
