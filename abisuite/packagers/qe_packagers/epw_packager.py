from .bases import BaseQEPackager


class QEEPWPackager(BaseQEPackager):
    """Packager class for an epw.x calculation by Quantum Espresso."""

    _calctype = "qe_epw"
    _loggername = "QEEPWPackager"

    def keep_file(self, handler, *args, **kwargs):
        """Return True if we package the file."""
        # there's a ton of files to keep in order to restart from a packaged
        # epw interpolation or save main results of a run
        keep_file_ext = [
                "_band.dat", "_band.kpt", ".txt", ".epmatwp", ".nnkp", "kgmap",
                ".mmn", ".ukk", ".win", ".wout",
                ]
        for file_ext in keep_file_ext:
            if handler.basename.endswith(file_ext):
                return True
        keep_filename = [
                "band.eig", "phband.freq", "ifc.q2r", "crystal.fmt",
                "IBTEvel_sup.fmt", "inv_taucb.fmt", "inv_tau.fmt",
                "epwdata.fmt", "dmedata.fmt", "vmedata.fmt",
                "mobility_nk.fmt", "mobility_nuq.fmt",
                "inv_taucb_freq.fmt", "inv_taucb_mode.fmt",
                "inv_tau_freq.fmt", "inv_tau_mode.fmt",
                ]
        for filename in keep_filename:
            if handler.basename == filename:
                return True
        keep_file_prefix = [
                "decay", "specfun.elself", "specfun_sup.elself",
                ]
        for file_prefix in keep_file_prefix:
            if handler.basename.startswith(file_prefix):
                return True
        if ".res.01" in handler.basename:
            # keep ziman resistivity files
            return True
        return super().keep_file(handler, *args, **kwargs)
