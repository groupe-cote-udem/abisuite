from .bases import BaseQEPackager
from ...handlers.file_handlers.bases import BaseFileHandler


class QEKpointsPackager(BaseQEPackager):
    """Packager class for a kpoints.x calculation by Quantum Espresso."""

    _calctype = "qe_kpoints"
    _loggername = "QEKpointsPackager"

    def keep_file(self, handler: BaseFileHandler, *args, **kwargs) -> bool:
        """Return True if we package the given file handler."""
        if handler.basename.endswith(".mesh"):
            return True
        return super().keep_file(handler, *args, **kwargs)
