from .bases import BaseQEPackager


class QEPWPackager(BaseQEPackager):
    """Packager class for a pw.x calculation by Quantum Espresso."""

    _calctype = "qe_pw"
    _loggername = "QEPWPackager"

    def keep_file(self, handler, *args, **kwargs):
        """Return True if we package the file."""
        if handler.basename.endswith(".xml"):
            return True
        return super().keep_file(handler, *args, **kwargs)
