from .bases import BaseQEPackager


class QEDOSPackager(BaseQEPackager):
    """Packager class for a dos.x calculation."""

    _calctype = "qe_dos"
    _loggername = "QEDOSPackager"

    def keep_file(self, handler, *args, **kwargs):
        """Return True if we package the file described by handler."""
        if handler.basename.endswith(".dos"):
            return True
        return super().keep_file(handler, *args, **kwargs)
