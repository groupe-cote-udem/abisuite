from .bases import BaseQEPackager


class QEPHPackager(BaseQEPackager):
    """Packager class for a 'ph.x' calculation."""

    _calctype = "qe_ph"
    _loggername = "QEPHPackager"

    def keep_file(self, handler, *args, **kwargs):
        """Return True if we package the file."""
        if ".dyn" in handler.basename:
            return True
        # save files used for epw interpolation calculations
        if handler.basename.endswith("dvscf1"):
            return True
        if "_ph0" in handler.path and ".phsave" in handler.path:
            return True
        return super().keep_file(handler, *args, **kwargs)
