import abc
import asyncio
import os

import aiofiles.os

import tqdm

from .routines import compute_packaging_path, get_packaging_roots
from .. import __USER_DATABASE__
from ..bases import BaseCalctypedUtility
from ..databases import DBCalculation
from ..handlers.file_handlers.bases import BaseFileHandler


class BasePackager(BaseCalctypedUtility, abc.ABC):
    """Base class for all Packagers objects."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calculation_directory = None
        self.nfiles_saved = 0
        self.nGbs_saved = 0

    @property
    def calculation_directory(self):
        """The packager's CalculationDirectory."""
        if self._calculation_directory is not None:
            return self._calculation_directory
        raise ValueError("Need to set the 'calculation_directory'.")

    async def set_calculation_directory(self, calc):
        """Sets calculation directory."""
        if isinstance(calc, str):
            from ..handlers import CalculationDirectory
            calc = await CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        self._calculation_directory = calc

    @abc.abstractmethod
    def keep_file(self, handler, *args, **kwargs):
        """Tells if a file must be packaged or not.

        Parameters
        ----------
        handler: FileHandler object
            The file handler to check.

        Returns
        -------
        bool: True if we package file, False if not.
        """
        name = handler.basename
        if name.endswith(".in"):
            return True
        if name.endswith(".log"):
            return True
        if ".log_" in name:
            # also copy backups of logs
            return True
        if name.endswith(".meta"):
            return True
        if name.endswith(".sh"):
            return True
        if name.startswith("slurm") and name.endswith(".out"):
            return True
        return False

    async def package(
            self,
            overwrite: bool = False,
            replace_with_symlink: bool = False,
            keep_density: bool = False,
            keep_wavefunctions: bool = False,
            pwd: str = None,
            **kwargs) -> None:
        """Packages the calculation directory attached to the packager object.

        It basically copies relevant files useful for safekeeping and
        ignores everything else.

        It also keeps the directory tree intact. That means it will keep the
        subdirectories hierarchy of the src w.r.t. a given root and create/keep
        the same hierarchy w.r.t. the dest root.

        Parameters
        ----------
        overwrite: bool, optional
            If True and files with the same names are present at packaging
            destination, those files will be overwritten.
        replace_with_symlink: bool, optional
            If True, the calculation is replaced with a symlink towards the
            packaged calculation.
        keep_density: bool, optional
            Density files are not usually packaged unless this flag is True.
        keep_wavefunctions: bool, optional
            Wavefunction files are not usually packaged unless
            this flag is True.
        pwd: str, optional
            The present working directory. This flag's only use is to print
            out relative paths to this one. If None, absolute paths are
            printed.

        all other kwargs are passed to the `get_packaging_roots` method.

        Raises
        ------
        ValueError:
            - If the calculation directory to package lies outside the
              src root.
        """
        src_root, dest_root = get_packaging_roots(
                _logger=self._logger, **kwargs)
        self._logger.debug(
                f"Packaging '{self.calculation_directory.path}' with src_root="
                f"'{src_root}' and dest_root='{dest_root}'.")
        # compare src root with calculation directory
        calcdirrelpath = os.path.relpath(
                self.calculation_directory.path, src_root)
        if calcdirrelpath.startswith(".."):
            raise ValueError(
                    f"Directory '{self.calculation_directory.path}' "
                    f"is outside package src root: '{src_root}'.")
        if __USER_DATABASE__ is not None:
            if not self.calculation_directory.connected_to_database:
                self.calculation_directory.connect_to_database()
            if self.calculation_directory.is_in_database:
                is_packaged = await self.calculation_directory.is_packaged
                if is_packaged and not overwrite:
                    self._logger.info("Already packaged, do nothing else.")
                    return
        all_handlers = await self.calculation_directory.walk(paths_only=False)
        coros = []
        # check if there is a file at calc dest
        calc_dest = compute_packaging_path(
                self.calculation_directory.path, src_root, dest_root)
        if await aiofiles.os.path.exists(calc_dest) and not overwrite:
            raise FileExistsError(
                    f"A file already exists at destination: {calc_dest}.")
        for handler in all_handlers:
            if not await handler.exists:
                continue
            if self.keep_file(
                    handler, keep_wavefunctions=keep_wavefunctions,
                    keep_density=keep_density,
                    ):
                newpath = compute_packaging_path(
                        handler.path, src_root, dest_root)
                coros.append(
                        self._package_handler(
                            handler, newpath, overwrite=overwrite))
        path = self.calculation_directory.path
        if pwd is not None:
            path = os.path.relpath(path, start=pwd)
        for future in tqdm.tqdm(
                asyncio.as_completed(coros),
                total=len(coros),
                desc=f"packaging files for {path}",
                unit="files"):
            await future
        # If we're here, we replace the whole calculation with a symlink
        # once the calculation has been packaged, repair the copied meta file
        # and change the db entry
        new_meta = compute_packaging_path(
                self.calculation_directory.meta_data_file.path, src_root,
                dest_root)
        new_workdir = os.path.dirname(new_meta)
        # update new meta data file
        await self.calculation_directory.update_new_meta_data_file(
                new_workdir, self.calculation_directory.path,
                # FG: 2023/02/20: do not update parents as we replace with
                # a symlink anyway if calculation is deleted
                update_parents=False,
                )
        # change db
        if __USER_DATABASE__ is not None:
            if not self.calculation_directory.is_in_database:
                # add to database
                self.calculation_directory.add_to_database()
            # change the packaged attribute to True
            self.calculation_directory.update_database_packaged(True)
        if not replace_with_symlink:
            return
        # now delete the calculation directory to replace it with a symlink
        await self.calculation_directory.delete()
        # replace with symlink
        from ..handlers import SymLinkFile
        symlink = SymLinkFile(loglevel=self._loglevel)
        symlink.source = new_workdir
        await symlink.set_path(self.calculation_directory.path)
        await symlink.write()
        # update db entry
        if __USER_DATABASE__ is not None:
            if DBCalculation.get_or_none(
                    DBCalculation.calculation == new_workdir) is not None:
                # calculation was probably packaged before and redone
                if overwrite:
                    # keep the old entry and delete the current one
                    self.calculation_directory.delete_from_database()
                else:
                    raise ValueError(
                            "A calculation already exists where the packaging "
                            "occurs. Set 'overwrite' to True to overwrite DB."
                            )
            else:
                await self.calculation_directory.db_model.update_calculation(
                    new_workdir)

    async def _package_handler(
            self,
            handler: BaseFileHandler,
            newpath: str,
            overwrite: bool = False,
            ):
        await handler.copy(newpath, overwrite=overwrite)
        self.nfiles_saved += 1
        self.nGbs_saved += await handler.file_size
