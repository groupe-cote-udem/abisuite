from .bases import BaseAbinitPackager
from ...handlers.file_handlers.bases import BaseFileHandler


class AbinitAnaddbPackager(BaseAbinitPackager):
    """Packager class for an anaddb calculation."""

    _calctype = "abinit_anaddb"
    _loggername = "AbinitAnaddbPackager"

    def keep_file(self, handler: BaseFileHandler, *args, **kwargs) -> bool:
        """Returns True if we package this file handler."""
        basename = handler.basename
        if "_qpt_" in basename:
            return True
        if basename.endswith("_PHFRQ"):
            return True
        if basename.endswith("_PHBST.nc"):
            return True
        if basename.endswith("_anaddb.nc"):
            return True
        if basename.endswith("_PHBANDS.agr"):
            return True
        if basename == "PHBST_partial_DOS":
            return True
        return super().keep_file(handler, *args, **kwargs)
