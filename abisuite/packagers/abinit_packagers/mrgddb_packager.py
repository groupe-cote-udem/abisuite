from .bases import BaseAbinitPackager
from ...handlers.file_handlers.bases import BaseFileHandler


class AbinitMrgddbPackager(BaseAbinitPackager):
    """Packager class for a mrgddb calculation."""

    _calctype = "abinit_mrgddb"
    _loggername = "AbinitMrgddbPackager"

    def keep_file(self, handler: BaseFileHandler, *args, **kwargs) -> bool:
        """Returns True if we package this file handler."""
        if handler.basename.endswith("_DDB"):
            return True
        return super().keep_file(handler, *args, **kwargs)
