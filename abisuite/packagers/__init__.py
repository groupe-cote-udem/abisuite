from .abinit_packagers import (
        AbinitAnaddbPackager, AbinitPackager, AbinitOpticPackager,
        AbinitMrgddbPackager,
        )
from .qe_packagers import (
        QEDOSPackager, QEEPWPackager, QEKpointsPackager, QEMatdynPackager,
        QEPHPackager, QEPWPackager,
        QEQ2RPackager,
        )
from .routines import compute_packaging_path, get_packaging_roots


__CALCTYPES_TO_PACKAGER_CLS__ = {
        "abinit": AbinitPackager,
        "abinit_anaddb": AbinitAnaddbPackager,
        "abinit_mrgddb": AbinitMrgddbPackager,
        "abinit_optic": AbinitOpticPackager,
        "qe_dos": QEDOSPackager,
        "qe_epw": QEEPWPackager,
        "qe_kpoints": QEKpointsPackager,
        "qe_matdyn": QEMatdynPackager,
        "qe_ph": QEPHPackager,
        "qe_pw": QEPWPackager,
        "qe_q2r": QEQ2RPackager,
        }
