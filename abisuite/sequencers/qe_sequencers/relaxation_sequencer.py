import os
from typing import Any, Mapping, Sequence

import aiofiles.os

from .scf_sequencer import QESCFSequencer
from ..bases import SequenceCalculation
from ..exceptions import SequenceNotConvergedError
from ..relaxation_sequencers import RelaxationSequencer
from ...handlers import QEPWLogFile
from ...launchers import QEPWLauncher
from ...launchers.restarters.qe_restarters.pw_restarter import (
        get_qe_pw_final_geometry,
        )
from ...status_checkers.exceptions import (
        CalculationFailedError, CalculationNotConvergedError,
        )


class QERelaxationSequencer(QESCFSequencer, RelaxationSequencer):
    """Sequencer for relaxing a structure using Quantum Espresso.

    The relaxation sequencer executes relaxation calculations
    (which are basically a series of SCF calculations) until the pressure
    reaches 0. An upper limit of calculations needs to be specified in order
    to not waste ressources in case of problems.
    """

    _loggername = "QERelaxationSequencer"

    def __init__(self, *args, bypass_convergence_check: bool = True, **kwargs):
        QESCFSequencer.__init__(self, *args, **kwargs)
        RelaxationSequencer.__init__(self, "qe", *args, **kwargs)
        self._maximum_relaxations = None
        # bypass convergence checks as we want to reach pressure of 0 with
        # multiple calcs instead of a single one
        self.bypass_convergence_check = bypass_convergence_check

    @property
    def maximum_relaxations(self):
        """Define the maximum number of relaxation runs."""
        if self._maximum_relaxations is not None:
            return self._maximum_relaxations
        raise ValueError("Need to set 'maximum_relaxations'.")

    @maximum_relaxations.setter
    def maximum_relaxations(self, max_):
        if not isinstance(max_, int):
            raise TypeError(
                    f"Expected int for 'maximum_relaxations' but got '{max_}'."
                    )
        self._maximum_relaxations = max_

    @property
    def scf_input_variables(self):
        """Define the scf input variables."""
        return self._scf_input_variables

    @scf_input_variables.setter
    def scf_input_variables(self, invars):
        if self.relax_cell:
            self._set_input_var_to(invars, "tprnfor", True)
            self._set_input_var_to(invars, "tstress", True)
        self._scf_input_variables = invars

    async def init_sequence(self) -> None:
        """Initialize the whole sequence."""
        await self.init_relaxation_sequence()

    def _get_relaxation_input_variables(
            self, relax_atoms: bool = True,
            ivars: Mapping[str, Any] = None,
            ) -> Mapping[str, Any]:
        if ivars is None:
            ivars = self.scf_input_variables.copy()
        if relax_atoms:
            ivars["calculation"] = "relax"
        else:
            ivars["calculation"] = "vc-relax"
        return ivars

    async def init_relaxation_sequence(
            self, *args, relax_atoms: bool = None,
            relax_cell: bool = None,
            check_pressure_0: bool = True,
            set_truly_completed: bool = True,
            check_convergence: bool = False,
            root: str = None, add_to_global_sequence: bool = True,
            starting_input_variables: dict[str, Any] = None,
            **kwargs,
            ) -> Sequence[SequenceCalculation]:
        """Initialize relaxation part of sequence."""
        # in any case, start by doing atomic relaxations only
        if relax_atoms is None:
            relax_atoms = self.relax_atoms
        if relax_cell is None:
            relax_cell = self.relax_cell
        if not relax_atoms and not relax_cell:
            raise ValueError(
                    "'relax_atoms' and 'relax_cell' cannot be False "
                    "at same time.")
        if starting_input_variables is None:
            starting_input_variables = self._get_relaxation_input_variables(
                *args, relax_atoms=relax_atoms,
                **kwargs)
        if relax_atoms:
            workdir = self._get_relaxation_workdir(
                    0, *args, root=root, **kwargs)
        else:
            workdir = self._get_relaxation_workdir(
                    1, *args, root=root, **kwargs)
        new_sequence = []
        seq = SequenceCalculation(
                "qe_pw", workdir,
                starting_input_variables, self.scf_calculation_parameters,
                loglevel=self._loglevel)
        if add_to_global_sequence:
            self.sequence.append(seq)
        new_sequence.append(seq)
        if not relax_cell and set_truly_completed:
            # nothing else to do
            self._truly_completed = True
            return
        # up to a maximum of 'self.maximum_relaxations' must be done
        # to relax cell (in order to reach pressure of exactly 0)
        # first check how many calcs are written
        n_relaxed = 0
        while await aiofiles.os.path.exists(
                self._get_relaxation_workdir(
                    n_relaxed + 1, *args, root=root, **kwargs)):
            n_relaxed += 1
        # there are n_relaxed calculations written build sequence up to there
        for icalc in range(1, n_relaxed + 1):
            calc = self._generate_relaxation_calculation(
                    icalc, *args, relax_atoms=relax_atoms,
                    add_to_global_sequence=add_to_global_sequence,
                    sequence=new_sequence, root=root,
                    **kwargs)
            if calc is not None:
                new_sequence.append(calc)
        # check if sequence is completed
        if add_to_global_sequence:
            relax_seq = self._get_relaxation_sequence(*args, **kwargs)
        else:
            relax_seq = new_sequence
        completed = await self.is_relaxation_sequence_completed(
                *args, sequence=relax_seq, relax_cell=relax_cell,
                check_pressure_0=check_pressure_0,
                )
        if completed is False:
            # add another calculation. otherwise we're done
            if n_relaxed >= self.maximum_relaxations:
                # raise error we should have reached convergence normally
                root = os.path.dirname(relax_seq[0].workdir)
                self._logger.error(
                        f"Relaxation sequence rooted at '{root}' has reached"
                        " the maximum number of relaxation calculations.")
                raise SequenceNotConvergedError(self, sequence_part="scf_")
            # append a calc
            calc = self._generate_relaxation_calculation(
                    n_relaxed + 1, *args, relax_atoms=relax_atoms,
                    add_to_global_sequence=add_to_global_sequence,
                    sequence=new_sequence, root=root,
                    **kwargs)
            new_sequence.append(calc)
        return new_sequence

    async def _check_last_calc_converged(
            self, sequence: Sequence[SequenceCalculation]) -> bool:
        last_calcdir = await sequence[-1].calculation_directory
        last_status = await last_calcdir.status
        if "calculation_converged" not in last_status:
            return False
        return last_status["calculation_converged"]

    async def is_sequence_completed(self) -> bool:
        """Return true if sequence is completed."""
        if not self.sequence:
            await self.init_sequence()
        if self._truly_completed:
            return True
        completed = await self.is_relaxation_sequence_completed(
                relax_cell=self.relax_cell,
                check_pressure_0=True,
                sequence=self.sequence,
                )
        if completed is None:
            return False
        return completed

    async def is_relaxation_sequence_completed(
            self, *args, check_pressure_0: bool = True,
            sequence: Sequence[SequenceCalculation] = None,
            relax_cell: bool = False,
            **kwargs) -> bool:
        """Return true if relaxation sequence is completed."""
        if sequence is None:
            sequence = self._get_relaxation_sequence(*args)
        n_relaxed = len(sequence)
        # check if sequence is completed. If not, launch another calc if we can
        if check_pressure_0:
            last_calc_pressure0 = await self._check_last_calc_pressure_0(
                *args, **kwargs)
            if last_calc_pressure0 is None:
                # no calcs written atm
                return None
        else:
            last_calc_pressure0 = True  # emulate 0 pressure
        # check if last calcs has converged
        last_calc_converged = await self._check_last_calc_converged(sequence)
        try:
            if not await QESCFSequencer.is_sequence_completed(
                    self, sequence=sequence,
                    raise_error_if_calculation_failed=True,
                    reverse_order=True, **kwargs):
                # wait for sequence to complete
                return None
        except (CalculationFailedError, CalculationNotConvergedError):
            # at least one calculation has not converged...
            # bypass all convergence check
            # in order to continue from where it failed
            for seqcalc in sequence:
                calcdir = await seqcalc.calculation_directory
                status = await calcdir.status
                if not status["calculation_converged"] and status[
                        "calculation_finished"] is not False:
                    # here calc either finished or errored
                    seqcalc.bypass_convergence_check = True
            # check which one, if it's the last one, launch a new calc
            # if it's not the last one, never mind
            if last_calc_converged:
                # the last calc was converge -> we're done!
                return True
            # check if last calc pressure is 0. if so we're done
            # unless we only relaxed atoms and we relax cell to
            if (last_calc_pressure0 or last_calc_pressure0 is None) and (
                    check_pressure_0):
                # last calc's pressure is 0 so we're done!
                return True
            # the last calc did not converge and pressure > 0 => do a new calc
            return False
        if check_pressure_0:
            if last_calc_pressure0:
                # make sure we run at least 2 relaxations
                if (relax_cell and n_relaxed >= 2) or not relax_cell:
                    return True
                return False
            self._logger.info(
                    "Last calculation did not reach a pressure of 0.")
            return False
        # we don't care about pressure
        # make sure we're converged
        if last_calc_converged:
            # make sure we run at least 2 relaxations
            if (relax_cell and n_relaxed >= 2) or not relax_cell:
                return True
            return False
        return False
        # if (last_calc_pressure0 and check_pressure_0):
        #  or last_calc_converged:
        #     # last calculation's pressure is 0 or we're converged
        # -> we're done
        #     # unless we have done at least 2 relaxations
        #     if (relax_cell and n_relaxed >= 2) or not relax_cell:
        #         return True
        #     return False
        # return False

    def _get_relaxation_sequence(
            self, *args, **kwargs) -> Sequence[SequenceCalculation]:
        return self.sequence

    def _generate_relaxation_calculation(
            self, icalc, *args, relax_atoms: bool = None,
            add_to_global_sequence: bool = True,
            sequence: Sequence[SequenceCalculation] = None,
            root: str = None,
            **kwargs) -> SequenceCalculation:
        if relax_atoms is None:
            relax_atoms = self.relax_atoms
        if icalc == 1 and not relax_atoms:
            # nothing to do
            return
        # add the calculations following the first one
        # pop out the geometry variables
        # so that they can be loaded from last calc without being overwritten
        invars = self._get_relaxation_input_variables(
                *args, relax_atoms=relax_atoms, **kwargs)
        for varname in QEPWLauncher._geometry_variables:
            if "*" in varname:
                begin = varname.split("*")[0]
                end = varname.split("*")[1]
                topop = []
                for invar in invars:
                    if invar.startswith(begin) and invar.endswith(end):
                        topop.append(invar)
                for invar in topop:
                    invars.pop(invar)
            else:
                invars.pop(varname, None)
        seq = SequenceCalculation(
                "qe_pw", self._get_relaxation_workdir(
                    icalc, *args, root=root, **kwargs),
                invars, self.scf_calculation_parameters,
                loglevel=self._loglevel)
        if sequence is None:
            sequence = self._get_relaxation_sequence(*args, **kwargs)
        seq.recover_from = sequence[-1]
        seq.recover_from.bypass_convergence_check = True
        seq.bypass_convergence_check = True
        seq.bypass_error_check = True
        seq.recover_from_other_kwargs["use_last_geometry"] = True
        seq.recover_from_update_variables = {"calculation": "vc-relax"}
        seq.dependencies.append(
                sequence[-1], set_bypass_convergence_check=False)
        if add_to_global_sequence:
            self.sequence.append(seq, set_bypass_convergence_check=False)
        return seq

    async def _check_last_calc_pressure_0(self) -> None:
        # check the last calculation's pressure. If 0, return True
        calc = await self.sequence[-1].calculation_directory
        if not await calc.exists:
            return None
        status = await calc.status
        if status["calculation_finished"] is not True:
            # not finished yet
            return None
        async with await QEPWLogFile.from_calculation(
                await self.sequence[-1].calculation_directory,
                loglevel=self._loglevel,
                ) as log:
            if log.pressure is None:
                raise LookupError(
                        "Couldn't extract pressure from log file at "
                        f"{log.path}.")
            if log.pressure == 0.0:
                self._truly_completed = True
                return True
        return False

    def _get_relaxation_workdir(self, icalc, root: str = None) -> str:
        if root is None:
            root = self.scf_workdir
        if icalc == 0 or icalc == "0":
            return os.path.join(root, "relax_atoms_only")
        return os.path.join(root, f"relax_cell_geometry_{icalc}")

    async def _get_relaxed_geometry_variables(self):
        # return variables as if we're defining them in a launcher
        # get ibrav from input file
        return await get_qe_pw_final_geometry(self.sequence[-1].workdir)
