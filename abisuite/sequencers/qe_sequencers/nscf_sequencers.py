from ..nscf_sequencers import NSCFSequencer, PositiveKpointsNSCFSequencer


class QENSCFSequencer(NSCFSequencer):
    """Sequencer class for NSCF calculation using Quantum Espresso."""

    def __init__(self, *args, **kwargs):
        NSCFSequencer.__init__(self, "qe", *args, **kwargs)

    @NSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, ivars):
        """Set the nscf input variables."""
        self._set_input_var_to(
                ivars, "calculation", "nscf")
        NSCFSequencer.nscf_input_variables.fset(self, ivars)


class QEPositiveKpointsNSCFSequencer(
        QENSCFSequencer,
        PositiveKpointsNSCFSequencer):
    """Positive NSCF kpoint sequencer for Quantum Espresso."""

    _loggername = "QEPositiveKpointsNSCFSequencer"

    def __init__(self, *args, **kwargs):
        QENSCFSequencer.__init__(self, *args, **kwargs)
        PositiveKpointsNSCFSequencer.__init__(self, "qe", *args, **kwargs)

    @property
    def nscf_input_variables(self):
        """Return the nscf input variables."""
        return QENSCFSequencer.nscf_input_variables.fget(self)

    @nscf_input_variables.setter
    def nscf_input_variables(self, ivars):
        QENSCFSequencer.nscf_input_variables.fset(self, ivars)
        PositiveKpointsNSCFSequencer.nscf_input_variables.fset(
                self, self.nscf_input_variables)
        self.nscf_input_variables["k_points"] = self._get_nscf_k_points()

    def _get_nscf_k_points(self, extra=0):
        kpts = []
        for i in range(self.nscf_kgrid[0] + extra):
            for j in range(self.nscf_kgrid[1] + extra):
                for k in range(self.nscf_kgrid[2] + extra):
                    kpts.append([i / self.nscf_kgrid[0],
                                 j / self.nscf_kgrid[1],
                                 k / self.nscf_kgrid[2],
                                 ])
        return {"parameter": "crystal",
                "k_points": kpts,
                "weights": [1 / len(kpts)] * len(kpts)}
