import asyncio
import os
from typing import Any, Mapping, Sequence

from async_property import async_property

import matplotlib.pyplot as plt

import numpy as np

from scipy.optimize import curve_fit

from .band_structure_sequencer import QEBandStructureSequencer
from .dos_sequencer import QEDOSSequencer
from .individual_phonon_sequencers import QEPhononComparatorSequencer
from .phonon_dispersion_sequencer import QEPhononDispersionSequencer
from .relaxation_sequencer import QERelaxationSequencer
from ..bases import SequenceCalculation
from ...colors import Colors
from ...constants import (
        CM_TO_EV, GPA_TO_EV_per_BOHR3,
        # HARTREE_TO_EV,
        JOULES_TO_EV,
        KELVIN_TO_RYDBERG,
        RYDBERG_TO_EV,
        k_B,
        )
from ...exceptions import LengthError
from ...handlers import (
        CalculationDirectory,
        QEKpointsMeshFile,
        QEMatdynFreqFile,
        # QEPWInputFile,
        )
from ...launchers.restarters.qe_restarters.pw_restarter import (
        get_qe_pw_final_geometry,
        )
from ...physics.lattice import qe_input_variables_to_unit_cell_volume
from ...plotters import Plot
from ...post_processors import (
        # DOSPlot,
        # ElectronicThermodynamicProperties,
        AdiabaticBulkModulus,
        AdiabaticBulkModulusPressureDerivative,
        HeatCapacityConstantPressure,
        HeatCapacityConstantVolume,
        PhononThermodynamicProperties,
        )
from ...routines import (
        is_dict_like, is_list_like, is_vector,
        )
from ...variables.bases import BaseInputVariable


def birch_murnaghan_eq_of_state(v, e0, v0, b0, b0prime):
    """Apply the Birgh-Murnaghan equation of state on a given set of volumes.

    https://en.wikipedia.org/wiki/Birch%E2%80%93Murnaghan_equation_of_state
    """
    quotient = np.divide(v0, v)
    power = np.power(quotient, 2/3)
    terme1 = power - 1
    terme2 = np.power(terme1, 3) * b0prime
    terme3 = np.power(terme1, 2)
    terme4 = 6 - 4 * power
    return e0 + 9 * v0 * b0 / 16 * (terme2 + terme3 * terme4)


# TODO: Rebase this class with a future PhononDispersionComparator sequencer.
# TODO: implement non-cubic system
class QEThermalExpansionSequencer(
        QEPhononComparatorSequencer, QEPhononDispersionSequencer,
        QEBandStructureSequencer, QEDOSSequencer,
        QERelaxationSequencer,
        ):
    """Thermal expansion sequencer for Quantum Espresso.

    Here we use a PhononConvergenceSequencer base class because it's almost
    the same thing as converging a SCF parameter onto phonon frequencies,
    as a starting point.

    A fit with the
    Birch-Murnaghan equation of state is used.

    deltas_volumes are the variation (in %) of the cell volume computed
    either from celldms given in the scf_input_variables or in the
    celldms written in the calculation stated in scf_load_geometry_from
    """

    _all_sequencer_prefixes = (
            "scf_", "nscf_", "dos_",
            "band_structure_", "phonons_", "q2r_", "matdyn_",
            "kpoints_", "matdyngrid_", "matdyndos_", "plot_",
            )
    _loggername = "QEThermalExpansionSequencer"
    _post_relaxation_scf_dirname = "final_scf"

    def __init__(self, *args, **kwargs):
        QEPhononComparatorSequencer.__init__(self, *args, **kwargs)
        QEPhononDispersionSequencer.__init__(self, *args, **kwargs)
        QEBandStructureSequencer.__init__(self, *args, **kwargs)
        QEDOSSequencer.__init__(self, *args, **kwargs)
        QERelaxationSequencer.__init__(
                self, *args, bypass_convergence_check=False, **kwargs)
        self._compute_band_structures = False
        self._compute_dosses = False
        self._compute_phonon_dosses = False
        self._deltas_volumes = None
        self._q2r_input_variables = None
        self._temperatures = None
        self._bulk_modulus_initial_guess = None
        self._relaxed_lattice_parameters = None
        self._phonons_fine_qpoint_grid = None
        self._extended_lattice_volumes = None
        self._phonons_qpoint_grid = None
        self._phonons_qpoint_grid_weights = None
        self._cv = None  # heat capacity constant volume
        self._cv_no_electrons = None
        self._cp = None  # heat capacity constant pressure
        self._cp_no_electrons = None
        self._b_ad = None  # adiabatic bulk modulus
        self._b_ad_no_electrons = None
        self._compute_electronic_contributions = False
        self._temp_cmap = None
        self._ibrav = None
        self._volumes_colors = None
        self._free_energy_data = None
        # dict of processed data
        self._f_total = None
        self._f_total_fit = None
        self._f_no_electrons_fit = None
        self._only_compute_electronic_contributions = False
        self._volumes = None
        self.relax_cell = True
        self._v0 = None

    @property
    def cv(self) -> HeatCapacityConstantVolume:
        """The heat capacity at constant volume."""
        return self._cv

    @property
    def cv_no_electrons(self) -> HeatCapacityConstantVolume:
        """The heat capacity at cst vol with no electronic contrib."""
        return self._cv_no_electrons

    @property
    def cp(self) -> HeatCapacityConstantPressure:
        """The heat capacity at constant pressure."""
        return self._cp

    @property
    def cp_no_electrons(self) -> HeatCapacityConstantPressure:
        """The heat capacity at constant pressure, no el contrib."""
        return self._cp_no_electrons

    @property
    def b_ad(self) -> AdiabaticBulkModulus:
        """The adiabatic bulk modulus."""
        return self._b_ad

    @property
    def b_ad_no_electrons(self) -> AdiabaticBulkModulus:
        """The adiabatic bulk modulus with no electronic contributions."""
        return self._b_ad_no_electrons

    @property
    def v0(self) -> float:
        """Relaxed unit cell volume (for delta = 0)."""
        return self._v0

    @v0.setter
    def v0(self, v: float) -> None:
        self._v0 = v

    @property
    def compute_band_structures(self) -> bool:
        """True if we compute band structures."""
        return self._compute_band_structures

    @compute_band_structures.setter
    def compute_band_structures(self, compute: bool) -> None:
        self._compute_band_structures = compute

    @property
    def compute_dosses(self) -> bool:
        """True if we compute dosses."""
        return self._compute_dosses

    @compute_dosses.setter
    def compute_dosses(self, compute) -> None:
        self._compute_dosses = compute

    @property
    def compute_phonon_dosses(self) -> bool:
        """True if we compute phonon dosses."""
        return self._compute_phonon_dosses

    @compute_phonon_dosses.setter
    def compute_phonon_dosses(self, compute) -> None:
        self._compute_phonon_dosses = compute

    @property
    def compute_electronic_contributions(self) -> bool:
        """True if we compute electronic contributions to free energy."""
        return self._compute_electronic_contributions

    @compute_electronic_contributions.setter
    def compute_electronic_contributions(self, contribute: bool) -> None:
        self._compute_electronic_contributions = contribute

    @property
    def only_compute_electronic_contributions(self) -> bool:
        """True if we compute only electronic contributions to free energy."""
        return self._only_compute_electronic_contributions

    @only_compute_electronic_contributions.setter
    def only_compute_electronic_contributions(self, contribute: bool) -> None:
        self._only_compute_electronic_contributions = contribute
        if contribute:
            self.compute_electronic_contributions = True

    @property
    def ibrav(self) -> int:
        """The ibrav in use for this material."""
        if self._ibrav is None:
            self._ibrav = self.scf_input_variables["ibrav"]
        return self._ibrav

    @property
    def bulk_modulus_initial_guess(self):
        """Define the bulk modulus value of the material in GPa.

        It is used to help fit the
        Birch-Murnaghan Equation of state to get the lattice expansion.
        """
        if self._bulk_modulus_initial_guess is None:
            raise ValueError("Need to set 'bulk_modulus_initial_guess'.")
        return self._bulk_modulus_initial_guess

    @bulk_modulus_initial_guess.setter
    def bulk_modulus_initial_guess(self, b):
        self._bulk_modulus_initial_guess = b

    @property
    def extended_lattice_volumes(self):
        """Define the list of extended volumes to compute."""
        if not self.sequence_completed:
            raise RuntimeError("Cannot compute 'extended_lattice_volume' "
                               "if sequence is not complete.")
        return self._extended_lattice_volumes

    @property
    def deltas_volumes(self):
        """List containing the percentage of change of volumes to compute.

        For instance, if we want to compute volumes with 0%, +/- 1% and +/- 2%
        we need to set deltas_volumes to [-2, -1, 0, 1, 2].
        """
        if self._deltas_volumes is None:
            raise ValueError("Need to set 'deltas_volumes'.")
        return self._deltas_volumes

    @deltas_volumes.setter
    def deltas_volumes(self, deltas):
        if not is_list_like(deltas):
            raise TypeError(
                    f"Expected list-like object but got '{deltas}'.")
        if len(deltas) <= 1:
            raise LengthError("Deltas len should be > than 1.")
        self._deltas_volumes = deltas

    @property
    def deltas_volumes_sorted(self) -> np.ndarray:
        """The sorted numpy array of deltas volumes."""
        return np.asarray(self.deltas_volumes)[self.deltas_volumes_order]

    @property
    def deltas_volumes_order(self) -> np.ndarray:
        """The ordering of the deltas_volumes."""
        return np.argsort(self.deltas_volumes)

    @property
    def volumes(self):
        """The list of all volumes."""
        return self._volumes

    @property
    def matdyndos_input_variables(self) -> Mapping[str, Any]:
        """The matdyn input variables for phonon dos calculations."""
        if self._matdyndos_input_variables is None:
            raise ValueError("Need to set 'matdyndos_input_variables'.")
        return self._matdyndos_input_variables

    @matdyndos_input_variables.setter
    def matdyndos_input_variables(self, ivars) -> None:
        self._set_input_var_to(ivars, "asr", self.asr)
        self._set_input_var_to(ivars, "dos", True)
        if "deltaE" not in ivars:
            raise ValueError("Need to set deltaE")
        for i in range(1, 4):
            if f"nk{i}" not in ivars:
                raise ValueError(f"Need to set 'nk{i}'.")
        self._matdyndos_input_variables = ivars

    @async_property
    async def free_energy_data(self) -> dict:
        """The free energy data dict."""
        if self._free_energy_data is not None:
            return self._free_energy_data
        data = await self._collect_data()
        # add phononic free energies
        data["temperatures"] = self.temperatures
        data["delta_volumes"] = self.deltas_volumes_sorted
        data["volumes"] = np.asarray(self.volumes)
        self._free_energy_data = data
        data["f_phonons"] = []
        if self.only_compute_electronic_contributions:
            return self._free_energy_data
        for temperature in self.temperatures:
            data["f_phonons"].append(
                    self._get_phonons_thermo_props(
                        temperature,
                        data["phonon_frequencies"],
                        data["qpts_weights"],
                        )
                    )
        return self._free_energy_data

    @property
    def kpoints_input_variables(self):
        """The input variables for the kpoints run."""
        return {"ibrav": self.ibrav,
                "mesh": self.phonons_fine_qpoint_grid,
                "shift": [0, 0, 0],  # need gamma for phonons
                "ibz_only": 0,  # only reduced pts
                }

    @property
    def n_scf_calculations(self):
        """Return the number of scf calculations to do."""
        # this parameter determines the number of calculations
        return len(self.deltas_volumes)

    @property
    def phonons_input_variables(self):
        """Define the phonon input variables."""
        return QEPhononComparatorSequencer.phonons_input_variables.fget(self)

    @phonons_input_variables.setter
    def phonons_input_variables(self, invars):
        QEPhononComparatorSequencer.phonons_input_variables.fset(self, invars)
        if not isinstance(invars, dict):
            raise TypeError("'phonons_input_variabels' must be a dict.")
        self._set_input_var_to(
                self.phonons_input_variables, "ldisp", True)
        for inq, nq in enumerate(self.phonons_qpoint_grid):
            self._set_input_var_to(
                    self.phonons_input_variables, f"nq{inq + 1}", nq)

    @property
    def phonons_fine_qpoint_grid(self) -> Sequence[float]:
        """Defines the phonons fine qpoint grid."""
        if self._phonons_fine_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_fine_qpoint_grid'.")
        return self._phonons_fine_qpoint_grid

    @phonons_fine_qpoint_grid.setter
    def phonons_fine_qpoint_grid(self, grid: Sequence[float]) -> None:
        if not is_vector(grid, length=3):
            raise TypeError(
                    "'phonons_fine_qpoint_grid' must be a list of length=3.")
        self._phonons_fine_qpoint_grid = grid

    @property
    def phonons_qpoint_grid(self):
        """Define the phonon qpoint grid."""
        if self._phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        return self._phonons_qpoint_grid

    @phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, grid):
        if not is_vector(grid, length=3):
            raise TypeError(
                    "'phonons_qpoint_grid' must be a list of length 3.")
        self._phonons_qpoint_grid = grid

    @property
    def phonons_qpoint_grid_weights(self):
        """The phonons qpoint weights for BZ integration."""
        return self._phonons_qpoint_grid_weights

    @property
    def q2r_input_variables(self):
        """Define the q2r input variables."""
        return self._q2r_input_variables

    @q2r_input_variables.setter
    def q2r_input_variables(self, ins):
        self._q2r_input_variables = ins

    @property
    def temperatures(self):
        """Temperatures to compute the lattice expansion.

        In Kelvin.
        """
        if self._temperatures is None:
            raise ValueError("Need to set 'temperatures'.")
        return self._temperatures

    @temperatures.setter
    def temperatures(self, temperatures):
        if not is_list_like(temperatures):
            raise TypeError(
                    f"Expected list-like object but got '{temperatures}'.")
        self._temperatures = temperatures

    @property
    def temperature_colors(self):
        """Temperature color list."""
        if self._temp_cmap is not None:
            return self._temp_cmap
        n_temps = len(self.temperatures)
        if n_temps == 1:
            self._temp_cmap = ["C0"]
            return self._temp_cmap
        # make a rainbow of colors for each temperature curves
        cmap = plt.get_cmap("gist_rainbow", n_temps)
        self._temp_cmap = [cmap(x / (n_temps - 1)) for x in range(0, n_temps)]
        return self._temp_cmap

    @property
    def relaxed_lattice_parameters(self) -> dict:
        """Define the relaxed lattice parameters dict."""
        if self._relaxed_lattice_parameters is None:
            raise ValueError("Need to set 'relaxed_lattice_parameters'.")
        return self._relaxed_lattice_parameters

    @relaxed_lattice_parameters.setter
    def relaxed_lattice_parameters(self, params):
        if not is_dict_like(params):
            raise TypeError("'relaxed_lattice_parameters' should be a dict.")
        self._relaxed_lattice_parameters = params

    def _get_vprime_vars(
            self, v0: float, delta: float, input_variables: dict[str, Any],
            ) -> tuple:
        rel_variation = 1 + delta / 100
        vprime = v0 * rel_variation
        ibrav = input_variables["ibrav"]
        if ibrav == 2:
            celldm1_prime = (4 * vprime) ** (1/3)
            return {"celldm(1)": celldm1_prime}, vprime
        elif ibrav == 4:
            # hexagonal
            # multiply celldm1 by sqrt[3] of delta
            celldm1 = input_variables["celldm(1)"]
            celldm1_prime = celldm1 * (rel_variation ** (1/3))
            if delta == 0.0:
                # assume already relaxed
                return (
                    {"celldm(1)": celldm1_prime,
                     "calculation": "scf",
                     }, vprime)
            # c will have same initial variation so don't
            # need to modify c/a
            return (
                {"celldm(1)": celldm1_prime,
                 }), vprime
        raise NotImplementedError(self.ibrav)

    def _preload_scf_specific_ivars(self) -> dict[str, Any]:
        if self.scf_load_geometry_from is not None:
            raise NotImplementedError
            # async with QEPWInputFile.from_calculation(
            #         self.scf_load_geometry_from,
            #         loglevel=self._loglevel) as inp:
            #     return inp.input_variables.copy()
        # get reference from input variables
        return self.scf_input_variables.copy()

    @property
    def scf_specific_input_variables(self):
        """Specific input variables for each scf calculations.

        The new vars are computed from the deltas_volumes property.
        """
        if self._scf_specific_input_variables is not None:
            # already computed
            return self._scf_specific_input_variables
        # first get the reference celldm(1)
        ivars = self._preload_scf_specific_ivars()
        v0, self._ibrav = qe_input_variables_to_unit_cell_volume(
                ivars, return_ibrav=True)
        self.v0 = v0
        specifics = []
        volumes = []
        for delta in self.deltas_volumes:
            if abs(delta) >= 100:
                # make sure we have decent percentages
                raise ValueError("Cannot exceed 100% volume expansion!")
            new_vars_for_delta, vprime = self._get_vprime_vars(
                    v0, delta, ivars)
            volumes.append(vprime)
            specifics.append(new_vars_for_delta)
        self._volumes = sorted(volumes)
        self._scf_specific_input_variables = specifics
        return specifics

    # TODO: When rebasing with a future phonon dispersion comparator sequencer
    # this property will dry out.
    @async_property
    async def sequence_completed(self):
        """Return True it the sequence has successfully completed."""
        # override this property since we add calculations after some of them
        # are finished (like the QERelaxationSequencer).
        # check if a matdyn calculation is present. if yes, return super value
        if not self.sequence:
            # FIXME: code not dry with BaseSequencer class...
            try:
                await self.init_sequence()
            except (TypeError, ValueError, KeyError):
                # some variables are not set yet
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        try:
            if not self.only_compute_electronic_contributions:
                calcs = self.get_sequence_calculation(
                        "matdyn_", always_return_list=True)
                if len(calcs) != len(self.deltas_volumes):
                    # not all matdyn calcs are written
                    return False
        except LookupError:
            # matdyn not present in sequence
            return False
        if self.ibrav == 2:
            return await (
                QEPhononComparatorSequencer.is_sequence_completed(
                    self))
        # if we have relaxation runs, check that they are all 'truly completed'
        coros = [self.is_relaxation_sequence_completed(
            sequence=self._get_relaxation_sequence(idelta),
            check_pressure_0=False, relax_cell=True)
            for idelta, delta in enumerate(self.deltas_volumes) if delta != 0.0
            ]
        relaxation_converged = await asyncio.gather(*coros)
        if 0.0 not in self.deltas_volumes:
            return all(relaxation_converged)
        delta_0_completed = (
                await QEPhononComparatorSequencer.is_sequence_completed(
                    self, sequence=self._get_relaxation_sequence(
                        self.deltas_volumes.index(0.0)),
                    ))
        return all(relaxation_converged) and delta_0_completed

    async def launch(self, *args, **kwargs):
        """Launch sequence."""
        await QEPhononComparatorSequencer.launch(self, *args, **kwargs)

    async def init_scf_sequence(self):
        """Initialize the scf part of the sequence."""
        if self.ibrav == 2:
            # for simple cubic systems -> just launch simple scfs
            await QEPhononComparatorSequencer.init_scf_sequence(self)
            # for each scf sequence calculation, keep the geometry variables
            # if we load geometry
            # i.e. keep the specifics since we expand the geometry variables
            for scfcalc, specifics in zip(
                    self.sequence, self.scf_specific_input_variables):
                scfcalc.keep_variables_after_loading_geometry = specifics
        else:
            # need to do full relaxation at fixed volumes
            relaxation_subsequences_coros = []
            for idelta, delta in enumerate(self.deltas_volumes):
                if delta == 0.0:
                    # just a regular scf
                    scf_vars = self.scf_input_variables.copy()
                    scf_vars.update(self.scf_specific_input_variables[idelta])
                    self.sequence.append(
                            SequenceCalculation(
                                "qe_pw",
                                self._get_scf_workdir_single_calculation(
                                    idelta),
                                scf_vars,
                                self.scf_calculation_parameters,
                                loglevel=self._loglevel))
                    continue
                relaxation_subsequences_coros.append(
                        self.init_relaxation_sequence(
                            idelta, relax_atoms=False, relax_cell=True,
                            check_pressure_0=False,
                            ))
            await asyncio.gather(*relaxation_subsequences_coros)
            await self.init_post_relaxation_subsequence()
        await self.init_f_el_subsequence()

    def _get_relaxation_sequence(
            self, idelta: int) -> Sequence[SequenceCalculation]:
        root = self._get_relaxation_root(idelta)
        relax_seq = []
        for seq in self.sequence:
            if seq.workdir.startswith(root):
                relax_seq.append(seq)
        return relax_seq

    async def init_post_relaxation_subsequence(self):
        """Initialize post relaxation scf runs."""
        for idelta, delta in enumerate(self.deltas_volumes):
            if delta == 0.0:
                # already handled
                continue
            relax_seq = self._get_relaxation_sequence(idelta)
            if not await self.is_relaxation_sequence_completed(
                    sequence=relax_seq, check_pressure_0=False,
                    relax_cell=True,
                    ):
                # wait for it to complete
                continue
            # launch post relaxation sequence
            ivars = self.scf_input_variables.copy()
            last_relax_calc = relax_seq[-1]
            fin_geo = await get_qe_pw_final_geometry(last_relax_calc.workdir)
            ivars.update(fin_geo)
            # load converged geometry
            seq = SequenceCalculation(
                    "qe_pw",
                    self._get_scf_workdir_single_calculation(idelta),
                    ivars,
                    self.scf_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            self.sequence.append(seq)

    def _get_post_relaxation_sequence(self) -> Sequence[SequenceCalculation]:
        # return the list of scf calcs post relaxation
        scf_calcs = []
        for idelta, delta in enumerate(self.deltas_volumes):
            if delta == 0:
                vol0wd = self._get_scf_workdir_single_calculation(idelta)
                break
        else:
            raise LookupError("Could not find delta = 0 in deltas volumes.")
        for scf_calc in self.sequence:
            basename = os.path.basename(scf_calc.workdir)
            if basename == self._post_relaxation_scf_dirname:
                scf_calcs.append(scf_calc)
                continue
            # also add the vol+0 scf
            if scf_calc.workdir == vol0wd:
                scf_calcs.append(scf_calc)
                continue
        return scf_calcs

    def _get_scf_calcs(self):
        if self.ibrav == 2:
            # for cubic lattice, just take the scf calcs already done as start
            calcs = self.get_sequence_calculation("scf_")
        else:
            # need to take post_relaxation calcs
            calcs = self._get_post_relaxation_sequence()
        tortn = []
        for scf in calcs:
            if scf.input_variables.get("disk_io", None) == "nowf":
                # electronic free energy calculation
                continue
            tortn.append(scf)
        return tortn

    async def init_f_el_subsequence(self) -> None:
        """Initialize f_el calculations."""
        if not self.compute_electronic_contributions:
            return
        # need to do f_electronic calculations
        # make a grid of scf calculations for each volume and temperature
        f_el_scf_calcs = []
        scf_calcs = self._get_scf_calcs()
        # simply launch scf calculations
        for scf_calc in scf_calcs:
            # we can launch already since do not need to wait for end
            for itemp, temperature in enumerate(self.temperatures):
                degauss = temperature * KELVIN_TO_RYDBERG
                var = scf_calc.input_variables.copy()
                var.update({
                    "degauss": degauss,
                    "smearing": "fd",
                    "occupations": "smearing",
                    "disk_io": "nowf",  # don't write wfk files
                    })
                f_el_scf_calc = SequenceCalculation(
                        "qe_pw",
                        self._get_f_el_scf_calc_workdir_from_scf_calc(
                            scf_calc, itemp),
                        var,
                        self.scf_calculation_parameters,
                        loglevel=self._loglevel,
                        )
                f_el_scf_calc.keep_variables_after_loading_geometry = (
                        scf_calc.keep_variables_after_loading_geometry)
                f_el_scf_calcs.append(f_el_scf_calc)
        if self.only_compute_electronic_contributions and self.ibrav == 2:
            # get rid of all the non f_el calcs since volume is determined
            self.sequence = f_el_scf_calcs
        else:
            self.sequence += f_el_scf_calcs

    async def init_band_structure_sequence(self):
        """Initialize band structure part of sequencer."""
        if not self.compute_band_structures:
            return
        bcalcs = []
        for scf in self._get_scf_calcs():
            bvars = scf.input_variables.copy()
            bvars.update(self.band_structure_input_variables)
            bcalc = self._get_sequence_calculation_band_structure_from_scf(
                    scf,
                    band_structure_workdir=(
                        self._get_band_structure_workdir_from_scf_calc(
                            scf)),
                    band_structure_input_variables=bvars,
                    )
            bcalcs.append(bcalc)
        self.sequence += bcalcs

    async def init_phonons_sequence(self):
        """Initialize the phonon part of the sequence."""
        if self.only_compute_electronic_contributions:
            return
        # from here, self.sequence contains only scf calcs.
        scf_calcs = self._get_scf_calcs()
        for scf_calc in scf_calcs:
            # for each scf calc, we do a full spectrum of phonon calcs
            # phonon base dir is appended with the same number as the scf calc
            await self.init_phonons_single_scf(scf_calc)

    async def init_nscf_sequence(self):
        """Initialize nscf part of sequence for dos calculations."""
        if not self.compute_dosses:
            return
        ncalcs = []
        for icalc, scf in enumerate(self._get_scf_calcs()):
            nvars = scf.input_variables.copy()
            nvars.update(self.nscf_input_variables)
            ncalc = self._init_nscf_sequence_against_scf_calc(
                    scf,
                    nscf_workdir=self._get_nscf_workdir(
                        icalc),
                    nscf_input_variables=nvars,
                    )
            ncalcs.append(ncalc)
        self.sequence += ncalcs

    async def init_dos_sequence(self):
        """Initialize dos part of sequencer."""
        if not self.compute_dosses:
            return
    #     # start with high sampled BZ nscf calc
    #     await self.init_nscf_sequence()
    #     for icalc, nscfcalc in enumerate(
    #             self.get_sequence_calculation("nscf")):
    #         doscalc = self._init_dos_sequence_against_gs_calc(
    #                 nscfcalc,
    #                 dos_workdir=self._get_dos_workdir(icalc),
    #                 )

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await QEPhononComparatorSequencer.init_sequence(self)
        # base class init sequence will launch scf and phonons
        await self.init_band_structure_sequence()
        await self.init_dos_sequence()
        # now we need to launch q2r and matdyn in order to incorporate acoustic
        # sum rules for phonon frequencies
        # only continue if nphonons have been set
        if self._nphonons is None or (
                self.only_compute_electronic_contributions):
            return
        await self.init_kpoints_sequence()
        await self.init_q2r_and_matdyn_sequence()

    def _get_kpoints_workdir_from_scf_calc(
            self, scf_calc: SequenceCalculation) -> str:
        ext = self._get_workdir_ext_from_workdir(scf_calc.workdir)
        return self._get_kpoints_workdir_from_ext(ext)

    def _get_kpoints_workdir_from_idelta(self, idelta: int) -> str:
        ext = self._get_workdir_ext(idelta)
        return self._get_kpoints_workdir_from_ext(ext)

    def _get_kpoints_workdir_from_ext(self, ext: str) -> str:
        if self.ibrav == 4:
            return os.path.join(self.kpoints_workdir, "k_points" + ext)
        return self.kpoints_workdir

    def _get_kpoints_workdir_from_q2r_calc(
            self, q2r_calc: SequenceCalculation) -> str:
        ext = self._get_workdir_ext_from_workdir(q2r_calc.workdir)
        return self._get_kpoints_workdir_from_ext(ext)

    def _get_kpoints_input_variables_from_scf_calc(
            self, scf_calc: SequenceCalculation) -> Mapping[str, Any]:
        ivars = self.kpoints_input_variables.copy()
        if self.ibrav == 4:
            ivars["celldm3"] = scf_calc.input_variables["celldm(3)"]
        return ivars

    async def init_kpoints_sequence(self):
        """Initialize the kpoints part of the sequence.

        This part only serves to generate the fine qpoint grid where
        the phonon frequencies will be calculated on.
        """
        # force local
        kpts_calc_params = {"queuing_system": "local", "mpi_command": ""}
        if self.ibrav == 2:
            # only need to do 1 calc as they are all the same!
            # (for cubic systems)
            kpoints_calc = SequenceCalculation(
                    "qe_kpoints",
                    self.kpoints_workdir,
                    self.kpoints_input_variables,
                    kpts_calc_params,
                    )
            self.sequence.append(kpoints_calc)
        else:
            # need to do 1 kpts run per volume
            for scf_calc in self._get_scf_calcs():
                kpts_calc = SequenceCalculation(
                        "qe_kpoints",
                        self._get_kpoints_workdir_from_scf_calc(scf_calc),
                        self._get_kpoints_input_variables_from_scf_calc(
                            scf_calc),
                        kpts_calc_params,
                        loglevel=self._loglevel,
                        )
                self.sequence.append(kpts_calc)

    async def init_q2r_and_matdyn_sequence(self):
        """Initialize the q2r and the matdyn part of the sequence."""
        # iterate over the whole sequence (from here includes the scf and ph)
        # for each scf calcs, make a q2r, a matdyn and a matdyngrid calc
        q2r_calcs = []
        matdyn_calcs = []
        scf_calcs = self._get_scf_calcs()
        for calc in scf_calcs:
            # create q2r calculation
            q2r_calc = SequenceCalculation(
                    "qe_q2r",
                    self._get_q2r_workdir_single_scf(calc),
                    self.q2r_input_variables,
                    self.q2r_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            # dependencies are all the phonon calculations for this scf calc
            for calc2 in self.get_sequence_calculation("phonons"):
                if calc in calc2.dependencies:
                    q2r_calc.dependencies.append(calc2)
            q2r_calcs.append(q2r_calc)
            # create matdyngrid and matdyn sequence at the same time
            try:
                matdyngrid_vars = await self._get_matdyngrid_input_variables(
                        q2r_calc)
            except NotADirectoryError:  # as err:
                # kpoints calc not done most likely
                # self._logger.exception(err)
                return
            matdyngrid_calc = SequenceCalculation(
                    "qe_matdyn",
                    self._get_matdyngrid_workdir_single_scf(calc),
                    matdyngrid_vars,
                    self.matdyn_calculation_parameters,
                    loglevel=self._loglevel)
            matdyngrid_calc.dependencies.append(q2r_calc)
            matdyn_calcs.append(matdyngrid_calc)
            # matdyn part now (for phonon dispersion)
            matdyn_calc = SequenceCalculation(
                    "qe_matdyn", self._get_matdyn_workdir_single_scf(calc),
                    self.matdyn_input_variables.copy(),
                    self.matdyn_calculation_parameters,
                    loglevel=self._loglevel)
            matdyn_calc.dependencies.append(q2r_calc)
            matdyn_calcs.append(matdyn_calc)
        self.sequence += q2r_calcs + matdyn_calcs

    async def plot_comparision(self) -> Mapping[str, Any]:
        """Post process data to get thermal expansion plots."""
        all_plots = {}
        for plot_ext, plot_func in {
                "etot": self._get_etot_plot,
                "chemical_potential": self._get_chemical_potential_plot,
                "electron_free_energy": self._get_f_electron_plot,
                "phonon_free_energy": self._get_f_phonon_plot,
                "total_free_energy": self._get_total_free_energy_plot,
                "lattice_expansion": self._get_lattice_expansion_plot,
                "heat_capacity": self._get_heat_capacity_plots,
                "bulk_modulus": self._get_bulk_modulus_plot,
                "bulk_modulus_derivative": (
                    self._get_bulk_modulus_derivative_plot),
                "electronic_entropy": self._get_electronic_entropy_plot,
                "electronic_internal_energy": (
                    self._get_electronic_internal_energy_plot),
                "phonon_dispersions": self._get_phonon_dispersions_plot,
                "negative_frequencies_weight": (
                    self._get_negative_phonon_frequencies_weight_plot),
                "negative_frequencies_f_total_contributions": (
                    self._get_negative_phonons_f_total_contributions),
                }.items():
            plot = await plot_func()
            if plot is None:
                continue
            all_plots[plot_ext] = plot
            if is_dict_like(plot):
                for subext, pl in plot.items():
                    try:
                        ext = f"{plot_ext}_{subext}"
                        await self._post_process_plot(
                            pl, name_extension=ext)
                    except Exception as err:
                        self._logger.error(
                                f"An error occured when processing plot {ext}")
                        self._logger.exception(err)
            else:
                await self._post_process_plot(plot, name_extension=plot_ext)
        return all_plots

    async def _get_equilibrium_lattice(
            self, no_electrons: bool = False,
            return_volumes: bool = False,
            ) -> Sequence[float]:
        vols = []
        data = await self.free_energy_data
        for itemp in range(len(data["temperatures"])):
            if no_electrons:
                fit_data = await self._get_f_no_electrons_fit(itemp)
            else:
                fit_data = await self._get_f_total_fit(itemp)
            vols.append(fit_data["parameters"][1])
        vols = np.asarray(vols)
        if return_volumes:
            return vols
        if self.ibrav == 2:
            return np.power(4 * vols, 1 / 3)
        elif self.ibrav == 4:
            return None
        raise NotImplementedError(self.ibrav)

    async def _get_heat_capacity_plots(self) -> Mapping[str, Plot]:
        temps = self.temperatures
        data = await self.free_energy_data
        veq = await self._get_equilibrium_lattice(return_volumes=True)
        veq_noelectrons = await self._get_equilibrium_lattice(
                no_electrons=True, return_volumes=True)
        cv = HeatCapacityConstantVolume(
                lattice_vs_temperature=await self._get_equilibrium_lattice(),
                equilibrium_volumes=veq,
                ibrav=self.ibrav,
                temperatures=self.temperatures,
                volumes=np.asarray(self.volumes),
                phonon_frequencies=data["phonon_frequencies"],
                weights=data["qpts_weights"],
                )
        self._cv = cv
        plots = {}
        if self.compute_electronic_contributions:
            cv.s_electrons = data["s_electrons"]
            cls = AdiabaticBulkModulus
            cv_no_electrons = HeatCapacityConstantVolume(
                    lattice_vs_temperature=await self._get_equilibrium_lattice(
                        no_electrons=True),
                    equilibrium_volumes=veq_noelectrons,
                    ibrav=self.ibrav,
                    temperatures=self.temperatures,
                    volumes=np.asarray(self.volumes),
                    phonon_frequencies=data["phonon_frequencies"],
                    weights=data["qpts_weights"],
                    )
            self._cv_no_electrons = cv_no_electrons
            meth = self._get_f_no_electrons_fit
            bm_noel = [(await meth(itemp))["parameters"][2]
                       for itemp in range(len(temps))]
            self._b_ad_no_electrons = cls.from_heat_capacity_constant_volume(
                    cv_no_electrons, loglevel=self._loglevel,
                    electrons=False,
                    isothermal_bulk_modulus=bm_noel,
                    )
            cls = HeatCapacityConstantPressure
            cp_noel = cls.from_heat_capacity_constant_volume(
                    cv_no_electrons, electrons=False, loglevel=self._loglevel,
                    bulk_modulus=bm_noel,
                    )
            self._cp_no_electrons = cp_noel
            plots["phonon_no_electrons"] = (
                    cv_no_electrons.get_phonon_heat_capacity_plot())
            plots["cp_offset_no_electrons"] = (
                    cp_noel.get_cp_offset_plot())
            plots["alpha_no_electrons"] = cv_no_electrons.get_alpha_plot()
            plots["phonons_constant_pressure_no_electrons"] = (
                    cp_noel.get_phonon_heat_capacity_pressure_plot())
        self._b_ad = AdiabaticBulkModulus.from_heat_capacity_constant_volume(
                cv, loglevel=self._loglevel,
                electrons=self.compute_electronic_contributions,
                isothermal_bulk_modulus=[
                    (await self._get_f_total_fit(itemp))["parameters"][2]
                    for itemp in range(len(temps))],
                )
        cp = HeatCapacityConstantPressure.from_heat_capacity_constant_volume(
                cv, loglevel=self._loglevel,
                # values here are in EV / bohr^3
                bulk_modulus=self.b_ad.adiabatic_bulk_modulus,
                electrons=self.compute_electronic_contributions,
                )
        self._cp = cp
        plots["phonons"] = (
                cv.get_phonon_heat_capacity_plot())
        plots["phonons_constant_pressure"] = (
                cp.get_phonon_heat_capacity_pressure_plot())
        plots["alpha"] = cv.get_alpha_plot()
        plots["cp_offset"] = cp.get_cp_offset_plot()
        if self.compute_electronic_contributions:
            plots["electrons"] = (
                    cv.get_electrons_heat_capacity_plot())
            plots["electrons_constant_pressure"] = (
                    cp.get_electrons_heat_capacity_pressure_plot())
            plots["total"] = (
                cp.get_total_heat_capacity_plot())
        return plots

    async def _get_negative_phonons_f_total_contributions(self):
        if self.only_compute_electronic_contributions:
            return
        negative = await self._get_f_phonons_negative_idx(0)
        if not any(negative):
            return
        self._logger.info(
                "Plotting negative phonons f_total contributions.")
        # for each negative phonons (different linestyles for each vols)
        # plot the equivalent contributions from the other volumes
        data = await self.free_energy_data
        plot = Plot(loglevel=self._loglevel)
        ilinestyle = 0
        linestyles = ["-", "--", ":"]
        # do graph at largest temperature to have upper bound
        temps = data["temperatures"]
        temp = temps[-1]
        kt = k_B * JOULES_TO_EV * temp
        volumes = data["volumes"]
        for ivol, neg in enumerate(negative):
            if not neg:
                continue
            linestyle = linestyles[ilinestyle]
            ilinestyle += 1
            # add phonon frequencies contrib
            negative_phonons = data["f_phonons"][0][ivol].negative_phonons
            cumulative_contributions = {}
            for icolor, (iqpt, ibranch) in enumerate(
                    zip(*negative_phonons)):
                contributions = []
                vols = []
                for ivol2, vol in enumerate(data["volumes"]):
                    thermo_prop = data["f_phonons"][0][ivol2]
                    freqs = thermo_prop.frequencies
                    freq = freqs[iqpt, ibranch]
                    if freq < 0:
                        continue
                    cumulative_contributions.setdefault(vol, 0)
                    vols.append(vol)
                    weight = thermo_prop.weights[iqpt]
                    sinh = np.sinh(freq / (2 * kt))
                    log = np.log(2 * sinh)
                    contribution = kt * log * weight
                    contributions.append(contribution)
                    cumulative_contributions[vol] += contribution
                plot.add_curve(
                        vols, contributions,
                        color=f"C{icolor}", linestyle=linestyle,
                        linewidth=1,
                        )
            plot.add_curve(
                    vols, [cumulative_contributions[vol] for vol in vols],
                    linestyle=linestyle, linewidth=2,
                    color="k",
                    label=f"Cumul. contrib. for vol {volumes[ivol]}")
        plot.xlabel = r"Volume [bohr$^3$]"
        plot.ylabel = r"$F_{phon}(q, \nu, V, T)$ [eV]"
        return plot

    async def _get_negative_phonon_frequencies_weight_plot(self):
        if self.only_compute_electronic_contributions:
            return
        negative = await self._get_f_phonons_negative_idx(0)
        if not any(negative):
            return
        self._logger.info(
                "Plotting weights contribution from negative phonons.")
        # plot sum weights[negative] vs volume
        data = await self.free_energy_data
        thermo_props = data["f_phonons"][0]
        # nvols x nqpts array
        positive_weights = np.array(
                [np.where(t.frequencies >= 0.0)[0] for t in thermo_props],
                dtype=object,
                )
        weights = np.array([t.weights for t in thermo_props])
        nbranches = thermo_props[0].frequencies.shape[1]
        totsum = np.array([np.sum(w) for w in weights]) * nbranches
        sums = [np.sum(ws[pos_ws]) for ws, pos_ws in zip(
            weights, positive_weights)]
        sums = (1 - np.divide(np.array(sums), totsum)) * 100
        volumes = data["volumes"]
        plot = Plot(loglevel=self._loglevel)
        plot.add_curve(
                volumes, sums,
                linestyle="-", markerfacecolor="C0", marker="o",
                color="C0", markersize=5,
                label="All",
                )
        plot.xlabel = r"V [bohr$^3$]"
        plot.ylabel = r"1 - $\Sigma_{q,\omega_q>0}w_q$ / $\Sigma_q w_q$ [%]"
        return plot

    def _get_fit_parameters(self, volumes, gs_energies, f):
        v0_index = int(len(volumes) // 2)  # middle (assume ordered data)
        v0_guess = volumes[v0_index]
        e0_guess = gs_energies[v0_index]
        if len(volumes) < 4:
            raise ValueError(
                    "Need more than 4 volumes with correct phonon freqs "
                    "to fit Birch-Murnaghan eq. of state (has 4 params).")
        # if parameter_range is not None:
        #     # check range is good
        #     if not isinstance(parameter_range, int):
        #         raise TypeError(
        #                 "parameter_range should be an int (list index).")
        #     if parameter_range > v0_index:
        #         raise ValueError("parameter_range should be abovetheV0_idx.")
        #     if parameter_range < 0:
        #         raise ValueError("parameter_range should be a number > 0.")
        #     volumes = volumes[
        #             v0_index - parameter_range:v0_index + parameter_range +1]
        #     free_energy_vs_volume = free_energy_vs_volume[
        #             v0_index - parameter_range:v0_index + parameter_range +1]
        #     gs_energies = gs_energies[
        #             v0_index - parameter_range:v0_index + parameter_range +1]
        # if previous_fit_parameters is not None:
        #     p0 = previous_fit_parameters
        # else:
        p0 = [e0_guess, v0_guess,
              self.bulk_modulus_initial_guess * GPA_TO_EV_per_BOHR3,
              4,  # B0 prime initial guess
              ]
        params, pcov = curve_fit(
                birch_murnaghan_eq_of_state,
                volumes,
                f,
                p0=p0, maxfev=10000,
                )
        vols = np.linspace(min(volumes), max(volumes), 1000)
        return birch_murnaghan_eq_of_state(
                vols,
                *params), params, vols

    async def _get_f_no_electrons_fit(self, itemp):
        if self._f_no_electrons_fit is None:
            self._f_no_electrons_fit = {}
        if itemp in self._f_no_electrons_fit:
            return self._f_no_electrons_fit[itemp]
        f_phon = await self._get_f_phonons(itemp)
        # compute fit
        data = await self.free_energy_data
        fit, params, vols = self._get_fit_parameters(
                data["volumes"],
                data["gs_internal_energies"],
                f_phon + data["gs_internal_energies"])
        self._f_no_electrons_fit[itemp] = {
                "fit": fit,
                "parameters": params,
                "volumes": vols,
                }
        return self._f_no_electrons_fit[itemp]

    async def _get_f_total_fit(self, itemp):
        if self._f_total_fit is None:
            self._f_total_fit = {}
        if itemp in self._f_total_fit:
            return self._f_total_fit[itemp]
        f_tot = await self._get_f_total(itemp)
        # compute fit
        data = await self.free_energy_data
        volumes = data["volumes"]
        gs_energies = await self._get_f_electrons(itemp)
        # efs = data["chemical_potentials"][:, itemp]
        fit, params, vols = self._get_fit_parameters(
                volumes, gs_energies, f_tot)
        self._f_total_fit[itemp] = {
                "fit": fit,
                "parameters": params,
                "volumes": vols,
                }
        return self._f_total_fit[itemp]

    async def _get_f_total(self, itemp):
        if self._f_total is None:
            self._f_total = {}
        if itemp in self._f_total:
            return self._f_total[itemp]
        f_electrons = await self._get_f_electrons(itemp)
        f_phonons = await self._get_f_phonons(itemp)
        self._f_total[itemp] = f_electrons + f_phonons
        return self._f_total[itemp]

    async def _get_bulk_modulus_derivative_plot(self):
        if self.only_compute_electronic_contributions:
            return
        self._logger.info("Plotting bulk modulus derivative B'(T)")
        plots = {}
        data = await self.free_energy_data
        plots["isothermal"] = await self._get_fitted_parameter_plot(
                3, "Bulk modulus derivative", "B'")
        bprime = [(await self._get_f_total_fit(itemp))["parameters"][3]
                  for itemp in range(len(data["temperatures"]))]
        bprime_ad = AdiabaticBulkModulusPressureDerivative(
                loglevel=self._loglevel,
                isothermal_bulk_modulus=self.b_ad.isothermal_bulk_modulus,
                isothermal_bulk_modulus_pressure_derivative=bprime,
                alpha=self.cp.alpha,
                adiabatic_bulk_modulus=self.b_ad.adiabatic_bulk_modulus,
                temperatures=self.b_ad.temperatures,
                )
        plots["adiabatic"] = (
                bprime_ad.get_adiabatic_bulk_modulus_pressure_derivative_plot(
                    ylabel=r"$B'$", yunits=None,
                    )
                )
        plots["adiabatic_offset"] = bprime_ad.get_adiabatic_offset_plot(
                )
        plots["adiabatic_scaling"] = bprime_ad.get_adiabatic_scaling_plot()
        if self.compute_electronic_contributions:
            meth = self._get_f_no_electrons_fit
            bprime_noel = [(await meth(itemp))["parameters"][3]
                           for itemp in range(len(data["temperatures"]))]
            bprime_ad = AdiabaticBulkModulusPressureDerivative(
                cp=self.cp_no_electrons,
                loglevel=self._loglevel,
                isothermal_bulk_modulus=(
                    self.b_ad_no_electrons.isothermal_bulk_modulus),
                isothermal_bulk_modulus_pressure_derivative=(
                    bprime_noel),
                alpha=self.cp_no_electrons.alpha,
                adiabatic_bulk_modulus=(
                    self.b_ad_no_electrons.adiabatic_bulk_modulus),
                temperatures=self.b_ad.temperatures,
                )
            meth = (
                bprime_ad.get_adiabatic_bulk_modulus_pressure_derivative_plot)
            plots["adiabatic_no_electrons"] = meth(
                        ylabel=r"$B'$", yunits=None, label="no electrons",
                        )
            plots["adiabatic_offset_no_electrons"] = (
                    bprime_ad.get_adiabatic_offset_plot(
                        ))
            plots["adiabatic_scaling_no_electrons"] = (
                    bprime_ad.get_adiabatic_scaling_plot())
        return plots

    async def _get_bulk_modulus_plot(self):
        if self.only_compute_electronic_contributions:
            return
        self._logger.info("Plotting bulk modulus B(T)")
        plots = {}
        plots["isothermal"] = await self._get_fitted_parameter_plot(
                2, "Bulk modulus", r"B [eV/bohr$^3$]")
        plots["adiabatic"] = self.b_ad.get_adiabatic_bulk_modulus_plot()
        plots["factor"] = self.b_ad.get_adiabatic_factor_plot()
        if self.compute_electronic_contributions:
            # isothermal already contains curve without electronic contrib
            bad = self.b_ad_no_electrons
            plots["adiabatic"] += bad.get_adiabatic_bulk_modulus_plot(
                    label="no electrons")
            plots["factor"] += bad.get_adiabatic_factor_plot(
                    label="no electrons")
        return plots

    async def _get_lattice_expansion_plot(self) -> None:
        if self.only_compute_electronic_contributions:
            return
        if self.ibrav == 2:
            def post(v):
                return np.power(4 * np.asarray(v), 1/3)
            self._logger.info("Plotting lattice expansion a(T)")
            plot = await self._get_fitted_parameter_plot(
                    1, "Lattice thermal expansion",
                    "Lattice constant [bohr]",
                    post_process_func=post,
                    )
            plot.add_hline(
                    self.relaxed_lattice_parameters["celldm(1)"],
                    linestyle="--", linewidth=2, color="k",
                    label=r"$a^{DFT}$",
                    )
            return plot
        elif self.ibrav == 4:
            # for non cubic -> we can only plot volume and redo a relax after
            plots = {}
            self._logger.info("Plotting lattice expansion V(T)")
            plot = await self._get_fitted_parameter_plot(
                    1, "Volume thermal expansion",
                    r"Equilibrium volume (bohr$^3$)",
                    )
            plots["v"] = plot
            volumes = plot.plot_objects["curves"][0].ydata
            post_relax_done, post_relax_seqs = (
                    await self._equilibrium_volume_relaxations_done(volumes))
            if post_relax_done:
                equilibrium_lattice_parameters = (
                        await self._get_equilibrium_lattice_parameters(
                            post_relax_seqs))
                # plot for celldm(1)
                temps = self.temperatures
                plot = Plot()
                self._logger.info("Plotting a(T) and c(T)")
                a = equilibrium_lattice_parameters["celldm(1)"]
                c_a = equilibrium_lattice_parameters["celldm(3)"]
                plot.add_curve(
                        temps, a,
                        linestyle="-", marker="s", color="C0",
                        label="a", markersize=3, markerfacecolor="C0",
                        )
                plot.add_curve(
                        temps, np.multiply(a, c_a), linestyle="-",
                        marker="o", color="C1", label="c", markersize=3,
                        markerfacecolor="C1",
                        )
                plots["lattice_parameters"] = plot
            return plots
        else:
            raise NotImplementedError(self.ibrav)

    async def _get_equilibrium_lattice_parameters(
            self, post_relax_seqs: dict[float, Sequence[SequenceCalculation]],
            ) -> Sequence[dict[str, float]]:
        eq_lat_params = {"celldm(1)": [], "celldm(3)": []}
        if self.ibrav not in (2, 4):
            raise NotImplementedError(self.ibrav)
        for seqs in post_relax_seqs.values():
            last_calc = seqs[-1]
            final_geometry = await get_qe_pw_final_geometry(
                    await last_calc.calculation_directory)
            celldm1 = final_geometry["celldm(1)"]
            if isinstance(celldm1, BaseInputVariable):
                celldm1 = celldm1.value
            eq_lat_params["celldm(1)"].append(celldm1)
            if self.ibrav == 4:
                celldm3 = final_geometry["celldm(3)"]
                if isinstance(celldm3, BaseInputVariable):
                    celldm3 = celldm3.value
                eq_lat_params["celldm(3)"].append(celldm3)
        return eq_lat_params

    async def _equilibrium_volume_relaxations_done(
            self, volumes: Sequence[float]) -> None:
        """Return true if the equilibrium lattice parameters for non-cubic."""
        scf_root = os.path.dirname(self.scf_workdir)
        eq_lat_param_runs = {}
        for temp, volume in zip(self.temperatures, volumes):
            rounded_temp = round(temp, 2)  # hope no conflict arise
            root = os.path.join(
                        scf_root, "equilibrium_volumes_runs",
                        f"{rounded_temp}K")
            starting_ivars = (
                    await self._get_equilibrium_lattice_parameters_scf_vars(
                        volume))
            relax_seq = await self.init_relaxation_sequence(
                    0,  # dummy here TODO: get rid of this
                    relax_atoms=False, relax_cell=True, check_pressure_0=False,
                    root=root, add_to_global_sequence=False,
                    starting_input_variables=starting_ivars,
                    )
            eq_lat_param_runs[rounded_temp] = relax_seq
        # launch them
        any_launched = False
        for temp, seqs in eq_lat_param_runs.items():
            any_launched_this_temp = False
            msg = Colors.color_text(
                    f"Post sequence not completed yet for T={temp}K",
                    "yellow", "bold")
            for seq in seqs:
                if not await seq.is_ready_to_launch:
                    continue
                self._logger.info(f"Launching {seq}.")
                await seq.launch(run=True)
                if not any_launched_this_temp:
                    self._logger.info(msg)
                any_launched, any_launched_this_temp = True, True
            if not await self.is_relaxation_sequence_completed(
                    check_pressure_0=False, relax_cell=True,
                    sequence=seqs,
                    ) and not any_launched_this_temp:
                self._logger.info(msg)
                any_launched = True
        return not any_launched, eq_lat_param_runs

    async def _get_equilibrium_lattice_parameters_scf_vars(
            self, volume: float) -> dict[str, Any]:
        """Get equilibrium lattice parameter relaxation scf vars."""
        scf_vars = self._preload_scf_specific_ivars()
        scf_vars = self._get_relaxation_input_variables(
                0,  # dummy
                ivars=scf_vars,
                update_with_specific_input_variables=False,
                )
        delta = (volume - self.v0) / self.v0 * 100
        vprime_vars, vprime = self._get_vprime_vars(self.v0, delta, scf_vars)
        assert np.isclose(vprime, volume), f"{vprime} != {volume}"
        scf_vars.update(vprime_vars)
        return scf_vars

    async def _get_fitted_parameter_plot(
            self, fitted_parameter_idx: int,
            plot_title: str, ylabel: str,
            post_process_func: callable = None,
            post_process_func_args: Sequence[Any] = None,
            ) -> Plot:
        data = await self.free_energy_data
        temps = data["temperatures"]
        plot = Plot(loglevel=self._loglevel)
        if post_process_func is None:
            def post_process_func(parameters, *args):
                return parameters
        if post_process_func_args is None:
            post_process_func_args = []
        if self.compute_electronic_contributions:
            parameters = []
            for itemp, _ in enumerate(temps):
                fit = await self._get_f_total_fit(itemp)
                parameters.append(fit["parameters"][fitted_parameter_idx])
            plot.add_curve(
                temps, post_process_func(parameters, *post_process_func_args),
                label=r"$F=F_e+F_{ph}$",
                linewidth=2, marker="s",
                markerfacecolor="C0",
                color="C0", markersize=5,
                )
        parameters = []
        for itemp, _ in enumerate(temps):
            fit = await self._get_f_no_electrons_fit(itemp)
            parameters.append(fit["parameters"][fitted_parameter_idx])
        plot.add_curve(
            temps, post_process_func(parameters, *post_process_func_args),
            label=r"$F=F_0 + F_{ph}$ (ph only)",
            linewidth=2, marker="s", markerfacecolor="C1",
            color="C1", markersize=5,
            )
        plot.title = plot_title
        plot.xlabel = "Temperature [K]"
        plot.ylabel = ylabel
        return plot

    async def _get_electronic_internal_energy_plot(self):
        if not self.compute_electronic_contributions:
            return
        data = await self.free_energy_data
        self._logger.info("Plotting internal energy term U(V, T)")
        temps = data["temperatures"]
        plot = Plot(loglevel=self._loglevel)
        plot_diff = Plot(loglevel=self._loglevel)
        for color, (idelta, delta) in zip(
                self.volumes_colors,
                enumerate(data["delta_volumes"])):
            us = data["u_electrons"][idelta, :]
            plot.add_curve(
                    temps, us, color=color, markerfacecolor=color,
                    marker="s", markersize=5, linewidth=2, linestyle="-",
                    label=r"$\delta V=$" + f"{delta}%",
                    )
            plot_diff.add_curve(
                    temps, us - us[0], color=color, markerfacecolor=color,
                    marker="s", markersize=5, linewidth=2, linestyle="-",
                    label=r"$\delta V=$" + f"{delta}%",
                    )
        plot.ylabel = r"$U$ [eV]"
        plot.xlabel = r"T [K]"
        plot.title = "Electrons internal energy"
        plot_diff.ylabel = r"$\Delta U$ [eV]"
        plot_diff.xlabel = r"T [K]"
        plot_diff.title = "Electrons internal energy difference"
        return {"u_vs_T": plot, "Delta_u_vs_T": plot_diff}

    async def _get_electronic_entropy_plot(self):
        if not self.compute_electronic_contributions:
            return
        self._logger.info("Plotting entropy term -TS(V, T)")
        data = await self.free_energy_data
        temps = data["temperatures"]
        plot = Plot(loglevel=self._loglevel)
        for color, (idelta, delta) in zip(
                self.volumes_colors,
                enumerate(data["delta_volumes"])):
            ts = data["s_electrons"][idelta, :]
            plot.add_curve(
                    temps, ts, color=color, markerfacecolor=color,
                    marker="s", markersize=5, linewidth=2, linestyle="-",
                    label=r"$\delta V=$" + f"{delta}%",
                    )
        # add 0K curve = 0
        plot.add_hline(0, color="k", linewidth=2, label="T=0K")
        plot.ylabel = r"$-TS$ [eV]"
        plot.xlabel = r"T [K]"
        plot.title = "Electrons entropic energy"
        return plot

    async def _get_total_free_energy_plot(self):
        # plot the total free energy
        if self.only_compute_electronic_contributions:
            return
        self._logger.info("Plotting total free energy F(V, T)")
        all_plots = {}
        data = await self.free_energy_data
        temps = data["temperatures"]
        # need to compute corrections
        volumes = data["volumes"]
        all_plots = {}
        plot_no_electrons = Plot(loglevel=self._loglevel)
        for itemp, (temp, color) in enumerate(
                zip(temps, self.temperature_colors)):
            f_phon = (
                    data["gs_internal_energies"] +
                    await self._get_f_phonons(itemp))
            plot_no_electrons.add_curve(
                    volumes, f_phon,
                    label=f"T={temp}K", linewidth=2,
                    # marker="o",
                    # markersize=5, markerfacecolor=color,
                    color=color,
                    )
            fit_data = await self._get_f_no_electrons_fit(itemp)
            plot_no_electrons.add_curve(
                    fit_data["volumes"],
                    fit_data["fit"],
                    label="Fit" if itemp == 0 else "",
                    linestyle="--", linewidth=2,
                    )
            negative = await self._get_f_phonons_negative_idx(itemp)
            positive = np.logical_not(negative)
            plot_no_electrons.add_curve(
                    volumes[positive], f_phon[positive],
                    marker="o", markersize=5, markerfacecolor=color,
                    linestyle="", color=color)
            if any(negative):
                plot_no_electrons.add_curve(
                    volumes[negative], f_phon[negative],
                    marker="x", markersize=5, markerfacecolor=color,
                    linestyle="", color=color)
        if self.compute_electronic_contributions:
            plot = Plot(loglevel=self._loglevel)
            for itemp, (temp, color) in enumerate(
                    zip(temps, self.temperature_colors)):
                f_total = await self._get_f_total(itemp)
                f_total_fit = await self._get_f_total_fit(itemp)
                plot.add_curve(
                    volumes, f_total,
                    label=f"T={temp}K",
                    color=color, linewidth=2,
                    # marker="o", markersize=5,
                    # markerfacecolor=color
                    )
                plot.add_curve(
                        volumes[positive], f_total[positive],
                        marker="o", markersize=5, markerfacecolor=color,
                        color=color, linestyle="")
                if any(negative):
                    plot.add_curve(
                            volumes[negative], f_total[negative],
                            marker="x", markersize=5, markerfacecolor=color,
                            color=color, linestyle="",
                            )
                fit_label = "Fit" if itemp == 0 else ""
                plot.add_curve(
                        f_total_fit["volumes"], f_total_fit["fit"],
                        color=color, linewidth=2,
                        linestyle="--", label=fit_label,
                        )
            plot.ylabel = r"$F$ [eV]"
            plot.xlabel = r"Volume [bohr$^3$]"
            plot.title = "Total Free Energy"
            all_plots["with_electrons"] = plot
            plot.legend_outside = True
        plot_no_electrons.ylabel = r"$E_{tot} + F_{phon}(T)$ [eV]"
        plot_no_electrons.legend_outside = True
        plot_no_electrons.xlabel = r"Volume [bohr$^3$]"
        plot_no_electrons.title = (
                "Total free energy without electronic thermal contributions")
        all_plots["no_electrons"] = plot_no_electrons
        return all_plots

    async def _get_f_phonons(self, itemp):
        data = await self.free_energy_data
        ph_thermo_props = data["f_phonons"][itemp]
        return np.array(
                [ph_thermo_prop.f_phonons
                 for ph_thermo_prop in ph_thermo_props])

    async def _get_f_electrons(self, itemp):
        data = await self.free_energy_data
        if self.compute_electronic_contributions:
            return data["f_electrons"][:, itemp]
        return data["gs_energies"]  # same for each temp

    async def _get_f_phonons_negative_idx(self, itemp):
        data = await self.free_energy_data
        thermo_props = data["f_phonons"][itemp]
        return np.array([
                thermo_prop.has_negative_phonons
                for thermo_prop in thermo_props])

    async def _get_f_phonon_plot(self):
        # plot the phonon free energy curves
        if self.only_compute_electronic_contributions:
            return
        self._logger.info("Plotting Phononic Free energy F(V, T)")
        data = await self.free_energy_data
        volumes = data["volumes"]
        temps = data["temperatures"]
        plot = Plot(loglevel=self._loglevel)
        neg_label_printed = False
        # start by zero point motion energy
        thermo_props = data["f_phonons"][0]
        zpm_vs_volume = [t.u0_phonon for t in thermo_props]
        plot.add_curve(
                volumes, zpm_vs_volume,
                label="T=0K", linewidth=2, linestyle="--", color="k",
                marker="s", markerfacecolor="k", markersize=5,
                )
        for itemp, (temp, color) in enumerate(
                zip(temps, self.temperature_colors)):
            all_free_energies = await self._get_f_phonons(itemp)
            negative = await self._get_f_phonons_negative_idx(itemp)
            positive = np.logical_not(negative)
            plot.add_curve(
                    volumes[positive],
                    all_free_energies[positive],
                    linewidth=2,
                    color=color, linestyle="", marker="s",
                    markersize=5, markerfacecolor=color,
                    )
            if any(negative):
                plot.add_curve(
                    volumes[negative],
                    all_free_energies[negative],
                    color=color, linestyle="", marker="x",
                    markersize=5, markerfacecolor=color,
                    linewidth=2,
                    label="Negative freqs" if not neg_label_printed else ""
                    )
                neg_label_printed = True
            plot.add_curve(
                    volumes, all_free_energies,
                    color=color, linestyle="-", linewidth=2,
                    label=f"T={temp}K",
                    )
        plot.ylabel = r"$F$ [eV]"
        plot.xlabel = r"Volume [bohr$^3$]"
        plot.title = "Phonon free energy"
        return plot

    async def _get_f_electron_plot(self):
        # plot electronic free energy curves
        if not self.compute_electronic_contributions:
            return
        self._logger.info("Plotting Electronic Free Energy F(V, T)")
        data = await self.free_energy_data
        temps = data["temperatures"]
        # need to compute corrections
        volumes = data["volumes"]
        plot = Plot(loglevel=self._loglevel)
        plot_inv = Plot(loglevel=self._loglevel)
        plot_diff = Plot(loglevel=self._loglevel)
        plot_diff_inv = Plot(loglevel=self._loglevel)
        if not self.only_compute_electronic_contributions:
            plot.add_curve(
                    volumes, data["gs_internal_energies"],
                    label="T=0K",
                    linewidth=2, linestyle="--",
                    marker="s", markerfacecolor="k", color="k",
                    markersize=5)
        for itemp, (temp, color) in enumerate(
                zip(temps, self.temperature_colors)):
            f_electrons = data["f_electrons"][:, itemp]
            plot.add_curve(
                volumes, f_electrons,
                label=f"T={temp}K",
                color=color, linewidth=2,
                marker="o", markersize=5,
                markerfacecolor=color)
            plot_diff.add_curve(
                volumes, f_electrons - data["f_electrons"][:, 0],
                color=color, linewidth=2,
                marker="o", markersize=5, label=f"T={temp}K",
                markerfacecolor=color)
        where0 = np.where(data["delta_volumes"] == 0)[0][0]
        for ivol, color in enumerate(self.volumes_colors):
            delta = data["delta_volumes"][ivol]
            plot_inv.add_curve(
                temps, data["f_electrons"][ivol, :],
                color=color, label=r"$\delta V=$" + f"{delta}%",
                marker="o", linewidth=2, markerfacecolor=color,
                markersize=5,
                )
            plot_diff_inv.add_curve(
                temps,
                data["f_electrons"][ivol, :] - data["f_electrons"][where0, :],
                color=color, label=r"$\delta V=$" + f"{delta}%",
                marker="o", linewidth=2, markerfacecolor=color,
                markersize=5,
                )
        plot_inv.ylabel = r"$F$ [eV]"
        plot_inv.xlabel = r"$T$ [K]"
        plot_inv.title = "Electron free energy vs temperature"
        plot_diff_inv.ylabel = r"$\Delta F = F(V, T) - F(V_0, T)$ [eV]"
        plot_diff_inv.xlabel = r"$T$ [K]"
        plot_diff_inv.title = "Electron free energy difference vs temperature"
        plot_diff.ylabel = r"$\Delta F = F(V, T) - F(V, T_0)$ [eV]"
        plot_diff.xlabel = r"Volume [bohr$^3$]"
        plot_diff.title = "Electron free energy difference vs volume"
        plot.ylabel = r"$F$ [eV]"
        plot.xlabel = r"Volume [bohr$^3$]"
        plot.title = "Electron Free Energy vs volume"
        plot.legend_outside = True
        plot_diff.legend_outside = True
        plot_diff_inv.legend_outside = True
        plot_inv.legend_outside = True
        return {"vs_V": plot, "vs_T": plot_inv, "vs_V_diff": plot_diff,
                "vs_T_diff": plot_diff_inv,
                }

    async def _get_chemical_potential_plot(self):
        # plot electronic chemical potential curves
        if not self.compute_electronic_contributions:
            return
        self._logger.info("Plotting Chemical potential plots mu(V, T)")
        data = await self.free_energy_data
        temps = data["temperatures"]
        mus = data["chemical_potentials"]
        all_plots = {}
        # we want to plot mu(T) for different V
        plot = Plot(loglevel=self._loglevel)
        plot_delta = Plot(loglevel=self._loglevel)
        for ivol, (delta, color) in enumerate(
                zip(data["delta_volumes"], self.volumes_colors)):
            sign = "+" if delta >= 0 else "-"
            plot.add_curve(
                temps, mus[ivol, :],
                label=r"$V = V_0" + sign + str(abs(delta)) + "$%",
                color=color, linewidth=2,
                marker="o", markersize=5,
                markerfacecolor=color)
            # plot the difference as well
            mu0 = mus[ivol, 0]
            plot_delta.add_curve(
                temps, np.abs(mu0 - mus[ivol, :]),
                label=r"$V = V_0" + sign + str(abs(delta)) + "$%",
                color=color, linewidth=2,
                marker="o", markersize=5,
                markerfacecolor=color)
        plot.ylabel = r"$\mu(T)$ [eV]"
        plot.xlabel = r"Temperature [K]"
        plot.title = "Chemical potential"
        all_plots["mu"] = plot
        plot_delta.ylabel = r"$|\epsilon_F - \mu(T)|$ [eV]"
        plot_delta.xlabel = r"Temperature [K]"
        plot_delta.title = (
                "Chemical potential difference with fermi level")
        all_plots["mu_diff"] = plot_delta
        return all_plots

    async def _get_etot_plot(self):
        # plot U0(V) for both etot and sum of eigs - correction
        """Plot the gs energy curve."""
        if self.only_compute_electronic_contributions:
            return
        self._logger.info("Plotting ETOT^DFT(V)")
        gs_plot = Plot()
        data = await self.free_energy_data
        energies = data["gs_energies"]
        volumes = data["volumes"]
        gs_plot.add_curve(
                volumes, energies, linewidth=2, linestyle="-",
                color="C0", marker="s", markerfacecolor="C0",
                markersize=5, label="DFT Etot",
                )
        gs_plot.add_curve(
                volumes, data["gs_internal_energies"], linewidth=2,
                linestyle="-", color="C2", marker="^", markerfacecolor="C2",
                markersize=5, label="DFT Internal Energy")
        if self.compute_electronic_contributions:
            # also plot lowest temp from electronic calcs
            f_el_lowest = data["f_electrons"][:, 0]
            gs_plot.add_curve(
                    volumes, f_el_lowest, linewidth=2, linestyle="-",
                    color="C1", marker="s", markerfacecolor="C1",
                    markersize=5, label=r"$F_{el}(T->0)$",
                    )
        gs_plot.xlims = [0.99 * min(volumes), 1.01 * max(volumes)]
        gs_plot.add_vline(
                self.v0,
                linestyle="--", linewidth=2)
        gs_plot.xlabel = r"Volume (bohr$^3$)"
        gs_plot.ylabel = "Energy (eV)"
        gs_plot.title = "Ground State Energy vs volume"
        return gs_plot

    @property
    def volumes_colors(self) -> list:
        """An array of colors for volume-dependent curves."""
        # TODO: generalize this method for ibrav != 2
        if self._volumes_colors is not None:
            return self._volumes_colors
        cmap = plt.get_cmap("gist_rainbow", len(self.deltas_volumes))
        colors = [cmap(x / (len(self.deltas_volumes) - 1))
                  for x in range(len(self.deltas_volumes))]
        self._volumes_colors = colors
        return self._volumes_colors

    async def _get_phonon_dispersions_plot(self) -> Plot:
        """Plot the phonon dispersion curves for all volumes.

        Returns
        -------
        The plot containing all the dispersions.
        """
        if self.only_compute_electronic_contributions:
            return
        calcs = self.get_sequence_calculation("matdyn")
        plots = []
        # order deltas by increasing order
        ordering = np.argsort(self.deltas_volumes)
        # TODO: generalize this method for ibrav != 2
        for iindex, index in enumerate(ordering):
            matdyn_calc = calcs[index]
            color = self.volumes_colors[iindex]
            vol = os.path.basename(matdyn_calc.workdir).split("_")[-1]
            plot = await self.plot_phonon_dispersion(
                    calc=matdyn_calc, name_extension=vol,
                    title=f"Phonon dispersion {vol}",
                    show=False,
                    )
            vol = vol.replace("percent", "%")
            plot.plot_objects["curves"][0].label = vol
            for curve in plot.plot_objects["curves"]:
                curve.color = color
            plots.append(plot)
        final = sum(plots[1:], plots[0])
        final.title = "Phonon dispersions vs volumes"
        return final

    def _get_phonon_dispersion_from(self, calc=None):
        if calc is None:
            return QEPhononDispersionSequencer._get_phonon_dispersion_from(
                    self)
        return calc.workdir

    async def _collect_gs_data(
            self, scf_workdir: str) -> dict:
        # self._logger.info(f"Collecting SCF data from: {scf_workdir}")
        data = {}
        async with await CalculationDirectory.from_calculation(
                scf_workdir, loglevel=self._loglevel) as calc:
            log = await calc.log_file
            infile = calc.input_file
            async with log:
                data["etot"] = log.total_energy * RYDBERG_TO_EV
                data["nelectrons"] = log.nelectrons
                data["fermi_energy"] = log.fermi_energy
                data["entropic_energy"] = (
                        log.smearing_contribution * RYDBERG_TO_EV)
                data["internal_energy"] = log.internal_energy * RYDBERG_TO_EV
            async with infile:
                data["volume"] = qe_input_variables_to_unit_cell_volume(
                        infile.input_variables)
        return data

    async def _collect_qpts_weights(self) -> dict[str, np.ndarray]:
        if self.ibrav == 2:
            self._logger.info(
                   f"Collecting Qpts weights from {self.kpoints_workdir}.")
            async with await QEKpointsMeshFile.from_calculation(
                    self.kpoints_workdir,
                    loglevel=self._loglevel) as mesh_file:
                return {"qpts_weights": mesh_file.weights}
        qpt_weights = {"qpts_weights": []}
        for idelta, _ in enumerate(self.deltas_volumes):
            kptdir = self._get_kpoints_workdir_from_idelta(idelta)
            self._logger.info(
                   f"Collecting Qpts weights from {kptdir}.")
            async with await QEKpointsMeshFile.from_calculation(
                    kptdir,
                    loglevel=self._loglevel) as mesh_file:
                qpt_weights["qpts_weights"].append(mesh_file.weights)
        return qpt_weights

    async def _collect_all_scf_data(self) -> tuple[dict, np.ndarray]:
        data = {}
        scf_coros = []
        self._logger.info("Collecting data from regular scf workdirs:")
        for idelta, _ in enumerate(self.deltas_volumes):
            # TODO: generalize this method for ibrav != 2
            # get scf data from regular scf calcs
            scf_workdir = self._get_scf_workdir_single_calculation(idelta)
            self._logger.info(f"- {scf_workdir}")
            scf_coros.append(self._collect_gs_data(scf_workdir))
        scf_data = await asyncio.gather(*scf_coros)
        volumes = np.asarray([d["volume"] for d in scf_data])
        gs_energies = np.asarray([d["etot"] for d in scf_data])
        gs_internal_energies = np.asarray(
                [d["internal_energy"] for d in scf_data])
        nelectrons = np.asarray([d["nelectrons"] for d in scf_data])
        order = np.argsort(volumes)
        data["volumes"] = volumes[order]  # this reset previous data if needed
        data["gs_energies"] = gs_energies[order]
        data["gs_internal_energies"] = gs_internal_energies[order]
        if not np.all(nelectrons == nelectrons[0]):
            raise ValueError(
                    "Some calculations don't have same number of electrons!")
        data["nelectrons"] = nelectrons[0]
        return data, order

    async def _collect_ph_data(self, matdyn_workdir):
        data = {}
        async with await QEMatdynFreqFile.from_calculation(
                matdyn_workdir,
                loglevel=self._loglevel) as freq_file:
            data["frequencies"] = np.asarray(freq_file.eigenvalues) * CM_TO_EV
        if (np.array(freq_file.eigenvalues) < 0.0).any():
            self._logger.warning(
                    f"Phonon frequencies < 0.0 for calc = {matdyn_workdir}%")
            for iqpt, freqs_qpt in enumerate(freq_file.eigenvalues):
                for ibranch, freq in enumerate(freqs_qpt):
                    if freq < 0.0:
                        self._logger.warning(
                                f"w = {freq} < 0.0 at qpt = {iqpt + 1}, "
                                f"branch = {ibranch}")
        return data

    async def _collect_all_ph_data(self, order):
        coros = []
        for idelta, _ in enumerate(self.deltas_volumes):
            # TODO: generalize this method for ibrav != 2
            scf_workdir = self._get_scf_workdir_single_calculation(idelta)
            matdyn_workdir = self._get_matdyngrid_workdir_single_scf(
                    scf_workdir)
            coros.append(self._collect_ph_data(matdyn_workdir))
        self._logger.info("Collecting phonon frequencies.")
        data = await asyncio.gather(*coros)
        freqs = np.array([d["frequencies"] for d in data])[order]
        # shape = [nvols, nqpt, nbranches]
        return {"phonon_frequencies": freqs}

    async def _collect_all_f_el_data(self, order):
        if not self.compute_electronic_contributions:
            return {}
        all_data = {}
        for idelta, delta in enumerate(self.deltas_volumes):
            coros = []
            for itemp, _ in enumerate(self.temperatures):
                # TODO: generalize this method for ibrav != 2
                workdir = self._get_f_el_scf_calc_workdir(idelta, itemp)
                coros.append(self._collect_gs_data(workdir))
            self._logger.info(
                    "Collecting data for electronic contribution "
                    f"to F for delta vol = {delta}%")
            data = await asyncio.gather(*coros)
            all_data.setdefault("f_electrons", [])
            all_data.setdefault("s_electrons", [])
            all_data.setdefault("u_electrons", [])
            all_data.setdefault("chemical_potentials", [])
            all_data["f_electrons"].append([d["etot"] for d in data])
            all_data["s_electrons"].append(
                    [d["entropic_energy"] for d in data])
            all_data["u_electrons"].append(
                    [d["internal_energy"] for d in data])
            all_data["chemical_potentials"].append(
                    [d["fermi_energy"] for d in data])
        all_data["chemical_potentials"] = np.asarray(
                all_data["chemical_potentials"])[order]
        all_data["f_electrons"] = np.asarray(all_data["f_electrons"])[order]
        all_data["s_electrons"] = np.asarray(all_data["s_electrons"])[order]
        all_data["u_electrons"] = np.asarray(all_data["u_electrons"])[order]
        return all_data

    async def _collect_data(self):
        """Collect GS energies, phonon frequencies for all volumes computed."""
        if self.only_compute_electronic_contributions:
            return await self._collect_all_f_el_data(self.deltas_volumes_order)
        data, order = await self._collect_all_scf_data()
        data.update(await self._collect_all_ph_data(order))
        data.update(await self._collect_all_f_el_data(order))
        data.update(await self._collect_qpts_weights())
        return data

    def _get_phonons_thermo_props(
            self,
            temperature: float,
            phonon_frequencies,
            qpts_weights,
            ) -> list[PhononThermodynamicProperties]:
        """Return the free energy vs volume curve for a given temperature.

        Parameters
        ----------
        temperature: float
            Self-explanatory in Kelvins.
        phonon_frequencies:
            The list of all phonon frequencies for each volumes.
        qpts_weights:
            The list of all qpts weights for each volumes.

        Returns
        -------
        list: list of PhononThermodynamicProperties
        """
        thermo_props = []
        for idelta, ph_freq in enumerate(phonon_frequencies):
            if self.ibrav == 2:
                qws = qpts_weights
            else:
                qws = qpts_weights[idelta]
            ph_thermo_props = PhononThermodynamicProperties(
                    energies=ph_freq,
                    temperature=temperature,
                    weights=qws,
                    loglevel=self._loglevel,
                    )
            thermo_props.append(ph_thermo_props)
        return thermo_props

    async def _get_matdyngrid_input_variables(self, q2r_calc):
        """From the q2r calculation, determine the matdyngrid input vars."""
        # since we change volume, qpts coordinates might be different for each
        # calculation. This is why we load them one by one in this method
        inp = {"asr": self.asr,
               "q_in_cryst_coord": True,
               "q_in_band_form": False,
               }
        # now for the qpts list, these are extracted from the 'kpoints' run
        async with await QEKpointsMeshFile.from_calculation(
                self._get_kpoints_workdir_from_q2r_calc(q2r_calc),
                loglevel=self._loglevel) as mesh:
            inp["q_points"] = {
                    "qpts": mesh.kpoints.tolist(),  # make sure no numpy arrays
                    "nqpts": [1] * mesh.nkpoints,
                    }
            # in the meantime, set the weights
            self._phonons_qpoint_weights = mesh.weights
        return inp

    def _get_matdyngrid_workdir_single_scf(self, scf_calc):
        if isinstance(scf_calc, SequenceCalculation):
            workdir = scf_calc.workdir
        else:
            workdir = scf_calc
        ext = self._get_workdir_ext_from_workdir(workdir)
        return self.matdyngrid_workdir + ext

    def _get_matdyn_workdir_single_scf(self, scf_calc):
        if isinstance(scf_calc, SequenceCalculation):
            workdir = scf_calc.workdir
        else:
            workdir = scf_calc
        ext = self._get_workdir_ext_from_workdir(workdir)
        return self.matdyn_workdir + ext

    def _get_q2r_workdir_single_scf(self, scf_calc):
        ext = self._get_workdir_ext_from_workdir(scf_calc.workdir)
        return self.q2r_workdir + ext

    def _get_phonon_workdir(self, iph, scf_calc):
        # override this to include a sub ph directory
        scf_ext = self._get_workdir_ext_from_workdir(scf_calc.workdir)
        # Samething as BaseIndividualPhononSequencer
        base_workdir = os.path.join(self.phonons_workdir + scf_ext, "ph")
        if self._phonons_workdir_zfill is not None:
            return base_workdir + str(iph + 1).zfill(
                    self._phonons_workdir_zfill)
        return base_workdir + str(iph + 1)

    def _get_scf_workdir_single_calculation(
            self, icalc: int,
            basename_only: bool = False) -> str:
        ext = self._get_workdir_ext(icalc)
        if basename_only:
            return os.path.basename(self.scf_workdir) + ext
        tot = self.scf_workdir + ext
        if self.ibrav == 2:
            return tot
        else:
            if self.deltas_volumes[icalc] == 0.0:
                return tot
            return os.path.join(tot, self._post_relaxation_scf_dirname)

    def _get_workdir_ext(self, icalc: int) -> str:
        delta = self.deltas_volumes[icalc]
        if delta >= 0:
            return f"_vol+{delta}percent"
        return f"_vol-{abs(delta)}percent"

    def _get_workdir_ext_from_workdir(self, workdir: str) -> str:
        og_workdir = workdir
        if os.path.basename(workdir) == self._post_relaxation_scf_dirname:
            workdir = os.path.dirname(workdir)
        if not workdir.endswith("percent"):
            raise ValueError(og_workdir)
        return "_" + workdir.split("_")[-1]

    def _get_f_el_scf_calc_workdir_from_scf_calc(
            self, scf_calc: SequenceCalculation, itemp: int) -> str:
        base = os.path.basename(scf_calc.workdir)
        if base == self._post_relaxation_scf_dirname:
            # take base of dirname
            base = os.path.basename(os.path.dirname(scf_calc.workdir))
        temp = round(self.temperatures[itemp], 2)  # hope no conflict arises
        base += f"_{temp}K"
        dirname = os.path.dirname(self.scf_workdir)
        return os.path.join(
            dirname,
            "f_electron_contributions", base)

    def _get_f_el_scf_calc_workdir(self, icalc, itemp):
        base = self._get_scf_workdir_single_calculation(
                icalc, basename_only=True)
        temp = round(self.temperatures[itemp], 2)  # hope no conflict arises
        base += f"_{temp}K"
        dirname = os.path.dirname(self.scf_workdir)
        return os.path.join(
            dirname,
            "f_electron_contributions", base)

    def _get_nscf_workdir(self, icalc):
        ext = self._get_workdir_ext(icalc)
        return self.nscf_workdir + ext

    def _get_band_structure_workdir_from_scf_calc(
            self, scf_calc: SequenceCalculation) -> str:
        ext = self._get_workdir_ext_from_workdir(scf_calc.workdir)
        return self.band_structure_workdir + ext

    def _get_band_structure_workdir(self, icalc):
        ext = self._get_workdir_ext(icalc)
        return self.band_structure_workdir + ext

    def _get_dos_workdir(self, icalc: int) -> str:
        ext = self._get_workdir_ext(icalc)
        return self.dos_workdir + ext

    def _get_matdyndos_workdir(self, icalc: int) -> str:
        ext = self._get_workdir_ext(icalc)
        return self.matdyndos_workdir + ext

    async def post_sequence(self) -> None:
        """Post sequence method called after everything has been completed."""
        await QEPhononComparatorSequencer.post_sequence(self)
        await self.plot_band_structure()
        await self.plot_dosses()
        await self.plot_phonon_dosses()

    async def plot_band_structure(self) -> None:
        """Plot the band structures."""
        if not self.compute_band_structures:
            return
        for calc in self.get_sequence_calculation("band_structure"):
            plot = await self._get_band_structure_plot(calc.workdir)
            ext = calc.workdir.split(self.band_structure_workdir)[-1]
            await self._post_process_plot(
                    plot, name_extension=f"band_structure{ext}"
                    )

    async def plot_dosses(self) -> None:
        """Plot the dosses."""
        if not self.compute_dosses:
            return
        for calc in self.get_sequence_calculation("dos"):
            plot = await self._get_dos_plot(calc.workdir)
            ext = calc.workdir.split(self.dos_workdir)[-1]
            await self._post_process_plot(
                    plot, name_extension=f"dos{ext}"
                    )

    async def plot_phonon_dosses(self) -> None:
        """Plot the phonon dosses."""
        if not self.compute_phonon_dosses:
            return
        raise NotImplementedError

    def _get_relaxation_workdir(
            self, icalc: int, idelta: int, *args, root: str = None,
            **kwargs) -> str:
        # Get the relaxation workdir
        wd = QERelaxationSequencer._get_relaxation_workdir(
                self, icalc, *args, root=root, **kwargs)
        if root is not None:
            return wd
        # should be smth like relax_atoms or relax_cell_N
        base = os.path.basename(wd)
        relax_root = self._get_relaxation_root(idelta)
        return os.path.join(relax_root, base)

    def _get_relaxation_root(self, idelta: int) -> str:
        newdir = self._get_scf_workdir_single_calculation(idelta)
        return os.path.join(os.path.dirname(newdir), "relaxation")

    def _get_relaxation_input_variables(
            self, idelta: int, *args,
            update_with_specific_input_variables: bool = True,
            relax_atoms: bool = False,
            **kwargs) -> str:
        ivars = QERelaxationSequencer._get_relaxation_input_variables(
                self, *args,
                relax_atoms=False,  # don't need to relax atoms only
                **kwargs,
                ).copy()
        # add relaxation variables
        # need to do relaxation with fixed volume
        ivars.update({
            "tprnfor": True, "tstress": True,
            "cell_dofree": "ibrav+shape",
            })
        if update_with_specific_input_variables:
            ivars.update(self.scf_specific_input_variables[idelta])
        return ivars
