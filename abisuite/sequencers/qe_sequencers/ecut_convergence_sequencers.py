from .individual_phonon_sequencers import (
        QEPhononConvergenceSequencer,
        )
from ..ecut_convergence_sequencers import (
        BaseEcutConvergenceSequencer,
        BaseEcutPhononConvergenceSequencer,
        )


class QEEcutConvergenceSequencer(BaseEcutConvergenceSequencer):
    """Convergence sequencer for the ecutwfc parameter."""

    _loggername = "QEEcutConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseEcutConvergenceSequencer.__init__(self, "qe", *args, **kwargs)

    @property
    def ecuts_input_variable_name(self):
        """Define the name for the ecut variable to converge."""
        return "ecutwfc"

    @property
    def ecuts_units(self):
        """Define the ecut variable units."""
        return "Ry"


class QEEcutPhononConvergenceSequencer(
        QEEcutConvergenceSequencer,
        QEPhononConvergenceSequencer,
        BaseEcutPhononConvergenceSequencer,
        ):
    """Converges the ecutwfc SCF parameter vs phonon freqs."""

    _loggername = "QEEcutPhononConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        QEEcutConvergenceSequencer.__init__(self, *args, **kwargs)
        QEPhononConvergenceSequencer.__init__(
                self, *args, **kwargs)
        BaseEcutPhononConvergenceSequencer.__init__(
                self, "qe", *args, **kwargs)
