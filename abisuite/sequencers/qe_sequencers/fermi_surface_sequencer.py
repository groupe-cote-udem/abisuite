from .nscf_sequencers import QEPositiveKpointsNSCFSequencer as NSCFSequencer
from ..fermi_surface_sequencer import BaseFermiSurfaceSequencer


class QEFermiSurfaceSequencer(BaseFermiSurfaceSequencer, NSCFSequencer):
    """A QE nscf sequencer that can plot the Fermi Surface of a material."""

    _all_sequencer_prefixes = ("scf_", "nscf_", "plot_", )
    _loggername = "QEFermiSurfaceSequencer"

    def __init__(self, *args, **kwargs):
        NSCFSequencer.__init__(self, *args, **kwargs)
        BaseFermiSurfaceSequencer.__init__(self, "qe", *args, **kwargs)

    @property
    def nscf_input_variables(self):
        """Define the nscf input variables."""
        return NSCFSequencer.nscf_input_variables.fget(self)

    @nscf_input_variables.setter
    def nscf_input_variables(self, nscf):
        if not isinstance(nscf, dict):
            raise TypeError(
                    "'nscf_input_variables' should be a dict.")
        self._set_input_var_to(nscf, "calculation", "nscf")
        self._set_input_var_to(nscf, "verbosity", "high")
        # self._set_input_var_to(nscf, "nosym", True)
        # these variables are used to update the dict of scf input vars
        scf = self.scf_input_variables.copy()
        scf.update(nscf)
        NSCFSequencer.nscf_input_variables.fset(self, scf)

    async def post_sequence(self):
        """Execute post sequence stuff."""
        await BaseFermiSurfaceSequencer.post_sequence(self)

    def _get_nscf_k_points(self):
        kpts = NSCFSequencer._get_nscf_k_points(self, extra=1)
        kpts["parameter"] = "tpiba"
        return kpts
