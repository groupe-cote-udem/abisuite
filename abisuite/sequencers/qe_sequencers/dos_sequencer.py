
from .nscf_sequencers import QENSCFSequencer
from ..bases import SequenceCalculation
from ..dos_sequencer import DOSSequencer
from ...plotters import Plot
from ...post_processors import DOSPlot


class QEDOSSequencer(DOSSequencer, QENSCFSequencer):
    """Sequencer class for QE dos calculation."""

    # The QE script to compute dos does not do NSCF on a finer grid
    # we need to do manually
    _all_sequencer_prefixes = ("scf_", "nscf_", "dos_", "plot_")

    def __init__(self, *args, **kwargs):
        QENSCFSequencer.__init__(self, *args, **kwargs)
        DOSSequencer.__init__(self, "qe", *args, **kwargs)

    @property
    def nscf_input_variables(self) -> dict:
        """The nscf input variables."""
        if self._nscf_input_variables is not None:
            return self._nscf_input_variables
        ivars = self.scf_input_variables.copy()
        ivars.update(self.dos_fine_kpoint_grid_variables)
        ivars["calculation"] = "nscf"
        self._nscf_input_variables = ivars
        return ivars

    async def init_sequence(self) -> None:
        """Initialize DOS sequence."""
        await QENSCFSequencer.init_sequence(self)
        self.init_dos_sequence()

    def init_dos_sequence(self) -> None:
        """Initialize dos part of sequence."""
        # link agains NSCF calc instead of SCF calc
        nscf_calc = self.get_sequence_calculation("nscf")
        self._init_dos_sequence_against_gs_calc(
                nscf_calc,
                )

    def _init_dos_sequence_against_gs_calc(
            self, calc: SequenceCalculation, *args, **kwargs,
            ) -> SequenceCalculation:
        # don't load geometry as launcher does not support it
        return DOSSequencer._init_dos_sequence_against_gs_calc(
                self, calc, *args,
                calctype="qe_dos",
                link_density=False, load_geometry=False,
                **kwargs,
                )

    async def plot_dos(self) -> Plot:
        """Plot the dos."""
        plot = await self._get_dos_plot(self.dos_workdir)
        self._dos_plot = plot
        await self._post_process_plot(plot, name_extension="dos")
        return plot

    async def _get_dos_plot(self, dos_workdir: str) -> Plot:
        dos = await DOSPlot.from_calculation(
                dos_workdir, loglevel=self._loglevel)
        plot = dos.get_plot(
                vertical=True, line_at_fermi=True,
                xlabel="DOS", xunits="states / eV / cell",
                ylabel="Energy", yunits="eV",
                title=(r"Density of States $E_F=$" +
                       f"{dos.fermi_energy:.2f} eV"),
                linewidth=2, color="k")
        plot.xlims = [0, 1.05 * max(plot.plot_objects["curves"][0].xdata)]
        return plot
