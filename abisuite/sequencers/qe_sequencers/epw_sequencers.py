import abc
from typing import Sequence

from .band_structure_sequencer import QEBandStructureSequencer
from .individual_phonon_sequencers import QEIndividualPhononSequencer
from .nscf_sequencers import QEPositiveKpointsNSCFSequencer
from .phonon_dispersion_sequencer import QEPhononDispersionSequencer
from ..bases import BaseSequencer, SequenceCalculation
from ...constants import mEV_TO_CM
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...post_processors import (
        AdaptativeSmearingHistogram,
        BandStructure, ConductivityTensor,
        EPHRelaxationTimes,
        MLWFDecay,
        PhononDispersion, Resistivity,
        )
from ...routines import is_vector
from ...status_checkers.exceptions import CalculationNotFinishedError


class BaseQEEPWSequencer(
        QEBandStructureSequencer, QEPositiveKpointsNSCFSequencer, abc.ABC,
        ):
    """Base class for a QEEPWSequencer.

    It uses a bandstructure sequencer and a
    positive kpt nscf sequencer.
    """

    def __init__(self, *args, **kwargs):
        """Call both of the mother classes' init methods."""
        self._init_phonons_sequencer(*args, **kwargs)
        QEBandStructureSequencer.__init__(self, *args, **kwargs)
        QEPositiveKpointsNSCFSequencer.__init__(self, *args, **kwargs)
        if "epw_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "'epw_' must be set in '_all_sequencer_prefixes'.")

    async def is_sequence_completed(self) -> bool:
        """Return true if sequence completed."""
        return await QEBandStructureSequencer.is_sequence_completed(self)

    @property
    def epw_input_variables(self):
        """Define the epw input variables."""
        return self._epw_input_variables

    @epw_input_variables.setter
    def epw_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    "Expected a dict for input vars but got: "
                    f"{input_vars}")
        # set kpoint path for wannierization according to values
        # in band structure calculation
        kpts_path = []

        def replace_latex_chars(label, latex_symbol, replace_with):
            if label.startswith("$" + latex_symbol):
                return label.replace(
                        "$" + latex_symbol, replace_with).strip("$")
            return label

        # add kpoint path to input variables. For EPW, they are given through
        # the variables 'wdata(x)' that are passed directly to wannier90
        for i, kpoint in enumerate(self.band_structure_kpoint_path[:-1]):
            label1 = list(kpoint.keys())[0]
            coords1 = kpoint[label1]
            label2 = list(self.band_structure_kpoint_path[i + 1].keys())[0]
            coords2 = self.band_structure_kpoint_path[i + 1][label2]
            for latex_symbol, replacement in zip(
                            (r"\Gamma", r"\Sigma"), ("G", "S")):
                label1 = replace_latex_chars(label1, latex_symbol, replacement)
                label2 = replace_latex_chars(label2, latex_symbol, replacement)
            coords1 = " ".join([str(round(x, 5)) for x in coords1])
            coords2 = " ".join([str(round(x, 5)) for x in coords2])
            kpts_path.append(f"{label1} {coords1} {label2} {coords2}")
        n_wdata = 0
        set_bands_plot = True
        for var, value in input_vars.items():
            if var.startswith("wdata("):
                n_wdata += 1
                wdata = value.lower()
                if "bands_plot" in wdata and "true" in wdata:
                    set_bands_plot = False
        input_vars[f"wdata({n_wdata + 1})"] = "begin kpoint_path"
        for i, kpt in enumerate(kpts_path):
            input_vars[f"wdata({n_wdata + 2 + i})"] = kpt
        input_vars[
                f"wdata({n_wdata + 2 + len(kpts_path)})"] = "end kpoint_path"
        if set_bands_plot:
            input_vars[
                f"wdata({n_wdata + 3 + len(kpts_path)})"] = "bands_plot = True"
        # set the nscf kpoint grid
        for i in range(1, 4):
            if f"nk{i}" in input_vars:
                self._logger.debug("Overwriting 'nkX' to match NSCF "
                                   "calculation.")
            input_vars[f"nk{i}"] = self.nscf_kgrid[i - 1]
        self._epw_input_variables = input_vars

    async def init_sequence(self, *args, **kwargs):
        """Initialize whole sequence."""
        # SCF, Phonons and band structure are set in mother classes
        # first start with SCF + PHONONS
        await self._init_phonons_sequence(*args, **kwargs)
        # Start with Band Structure
        await QEBandStructureSequencer.init_sequence(self, *args, **kwargs)
        # then do NSCF run
        await QEPositiveKpointsNSCFSequencer.init_sequence(
                self, *args, **kwargs)
        await self.init_epw_sequence()

    async def init_epw_sequence(self):
        """Initialize EPW part of the sequence."""
        # the sequencer contains 2 times the same scf calculation but its
        # fine it will just double check it for nothing (no big deal)
        phonons_calc = self.get_sequence_calculation(
                "phonons", always_return_list=True)
        nscfcalc = self.get_sequence_calculation(
                "nscf", always_return_list=True)
        epw_calc = SequenceCalculation(
                "qe_epw", self.epw_workdir, self.epw_input_variables,
                self.epw_calculation_parameters, loglevel=self._loglevel)
        epw_calc.dependencies += nscfcalc + phonons_calc
        self.sequence.append(epw_calc)

    async def plot_band_structure(self):
        """Plot the band structure."""
        # Plot the band structure with the wannier interpolation on top of it.
        # pop the plot parameters to make sure the bs plot is not shown/saved
        show = self.plot_calculation_parameters.pop("show", True)
        self.plot_show = False
        save = self.plot_calculation_parameters.pop("save", None)
        self.plot_save = None
        # same with save pickle
        save_pickle = self.plot_calculation_parameters.pop("save_pickle", None)
        self.plot_save_pickle = None
        # W90 prints it's own path density we need to adjust axis
        # FIXME: This is disabled for now as I think it only works for cubic
        # systems
        self.plot_calculation_parameters["adjust_axis"] = True
        # self.plot_calculation_parameters["adjust_axis"] = False
        dft_plot = await super().plot_band_structure(
                name_extension="band_structure")
        # reset parameters
        self.plot_show = show
        self.plot_save = save
        self.plot_save_pickle = save_pickle
        self._logger.info("Plotting Wannier Interpolation...")

        labels = self._get_band_structure_labels()
        n = self.band_structure_kpoint_path_density
        try:
            wannier_bs = await BandStructure.from_calculation(
                    self._get_epw_band_structure_workdir(),
                    loglevel=self._loglevel)
            adjust = self.plot_calculation_parameters["adjust_axis"]
            if self.epw_input_variables.get("filkf", None) is not None:
                wannier_bs.set_kpath_from_uniform_density(labels, n)
                # last kpt not included usually
                wannier_bs.kpath[-1][1] -= 1
                wannier_plot = wannier_bs.get_plot(
                        yunits="eV",
                        ylabel="Energy",
                        color="r",
                        linewidth=2,
                        adjust_axis=adjust,
                        )
            else:
                wannier_plot = wannier_bs.get_plot(
                        yunits="eV",
                        ylabel="Energy",
                        color="r",
                        linewidth=2,
                        adjust_axis=adjust,
                        )
            final_plot = dft_plot + wannier_plot
        except FileNotFoundError as err:
            self._logger.error("Could not plot Wannierized band structure.")
            self._logger.exception(err)
            final_plot = dft_plot
        # apply other properties to plot
        for param, value in self.plot_calculation_parameters.items():
            if param in ("show", "save"):
                # postfix parameters
                continue
            setattr(final_plot, param, value)
        curves = final_plot.plot_objects["curves"]
        curves[0].label = "dft"
        curves[-1].label = "wannier"
        await self._post_process_plot(
                final_plot,
                name_extension="band_structure_with_wannier_interpolation")
        return final_plot

    @abc.abstractmethod
    async def _init_phonons_sequence(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _init_phonons_sequencer(self, *args, **kwargs):
        pass

    def _get_epw_band_structure_workdir(self):
        return self.epw_workdir


class QEEPWSequencer(BaseQEEPWSequencer, QEIndividualPhononSequencer):
    """Sequencer for an EPW calculation including wannier interpolation.

    Interpolates the band structure only.
    Use this for a one shot epw calculation.
    """

    _all_sequencer_prefixes = (
            "scf_", "phonons_", "nscf_", "band_structure_", "epw_",
            "plot_")
    _loggername = "QEEPWSequencer"

    async def _init_phonons_sequence(self, *args, **kwargs):
        await QEIndividualPhononSequencer.init_sequence(self, *args, **kwargs)

    def _init_phonons_sequencer(self, *args, **kwargs):
        QEIndividualPhononSequencer.__init__(self, *args, **kwargs)


class QEEPWWithPhononInterpolationSequencer(
        BaseQEEPWSequencer, QEPhononDispersionSequencer):
    """Same as QEEPWSequencer but produces phonon dispersion interpolation.

    A matdyn and q2r run are added in order to
    get the phonon dispersion as well as the phonon interpolation of EPW to
    compare.

    The epw part of the sequence makes the wannierization while the
    'epwdispinterpolation' part makes the dispersion interpolations.

    There is an additionnal 'epwelectronicnesting' part which computes
    the electronic nesting function on the band structure kpt path
    (using a custom filqf.txt file). To enable it, set the
    'compute_electronic_nesting' flag to True and set the corresponding
    'epwelectronicnesting_fine_kgrid' interpolation grid.
    """

    _all_sequencer_prefixes = (
            "scf_", "phonons_", "matdyn_", "nscf_", "band_structure_", "epw_",
            "epwdispinterpolation_", 'epwelectronicnesting_',
            "plot_", "q2r_")
    _loggername = "QEEPWWithPhononInterpolationSequencer"

    def __init__(self, *args, **kwargs):
        BaseQEEPWSequencer.__init__(self, *args, **kwargs)
        self._compute_electronic_nesting = False
        self._epwelectronicnesting_fine_kgrid = None

    async def is_sequence_completed(self) -> bool:
        """Return true if sequence is completed."""
        epw_completed = await BaseQEEPWSequencer.is_sequence_completed(self)
        if not epw_completed:
            self._logger.debug(f"EPW sequence not completed for {self}.")
        return epw_completed

    @property
    def compute_electronic_nesting(self) -> bool:
        """True if we compute the electronic nesting."""
        return self._compute_electronic_nesting

    @compute_electronic_nesting.setter
    def compute_electronic_nesting(self, compute) -> None:
        self._compute_electronic_nesting = compute

    @property
    def epwelectronicnesting_fine_kgrid(self) -> Sequence[int]:
        """The fine kgrid used to compute the nesting function."""
        if self._epwelectronicnesting_fine_kgrid is None:
            raise ValueError("Need to set 'epwelectronicnesting_fine_kgrid'.")
        return self._epwelectronicnesting_fine_kgrid

    @epwelectronicnesting_fine_kgrid.setter
    def epwelectronicnesting_fine_kgrid(self, grid: Sequence[int]) -> None:
        if not is_vector(grid, length=3, dtype=int):
            raise ValueError("The grid should be a vector of 3 ints.")
        self._epwelectronicnesting_fine_kgrid = grid

    @BaseQEEPWSequencer.epw_input_variables.setter
    def epw_input_variables(self, invars):
        """Set the epw input variables."""
        BaseQEEPWSequencer.epw_input_variables.fset(self, invars)
        # make sure band_plot is set to True
        self._set_input_var_to(self.epw_input_variables, "band_plot", False)
        self._set_input_var_to(self.epw_input_variables, "elph", True)
        self._set_input_var_to(self.epw_input_variables, "a2f", False)
        self._set_input_var_to(self.epw_input_variables, "phonselfen", False)
        # set fine grid to trivial grids
        for var in ("nqf1", "nqf2", "nqf3", "nkf1", "nkf2", "nkf3"):
            self.epw_input_variables[var] = 1

    async def init_epw_sequence(self, *args, **kwargs):
        """Initialize the EPW part of the sequence."""
        # link the matdyn and band_structure calculation to epw to generate
        # the filqf and filkf files.
        if self._nphonons is None:
            # nphonons not yet determined
            return
        await super().init_epw_sequence(*args, **kwargs)
        epwseq = self.get_sequence_calculation("epw")
        q2rseq = self.get_sequence_calculation("q2r", always_return_list=True)
        # matdynseq = self.get_sequence_calculation("matdyn")
        # bsseq = self.get_sequence_calculation("band_structure")
        epwseq.dependencies += q2rseq       # , matdynseq, bsseq]
        self.init_epwdispinterpolation()
        self.init_epwelectronicnesting()
        # check if PhononDispersion is done. if yes plot phonon dispersion
        # already
        async with await self.get_sequence_calculation(
                "matdyn").calculation_directory as calc:
            if (await calc.status)["calculation_finished"] is True:
                await QEPhononDispersionSequencer.plot_phonon_dispersion(
                        self, name_extension="ph_disp_only")

    def init_epwdispinterpolation(self):
        """Initialize the epwdispinterpolation part of the sequence."""
        seq = SequenceCalculation(
                "qe_epw", self.epwdispinterpolation_workdir,
                {},  # self.epwdispinterpolation_input_variables,
                self.epwdispinterpolation_calculation_parameters,
                loglevel=self._loglevel,
                )
        seq.dependencies += [
                self.get_sequence_calculation("epw"),
                self.get_sequence_calculation("matdyn"),
                self.get_sequence_calculation("q2r"),
                self.get_sequence_calculation("band_structure"),
                ]
        seq.recover_from = self.epw_workdir
        seq.recover_from_update_variables = {
                 "epbwrite": False,
                 "epbread": False,
                 "epwwrite": False,
                 "epwread": True,
                 "wannierize": False,
                 "band_plot": True,
                 "restart": True,
                }
        seq.recover_from_pop_variables = [
                "filkf", "filqf",
                ]
        for var in ("q", "k"):
            for nk in (f"n{var}f1", f"n{var}f2", f"n{var}f3"):
                seq.recover_from_pop_variables.append(nk)
        self.sequence.append(seq)

    def init_epwelectronicnesting(self):
        """Initialize the epwelectronicnesting part of the sequence."""
        if not self.compute_electronic_nesting:
            return
        seq = SequenceCalculation(
                "qe_epw", self.epwelectronicnesting_workdir,
                {},  # self.epwdispinterpolation_input_variables,
                self.epwelectronicnesting_calculation_parameters,
                loglevel=self._loglevel,
                )
        seq.dependencies += [
                self.get_sequence_calculation("epw"),
                self.get_sequence_calculation("band_structure"),
                ]
        seq.recover_from = self.epw_workdir
        seq.recover_from_update_variables = {
                 "epbwrite": False,
                 "epbread": False,
                 "epwwrite": False,
                 "epwread": True,
                 "wannierize": False,
                 "band_plot": True,
                 "restart": True,
                 "elph": False,
                 "nest_fn": True,
                 "nkf1": self.epwelectronicnesting_fine_kgrid[0],
                 "nkf2": self.epwelectronicnesting_fine_kgrid[1],
                 "nkf3": self.epwelectronicnesting_fine_kgrid[2],
                }
        seq.recover_from_pop_variables = [
                "filkf", "filqf",
                ]
        for var in ("q", ):
            for nk in (f"n{var}f1", f"n{var}f2", f"n{var}f3"):
                seq.recover_from_pop_variables.append(nk)
        self.sequence.append(seq)

    async def plot_phonon_dispersion(self):
        """Plot the phonon dispersion with interpolation."""
        # Plot the phonon dispersion with the wannier interpolation on
        # top of it.
        # pop the plot parameters to make sure the bs plot is not shown/saved
        show = self.plot_calculation_parameters.pop("show", True)
        self.plot_show = False
        save = self.plot_calculation_parameters.pop("save", None)
        self.plot_save = None
        save_pickle = self.plot_calculation_parameters.pop("save_pickle", None)
        self.plot_save_pickle = None
        # EPW prints phonon wavevectors in cartesian coordinates
        # don't shrink/expand qpts axis
        self.plot_calculation_parameters["adjust_axis"] = False
        phdisp_plot = await super().plot_phonon_dispersion()
        # reset parameters
        self.plot_show = show
        self.plot_save = save
        self.plot_save_pickle = save_pickle
        # # nevermind the rest for now
        try:
            wannier_phdisp = await PhononDispersion.from_calculation(
                    self.epwdispinterpolation_workdir, loglevel=self._loglevel)
            # EPW usually prints eig freqs in meV. Transfer to CM-1
            wannier_phdisp.frequencies *= mEV_TO_CM
            wannier_plot = wannier_phdisp.get_plot(
                    yunits=r"cm$^{-1}$",
                    ylabel="Energy",
                    color="r",
                    linewidth=2,
                    adjust_axis=self.plot_calculation_parameters["adjust_axis"]
                    )
            final_plot = phdisp_plot + wannier_plot
            final_plot.plot_objects["curves"][-1].label = "wannier"
        except FileNotFoundError as err:
            self._logger.error("Could not plot wannierized phonon dispersion")
            self._logger.exception(err)
            final_plot = phdisp_plot
        final_plot.plot_objects["curves"][0].label = "dft"
        final_plot = await self._post_process_plot(
                final_plot,
                name_extension="phonon_dispersion_with_wannier_interpolation"
                )
        return final_plot

    async def _init_phonons_sequence(self, *args, **kwargs):
        await QEPhononDispersionSequencer.init_sequence(self, *args, **kwargs)

    def _init_phonons_sequencer(self, *args, **kwargs):
        QEPhononDispersionSequencer.__init__(self, *args, **kwargs)

    async def post_sequence(self, *args, **kwargs):
        """Post sequence procedures."""
        await super().post_sequence(*args, **kwargs)
        await self.plot_hamiltonian_decay()

    async def plot_hamiltonian_decay(self):
        """Plots the hamiltonian decay."""
        decays = await MLWFDecay.from_calculation(
                self.epw_workdir, loglevel=self._loglevel)
        for decay_type, decay in decays.items():
            plot = decay.create_plot()
            await self._post_process_plot(
                    plot, name_extension=f"decay_{decay_type}")


class _BasePostQEEPWWannierizationSequencer(BaseSequencer, abc.ABC):
    """Base class for post EPW wannierization sequencers.

    This is a base class and is not meant to be directly instanciated.
    """

    _mandatory_input_variables = None
    _input_variables_should_be_there = None
    _post_qe_epw_prefix = None

    def __init__(self, *args, **kwargs):
        super().__init__("qe", *args, **kwargs)
        self._epw_wannierization_calculation = None
        if self._post_qe_epw_prefix is None:
            raise DevError("Need to set '_post_qe_epw_prefix'.")
        if not self._post_qe_epw_prefix.endswith("_"):
            raise DevError("Need '_post_qe_epw_prefix' to end with '_'.")
        if self._mandatory_input_variables is None:
            raise DevError("Need to set '_mandatory_input_variables'.")
        if self._input_variables_should_be_there is None:
            raise DevError("Need to set '_input_variables_should_be_there'.")

    @property
    def epw_wannierization_calculation(self):
        """The epw calculation that computed the wannierization."""
        if self._epw_wannierization_calculation is None:
            raise ValueError("Needs to set 'epw_wannierization_calculation'.")
        return self._epw_wannierization_calculation

    async def set_epw_wannierization_calculation(self, epw):
        async with await CalculationDirectory.from_calculation(
                epw, loglevel=self._loglevel) as calc:
            if (await calc.status)[
                    "calculation_finished"] is not True and not (
                    self.bypass_convergence_check):
                raise CalculationNotFinishedError(epw)
            self._epw_wannierization_calculation = calc.path

    async def init_sequence(self, *args, **kwargs):
        await super().init_sequence(*args, **kwargs)
        self.init_post_qe_epw_wannierization_sequence()

    def init_post_qe_epw_wannierization_sequence(self, *args, **kwargs):
        postseq = SequenceCalculation(
                "qe_epw",
                getattr(self, f"{self._post_qe_epw_prefix}workdir"),
                getattr(self, f"{self._post_qe_epw_prefix}input_variables"),
                getattr(
                    self, f"{self._post_qe_epw_prefix}calculation_parameters"),
                loglevel=self._loglevel)
        postseq.recover_from = self.epw_wannierization_calculation
        postseq.recover_from_update_variables = {
                 "epbwrite": False,
                 "epbread": False,
                 "epwwrite": False,
                 "epwread": True,
                 "wannierize": False,
                }
        postseq.recover_from_pop_variables = ["filqf", "filkf", "band_plot"]
        for var in ("q", "k"):
            if postseq.input_variables.get(f"rand_{var}", False) is True:
                for nk in (f"n{var}f1", f"n{var}f2", f"n{var}f3"):
                    postseq.recover_from_pop_variables.append(nk)
        self.sequence.append(postseq)

    async def post_sequence(self, *args, **kwargs):
        await super().post_sequence(*args, **kwargs)
        await self.plot_post_epw_wannierization()

    def _set_mandatory_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    "Expected a dict for input vars but got: "
                    f"{input_vars}")
        for varname, varvalue in self._mandatory_input_variables.items():
            self._set_input_var_to(input_vars, varname, varvalue)
        # make sure some input vars are set
        for varname in self._input_variables_should_be_there:
            if varname not in input_vars:
                raise ValueError(
                        f"'{varname}' should be in the input variables.")


class QEEPWDispersionInterpolationSequencer(
        _BasePostQEEPWWannierizationSequencer):
    """Sequencer for a simple epw interpolation following a wannierization.

    Used for dispersion plots (phonon dispersions and band structure), in order
    to confirm that wannierization was correct.
    """

    _all_sequencer_prefixes = ("interpolation_", "plot_", )
    _post_qe_epw_prefix = "interpolation_"
    _mandatory_input_variables = {
            "restart": True,
            "wannierize": False,
            }
    _input_variables_should_be_there = tuple()
    _loggername = "QEEPWInterpolationSequencer"

    @property
    def interpolation_input_variables(self):
        """Define the interpolation input variables."""
        return self._interpolation_input_variables

    @interpolation_input_variables.setter
    def interpolation_input_variables(self, input_vars):
        self._set_mandatory_input_variables(input_vars)
        self._interpolation_input_variables = input_vars

    async def plot_post_epw_wannierization(self):
        """Make dispersion plot."""
        print("TODO: Dispersion PLOTS")


class _BaseTransportPostQEEPWWannierizationSequencer(
        _BasePostQEEPWWannierizationSequencer, abc.ABC):
    """Base class for transport post wannierization sequencers."""

    async def plot_post_epw_wannierization(self, *args, **kwargs):
        """Plot post epw wannierization plots."""
        await self.plot_transport(*args, **kwargs)

    @abc.abstractmethod
    async def plot_transport(self, *args, **kwargs):
        pass


class QEEPWIBTESequencer(_BaseTransportPostQEEPWWannierizationSequencer):
    """Sequencer for an IBTE calculation.

    Simple sequencer that requires to be launched after a full QEEPW
    sequencer has completed the interpolation.

    This sequencer is not a continuation of the epw sequencer since normally
    we want to check the interpolation before continuing.
    """

    _all_sequencer_prefixes = ("ibte_", "plot_", )
    _post_qe_epw_prefix = "ibte_"
    _mandatory_input_variables = {
                "a2f": False,
                "int_mob": True,
                "iterative_bte": True,
                "scattering": True,
                "scattering_serta": True,
                "carrier": False,
                "phonselfen": False,
                "elph": True,
                "etf_mem": 0,
                "mp_mesh_k": True,
                "ephwrite": False,
                "restart": True,
                }
    _input_variables_should_be_there = (
                "assume_metal", "restart_step", "etf_mem", "degaussw",
                "temps", "nstemp", "vme",
                "nkf1", "nkf2", "nkf3", "nqf1", "nqf2", "nqf3", "fsthick",
                "ngaussw",
                )
    _loggername = "QEIBTESequencer"

    @property
    def ibte_input_variables(self):
        """Define the IBTE input variables."""
        return self._ibte_input_variables

    @ibte_input_variables.setter
    def ibte_input_variables(self, input_vars):
        self._set_mandatory_input_variables(input_vars)
        self._ibte_input_variables = input_vars

    async def plot_post_epw_wannierization(self, *args, **kwargs):
        """Make the post epw wannierization plots."""
        await super().plot_post_epw_wannierization(*args, **kwargs)
        await self.plot_relaxation_times()
        await self.plot_adaptative_smearing_histogram()

    async def plot_relaxation_times(self):
        """Make the 1/tau scatter plot."""
        taus = await EPHRelaxationTimes.from_calculation(
                self.ibte_workdir, loglevel=self._loglevel)
        for plotname, tau in taus.items():
            # taus is a dict of plots (1 plot per temperature)
            plots = tau.get_plot()
            await self._post_process_plot(plots, name_extension=plotname)

    async def plot_adaptative_smearing_histogram(self) -> None:
        """Plot adaptative smearing histogram if relevent."""
        if "degaussw" not in self.ibte_input_variables:
            raise RuntimeError("'degaussw' not in ibte_input_variables...")
        degaussw = self.ibte_input_variables.get("degaussw")
        if degaussw != 0.0:
            return  # fixed smearing
        hist = await AdaptativeSmearingHistogram.from_calculation(
                self.ibte_workdir, loglevel=self._loglevel)
        try:
            plot = hist.create_plot()
        except Exception as err:
            self._logger.exception(err)
            self._logger.error(
                    "An error occured while creating the adaptative "
                    "smearing histogram."
                    )
            return
        await self._post_process_plot(
                plot,
                name_extension="adaptative_smearing_histogram",
                )

    async def plot_transport(self):
        """Make the transport plots."""
        # make multiple plots. one for resistivity, conductivity, with serta
        # on both as well and each component on each
        # get each component to plot, by default it's all of them
        elements = self.plot_calculation_parameters.get("elements", None)
        async with await CalculationDirectory.from_calculation(
                self.ibte_workdir, loglevel=self._loglevel) as calc:
            conductivity_tensor = await ConductivityTensor.from_handler(
                    await calc.log_file)
        for serta in [True, False]:
            for resistivity, factor, yunits, title, name in zip(
                    [True, False],
                    [1e6, 1e-6],
                    [r"$\mu\Omega$cm", r"$[\mu\Omega$cm]$^{-1}$"],
                    ["Resistivity tensor", "Conductivty tensor"],
                    ["resistivity", "conductivity"]):
                if serta:
                    title += " (SERTA)"
                    name += "_serta"
                else:
                    title += " (IBTE)"
                    name += "_ibte"
                plot = conductivity_tensor.get_plot(
                        resistivity=resistivity, conversion_factor=factor,
                        xunits="K", yunits=yunits, linestyle="-",
                        linewidth=2, elements=elements, serta=serta,
                        title=title,
                        )
                await self._post_process_plot(plot, name_extension=name)


class QEEPWZimanSequencer(_BaseTransportPostQEEPWWannierizationSequencer):
    """Sequencer to make a ziman calculation.

    Simple sequencer that requires to be launched after a full QEEPW
    sequencer has completed the interpolation. Used for Ziman resistivity
    calculations.

    This sequencer is not a continuation of the epw sequencer since normally
    we want to check the interpolation before continuing.
    """

    _all_sequencer_prefixes = ("ziman_", "plot_", )
    _post_qe_epw_prefix = "ziman_"
    _loggername = "QEZimanSequencer"
    _mandatory_input_variables = {
                "a2f": True,
                "phonselfen": True,
                }
    _input_variables_should_be_there = (
                "degaussw", "ngaussw",
                "vme",
                "fsthick",
                )

    @property
    def ziman_input_variables(self):
        """Define the ziman input variables."""
        return self._ziman_input_variables

    @ziman_input_variables.setter
    def ziman_input_variables(self, input_vars):
        self._set_mandatory_input_variables(input_vars)
        self._ziman_input_variables = input_vars

    async def plot_transport(self):
        """Make the transport plots."""
        resistivity = await Resistivity.from_calculation(
                self.ziman_workdir, loglevel=self._loglevel)
        plot = resistivity.get_plot()
        await self._post_process_plot(plot, name_extension="ziman_resistivity")
