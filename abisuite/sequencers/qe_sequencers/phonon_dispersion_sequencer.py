from .individual_phonon_sequencers import QEIndividualPhononSequencer
from ..bases import SequenceCalculation
from ..phonon_dispersion_sequencer import BasePhononDispersionSequencer
from ...routines import is_dict


class QEPhononDispersionSequencer(
        BasePhononDispersionSequencer, QEIndividualPhononSequencer):
    """A sequencer that can launch phonon calculations one qpt at a time.

    Also computes the phonon dispersion.
    """

    _all_sequencer_prefixes = ("scf_", "phonons_", "q2r_", "matdyn_", "plot_")
    _loggername = "QEPhononDispersionSequencer"

    def __init__(self, *args, **kwargs):
        QEIndividualPhononSequencer.__init__(self, *args, **kwargs)
        BasePhononDispersionSequencer.__init__(
                self, QEIndividualPhononSequencer, *args, **kwargs)
        self._asr = None

    @property
    def asr(self):
        """Define the acoustic sum rule to impose on phonon frequencies."""
        if self._asr is None:
            raise ValueError("Need to set 'asr'.")
        return self._asr

    @asr.setter
    def asr(self, asr):
        self._asr = asr

    @property
    def matdyn_input_variables(self):
        """Define the matdyn input variables."""
        return self._matdyn_input_variables

    @matdyn_input_variables.setter
    def matdyn_input_variables(self, input_vars):
        if not is_dict(input_vars):
            raise TypeError(
                    f"Expected dict for input vars but got: {input_vars}")
        qpt_path = [list(q.values())[0] for q in self.qpoint_path]
        input_vars["q_points"] = {
                "qpts": qpt_path,
                "nqpts": [self.qpoint_path_density] * len(qpt_path),
                }
        self._set_input_var_to(
                input_vars, "q_in_band_form", True)
        self._set_input_var_to(
                input_vars, "q_in_cryst_coord", True)
        self._set_input_var_to(
                input_vars, "asr", self.asr)
        self._matdyn_input_variables = input_vars

    @property
    def matdyn_qpoint_path(self):
        """Define the phonon dispersion qpoint path."""
        return self.qpoint_path

    @matdyn_qpoint_path.setter
    def matdyn_qpoint_path(self, qptpath):
        self.qpoint_path = qptpath

    @property
    def matdyn_qpoint_path_density(self):
        """Define the phonon dispersion qpoint path density."""
        return self.qpoint_path_density

    @matdyn_qpoint_path_density.setter
    def matdyn_qpoint_path_density(self, den):
        self.qpoint_path_density = den

    @property
    def nphonons(self):
        """Define the number of phonon qpoints."""
        return QEIndividualPhononSequencer.nphonons.fget(self)

    @nphonons.setter
    def nphonons(self, *args):
        raise AttributeError("Cannot set directly 'nphonons'.")

    @property
    def phonons_input_variables(self):
        """Define the phonon input variables."""
        return QEIndividualPhononSequencer.phonons_input_variables.fget(self)

    @phonons_input_variables.setter
    def phonons_input_variables(self, invars):
        QEIndividualPhononSequencer.phonons_input_variables.fset(self, invars)
        if not isinstance(invars, dict):
            raise TypeError("'phonons_input_variabels' must be a dict.")
        self._set_input_var_to(
                self.phonons_input_variables, "ldisp", True)
        for inq, nq in enumerate(self.phonons_qpoint_grid):
            self._set_input_var_to(
                    self.phonons_input_variables, f"nq{inq + 1}", nq)

    @property
    def q2r_input_variables(self):
        """Define the q2r input variables."""
        return self._q2r_input_variables

    @q2r_input_variables.setter
    def q2r_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected dict for input vars but got: {input_vars}")
        self._q2r_input_variables = input_vars

    async def is_sequence_completed(self):
        """Return True if the sequence has successfully completed."""
        # override this property since we add calculations after some of them
        # are finished (like the QERelaxationSequencer).
        # check if a matdyn calculation is present. if yes, return super value
        if not self.sequence:
            # FIXME: code not dry with BaseSequencer class...
            try:
                await self.init_sequence()
            except (TypeError, ValueError, KeyError, RuntimeError):
                # some variables are not set yet
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        try:
            self.get_sequence_calculation("matdyn_")
        except (ValueError, LookupError, RuntimeError):
            # matdyn not present in sequence
            return False
        return await QEIndividualPhononSequencer.is_sequence_completed(self)

    async def init_sequence(self):
        """Initialize the whole sequence."""
        # first, try to determine number of phonons. For this, start by doing
        # the normal sequence setting. if nphonons is set, continue otherwise
        # stop
        # init sequence from phonon dispersion sequencer
        await BasePhononDispersionSequencer.init_sequence(self)
        if self._nphonons is None:
            return
        self.init_q2r_sequence()
        self.init_matdyn_sequence()

    async def init_phonons_sequence(self, *args, **kwargs):
        """Initialize the phonon part of the sequence."""
        # we need to determine total number of phonons qpts to run.
        # for this, start the first qpoint.
        scf_calc = self.get_sequence_calculation("scf_")
        await self.init_phonons_qpoint(0, scf_calc)
        first_ph = self.sequence[1]
        async with await first_ph.calculation_directory as calc:
            if not await calc.exists:
                # calc not written yet. wait for it to be ran
                return
            if (await calc.status)["calculation_finished"] is not True:
                # wait for it to finish
                return
            # if finished, get number of phonons
            async with await calc.log_file as log:
                self._nphonons = log.nphonons
        # set the other phonons
        for iph in range(1, self._nphonons):
            await self.init_phonons_qpoint(iph, scf_calc)

    def init_q2r_sequence(self):
        """Initialize the q2r part of the sequence."""
        q2r_run = SequenceCalculation(
                    "qe_q2r", self.q2r_workdir,
                    self.q2r_input_variables,
                    self.q2r_calculation_parameters,
                    loglevel=self._loglevel)
        # dependencies of q2r calculation are all the phonon calculations
        for calc in self.sequence:
            if calc.calctype == "qe_ph":
                q2r_run.dependencies.append(calc)
        self.sequence.append(q2r_run)

    def init_matdyn_sequence(self):
        """Initialize the matdyn part of the sequence."""
        q2r_run = self.get_sequence_calculation("q2r_")
        matdyn_run = SequenceCalculation(
                        "qe_matdyn", self.matdyn_workdir,
                        self.matdyn_input_variables,
                        self.matdyn_calculation_parameters,
                        loglevel=self._loglevel)
        matdyn_run.dependencies.append(q2r_run)
        self.sequence.append(matdyn_run)

    async def launch(self, *args, **kwargs):
        """Launch the sequence."""
        await BasePhononDispersionSequencer.launch(self, *args, **kwargs)

    def _get_phonon_dispersion_from(self):
        return self.matdyn_workdir


class QEPhononDispersionQgridSequencer(QEPhononDispersionSequencer):
    """Sequencer for the qgrid convergence of a phonon dispersion."""

    _all_sequencer_prefixes = ("scf_", "phonons_", "q2r_", "matdyn_", "plot_")
    _loggername = "QEPhononDispersionKgridSequencer"
