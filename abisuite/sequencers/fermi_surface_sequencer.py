import asyncio

from async_property import async_property

from .nscf_sequencers import PositiveKpointsNSCFSequencer
from ..physics.symmetries import PointGroup
from ..post_processors import FermiSurface
from ..routines import (
        is_list_like,
        is_scalar,
        suppress_warnings,
        )


class BaseFermiSurfaceSequencer(PositiveKpointsNSCFSequencer):
    """Base class for fermi surface sequencers."""

    _all_sequencer_prefixes = ("scf_", "nscf_", "plot_", )

    def __init__(self, *args, **kwargs):
        PositiveKpointsNSCFSequencer.__init__(self, *args, **kwargs)
        self._fermi_surface_k_z = None
        self._point_group = None

    # TODO: put the point group detection into a more specific place
    # not hidden inside this peculiar class (unrelated lol)
    # FG: 2021/11/22
    @async_property
    async def point_group(self):
        """Set the point group name for the material."""
        if self._point_group is not None:
            return self._point_group
        self._point_group = await PointGroup.from_calculation(
                self.scf_workdir, loglevel=self._loglevel)
        return self._point_group

    @property
    def fermi_surface_k_z(self):
        """Define the fermi surface k_z level to plot."""
        return self._fermi_surface_k_z

    @fermi_surface_k_z.setter
    def fermi_surface_k_z(self, k_z):
        list_kzs = k_z
        if not is_list_like(k_z):
            list_kzs = (k_z, )
        for k_z in list_kzs:
            if not is_scalar(k_z):
                raise TypeError(k_z)
        self._fermi_surface_k_z = list_kzs

    async def post_sequence(self):
        """Execute post sequence stuff."""
        await self.plot_fermi_surface()

    @suppress_warnings
    async def plot_fermi_surface(self):
        """Use log file eigenvalues to plot fermi surface."""
        nscf_calc = self.get_sequence_calculation("nscf")
        fs = await FermiSurface.from_calculation(
                nscf_calc.workdir, loglevel=self._loglevel)
        fs.point_group = await self.point_group
        if self.fermi_surface_k_z is not None:
            # create one plot for each k_z asked.
            await asyncio.gather(
                    *(self._create_fs_plot_for_k_z(
                        fs, fermi_surface_k_z)
                        for fermi_surface_k_z in self.fermi_surface_k_z))
        # also make all fermi sheets plots
        plots = fs.create_plot()
        await asyncio.gather(
                *(self._post_process_plot(
                    plot, name_extension=f"isheet{isheet}")
                    for isheet, plot in enumerate(plots))
                )
        if len(plots) > 1:
            all_plots = sum(plots[1:], plots[0])
            await self._post_process_plot(
                    all_plots, name_extension="all_fermi_sheets")

    async def _create_fs_plot_for_k_z(self, fs, fermi_surface_k_z):
        plot = fs.create_plot(
                fermi_surface_k_z=fermi_surface_k_z,
                )
        await self._post_process_plot(
                plot,  # use_show=True,
                name_extension=f"k_z={fermi_surface_k_z}")
