import abc
import logging
import os
from typing import Any, Mapping, Sequence

import aiofiles.os

from async_property import async_property

from .exceptions import DifferentSequenceError
from ..bases import BaseUtility
from ..colors import Colors
from ..exceptions import DevError
from ..handlers import (
        CALCTYPES_TO_INPUT_FILE_CLS, CalculationDirectory,
        is_calculation_directory,
        )
from ..launchers import __ALL_LAUNCHERS__
from ..launchers.exceptions import TooManyCalculationsInQueueError
from ..plotters import MultiPlot, Plot
from ..routines import (
        expand_symlink, expand_symlinks, full_abspath,
        is_dict, is_list_like)
from ..status_checkers import (
        CalculationFailedError,
        CalculationNotConvergedError, CalculationNotFinishedError,
        __ERROR_STRING__,
        )


# list of input variables that are exceptions to differences between
# an old and a new calculation (e.g.: variables that do not influence
# the outcome of a calculation like 'prefix' in qe_pw and can change
# from a old to new calculation because we symlinked a calculation for
# some reason)
# TODO: move this somewhere else (I think it's weird to be there) FG:2021/05/12
__EXCEPTION_VARIABLES__ = {
        "abinit": [
            "autoparal", "chksymtnons", "dilatmx", "fftgw", "gwmem", "gwpara",
            "indata_prefix", "iomode", "ionmov",
            "istwfk",
            "nline", "nnsclo", "npband", "npfft", "npkpt", "nstep", "prtden",
            "prtgsr", "prtdos", "prtdosm", "prtprocar", "outdata_prefix",
            "tmpdata_prefix",
            ],
        "abinit_optic": [
            "ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile",
            ],
        "qe_epw": [
            "dis_win_max",
            "dvscf_dir", "epbwrite", "epbread",
            "etf_mem", "filkf", "filqf", "outdir", "prefix",
            "selecqread",
            "title",
            "mp_mesh_k", "num_iter", "wdata(*)", "restart_step",
            # fermi level stuff can change the results tho
            "efermi_read", "fermi_energy",
            # temps stuff can also change results tho
            "temps", "nstemp",
            ],
        "qe_matdyn": [
            "fleig", "flfrc", "flfrq", "flvec",
            ],
        "qe_ph": [
            "alpha_mix(*)", "amass(*)", "fildyn", "prefix", "outdir",
            "recover", "reduce_io",
            "title", "verbosity",
            ],
        "qe_pw": [
            "diago_cg_maxiter", "diagonalization",
            "diago_thr_init",
            "disk_io",
            "electron_maxstep", "etot_conv_thr", "forc_conv_thr",
            "press_conv_thr",
            "mixing_beta", "mixing_ndim", "prefix", "outdir",
            "pseudo_dir", "tprnfor", "tstress",
            ],
        "qe_q2r": [
            "flfrc", "fildyn",
            ],
        }


class SequenceCalculation(BaseUtility):
    """Class that represents a calculation to do for a Sequencer object."""

    _default_calcdir_loglevel = logging.ERROR
    _loggername = "SequenceCalculation"

    def __init__(
            self,
            calctype: str, workdir: str, input_variables: Mapping[str, Any],
            calculation_parameters: Mapping[str, Any], *args,
            bypass_sequence_comparison: bool = False,
            bypass_convergence_check: bool = False,
            bypass_error_check: bool = False,
            **kwargs):
        """Init method.

        Parameters
        ----------
        calctype: str
            The calculation calctype.
        workdir: str
            The calculation workdir.
        input_variables: dict
            The input variables dict.
        calculation_parameters: dict
            The calculation parameters dict.
        bypass_convergence_check: bool, optional
            If True, the sequence will continue if a calculation is
            finished but not converged. If False, an error is raised
            if a calculation in the sequence is not converged.
        bypass_sequence_calculation: bool, optional
            If True, the sequence comparison is skipped. In other words,
            if a calculation exists at the same workdir, it is assumed it
            is the same calculation without comparing input variables
            and dependencies.
        bypass_error_check: bool, optional
            If True and a calculation has errored, just continue.
            Otherwise an error is raised.
        """
        super().__init__(*args, **kwargs)
        self.workdir = full_abspath(workdir)
        self._bypass_sequence_comparison = bypass_sequence_comparison
        self._bypass_convergence_check = bypass_convergence_check
        self._bypass_error_check = bypass_error_check
        self.dependencies = Sequence(
                bypass_convergence_check=bypass_convergence_check,
                bypass_sequence_comparison=bypass_sequence_comparison,
                loglevel=self._loglevel)
        self.calctype = calctype
        self.input_variables = input_variables
        self.load_geometry_from = None
        self.recover_from = None
        self.recover_from_update_variables = None
        self.recover_from_pop_variables = None
        self.recover_from_other_kwargs = {}
        # introducing the following property to keep some variables after
        # loading geometry since this is done after applying input variables
        # sometimes we want some geometry variables to be kept (like acell
        # in thermal expansion sequencers).
        self.keep_variables_after_loading_geometry = None
        self.load_kpts_from = None
        # for abinit only
        self.link_bs_reso_from = None
        self.link_den_from = None
        self.link_scr_from = None
        self.link_wfk_from = None
        self.load_ddk_from = None
        self.calculation_parameters = calculation_parameters
        self.jobname = os.path.basename(self.workdir)
        if "jobname" in self.calculation_parameters:
            self.jobname = self.calculation_parameters.pop("jobname")
        else:
            self._logger.debug(
                f"Taking default jobname as: '{self.jobname}' for '{workdir}'")
        self._launcher = None
        self._is_ready_to_launch = None
        self._calculation_directory = None
        self._logger.debug(
                f"Created SequenceCalculation: {self}.")

    def __eq__(self, other):
        """Define how we can equalize 2 calculations."""
        if self.workdir != other.workdir:
            return False
        for dependency in self.dependencies:
            if dependency not in other.dependencies:
                return False
        for dependency in other.dependencies:
            if dependency not in self.dependencies:
                return False
        if self.calctype != other.calctype:
            return False
        if self.input_variables != other.input_variables:
            return False
        if self.load_geometry_from != other.load_geometry_from:
            return False
        if self.calculation_parameters != other.calculation_parameters:
            return False
        return True

    def __repr__(self):
        """Return a string representation of the calculation sequence."""
        rel = os.path.relpath(self.workdir, os.getcwd())
        return f"< class SequenceCalculation: {rel} >"

    @property
    def bypass_sequence_comparison(self):
        """Set to True to bypass sequence comparison."""
        return self._bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self._bypass_sequence_comparison = bypass
        self.dependencies.bypass_sequence_comparison = bypass

    @property
    def bypass_convergence_check(self):
        """Set to True to bypass convergence check."""
        return self._bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self._bypass_convergence_check = bypass
        self.dependencies.bypass_convergence_check = bypass

    @property
    def bypass_error_check(self) -> bool:
        """Set to true to bypass error check."""
        return self._bypass_error_check

    @bypass_error_check.setter
    def bypass_error_check(self, bypass: bool) -> None:
        self._bypass_error_check = bypass

    @property
    def link_density_from(self):
        """Define the calculation to link the density from."""
        return self.link_den_from

    @link_density_from.setter
    def link_density_from(self, link):
        self.link_den_from = link

    @async_property
    async def calculation_directory(self):
        """Return the CalculationDirectory object associated."""
        if self._calculation_directory is not None:
            return self._calculation_directory
        loglevel = self._default_calcdir_loglevel
        if self._loglevel <= logging.DEBUG:
            loglevel = self._loglevel
        if await is_calculation_directory(
                self.workdir, loglevel=loglevel):
            async with await CalculationDirectory.from_calculation(
                    self.workdir, loglevel=self._loglevel) as calc:
                self._calculation_directory = calc
                if await calc.calctype != self.calctype:
                    raise TypeError(
                            f"A different calculation already exists here:"
                            f" '{self}', '{self.calctype}'")
                return calc
        # else, just create normal calc dir
        self._calculation_directory = CalculationDirectory(
                CALCTYPES_TO_INPUT_FILE_CLS[self.calctype],
                loglevel=loglevel)
        self._calculation_directory.set_calctype(self.calctype)
        await self._calculation_directory.set_path(self.workdir)
        # check if directory exists. if it does, raise an error cause
        # it's weird
        if await self._calculation_directory.exists:
            raise IsADirectoryError(
                    f"{self.workdir} exists but it's not a calcdir.")
        return self._calculation_directory

    @async_property
    async def launcher(self):
        """Return the Launcher object associated."""
        if self._launcher is not None:
            return self._launcher
        self._launcher = await self.get_launcher()
        return self._launcher

    async def get_launcher(self, **kwargs):
        """Compute the Launcher object for this calculation.

        Parameters
        ----------
        kwargs are passed to the '_load_dependencies' method.

        Returns
        -------
        Launcher object: The Launcher for this calculation.
        """
        launcher_cls = __ALL_LAUNCHERS__[self.calctype]
        launcher = launcher_cls(self.jobname, loglevel=logging.ERROR)
        await launcher.set_workdir(self.workdir)
        # start by recovering the calculation if needed
        if self.recover_from is not None:
            recover_from = self.recover_from
            if isinstance(self.recover_from, SequenceCalculation):
                recover_from = self.recover_from.workdir
            try:
                await launcher.recover_from(
                    recover_from,
                    update_variables=self.recover_from_update_variables,
                    pop_variables=self.recover_from_pop_variables,
                    keep_pbs_file_parameters=False,
                    **self.recover_from_other_kwargs)
            except (FileNotFoundError, NotADirectoryError) as err:
                self._logger.error(
                        f"Calc to recover from '{recover_from}' for calc "
                        f"'{self.workdir}' does not exists.")
                self._logger.exception(err)
                raise err
            # I think the line below is wrong and we should use the update
            # kwargs above instead (FG: 27/01/2021)
            # e.g.: when recovering to launch a new relaxation once the
            # previous one is done, we don't want to cancel the optimization!
            # After thinking about it for the night, I think it is good
            # but to prevent overwriting the updated variables, we should
            # just pop them from the input variables.
            invars = self.input_variables.copy()
            for var in self.recover_from_update_variables:
                invars.pop(var, None)
            launcher.input_variables.update(invars)
        else:
            await launcher.set_input_variables(self.input_variables)
        # set all the other optional attributes
        for attr, value in self.calculation_parameters.items():
            if attr == "pseudos":
                await launcher.set_pseudos(value)
                continue
            setattr(launcher, attr, value)
        # all dependencies should be finished and converged if relevant
        await self._load_dependencies(launcher, **kwargs)
        self._launcher = launcher
        return self._launcher

    @async_property
    async def is_local_launch(self):
        """Return True if the launch is a 'local' one."""
        # check if it was a local launch
        # read the rundirectory beforehand to be sure pbs file exists
        rundir = self.calculation_directory.run_directory
        if await rundir.exists and not rundir.has_been_read:
            await rundir.read()
        async with self.calculation_directory.pbs_file as pbs:
            if pbs.queuing_system == "local":
                return True
        return False

    @async_property
    async def is_ready_to_launch(self):
        """Return True if this calculation is ready to be launched."""
        if self._is_ready_to_launch is not None:
            return self._is_ready_to_launch
        # check if calculation is ready to be launched.
        # first of all check its dependencies: they should be done and
        # converged if necessary
        self._logger.debug(
                f"Checking if calculation {self} is ready to launch.")
        for dependency in self.dependencies:
            # dependencies should be SequenceCalculation objects
            # dependencies should be at least written on disk
            if not await (await dependency.calculation_directory).exists:
                # dependency not even written
                self._logger.debug(
                        f"Dependency not written: {dependency}.")
                self._is_ready_to_launch = False
                return self._is_ready_to_launch
            if dependency.bypass_convergence_check:
                continue
            # dependency written. check its status
            calc = await dependency.calculation_directory
            status = await calc.status
            # check status
            finished = status["calculation_finished"]
            if finished is not True:
                self._logger.debug(
                        f"Dependency not finished: {dependency}")
                self._is_ready_to_launch = False
                return self._is_ready_to_launch
            if "calculation_converged" in status:
                converged = status["calculation_converged"]
                if converged is False:
                    self._logger.debug(
                            f"Dependency not converged: {dependency}")
                    self._is_ready_to_launch = False
                    return self._is_ready_to_launch
            # dependency exists and is converged (if relevant) => continue
        if self.load_geometry_from is not None:
            if isinstance(self.load_geometry_from, SequenceCalculation):
                load = self.load_geometry_from.workdir
            else:
                load = self.load_geometry_from
            if not await aiofiles.os.path.exists(load):
                self._is_ready_to_launch = False
                return self._is_ready_to_launch
        # all dependencies are good, check calculation in itself
        if not await (await self.calculation_directory).exists:
            # calculation does not exists => it's ready to launch
            self._logger.debug(
                    f"Calculation not written: {self}")
            self._is_ready_to_launch = True
            return self._is_ready_to_launch
        # calculation already exist. compare OLD vs NEW calc to see if
        # something is different. if not, don't need to relaunch
        if self.bypass_sequence_comparison:
            # bypass checking
            self._is_ready_to_launch = False
            return self._is_ready_to_launch
        try:
            await self._compare_old_vs_new_calculation()
        except CalculationNotConvergedError as err:
            # calculation finished but is not converged
            if self.bypass_convergence_check:
                # we don't care if calculation is not converged
                self._is_ready_to_launch = True
                return self._is_ready_to_launch
            # calculation has not converged but is finished => something wrong
            # happened
            self._logger.error(
                    f"OLD calculation not converged / finished: "
                    f"{self}")
            raise err
        except CalculationNotFinishedError:
            # calculation is running
            self._is_ready_to_launch = False
            return self._is_ready_to_launch
        except CalculationFailedError as err:
            # calculation finished but an error occured
            if self.bypass_error_check:
                self._is_ready_to_launch = False
                return self._is_ready_to_launch
            raise err
        except DifferentSequenceError as err:
            # calculations are different. raise an error to tell that
            self._logger.error(
                f"There are differences with already existing calculation: "
                f"'{self}'.")
            self._logger.exception(err)
            raise err
        # no error has been detected, calculation is the same and it's
        # finished and converged (if relevant). Thus, no need to launch.
        # OR: there was nothing to compared (no input variables given)
        self._logger.debug("Calculation successfully completed: "
                           f"{self}")
        self._is_ready_to_launch = False
        return self._is_ready_to_launch

    async def launch(self, *args, run=True, **kwargs):
        """Launch the calculation."""
        # launch a particular calculation (should be a SequenceCalculation obj)
        # at this stage, the calculation should be ready to launch
        await (await self.launcher).write(*args, **kwargs)
        if run:
            await (await self.launcher).run()

    async def _compare_old_vs_new_calculation(self):
        # read the calculation that exists already
        calc = await self.calculation_directory
        await calc.read()
        # check the calculation has finished and, if relevant, converged
        status = await calc.status
        if status["calculation_finished"] is False:
            raise CalculationNotFinishedError(self.workdir)
        elif status["calculation_finished"] == __ERROR_STRING__:
            raise CalculationFailedError(self.workdir)
        if "calculation_converged" in status:
            if status["calculation_converged"] is False:
                if not self.bypass_convergence_check:
                    raise CalculationNotConvergedError(self.workdir)
        # check links are the same
        have_links = len(self.dependencies) > 0
        have_input_vars = self.input_variables is not None
        if have_links and have_input_vars:
            # don't compare links if no variables given.
            # check the same parents have been given.
            async with calc.meta_data_file as meta:
                new_parents_raw = [x.workdir for x in self.dependencies]
                # if in the new parents, the parents are symlinks to other
                # calcs, we need to point directly to the good dependency
                # because in the meta file it is the TRUE parent (the one
                # the symlink points to) which is written
                new_parents = []
                for parent in new_parents_raw:
                    if await aiofiles.os.path.islink(parent):
                        # if not an absolute path raise an error
                        # (cannot assume where it links to exactly)
                        source = await aiofiles.os.readlink(parent)
                        if not os.path.isabs(source):
                            # try making it absolute
                            new_source = full_abspath(
                                    os.path.join(
                                        os.path.dirname(parent),
                                        source))
                            if not await aiofiles.os.path.isdir(new_source):
                                msg = (
                                    "symlink to another calculation is not "
                                    f"absolute: '{parent}'. Make it absolute!")
                                raise FileNotFoundError(msg)
                            else:
                                source = new_source
                        new_parents.append(source)
                    else:
                        new_parents.append(parent)
                # now check that all dependencies are good
                for parent in meta.parents:  # loop over old parents
                    # go to source
                    while await aiofiles.os.path.islink(parent):
                        parent = await aiofiles.os.readlink(parent)
                    # call normpath because in windows,
                    parent = os.path.normpath(parent)
                    if not any(
                            [await aiofiles.os.path.samefile(x, parent)
                             for x in new_parents]):
                        toprint = "\n".join(new_parents)
                        raise DifferentSequenceError(
                            f"Calc: '{self}' has parent '{parent}' "
                            f"but it's not found in calcs to link: "
                            f"'{toprint}'.")
                old_parents = await expand_symlinks(meta.parents)
                for link in new_parents:
                    link = await expand_symlink(link)
                    samefiles = [
                            os.path.samefile(link, p) for p in old_parents]
                    if not any(samefiles):
                        raise DifferentSequenceError(
                                f"Calc to link: '{link}' not found in "
                                f"parents of already existing calculation:"
                                f" '{self}': {old_parents}")
        # check input variables are the same
        if self.input_variables is not None:
            # compare input variables to make sure it's the same thing
            # to make sure it works out, we need to call the same launcher
            # and link all the calculations
            if self.calctype in (
                    "abinit", "abinit_optic", "qe_epw", "qe_matdyn",
                    "wannier90",
                    ):
                # for these calculations we need to link dependencies to
                # compare input variables (since the input variables depend
                # on what have been linked).
                link = True
            else:
                link = False
            launcher = await self.get_launcher(link_dependencies=link)
            # check if variables are the same
            async with calc.input_file as input_file:
                self._compare_input_variables(
                                input_file.input_variables,
                                launcher.input_variables,
                                is_calc_a_symlink=os.path.islink(self.workdir),
                                )
        # everything matches, return the calc directory
        return calc

    def _compare_input_variables(
            self, old_vars_dict, new_vars_dict, is_calc_a_symlink=False):
        """Compare input variables from an old calculation with the new one.

        Parameters
        ----------
        old_vars_dict: dict
            The dictionary of input variables for the old calculation.
        new_vars_dict: dict
            The dictionary of input variables for the new calculation.
        is_calc_a_symlink: bool, optional
            Defaults to False. If True, when we use a symlink to
            an old calculation, sometimes there are variables
            that do not influence the calculation
            but can differ from the the new ones. For these we skip the check.
        """
        if old_vars_dict == new_vars_dict:
            return
        # we want a detailed analysis
        errors = []
        diff = Colors.color_text("DIFF ERROR", "bold", "red") + ": "
        for k, v in old_vars_dict.items():
            if k not in new_vars_dict:
                if self._skip_var(k):
                    continue
                errors.append(
                        diff + f"'{k}'='{v}' is present in OLD calc but not "
                        "in NEW.")
                continue
            if v != new_vars_dict[k]:
                if self._skip_var(k):
                    continue
                # TODO: make this more general
                if k.startswith("celldm("):
                    # round results to 6 decimals
                    if round(v.value, 6) == round(new_vars_dict[k].value, 6):
                        self._logger.warning(
                            f"'{k}'='{v.value}' in OLD not equal but close to "
                            f"'{k}'='{new_vars_dict[k].value}' in NEW."
                            )
                        continue
                errors.append(
                        diff +
                        f"'{k}'='{v.value}' in OLD but "
                        f"'{k}'='{new_vars_dict[k].value}' in NEW."
                        )
        for k, v in new_vars_dict.items():
            if k not in old_vars_dict:
                if self._skip_var(k):
                    continue
                errors.append(
                        diff + f"'{k}'='{v}' is present in NEW calc but not "
                        "in OLD.")
                continue
        if errors:
            raise DifferentSequenceError(errors, logger=self._logger)

    async def _load_dependencies(self, launcher, link_dependencies=True):
        """Load dependencies for a given launcher object.

        Parameters
        ----------
        link_dependencies: bool, optional
            If True, dependencies are linked to the Launcher.
        """
        # load dependencies when building the launcher object
        # GEOMETRY
        if self.load_geometry_from is not None:
            load_geometry_from = self.load_geometry_from
            if isinstance(load_geometry_from, SequenceCalculation):
                load_geometry_from = load_geometry_from.workdir
            await launcher.load_geometry_from(load_geometry_from)
            # if we need to keep variables, keep them
            if self.keep_variables_after_loading_geometry is not None:
                launcher.input_variables.update(
                        self.keep_variables_after_loading_geometry)
        # KPTS
        if self.load_kpts_from is not None:
            load_kpts_from = self.load_kpts_from
            if isinstance(load_kpts_from, SequenceCalculation):
                load_kpts_from = load_kpts_from.workdir
            await launcher.load_kpts_from(load_kpts_from)
        if not link_dependencies:
            return
        # link files to calculation
        try:
            # GENERAL DEPENDENCY
            if self.dependencies:
                for link in self.dependencies:
                    await launcher.link_calculation(link.workdir)
            # DENSITY
            if self.link_den_from is not None:
                if isinstance(self.link_den_from, SequenceCalculation):
                    await launcher.link_den_from(self.link_den_from.workdir)
                else:
                    await launcher.link_den_from(self.link_den_from)
            # WFK
            if self.link_wfk_from is not None:
                if isinstance(self.link_wfk_from, SequenceCalculation):
                    await launcher.link_wfk_from(self.link_wfk_from.workdir)
                else:
                    await launcher.link_wfk_from(self.link_wfk_from)
            # DDK
            if self.load_ddk_from is not None:
                await launcher.link_ddk_from(self.load_ddk_from)
            # SCR
            if self.link_scr_from is not None:
                link_scr_from = self.link_scr_from
                if isinstance(link_scr_from, SequenceCalculation):
                    link_scr_from = link_scr_from.workdir
                await launcher.link_scr_from(link_scr_from)
            # BS RESONANT BLOCK
            if self.link_bs_reso_from is not None:
                linkbsreso = self.link_bs_reso_from
                if isinstance(linkbsreso, SequenceCalculation):
                    linkbsreso = linkbsreso.workdir
                await launcher.link_bs_reso_from(linkbsreso)
        except Exception as err:
            self._logger.error(
                "Something happened while linking/loading something for calc "
                f"'{self}'.")
            self._logger.exception(err)
            raise err

    def _skip_var(self, var):
        """Check if a var that has changed can be safely skipped.

        This is based on the list of variables that can be skipped:
        __EXCEPTION_VARIABLES__ defined above.

        Parameters
        ----------
        var: str
            The varname to checkup.

        Returns
        -------
        bool: True if we can skip, False otherwise.
        """
        if self.calctype not in __EXCEPTION_VARIABLES__:
            return False
        exc = __EXCEPTION_VARIABLES__[self.calctype]
        if var in exc:
            return True
        for k in exc:
            if "*" not in k:
                continue
            begin = k.split("*")[0]
            if var.startswith(begin):
                return True
        return False


class BaseSequencer(BaseUtility, abc.ABC):
    """Base class for any Sequencer object."""

    _all_sequencer_prefixes = None

    def __init__(
            self, software, *args, bypass_convergence_check=False,
            bypass_sequence_comparison=False, **kwargs):
        """Init method.

        Parameters
        ----------
        software: str
            The software used for the sequence. Either 'abinit' or 'qe' or
            'wannier90' for now.
        bypass_convergence_check: bool, optional
            If True, the sequencer won't care if a calculation is converged or
            not when it is finished before continuing the sequence.
        bypass_sequence_comparison: bool, optional
            If True, the sequence validation is bypassed. That means we don't
            check if all the calculations are coherent when we rerun the
            sequencer. Use this if all calculations were successfull and you
            don't change any parameters but want to get the results faster.
        """
        super().__init__(*args, **kwargs)
        self.software = software
        self.sequence = Sequence(loglevel=self._loglevel)
        self._bypass_sequence_comparison = False
        self.bypass_sequence_comparison = bypass_sequence_comparison
        self.bypass_convergence_check = bypass_convergence_check
        if self._all_sequencer_prefixes is None:
            raise DevError("Need to specify '_all_sequencer_prefixes'.")
        for seq in self._all_sequencer_prefixes:
            # set up all seq_calculation_parameters
            setattr(self, "_" + seq + "calculation_parameters", {})
            # set all workdirs
            setattr(self, "_" + seq + "workdir", None)
            # set all input variables
            setattr(self, "_" + seq + "input_variables", None)
        self._has_been_run = False

    @property
    def bypass_sequence_comparison(self):
        """Boolean flag is True means that we don't compare OLD vs NEW seqs."""
        return self._bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self.sequence.bypass_sequence_comparison = bypass
        self._bypass_sequence_comparison = bypass

    def __getattr__(self, attr):
        """Get a sequence attribute.

        This method is defined here because it is not necessary to define each
        sequence property. In this method, we define defaults
        {seq_part}_workdir,
        {seq_part}_input_variables and {seq_part}_calculation_parameters
        automatically.
        """
        for seq_prefix in self._all_sequencer_prefixes:
            if attr.startswith(seq_prefix):
                if attr.endswith("workdir"):
                    workdir = getattr(self, "_" + seq_prefix + "workdir")
                    if workdir is None:
                        # raise AttributeError(f"'{attr}' not set.")
                        raise RuntimeError(f"'{attr}' not set for obj {self}.")
                # I don't remember why this needs to be commented out...
                # find out why! (FG: 03/08/2020)
                # Answer: because each sequencers defines the property for the
                # input variables which returns the '_{seq}_input_variables'
                # attribute (FG: 03/08/2020).
                # Wow that was a quick self answered question haha
                # if attr.endswith("input_variables"):
                #    return getattr(self, "_" + seq_prefix + "input_variables")
                if attr.endswith("calculation_parameters"):
                    return getattr(
                            self, "_" + seq_prefix + "calculation_parameters")
                attr_name = attr.split(seq_prefix)[-1]
                if attr_name not in getattr(
                        self, seq_prefix + "calculation_parameters"):
                    # raise AttributeError(
                    raise RuntimeError(
                            f"'{self.__class__}': No attr named: '{attr}'")
                return getattr(
                        self,
                        "_" + seq_prefix + "calculation_parameters")[attr_name]
        else:
            # raise AttributeError(
            raise RuntimeError(
                    f"'{self.__class__}': No attr named: '{attr}'")

    async def repr(self):
        """Return a string representation of the sequencer object."""
        if not self.sequence:
            await self.init_sequence()
        return (str(self.__class__) + ": [\n  " + "\n  ".join(
            [repr(x) for x in self.sequence]) + "]")

    def __setattr__(self, attr, value):
        """Set sequencer attributes.

        Samething as the __getattr__ method. It is defined to prevent having
        to always define some attributes to all sequencers.
        """
        if attr in dir(self) or attr.startswith("_") or attr in (
                "sequence", "software"):
            super().__setattr__(attr, value)
            return
        # check if a 'workdir' attribute
        if attr in ([prefix + "workdir"
                     for prefix in self._all_sequencer_prefixes]):
            if value is not None:
                value = full_abspath(value)
            super().__setattr__(attr, value)
            return
        # check if a 'calculation_parameter' attribute:
        if attr in ([prefix + "calculation_parameters"
                     for prefix in self._all_sequencer_prefixes]):
            super().__setattr__(attr, value)
            return
        # check if a 'parameter' for calculation_parameters dict
        for seq_prefix in self._all_sequencer_prefixes:
            if attr.startswith(seq_prefix):
                attr_name = attr.split(seq_prefix)[-1]
                getattr(
                    self, seq_prefix + "calculation_parameters"
                    )[attr_name] = value
                return
        else:
            # raise AttributeError(f"No attr named: {attr}")
            raise RuntimeError(f"No attr named: '{attr}'")

    @property
    def bypass_sequence_comparison(self):
        """Set to True to define the sequence comparison."""
        return self.sequence.bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self.sequence.bypass_sequence_comparison = bypass

    @property
    def bypass_convergence_check(self):
        """Set to True to bypass convergence checks."""
        return self.sequence.bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self.sequence.bypass_convergence_check = bypass

    @property
    def has_been_run(self):
        """Return True if the calculation has been run at least once."""
        return self._has_been_run

    @async_property
    async def sequence_completed(self):
        """Return True if the sequence has successfully completed."""
        return await self.is_sequence_completed()

    async def is_sequence_completed(
            self,
            reverse_order: bool = False,
            raise_error_if_calculation_failed: bool = False,
            sequence: Sequence[SequenceCalculation] = None,
            ) -> bool:
        """Tell if sequence is completed or not.

        Arguments
        ---------
        reverse_order: bool, optional
            If True, sequence completion status is checked from last
            calculation to first.
        sequence: Sequence[SequenceCalculation], optional
            The sequence to check if completed. If None, the self.sequence
            attribute is taken.
        """
        # check that all calculations are completed
        # self.clear_sequence()
        # self.init_sequence()
        # sequence is completed if all calculations from sequence are completed
        self._logger.debug("Checking if sequence is completed.")
        if sequence is None:
            if not self.sequence:
                # init the sequence and rerun
                try:
                    await self.init_sequence()
                    return await self.sequence_completed
                except (TypeError, ValueError, KeyError, RuntimeError):
                    # some variables are not set
                    self._logger.info(
                            "Sequence not completed because some variables not"
                            " set.")
                    return False
            sequence = self.sequence
        try:
            if reverse_order:
                sequence = reversed(sequence)
            for seq in sequence:
                self._logger.debug(
                        f"Checking if {seq} is completed.")
                calc = await seq.calculation_directory
                if not await calc.exists:
                    self._logger.info(
                            f"Sequence incomplete because {seq} does not "
                            "exists.")
                    return False
                await calc.read()
                status = await calc.status
                if status["calculation_finished"] == (
                        __ERROR_STRING__):
                    self._logger.warning(
                            Colors.color_text("ERRORED", "bold", "red") +
                            f": {calc.path}.")
                    if raise_error_if_calculation_failed:
                        raise CalculationFailedError(calc.path)
                    return False
                elif status["calculation_finished"] is False:
                    if status["calculation_started"] is True:
                        self._logger.warning(
                                Colors.color_text(
                                    "RUNNING", "bold", "yellow") +
                                f": {calc.path}.")
                    else:
                        self._logger.warning(
                            Colors.color_text("NOT STARTED", "bold", "cyan") +
                            f": {calc.path}."
                            )
                    return False
                if "calculation_converged" in status:
                    conv = status["calculation_converged"]
                    if conv is False and not self.bypass_convergence_check:
                        self._logger.error(
                                f"{calc.path} has not converged.")
                        raise CalculationNotConvergedError(seq.workdir)
                # calculation completed and converged.
                # But is it the same as the
                # calculation we want?
                try:
                    # this will raise an error if there's something different
                    self._logger.debug(
                            f"Checking if {seq} is ready to launch.")
                    await seq.is_ready_to_launch
                except DifferentSequenceError:
                    if self.bypass_sequence_comparison or (
                            seq.bypass_sequence_comparison):
                        seq._is_ready_to_launch = True
                        self._logger.info(
                                f"Sequence part {seq} is different but we "
                                "bypass check.")
                        continue
                    self._logger.info(f"{seq} is different.")
                    raise
                except Exception as err:
                    # calculation is different
                    self._logger.error(
                            "Something happened while checking if seq part"
                            f" {seq} is ready to launch.")
                    self._logger.exception(err)
                    return False
            # all calculations are completed/converged +
            # they are the same as the
            # one we want. everything is done then!
            self._logger.debug("Sequence Completed!")
            return True
        except AttributeError as err:
            self._logger.exception(
                    f"An AttributeError occured within sequencer '{self}'."
                    )
            self._logger.exception(err)
            raise RuntimeError()

    @abc.abstractmethod
    async def init_sequence(self, *args, **kwargs):
        """Initialize whole sequence."""
        # this method is the backbone of the sequencer. it creates the
        # sequence of calculations and the corresponding dependencies.
        pass

    def apply_plot_calculation_parameters(self, plot):
        """Apply the plot parameters to a given plot object.

        Parameters
        ----------
        plot: Plot object
            The plot on which we wish to apply the plot parameters.
        """
        if "grid" not in self.plot_calculation_parameters:
            # always add grid if not specified
            plot.grid = True
        exc = ["show", "save", "save_pickle", ]
        for param, value in self.plot_calculation_parameters.items():
            if param in exc:
                continue
            elif param == "title" and plot.title:
                # add to it if necessary
                plot.title = value + " | " + plot.title
            else:
                setattr(plot, param, value)

    async def clean(self):
        """Clean all calculation directories stored.

        Raises
        ------
        SequenceNotCompletedError:
            If sequence is not completed when this method is called.
        """
        from .exceptions import SequenceNotCompletedError
        if not self.sequence_completed:
            raise SequenceNotCompletedError(self)
        for calc in self.sequence:
            await calc.clean()

    async def clear_sequence(self):
        """Clear the sequence list."""
        bypass_convergence = self.bypass_convergence_check
        bypass_comparison = self.bypass_sequence_comparison
        del self.sequence
        self.sequence = Sequence(
                bypass_convergence_check=bypass_convergence,
                bypass_sequence_comparison=bypass_comparison,
                loglevel=self._loglevel)

    def get_sequence_calculation(
            self, prefix: str,
            always_return_list: bool = False,
            ) -> list[SequenceCalculation]:
        """Get (all/the) calculation object(s) corresponding to a prefix.

        Parameters
        ----------
        prefix: str
            The prefix to get the sequence calculation from.
        always_return_list: bool, optional
            Always return a list even if it's empty or has length of 1.

        Returns
        -------
        SequenceCalculation objec / list:
            The calculation object or the list of these objects (if more than
            1) corresponding to the given prefix.
        """
        if not len(self.sequence):
            raise RuntimeError(
                "Call 'init_sequence' before getting a Sequence"
                " Calulation.")
        if not prefix.endswith("_"):
            prefix += "_"
        if prefix not in self._all_sequencer_prefixes:
            raise KeyError(prefix)
        workdir = getattr(self, prefix + "workdir")
        if workdir is None:
            raise ValueError(f"Need to set '{prefix}workdir'.")
        seq_workdir = full_abspath(workdir)
        allcalcs = []
        for calc in self.sequence:
            if calc.workdir.startswith(seq_workdir):
                allcalcs.append(calc)
        if not len(allcalcs):
            if always_return_list:
                return allcalcs
            raise LookupError(
                    f"Could not find calculation: '{prefix}' in sequence.")
        if len(allcalcs) == 1 and not always_return_list:
            return allcalcs[0]
        return allcalcs

    async def launch(self, *args, **kwargs):
        """Launch the sequence of calculations."""
        need_to_launch = True
        self._has_been_run = True
        while need_to_launch:
            calcs_to_launch = await self._do_launch(*args, **kwargs)
            if await self.sequence_completed:
                self._logger.info("This sequence is done!")
                await self.post_sequence()
                return
            # if the one of the last calculation was local, relaunch
            need_to_launch = any([x.is_local_launch
                                 for x in calcs_to_launch])
            if need_to_launch:
                self._logger.debug(
                        "Automatically continuing sequence since one of "
                        "the last launched calculation was a local one.")
                continue
            else:
                break
        # we had to stop here
        self._logger.info(
                "Sequence interrupted: need to relaunch after calculations"
                " are completed.")

    async def post_sequence(self, *args, **kwargs):
        """Execute post sequence tasks when sequence is completed.

        By default there is nothing else to do.
        """
        pass

    async def run(self, *args, **kwargs):
        """Run the sequence.

        This is an alias to the
        :meth:`launch() <abisuite.sequencers.bases.BaseSequencer.launch>`
        method.
        """
        await self.launch(*args, **kwargs)

    async def write(self, *args, **kwargs):
        """Like launch but only write calculations."""
        await self.launch(*args, run=False, **kwargs)

    async def _do_launch(self, *args, run: bool = True, **kwargs) -> list:
        # actually launch the sequence and return what have been launched
        await self.clear_sequence()
        await self.init_sequence()
        calcs_to_launch = []
        # compute calculations to launch now
        for calc in self.sequence:
            if await calc.is_ready_to_launch:
                self._logger.debug(f"{calc} needs to be launched.")
                calcs_to_launch.append(calc)
            else:
                self._logger.debug(f"{calc} already launched/done.")
        # launch all calculations that need to be launched
        # order MAY matter here do not launch simultaneously
        for calc in calcs_to_launch:
            await self._do_launch_single_calculation(
                        calc, run, *args, **kwargs)
        return calcs_to_launch

    async def _do_launch_single_calculation(
            self,
            calc: SequenceCalculation, run: bool,
            *args, **kwargs) -> None:
        if run:
            self._logger.info(f"Launching calculation: {calc}")
        else:
            self._logger.info(f"Writing calculation: {calc}")
        try:
            await calc.launch(*args, run=run, **kwargs)
        except TooManyCalculationsInQueueError as err:
            # already too many calculations
            self._logger.error(
                "Too many calculations in queue, cannot launch "
                f"{calc}")
            raise err

    async def _post_process_plot(
            self, plot: Plot | MultiPlot | Mapping,
            name_extension: str = "",
            use_show: bool = False,
            show: bool = True,
            ) -> Plot | MultiPlot | Mapping:
        """Postprocess plots in a generic way.

        Parameters
        ----------
        plot: Plot object to handle
        name_extension: str, optional
            String extension to the filenames used to save the plot.
        use_show: bool, optional
            If True, we don't call the plot() method and use the
            show() method instead. This is in case the 'plot' was already
            called before post processing it.
        show: bool, optional
            If True, we check the 'plot_calculation_parameters' to determine
            if we show plot. Otherwise we force it to not display.

        Returns
        -------
        The plot given as input.
        """
        if is_dict(plot):
            plots = {}
            for plotname, subplot in plot.items():
                plots[plotname] = await self._post_process_plot(
                        subplot,
                        name_extension=name_extension + f"_{plotname}",
                        show=show, use_show=use_show,
                        )
            return plots
        if "plot_" not in self._all_sequencer_prefixes:
            raise RuntimeError(
                    "Not supposed to handle plots if it's not part of the "
                    "Sequencer.")
        # show and save
        if show:
            show = self.plot_calculation_parameters.get("show", True)
        save = self.plot_calculation_parameters.get("save", None)
        self.apply_plot_calculation_parameters(plot)
        if isinstance(plot, MultiPlot):
            show_legend_on = self.plot_calculation_parameters.get(
                    "show_legend_on", None)
            if not use_show:
                plot.plot(show=show, show_legend_on=show_legend_on)
            else:
                if show:
                    plot.show()
        elif isinstance(plot, Plot):
            if not use_show:
                plot.plot(show=show)
            else:
                if show:
                    plot.show()
        else:
            raise TypeError(plot)
        if name_extension and not name_extension.startswith("_"):
            name_extension = "_" + name_extension
        if save is not None:
            if not save.endswith(".pdf"):
                save = save + name_extension + ".pdf"
            else:
                save = save[:-4] + name_extension + ".pdf"
            if save is not None:
                await plot.save(save, overwrite=True)
        save_pickle = self.plot_calculation_parameters.get(
                "save_pickle", None)
        if save_pickle is not None:
            if not save_pickle.endswith(".pickle"):
                save_pickle = save_pickle + name_extension + ".pickle"
            else:
                save_pickle = (
                        save_pickle[:-7] +
                        name_extension + ".pickle")
            # overwrite by default
            await plot.save_plot(save_pickle, overwrite=True)
        # reset to delete matplotlib figures which are
        # not required anymore since
        # show and save have been called.
        plot.reset()
        return plot

    def _set_input_var_to(self, input_vars, varname, value):
        # method to change an input variable and warning user at same time
        if varname not in input_vars:
            self._logger.debug(f"Setting '{varname}' to '{value}'.")
            input_vars[varname] = value
        if input_vars[varname] != value:
            self._logger.debug(f"Setting '{varname}' to '{value}'.")
            input_vars[varname] = value


class Sequence(BaseUtility):
    """A utility class that stores sequence calculation.

    Basically acts as an ordered mutable set.
    """

    _loggername = "Sequence"

    def __init__(self, *args, bypass_sequence_comparison=False,
                 bypass_convergence_check=False, **kwargs):
        super().__init__(*args, **kwargs)
        self._bypass_sequence_comparison = bypass_sequence_comparison
        self._bypass_convergence_check = bypass_convergence_check
        self._container = []

    def __add__(self, obj):
        """Define how we can add these lists of sequence."""
        if not is_list_like(obj):
            self._logger.error(f"Tried to add: '{obj}'.")
            raise TypeError("Cannot add a non-list like obj.")
        for item in obj:
            self.append(item)
        return self

    def __bool__(self):
        """Return True if the sequence is not empty."""
        return bool(self._container)

    def __contains__(self, obj):
        """Check if a calculation is inside this container."""
        return obj in self._container

    def __iter__(self):
        """Iterate over this container."""
        for calc in self._container:
            yield calc

    def __len__(self):
        """Return the number of sequence calculation."""
        return len(self._container)

    def __getitem__(self, index):
        """Get an item from the container."""
        return self._container[index]

    def __setitem__(self, index, value):
        """Set and item into this container."""
        if not isinstance(value, SequenceCalculation):
            raise TypeError(
                    "Can only set 'SequenceCalculation' objects but got: "
                    f"{value}")
        value.bypass_sequence_comparison = self.bypass_sequence_comparison
        value.bypass_convergence_check = self.bypass_convergence_check
        self._container[index] = value

    def __repr__(self):
        """Return a string representation of this container."""
        return f"[{', '.join([repr(x) for x in self])}]"

    @property
    def bypass_convergence_check(self):
        """Set this parameter to all sequence calculations."""
        return self._bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        self._bypass_convergence_check = bypass
        for calc in self:
            calc.bypass_convergence_check = bypass

    @property
    def bypass_sequence_comparison(self):
        """Set this parameter to all sequence calculations."""
        return self._bypass_sequence_comparison

    @bypass_sequence_comparison.setter
    def bypass_sequence_comparison(self, bypass):
        self._bypass_sequence_comparison = bypass
        for calc in self:
            calc.bypass_sequence_comparison = bypass

    def append(
            self, obj: SequenceCalculation,
            set_bypass_comparison_check: bool = True,
            set_bypass_convergence_check: bool = True,
            ) -> None:
        """Append a calculation to this list."""
        if not isinstance(obj, SequenceCalculation):
            raise TypeError(
                    "Can only append 'SequenceCalculation' object but got: "
                    f"{obj}")
        if obj in self:
            # already in self, nevermind this item
            return
        # don't comment the following as we want to be able to set these
        # flags and that they still apply even if sequence is not initialized
        if self.bypass_sequence_comparison and set_bypass_convergence_check:
            obj.bypass_sequence_comparison = self.bypass_sequence_comparison
        if set_bypass_convergence_check:
            obj.bypass_convergence_check = self.bypass_convergence_check
        self._container.append(obj)

    def clear(self):
        """Clear the container of all calculations."""
        del self._container
        self._container = []

    def index(self, calc):
        """Return the index of a given calculation in the sequence.

        If the calc appears more than one time in the Sequence, the first
        index is returned.

        Parameters
        ----------
        calc: SequenceCalculation object
            The object to get the index from.

        Returns
        -------
        int: The index of the SequenceCalculation object in the sequence.
        """
        if not isinstance(calc, SequenceCalculation):
            raise TypeError(
                    "Expected a 'SequenceCalculation' object but got "
                    f"'{calc}'.")
        for icalc, incalc in enumerate(self):
            if calc is incalc:
                return icalc
        raise KeyError(
                f"'{calc}' was not found in sequence: '{self}'.")
