from typing import Any, Mapping

from .bases import SequenceCalculation
from .scf_sequencer import SCFSequencer
from ..exceptions import DevError
from ..routines import is_list_like


class NSCFSequencer(SCFSequencer):
    """A sequencer for a nscf calculation following a scf calculation."""

    _all_sequencer_prefixes = (
            "scf_", "nscf_", )
    _loggername = "NSCFSequencer"

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        if "nscf_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "'nscf_' should be set in '_all_sequencer_prefixes'.")

    @property
    def nscf_input_variables(self):
        """The nscf input variables."""
        if self._nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        return self._nscf_input_variables

    @nscf_input_variables.setter
    def nscf_input_variables(self, ivars):
        self._nscf_input_variables = ivars

    def init_nscf_sequence(self) -> None:
        """Initialize the nscf part of the sequence."""
        scfcalc = self.get_sequence_calculation("scf")
        nscfcalc = self._init_nscf_sequence_against_scf_calc(scfcalc)
        self.sequence.append(nscfcalc)

    def _init_nscf_sequence_against_scf_calc(
            self, scfcalc: SequenceCalculation,
            nscf_workdir: str = None,
            nscf_input_variables: Mapping[str, Any] = None,
            ) -> SequenceCalculation:
        if nscf_workdir is None:
            nscf_workdir = self.nscf_workdir
        if nscf_input_variables is None:
            nscf_input_variables = self.nscf_input_variables
        nscfcalc = SequenceCalculation(
                    scfcalc.calctype, nscf_workdir,
                    nscf_input_variables,
                    self.nscf_calculation_parameters,
                    loglevel=self._loglevel)
        # nscf depend on scf
        nscfcalc.dependencies.append(scfcalc)
        nscfcalc.load_geometry_from = scfcalc.workdir
        return nscfcalc

    async def init_sequence(self, *args, **kwargs) -> None:
        """Initialize the whole sequence."""
        # SCF calculation is set in mother class
        await SCFSequencer.init_sequence(self, *args, **kwargs)
        self.init_nscf_sequence()


class PositiveKpointsNSCFSequencer(NSCFSequencer):
    """A sequencer for a nscf calculation following a scf calculation.

    But, kpoints disposed on a regular kpoint grid with only positive
    coordinates (useful for calculations with wannier90).
    """

    _loggername = "PositiveKpointsNSCFSequencer"

    def __init__(self, *args, **kwargs):
        NSCFSequencer.__init__(self, *args, **kwargs)
        # define kgrid parameters
        self._nscf_kgrid = None

    @property
    def nscf_kgrid(self):
        """Define the nscf kgrid."""
        if self._nscf_kgrid is not None:
            return self._nscf_kgrid
        raise ValueError("Need to set 'nscf_kgrid'")

    @nscf_kgrid.setter
    def nscf_kgrid(self, nscf_kgrid):
        if not is_list_like(nscf_kgrid):
            nscf_kgrid = [nscf_kgrid] * 3
        if not all([isinstance(x, int) for x in nscf_kgrid]):
            raise TypeError("Need integers for nscf kgrid.")
        self._nscf_kgrid = nscf_kgrid
