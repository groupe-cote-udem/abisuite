import abc
from typing import Any

from .bases import SequenceCalculation
from .scf_sequencer import SCFSequencer


class DOSSequencer(SCFSequencer):
    """Sequencer class for DOS calculations."""

    _all_sequencer_prefixes = ("scf_", "dos_", "plot_")

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        self._dos_fine_kpoint_grid_variables = None
        self._dos_plot = None
        self._projected_dos_plots = None

    @property
    def dos_input_variables(self) -> dict:
        """Set the dos input variables."""
        if self._dos_input_variables is None:
            raise ValueError("Need to set 'dos_input_variables'.")
        return self._dos_input_variables

    @dos_input_variables.setter
    def dos_input_variables(self, dos: dict[str, Any]) -> None:
        if not isinstance(dos, dict):
            raise TypeError(f"dos input vars should be a dict. Got '{dos}'.")
        self._dos_input_variables = dos

    @property
    def dos_fine_kpoint_grid_variables(self):
        """Set the dict of fine kpoint grid variables.

        This dict updates the dict containing all the dos input variables.
        """
        if self._dos_fine_kpoint_grid_variables is None:
            raise ValueError("Need to set 'dos_fine_kpoint_grid_variables'.")
        return self._dos_fine_kpoint_grid_variables

    @dos_fine_kpoint_grid_variables.setter
    def dos_fine_kpoint_grid_variables(self, grid):
        if not isinstance(grid, dict):
            raise TypeError(
                    "'dos_fine_kpoint_grid_variables' should be a dict but got"
                    f": '{grid}'.")
        self._dos_fine_kpoint_grid_variables = grid

    @property
    def dos_plot(self):
        """Return the DOS plot."""
        if self._dos_plot is not None:
            return self._dos_plot
        raise RuntimeError("'dos_plot' not generated yet.")

    @property
    def projected_dos_plots(self):
        """Return the projected DOS plots."""
        if self._projected_dos_plots is not None:
            return self._projected_dos_plots
        raise RuntimeError("'projected_dos_plots' not generated.")

    def init_dos_sequence(self):
        """Initialize the DOS part of the sequence."""
        raise NotImplementedError

    def _init_dos_sequence_against_gs_calc(
            self, calc: SequenceCalculation,
            calctype: str = None,
            link_density: bool = True,
            load_geometry: bool = True,
            dos_workdir: str = None,
            ) -> SequenceCalculation:
        if calctype is None:
            calctype = calc.calctype
        if dos_workdir is None:
            dos_workdir = self.dos_workdir
        doscalc = SequenceCalculation(
                calctype, dos_workdir,
                self.dos_input_variables,
                self.dos_calculation_parameters,
                loglevel=self._loglevel)
        doscalc.dependencies.append(calc)
        if link_density:
            doscalc.link_den_from = calc
        if load_geometry:
            doscalc.load_geometry_from = calc
        self.sequence.append(doscalc)
        return doscalc

    async def init_sequence(self):
        """Initialize the whole sequence."""
        raise NotImplementedError

    @abc.abstractmethod
    async def plot_dos(self):
        """Plots the DOS."""
        pass

    async def post_sequence(self, *args, **kwargs):
        """Execute post sequence stuff."""
        await SCFSequencer.post_sequence(self, *args, **kwargs)
        await self.plot_dos()
