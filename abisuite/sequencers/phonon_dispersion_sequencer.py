import abc

import numpy as np

from .individual_phonon_sequencers import BaseIndividualPhononSequencer
from ..exceptions import DevError
from ..plotters import Plot
from ..post_processors import PhononDispersion
from ..routines import is_list_like, is_vector


# TODO: this is the only Sequencer class that works this way
# clean this! (FG: 2021/02/19)
# most probably it was done in the early stages of developement of sequencers
class BasePhononDispersionSequencer(abc.ABC):
    """Base class for phonon dispersion sequencers."""

    def __init__(self, individual_ph_cls, *args, **kwargs):
        # check that the "plot_" prefix is available
        if "plot_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "Need to add 'plot_' in '_all_sequencer_prefixes' of "
                    f"'{self.__class__}'.")
        if not isinstance(self, BaseIndividualPhononSequencer):
            raise DevError(
                    "Phonon dispersion sequencer must be subclasses of "
                    "IndividualPhononSequencers.")
        if not issubclass(individual_ph_cls, BaseIndividualPhononSequencer):
            raise DevError(
                    "Phonon dispersion sequencer must be subclasses of "
                    "IndividualPhononSequencers.")
        self._phonons_qpoint_grid = None
        self._qpoint_path = None
        self._qpoint_path_density = None
        self._individual_ph_cls = individual_ph_cls

    @property
    def phonons_qpoint_grid(self):
        """Define the phonon qpoint grid."""
        if self._phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        return self._phonons_qpoint_grid

    @phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, grid):
        if not is_vector(grid, length=3):
            raise TypeError(
                    "'phonons_qpoint_grid' must be a list of length 3.")
        self._phonons_qpoint_grid = grid

    @property
    def qpoint_path(self):
        """Define the phonon dispersion qpoint path."""
        if self._qpoint_path is not None:
            return self._qpoint_path
        raise ValueError("Need to set 'qpoint_path'.")

    @qpoint_path.setter
    def qpoint_path(self, qpoint_path):
        if not is_list_like(qpoint_path):
            raise TypeError("Expected a list for 'qpoint_path' but got: "
                            f"{qpoint_path}")
        if len(qpoint_path) < 2:
            raise ValueError("'qpoint_path' must have at least 2 points.")
        for qpt in qpoint_path:
            if not isinstance(qpt, dict):
                raise TypeError("Expected a dict for a kpt but got: "
                                f"{qpt}.")
            if len(qpt) > 1:
                raise SyntaxError(
                        "A qpt in qpt_path is a dict whose 'only' key is the "
                        "qpt label and value its coordinates in crystal coords"
                        )
            label = list(qpt.keys())[0]
            coords = qpt[label]
            if not is_list_like(coords):
                raise TypeError(
                        "Expected list for q_point_path coords but got: "
                        f"{coords}")
            if len(coords) != 3:
                raise ValueError("qpoint path coordinates must be of len 3!")
        self._qpoint_path = qpoint_path

    @property
    def qpoint_path_density(self):
        """Define the phonon dispersion qpoint path density."""
        if self._qpoint_path_density is not None:
            return self._qpoint_path_density
        raise ValueError("Need to set 'qpoint_path_density'.")

    @qpoint_path_density.setter
    def qpoint_path_density(self, den):
        if not isinstance(den, int):
            raise TypeError(
                    "qpt density should be an integer.")
        self._qpoint_path_density = den

    async def init_sequence(self):
        """Initialize whole sequence."""
        await self._individual_ph_cls.init_sequence(self)

    async def launch(self, *args, run=True, **kwargs):
        """Launch the sequence of calculation.

        And, if it's completed, plot the phonon dispersion.
        """
        await self._individual_ph_cls.launch(self, *args, run=run, **kwargs)
        if await self.sequence_completed and run:
            self._logger.info("Plotting phonon dispersion...")
            await self.plot_phonon_dispersion()

    async def plot_phonon_dispersion(
            self, name_extension: str = "",
            title: str = "", show: bool = True,
            **kwargs) -> Plot:
        """Make the phonon dispersion plots.

        Parameters
        ----------
        name_extension: str, optional
            An extension to the filename where the plot will be saved
            (if saved).
        title: str, optional
            The plot title.
        show: bool, optional
            If False, the plot is forced to not show.

        Returns
        -------
        Plot: The Plot instance of the phonon dispersion.
        """
        labels = [list(x.keys())[0] for x in self.qpoint_path]
        n = self.qpoint_path_density
        ph_disp = await PhononDispersion.from_calculation(
                self._get_phonon_dispersion_from(**kwargs),
                loglevel=self._loglevel)
        ph_disp.set_qpath_from_uniform_density(labels, n)
        await self._post_fix_phonon_dispersion(ph_disp)
        plot = ph_disp.get_plot(
                yunits=r"cm$^{-1}$",
                ylabel="Energy",
                color=self.plot_calculation_parameters.get("color", "k"),
                linewidth=2,
                adjust_axis=self.plot_calculation_parameters.get(
                    "adjust_axis", True),
                )
        plot.title = title
        # only show positive frequencies
        maxeigs = np.max(ph_disp.frequencies)
        plot.ylims = [0, maxeigs * 1.10]  # +10% above
        # only show relevent x axis (too much is shown)
        plot.xlims = [0, 1.0]
        return await self._post_process_plot(
                plot, name_extension=name_extension,
                show=show,
                )

    @abc.abstractmethod
    def _get_phonon_dispersion_from(self, **kwargs):
        # this method should return the calculation directory where the phonon
        # dispersion is stored / computed
        pass

    async def _post_fix_phonon_dispersion(self, phdisp_obj):
        # nothing to do by default
        # this function makes sure ph dispersion have good units and LOTO
        # splitting is applied
        return
