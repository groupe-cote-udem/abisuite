import os

from async_property import async_property

import numpy as np

from .individual_phonon_sequencers import AbinitIndividualPhononSequencer
from ..bases import SequenceCalculation
from ..phonon_dispersion_sequencer import BasePhononDispersionSequencer
from ...constants import HA_TO_CM
from ...handlers import AbinitAnaddbLogFile, AbinitLogFile
from ...routines import is_2d_arr, is_str, is_vector


class AbinitPhononDispersionSequencer(
        BasePhononDispersionSequencer, AbinitIndividualPhononSequencer):
    """A sequencer to get the phonon dispersion.

    In abinit, the phonon dispersion is computed following this sequence:

    1. Well converged GS calculation (scf);
    2. DFPT for each required qpts. This sequencer launches one calculation
       for each qpt. It also executes a local pre-run to get the list of
       irreducible qpts to do (phonons).
    3. If needed, launch an electric field perturbation to get LO-TO
       splitting accurately.
    4. Merge the dynamical matrix DDB from each phonon calculation (mrgddb).
    5. Diagonalize the dynamical matrix and apply corrections if necessary
       (anaddb).
    6. Plot phonon dispersion (plot).
    """

    _all_sequencer_prefixes = (
            "scf_", "phonons_", "ddk_", "mrgddb_", "anaddb_", "plot_", )
    _loggername = "AbinitPhononDispersionSequencer"

    def __init__(self, *args, **kwargs):
        AbinitIndividualPhononSequencer.__init__(self, *args, **kwargs)
        BasePhononDispersionSequencer.__init__(
                self, AbinitIndividualPhononSequencer, *args, **kwargs)
        self._phonons_qgrid_generation_input_variable_name = None
        self._qpts_list = None

    @property
    def phonons_qgrid_generation_input_variable_name(self):
        """Set the phonons qgrid generation input variable name."""
        if self._phonons_qgrid_generation_input_variable_name is not None:
            return self._phonons_qgrid_generation_input_variable_name
        raise ValueError(
                "Need to set 'phonons_qgrid_generation_input_variable_name'.")

    @phonons_qgrid_generation_input_variable_name.setter
    def phonons_qgrid_generation_input_variable_name(self, varname):
        if not is_str(varname):
            raise TypeError(
                    "'phonons_qgrid_generation_input_variable_name' should be "
                    f" a str but got: '{varname}'.")
        self._phonons_qgrid_generation_input_variable_name = varname

    @BasePhononDispersionSequencer.phonons_qpoint_grid.setter
    def phonons_qpoint_grid(self, grid):
        """Set the phonons qpoint grid."""
        # must be either a list of 3 elements or a 3x3 2d array
        if not is_vector(grid, length=3) and not is_2d_arr(grid, size=(3, 3)):
            raise TypeError(
                    "'phonons_qpoint_grid' must either be a list of len 3 or a"
                    " 3x3 array.")
        self._phonons_qpoint_grid = grid

    # FG: 2021/04/21
    # TODO: rebase this with the QEPhononDispersion sequence_completed
    @async_property
    async def sequence_completed(self):
        """Return True if the sequence has successfully completed."""
        if not self.sequence:
            # FIXME: code not dry with BaseSequencer class...
            try:
                await self.init_sequence()
            except (TypeError, ValueError, KeyError):
                # some variables are not set yet
                return False
            except AttributeError as err:
                self._logger.exception(
                        f"An AttributeError occured within sequencer '{self}'."
                        )
                raise err
        try:
            self.get_sequence_calculation("anaddb_")
        except LookupError:
            # anaddb not present in sequence
            return False
        return await AbinitIndividualPhononSequencer.sequence_completed._fget(
                self)

    @property
    def anaddb_input_variables(self):
        """Define the anaddb input variables."""
        return self._anaddb_input_variables

    @anaddb_input_variables.setter
    def anaddb_input_variables(self, input_vars):
        # for abinit we need to generate explicitely the qpts in the path
        qpts = [list(x.values())[0] for x in self.qpoint_path]
        qph1l = []
        for qpt_start, qpt_end in zip(qpts[:-1], qpts[1:]):
            # go through numpy arrays for math manipulations
            if len(qpt_start) > 3:
                raise ValueError("qpts must be len-3 vectors.")
            delta = (np.array(qpt_end) -
                     np.array(qpt_start)) / self.qpoint_path_density
            for i in range(self.qpoint_path_density):
                qpt = np.array(qpt_start) + i * delta
                qpt = qpt.tolist()
                qpt.append(1.0)  # weight
                qph1l.append(qpt)
        # append last vector as well
        qph1l.append(qpt_end + [1.0])
        input_vars["qph1l"] = qph1l
        input_vars["nph1l"] = len(qph1l)
        # make check for LOTO splitting
        if self.compute_electric_field_response:
            if input_vars.get(
                    "nph2l", 0) == 0 or not len(input_vars.get("qph2l", [])):
                raise ValueError(
                        "Computed electric field response but didnt give any "
                        "direction to compute LO-TO splitting (nph2l, qph2l)."
                        )
        if is_vector(self.phonons_qpoint_grid, length=3):
            self._set_input_var_to(
                input_vars, "ngqpt", self.phonons_qpoint_grid)
        else:
            if "ngqpt" not in input_vars:
                raise ValueError("Need to define 'ngqpt'.")
        self._anaddb_input_variables = input_vars

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await BasePhononDispersionSequencer.init_sequence(self)
        if not self._ready_for_post_phonons_sequence():
            return
        self.init_mrgddb_sequence()
        self.init_anaddb_sequence()

    async def init_phonons_sequence(self):
        """Initialize the phonons part of the sequence."""
        if self._nphonons is not None:
            await AbinitIndividualPhononSequencer.init_phonons_sequence(self)
            return
        # need to determine the qpts for the given qpt grid
        # do this by creating an extra phonon run which only serves the
        # purpose of getting the qpt grid
        # only do 1 shift of 0 to include Gamma
        invars = {
                "kptopt": 1,
                self.phonons_qgrid_generation_input_variable_name: (
                    self.phonons_qpoint_grid),
                "nshiftk": 1,
                "shiftk": [0.0, 0.0, 0.0], "prtvol": -1, "nstep": 0,
                "toldfe": 1e-1,  # dummy (not actually used)
                "ecut": 1.0,     # dummy
                }
        # check that kgrid/qgrid varname is also present in scf vars
        if self.phonons_qgrid_generation_input_variable_name not in (
                self.scf_input_variables):
            raise RuntimeError(
                    "Qgrid/Kgrid must be generated the same way to be "
                    "commensurate.")
        calc_params = self.phonons_calculation_parameters.copy()
        calc_params.update(
                {"queuing_system": "local",
                 "mpi_command": "",
                 })

        calc = SequenceCalculation(
                "abinit",
                os.path.join(self.phonons_workdir + "_qgrid_generation"),
                invars, calc_params,
                bypass_convergence_check=True,
                )
        calc.load_geometry_from = self.get_sequence_calculation("scf")
        self.sequence.append(calc)
        # if completed sequence from here, load the generated qpts
        async with await calc.calculation_directory as calcdir:
            if not await calcdir.exists:
                return
            if (await calcdir.status)["calculation_finished"] is True:
                async with await AbinitLogFile.from_calculation(
                        calc.workdir, loglevel=self._loglevel) as log:
                    self._qpts_list = log.input_variables["kpt"]
                    self.nphonons = len(self._qpts_list)
                await self.init_phonons_sequence()

    def init_mrgddb_sequence(self):
        """Initialize the mrgddb part of the sequence."""
        mrgddb_run = SequenceCalculation(
                "abinit_mrgddb", self.mrgddb_workdir,
                {},  # no input vars for mrgddb
                self.mrgddb_calculation_parameters,
                loglevel=self._loglevel)
        # add all phonon calculations as dependencies
        for calc in self.get_sequence_calculation("phonons"):
            # just skip the qgrid generation part and irreps determination
            if "qgrid_generation" in calc.workdir:
                continue
            if "irreps_determination" in calc.workdir:
                continue
            mrgddb_run.dependencies.append(calc)
        self.sequence.append(mrgddb_run)

    def init_anaddb_sequence(self):
        """Initialize the anaddb part of the sequence."""
        # do the anaddb calc
        anaddb_run = SequenceCalculation(
                "abinit_anaddb", self.anaddb_workdir,
                self.anaddb_input_variables,
                self.anaddb_calculation_parameters,
                loglevel=self._loglevel)
        # only the mrgddb calculation as dependency
        anaddb_run.dependencies.append(
                self.get_sequence_calculation("mrgddb"))
        self.sequence.append(anaddb_run)

    async def launch(self, *args, **kwargs):
        """Launch the sequencer."""
        await BasePhononDispersionSequencer.launch(self, *args, **kwargs)

    def _get_phonon_dispersion_from(self):
        return self.anaddb_workdir

    def _get_phonon_qpt_coordinates(self, iph):
        return self._qpts_list[iph]

    # def _get_phonons_specific_input_variables(self, iph):
    #     # returns specific input variable for phonon number 'iph'.
    #     qpt = self._qpts_list[iph]
    #     if not isinstance(qpt, list):
    #         qpt = list(qpt)
    #     spec = {"kptopt": 3, "rfdir": [1, 1, 1], "rfphon": 1, "irdwfk": 1,
    #             "irdddk": 0, "qpt": qpt, "nqpt": 1}
    #     if qpt == [0.0, 0.0, 0.0]:
    #         # read ddk if electric field response
    #         if self.compute_electric_field_response:
    #             spec["irdddk"] = 1
    #             # compute electric field response only for gamma
    #             spec["rfelfd"] = 3
    #         # set kptopt to 2 instead for Gamma because we can use TR syms
    #         spec["kptopt"] = 2
    #     try:
    #         spec.update(self.phonons_specific_input_variables[iph])
    #     except ValueError:
    #         # don't mind if phonons specific input vars are not set
    #         pass
    #     return spec

    async def _post_fix_phonon_dispersion(self, phdisp_obj):
        # need to convert freqs in Hartrees into cm-1
        phdisp_obj.frequencies *= HA_TO_CM
        if not self.compute_electric_field_response:
            # nothing to do
            return
        # need to fix fot the LO-TO splitting
        # get corrected frequencies at Gamma from output file
        async with await AbinitAnaddbLogFile.from_calculation(
                self.anaddb_workdir, loglevel=self._loglevel) as logfile:
            # get correction
            correction = logfile.non_analytical_gamma_corrections[
                    "eigenvalues"]
        # find gamma vector (if present (usually it is))
        for i, qpt in enumerate(phdisp_obj.qpts):
            if list(qpt) in ([0.0, 0.0, 0.0], [1.0, 1.0, 1.0]):
                phdisp_obj.frequencies[i] = np.array(correction) * HA_TO_CM

    def _ready_for_post_phonons_sequence(self):
        if self._nphonons is None:
            return False
        if self.phonons_qpoints_split_by_irreps is not None:
            for iqpt in self.phonons_qpoints_split_by_irreps:
                if iqpt - 1 not in self._irreps:
                    return False
        return True
