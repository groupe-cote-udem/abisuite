import abc
import asyncio
import os

from async_property import async_property

import numpy as np

from .kgrid_convergence_sequencers import AbinitKgridConvergenceSequencer
from .nscf_sequencers import AbinitNSCFSequencer
from ..bases import SequenceCalculation
from ..exceptions import SequenceNotConvergedError
from ...constants import EV_TO_mEV
from ...exceptions import DevError
from ...handlers import AbinitOutputFile
from ...plotters import Plot
from ...routines import (
        is_2d_arr,
        is_list_like, is_scalar, is_vector,
        sort_data,
        )


class BaseAbinitWithGWScreeningSequencer(AbinitNSCFSequencer):
    """Base class for abinit sequencers with GW screening calculation."""

    def __init__(self, *args, **kwargs):
        AbinitNSCFSequencer.__init__(self, *args, **kwargs)
        self.nscf_load_geometry_from_scf = True
        self.nscf_link_density_from_scf = True
        if "screening_" not in self._all_sequencer_prefixes:
            raise DevError(
                    "Need to have 'screening_' in sequencer prefixes.")

    @property
    def nscf_input_variables(self):
        """Set the nscf input variables."""
        if self._nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        return self._nscf_input_variables

    @nscf_input_variables.setter
    def nscf_input_variables(self, iv):
        self._set_input_var_to(iv, "iscf", -2)
        AbinitNSCFSequencer.nscf_input_variables.fset(self, iv)

    @property
    def screening_input_variables(self):
        """Set the screening input variables."""
        if self._screening_input_variables is None:
            raise ValueError("Need to set 'screening_input_variables'.")
        return self._screening_input_variables

    @screening_input_variables.setter
    def screening_input_variables(self, iv):
        self._set_input_var_to(iv, "optdriver", 3)
        self._set_input_var_to(iv, "istwfk", "*1")
        # check if nband is at least >= than nband of nscf run
        if "nband" in iv:
            nband_nscf = self.nscf_input_variables["nband"]
            scr_nband = iv["nband"]
            if nband_nscf < scr_nband:
                raise ValueError(
                        f"Screening nband ({scr_nband}) should be <= "
                        f"nscf nband ({nband_nscf}).")
        self._screening_input_variables = iv

    def _init_screening_sequence(self):
        # initialize the screening part of the sequencer
        calc = SequenceCalculation(
                "abinit",
                self.screening_workdir,
                self.screening_input_variables,
                self.screening_calculation_parameters,
                loglevel=self._loglevel,
                )
        nscfcalc = self.get_sequence_calculation("nscf")
        calc.dependencies += [nscfcalc]
        calc.load_geometry_from = nscfcalc
        calc.load_kpts_from = nscfcalc
        calc.link_wfk_from = nscfcalc
        self.sequence.append(calc)

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await AbinitNSCFSequencer.init_sequence(self)
        self._init_screening_sequence()


class AbinitGWSequencer(BaseAbinitWithGWScreeningSequencer):
    """Sequencer class for GW runs with abinit."""

    _all_sequencer_prefixes = ("scf_", "nscf_", "screening_", "selfenergy_", )
    _loggername = "AbinitGWSequencer"

    @property
    def selfenergy_input_variables(self):
        """Set the self-energy input variables."""
        if self._selfenergy_input_variables is None:
            raise ValueError("Need to set 'selfenergy_input_variables'.")
        return self._selfenergy_input_variables

    @selfenergy_input_variables.setter
    def selfenergy_input_variables(self, iv):
        self._set_input_var_to(iv, "optdriver", 4)
        self._set_input_var_to(iv, "istwfk", "*1")
        if "nband" in iv:
            # check if nband is at least >= than nband of nscf run
            nband_nscf = self.nscf_input_variables["nband"]
            self_nband = iv["nband"]
            if nband_nscf < self_nband:
                raise ValueError(
                        f"Self energy nband ({self_nband}) should be <= "
                        f"nscf nband ({nband_nscf}).")
        self._selfenergy_input_variables = iv

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await BaseAbinitWithGWScreeningSequencer.init_sequence(self)
        self._init_selfenergy_sequence()

    def _init_selfenergy_sequence(self):
        # initialize self energy part of the sequence
        calc = SequenceCalculation(
                "abinit",
                self.selfenergy_workdir,
                self.selfenergy_input_variables,
                self.selfenergy_calculation_parameters,
                loglevel=self._loglevel,
                )
        nscfcalc = self.get_sequence_calculation("nscf")
        screencalc = self.get_sequence_calculation("screening")
        calc.dependencies += [nscfcalc, screencalc]
        calc.link_wfk_from = nscfcalc
        calc.link_scr_from = screencalc
        calc.load_geometry_from = nscfcalc
        calc.load_kpts_from = nscfcalc
        self.sequence.append(calc)


class BaseAbinitGWConvergenceSequencer(AbinitGWSequencer, abc.ABC):
    """Base class for a convergence study for GW calculations."""

    _all_sequencer_prefixes = (
            "scf_", "nscf_", "screening_", "selfenergy_", "plot_")
    _convergence_criterion_name = None
    _convergence_list_name = None
    _param_to_converge_label = None

    def __init__(self, *args, **kwargs):
        if self._param_to_converge_label is None:
            raise DevError("Need to set '_param_to_converge_label'.")
        if self._convergence_list_name is None:
            raise DevError("Need to set '_convergence_list_name'.")
        if self._convergence_criterion_name is None:
            raise DevError("Need to set '_convergence_criterion_name'.")
        AbinitGWSequencer.__init__(self, *args, **kwargs)
        self._convergence_criterion = None
        self._params_to_test = None
        self._converged_parameter = None

    async def compute_convergence(self, xdata=None, ydata=None):
        """Compute nband converged parameters."""
        criterion = self._get_param_convergence_criterion()
        if xdata is None or ydata is None:
            newxdata, newydata = await self._get_convergence_data()
            if xdata is None:
                xdata = newxdata
            if ydata is None:
                ydata = newydata
        # sort it
        xdata, ydata = sort_data(xdata, ydata)
        deltas = np.abs(np.array(ydata[:-1]) - ydata[-1]) * EV_TO_mEV
        compared = [delta < criterion for delta in deltas]
        if not any(compared):
            raise SequenceNotConvergedError(self, sequence_part="selfenergy_")
        for xdat, comp in zip(xdata, compared):
            if comp:
                self._converged_parameter = {
                        self._param_to_converge_label: xdat}
                return

    async def plot_convergence(self, post_process=True):
        """Plot the convergence against QP gap renormalization."""
        params = []
        qp_gaps_renorm = []
        params, qp_gaps_renorm = await self._get_convergence_data()
        plot = Plot(loglevel=self._loglevel)
        params = self._get_xdata_for_plot(params)
        plot.add_curve(
                params, qp_gaps_renorm, marker="^", markersize=5,
                markerfacecolor="b", color="b", linewidth=2,
                label=r"$\Delta E_{gap}$ (QP-KS direct)",
                sort_data=True,
                )
        curve = plot.plot_objects["curves"][0]
        plot.add_curve(
                curve.xdata[:-1],
                np.abs(curve.ydata[:-1] - curve.ydata[-1]) * EV_TO_mEV,
                marker="v", markerfacecolor="r", markersize=5, color="r",
                linewidth=2, label=r"$|\Delta(\Delta E_{gap})|$", twinx=True,
                )
        plot.add_hline(
                self._get_param_convergence_criterion(),
                twinx=True,
                linewidth=2, linestyle="--",
                label=(
                    r"$\Delta(\Delta E_{gap})=$" +
                    str(self._get_param_convergence_criterion()) + " meV"),
                )
        plot.ylabel = r"$\Delta E_{gap}$ (QP-KS direct) [eV]"
        plot.ylabel_twinx = r"$|\Delta(\Delta E_{gap})|$ [meV]"
        plot.xlabel = self._param_to_converge_label
        plot.title = (
                f"Gap variation vs {self._param_to_converge_label}"
                " convergence study"
                )
        if post_process:
            await self._post_process_plot(plot)
        self._convergence_plot = plot
        await self.compute_convergence(xdata=params, ydata=qp_gaps_renorm)
        return plot

    async def post_sequence(self):
        """Execute post sequence tasks."""
        await AbinitGWSequencer.post_sequence(self)
        await self.plot_convergence()

    async def _get_convergence_data(self) -> None:
        xdata = []
        ydata = []
        coros = []
        for param_to_test in self._get_params_to_test():
            xdata.append(param_to_test)
            coros.append(
                    self._extract_qp_renorm_from_param_to_test(param_to_test))
        ydata = await asyncio.gather(*coros)
        return xdata, ydata

    async def _extract_qp_renorm_from_param_to_test(self, param_to_test):
        calculation = self._get_selfenergy_workdir(param_to_test)
        async with await AbinitOutputFile.from_calculation(
                calculation,
                loglevel=self._loglevel) as out:
            qp_renorm = out.dtsets[0]["gw_self_energy"]["Delta_QP_KS"]
            if np.isnan(qp_renorm):
                raise ValueError(
                        "Obtained a NaN value for QP renormalization for: "
                        f"'{calculation.path}'."
                        )
            return qp_renorm

    def _get_param_convergence_criterion(self):
        if self._convergence_criterion is None:
            raise ValueError(
                f"Need to set '{self._convergence_criterion_name}' "
                "(in meV).")
        return self._convergence_criterion

    def _get_params_to_test(self):
        if self._params_to_test is None:
            raise ValueError(
                    f"Need to set '{self._convergence_list_name}'.")
        return self._params_to_test

    def _get_screening_workdir(self, param):
        return os.path.join(
                self.screening_workdir,
                f"{self._param_to_converge_label}{param}")

    def _get_selfenergy_workdir(self, param):
        return os.path.join(
                self.selfenergy_workdir,
                f"{self._param_to_converge_label}{param}")

    def _set_param_convergence_criterion(self, criterion):
        if not is_scalar(criterion):
            raise TypeError(
                    f"'{self._convergence_criterion_name}' must be "
                    "a scalar.")
        if criterion < 0.0:
            raise ValueError(
                    f"'{self._convergence_criterion_name}' must be "
                    "> 0.")
        self._convergence_criterion = criterion

    def _get_xdata_for_plot(self, params):
        # method used in case we need to modify the xdata parameters
        return params

    def _set_params_to_test(self, params):
        if not is_list_like(params):
            raise TypeError(
                f"'{self._convergence_list_name}' must be a list.")
        self._params_to_test = params


class BaseAbinitGWnbandConvergenceSequencer(
        BaseAbinitGWConvergenceSequencer, abc.ABC):
    """Base class for an abinit gw nband convergence sequencer."""

    _param_to_converge_label = "nband"

    def _set_params_to_test(self, nbands):
        BaseAbinitGWConvergenceSequencer._set_params_to_test(self, nbands)
        for nband in nbands:
            if not isinstance(nband, int):
                raise ValueError(
                        f"'{self._convergence_list_name}' should be "
                        "all integers.")
        # make sure maximal nbands does not go over the nband used for the
        # NSCF band
        nscf_nband = self.nscf_input_variables["nband"]
        if max(nbands) > nscf_nband:
            raise ValueError(
                    f"nband to test ({max(nbands)}) is higher than the # "
                    f"of bands used to compute WFK (nscf nband = {nscf_nband})"
                    )


class AbinitGWSelfEnergynbandConvergenceSequencer(
        BaseAbinitGWnbandConvergenceSequencer):
    """Sequencer to make a nband convergence test agains QP gap renorm.

    Convergence sequencer for the number of bands in the self energy
    calculation. Convergence is done agains QP band gap renormalization.
    """

    _convergence_list_name = "selfenergy_nbands"
    _convergence_criterion_name = (
            "selfenergy_nband_convergence_criterion")
    _loggername = "AbinitGWnbandConvergenceSequencer"

    @async_property
    async def converged_self_energy_nband(self):
        """Return the converged self energy nband."""
        if self._converged_parameter is not None:
            return self._converged_parameter
        await self.compute_convergence()
        return self._converged_parameter

    @property
    def selfenergy_nband_convergence_criterion(self):
        """Return the nband convergence criterion."""
        return self._get_param_convergence_criterion()

    @selfenergy_nband_convergence_criterion.setter
    def selfenergy_nband_convergence_criterion(self, criterion):
        self._set_param_convergence_criterion(criterion)

    @property
    def selfenergy_nbands(self):
        """Set the list of nbands to test."""
        return self._get_params_to_test()

    @selfenergy_nbands.setter
    def selfenergy_nbands(self, nbands):
        self._set_params_to_test(nbands)

    def _init_selfenergy_sequence(self):
        # initialize self energy part of the sequence
        for nband in self.selfenergy_nbands:
            iv = self.selfenergy_input_variables.copy()
            iv["nband"] = nband
            calc = SequenceCalculation(
                    "abinit",
                    self._get_selfenergy_workdir(nband),
                    iv,
                    self.selfenergy_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            nscfcalc = self.get_sequence_calculation("nscf")
            screencalc = self.get_sequence_calculation("screening")
            calc.dependencies += [nscfcalc, screencalc]
            calc.link_wfk_from = nscfcalc
            calc.link_scr_from = screencalc
            calc.load_geometry_from = nscfcalc
            calc.load_kpts_from = nscfcalc
            self.sequence.append(calc)


class BaseAbinitGWScreeningConvergenceSequencer(
        BaseAbinitGWConvergenceSequencer):
    """Base class for abinit gw screening convergence sequencers."""

    def _init_screening_sequence(self):
        # initialize the screening part of the sequencer
        for param in self._get_params_to_test():
            iv = self.screening_input_variables.copy()
            iv[self._param_to_converge_label] = param
            calc = SequenceCalculation(
                    "abinit",
                    self._get_screening_workdir(param),
                    iv,
                    self.screening_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            nscfcalc = self.get_sequence_calculation("nscf")
            calc.dependencies += [nscfcalc]
            calc.load_geometry_from = nscfcalc
            calc.load_kpts_from = nscfcalc
            calc.link_wfk_from = nscfcalc
            self.sequence.append(calc)

    def _init_selfenergy_sequence(self):
        # initialize self energy part of the sequence
        nscfcalc = self.get_sequence_calculation("nscf")
        for seq in self.get_sequence_calculation("screening"):
            param = seq.input_variables[self._param_to_converge_label]
            calc = SequenceCalculation(
                    "abinit",
                    self._get_selfenergy_workdir(param),
                    self.selfenergy_input_variables,
                    self.selfenergy_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            calc.dependencies += [nscfcalc, seq]
            calc.link_wfk_from = nscfcalc
            calc.link_scr_from = seq
            calc.load_geometry_from = nscfcalc
            calc.load_kpts_from = nscfcalc
            self.sequence.append(calc)


class AbinitGWScreeningnbandConvergenceSequencer(
        BaseAbinitGWScreeningConvergenceSequencer,
        BaseAbinitGWnbandConvergenceSequencer):
    """Sequencer to make a nband convergence test agains QP gap renorm.

    The convergence of nbands is the number of bands in the screening
    calculation. Because the particle susceptibility has a double sum
    on the states. This sum must then be converged because it goes
    to infinity technically.

    We converge agains QP gap renormalization.
    """

    _convergence_list_name = "screening_nbands"
    _convergence_criterion_name = (
            "screening_nband_convergence_criterion")
    _loggername = "AbinitGWScreeningnbandConvergenceSequencer"

    @async_property
    async def converged_screening_nband(self):
        """Return the converged screening nband."""
        if self._converged_parameter is not None:
            return self._converged_parameter
        await self.compute_convergence()
        return self._converged_parameter

    @property
    def screening_nband_convergence_criterion(self):
        """Return the nband convergence criterion."""
        return self._get_param_convergence_criterion()

    @screening_nband_convergence_criterion.setter
    def screening_nband_convergence_criterion(self, criterion):
        self._set_param_convergence_criterion(criterion)

    @property
    def screening_nbands(self):
        """Set the list of nbands to test."""
        return self._get_params_to_test()

    @screening_nbands.setter
    def screening_nbands(self, nbands):
        self._set_params_to_test(nbands)


class AbinitGWecutepsConvergenceSequencer(
        BaseAbinitGWScreeningConvergenceSequencer):
    """Abinit gw sequencer class to make a convergence of ecuteps."""

    _convergence_list_name = "screening_ecutepss"
    _convergence_criterion_name = (
            "screening_ecuteps_convergence_criterion")
    _loggername = "AbinitGWecutepsConvergenceSequencer"
    _param_to_converge_label = "ecuteps"

    @async_property
    async def converged_ecuteps(self):
        """Return the converged ecuteps."""
        if self._converged_parameter is not None:
            return self._converged_parameter
        await self.compute_convergence()
        return self._converged_parameter

    @property
    def screening_ecuteps_convergence_criterion(self):
        """Return the ecuteps convergence criterion."""
        self._get_param_convergence_criterion()

    @screening_ecuteps_convergence_criterion.setter
    def screening_ecuteps_convergence_criterion(self, criterion):
        self._set_param_convergence_criterion(criterion)

    @property
    def screening_ecutepss(self):
        """Set the list of ecutepss to test."""
        return self._get_params_to_test()

    @screening_ecutepss.setter
    def screening_ecutepss(self, ecutepss):
        self._set_params_to_test(ecutepss)


class BaseAbinitKgridConvergenceWithGWScreeningSequencer(
        BaseAbinitWithGWScreeningSequencer,
        AbinitKgridConvergenceSequencer):
    """Base class for abinit kgrid convergence sequencers with gw scr."""

    def __init__(self, *args, **kwargs):
        BaseAbinitWithGWScreeningSequencer.__init__(self, *args, **kwargs)
        AbinitKgridConvergenceSequencer.__init__(self, *args, **kwargs)

    async def init_sequence(self):
        """Initialize whole sequence."""
        await AbinitKgridConvergenceSequencer.init_sequence(self)
        self._init_nscf_sequence()
        self._init_screening_sequence()

    def _get_nscf_workdir(self, param):
        return self._get_workdir_from_converging_param(
                param, prefix="nscf", use_parameter_subdirs=False)

    def _get_params_to_test(self):
        return self.scf_kgrids

    def _set_params_to_test(self, params):
        self.scf_kgrids = params

    def _get_scf_workdir_single_calculation(self, *args, **kwargs):
        cls = AbinitKgridConvergenceSequencer
        return cls._get_scf_workdir_single_calculation(
                self, *args, use_parameter_subdirs=False, **kwargs)

    def _get_screening_workdir(self, param):
        return self._get_workdir_from_converging_param(
                param, prefix="screening", use_parameter_subdirs=False)

    def _init_nscf_sequence(self):
        for scfcalc in self.get_sequence_calculation(
                "scf", always_return_list=True,
                ):
            param = scfcalc.input_variables[self.kgrids_input_variable_name]
            nscfcalc = SequenceCalculation(
                    "abinit",
                    self._get_nscf_workdir(param),
                    self.nscf_input_variables,
                    self.nscf_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            nscfcalc.load_geometry_from = scfcalc
            nscfcalc.link_den_from = scfcalc
            nscfcalc.load_kpts_from = scfcalc
            nscfcalc.dependencies.append(scfcalc)
            self.sequence.append(nscfcalc)

    def _init_screening_sequence(self):
        # initialize the screening part of the sequencer
        for nscfcalc in self.get_sequence_calculation(
                "nscf", always_return_list=True,
                ):
            scfcalc = nscfcalc.dependencies[0]
            param = scfcalc.input_variables[self.kgrids_input_variable_name]
            calc = SequenceCalculation(
                    "abinit",
                    self._get_screening_workdir(param),
                    self.screening_input_variables,
                    self.screening_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            calc.dependencies += [nscfcalc]
            calc.load_geometry_from = nscfcalc
            calc.load_kpts_from = nscfcalc
            calc.link_wfk_from = nscfcalc
            self.sequence.append(calc)


# TODO: need to cleanup convergence sequencers...
# it's starting to become a mess out there!!!
# FG: 2021/11/08
class AbinitGWKgridConvergenceSequencer(
        BaseAbinitGWScreeningConvergenceSequencer,
        BaseAbinitKgridConvergenceWithGWScreeningSequencer):
    """Sequencer to make a GW convergence vs kgrid."""

    _convergence_list_name = "scf_kgrids"
    _convergence_criterion_name = "scf_kgrid_convergence_criterion"
    _loggername = "AbinitGWKgridConvergenceSequencer"
    _param_to_converge_label = "kgrid"

    def __init__(self, *args, **kwargs):
        BaseAbinitGWScreeningConvergenceSequencer.__init__(
                self, *args, **kwargs)
        BaseAbinitKgridConvergenceWithGWScreeningSequencer.__init__(
                self, *args, **kwargs)

    @property
    def converged_kgrid(self):
        """Return the converged kgrid."""
        if self._converged_parameter is not None:
            return self._converged_parameter
        self.compute_convergence()
        return self.converged_kgrid

    async def init_sequence(self):
        """Initialize whole sequence."""
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        await cls.init_sequence(self)
        self._init_selfenergy_sequence()

    async def compute_convergence(self, *args, xdata=None, **kwargs):
        """Compute convergence."""
        xdata = self.scf_kgrids
        # create new list-like array representing data for easier sorting
        newxdata = []
        for i, kgrid in enumerate(xdata):
            if is_vector(kgrid, length=3):
                newxdata.append(kgrid[0] * kgrid[1] * kgrid[2])
            elif is_2d_arr(kgrid):
                newxdata.append(f"{self.kgrids_input_variable_name} #{i}")
            else:
                raise TypeError("kgrid type not supported.")
        await BaseAbinitGWScreeningConvergenceSequencer.compute_convergence(
                self, *args, xdata=newxdata, **kwargs)
        # the 'converged' parameter will be an element of this newxdata
        # reset it to a good kgrid list
        self._converged_parameter[self._param_to_converge_label] = (
                xdata[newxdata.index(
                    self._converged_parameter[self._param_to_converge_label])])

    async def plot_convergence(self, *args, **kwargs):
        """Plot convergence."""
        # put in try except finally block in case sequence is not converged
        # that way we can still save the convergence plot.
        try:
            plot = await BaseAbinitGWConvergenceSequencer.plot_convergence(
                self, *args, post_process=False, **kwargs)
        except SequenceNotConvergedError:
            self._add_xdata_to_plot(self._convergence_plot)
            await self._post_process_plot(self._convergence_plot)
            raise
        self._add_xdata_to_plot(plot)
        await self._post_process_plot(plot)

    def _init_nscf_sequence(self):
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        cls._init_nscf_sequence(self)

    def _init_screening_sequence(self):
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        cls._init_screening_sequence(self)

    def _init_selfenergy_sequence(self):
        # initialize self energy part of the sequence
        for scrcalc in self.get_sequence_calculation(
                "screening",
                always_return_list=True,
                ):
            nscfcalc = scrcalc.dependencies[0]
            scfcalc = nscfcalc.dependencies[0]
            param = scfcalc.input_variables[self.kgrids_input_variable_name]
            calc = SequenceCalculation(
                    "abinit",
                    self._get_selfenergy_workdir(param),
                    self.selfenergy_input_variables,
                    self.selfenergy_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            calc.dependencies += [nscfcalc, scrcalc]
            calc.link_wfk_from = nscfcalc
            calc.link_scr_from = scrcalc
            calc.load_geometry_from = nscfcalc
            calc.load_kpts_from = nscfcalc
            self.sequence.append(calc)

    def _get_params_to_test(self):
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        return cls._get_params_to_test(self)

    def _set_params_to_test(self, params):
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        cls._set_params_to_test(self, params)

    def _get_xdata_for_plot(self, params):
        return [self._get_params_to_test().index(x) for x in params]

    def _get_param_convergence_criterion(self):
        return self.scf_convergence_criterion

    def _get_selfenergy_workdir(self, param):
        return self._get_workdir_from_converging_param(
                param, prefix="selfenergy", use_parameter_subdirs=False)

    def _get_screening_workdir(self, *args, **kwargs):
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        return cls._get_screening_workdir(self, *args, **kwargs)

    def _set_param_convergence_criterion(self, criterion):
        self.scf_convergence_criterion = criterion
