import os

from .scf_sequencer import AbinitSCFSequencer
from ..bases import SequenceCalculation
from ..relaxation_sequencers import RelaxationSequencer
from ...handlers import AbinitInputFile, AbinitLogFile
from ...launchers.launchers.abinit_launchers.abinit_launcher import (
        __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__,
        __ALL_ABINIT_GEOMETRY_VARIABLES__,
        )
from ...status_checkers import __ERROR_STRING__


class AbinitRelaxationSequencer(AbinitSCFSequencer, RelaxationSequencer):
    """Relaxation Sequencer for abinit.

    This is just an SCF calculation with some specific input variables.
    """

    _loggername = "AbinitRelaxationSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        RelaxationSequencer.__init__(self, "abinit", *args, **kwargs)

    @property
    def scf_input_variables(self):
        """Define the scf input variables."""
        return self._scf_input_variables

    @scf_input_variables.setter
    def scf_input_variables(self, inputs):
        if not isinstance(inputs, dict):
            raise TypeError(
                    "'scf_input_variables' should be a dict.")
        # mandatory variables
        for var in ("ionmov", "ntime", ):
            if var not in inputs:
                raise ValueError(f"'{var}' should be specified.")
        # ionmov should be >= 2
        ionmov = inputs.get("ionmov")
        if ionmov < 2:
            raise ValueError(
                    f"ionmov should be >= 2 while it is set to '{ionmov}'.")
        self._scf_input_variables = inputs

    async def init_sequence(self):
        """Initialize the whole sequence.

        If `self.relax_atoms` is True, we start by optimizing atoms
        using optcell = 0.
        Then, `if self.relax_cell` is True, we relax the cell
        using optcell = 2. If `relax_atoms` is False, we straight do
        the full cell relaxation (optcell = 2) (not recommended by
        Abinit).
        """
        if self.relax_atoms:
            invars = self.scf_input_variables.copy()
            invars.update({"optcell": 0})
            seq = SequenceCalculation(
                    "abinit", self._get_relax_atoms_workdir(),
                    invars, self.scf_calculation_parameters,
                    loglevel=self._loglevel)
            self.sequence.append(seq)
        if self.relax_cell:
            await self.init_cell_optimization_sequence()
        else:
            self._truly_completed = True

    async def init_cell_optimization_sequence(self):
        """Initialize cell optimization part of the sequence."""
        invars = self.scf_input_variables.copy()
        # only set optcell = 2 if not already present
        # we want to allow for different optcell
        if "optcell" not in invars:
            invars.update({"optcell": 2})
            optcell = 2
        else:
            optcell = invars["optcell"]
            if optcell == 1:
                # set to 2 by default
                invars.update({"optcell": 2})
                optcell = 2
            self._logger.info(
                    "Using user-defined optcell for cell optimization: "
                    f"{optcell}.")
        if "dilatmx" not in invars:
            self._logger.info(
                    "Using dilatmx=1.15 since it is not specified in input "
                    "variables")
            invars["dilatmx"] = 1.15
        # try a first cell optimization
        seq = SequenceCalculation(
                "abinit", self._get_relax_cell_workdir(),
                invars,
                self.scf_calculation_parameters,
                loglevel=self._loglevel)
        if self.relax_atoms:
            seq.recover_from = self.sequence[0]
            seq.recover_from_other_kwargs["use_last_geometry"] = True
            seq.recover_from_update_variables = {"optcell": optcell}
            # pop out some vars to make sure we don't overwrite the ones
            # retrieved from the relax atoms one
            for varname in ["xcart", "xred", "xangst"]:
                seq.input_variables.pop(varname, None)
            seq.dependencies.append(self.sequence[0])
        self.sequence.append(seq)
        # check if this calc has succeeded
        calc = await seq.calculation_directory
        if not await calc.exists:
            return
        async with calc:
            stat = await calc.status
        if stat["calculation_finished"] is True:
            if stat["calculation_converged"] is True:
                # we're done
                self._truly_completed = True
                return
            # calculation finished but not converged.
            raise LookupError(
                    f"Relaxation {calc.path} has finished but not converged.")
        elif stat["calculation_finished"] == __ERROR_STRING__:
            await self.init_chkdilatmx0_relaxation_sequence()

    async def init_chkdilatmx0_relaxation_sequence(self):
        """Initialize cell optimization part of the sequence with chkdilatmx=0.

        This is called if the first cell optimization calculations failed
        because abinit overshot the dilatmx criterion.
        """
        # check error in log file. if we overshot the dilatmx parameter
        # launch a new one with chkdilatmx = 0
        calc = await self.sequence[1].calculation_directory
        invars = self.sequence[1].input_variables.copy()
        try:
            async with await calc.log_file as log:
                if all(err["name"] != "DilatmxError" for err in log.errors):
                    raise LookupError(
                            f"calc {calc.path} has an unexpected errors: "
                            f"{log.errors}.")
        except Exception as err:
            self._logger.error("An error occured while parsing log file")
            self._logger.exception(err)
            return
        self.sequence[1].bypass_convergence_check = True
        self._logger.info(
                "Abinit overshot dilatmx. Relaunch with chkdilatmx=0")
        seq_chk0 = SequenceCalculation(
                "abinit",
                self._get_relax_cell_workdir(step="chkdilatmx0"),
                invars, self.scf_calculation_parameters,
                loglevel=self._loglevel)
        seq_chk0.recover_from = self.sequence[1]
        seq_chk0.recover_from_other_kwargs["use_last_geometry"] = True
        seq_chk0.recover_from_update_variables = {"chkdilatmx": 0}
        seq_chk0.dependencies.append(self.sequence[1])
        self.sequence.append(seq_chk0)
        # if this calc is finished. add another one
        async with await seq_chk0.calculation_directory as calc:
            stat = await calc.status
        if stat["calculation_finished"] == __ERROR_STRING__:
            raise LookupError(
                    f"An unexpected error occured in {calc.path}")
        if stat["calculation_finished"] is not True:
            # not completed yet
            return
        seq_chk1 = SequenceCalculation(
                "abinit",
                self._get_relax_cell_workdir(step=1),
                invars, self.scf_calculation_parameters,
                loglevel=self._loglevel,
                )
        seq_chk1.recover_from = seq_chk0
        seq_chk1.recover_from_other_kwargs["use_last_geometry"] = True
        seq_chk1.recover_from_update_variables = {"chkdilatmx": 1}
        seq_chk1.dependencies.append(seq_chk0)
        self.sequence.append(seq_chk1)
        self._truly_completed = True

    def _get_relax_atoms_workdir(self):
        return os.path.join(self.scf_workdir, "relax_atoms_only")

    def _get_relax_cell_workdir(self, step: int | str = None) -> str:
        basename = "relax_cell_geometry"
        if step is not None:
            basename += f"_{step}"
        return os.path.join(self.scf_workdir, basename)

    async def _get_relaxed_geometry_variables(self):
        # this method is called only if sequence is completed
        # if geometry variables undefined in input variables,
        # load them from output file of the relaxation calculation
        if self.relax_cell:
            workdir = self._get_relax_cell_workdir()
        else:
            workdir = self._get_relax_atoms_workdir()
        variables = {}
        async with await AbinitLogFile.from_calculation(
                workdir) as log, await AbinitInputFile.from_calculation(
                        workdir) as inp:
            output = log.output_variables
            inputs = inp.input_variables
        for var in __ALL_ABINIT_GEOMETRY_VARIABLES__:
            aoftgv = __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__
            if any([var in sublist for sublist in aoftgv]):
                # deal with them later
                continue
            if var not in output:
                if var not in ["rprim", "pp_dirpath", "pseudos"]:
                    raise LookupError(f"'{var}' not in relaxation output.")
                if var in inputs:
                    variables[var] = inputs[var]
                    continue
                if var == "rprim":
                    # var == rprim, we treat it differently since it has a
                    # default value which, when used, is not echoed.
                    # check first if it is defined in the input file
                    # use default (identity matrix)
                    variables[var] = [
                            [1.0, 0.0, 0.0],
                            [0.0, 1.0, 0.0],
                            [0.0, 0.0, 1.0]]
            else:
                variables[var] = output[var]
        for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__:
            n_present_in_output = 0
            for var in sublist:
                if var in output:
                    n_present_in_output += 1
            if n_present_in_output == 1:
                # just load this one
                for var in sublist:
                    if var not in output:
                        continue
                    variables[var] = output[var]
            elif n_present_in_output > 1:
                # more than one var is defined in output. take the same
                # one that is defined in input
                # btw there should be only one var in input technically
                for var in sublist:
                    if var in inp.input_variables:
                        variables[var] = output[var]
                        break
            else:
                natom = inputs["natom"]
                if "xred" in sublist and natom == 1:
                    # if only 1 atom => it was not displaced and thus may
                    # not have been reported in output. By default take
                    # the input xred or other coords
                    for var in sublist:
                        if var in inputs:
                            variables[var] = inputs[var]
                            break
                    else:
                        raise LookupError(
                            f"None of '{sublist}' defined in output and inputs"
                            )
                else:
                    raise LookupError(
                            f"None of '{sublist}' defined in output...")
        return variables
