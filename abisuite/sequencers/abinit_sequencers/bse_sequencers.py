import abc
import asyncio
import os

import matplotlib.pyplot as plt

from .gw_sequencers import (
        BaseAbinitKgridConvergenceWithGWScreeningSequencer,
        BaseAbinitWithGWScreeningSequencer,
        )
from ..bases import SequenceCalculation
from ...post_processors import DielectricTensor
from ...routines import (
        is_integer,
        is_list_like, is_vector,
        )


class AbinitBSESequencer(BaseAbinitWithGWScreeningSequencer):
    """Abinit sequencer for BSE calculations.

    This sequencer is made of 2 nscf runs. 1 with symetries and one
    without it (see abinit tutorial on BSE).

    This is because for the bse equation, one needs the full kpt grid
    randomly shifted whereas the screening calculation can be taken
    in the IBZ only.
    """

    _all_sequencer_prefixes = (
            "scf_", "nscf_", "screening_", "nscfnosym_", "bse_", "plot_",
            )
    _loggername = "AbinitBSESequencer"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._link_bs_reso_from = None

    @property
    def link_bs_reso_from(self):
        """Path to use an already produced BSR file."""
        return self._link_bs_reso_from

    @link_bs_reso_from.setter
    def link_bs_reso_from(self, link):
        self._link_bs_reso_from = link

    async def init_sequence(self):
        """Initialize whole sequence."""
        await BaseAbinitWithGWScreeningSequencer.init_sequence(self)
        self._init_nscfnosym_sequence()
        self._init_bse_sequence()

    @property
    def nscfnosym_input_variables(self):
        """Set the nscfnosym input variables."""
        if self._nscfnosym_input_variables is None:
            raise ValueError(
                    "Need to set 'nscfnosym_input_variables'.")
        return self._nscfnosym_input_variables

    @nscfnosym_input_variables.setter
    def nscfnosym_input_variables(self, iv):
        self._set_input_var_to(iv, "iscf", -2)
        self._nscfnosym_input_variables = iv

    @property
    def bse_input_variables(self):
        """Set the bse input variables."""
        if self._bse_input_variables is None:
            raise ValueError("Need to set 'bse_input_variables'.")
        return self._bse_input_variables

    @bse_input_variables.setter
    def bse_input_variables(self, iv):
        self._set_input_var_to(iv, "optdriver", 99)
        self._set_input_var_to(iv, "chksymbreak", 0)
        self._bse_input_variables = iv

    def _init_nscfnosym_sequence(self):
        nscfnosymcalc = SequenceCalculation(
                "abinit",
                self.nscfnosym_workdir,
                self.nscfnosym_input_variables,
                self.nscfnosym_calculation_parameters,
                loglevel=self._loglevel,
                )
        scf = self.get_sequence_calculation("scf")
        nscfnosymcalc.dependencies.append(scf)
        nscfnosymcalc.load_geometry_from = scf
        nscfnosymcalc.link_den_from = scf
        self.sequence.append(nscfnosymcalc)

    def _init_bse_sequence(self):
        nscfsymnocalc = self.get_sequence_calculation("nscfnosym")
        scr = self.get_sequence_calculation("screening")
        bse = SequenceCalculation(
                "abinit",
                self.bse_workdir,
                self.bse_input_variables,
                self.bse_calculation_parameters,
                loglevel=self._loglevel,
                )
        bse.dependencies.append(scr)
        bse.dependencies.append(nscfsymnocalc)
        bse.link_scr_from = scr
        bse.link_wfk_from = nscfsymnocalc
        bse.load_geometry_from = self.get_sequence_calculation("scf")
        bse.load_kpts_from = nscfsymnocalc
        bse.link_bs_reso_from = self.link_bs_reso_from
        self.sequence.append(bse)

    async def post_sequence(self):
        """Execute post sequence tasks."""
        await BaseAbinitWithGWScreeningSequencer.post_sequence(self)
        await self.plot_dielectric_function()

    async def plot_dielectric_function(
            self, bse_workdir=None, split_by_mdf_type=False,
            mdf_types=None, post_process=True,
            ):
        """Make the dielectric function plots.

        Parameters
        ----------
        bse_workdir: str, optional
            If None, the sequencer's 'bse_workdir' is taken. Otherwise
            this one is taken.
        split_by_mdf_type: bool, optional
            If False, all mdf_types are merged into a single plot.
        mdf_types: str, list-like, {'exc', 'gw_nlf', 'rpa_nlf'}
            The mdf type to plot. If None, they are all plotted.
            Can be a list of the ones to plot.
        post_process: bool, optional
            If True, the plots are post processed.

        Returns
        -------
        Plot:
            The plot object representing the dielectric function(s).
            In case the 'split_by_mdf_type' arg is False.
        dict:
            A dict whose keys are the mdf_types and whose values
            are the corresponding plots.
        """
        plots = []
        all_possible_mdf_types = ["exc", "gw_nlf", "rpa_nlf"]
        if mdf_types is not None:
            if not is_list_like(mdf_types):
                mdf_types = (mdf_types, )
        else:
            mdf_types = all_possible_mdf_types
        if bse_workdir is None:
            bse_workdir = self.bse_workdir
        plots = await asyncio.gather(
                *(self._create_dielectric_tensor_plot(
                    mdf_type, color, all_possible_mdf_types, bse_workdir)
                  for mdf_type, color in zip(mdf_types, ["r", "g", "b"])
                  )
                )
        if split_by_mdf_type:
            toreturn = {}
            coro = []
            for plot, mdf_type in zip(plots, mdf_types):
                if post_process:
                    coro.append(self._post_process_plot(
                        plot, name_extension=mdf_type))
                toreturn[mdf_type] = plot
            await asyncio.gather(*coro)
            return toreturn
        else:
            final_plot = sum(plots[1:], plots[0])
            if post_process:
                await self._post_process_plot(final_plot)
            return final_plot

    async def _create_dielectric_tensor_plot(
            self, mdf_type, color, all_possible_mdf_types, bse_workdir):
        if mdf_type not in all_possible_mdf_types:
            raise ValueError(f"'mdf_type'={mdf_type} is invalid.")
        diel_tensor = await DielectricTensor.from_calculation(
               bse_workdir, mdf_type=mdf_type,
               loglevel=self._loglevel)
        return diel_tensor.get_plot(
                elements=((0, 0), ), color=color, label=mdf_type,
                multiplot=False)[1]  # Im part


class BaseAbinitBSEConvergenceSequencer(AbinitBSESequencer, abc.ABC):
    """Base class for abinit BSE convergence sequencers."""

    async def plot_dielectric_function(self):
        """Plot the dielectric function convergences."""
        bsecalcs = self.get_sequence_calculation("bse")
        n = len(bsecalcs)
        cmap = plt.get_cmap("gist_rainbow", n)
        colors = [cmap(x / (n - 1)) for x in range(n)]
        final_plots = []
        allplots = await asyncio.gather(
            *(AbinitBSESequencer.plot_dielectric_function(
                        self,
                        split_by_mdf_type=True,
                        bse_workdir=bsecalc.workdir,
                        mdf_types="exc", post_process=False,
                        )
              for bsecalc in bsecalcs))
        for icalc, (plots, bsecalc, color) in enumerate(
                zip(allplots, bsecalcs, colors)):
            # only retain the exc plot since the other ones should not change
            plot = plots["exc"]
            plot.plot_objects["curves"][0].color = color
            plot.plot_objects["curves"][0].label = self._get_plot_curve_label(
                    bsecalc, icalc)
            final_plots.append(plot)
        final = sum(final_plots[1:], final_plots[0])
        await self._post_process_plot(final)

    @abc.abstractmethod
    def _get_bse_specific_workdir(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _get_bse_specific_input_variables(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _get_plot_curve_label(self, *args, **kwargs):
        pass

    @abc.abstractmethod
    def _get_n_bse_calcs(self, *args, **kwargs):
        pass

    def _init_bse_sequence(self):
        nscfsymnocalc = self.get_sequence_calculation("nscfnosym")
        scr = self.get_sequence_calculation("screening")
        for i in range(self._get_n_bse_calcs()):
            bse = SequenceCalculation(
                    "abinit",
                    self._get_bse_specific_workdir(i),
                    self._get_bse_specific_input_variables(i),
                    self.bse_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            bse.dependencies.append(scr)
            bse.dependencies.append(nscfsymnocalc)
            bse.link_scr_from = scr
            bse.link_wfk_from = nscfsymnocalc
            bse.load_geometry_from = self.get_sequence_calculation("scf")
            bse.load_kpts_from = nscfsymnocalc
            self.sequence.append(bse)


class AbinitBSEecutepsConvergenceSequencer(BaseAbinitBSEConvergenceSequencer):
    """Sequencer for an ecuteps convergence for a BSE calculation."""

    _loggername = "AbinitBSEecutepsConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseAbinitBSEConvergenceSequencer.__init__(self, *args, **kwargs)
        self._bs_ecutepss = None

    @property
    def bs_ecutepss(self):
        """Set the list of ecuteps to test."""
        if self._bs_ecutepss is None:
            raise ValueError("Need to set 'bs_ecutepss'.")
        return self._bs_ecutepss

    @bs_ecutepss.setter
    def bs_ecutepss(self, ecuts):
        if not is_list_like(ecuts):
            raise TypeError("'bs_ecutepss' must be list-like.")
        scr_ecuteps = self.screening_input_variables["ecuteps"]
        for ecut in ecuts:
            if ecut <= 0.0:
                raise ValueError("ecuteps must be > 0.0")
            if ecut > scr_ecuteps:
                raise ValueError(
                        "(bse) ecuteps must be <= "
                        f"(scr) ecuteps ({scr_ecuteps})."
                        )
        self._bs_ecutepss = ecuts

    @property
    def bse_ecutepss(self):
        """Set the list of ecuteps to test."""
        return self.bs_ecutepss

    @bse_ecutepss.setter
    def bse_ecutepss(self, ecuts):
        self.bs_ecutepss = ecuts

    def _get_bse_specific_workdir(self, icalc):
        ecuteps = self.bs_ecutepss[icalc]
        return os.path.join(self.bse_workdir, f"ecuteps{ecuteps}")

    def _get_bse_specific_input_variables(self, icalc):
        iv = self.bse_input_variables.copy()
        iv["ecuteps"] = self.bs_ecutepss[icalc]
        return iv

    def _get_plot_curve_label(self, bsecalc, *args):
        ecut = bsecalc.input_variables["ecuteps"]
        return f"ecuteps = {ecut} Ha"

    def _get_n_bse_calcs(self):
        return len(self.bse_ecutepss)


class AbinitBSEnbandConvergenceSequencer(BaseAbinitBSEConvergenceSequencer):
    """Sequencer to make a nband convergence in transition space for BSE calcs.

    Converging on both the 'bs_loband' parameter and 'nband'.
    We converge on both parameters at same time. User gives two lists of
    parameters that we will iterate over both of them at same time.
    """

    _loggername = "AbinitBSEnbandConvergenceSequencer"

    def __init__(self, *args, **kwargs):
        BaseAbinitBSEConvergenceSequencer.__init__(self, *args, **kwargs)
        self._bs_lobands = None
        self._bs_nbands = None

    @property
    def bse_lobands(self):
        """Alias of 'bs_lobands'."""
        return self.bs_lobands

    @property
    def bse_nbands(self):
        """Alias of 'bs_nbands'."""
        return self.bse_nbands

    @bse_lobands.setter
    def bse_lobands(self, lobands):
        self.bs_lobands = lobands

    @bse_nbands.setter
    def bse_nbands(self, nbands):
        self.bs_nbands = nbands

    @property
    def bs_lobands(self):
        """Set the list of 'bs_loband' to test."""
        if self._bs_lobands is None:
            raise ValueError("Need to set 'bs_lobands'.")
        return self._bs_lobands

    @bs_lobands.setter
    def bs_lobands(self, lobands):
        if not is_list_like(lobands):
            raise TypeError("'bs_lobands' must be list-like.")
        if self._bs_nbands is not None:
            if len(lobands) != len(self.bs_nbands):
                raise ValueError(
                        "'bs_lobands' must be same length as 'bs_nbands'.")
        for loband in lobands:
            if not is_integer(loband):
                raise TypeError(
                        f"nbands must be integers. One of them is: {loband}.")
        self._bs_lobands = lobands

    @property
    def bs_nbands(self):
        """Set the 'nband' to test."""
        if self._bs_nbands is None:
            raise ValueError("Need to set 'bs_nbands'.")
        return self._bs_nbands

    @bs_nbands.setter
    def bs_nbands(self, nbands):
        if not is_list_like(nbands):
            raise TypeError("'bs_nbands' must be list-like.")
        if self._bs_lobands is not None:
            if len(nbands) != len(self.bs_lobands):
                raise ValueError(
                        "'bs_nbands must be same length as 'bs_lobands'.")
        for nband in nbands:
            if not is_integer(nband):
                raise TypeError(
                        f"nbands must be integers. One of them is: {nband}.")
        self._bs_nbands = nbands

    def _get_bse_specific_workdir(self, icalc):
        bs_loband = self.bs_lobands[icalc]
        nband = self.bs_nbands[icalc]
        return os.path.join(
                self.bse_workdir,
                f"bs_loband{bs_loband}_nband{nband}",
                )

    def _get_bse_specific_input_variables(self, icalc):
        iv = self.bse_input_variables.copy()
        iv["bs_loband"] = self.bs_lobands[icalc]
        iv["nband"] = self.bs_nbands[icalc]
        return iv

    def _get_plot_curve_label(self, bsecalc, *args):
        lo_band = bsecalc.input_variables["bs_loband"]
        nband = bsecalc.input_variables["nband"]
        return f"bs_loband={lo_band}, nband={nband}"

    def _get_n_bse_calcs(self):
        return len(self.bs_lobands)


class AbinitBSEKgridConvergenceSequencer(
        BaseAbinitBSEConvergenceSequencer,
        BaseAbinitKgridConvergenceWithGWScreeningSequencer):
    """Abinit sequencer to make a kgrid convergence test."""

    _loggername = "AbinitBSEngkptConvergenceSequencer"
    _param_to_converge_label = "kgrid"

    def __init__(self, *args, **kwargs):
        BaseAbinitBSEConvergenceSequencer.__init__(self, *args, **kwargs)
        BaseAbinitKgridConvergenceWithGWScreeningSequencer.__init__(
                self, *args, **kwargs)

    async def init_sequence(self):
        """Initialize whole sequence."""
        cls = BaseAbinitKgridConvergenceWithGWScreeningSequencer
        await cls.init_sequence(self)
        self._init_nscfnosym_sequence()
        self._init_bse_sequence()

    def compute_convergence(self):
        """Not relevant here."""
        raise RuntimeError("Not relevent for this sequence.")

    async def post_sequence(self):
        """Execute post sequence tasks."""
        await BaseAbinitBSEConvergenceSequencer.post_sequence(self)

    def _get_bse_workdir(self, param):
        return self._get_workdir_from_converging_param(
                param, prefix="bse", use_parameter_subdirs=False)

    def _get_nscfnosym_workdir(self, param):
        return self._get_workdir_from_converging_param(
                param, prefix="nscfnosym", use_parameter_subdirs=False)

    def _init_nscfnosym_sequence(self):
        for scfcalc in self.get_sequence_calculation("scf"):
            param = scfcalc.input_variables[self.kgrids_input_variable_name]
            iv = self.nscfnosym_input_variables.copy()
            iv[self.kgrids_input_variable_name] = scfcalc.input_variables[
                    self.kgrids_input_variable_name]
            nscfcalc = SequenceCalculation(
                    "abinit",
                    self._get_nscfnosym_workdir(param),
                    iv,
                    self.nscfnosym_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            nscfcalc.load_geometry_from = scfcalc
            nscfcalc.dependencies.append(scfcalc)
            nscfcalc.link_den_from = scfcalc
            self.sequence.append(nscfcalc)

    def _init_bse_sequence(self):
        scrs = self.get_sequence_calculation("screening")
        for nscf in self.get_sequence_calculation("nscfnosym"):
            param = nscf.input_variables[self.kgrids_input_variable_name]
            bse = SequenceCalculation(
                    "abinit",
                    self._get_bse_workdir(param),
                    self.bse_input_variables,
                    self.bse_calculation_parameters,
                    loglevel=self._loglevel,
                    )
            bse.load_kpts_from = nscf
            bse.load_geometry_from = nscf
            scr_workdir = self._get_screening_workdir(param)
            for scr in scrs:
                if scr.workdir == scr_workdir:
                    bse.link_scr_from = scr
                    bse.dependencies.append(scr)
                    break
            else:
                LookupError()
            bse.link_wfk_from = nscf
            bse.dependencies.append(nscf)
            self.sequence.append(bse)

    def _get_bse_specific_workdir(self, icalc):
        param = self.scf_kgrids[icalc]
        return self._get_bse_workdir(param)

    # not relevant here
    def _get_bse_specific_input_variables(self, *args, **kwargs):
        raise RuntimeError("Should not be called...")

    def _get_plot_curve_label(self, bsecalc, icalc):
        # need to get scf kgrid
        nscf = bsecalc.link_wfk_from
        kgrid = nscf.input_variables[self.kgrids_input_variable_name]
        if is_vector(kgrid, length=3):
            return r"$N_{k}=$" + f"{kgrid[0]}x{kgrid[1]}x{kgrid[2]}"
        return f"{self.kgrids_input_variable_name} #{icalc}"

    def _get_n_bse_calcs(self):
        return len(self.scf_kgrids)
