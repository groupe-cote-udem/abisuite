import asyncio

from .ddk_sequencer import DDKSequencer
from .nscf_sequencers import AbinitNSCFSequencer
from ..bases import SequenceCalculation
from ...post_processors import DielectricTensor
from ...routines import is_list_like


class AbinitOpticSequencer(DDKSequencer, AbinitNSCFSequencer):
    """Sequencer for an optic run.

    The sequence goes as follows:
      - GS SCF run
      - NSCF run with dense kmesh and lots of bands in IBZ
        using DEN from SCF run
      - Same NSCF calc but in complete BZ from WFK of first NSCF run
        (Full BZ, executed only if non-linear properties are required)
      - DDK
      - optic
    """

    _all_sequencer_prefixes = (
            "scf_", "nscf_", "nscffbz_", "ddk_", "optic_", "plot_", )
    _loggername = "AbinitOpticSequencer"

    def __init__(self, *args, **kwargs):
        DDKSequencer.__init__(self, *args, **kwargs)
        AbinitNSCFSequencer.__init__(self, *args, **kwargs)
        self._compute_non_linear_optical_response = None
        self._force_kptopt = False
        self._skip_nscffbz_calculation = False
        self._nscf_link_den_only = False

    @property
    def nscf_link_den_only(self) -> bool:
        """True if we ONLY link DEN fron SCF calc for NSCF calc.

        Normally we'd link DEN and WFK.
        """
        return self._nscf_link_den_only

    @nscf_link_den_only.setter
    def nscf_link_den_only(self, link) -> None:
        self._nscf_link_den_only = link

    @property
    def skip_nscffbz_calculation(self) -> bool:
        """If this property is True, we skip the 2nd nscf calculation."""
        return self._skip_nscffbz_calculation

    @skip_nscffbz_calculation.setter
    def skip_nscffbz_calculation(self, skip: bool) -> None:
        self._skip_nscffbz_calculation = skip

    @property
    def compute_non_linear_optical_response(self):
        """Set to True to compute non-linear properties."""
        if self._compute_non_linear_optical_response is not None:
            return self._compute_non_linear_optical_response
        raise ValueError(
                "Need to set 'compute_non_linear_optical_response' to True or "
                "False. This will decide if we ise TRS or not.")

    @compute_non_linear_optical_response.setter
    def compute_non_linear_optical_response(self, compute):
        self._compute_non_linear_optical_response = compute

    @property
    def force_kptopt(self):
        """Set to True to force kptopt throughout the sequence."""
        return self._force_kptopt

    @force_kptopt.setter
    def force_kptopt(self, force):
        self._force_kptopt = force

    @property
    def need_2nd_nscf(self):
        """Return True if we need the 2nd NSCF calculation in FBZ."""
        if self.skip_nscffbz_calculation:
            return False
        return self.force_kptopt or self.compute_non_linear_optical_response

    @property
    def nscffbz_input_variables(self):
        """Set the nscffbz input variables."""
        return self._nscffbz_input_variables

    @nscffbz_input_variables.setter
    def nscffbz_input_variables(self, input_vars):
        self._nscffbz_input_variables = input_vars.copy()

    @AbinitNSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, input_vars):
        """Set the nscf input variables."""
        # iscf must be set to -2
        # kptopt must be set to 1
        # make copy to not modify dict from user in case it is used elsewhere
        input_vars_copy = input_vars.copy()
        occopt = input_vars_copy.get("occopt", 1)
        if input_vars_copy.get("iscf", 1) > 0:
            self._set_input_var_to(
                    input_vars_copy, "iscf",
                    -3 if occopt in (0, 2) else -2)
        if self.force_kptopt:
            # only modify kptopt if it is not set
            if "kptopt" not in input_vars_copy:
                self._set_input_var_to(input_vars_copy, "kptopt", 1)
        else:
            # kptopt must be set to 1
            if input_vars_copy.get("kptopt", 1) != 1:
                self._set_input_var_to(input_vars_copy, "kptopt", 1)
        # remove irdden and irdwfk from input vars
        # this is done to prevent calculation linking to automatically link
        # files we don't want because second NSCF run requires DEN from SCF
        # run but wfk from first nscf run (not sure why?!?)
        # these variables will be automatically set when we link the files
        # we want in the init_sequence function.
        input_vars_copy.pop("irdden", None)
        input_vars_copy.pop("irdwfk", None)
        AbinitNSCFSequencer.nscf_input_variables.fset(self, input_vars_copy)
        # set nscffbz_input_variable at the same time
        if self.skip_nscffbz_calculation:
            return
        fbz = input_vars_copy.copy()
        if self.force_kptopt:
            # force a NSCF run with a given kptopt
            if "kptopt" not in fbz:
                raise ValueError("force_kptopt is True but no kptopt given.")
        else:
            # use wfk_task to generate WFK on full BZ
            fbz["optdriver"] = 8
            fbz["wfk_task"] = "wfk_fullbz"
        self.nscffbz_input_variables = fbz

    @DDKSequencer.ddk_input_variables.setter
    def ddk_input_variables(self, input_vars):
        """Set the ddk input variables."""
        # start from nscffbz input vars and update
        # kptopt should have been set to 3 from these
        if self.need_2nd_nscf:
            copy = self.nscffbz_input_variables.copy()
        else:
            copy = self.nscf_input_variables.copy()
        copy.update(input_vars)
        if not self.need_2nd_nscf:
            # we can skip dfpt code using this trick but it's buggy
            # for non-linear properties.
            # see discussion here (FG: 2021/10/25):
            # https://discourse.abinit.org/t/abinit-terminated-with-signal-9/3010
            copy["wfk_task"] = "wfk_ddk"
            # DDKSequencer.ddk_input_variables.fset(self, copy)
            copy["irdwfk"] = 1
            copy["optdriver"] = 8
            self._ddk_input_variables = copy
            return
        copy.pop("optdriver", None)
        copy.pop("wfk_task", None)
        # for optic run, set nstep to 1
        DDKSequencer.ddk_input_variables.fset(self, copy)
        nstep = self.ddk_input_variables.get("nstep", 30)
        if nstep != 1:
            self._set_input_var_to(
                    self.ddk_input_variables, "nstep", 1)
        # set nline to 0
        nline = self.ddk_input_variables.get("nline", 4)
        if nline != 0:
            self._set_input_var_to(
                    self.ddk_input_variables, "nline", 0)
        # set prtwf to 3 for size reduced ddk files for optic
        self._set_input_var_to(
                self.ddk_input_variables, "prtwf", 3)
        if self.force_kptopt:
            # set kptopt to whatever kptopt was set in the 2nd nscf run
            if self.need_2nd_nscf:
                kptopt = self.nscffbz_input_variables["kptopt"]
            else:
                kptopt = self.nscf_input_variables["kptopt"]
            self._set_input_var_to(
                self.ddk_input_variables, "kptopt", kptopt)
        else:
            # set to 2 if not doing non-linear stuff
            if not self.compute_non_linear_optical_response:
                # We can use wfk task instead here
                self.ddk_input_variables.update({
                    "optdriver": 8, "wfk_task": "wfk_ddk",
                    })
            else:
                self.ddk_input_variables["kptopt"] = 3

    @property
    def optic_input_variables(self):
        """Set the optic input variables."""
        return self._optic_input_variables

    @optic_input_variables.setter
    def optic_input_variables(self, input_vars):
        # check that we are consisten
        # if we ask for non lin comp, we should have fun the 2nd nscf run
        # with kptopt = 3
        for varname in ("num_nonlin_comp", "num_nonlin2_comp"):
            cnlor = self.compute_non_linear_optical_response
            if varname not in input_vars:
                # automatic 0
                continue
            if input_vars[varname] > 0 and not cnlor:
                raise ValueError(
                        "You're asking for a non linear comp but the wfk was "
                        "computed only on hald the FBZ using TRS (kptopt = 2)."
                        )
        self._optic_input_variables = input_vars

    def init_nscf_sequence(self):
        """Initialize the NSCF part of the sequence."""
        # do the same as usual but manually link the DEN and WFK files
        AbinitNSCFSequencer.init_nscf_sequence(self)
        nscf = self.get_sequence_calculation("nscf")
        nscf.link_den_from = self.scf_workdir
        if not self.nscf_link_den_only:
            nscf.link_wfk_from = self.scf_workdir

    def init_second_nscf_sequence(self):
        """Initialize the 2nd nscf part of the sequence."""
        if self.skip_nscffbz_calculation:
            return
        seq = SequenceCalculation(
                "abinit",
                self.nscffbz_workdir,
                self.nscffbz_input_variables,
                self.nscffbz_calculation_parameters,
                loglevel=self._loglevel)
        seq.load_geometry_from = self.scf_workdir
        seq.dependencies.append(self.get_sequence_calculation("scf"))
        seq.dependencies.append(self.get_sequence_calculation("nscf"))
        seq.link_den_from = self.scf_workdir
        seq.link_wfk_from = self.nscf_workdir
        self.sequence.append(seq)

    def init_ddk_sequence(self):
        """Initialize the ddk part of the sequence."""
        DDKSequencer.init_ddk_sequence(self)
        # change dependancy from SCF to last NSCF calc if needed
        ddk = self.get_sequence_calculation("ddk")
        if not is_list_like(ddk):
            ddk = [ddk]
        if not self.need_2nd_nscf:
            last_nscf = self.get_sequence_calculation("nscf")
        else:
            last_nscf = self.get_sequence_calculation("nscffbz")
        for ddkcalc in ddk:
            ddkcalc.dependencies.clear()
            ddkcalc.dependencies.append(last_nscf)
            ddkcalc.load_geometry_from = last_nscf
            ddkcalc.link_wfk_from = last_nscf
            ddkcalc.load_kpts_from = last_nscf

    async def init_sequence(self):
        """Initialize the whole sequence."""
        # make GS run and first nscf fun
        await AbinitNSCFSequencer.init_sequence(self)
        # add second nscf sequence calculation on FBZ if needed
        if self.need_2nd_nscf:
            self.init_second_nscf_sequence()
        self.init_ddk_sequence()
        self.init_optic_sequence()

    def init_optic_sequence(self):
        """Initialize the optic part of the sequence."""
        seq = SequenceCalculation(
                    "abinit_optic", self.optic_workdir,
                    self.optic_input_variables,
                    self.optic_calculation_parameters,
                    loglevel=self._loglevel)
        # the optic run has the DDK run as dependencies and the last nscf run
        # (to get the WFK in the FBZ)
        ddk = self.get_sequence_calculation("ddk")
        if not is_list_like(ddk):
            ddk = [ddk]
        seq.dependencies += ddk
        if self.need_2nd_nscf:
            nscffbz = self.get_sequence_calculation("nscffbz")
            seq.dependencies += [nscffbz]
        else:
            seq.dependencies += [self.get_sequence_calculation("nscf")]
        self.sequence.append(seq)

    async def plot_dielectric_tensor(self):
        """Plot the dielectric tensor."""
        # first check if we need to plot the dielectric tensor at all
        # check in the optic input variables if we need
        lincomp = self.optic_input_variables.get("num_lin_comp", 0)
        if not lincomp:
            self._logger.info("Don't need to plot dielectric tensor.")
            return
        diel = await DielectricTensor.from_calculation(
                self.optic_workdir, loglevel=self._loglevel)
        multi_diel_plot = diel.get_plot(multiplot=True)
        diel_plots = diel.get_plot(multiplot=False)
        all_plots = list(diel_plots) + [multi_diel_plot]
        # apply other properties to plot
        for param, value in self.plot_calculation_parameters.items():
            if param in ("show", "save", "save_plot", "show_legend_on"):
                # postfix parameters
                continue
            for plot in all_plots:
                setattr(plot, param, value)
        await asyncio.gather(
                self._post_process_plot(
                    multi_diel_plot, name_extension="combined"),
                self._post_process_plot(
                    diel_plots[0], name_extension="real_part"),
                self._post_process_plot(
                    diel_plots[1], name_extension="imag_part"),
                )

    async def post_sequence(self, *args, **kwargs):
        """Execute the post sequence stuff."""
        await super().post_sequence(*args, **kwargs)
        # plot all the optic data
        # for now only the dielectric tensor is implemented
        await self.plot_dielectric_tensor()
