from .nscf_sequencers import AbinitPositiveKpointsNSCFSequencer
from ..fermi_surface_sequencer import BaseFermiSurfaceSequencer


class AbinitFermiSurfaceSequencer(
        BaseFermiSurfaceSequencer, AbinitPositiveKpointsNSCFSequencer):
    """An abinit sequencer to plot fermi surfaces."""

    _all_sequencer_prefixes = ("scf_", "nscf_", "plot_", )
    _loggername = "AbinitFermiSurfaceSequencer"

    def __init__(self, *args, **kwargs):
        AbinitPositiveKpointsNSCFSequencer.__init__(self, *args, **kwargs)
        BaseFermiSurfaceSequencer.__init__(self, "abinit", *args, **kwargs)

    async def post_sequence(self):
        """Execute post sequence stuff."""
        await BaseFermiSurfaceSequencer.post_sequence(self)

    def _get_nscf_k_points(self):
        return {"ngkpt": [n + 1 for n in self.nscf_kgrid],
                "kptopt": 3,
                "prtfsurf": 1, "iscf": -3,
                }
