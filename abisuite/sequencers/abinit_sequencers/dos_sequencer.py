from typing import Any

from .scf_sequencer import AbinitSCFSequencer
from ..dos_sequencer import DOSSequencer
from ...constants import HARTREE_TO_EV
from ...plotters import Plot
from ...post_processors import DOSPlot, ProjectedDOSPlot


class AbinitDOSSequencer(DOSSequencer, AbinitSCFSequencer):
    """Sequencer class for a DOS calculation done by the Abinit software.

    This Sequence is basically a SCF calculation + a DOS calculation based
    on the SCF calculation. The DOS calculation is an NSCF calculation
    with a denser kpt grid.

    A DOS plot is also generated once the sequence is completed.
    """

    _all_sequencer_prefixes = DOSSequencer._all_sequencer_prefixes
    _loggername = "AbinitDOSSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        DOSSequencer.__init__(self, "abinit", *args, **kwargs)

    @property
    def dos_input_variables(self) -> dict:
        """Set the dos input variables."""
        return DOSSequencer.dos_input_variables.fget(self)

    @dos_input_variables.setter
    def dos_input_variables(self, dos: dict[str, Any]) -> None:
        if not isinstance(dos, dict):
            raise TypeError(f"dos input vars should be a dict. Got '{dos}'.")
        if dos.get("prtdos", 0) == 0:
            raise ValueError("'prtdos' should be set > 0.")
        # make copy to make sure we don't update something we shouldn't
        dos = dos.copy()
        dos.update(self.dos_fine_kpoint_grid_variables)
        DOSSequencer.dos_input_variables.__set__(
                self, dos)

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await super(DOSSequencer, self).init_sequence()
        self.init_dos_sequence()

    def init_dos_sequence(self):
        """Initialize the DOS part of the sequence."""
        scfcalc = self.get_sequence_calculation("scf")
        self._init_dos_sequence_against_gs_calc(scfcalc)

    async def plot_dos(self) -> Plot:
        """Plot the DOS.

        If projected DOS_AT files are present, those
        projected dos will also be plot.

        Returns
        -------
        Plot object: If only the DOS is plot
        tuple: (DOS Plot object, list of Projected DOS plots).
        """
        dos_plot = await DOSPlot.from_calculation(
                self.dos_workdir, loglevel=self._loglevel)
        # convert everything to eV. units are read in Ha
        dos_plot.energies *= HARTREE_TO_EV
        dos_plot.dos /= HARTREE_TO_EV
        dos_plot.fermi_energy *= HARTREE_TO_EV
        plot = dos_plot.get_plot(
                vertical=True, line_at_fermi=True, xlabel="DOS",
                xunits="electrons / eV / cell", ylabel="Energy", yunits="eV",
                title=(r"Density of States $E_F=$" +
                       f"{dos_plot.fermi_energy:.2f} eV"),
                linewidth=2, color="k")
        await self._post_process_plot(plot, name_extension="dos")
        self._dos_plot = plot
        # check if there are projected dos files. if yes, plot them
        self._projected_dos_plots = []
        try:
            projected_dosses = await ProjectedDOSPlot.from_calculation(
                    self.dos_workdir, loglevel=self._loglevel)
            for projected_dos in projected_dosses:
                projected_dos.energies *= HARTREE_TO_EV
                atom = projected_dos.atom_number
                for idx, dos in projected_dos.projected_dos.items():
                    projected_dos.projected_dos[idx] = dos / HARTREE_TO_EV
                projected_dos.fermi_energy *= HARTREE_TO_EV
                plots, multi_plot = projected_dos.get_plot(
                        vertical=True, line_at_fermi=True,
                        xlabel="projected DOS",
                        xunits="electrons / eV / cell",
                        ylabel="Energy", yunits="eV",
                        title=(r"Density of States $E_F=$" +
                               f"{projected_dos.fermi_energy:.2f} eV"),
                        linewidth=2)
                for l_idx, plot in enumerate(plots):
                    await self._post_process_plot(
                            plot,
                            name_extension=(
                                "projected_dos_atom"
                                f"{projected_dos.atom_number}_l={l_idx}"))
                    self._projected_dos_plots.append(plot)
                await self._post_process_plot(
                        multi_plot, name_extension=(
                            f"projected_dos_atom{atom}_multi"))
        except (FileNotFoundError, ValueError):
            return self._dos_plot
        else:
            return self._dos_plot, self._projected_dos_plots
