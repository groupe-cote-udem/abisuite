from ...routines import is_dict


def kpts_defined(input_vars):
    """Tells if kpts were defined in a dict of input vars.

    Parameters
    ----------
    input_vars: dict
        The dict of input vars to analyse.

    Raises
    ------
    TypeError: if 'input_vars' is not a dict.

    Returns
    -------
    bool: True if kpts were defined. False otherwise.
    """
    if not is_dict(input_vars):
        raise TypeError("'input_vars' should be a dict.")
    for var in ("ngkpt", "kptrlatt", "kptbounds"):
        if var in input_vars:
            return True
    return False
