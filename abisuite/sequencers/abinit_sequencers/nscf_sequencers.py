from .routines import kpts_defined
from .scf_sequencer import AbinitSCFSequencer
from ..nscf_sequencers import NSCFSequencer, PositiveKpointsNSCFSequencer


class AbinitNSCFSequencer(NSCFSequencer, AbinitSCFSequencer):
    """NSCF sequencer for abinit calculations."""

    _all_sequencer_prefixes = ("scf_", "nscf_", )
    _loggername = "AbinitNSCFSequencer"

    def __init__(self, *args, **kwargs):
        AbinitSCFSequencer.__init__(self, *args, **kwargs)
        NSCFSequencer.__init__(self, "abinit", *args, **kwargs)
        self._nscf_link_density_from_scf = False
        self._nscf_load_geometry_from_scf = False

    @NSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, ivars):
        """Set the nscf input variables."""
        iscf = ivars.get("iscf", 7)
        if iscf > 0:
            # nscf we want to default iscf to -2 but user
            # could choose to set it to -3 for instance
            self._set_input_var_to(ivars, "iscf", -2)
        self._nscf_input_variables = ivars

    @property
    def nscf_link_density_from_scf(self):
        """Tells if we link or not the density file from SCF calculation.

        Returns
        -------
        bool: True if we link, False otherwise.
        """
        return self._nscf_link_density_from_scf

    @nscf_link_density_from_scf.setter
    def nscf_link_density_from_scf(self, link):
        if link not in (True, False, 1, 0):
            raise ValueError(link)
        self._nscf_link_density_from_scf = link

    @property
    def nscf_load_geometry_from_scf(self):
        """Tells if we load or not the atomic geometry file from SCF calc.

        Returns
        -------
        bool: True if we load, False otherwise.
        """
        return self._nscf_load_geometry_from_scf

    @nscf_load_geometry_from_scf.setter
    def nscf_load_geometry_from_scf(self, load):
        if load not in (True, False, 1, 0):
            raise ValueError(load)
        self._nscf_load_geometry_from_scf = load

    async def init_sequence(self):
        """Initialize the whole sequence."""
        await NSCFSequencer.init_sequence(self)

    def init_nscf_sequence(self):
        """Initialize the nscf part of the sequence."""
        NSCFSequencer.init_nscf_sequence(self)
        nscfcalc = self.get_sequence_calculation("nscf")
        scfcalc = self.get_sequence_calculation("scf")
        # link kpts if kpts variables are not defined
        if not kpts_defined(nscfcalc.input_variables):
            nscfcalc.load_kpts_from = scfcalc
        nscfcalc.dependencies.append(scfcalc)
        if nscfcalc.input_variables.get("irdden", 1) == 1 or (
                self.nscf_link_density_from_scf):
            nscfcalc.link_density_from = scfcalc
        if self.nscf_load_geometry_from_scf:
            nscfcalc.load_geometry_from = scfcalc


class AbinitPositiveKpointsNSCFSequencer(
        PositiveKpointsNSCFSequencer, AbinitNSCFSequencer):
    """An abinit nscf sequencer for a grid of kpts with only pos. coords."""

    _all_sequencer_prefixes = (
            PositiveKpointsNSCFSequencer._all_sequencer_prefixes)
    _loggername = "AbinitPositiveKpointsNSCFSequencer"

    def __init__(self, *args, **kwargs):
        PositiveKpointsNSCFSequencer.__init__(self, "abinit", *args, **kwargs)
        AbinitNSCFSequencer.__init__(self, *args, **kwargs)

    @PositiveKpointsNSCFSequencer.nscf_input_variables.setter
    def nscf_input_variables(self, input_vars):
        """Set the nscf input variables."""
        PositiveKpointsNSCFSequencer.nscf_input_variables.fset(
                self, input_vars)
        self.nscf_input_variables.update(self._get_nscf_k_points())
        # overwrite shifting (disable it)
        self._set_input_var_to(self.nscf_input_variables, "nshiftk", 1)
        self._set_input_var_to(
                self.nscf_input_variables, "shiftk", [[0.0, 0.0, 0.0]])
        AbinitNSCFSequencer.nscf_input_variables.fset(
                self, self.nscf_input_variables)

    def _get_nscf_k_points(self, extra=0):
        kpts = []
        for i in range(self.nscf_kgrid[0] + extra):
            for j in range(self.nscf_kgrid[1] + extra):
                for k in range(self.nscf_kgrid[2] + extra):
                    kpts.append([i / self.nscf_kgrid[0],
                                 j / self.nscf_kgrid[1],
                                 k / self.nscf_kgrid[2],
                                 ])
        return {"kpt": kpts, "nkpt": len(kpts), "kptopt": 0}
