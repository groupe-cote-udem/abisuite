import abc

from async_property import async_property

from .exceptions import SequenceNotCompletedError
from .scf_sequencer import SCFSequencer


class RelaxationSequencer(SCFSequencer):
    """Base class for any relaxation sequencers."""

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        self._relax_atoms = None
        self._relax_cell = None
        # use special flag for sequence completed to return True
        # since we add calculations only once every calcs
        # are finished
        self._truly_completed = False

    @property
    def relax_atoms(self):
        """Set to True to relax atomic positions."""
        if self._relax_atoms is not None:
            return self._relax_atoms
        raise ValueError("Need to set 'relax_atoms'.")

    @relax_atoms.setter
    def relax_atoms(self, relax):
        self._relax_atoms = relax

    @property
    def relax_cell(self):
        """Set to True to relax cell parameters."""
        if self._relax_cell is not None:
            return self._relax_cell
        raise ValueError("Need to set 'relax_cell'.")

    @relax_cell.setter
    def relax_cell(self, relax):
        self._relax_cell = relax

    @async_property
    async def relaxed_geometry_variables(self):
        """Return the relaxed geometry variables."""
        if not await self.sequence_completed:
            raise SequenceNotCompletedError(self)
        return await self._get_relaxed_geometry_variables()

    async def is_sequence_completed(self):
        """Return True if the sequence has successfully completed."""
        if not self.sequence:
            await self.init_sequence()
        if not self._truly_completed:
            return False
        return await SCFSequencer.is_sequence_completed(self)

    @abc.abstractmethod
    async def _get_relaxed_geometry_variables(self):
        pass
