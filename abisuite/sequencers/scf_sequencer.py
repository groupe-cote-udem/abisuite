import abc
import os

from async_property import async_property

from .bases import BaseSequencer, SequenceCalculation
from ..constants import HARTREE_TO_mEV, RYDBERG_TO_mEV
from ..exceptions import DevError
from ..plotters import Plot
from ..routines import is_list_like, is_scalar_or_str, is_vector


class SCFSequencer(BaseSequencer):
    """A sequencer which only holds a scf calculation.

    Useful as a base class for other more complex sequencers.
    """

    _all_sequencer_prefixes = ("scf_", )
    _loggername = "SCFSequencer"

    def __init__(self, *args, **kwargs):
        BaseSequencer.__init__(self, *args, **kwargs)
        if "scf_" not in self._all_sequencer_prefixes:
            raise DevError("'scf_' should be in '_all_sequencer_prefixes'.")

    # TODO: get rid of this property (FG 2021/10/27)
    @property
    def calctype(self):
        """Return the calculation's calctype."""
        if self.software == "qe":
            return "qe_pw"
        elif self.software == "abinit":
            return "abinit"
        else:
            raise NotImplementedError(self.software)

    @property
    def scf_input_variables(self):
        """Define the scf input variables."""
        return self._scf_input_variables

    @scf_input_variables.setter
    def scf_input_variables(self, input_vars):
        if not isinstance(input_vars, dict):
            raise TypeError(
                    f"Expected a dict for scf input vars got instead: "
                    f"{input_vars}")
        if self.software == "qe":
            if input_vars.get("calculation", "scf") != "scf":
                self._logger.info("Overwriting 'calculation' var for 'scf'.")
                input_vars["calculation"] = "scf"
        self._scf_input_variables = input_vars

    async def init_sequence(self, *args, **kwargs):
        """Initialize the whole sequence."""
        self._logger.debug("Initializing sequence.")
        await BaseSequencer.init_sequence(self, *args, **kwargs)
        self.initialize_scf_sequence(*args, **kwargs)

    def initialize_scf_sequence(self, *args, **kwargs):
        """Initialize SCF part of the sequence.

        In the case of a single SCF sequencer, only this method
        might be executed.
        """
        self._logger.debug("Initializing SCF part of the sequence.")
        scfcalc = SequenceCalculation(
                self.calctype, self.scf_workdir, self.scf_input_variables,
                self.scf_calculation_parameters, loglevel=self._loglevel)
        self.sequence.append(scfcalc)


class SCFComparatorSequencer(SCFSequencer):
    """General scf sequencer that compares many scf calculations.

    Useful for comparing against one or many parameters
    that are changed throughout the calculations.

    SCF workdir are differentiated by a single number only.
    """

    _all_sequences_prefixed = ("scf_", "plot_", )

    def __init__(self, *args, **kwargs):
        SCFSequencer.__init__(self, *args, **kwargs)
        # allow to load geometry from somewhere
        self._scf_load_geometry_from = None
        self._scf_specific_input_variables = None

    @property
    def scf_load_geometry_from(self):
        """Define where to load geometry if needed."""
        return self._scf_load_geometry_from

    @scf_load_geometry_from.setter
    def scf_load_geometry_from(self, path):
        self._scf_load_geometry_from = path

    @property
    def natom_input_variable_name(self):
        """Define the input variable name that counts number of atoms."""
        if self.software == "qe":
            return "nat"
        else:  # abinit
            return "natom"

    @property
    def n_scf_calculations(self):
        """Return the number of scf calculations."""
        return len(self.scf_specific_input_variables)

    @property
    def scf_specific_input_variables(self):
        """Define Specific input variables for each different calculations."""
        if self._scf_specific_input_variables is None:
            raise ValueError("Need to set 'scf_specific_input_variables'.")
        return self._scf_specific_input_variables

    @scf_specific_input_variables.setter
    def scf_specific_input_variables(self, spec):
        if not is_list_like(spec):
            raise TypeError(
                    f"Expected list-like but got: '{spec}'.")
        for item in spec:
            if not isinstance(item, dict):
                raise TypeError(
                        f"spec_inputs must be a list of dicts. "
                        f"Got: '{spec}'.")
        self._scf_specific_input_variables = spec

    async def init_sequence(self, *args, **kwargs):
        """Initialize the whole sequence."""
        # override master class init sequence
        await self.init_scf_sequence()

    async def init_scf_sequence(self):
        """Initialize the scf part of the sequence."""
        for iparam in range(self.n_scf_calculations):
            scfcalc = SequenceCalculation(
                    self.calctype,
                    self._get_scf_workdir_single_calculation(iparam),
                    self._get_scf_input_variables_single_calculation(iparam),
                    self.scf_calculation_parameters, loglevel=self._loglevel)
            if self.scf_load_geometry_from is not None:
                scfcalc.load_geometry_from = self.scf_load_geometry_from
            self.sequence.append(scfcalc)

    async def post_sequence(self):
        """Execute the post sequence tasks."""
        await self.plot_comparision()

    @abc.abstractmethod
    async def plot_comparision(self):
        """Make the comparison plots."""
        pass

    def _get_scf_input_variables_single_calculation(self, icalc):
        """Return the complete set of input variables.

        For a single calculation according to its index.
        """
        if self.scf_input_variables is None:
            return None
        # common input vars
        input_vars_copy = self.scf_input_variables.copy()
        input_vars_copy.update(
                self.scf_specific_input_variables[icalc])
        return input_vars_copy

    def _get_scf_workdir_single_calculation(self, icalc):
        """Return the path for a specific scf calculation from its index.

        If the changing variable is a single scalar or a string, the extension
        is a composition of the varname and its value. Otherwise, if
        it's just a single variable, it's just
        the varname and the calculation index.

        Otherwise, it's just the calculation index.
        """
        is_single_var = True
        is_scalar_var = True
        for variables_dict in self.scf_specific_input_variables:
            if len(variables_dict) == 1:
                varname = list(variables_dict.keys())[0]
                varvalue = variables_dict[varname]
                if not is_scalar_or_str(varvalue):
                    is_scalar_var = False
            else:
                is_single_var = False
        ext = "_"
        ivars = self.scf_specific_input_variables[icalc]
        if is_single_var:
            this_varname = list(ivars.keys())[0]
            ext += this_varname
        else:
            return self.scf_workdir + f"_{icalc + 1}"
        if is_scalar_var:
            # don't add _ since we use _ to split workdirs sometimes
            this_value = ivars[this_varname]
            ext += str(this_value)
            return self.scf_workdir + ext
        return self.scf_workdir + ext + f"{icalc + 1}"


class SCFConvergenceSequencer(SCFComparatorSequencer):
    """General SCF sequencer for convergence studies vs etotal."""

    _all_sequencer_prefixes = ("scf_", "plot_", )
    # this is the scf_{} parameter to loop on
    _parameter_to_converge = None

    def __init__(self, *args, **kwargs):
        SCFComparatorSequencer.__init__(self, *args, **kwargs)
        # set the convergence parameter
        if self._parameter_to_converge is None:
            raise DevError("Need to set '_parameter_to_converge'.")
        setattr(self, "_scf_" + self._parameter_to_converge, None)
        self._scf_convergence_criterion = None  # in meV/atom
        self._scf_converged_parameter = None

    @async_property
    async def scf_converged_parameter(self):
        """Return the scf converged parameters."""
        if not await self.sequence_completed:
            raise ValueError("Need to complete sequence first.")
        if self._scf_converged_parameter is None:
            await self.compute_convergence()
        return self._scf_converged_parameter

    @property
    def scf_convergence_criterion(self):
        """Define the scf convergence criterion."""
        if self._scf_convergence_criterion is None:
            raise ValueError(
                    "Need to set 'scf_convergence_criterion'. A good criterion"
                    " would be 1meV/atom.")
        return self._scf_convergence_criterion

    @scf_convergence_criterion.setter
    def scf_convergence_criterion(self, criterion):
        self._scf_convergence_criterion = criterion

    @property
    def scf_specific_input_variables(self):
        """Define the specific input variables for each scf calculations."""
        try:
            return SCFComparatorSequencer.scf_specific_input_variables.fget(
                    self)
        except ValueError:
            self.scf_specific_input_variables = [{}] * len(
                    self._parameters_to_converge)
            return self.scf_specific_input_variables

    @scf_specific_input_variables.setter
    def scf_specific_input_variables(self, spec):
        # do the same as mother class but add a check
        SCFComparatorSequencer.scf_specific_input_variables.fset(
                self, spec)
        if len(spec) != len(self._parameters_to_converge):
            raise ValueError(
                    "Specific input variables must be same length as number of"
                    " q-points.")
        self._scf_specific_input_variables = spec

    @property
    def _parameters_to_converge(self):
        attr_name = "scf_" + self._parameter_to_converge
        try:
            return getattr(self, attr_name)
        except AttributeError:
            raise DevError(f"Need to define property '{attr_name}'.")

    @property
    def _parameter_to_converge_input_variable_name(self):
        return getattr(
                self, self._parameter_to_converge + "_input_variable_name")

    @property
    def _parameter_to_converge_units(self):
        return getattr(
                self, self._parameter_to_converge + "_units")

    @property
    def etotal_conversion_factor(self):
        """Define the conversion factor for the total energy (to get meV)."""
        if self.software == "qe":
            return RYDBERG_TO_mEV
        else:  # abinit
            return HARTREE_TO_mEV

    async def compute_convergence(self, rootdir=None):
        """Compute the convergence and establish a converged parameter.

        Parameters
        ----------
        rootdir: str, optional
            If None, rootdir is taken to be the scf_workdir.
            Otherwise, specifies the rootdir that contains all the calculations
            that were used to test the convergence of parameters.

        Returns
        -------
        xdata: the list of parameters to converge
        deltas: the variation of total energy w.r.t. the highest parameter
        etots: the total energy for each parameter.

        Notes
        -----
        The 'highest' parameter means the last one given as input.
        """
        if rootdir is None:
            rootdir = self.scf_workdir
        xdata = self._parameters_to_converge
        etots = []
        # gather all etotals
        for seq_calc in self.sequence:
            # if not os.path.samefile(
            #         os.path.dirname(seq_calc.workdir),
            #         rootdir):
            #     continue
            # the following if statement might break on Windows
            # (but who cares anyway?!?) or perhaps with symlinks
            # FG: 2021/04/19
            if rootdir not in seq_calc.workdir:
                continue
            async with await seq_calc.calculation_directory as calcdir:
                async with await calcdir.log_file as log:
                    etots.append(log.etot)
        # convert data
        # get number of atom from input file not from input variables
        # this is to cover the case where we load geometry from another calc
        nat = (await self.sequence[0].launcher).input_variables.get(
                self.natom_input_variable_name).value
        etots = [x * self.etotal_conversion_factor / nat for x in etots]
        # compute deltas
        deltas = [abs(etot - etots[-1]) for etot in etots[:-1]]
        # the converged criterion is the lowest parameter such that delta is
        # lower than the criterion
        for x, delta in zip(xdata[:-1], deltas):
            if delta > self.scf_convergence_criterion:
                continue
            self._scf_converged_parameter = x
            break
        return xdata, deltas, etots

    async def plot_comparision(self):
        """Make the comparison plots.

        Actually this method is only an alias of the
        :meth:`plot_convergence()
        <abisuite.sequencers.scf_sequencer.SCFConvergenceSequencer
        .plot_convergence>` method.
        """
        return await self.plot_convergence()

    async def plot_convergence(self):
        """Make the convergence plots.

        These are plots of Etotal [meV/atom]
        vs the parameter to converge.

        A twinx axis is set for the Delta compared to the last ecut.
        """
        self._logger.info("Plotting convergence.")
        # make the plot
        plot = Plot(loglevel=self._loglevel)
        xdata = self._add_xdata_to_plot(plot)
        _, deltas, etots = await self.compute_convergence(self.scf_workdir)
        plot.add_curve(
                xdata, etots, color="b", linewidth=2,
                linestyle="-",
                label=r"$E_{tot}$", marker="s", markersize=5,
                markerfacecolor="b",)
        plot.add_curve(
                xdata[:-1], deltas, color="r",
                linewidth=2,
                linestyle="-", marker="o", markersize=5, markerfacecolor="r",
                label=r"$\Delta E_{tot}$",
                semilogy=True, twinx=True)
        # criterion threshold
        plot.add_hline(
                self.scf_convergence_criterion, twinx=True, color="k",
                linestyle="--",
                label=(
                    r"$\Delta E_{tot}=$" +
                    f"{self.scf_convergence_criterion}meV/at"),
                linewidth=2)
        plot.grid = self.plot_calculation_parameters.get("grid", True)
        plot.xlabel = f"{self._parameter_to_converge_input_variable_name}"
        if self._parameter_to_converge_units is not None:
            plot.xlabel += " [" + self._parameter_to_converge_units + "]"
        plot.ylabel = r"$E_{tot}$ [meV/at]"
        plot.ylabel_twinx = r"$\Delta E_{tot}$ [meV/at]"

        # show and save
        await self._post_process_plot(plot)

    def _add_xtick_labels_to_convergence_plot(self, plot):
        raise DevError(
                "xdata is list-like: we need to implement the xtick labels.")

    def _add_xdata_to_plot(self, plot):
        xdata = self._parameters_to_converge
        # if data is list like, we need to give different xticks
        if is_list_like(self._parameters_to_converge[0]):
            xdata = list(range(len(xdata)))
            self._add_xtick_labels_to_convergence_plot(plot)
        elif isinstance(self._parameters_to_converge[0], dict):
            xdata = list(range(len(xdata)))
            self._add_xtick_labels_to_convergence_plot(plot)
        return xdata

    def _convert_param_to_str(self, param, icalc=None):
        if is_scalar_or_str(param):
            return str(param)
        elif is_vector(param):
            return "_".join([str(x) for x in param])
        if icalc is None:
            raise RuntimeError(
                    f"Could not convert param '{param}' to a convenient str."
                    " And icalc is not stated.")
        return str(icalc)

    def _get_scf_input_variables_single_calculation(self, icalc):
        """Return the input variables for a single calc from its index."""
        cls = SCFComparatorSequencer
        func = cls._get_scf_input_variables_single_calculation
        # add specific parameters
        input_vars_copy = func(self, icalc)
        param = self._parameters_to_converge[icalc]
        input_vars_copy.update(
                {self._parameter_to_converge_input_variable_name: param})
        return input_vars_copy

    def _get_scf_workdir_single_calculation(self, icalc, *args, **kwargs):
        """Return the path for a scf calculation according to its index."""
        param = self._parameters_to_converge[icalc]
        return self._get_workdir_from_converging_param(param, *args, **kwargs)

    # TODO: use this method in the future (FG: 2021/11/08)
    def _get_workdir_from_converging_param(
            self, param, prefix="scf",
            use_parameter_subdirs=True,
            ):
        """Get the workdir from the changing parameter.

        Parameters
        ----------
        param: The changing parameter's value.
        prefix: str, optional
            The sequence prefix that we want to get the workdir from.
        use_parameter_subdirs: bool, optional
            If True, an extra subdir is inserted between the workdir
            set for this prefix and the actual individual runs.
        """
        icalc = self._parameters_to_converge.index(param)
        if not use_parameter_subdirs:
            return os.path.join(
                getattr(self, f"{prefix.rstrip('_')}_workdir"),
                (self._parameter_to_converge_input_variable_name + "_" +
                 self._convert_param_to_str(param, icalc)))
        else:
            return os.path.join(
                getattr(self, f"{prefix.rstrip('_')}_workdir"),
                self._parameter_to_converge_input_variable_name,
                (self._parameter_to_converge_input_variable_name + "_" +
                 self._convert_param_to_str(param, icalc)))
