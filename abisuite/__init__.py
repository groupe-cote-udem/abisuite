import os


__PWD__ = os.getcwd()
__USER_DATABASE__ = None
try:
    from peewee import SqliteDatabase
    # The user's database object
    __USER_DATABASE__ = SqliteDatabase(None)
except (ImportError, ModuleNotFoundError):
    import warnings
    warnings.warn(
            "'peewee' is not available. Database module is not enabled.\n"
            "You can enable it by installing peewee: "
            "'python3 -m pip install peewee'."
            )

# LOAD CONFIG FILE FIRST
from .config import ConfigFileParser
USER_CONFIG = ConfigFileParser()
__USER_CONFIG__ = USER_CONFIG

# Define implementation restrictions
__IMPLEMENTED_QUEUING_SYSTEMS__ = (
                "local", "grid_engine", "pbs_professional", "slurm", "torque",
                )
__IMPLEMENTED_CALCTYPES__ = (
        "abinit", "abinit_anaddb", "abinit_cut3d", "abinit_mrgddb",
        "abinit_optic", "qe_dos",
        "qe_dynmat", "qe_epsilon", "qe_epw", "qe_fs",
        "qe_kpoints", "qe_ld1", "qe_matdyn",
        "qe_ph", "qe_pp", "qe_projwfc", "qe_pw", "qe_pw2wannier90", "qe_q2r",
        "wannier90",
        )

from .system import Queue
from ._version import __version__


SYSTEM_QUEUE = Queue()
__SYSTEM_QUEUE__ = SYSTEM_QUEUE
