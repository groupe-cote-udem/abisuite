from typing import Sequence

from ..bases import BaseUtility
from ..handlers import CalculationDirectory
from ..routines import is_dict, is_list_like, is_str


class CalculationsList(BaseUtility, list):
    """A simple calculation list utility for calculculations."""

    _loggername = "CalculationsList"

    def __init__(self, list_of_obj, *args, **kwargs):
        """Init method.

        Parameters
        ----------
        list_of_obj: list-like
            The initial list of calculations.
        """
        BaseUtility.__init__(self, *args, **kwargs)
        if not is_list_like(list_of_obj):
            raise TypeError(list_of_obj)
        list.__init__(self, list(self._check(list_of_obj)))

    def __add__(self, obj):
        if not is_list_like(obj):
            return TypeError(f"Cannot add non-list objects like '{obj}'.")
        for item in obj:
            self.append(item)

    # def __iter__(self):
    #     for item in self._container:
    #         yield item

    def __repr__(self):
        str_ = "< CalculationsList :\n"
        for calc in self:
            if is_dict(calc):
                calc = calc["calculation_directory"]
            str_ += f"    - '{calc.path}'\n"
        return str_ + ">"

    def append(self, calc):
        """Appends a calculation to the list.

        Parameters
        ----------
        calc: str or CalculationDirectory instance
            The calculation to append.
        """
        super().append(self._check(calc))

    def _check(
            self,
            calc: Sequence[CalculationDirectory | dict] | CalculationDirectory,
            recursive: bool = True) -> None:
        """Checks that the given (list of) item(s) ok.

        Check if it is either a str
        or a CalculationDirectory instance or dict info.

        Parameters
        ----------
        calc: obj
            The item to check out.
        recursive: bool, optional
            If True, if a list of item is given, checks that all items
            it in are good.

        Returns
        -------
        What was given in argument if everything is good.
        """
        if is_list_like(calc) and recursive:
            for item in calc:
                self._check(item, recursive=False)
            return calc
        if is_str(calc):
            return calc
        if isinstance(calc, CalculationDirectory):
            return calc
        if is_dict(calc):
            return calc
        raise TypeError(calc)
