import os

from .bases import BaseAbisuiteSubScript
from .routines import walltime_to_str
from .utils import CalculationsList
from .. import __USER_CONFIG__, __USER_DATABASE__
from ..colors import Colors
from ..databases import DBCalculation
from ..handlers import CalculationDirectory
from ..routines import full_abspath
from ..utils import TerminalTable


class AbisuiteDBScript(BaseAbisuiteSubScript):
    """Executes the 'db' succommand of the abisuite script."""

    _loggername = "AbisuiteDBScript"
    _subscript_color = "green"
    _subscript_name = "db"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = None

    def add_calculations_to_database(self):
        """Adds calculations to the database."""
        for calc_info in self.calculations:
            calculation = calc_info["calculation_directory"]
            if not calculation.is_in_database:
                print(Colors.bold_text("ADDDING") +
                      f"calculation: '{calculation.path}'.")
                calculation.add_to_database()
                self.success("database insertion.")
            else:
                print(Colors.bold_text("Already in database:") +
                      f"'{calculation.path}'.")

    def clean_database(self):
        """Cleanses the database from calculations that don't exist anymore."""
        print(Colors.bold_text("CLEANING") + f" database: '{self.db.db}'.")
        DBCalculation.clean_database()
        self.success("cleaning.")

    def delete_calculations_from_database(self, force=False):
        """As the name suggests, deletes calculations from the database.

        Parameters
        ----------
        force: bool, optional
            If False, a database integrity check is done before
            deletion. Otherwise, we force deletion even though the database
            contains errors.
        """
        for calc_info in self.calculations:
            calc = calc_info["calculation_directory"]
            print(Colors.bold_text("DELETING from DB:") +
                  f" '{calc.path}'.")
            calc.remove_from_database()
            self.successfull("deletion.")

    def get(self, get_id=False, get_status=False, get_monitored=False,
            get_packaged=False, get_project=False, get_walltime=False,
            ):
        """Prints out requested data from database in a nicely formatted table.

        Parameters
        ----------
        get_id: bool, optional
            If True, the ids of calculations are printed.
        get_status: bool, optional
            If True, the status of calculations are printed.
        get_monitored: bool, optional
            If True, the 'monitored' attribute of calculations are printed.
        get_packaged: bool, optional
            If True, the 'packaged' attribute of calculations are printed.
        get_project: bool, optional
            If True, the 'project' attribute of calculations are printed.
        get_walltime: bool, optional
            If True, the walltime is printed if available.
        """
        if not any((get_id, get_status, get_monitored,
                    get_packaged, get_project, get_walltime)):
            return
        column_names = ["Calculation(s)"]
        column_alignments = ["left"]
        if get_id:
            column_names.append("ID")
            column_alignments.append("right")
        if get_monitored:
            column_names.append("Monitored")
            column_alignments.append("right")
        if get_packaged:
            column_names.append("Packaged")
            column_alignments.append("right")
        if get_project:
            column_names.append("Project")
            column_alignments.append("right")
        if get_walltime:
            column_names.append("Walltime")
            column_alignments.append("right")
        if get_status:
            column_names.append("Status Dict")
            column_alignments.append("right")
        table = TerminalTable(
                column_names, column_alignments=column_alignments,
                bold_column_headers=True, loglevel=self._loglevel)
        pwd = os.getcwd()
        for calc_info in self.calculations:
            calc = calc_info["calculation_directory"]
            row_data = [os.path.relpath(calc.path, pwd)]
            if not calc.is_in_database:
                calc.add_to_database()
            if get_id:
                row_data.append(
                        calc._db_model.id)
            if get_monitored:
                row_data.append(
                        calc._db_model.monitored)
            if get_packaged:
                row_data.append(
                        calc._db_model.packaged)
            if get_project:
                row_data.append(
                        calc._db_model.project)
            if get_walltime:
                walltime = calc._db_model.walltime
                if walltime is not None:
                    row_data.append(walltime_to_str(walltime))
                else:
                    row_data.append("N/A")
            if get_status:
                row_data.append(calc._db_model.status)
            table.add_row(row_data)
        table.print()

    def load_calculations_from_database(
            self, all_calculations=False, all_monitored_calculations=False):
        """Loads calculations from database.

        Parameters
        ----------
        all_calculations: bool, optional
            If True, loads all calculations from database.
        all_monitored_calculations: bool, optional
            If True, loads all monitored calculations from database.
        """
        if all_calculations and all_monitored_calculations:
            raise ValueError(
                "Make up your mind! Do I fetch all calculations or just "
                "the monitored ones?!?")
        if all_calculations:
            print(Colors.bold_text("LOADING") +
                  ": all calculations from database.")
            models = DBCalculation.select()
        elif all_monitored_calculations:
            print(Colors.bold_text("LOADING") +
                  ": all monitored calculations from database.")
            models = DBCalculation.select(
                         DBCalculation.calculation).where(
                             DBCalculation.monitored is False)
        list_ = []
        for model in models:
            calc = CalculationDirectory.from_calculation(
                    model.calculation, loglevel=self._loglevel)
            calc._db_model = model
            list_.append(
                    {"calculation_directory": calc, "path": calc.path,
                     "is_symlink": False})
        self.calculations = CalculationsList(
                list_, loglevel=self._loglevel)
        # if not a remote, get full abspaths
        # if not self.db.is_remote:
        #     self.calculations = CalculationsList([
        #             full_abspath(calc) for calc in self.calculations],
        #             loglevel=self._loglevel)

    def load_database(
            self, db_path, remote=None, download=False,
            refresh=False):
        """Loads the database given.

        Parameters
        ----------
        db_path: str
            Can be 'default', in which case it loads the database set
            in the user's config file. Otherwise it should be a path
            pointing to the database wanted.
        refresh: bool, optional
            If True, the 'status' database entry for selected calculations
            will be refreshed.
        remote: str, optional
            If None, we load the given file path. Otherwise, we look for
            either cached database if it exists or we download it if
            requested.
        download: bool, optional
            If remote is not None, setting this flag to True will make
            the script to download the remote's database.
        """
        if remote is None:
            if db_path == "default":
                db_path = __USER_CONFIG__.DATABASE.path
                print(Colors.bold_text("LOADING default database:") +
                      f"'{db_path}'.")
            if __USER_DATABASE__ is None:
                raise ImportError(
                        "Need to install 'peewee' in order to connect to "
                        "database.")
            __USER_DATABASE__.init(db_path)
        else:
            raise NotImplementedError()
            # if download:
            #     print(Colors.bold_text("DOWNLOADING remote database:") +
            #           f" '{remote}'.")
            # else:
            #     print(
            # Colors.bold_text("LOADING cached remote database for:") +
            #           f" '{remote}'.")
            # try:
            #     self.db = AbiDB.from_remote(
            #             remote, download=download, refresh=refresh,
            #             loglevel=self._loglevel)
            # except CachedAbiDBFileNotFoundError:
            #     self._logger.error(
            #         "Cached DB not found. Try using the --download flag.")
            #     self._logger.exception()
            #     sys.exit()
        print(Colors.color_text("SUCCESSFULL", "green", "bold") +
              ": database loaded.")

    def init_database(self, db_path):
        """Initialize a database at a given path.

        Parameters
        ----------
        db_path: str
            The path where we want to initialize the database.
        """
        db_path = full_abspath(db_path)
        print(Colors.bold_text("Creating database here:") +
              f"'{db_path}'.")
        if __USER_DATABASE__ is None:
            raise ImportError(
                    "Need to install 'peewee' in order to create a "
                    "database.")
        __USER_DATABASE__.init(db_path)
        import sqlite3
        with sqlite3.connect(db_path) as con:
            pass
        con.close()
        print(Colors.color_text("SUCCESSFULL", "green", "bold") +
              ": database created.\n"
              "If you want to make it the default database"
              ", consider adding this entry in the config file "
              f"('{__USER_CONFIG__.config_path}') under [DATABASE]: "
              f"'path = {db_path}'.\nThis can be done using the "
              "following command: 'abisuite config --set-default-db-path="
              f"{db_path}'")

    def refresh_calculations(self):
        """Refreshes the calculation statuses stored in the database."""
        for calculation in self.calculations:
            calculation["calculation_directory"]._db_model.refresh()

    async def reset_status(self):
        """Resets the status of the calculations."""
        for calc_info in self.calculations:
            calc = calc_info["calculation_directory"]
            print(Colors.bold_text("Resetting status of:") +
                  f" '{calc.path}'.")
            if not calc.is_in_database:
                print(Colors.bold_text("Adding calculation to database."))
                calc.add_to_database()
            try:
                await calc.reset_status()
            except Exception:
                self._logger.exception("reset status failed.")
                self.error("while resetting status.")
            else:
                self.success("status reset.")

    def set(self, set_monitored_true=False, set_monitored_false=False,
            set_project=None, set_packaged_true=False,
            set_packaged_false=False, update_calculation_status=None):
        """Sets data in the database regarding the calculations stored.

        Parameters
        ----------
        set_monitored_true: bool, optional
            If True, the 'monitored' database entry will be set to True.
        set_monitored_false: bool, optional
            If True, the 'monitored' database entry will be set to False.
        set_project: str, optional
            If not None, will set the 'project' database entry to this string.
        set_packaged_true: bool, optional
            If True, the 'packaged' database entry will be set to True.
        set_packaged_false: bool, optional
            If True, the 'packaged' database entry will be set to False.
        update_calculation_status: dict, optional
            If not None, the calculation status entry will be updated with this
            dictionary.
        """
        if not any(
                [set_monitored_true, set_monitored_false, set_project,
                 set_packaged_true, set_packaged_false,
                 update_calculation_status]):
            return
        fields_to_update = []
        if set_monitored_true and set_monitored_false:
            raise ValueError(
                    "Make up your mind about the 'monitored' attribute: "
                    "I cannot set it to both True and False at the same time!")
        if set_packaged_true and set_packaged_false:
            raise ValueError(
                    "Make up your mind about the 'packaged' attribute: "
                    "I cannot set it to both True and False at the same time!")
        if set_monitored_true:
            print(f"Now monitoring: '{self.calculations}'")
            fields_to_update.append(DBCalculation.monitored)
        if set_monitored_false:
            print(f"Not monitoring: '{self.calculations}'")
            fields_to_update.append(DBCalculation.monitored)
        if set_packaged_true:
            print(f"Now packaged: '{self.calculations}'")
            fields_to_update.append(DBCalculation.packaged)
        if set_packaged_false:
            print(f"Not packaged: '{self.calculations}'")
            fields_to_update.append(DBCalculation.packaged)
        if set_project is not None:
            print(f"Project '{set_project}' now contains these calculations:" +
                  f"{self.calculations}.")
            fields_to_update.append(DBCalculation.project)
        if update_calculation_status is not None:
            print(Colors.bold_text("UPDATING calculation status with: ") +
                  str(update_calculation_status))
            fields_to_update.append(DBCalculation.status)
            print("The following calculation(s) have been updated: "
                  f"'{self.calculations}'.")
        for calc_info in self.calculations:
            calc = calc_info["calculation_directory"]
            if not calc.is_in_database:
                calc.add_to_database()
            model = calc._db_model
            if set_monitored_false:
                model.monitored = False
            if set_monitored_true:
                model.monitored = True
            if set_packaged_true:
                model.packaged = True
            if set_packaged_false:
                model.packaged = False
            if set_project is not None:
                model.project = set_project
            if update_calculation_status:
                model.status.update(update_calculation_status)
        if __USER_DATABASE__ is None:
            raise RuntimeError(
                    "No database loaded in order to set data.")
        with __USER_DATABASE__.atomic():
            DBCalculation.bulk_update(
                [calc_info["calculation_directory"]._db_model
                 for calc_info in self.calculations],
                fields_to_update, batch_size=50)
        self.success("DB update.")

    def show_database(self):
        """Prints the database path."""
        if __USER_DATABASE__ is None:
            raise RuntimeError("No database to show.")
        print(f"Transacting with database: '{__USER_DATABASE__.database}'.")
