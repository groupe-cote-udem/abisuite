import os
from typing import Sequence

from .bases import BaseAbisuiteSubScript
from .. import __USER_CONFIG__
from ..colors import Colors
from ..exceptions import DevError
from ..routines import full_abspath, is_int, is_str


class AbisuiteConfigScript(BaseAbisuiteSubScript):
    """Handles the 'config' sub script of abisuite."""

    _loggername = "AbisuiteConfigScript"
    _subscript_color = "blue"
    _subscript_name = "config"

    def load_calculations(self, *args, **kwargs):  # noqa: D102
        raise DevError("Not relevant for this script.")

    def set_config(
            self,
            default_db_path=None,
            default_project_account=None,
            default_pseudos_dir=None,
            max_jobs_in_queue=None,
            package_dest_root=None,
            package_src_root=None,
            queuing_system=None,
            username=None,
            ):
        """Set config values.

        Parameters
        ----------
        default_db_path: str, optional
            If not None, sets the default database path to this one.
        default_project_account: str, optional
            If not None, sets the default project account to this one.
        default_pseudos_dir: str, optional
            If not None, sets the default pseudos dir to this one.
        max_jobs_in_queue: int, optional
            If not None, sets this value for the maximum number of jobs in
            queue. If 'no_limit' is passed, then no the corresponding config
            entry is set to None which means no job limits is imposed.
        package_dest_root: str, optional
            If not None, sets the 'package_dest_root' in the config
            to this path.
        package_src_root: str, optional
            If not None, sets the 'package_src_root' in the config
            to this path.
        queuing_system: str, optional
            If not None, sets the queuing system to this one.
        username: str, optional
            If not None, sets the username to this one.

        Raises
        ------
        TypeError: If one of the arguments is not of the good type.
        ValueError: If one of the arguments does not have a valid value.
        """
        if not any(
                (default_db_path, default_project_account, default_pseudos_dir,
                 max_jobs_in_queue, package_dest_root, package_src_root,
                 queuing_system, username, )
                ):
            return
        if default_db_path is not None:
            if not is_str(default_db_path):
                raise TypeError("'default_db_path' should be a string.")
            print(Colors.bold_text("SETTING:") +
                  f" default database path to '{default_db_path}'.")
            __USER_CONFIG__.write(
                "path", default_db_path, "DATABASE", force=True)
        if default_project_account is not None:
            if not is_str(default_project_account):
                raise TypeError("'default_project_account' should be a str.")
            print(Colors.bold_text("SETTING:") +
                  f" default project account to '{default_project_account}'.")
            __USER_CONFIG__.write(
                "default_project_account", default_project_account, "SYSTEM",
                force=True)
        if default_pseudos_dir is not None:
            if not is_str(default_pseudos_dir):
                raise TypeError("'default_pseudos_dir' should be a str.")
            default_pseudos_dir = full_abspath(default_pseudos_dir)
            if not os.path.isdir(default_pseudos_dir):
                raise ValueError(
                        "'default_pseudos_dir' should point to valid "
                        "directory.")
            print(Colors.bold_text("SETTING:") +
                  f" default pseudos directory to '{default_pseudos_dir}'.")
            __USER_CONFIG__.write(
                    "default_pseudos_dir", default_pseudos_dir, "DEFAULTS",
                    force=True)
        if max_jobs_in_queue:
            if max_jobs_in_queue == "no_limit":
                print(Colors.bold_text("SETTING:") +
                      " no limits to number of jobs in queue.")
                __USER_CONFIG__.write(
                        "max_jobs_in_queue", None, "SYSTEM", force=True)
            else:
                try:
                    max_jobs_in_queue = int(max_jobs_in_queue)
                except Exception:
                    pass
                if not is_int(max_jobs_in_queue, strickly_positive=True):
                    raise ValueError("'max_jobs_in_queue' must be an int > 0.")
                print(Colors.bold_text("SETTING:") +
                      " limit on number of jobs in queue to "
                      f"{max_jobs_in_queue}.")
                __USER_CONFIG__.write(
                        "max_jobs_in_queue", max_jobs_in_queue, "SYSTEM",
                        force=True,
                        )
        for package_var_name, package_var in {
                "package_dest_root": package_dest_root,
                "package_src_root": package_src_root,
                }.items():
            if package_var is None:
                continue
            if not is_str(package_var):
                raise TypeError("'{package_var_name}' must be a str.")
            package_var = full_abspath(package_var)
            print(Colors.bold_text("SETTING:") +
                  f" {package_var_name} to {package_var}.")
            __USER_CONFIG__.write(
                    package_var_name, package_var, "DATABASE", force=True)
        if queuing_system is not None:
            from abisuite import __IMPLEMENTED_QUEUING_SYSTEMS__
            if not is_str(queuing_system):
                raise TypeError("'queuing_system' should be a str.")
            if queuing_system not in __IMPLEMENTED_QUEUING_SYSTEMS__:
                raise ValueError(
                        f"Invalid queuing system: '{queuing_system}'. Valid "
                        f"are: {__IMPLEMENTED_QUEUING_SYSTEMS__}.")
            print(Colors.bold_text("SETTING:") +
                  f" queuing system to '{queuing_system}'.")
            __USER_CONFIG__.write(
                    "queuing_system", queuing_system, "SYSTEM", force=True)
        if username is not None:
            if not is_str(username):
                raise TypeError("'username' should be a string.")
            print(Colors.bold_text("SETTING") + f": username to '{username}'.")
            __USER_CONFIG__.write(
                    "username", username, "SYSTEM", force=True)
        self.success("Setting config values.")

    def show_config(
            self,
            build: Sequence[str] = None,
            builds: bool = False,
            config: bool = False,
            max_jobs_in_queue: bool = False,
            db_path: bool = False,
            default_project_account: bool = False,
            default_pseudos_dir: bool = False,
            package_dest_root: bool = False,
            package_src_root: bool = False,
            queuing_system: bool = False,
            username: bool = False,
            show_qe: bool = False,
            ):
        """Print out on screen the abisuite config info.

        Parameters
        ----------
        build: list, optional
            If not None, shows the properties of all builds listed in this
            list.
        builds: bool, optional
            If True, the list of different builds stored in the config file are
            printed out.
        config: bool, optional
            If True, the config file path is printed out.
        db_path: bool, optional
            If True, the database path is printed out.
        default_project_account: bool, optional
            If True, the default project account is printed out.
        default_pseudos_dir: bool, optional
            If True, the default pseudo directory is printed out.
        max_jobs_in_queue: bool, optional
            If True, print out the maximum number of jobs in queue.
        package_dest_root: bool, optional
            If True, prints out the package destination root.
        package_src_root: bool, optional
            If True, prints out the package source root.
        queuing_system: bool, optional
            If True, prints out the queuing system.
        username: bool, optional
            If True, prints out the username.
        show_qe: bool, optional
            If True, prints out QE default informations.
        """
        if build:
            for build_entry in build:
                print(Colors.bold_text("Printing build info for: ") +
                      f"'{build_entry}'.")
                if build_entry not in __USER_CONFIG__:
                    self.error("not a build defined in config file.")
                    continue
                print(__USER_CONFIG__[build_entry])
        if builds:
            print(Colors.bold_text("Printing builds:"))
            print("\n".join([f"- {b}" for b in __USER_CONFIG__.list_builds()]))
        if config:
            print(Colors.bold_text("CONFIG PATH:") +
                  f" '{__USER_CONFIG__.config_path}'.")
        if db_path:
            if __USER_CONFIG__.DATABASE.path is not None:
                print(Colors.bold_text("Database path: ") +
                      __USER_CONFIG__.DATABASE.path)
            else:
                print(Colors.bold_text("No Database is set."))
                print("You can set one using the following command: "
                      "abisuite db --init-db=path/to/the/database/file.db")
        if default_project_account:
            if __USER_CONFIG__.SYSTEM.default_project_account is not None:
                print(Colors.bold_text("Default project account") +
                      f": '{__USER_CONFIG__.SYSTEM.default_project_account}'.")
            else:
                print(Colors.bold_text("No default project account."))
        if default_pseudos_dir:
            if __USER_CONFIG__.DEFAULTS.default_pseudos_dir is None:
                print(Colors.bold_text("No default pseudos directory."))
            else:
                print(Colors.bold_text("Default pseudos directory: ") +
                      __USER_CONFIG__.DEFAULTS.default_pseudos_dir)
        if max_jobs_in_queue:
            print(Colors.bold_text("Maximum # of jobs in queue: ") +
                  f"{__USER_CONFIG__['SYSTEM']['max_jobs_in_queue']}.")
        if package_dest_root:
            print(Colors.bold_text("Package destination root: ") +
                  f"{__USER_CONFIG__['DATABASE']['package_dest_root']}")
        if package_src_root:
            print(Colors.bold_text("Package source root: ") +
                  f"{__USER_CONFIG__['DATABASE']['package_src_root']}")
        if queuing_system:
            print(Colors.bold_text("Queuing System: ") +
                  __USER_CONFIG__.SYSTEM.queuing_system)
        if username:
            if __USER_CONFIG__.SYSTEM.username is None:
                print(Colors.bold_text("No username set."))
            else:
                print(Colors.bold_text("Username: ")
                      + __USER_CONFIG__.SYSTEM.username)
        if show_qe:
            print(Colors.bold_text("Quantum Espresso Default Values: ") +
                  f"{__USER_CONFIG__['QUANTUM_ESPRESSO']}")
