"""Packaging subscript."""

import asyncio
import os
from typing import Any

import aiofiles
import aiofiles.os

import tqdm

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..exceptions import CalculationNotFoundError
from ..handlers import (
        GenericDirectory, GenericFile,
        SlurmFilesToDeleteFile, is_calculation_directory,
        )
from ..packagers.routines import compute_packaging_path, get_packaging_roots
from ..routines import (
        get_calculation_directory_from_calculation_file,
        )


# Package all files ending with these extensions as well
# even though they are outside any calculation directory
__PACKAGE_FILE_ENDINGS__ = (
        ".pickle", ".pdf", ".py", ".txt", "readme",
        "README", ".npz", ".npy", ".md", ".rst",
        )

__BACKUP_FILE_NAME__ = "TEMP_PACKAGING_BACKUP"


class AbisuitePackageScript(BaseAbisuiteSubScript):
    """Executes the 'package' subcommand of the abisuite script."""

    _loggername = "AbisuitePackageScript"
    _subscript_color = "cyan"
    _subscript_name = "package"

    def __init__(self, *args, **kwargs):
        """Package subscript class init method."""
        super().__init__(*args, **kwargs)
        # calculations_src are the main srcs used to build the calculation
        # tree. It can be built from various ways.
        self.calculations_src = None
        self.tot_files_cleared = 0
        self.tot_Gbs_cleared = 0
        self.tot_files_packaged = 0
        self.tot_Gbs_packaged = 0
        self.package_sources = []

    async def create_backup_file(self):
        """Backup all the calculations to package in a temporary file."""
        async with aiofiles.open(__BACKUP_FILE_NAME__, "w") as f:
            await f.write("\n".join(self.calculations_src))

    async def delete_backup_file(self):
        """Delete the backup file if it still exists."""
        if await aiofiles.os.path.exists(__BACKUP_FILE_NAME__):
            await aiofiles.os.remove(__BACKUP_FILE_NAME__)

    async def from_file(self, filename):
        """Load calculations from given file.

        Parameters
        ----------
        filename: str
            The file path.

        Raises
        ------
        FileNotFoundError: if file is not found.
        """
        if not await aiofiles.os.path.isfile(filename):
            raise FileNotFoundError(filename)
        self._logger.info(
                f"Loading calculations to package from '{filename}'.")
        async with aiofiles.open(filename, "r") as f:
            calculations = await f.readlines()
        self.calculations_src = [x.strip("\n") for x in calculations]

    async def load_calculations(
            self, *args, force: bool = False,
            only_successfull: bool = False) -> None:
        """Load the concerned calculations.

        Parameters
        ----------
        force: bool, optional
            If True, already packaged calculations (according to database)
            will be repackaged.
            Also, all calculations, regarding of its status will
            be packaged.
        only_successfull: bool, optional
            If True, only successfull calculations will be packaged.
        """
        if self.calculations_src is None:
            self.calculations_src = list(args)
        await super().load_calculations(
                self.calculations_src,
                load_errored=force or not only_successfull,
                load_unstarted=force or not only_successfull,
                load_running=force or not only_successfull,
                load_finished=True,
                load_converged=True,
                filter_calculations=True,
                )

    async def package_calculations(
            self, force: bool = False,
            **kwargs,
            ) -> None:
        """Package the calculations stored in the 'calculations' property.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated and only what will be done will
            be printed on screen. Nothing else is done.
        force: bool, optional
            If True, already packaged calculations (according to database)
            will be repackaged. This option also turns 'overwrite' option
            to True. Also, all calculations, regarding of its status will
            be packaged.
        overwrite: bool, optional
            In case a calculation is not marked as 'packaged' in the database
            but files already exists at destination, these files will
            be overwritten. This option is automatically turned on if
            'force' is True.
        replace_with_symlink: bool, optional
            If True, the packaged calculation will be replaced with a symlink.
        keep_density: bool, optional
            If True, the density files will be packaged even though they
            would be left out otherwise.
        keep_wavefunctions: bool, optional
            If True, the wavefunction files will be packaged even though they
            would be left out otherwise.
        """
        self._logger.info(
                f"Starting to package {len(self.calculations)} calculations.")
        if kwargs.get("keep_density", False):
            self._logger.info("Keeping densities is set to True")
        if kwargs.get("keep_wavefunctions", False):
            self._logger.info("Keep wavefunctions is set to True")
        # package_coros = []
        for icalc, calc_info in enumerate(self.calculations):
            await self._package_calculation(
                    calc_info, force=force,
                    total=len(self.calculations),
                    index=icalc,
                    **kwargs)
            # package_coros.append(
            #         self._package_calculation(
            #             calc_info,
            #             force=force, **kwargs))
        # await asyncio.gather(*package_coros)

    async def _skip_already_packaged(
            self, calc_info: dict[str, Any],
            force: bool,
            replace_with_symlink: bool,
            ) -> bool:
        # takes the calc info and return True if we skip packaging
        if replace_with_symlink and not calc_info["is_symlink"]:
            # redo packaging even if already packaged
            return False
        calculation = calc_info["calculation_directory"]
        if await calculation.is_packaged and not force:
            return True
        return False

    async def _package_calculation(
            self, calc_info: dict[str, Any],
            dry: bool = False,
            force: bool = False,
            replace_with_symlink: bool = False,
            overwrite: bool = False,
            keep_density: bool = False,
            keep_wavefunctions: bool = False,
            total: int = 0,
            index: int = 0,
            store_package_sources: str = None,
            ) -> None:
        """Package a single calculation."""
        calculation = calc_info["calculation_directory"]
        count = Colors.bold_text(f"[{index + 1}/{total}]: ")
        if await self._skip_already_packaged(
                calc_info, force, replace_with_symlink):
            self._logger.info(
                    count +
                    Colors.color_text(
                        "Skipping Already Packaged", "green", "bold") +
                    f" calculation: '{calculation.path}'.")
            return
        path = os.path.relpath(calculation.path, start=self.pwd)
        if dry:
            print(Colors.bold_text("Will package ") +
                  self.get_status_flag(await calculation.status) +
                  f" '{path}'.")
            self.package_sources.append(calculation.path)
            return
        else:
            print(count + Colors.color_text("Packaging ", "bold") +
                  self.get_status_flag(await calculation.status) +
                  f" '{path}'.")
        try:
            ngbs = await calculation.directory_size
            nfiles = len(await calculation.walk(paths_only=True))
            await calculation.package(
                    overwrite=overwrite or force,
                    replace_with_symlink=replace_with_symlink,
                    keep_wavefunctions=keep_wavefunctions,
                    keep_density=keep_density,
                    pwd=self.pwd,
                    )
        except Exception:
            self._logger.exception(Colors.color_text("FAILED", "red"))
        else:
            packager = await calculation.packager
            self.tot_Gbs_packaged += packager.nGbs_saved
            self.tot_files_packaged += (
                    packager.nfiles_saved)
            self.print_summary(
                    calculation.path,
                    packager.nGbs_saved,
                    packager.nfiles_saved,
                    final=False, replace_with_symlink=False)
            if replace_with_symlink:
                self.print_summary(
                        calculation.path,
                        ngbs, nfiles, final=False,
                        replace_with_symlink=True)
                self.tot_Gbs_cleared += ngbs
                self.tot_files_cleared += nfiles
        # clear content of directory to free RAM
        calculation.clear_content()

    async def package_other_files(self, overwrite=False, dry=False):
        """From the 'calculations_src' property, packages all other files.

        These are outside calculations directories.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated and only what will be done will
            be printed on screen. Nothing else is done.
        overwrite: bool, optional
            If True, files present at destination will be overwritten.
        """
        self._logger.info("Packaging other files outside calculations.")
        # also package results directory and python files independantly
        src_root, dest_root = get_packaging_roots()
        coros = []
        packaged_dirs = []
        for src in self.calculations_src:
            # walk all source directory and copy every non-calculation files
            # that are relevant
            for root, dirs, files in os.walk(src):
                if root in packaged_dirs:
                    continue
                for dir_ in dirs:
                    if not await self._package_directory(dir_):
                        continue
                    path = os.path.join(root, dir_)
                    packaged_dirs.append(path)
                    if dry:
                        print(Colors.bold_text("Will package:") +
                              f" {path}.")
                        self.package_sources.append(path)
                        continue
                    coros.append(
                            self._package_this_dir(path, src_root, dest_root))
                for file_ in files:
                    if not self._package_file(file_):
                        continue
                    path = os.path.join(root, file_)
                    if dry:
                        print(Colors.bold_text("Will package:") +
                              f" {os.path.relpath(path, start=self.pwd)}.")
                        self.package_sources.append(path)
                        continue
                    coros.append(
                            self._package_this_file(path, src_root, dest_root))
        await asyncio.gather(*coros)
        if not dry:
            self.success("Other files packaged.")

    async def write_package_sources(
            self, store_package_sources: str = None) -> None:
        """Write a file that contains the package sources in a dry run."""
        if store_package_sources is None:
            return
        self._logger.info(
                f"Storing package sources in: '{store_package_sources}'.")
        if await aiofiles.os.path.isfile(store_package_sources):
            self._logger.info(
                    "Deleting already existing package sources file.")
            await aiofiles.os.remove(store_package_sources)
        async with aiofiles.open(store_package_sources, "w") as f:
            for source in self.package_sources:
                await f.write(f"{source}\n")
        self.success("Writing done.")

    async def _package_this_file(self, path, src_root, dest_root):
        handler = GenericFile(loglevel=self._loglevel)
        await handler.set_path(path)
        relpath = os.path.relpath(handler.path, start=self.pwd)
        print(Colors.bold_text("Packaging") +
              f" file '{relpath}'.")
        try:
            await handler.copy(compute_packaging_path(
                handler.path, src_root, dest_root),
                overwrite=True)
        except Exception:
            self._logger.exception(
                    Colors.color_text("FAILED", "red") + " to package file: "
                    + relpath,
                    )
        else:
            self.tot_files_packaged += 1
            self.tot_Gbs_packaged += await handler.file_size

    async def _package_this_dir(self, path, src_root, dest_root):
        handler = GenericDirectory(loglevel=self._loglevel)
        await handler.set_path(path)
        print(Colors.bold_text("Packaging") +
              " results directory: "
              f"'{os.path.relpath(handler.path, start=self.pwd)}'.")
        async with handler:
            try:
                await handler.copy(
                    compute_packaging_path(
                        handler.path, src_root, dest_root),
                    overwrite=True)
            except Exception:
                self._logger.exception(
                      Colors.color_text("FAILED", "red", "bold"))
            else:
                self.tot_files_packaged += len(
                        await handler.walk(paths_only=False))
                self.tot_Gbs_packaged += (
                        await handler.directory_size)

    def print_summary(
            self,
            calc_path: str = None,
            mem: float = None,
            files: int = None,
            replace_with_symlink: bool = False,
            final: bool = True,
            final_cleared: bool = False,
            ) -> None:
        """Print the packaging summary.

        Parameters
        ----------
        calc_path: str, optional
            The Calculation directory's path.
        mem: float, optional
            Gives the total amount of memory cleared. If None, total is used.
        files: int, optional
            Gives the number of files cleared. If None, total is used.
        final: bool, optional
            If True, prints a final summary.
        final_cleared: bool, optional
            If True, adds a 'cleared' to the print stmt.
        replace_with_symlink: bool, optional
            If True, prints out as well what has been cleared.
        """
        mem_units = "Gb"
        if mem is None:
            if final_cleared:
                mem = self.tot_Gbs_cleared
            else:
                mem = self.tot_Gbs_packaged
        if mem >= 1024:
            mem_units = "Tb"
            mem /= 1024
        elif mem <= 0.1:
            mem *= 1024
            mem_units = "Mb"
        files_units = ""
        if files is None:
            if final_cleared:
                files = self.tot_files_cleared
            else:
                files = self.tot_files_packaged
        if files >= 1000:
            files /= 1000
            files = round(files, 1)
            files_units = "k"
        ending = "packaged"
        if (replace_with_symlink and not final) or final_cleared:
            ending = "cleared"
        if final:
            if not final_cleared:
                print(Colors.bold_text("Packaging summary:"))
            msg = ""
            meth = print
        else:
            meth = self.successfull
            if calc_path is None:
                calc_path = "[DEVERROR]"
            calc_path = os.path.relpath(calc_path, start=self.pwd)
            if replace_with_symlink:
                msg = f"Replaced with symlink: {calc_path} "
            else:
                msg = f"Calculation packaged: {calc_path} "
        meth(f"{msg}{files}{files_units} files for "
             f"{mem:.1f} {mem_units} {ending}.")

    async def use_slurm_files_to_delete(self, path):
        """Load calculations from a 'slurm files to delete' file.

        This method fills the 'self.calculations_src' list.

        Parameters
        ----------
        path: str
            The path to the slurm file.
        """
        self._logger.info(
                Colors.bold_text("USING Slurm files to delete: ") + path)
        async with await SlurmFilesToDeleteFile.from_file(
                path, loglevel=self._loglevel) as slurm:
            files_to_delete = slurm.files_to_delete
        self.calculations_src = set()  # to avoid duplicates

        self._logger.info("Parsing slurm file. This may take a while...")
        errored = []
        for file_to_delete in tqdm.tqdm(
                files_to_delete, total=len(files_to_delete), unit="filename",
                unit_scale=True):
            # regroup only calc dirs and
            try:
                src = await self._get_calc_src(file_to_delete)
            except Exception:
                self._logger.error(
                        "An error occured while processing this path: "
                        f"{file_to_delete}. Skipping it...")
                errored.append(file_to_delete)
            if src is not None:
                self.calculations_src.add(src)
        self.calculations_src = list(self.calculations_src)
        if errored:
            msg = "An error occured for the following files:\n"
            for error in errored:
                msg += f"- {error}\n"
            self._logger.error(msg)
            self._logger.error(
                    "User might want to treat these files manually.")
        # calculations = []
        # for calc in calculations_src:
        #     if is_calculation_directory(calc):
        #         calculations.append(
        #                 CalculationDirectory.from_calculation(calc))

    async def _get_calc_src(self, filename):
        # from a filename, returns the corresponding calculation directory
        # or the parent directory in case it's a file we want to package
        for ending in __PACKAGE_FILE_ENDINGS__:
            if filename.endswith(ending):
                # return it's dirname since the dirname will be walked
                # through later
                return os.path.dirname(filename)
        # go up recursively until we find a calcdir
        try:
            return await get_calculation_directory_from_calculation_file(
                    filename)
        except CalculationNotFoundError:
            return None

    async def _package_directory(self, dirname):
        """Tells if we keep this directory or not based on its name.

        Parameters
        ----------
        dirname: str
            The name (basename) of the directory.

        Returns
        -------
        bool: True if we package. False otherwise.
        """
        if await is_calculation_directory(dirname):
            # calculations directories are supposed to be already packaged
            return False
        if dirname.lower().endswith("results"):
            return True
        return False

    def _package_file(self, filename):
        """Tells if we keep this file or not based on its name.

        Parameters
        ----------
        filename: str
            The name (basename) of the file.

        Returns
        -------
        bool: True if we package. False otherwise.
        """
        filename = filename.lower()
        for ending in __PACKAGE_FILE_ENDINGS__:
            if filename.endswith(ending):
                return True
        return False
