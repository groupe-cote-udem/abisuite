import subprocess

from .bases import BaseAbisuiteVisualizeSubScript
from ..linux_tools import which


class AbisuiteVisualizeChargeScript(BaseAbisuiteVisualizeSubScript):
    """Executes the 'visualize_charge' subcommand of the abisuite script."""

    _loggername = "AbisuiteChargeStructureScript"

    async def visualize_charge(self):
        """Visualize charge using any means necessary.

        Depending on what data files have been loaded/found, the correct
        software to vusalize structure will be automatically detected.
        """
        if "xsf" in self.data_files:
            await self.visualize_charge_from_xsf(self.data_files["xsf"])

    async def visualize_charge_from_xsf(self, xsf_file):
        """Visualize the charge density by calling VESTA.

        VESTA is called on the
        (possibly generated) xsf file representing the electronic density.
        """
        # launch vesta if it is available
        if await which("vesta") is not None:
            self._logger.info("Launching 'vesta' to visualize structure.")
            subprocess.run(["vesta", xsf_file])
        elif await which("VESTA") is not None:
            self._logger.info("Launching 'VESTA' to visualize structure.")
            subprocess.run(["VESTA", xsf_file])
        else:
            self.error("'vesta' not available in 'PATH'. Please install it.")
