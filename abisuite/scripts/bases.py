"""Base classes for abisuite scripts and subscripts."""

import argparse
import asyncio
import logging
import os
from typing import Any, Sequence

import aiofiles.os

import aioshutil

import tqdm

from .routines import get_all_calculations_recursively
from .. import __SYSTEM_QUEUE__
from ..bases import BaseUtility
from ..colors import Colors
from ..exceptions import DevError
from ..handlers import (
        CalculationDirectory, GenericDirectory,
        is_calculation_directory,
        )
from ..launchers import AbinitCut3DLauncher
from ..routines import connect_to_database, full_abspath
from ..status_checkers import __ERROR_STRING__, __NON_SCF_STATUS_STRING__


class BaseAbisuiteSubScript(BaseUtility):
    """Base class for abisuite subcommands script class.

    This class basically implements a load_calculations method based
    on a list of calculation sources.

    Note
    ----
    This class is not meant to be usable directly but only subclassed.
    """

    unstarted = Colors.color_text("UNSTARTED", "cyan", "bold")
    running = Colors.color_text("RUNNING", "yellow", "bold")
    errored = Colors.color_text("ERRORED", "red", "bold")
    converged = (
            Colors.color_text("FINISHED ", "green", "bold") +
            Colors.bold_text("& ") +
            Colors.color_text("CONVERGED", "green", "bold"))
    not_converged = (
            Colors.color_text("FINISHED ", "green", "bold") +
            Colors.bold_text("& ") +
            Colors.color_text("NOT CONVERGED", "red", "bold"))
    finished = Colors.color_text("FINISHED", "green", "bold")
    finished_nonscf = (
            Colors.color_text("FINISHED ", "green", "bold") +
            Colors.color_text("(not SCF)", "blue", "bold")
            )
    _subscript_color = None
    _subscript_name = None

    def __init__(self, *args, **kwargs):
        """Base subscript init method."""
        super().__init__(*args, **kwargs)
        self.pwd = os.getcwd()
        self._present_self()

    def _present_self(self) -> None:
        if self._subscript_name is None:
            raise DevError("Need to set subscript_name")
        attrs = ["bold"]
        if self._subscript_color is not None:
            attrs.append(self._subscript_color)
        txt = ("=== Executing subscript: "
               f"{Colors.color_text(self._subscript_name, *attrs)} ===\n")
        print(txt)

    async def cancel_job(self, calculation_directory, dry=False):
        """Cancel a job based on its calculation directory object.

        Parameters
        ----------
        calculation_directory: CalculationDirectory instance
            The CalculationDirectory instance we want to cancel the job of.
        dry: bool, optional
            If True, no job is cancelled but the job id is printed out if
            it would be cancelled otherwise.
        """
        async with calculation_directory.pbs_file as pbs:
            if pbs.queuing_system == "local":
                return
        async with __SYSTEM_QUEUE__ as queue:
            if calculation_directory not in queue:
                return
            job = queue[calculation_directory]
            if not dry:
                print(Colors.bold_text("CANCELLING") +
                      f" job id: {job.id}.")
                queue.cancel_job(job)
                self.success("job cancellation.")
            else:
                print(Colors.bold_text("Will cancel job id:") +
                      f" job id: {job.id}.")

    def get_status_flag(self, status: dict) -> str:
        """Return the status (colored) flag for the given calculation status.

        Parameters
        ----------
        status: dict
            Status dictionary of a calculation.

        Raises
        ------
        ValueError: if status dict is wrong somehow.
        """
        if status["calculation_started"] is False:
            return self.unstarted
        if status["calculation_finished"] is False:
            return self.running
        if status["calculation_finished"] == __ERROR_STRING__:
            return self.errored
        if status["calculation_finished"] is True:
            if "calculation_converged" in status:
                if status["calculation_converged"] is True:
                    return self.converged
                elif status["calculation_converged"] is False:
                    return self.not_converged
                elif status["calculation_converged"] == __ERROR_STRING__:
                    return self.errored
                elif (status["calculation_converged"] ==
                      __NON_SCF_STATUS_STRING__):
                    return self.finished_nonscf
            else:
                return self.finished
        else:
            raise ValueError(status)

    def error(self, text):
        """Print a given text with a bug 'ERROR' written before it.

        In red and bold painted blood.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("ERROR: ", "bold", "red"))

    def warning(self, text):
        """Print a warning message with orange and bold label."""
        self._print_this_with(
                text,
                Colors.color_text("WARNING: ", "bold", "orange"))

    async def load_calculations(
            self,
            calculations_srcs: Sequence[str],
            follow_symlinks: bool = False,
            load_converged: bool = True,
            load_errored: bool = True,
            load_finished: bool = True,
            load_running: bool = True,
            load_unstarted: bool = True,
            filter_calculations: bool = True,
            print_info: bool = True,
            **kwargs,
            ):
        """Load the calculations to clean.

        Parameters
        ----------
        calculations_srcs: list-like
            The list of calculations srcs to compute the total tree
            of calculations to clean.
        load_converged: bool, optional
            If False, don't load converged calculations.
        load_errored: bool, optional
            If False, don't load errored calculations.
        load_finished: bool, optional
            If False, don't load finished calculations.
        load_running: bool, optional
            If False, don't load running calculations.
        load_unstarted: bool, optional
            If False, don't load unstarted calculations.
        follow_symlinks: bool, optional
            If True, symlinks towards other calculations are followed.
        filter_calculations: bool, optional
            If True, filter calculations. If False, do not care about
            filtering thus ignoring all the other arguments.
        print_info: bool, optional
            If True, the reasons why calculations are skipped are printed.
        other_kwargs:
            Passed to the
            :meth:`.BaseAbisuiteSubScript.skip_this_calculation` method.
        """
        self._logger.info("Loading calculations.")
        self._logger.debug("Loading and keeping 'is_symlink' attribute.")
        connect_to_database()
        calculations_data = await get_all_calculations_recursively(
                calculations_srcs,
                follow_symlinks=follow_symlinks,
                loglevel=self._loglevel)
        filtered = []
        if filter_calculations:
            coros = []
            for calculation in calculations_data:
                coros.append(self.skip_this_calculation(
                        calculation,
                        continue_on_converged=not load_converged,
                        continue_on_error=not load_errored,
                        continue_on_finished=not load_finished,
                        continue_on_running=not load_running,
                        continue_on_unstarted=not load_unstarted,
                        **kwargs,
                        ))
            reasons = []
            for coro in tqdm.tqdm(
                    asyncio.as_completed(coros),
                    desc="Filtering calculations by status",
                    unit="calcs", total=len(calculations_data)):
                calculation_data, skip, reason = await coro
                if skip:
                    reasons.append(reason)
                    continue
                filtered.append(calculation_data)
            if print_info:
                for reason in reasons:
                    print(reason)
        self.calculations = (
                filtered if filter_calculations else calculations_data)

    # TODO: put this method in a kind of iterator which would make
    # more sense than an instance method (FG: 2021/07/26)
    async def skip_this_calculation(
            self,
            calc_info: dict[str, Any],
            continue_on_error: bool = True,
            continue_on_unstarted: bool = True,
            continue_on_running: bool = True,
            continue_on_finished: bool = True,
            continue_on_converged: bool = True,
            ) -> tuple[dict, bool]:
        """Tell if we skip a given calc based on its status.

        When iterating over a list of CalculationDirectory object and only
        want to manage some calculations based on their status,
        use this function.

        Parameters
        ----------
        calc_info: dict
            The calculation dict info as returned by the
            `CalculationDirectory.build_tree` method.
        continue_on_error: bool, optional
            If True (default), if the calculation errored out,
            we skip it.
        continue_on_unstarted: bool, optional
            If True (default), if the calculation is not started, we skip it.
        continue_on_running: bool, optional
            If True (default), if the calculation is running, we skip it.
        continue_on_finished: bool, optional
            If True (default), if the calculation is finished, we skit it.
        continue_on_converged: bool, optional
            If True (default), if the calculation is finished and converged
            (if defined), we skip it.

        Returns
        -------
        tuple: (dict, bool):
            * The dict is the original calculation info.
            * The bool is True if we skip this calculation. False otherwise.

        Side Effects
        ------------
        Calculation that will be skipped
        will have their content cleared
        in order to free some memory (useful when there are a lot of files in
        a calculation directory).
        """
        calculation = calc_info["calculation_directory"]
        async with calculation:
            status = await calculation.status
            finished = status["calculation_finished"]
            started = status["calculation_started"]
            converged = None
            if "calculation_converged" in status:
                converged = status["calculation_converged"]
            if converged == __NON_SCF_STATUS_STRING__:
                converged = None
            relpath = os.path.relpath(calculation.path, start=self.pwd)
            if started is False and continue_on_unstarted:
                reason = (
                        "Skipping " +
                        Colors.color_text("UNSTARTED", "cyan", "bold") +
                        f" calculation: '{relpath}'.")
                calculation.clear_content()
                return (calc_info, True, reason)
            if finished is False and started is True and continue_on_running:
                reason = ("Skipping " +
                          Colors.color_text("RUNNING", "yellow", "bold") +
                          f" calculation: '{relpath}'.")
                calculation.clear_content()
                return (calc_info, True, reason)
            if finished is True:
                if converged is None and continue_on_finished:
                    reason = ("Skipping " +
                              Colors.color_text("FINISHED", "bold", "green") +
                              f" calculation: '{relpath}'.")
                    calculation.clear_content()
                    return (calc_info, True, reason)
                else:
                    # converged attribute is set
                    if converged is True and continue_on_converged:
                        reason = (
                                "Skipping " +
                                Colors.color_text(
                                    "FINISHED & CONVERGED", "bold",
                                    "green") +
                                f" calculation: '{relpath}'.")
                        calculation.clear_content()
                        return (calc_info, True, reason)
            if finished == "error" and continue_on_error:
                reason = (
                        "Skipping " +
                        Colors.color_text("ERRORED", "red", "bold") +
                        f" calculation: '{relpath}'.")
                calculation.clear_content()
                return (calc_info, True, reason)
        return (calc_info, False, None)

    def success(self, text):
        """Prints out a given text with a bug 'SUCCESS' written before it.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("SUCCESS: ", "bold", "green"))

    def successfull(self, text):
        """Prints out a given text with a bug 'SUCCESSFULL' written before it.

        Parameters
        ----------
        text: str
            The text to print.
        """
        self._print_this_with(
                text,
                Colors.color_text("SUCCESSFULL: ", "bold", "green"))

    def _print_this_with(self, text, label):
        # text: user specified text to print
        # label: something that will label the text (printed before it)
        print(f"{label} {text}")


# TODO: delete me (FG 2021/10/27)
class BaseAbisuiteArgParser:
    """Baseclass for any argparser for the abisuite package."""

    description = None

    def __init__(self):
        self.parser = self.create_parser()

    def parse_args(self):
        """Parse the command line arguments."""
        return self.parser.parse_args()

    def create_parser(self):
        """Create the arg parser."""
        parser = argparse.ArgumentParser(description=self.description)
        parser.add_argument("--loglevel", "-l", action="store", type=int,
                            default=logging.INFO,
                            help=("Sets the logging level."))
        return parser


class BaseAbisuiteVisualizeSubScript(BaseAbisuiteSubScript):
    """Base class for visualizer subscripts that needs abinit cut3d utility."""

    _cut3d_run_basename = "cut3d_run"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.calc_path = None
        self.data_files = {}

    async def generate_xsf_file(self, force=False):
        """Generate the xsf file if it does not exists.

        Parameters
        ----------
        force: bool, optional
            If xsf file already exists,
        """
        if "xsf" in self.data_files and not force:
            self._logger.info(
                    "XSF file already exists. Don't need to generate.")
            return
        den_file = self.data_files["density"]
        if self._cut3d_run_basename in await aiofiles.os.listdir(
                os.path.dirname(den_file)):
            if force:
                # clean it up
                self._logger.info("Cleaning up previous xsf files.")
                await aioshutil.rmtree(
                        os.path.join(
                            os.path.dirname(den_file),
                            self._cut3d_run_basename))
            else:
                raise FileExistsError(
                        "cut3d rundir already exists. Use --force to redo.")
        # xsf does not exist => generate it using cut3d
        # create cut3d launcher
        self._logger.info("Launching 'cut3d' to generate 'xsf' file.")
        launcher = AbinitCut3DLauncher(loglevel=self._loglevel)
        if self.calc_path is not None:
            # generate from a calculation
            await launcher.set_workdir(self.calc_path)
        else:
            # generate next to the DEN file
            await launcher.set_workdir(os.path.join(
                    os.path.dirname(self.data_files["density"]),
                    self._cut3d_run_basename))
            # link den file
            try:
                await launcher.link_den_file(self.data_files["density"])
            except FileExistsError:
                # already linked
                pass
        await launcher.write(overwrite=True)
        await launcher.run()
        self.data_files["xsf"] = os.path.join(
                launcher.calculation_directory.output_data_directory.path,
                launcher.input_file.output_file_path)
        if await aiofiles.os.path.isfile(self.data_files["xsf"]):
            self.success("Generated XSF file.")
        else:
            self.error("An error occured while generating XSF file.")

    async def load_data(self, calc_or_file, **kwargs):
        """Loads up the DEN file/calc to visualize.

        Parameters
        ----------
        calc_or_file: str
            Path towards the calculation to visualize or file path
            of the DEN file to visualize.
        """
        if await is_calculation_directory(calc_or_file):
            await self._load_data_from_calc(calc_or_file)
        else:
            await self._load_data_from_file(calc_or_file, **kwargs)

    async def _load_data_from_calc(self, calcpath):
        async with await CalculationDirectory.from_calculation(
                calcpath) as calc:
            if await calc.calctype == "abinit":
                # check if an xsf file already exists
                async with calc.output_data_directory as output:
                    async for filehandler in output:
                        if filehandler.basename == self._cut3d_run_basename:
                            for filename in await filehandler.walk():
                                if filename.endswith(".xsf"):
                                    self.data_files["xsf"] = filename
                        if filehandler.path.endswith(".xsf"):
                            # xsf file already exists
                            self.data_files["xsf"] = filehandler.path
                        if "DEN" in filehandler.path:
                            self.data_files["density"] = filehandler.path
                if "xsf" not in self.data_files:
                    await self.generate_xsf_file()
            else:
                raise NotImplementedError(calc.calctype)

    async def _load_data_from_file(self, filepath, **kwargs):
        if not os.path.isfile(filepath):
            raise FileNotFoundError(filepath)
        # assume we have an Abinit DEN file.
        if "_DEN" not in filepath:
            raise FileNotFoundError(
                    f"Expected Abinit DEN file but got: '{filepath}'.")
        # check if an xsf file has been generated next to the DEN file
        self.data_files["density"] = full_abspath(filepath)
        if self._cut3d_run_basename in await aiofiles.os.listdir(
                os.path.dirname(self.data_files["density"])):
            # this dir is not a regular calc dir. thus we manually walk through
            self._logger.debug("Cut3d has a run directory")
            dir_ = GenericDirectory(loglevel=self._loglevel)
            await dir_.set_path(os.path.join(
                    os.path.dirname(filepath), self._cut3d_run_basename))
            for handler in await dir_.walk(paths_only=False):
                if handler.basename.endswith(".xsf"):
                    self.data_files["xsf"] = handler.path
                    self._logger.info("Found '.xsf' file already existing.")
                    return
        # generate xsf file
        await self.generate_xsf_file(**kwargs)


class _BaseAbisuiteSubScriptMinimalLoadingReqs(BaseAbisuiteSubScript):
    """Subscript which loads calculations with minimal requirements.

    Usefull if calculation tree is broken for some reason.
    """

    async def load_calculations(
            self, calculations_srcs: Sequence[str]) -> None:
        """Loads the calcs to attemp repair into the 'calculations' attribute.

        This method overrides the mother's class method since some files
        can be broken or missing, the aforementioned method is likely to fail.

        Parameters
        ----------
        calculations_srcs: list
            The list of calculations root sources. It will recursively
            load calculations path from these sources.
        """
        coros = []
        self.calculations = []
        for src in calculations_srcs:
            src = full_abspath(src)
            coros.append(self._load_single_calculation(src))
        await asyncio.gather(*coros)

    async def _load_single_calculation(self, src: str) -> None:
        if not await aiofiles.os.path.isdir(src):
            return
        # check if there is a meta data file
        dir_content = await aiofiles.os.listdir(src)
        have_run = False
        have_idd = False
        for filename in dir_content:
            if (filename.startswith(".") and filename.endswith(".meta")):
                self.calculations.append({"path": src})
                return
            # check if a run dir and input data dir
            if filename == "run" and await aiofiles.os.path.isdir(
                    os.path.join(src, filename)):
                have_run = True
                self._logger.debug(f"Found a run directory in {src}")
            if filename == "input_data" and await aiofiles.os.path.isdir(
                    os.path.join(src, filename)):
                have_idd = True
                self._logger.debug(f"Found an input data directory in {src}")
            if have_idd and have_run:
                self._logger.debug(
                    f"Found a calc at {src} because it has a run directory "
                    " and an input data directory.")
                self.calculations.append({"path": src})
                return
        # not a calcdir => recursively check in it for deeper calc dirs
        new_coros = []
        for name in dir_content:
            joined = os.path.join(src, name)
            if await aiofiles.os.path.isdir(joined):
                new_coros.append(self.load_calculations((joined, )))
        await asyncio.gather(*new_coros)
