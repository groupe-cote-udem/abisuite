from typing import Sequence

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..plotters import Plot


class AbisuitePlotScript(BaseAbisuiteSubScript):
    """Executes the 'plot' subcommand of the abisuite script."""

    _loggername = "AbisuitePlotScript"
    _subscript_name = "plot"
    _subscript_color = "green"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.plot = None

    def load_plot(self, path: str) -> None:
        """Load the plot object from a path."""
        self._logger.info(Colors.bold_text("Using plot file:") + f" '{path}'.")
        self.plot = Plot.load_plot(path, loglevel=self._loglevel)

    def modify_plot(
            self, xlims: Sequence[float | int] = None,
            ylims: Sequence[float | int] = None,
            ) -> None:
        """Modify the plot with these new attributes."""
        if xlims is not None:
            self._logger.info(
                    Colors.bold_text("Adjusting xlims to:") + f" '{xlims}'.")
            self.plot.xlims = xlims
        if ylims is not None:
            self._logger.info(
                    Colors.bold_text("Adjusting ylims to:") + f" '{ylims}'.")
            self.plot.ylims = ylims

    async def save_plot(self, path: str) -> None:
        """Save the plot to a given path."""
        if not path.endswith(".pdf"):
            path += ".pdf"
        self._logger.info(
                Colors.bold_text("Saving plot to pdf ->") + f" '{path}'.")
        self.plot.reset()
        self.plot.plot(show=False)
        await self.plot.save(path, overwrite=True)

    def show_plot(self) -> None:
        """Show the plot."""
        self.plot.plot()

    async def save_pickle(self, path: str) -> None:
        """Save the plot pickle to a given path."""
        self._logger.info(
                Colors.bold_text("Saving plot pickle ->") +
                f" '{path}'.")
        await self.plot.save_pickle(path, overwrite=True)
