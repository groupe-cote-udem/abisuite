import asyncio
import logging
import os

import numpy as np

import peewee

import tqdm

from .bases import BaseAbisuiteSubScript
from .routines import walltime_to_str
from .. import __SYSTEM_QUEUE__
from ..colors import Colors
from ..databases import DBCalculation
from ..handlers import CalculationDirectory, PBSFile
from ..routines import connect_to_database, is_scalar
from ..status_checkers.bases import (
        __ERROR_STRING__,
        )
from ..utils import TerminalTable


SQL_LIMIT = 500


class AbisuiteStatusScript(BaseAbisuiteSubScript):
    """Executes the 'status' subcommand of the abisuite script.

    Basically prints out a nicely formatted table of a given set
    of calculations.
    """

    _loggername = "AbisuiteStatusScript"
    _subscript_color = "Green"
    _subscript_name = "status"

    async def status(
            self, sort_by: str = "path",
            **kwargs,
            ):
        """Prints out a nicely formatted table of the given set of calcs.

        Parameters
        ----------
        force: bool, optional
            If True, we will force the computing of the status and not check
            into the DB for the calculation status and walltime.
        force_errored: bool, optional
            Same as 'force' but only for errored calculations.
        sort_by: str, {'walltime', 'path'}
            Specifies the sort. If 'path', sorting is done on paths (default).
            If 'walltime', sorting is done on walltimes instead.

        Return
        ------
        TerminalTable instance used to print out status table.
        """
        columns = ["Calculation", "status", "walltime"]
        column_alignments = ["left", "center", "center"]
        max_walltime = 0  # maximum walltime
        self._logger.info("Computing status table. This may take a while...")
        calc_paths = [
                calc_info["calculation_directory"].path
                for calc_info in self.calculations
                ]
        is_symlinks = {
                calc_inf["calculation_directory"].path: calc_inf["is_symlink"]
                for calc_inf in self.calculations
                }
        data, max_walltime = await self._compute_status_from_paths(
                calc_paths, is_symlinks, **kwargs)
        # print status table
        # iterate over walltimes to format it in a more readable way
        walltimes = [row[-1] for row in data]
        # MANAGE SORTING HERE
        if sort_by == "walltime":
            sorted_indices = np.argsort(walltimes)
            self._logger.debug("Sorting by walltimes.")
        else:
            # sort on paths
            sorted_indices = np.argsort([row[0] for row in data])
        for row, walltime in zip(data, walltimes):
            if not is_scalar(walltime):
                continue
            row[-1] = walltime_to_str(walltime, max_walltime=max_walltime)
        table = TerminalTable(
                columns, column_alignments=column_alignments,
                bold_column_headers=True, loglevel=self._loglevel)
        for index in sorted_indices:
            table.add_row(data[index])
        table.print()
        return table

    async def _get_calc_models(self, calc_paths):
        connect_to_database()
        models = []
        if len(calc_paths) > SQL_LIMIT:
            self._logger.info(
                    f"Number of calculation ({len(calc_paths)} "
                    f"to compute stats > {SQL_LIMIT} -> "
                    "We need to split this in chunks to prevent SQLite errors."
                    )
            # need to split job in chunks
            todo = calc_paths.copy()
            while len(todo) > SQL_LIMIT:
                models += await self._get_calc_models(todo[:SQL_LIMIT])
            return models
        db_call = DBCalculation.select(
                DBCalculation.calculation, DBCalculation.status,
                DBCalculation.walltime).where(
                DBCalculation.calculation.in_(calc_paths))
        # check that all calc_paths are present
        db_call = list(db_call)
        paths = [model.calculation for model in db_call]
        if len(db_call) == len(calc_paths):
            # we're ok
            return db_call
        for calc_path in calc_paths:
            if calc_path in paths:
                continue
            # insert in db
            self._logger.debug(f"Need to insert {calc_path} into database.")
            calcdir = await CalculationDirectory.from_calculation(
                    calc_path, loglevel=self._loglevel)
            try:
                calcdir.add_to_database()
            except peewee.IntegrityError:
                # for some reason this raise an error everytime but it works??
                pass
            db_call.append(calcdir._db_model)
        return db_call

    async def _compute_status_from_paths(
            self, calc_paths: list[DBCalculation],
            is_symlinks: list[bool],
            **kwargs) -> tuple:
        data = []
        # here order might not be preserved when querying DB
        calc_models = await self._get_calc_models(calc_paths)
        coros = []
        walltimes = []
        for calc_model in calc_models:
            try:
                coro = self._get_status_from_calc_model(
                        calc_model, is_symlinks[calc_model.calculation],
                        **kwargs)
            except KeyError:
                # this can happens sometimes when we're packaging calculations
                # at the same time we're reading statuses
                # just skip is_symlink part when that happens
                coro = self._get_status_from_calc_model(
                        calc_model, None, **kwargs)
            coros.append(coro)
        for future in tqdm.tqdm(
                asyncio.as_completed(coros),
                desc="Getting status", unit="calcs", total=len(coros)):
            data_this_calc = await future
            walltime = data_this_calc[-1]
            data.append(data_this_calc)
            if not is_scalar(walltime):
                # walltime is either an error string or None
                continue
            walltimes.append(walltime)
        if not walltimes:
            max_walltimes = None
        else:
            max_walltimes = max(walltimes)
        return data, max_walltimes

    def _get_calculation_path_string(
            self, calc_model: DBCalculation, no_shortening: bool) -> str:
        relpath = os.path.relpath(calc_model.calculation, self.pwd)
        if len(relpath) > 100 and not no_shortening:
            relpath = relpath[:50] + "[...]" + relpath[-50:]
        return relpath

    async def _is_calculation_in_queue(self, calc_model):
        calcdir = calc_model.calculation
        rundir = os.path.join(calcdir, "run")
        async with await PBSFile.from_calculation(
                calc_model.calculation,
                loglevel=self._loglevel) as pbs_file:
            if pbs_file.queuing_system == "local":
                return False
            async with __SYSTEM_QUEUE__ as queue:
                if calcdir in queue or rundir in queue:
                    return True
        return False

    def get_status_flag(self, status: dict, inqueue: bool) -> str:
        """Return status flag.

        Parameters
        ----------
        status: dict
            The status dictionary.
        inqueue: bool
            True if calculation is in queue. False otherwise.

        Returns
        -------
        str: The status flag.
        """
        flag = super().get_status_flag(status)
        if status["calculation_started"] is False:
            if not inqueue:
                flag += "/" + Colors.color_text(
                        "NOT IN QUEUE", "purple", "bold")
            return flag
        # calculation started
        if status["calculation_finished"] is False:
            return flag
        if status["calculation_finished"] == __ERROR_STRING__:
            # check if calc is also running
            if inqueue:
                flag += "/" + Colors.color_text(
                        "RUNNING", "yellow", "bold")
            return flag
        return flag

    def _colorify_calculation_path(
            self, calculation_path: str,
            calculation_status: str,
            is_symlink: bool,
            ) -> str:
        if is_symlink:
            # calculation was already packaged or manually linked
            return Colors.color_text(calculation_path, "darkblue")
        if "UNSTARTED" in calculation_status:
            return Colors.color_text(calculation_path, "cyan")
        if "RUNNING" in calculation_status:
            return Colors.color_text(calculation_path, "yellow")
        if "ERROR" in calculation_status:
            return Colors.color_text(calculation_path, "red")
        # from here, calculation has finished successfully
        if "NOT CONVERGED" in calculation_status:
            return Colors.color_text(calculation_path, "orange")
        return Colors.color_text(calculation_path, "green")

    async def _get_walltime(
            self,
            calc_model: DBCalculation,
            force: bool = False,
            force_errored: bool = False,
            ) -> float | str:
        # compute walltime
        if calc_model.status["calculation_finished"] is not True:
            # walltime not available
            return Colors.bold_text("not available")
        try:
            walltime = calc_model.walltime
            if walltime is None or force or force_errored:
                await calc_model.update_walltime(new_walltime=None)
                walltime = calc_model.walltime
        except Exception as err:
            self._logger.error(
                    "An error occured while getting walltime for "
                    f"Calc: '{calc_model.calculation}'.")
            if self._loglevel <= logging.DEBUG:
                self._logger.exception(err)
            walltime = __ERROR_STRING__
        if walltime is None:
            # for some reason, walltime is not available
            return Colors.bold_text("not available")
        if walltime == __ERROR_STRING__:
            return Colors.color_text("not available [err]", "yellow", "bold")
        return walltime

    async def _get_status_from_calc_model(
            self,
            calc_model: DBCalculation, is_symlink: bool,
            force: bool = False,
            force_errored: bool = False,
            no_shortening: bool = False,
            ) -> list:
        # update database model if needed
        stat = calc_model.status
        if calc_model.status is None or force:
            await calc_model.update_calculation_status(
                    new_status=None, recompute=True)
        else:
            if calc_model.status["calculation_finished"] is False:
                await calc_model.update_calculation_status(
                    new_status=None, recompute=True)
            elif stat["calculation_finished"] == __ERROR_STRING__ and (
                    force_errored):
                await calc_model.update_calculation_status(
                    new_status=None, recompute=True)
        # gather status strings
        calculation_path = self._get_calculation_path_string(
                calc_model, no_shortening)
        # check if calculation is in queue
        inqueue = await self._is_calculation_in_queue(calc_model)
        calculation_status = self.get_status_flag(calc_model.status, inqueue)
        walltime = await self._get_walltime(
                calc_model, force=force, force_errored=force_errored)
        calculation_path = self._colorify_calculation_path(
                calculation_path, calculation_status,
                is_symlink,
                )
        return [
                calculation_path, calculation_status,
                walltime,
                ]
