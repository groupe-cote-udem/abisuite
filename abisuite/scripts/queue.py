from .bases import BaseAbisuiteSubScript
from .. import __SYSTEM_QUEUE__
from ..colors import Colors


class AbisuiteQueueScript(BaseAbisuiteSubScript):
    """Executes the 'queue' subcommand of the abisuite script."""

    _loggername = "AbisuiteQueueScript"
    _subscript_name = "queue"
    _subscript_color = "yellow"

    async def cancel_all_calculations(self, dry=False):
        """Cancels all calculations that are running.

        Parameters
        ----------
        dry: bool, optional
            If True, only do a dry run.
        """
        print(Colors.bold_text("CANCELLING ALL running calculations..."))
        if dry:
            self._logger.info("Dry run only.")
        async with __SYSTEM_QUEUE__ as queue:
            for job in queue:
                if dry:
                    print(Colors.bold_text("Will cancel: ") +
                          job.calculation_directory.path)
                    continue
                print(Colors.bold_text("Cancelling: ") +
                      (await job.calculation_directory).path)
                job.cancel()

    async def cancel_calculations(self, dry=False):
        """Cancels calculations that are running.

        Parameters
        ----------
        dry: bool, optional
            If True, only do a dry run.
        """
        print(Colors.bold_text("CANCELLING running calculations..."))
        if dry:
            self._logger.info("Dry run only.")
        for calc_info in self.calculations:
            calculation = calc_info["calculation_directory"]
            async with __SYSTEM_QUEUE__ as queue:
                if calculation not in queue:
                    print(Colors.bold_text("Not running -> not cancelling:") +
                          calculation.path)
                    continue
                if dry:
                    print(Colors.bold_text("Will cancel: ") +
                          calculation.path)
                    continue
                print(Colors.bold_text("CANCELLING:") +
                      f"'{calculation.path}'.")
                queue.cancel_job(calculation)
            self.successfull(f"Cancelled job: {calculation.path}.")

    async def print_queue(self, **kwargs):
        """Prints the system queue."""
        async with __SYSTEM_QUEUE__ as queue:
            await queue.print(show_progress=True, **kwargs)
