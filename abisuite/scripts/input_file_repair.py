import asyncio
import os

from .bases import _BaseAbisuiteSubScriptMinimalLoadingReqs
from ..handlers import (
        CALCTYPES_TO_INPUT_FILE_CLS,
        MetaDataFile,
        )
from ..linux_tools import touch
from ..routines import full_abspath
from ..variables import CALCTYPES_TO_INPUT_VARIABLES


class AbisuiteInputFileRepairScript(_BaseAbisuiteSubScriptMinimalLoadingReqs):
    """Executes the 'inputrepair' part of the abisuite script.

    Attemps at repairing a possibly broken input file in a calculation
    directory.
    """

    _loggername = "AbisuiteInputFileRepairScript"
    _subscript_color = "yellow"
    _subscript_name = "inputrepair"

    async def repair(self, dry_run: bool = False) -> None:
        """Repair input files from calculations.

        Parameters
        ----------
        dry_run: bool, optional
            If True, only prints out calculations to be repaired.
        """
        coros = []
        for calc_info in self.calculations:
            to_repair = calc_info["path"]
            here = full_abspath(to_repair)
            print(  # noqa: T001
                    f"Trying to repair input file file in: '{here}'")
            if dry_run:
                continue
            coros.append(self._repair_single_calculation(here))
        await asyncio.gather(*coros)

    async def _repair_single_calculation(self, here: str) -> None:
        try:
            # first get calctype and input file path
            async with await MetaDataFile.from_calculation(
                            here, loglevel=self._loglevel) as meta:
                calctype = meta.calctype
                path = meta.input_file_path
                workdir = meta.calc_workdir
            async with await CALCTYPES_TO_INPUT_FILE_CLS[calctype].from_file(
                            path, loglevel=self._loglevel) as input_file:
                # we need to get the new working directory and hope its
                # basename is the same as the old one
                basename = os.path.basename(workdir)
                if basename not in input_file.path:
                    self.warning("Unable to repair in: '{here}'.")
                    return
                # ok can repair from here. get new relative paths
                try:
                    for varname, var in input_file.input_variables.items():
                        # add the sep. here to make sure the split goes well
                        if CALCTYPES_TO_INPUT_VARIABLES[calctype][varname].get(
                                    "path_convertible", False) is False:
                            # this variable cannot be converted
                            continue
                        # this variable can be converted => thus convert it!
                        new = os.path.join(
                                workdir,
                                var.value.split(basename + os.path.sep)[-1])
                        input_file.input_variables[varname] = new
                    else:
                        # loop ended, rewrite the input file
                        await input_file.write(overwrite=True)
                except ValueError as err:
                    self.warning(
                        "An error occured while trying to repair input file:"
                        f"'{path}' \n {err}. "
                        "At least we will write an empty file")
                    await touch(path)
                self.success("Repairing attempt done.")  # noqa: T001
        except FileNotFoundError:
            # this is not a calculation directory, skip it
            pass
