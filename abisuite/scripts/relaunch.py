import asyncio
import os
from typing import Any

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..handlers import CalculationDirectory, PBSFile
from ..handlers.file_handlers.bases import BaseInputFileHandler
from ..launchers import __ALL_LAUNCHERS__
from ..launchers.exceptions import TooManyCalculationsInQueueError
from ..launchers.launchers.bases import BaseLauncher


class AbisuiteRelaunchScript(BaseAbisuiteSubScript):
    """Executes the 'relaunch' subcommand of the abisuite script."""

    _loggername = "AbisuiteRelaunchScript"
    _subscript_color = "cyan"
    _subscript_name = "relaunch"

    async def relaunch(self, **kwargs):
        """Relaunches the calculations stored in the 'calculations' attribute.

        Parameters
        ----------
        build: str, optional
            If not None, will relaunch the calculations using this build
            defined in the user's config file.
        command_arguments: str, optional
            If not None, relaunches calculations using these command arguments.
        dry: bool, optional
            If True, dry mode is activated and only what will be relaunched
            will be printed on screen.
        input_variables_pop: list-like, optional
            If not None, gives the list of input variables to pop when
            relaunching the calculations.
        input_variables_update: dict, optional
            If not None, gives the dict of input variables that updates the
            input variables already there when relaunching calculations.
        jobname: str, optional
            If not None, states the new jobname for the job.
        memory_per_cpu: str, optional
            If not None, will relaunch the calculations using this memory
            per cpu attribute.
        mpi_command: str, optional
            If not None, will relaunch calculations using this mpi command.
        nodes: int, optional
            If not None, will relaunch calculations using this number of nodes.
            Is not compatible with the ntasks argument.
        ntasks: int, optional
            If not None, will relaunch calculations using this number of tasks.
            Not compatible with the nodes+ppn arguments.
        ppn: int, optional
            If not None, will relaunch calculations using this number of tasks
            per nodes. Not compatible with the ntasks argument.
        project_account: str, optional
            If not None, relaunch the calculations using this project account.
        queuing_system: str, optional
            If not None, will relaunch calculations using this queuing system.
            Usefull when relaunching in a 'local' way.
        walltime: str, optional
            If not None, will relaunch calculations using this walltime.
        """
        if not self.calculations:
            print(Colors.bold_text("No calculations to relaunch."))
            return
        await asyncio.gather(
                *(self._relaunch_calculation(
                    calc_info["calculation_directory"], **kwargs)
                    for calc_info in self.calculations))

    # TODO: split this method to make it shorter and more readable
    # (FG: 2021/10/04)
    async def _relaunch_calculation(
            self, calculation: CalculationDirectory,
            build: str = None,
            command_arguments: dict = None,
            dry: bool = False,
            input_variables_pop: list[str] = None,
            input_variables_update: dict[str, Any] = None,
            jobname: str = None,
            memory_per_cpu: str = None,
            mpi_command: str = None,
            nodes: int = None,
            ntasks: int = None,
            ppn: int = None,
            project_account: str = None,
            queuing_system: str = None,
            walltime: str = None,
            ignore_calculation_limit: bool = False,
            ) -> None:
        # relaunch a calculation
        relpath = os.path.relpath(calculation.path, self.pwd)
        async with calculation as calc:
            if dry:
                print(Colors.bold_text("Will relaunch:") +
                      f" '{relpath}'.")
                return
            else:
                print(Colors.bold_text("Relaunching") +
                      f": '{relpath}'")
            # cancel job if needed
            await self.cancel_job(calc)
            launcher = await self._get_launcher(calc)
            prt = "Relaunching with:"
            prt += await self._set_build(launcher, build)
            async with calc.pbs_file as pbs, calc.input_file as inp:
                prt += await self._set_mpi_command(pbs, mpi_command)
                prt += await self._set_command_arguments(
                        pbs, command_arguments)
                # do queuing system first as it has requirements
                # and can raise error if
                # some of them not fulfilled
                prt += await self._set_queuing_system(
                        pbs, queuing_system, project_account, jobname,
                        walltime,
                        memory_per_cpu, ntasks, ppn)
                prt += await self._set_memory_per_cpu(pbs, memory_per_cpu)
                prt += await self._set_ntasks(pbs, ntasks)
                prt += await self._set_jobname(pbs, jobname)
                prt += await self._set_project_account(pbs, project_account)
                prt += await self._set_nodes_ppn(pbs, nodes, ppn)
                prt += await self._set_walltime(pbs, walltime)
                prt += await self._update_input_variables(
                        inp, launcher, input_variables_update)
                prt += await self._pop_input_variables(
                        inp, input_variables_pop)
            options = (
                    build,
                    command_arguments,
                    input_variables_pop,
                    input_variables_update,
                    jobname,
                    memory_per_cpu,
                    mpi_command,
                    nodes,
                    ntasks,
                    ppn,
                    queuing_system,
                    walltime,
                    )
            if any([x is not None for x in options]):
                print(prt)
        try:
            if any([input_variables_update is not None,
                    input_variables_pop is not None]):
                # if we use abinit we need to define pseudo potential files
                if await calc.calctype == "abinit":
                    from abisuite.handlers import AbinitInputFile
                    async with await AbinitInputFile.from_calculation(
                            calc, loglevel=self._loglevel) as files:
                        await launcher.set_pseudos(
                                files.input_variables["pseudos"])
                from abisuite.handlers.file_approvers.exceptions import (
                        InputFileError,
                        )
                try:
                    # only validate input file
                    await launcher.validate(
                            only_validate_input_file=True,
                            )
                except InputFileError as err:
                    self.error("while validating input file.")
                    launcher._logger.exception(err)
                    return
            await self._clear_files_before_relaunch(calc)
            await launcher.launch(
                    ignore_calculation_limit=ignore_calculation_limit,
                    )
        except Exception as e:
            self.error(f"while relaunching: '{relpath}'.")
            launcher._logger.exception(e)
        except TooManyCalculationsInQueueError as err:
            self.error(str(err))
            self.error(
                "Too many calculations in queue, cannot relaunch: "
                f"'{relpath}'. Aborting NOW!")
            return
        else:
            self.successfull(f"relaunch '{relpath}'.")
            await calculation.reset_status()
        calculation.clear_content()

    async def _clear_files_before_relaunch(
            self,
            calc: CalculationDirectory,
            ) -> None:
        # if a log file is present, delete it
        log_file = await calc.log_file
        if await log_file.exists:
            await log_file.bump_copy()
            await log_file.delete()
        stderr_file = await calc.stderr_file
        # also delete stderr file
        if await stderr_file.exists:
            await stderr_file.bump_copy()
            await stderr_file.delete()
        await asyncio.gather(
                *[self._clear_handler_before_relaunch(handler)
                  for handler in await calc.walk(paths_only=False)])

    async def _clear_handler_before_relaunch(self, handler):
        # delete core files if there are any
        if handler.basename.startswith("core."):
            await handler.delete()
        # delete CRASH file if it exists
        if handler.basename == "CRASH":
            await handler.delete()

    async def _get_launcher(self, calc: CalculationDirectory) -> None:
        calctype = await calc.calctype
        launcher_cls = __ALL_LAUNCHERS__[calctype]
        launcher = launcher_cls(
                await calc.jobname, loglevel=self._loglevel)
        await launcher.set_calculation_directory(calc)
        return launcher

    async def _set_build(self, launcher: BaseLauncher, build: str) -> str:
        if build is None:
            return ""
        launcher.build = build
        await launcher.pbs_file.write(overwrite=True)
        return f" 'build'={build}"

    async def _set_jobname(
            self, pbs: PBSFile, jobname: str) -> str:
        if jobname is None:
            return ""
        pbs.jobname = jobname
        return f" 'jobname'={jobname}"

    async def _set_project_account(
            self, pbs: PBSFile, project_account: str) -> str:
        if project_account is None:
            return ""
        pbs.project_account = project_account
        return f" 'project_account'={project_account}'"

    async def _set_memory_per_cpu(
            self, pbs: PBSFile, memory_per_cpu: str) -> str:
        if memory_per_cpu is None:
            return ""
        pbs.memory_per_cpu = memory_per_cpu
        return f" 'memory_per_cpu={memory_per_cpu}'"

    async def _set_mpi_command(
            self, pbs: PBSFile, mpi_command: str) -> str:
        if mpi_command is None:
            return ""
        pbs.mpi_command = mpi_command
        return f" 'mpi_command={mpi_command}'"

    async def _set_ntasks(
            self, pbs: PBSFile, ntasks: int) -> str:
        if ntasks is None:
            return ""
        pbs.ntasks = int(ntasks)
        # need to reset total_ncpus
        # TODO: find a better way to do this (FG: 2023/05/17)
        # if we don't do this, relauncher complains when changing number of
        # tasks and number of pools (does not recompute total_ncpus)
        pbs.structure._total_ncpus = None
        return f" 'ntasks'={ntasks}"

    async def _set_nodes_ppn(
            self, pbs: PBSFile, nodes: int, ppn: int) -> str:
        if nodes is None:
            if ppn is not None:
                raise ValueError("Need to set nodes if setting ppn.")
            return ""
        if ppn is None:
            if nodes is not None:
                raise ValueError("Need to set ppn if setting nodes.")
            return ""
        pbs.nodes = int(nodes)
        pbs.ppn = int(ppn)
        pbs.ntasks = None
        return f" 'nodes'={nodes} 'ppn'={ppn}"

    async def _set_command_arguments(
            self, pbs: PBSFile, command_arguments: str) -> str:
        if command_arguments is None:
            return ""
        pbs.command_arguments = command_arguments
        return f" 'command_arguments'={command_arguments}"

    async def _set_queuing_system(
            self, pbs: PBSFile, queuing_system: str,
            project_account: str, jobname: str, walltime: str,
            memory_per_cpu: str,
            ntasks: int, ppn: int,
            ) -> str:
        if queuing_system is None:
            return ""
        if pbs.queuing_system == "local":
            # need to supply some more info
            if project_account is None:
                raise ValueError(
                        "Need to set 'project_account'.")
            if jobname is None:
                raise ValueError("Need to set 'jobname'.")
            if walltime is None:
                raise ValueError("Need to set 'walltime'.")
            if memory_per_cpu is None:
                raise ValueError(
                        "Need to set 'memory'.")
            if ntasks is None and ppn is None:
                raise ValueError(
                        "Need to set 'ntasks' or 'ppn+nodes'.")
        pbs.queuing_system = queuing_system
        return f" 'queuing_system'={queuing_system}"

    async def _set_walltime(
            self, pbs: PBSFile, walltime: str) -> str:
        if walltime is None:
            return ""
        pbs.walltime = walltime
        return f" 'walltime'={walltime}"

    async def _update_input_variables(
            self, inp: BaseInputFileHandler, launcher: BaseLauncher,
            input_variables_update: dict[str, Any],
            ) -> str:
        if input_variables_update is None:
            return ""
        try:
            inp.input_variables.update(input_variables_update)
        except ValueError as err:
            self.error("while updating variables.")
            launcher._logger.exception(err)
            return
        return f" updated input variables = {input_variables_update}"

    async def _pop_input_variables(
            self, inp: BaseInputFileHandler,
            input_variables_pop: list[str]) -> str:
        if input_variables_pop is None:
            return ""
        for var in input_variables_pop:
            if var not in inp.input_variables:
                continue
            inp.input_variables.pop(var)
        return f" popped input variables = {input_variables_pop}"
