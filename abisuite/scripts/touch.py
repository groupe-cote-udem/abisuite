import tqdm

from .bases import BaseAbisuiteSubScript
from ..handlers import SlurmFilesToDeleteFile
from ..linux_tools import touch


class AbisuiteTouchScript(BaseAbisuiteSubScript):
    """Executes the 'touch' subcommand of the abisuite script."""

    _loggername = "AbisuiteTouchScript"
    _subscript_color = "blue"
    _subscript_name = "touch"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.files_to_touch = []

    async def load_files_to_touch(
            self, slurm_files_to_delete_path: str) -> None:
        """Load the files to delete from the 'slurm_files_to_delete' file."""
        async with await SlurmFilesToDeleteFile.from_file(
                slurm_files_to_delete_path, loglevel=self._loglevel) as slurm:
            self.files_to_touch += slurm.files_to_delete

    async def touch_files(self) -> None:
        """Touch files."""
        print(f"There are {len(self.files_to_touch)} files to touch.")
        # don't do async as there can be too many files opened at same time.
        for path in tqdm.tqdm(
                self.files_to_touch,
                desc="Touching files", unit="files",
                total=len(self.files_to_touch)):
            try:
                await touch(path, sync_open=True)
            except FileNotFoundError:
                # maybe already deleted
                continue
