import subprocess

import aiofiles.os

from .bases import BaseAbisuiteVisualizeSubScript
from ..colors import Colors
from ..linux_tools import which


class AbisuiteVisualizeStructureScript(BaseAbisuiteVisualizeSubScript):
    """Executes the 'visualize_structure' subcommand of the abisuite script."""

    _loggername = "AbisuiteVisualizeStructureScript"
    _subscript_color = "Green"
    _subscript_name = "visualize_structure"

    async def visualize_structure(self, vesta: str = None) -> None:
        """Visualize structure using any means necessary.

        Depending on what data files have been loaded/found, the correct
        software to vusalize structure will be automatically detected.
        """
        if "xsf" in self.data_files:
            await self.visualize_structure_from_xsf(
                    self.data_files["xsf"], vesta=vesta,
                    )
        elif "qe_input" in self.data_files:
            await self.visualize_structure_from_qe_input(
                    self.data_files["qe_input"])

    async def visualize_structure_from_qe_input(self, qe_input):
        """Vsualize the crystal structure by calling XCrysden on the qe input.

        Xcrysden msut be installed otherwise an error occurs.
        """
        if await which("xcrysden") is not None:
            print(Colors.bold_text("Launching XCrysden:") +
                  f" on QE input file {qe_input}.")
            subprocess.run(["xcrysden", "--pwi", qe_input])
        else:
            self.error("'xcrysden' not accessible. Install it.")

    async def visualize_structure_from_xsf(
            self, xsf_file: str, vesta: str = None) -> None:
        """Visualize the crystal structure by calling VESTA.

        VESTA is called on the
        (possibly generated) xsf file representing the electronic density.
        """
        # launch vesta if it is available
        if vesta is not None:
            self._logger.info(f"User specified vesta path: {vesta}")
            if not await aiofiles.os.path.isfile(vesta):
                raise FileNotFoundError(vesta)
        else:
            vesta = await which("vesta") or await which("VESTA")
            if vesta is None:
                self.error("'vesta/VESTA' not available in 'PATH'.")
        self._logger.info(f"Found vesta program at: {vesta}")
        self._logger.info("Launching 'vesta' to visualize structure.")
        subprocess.run([vesta, xsf_file])
