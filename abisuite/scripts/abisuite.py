#! /usr/bin/env python3
# Make sure no major python import is done prior to execution
# this is to speedup execution of script
import argparse
import ast
import asyncio
import logging
import os
import sys
import time
from functools import partial, wraps
from typing import Callable

import aiofiles.os


logo = ("""
  __ _| |__ (_)___ _   _(_) |_ ___
 / _` | '_ \\| / __| | | | | __/ _ \\
| (_| | |_) | \\__ \\ |_| | | ||  __/
 \\__,_|_.__/|_|___/\\__,_|_|\\__\\___|
""")


def keyboard_interrupt_wrap(func):
    """Wrap functions and prints a nice message in case of Keyboard interrupt.

    This way, we don't see an annoying
    super long traceback that we don't care anyway.
    """
    async def wrapped(*args, **kwargs):
        try:
            if asyncio.iscoroutinefunction(func):
                return await func(*args, **kwargs)
            return func(*args, **kwargs)
        except KeyboardInterrupt:
            print("\nYou killed me using your keyboard :(")
    return wrapped


def time_function_execution(
        func: Callable = None, print_arguments: bool = True) -> Callable:
    """Wrapper that prints timing info about a function.

    Can be called with or without arguments.

    Parameters
    ----------
    func: function
        The function that we want to time.
    print_arguments: bool, optional
        If True (default), the timed function arguments will be printed
        out if any.
    """
    # decorator that can be called with or without args. See:
    # https://stackoverflow.com/a/39335652/6362595
    if func is None:
        # decorator called without any args
        return partial(
                time_function_execution, print_arguments=print_arguments)

    @wraps(func)
    async def timed_func(*args, **kwargs):
        print(f"Timing function '{func.__name__}'")
        if args and print_arguments:
            print(f"Arguments are: {args}")
        if kwargs and print_arguments:
            print(f"Keyword arguments are {kwargs}.")
        print()
        start = time.perf_counter()
        try:
            if asyncio.iscoroutinefunction(func):
                to_return = await func(*args, **kwargs)
            else:
                to_return = func(*args, **kwargs)
        finally:
            # do the import here as it will not slow down code furthermore
            from abisuite.scripts.routines import walltime_to_str
            end = time.perf_counter()
            print(f"\nTotal function '{func.__name__}' walltime execution: "
                  f"{walltime_to_str(end - start)}.")
        return to_return
    return timed_func


__ALL_SUBSCRIPTS__ = (
        "clean", "config", "db", "debug", "grep", "inputrepair",
        "math", "metadatarepair",
        "mv", "package", "plot", "queue", "relaunch", "rm", "status",
        "touch", "visualize_charge", "visualize_structure",
        )


class AbisuiteArgParser:
    """Arg parser for abisuite script."""

    # scripts that support remote access
    _remote_available_subscripts = (
            "clean", "package", "relaunch", "rm", "status", )
    # scripts that handles many calculations
    _scripts_handles_many_calcs = (
            "clean", "db", "debug", "grep", "inputrepair",
            "metadatarepair", "mv", "package",
            "queue", "relaunch", "rm", "status",
            )

    def __init__(self):
        # some checks for consistency
        for subscript in self._remote_available_subscripts:
            if subscript not in __ALL_SUBSCRIPTS__:
                raise ValueError(subscript)
        for subscript in self._scripts_handles_many_calcs:
            if subscript not in __ALL_SUBSCRIPTS__:
                raise ValueError(subscript)
        self.parsers = self.create_parser()

    def create_parser(self):
        """Create main parser and sub parsers.

        Returns
        -------
        dict: the dictionary containing all parsers.
        """
        main_parser = argparse.ArgumentParser(
                add_help=True,
                description=(
                    "The abisuite script consists of multiple command line "
                    "tools in order to manage, monitor, (re)launch and modify "
                    "calculations."),
                prog="abisuite",
                epilog=("For any bugs or to contribute, please see: "
                        "https://gitlab.com/groupe-cote-udem/abisuite"),
                )
        main_parser.add_argument(
                "--version", help="Print abisuite version tag.",
                action="store_true",
                )
        subparsers = main_parser.add_subparsers(
                help="The abisuite script is split into many 'subscripts'.",
                dest="subcommand", required=False,
                )
        parsers = {
                "main": main_parser,
                }
        # create all subparsers
        for subscript in __ALL_SUBSCRIPTS__:
            parsers[subscript] = getattr(
                    self, f"get_{subscript}_subparser")(subparsers)
        # common arguments to all parsers
        for subparser_name, subparser in parsers.items():
            if subparser_name == "main":
                continue
            # add loglevel to all subparsers
            subparser.add_argument(
                "--loglevel", "-l", action="store", type=int,
                default=logging.INFO,
                help=("Sets the logging level."))
            # add profiling to all subparsers
            subparser.add_argument(
                    "--profile", action="store_true",
                    help=("Profile abisuite command. A 'profile.prof' file is"
                          " created to visualize using 'snakeviz' package."),
                    )
            # add a 'calculations' argument that takes any number of
            # 'calculations' path.
            if subparser_name in self._scripts_handles_many_calcs:
                subparser.add_argument(
                    "calculations", action="store", type=str,
                    help=(
                        "Calculation(s) root(s) that we want to manipulate."),
                    default=None,
                    nargs="*",
                    )
            # add remote option to the 'remote' available subscripts
            if subparser_name in self._remote_available_subscripts:
                # some scripts don't support 'remote' or remote has a different
                # meaning which is described in the corresponding methods.
                subparser.add_argument(
                    "--remote", action="store", type=str,
                    default=None,
                    help=(
                        "Execute abisuite script on given remote. "
                        "The stdout and stderr of this action will be printed."
                        ),
                    )
        return parsers

    def get_clean_subparser(self, parser):
        """Gets the 'clean' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "clean",
                help=(
                    "Deletes unnecessary files after calculations have been "
                    "run in order to save some disk space and file numbers. "
                    " Only calculation that have finished and converged will "
                    "be cleaned by default unless the --force option is used."
                    ),
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=(
                    "Force clean calculations even if they are "
                    "not finished/converged"))
        subparser.add_argument(
                "--dry", action="store_true",
                help=(
                    "Dry run: only prints which files will be deleted in each"
                    " repository. Also displays total amount of disk memory "
                    "that will be freed."))
        subparser.set_defaults(func=abisuite_clean)
        return subparser

    def get_config_subparser(self, parser):
        """Gets the 'config' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "config",
                help=(
                    "Handles user config file. Sets and gets information from"
                    " it."
                    ),
                )
        subparser.add_argument(
                "--show-builds", action="store_true",
                help=(
                    "List out all builds stored in the config file."
                    ),
                )
        subparser.add_argument(
                "--show-build", action="store", nargs="+", default=None,
                help=(
                    "Prints out info on the given build. Can be called "
                    "multiple times."
                    ),
                )
        subparser.add_argument(
                "--show-max-jobs-in-queue", action="store_true",
                help=(
                    "Prints out the maximum number of jobs in queue."
                    ),
                )
        subparser.add_argument(
                "--show-db-path", action="store_true",
                help=(
                    "Prints out the database path."
                    ),
                )
        subparser.add_argument(
                "--set-max-jobs-in-queue", action="store",
                help=("Set the maximum number of jobs in queue. "
                      "If set to 'no_limit', then no limit is applied on the "
                      "number of jobs in queue."),
                )
        subparser.add_argument(
                "--set-default-db-path", action="store", default=None,
                type=str,
                help=("Sets the default database path to this one."))
        subparser.add_argument(
                "--set-default-project-account", action="store", default=None,
                type=str,
                help=("Sets the default project account to this one."))
        subparser.add_argument(
                "--set-default-pseudos-dir", action="store", default=None,
                type=str,
                help=("Sets the default pseudos directory to this one."))
        subparser.add_argument(
                "--set-package-dest-root", action="store", default=None,
                type=str, help=(
                    "Sets the package destination root."))
        subparser.add_argument(
                "--set-package-src-root", action="store", default=None,
                type=str, help=(
                    "Sets the package source root."))
        subparser.add_argument(
                "--set-queuing-system", action="store", default=None,
                type=str,
                help=("Sets the queuing system to this one."))
        subparser.add_argument(
                "--set-username", action="store", default=None,
                type=str,
                help=("Sets the username to this one."))
        subparser.add_argument(
                "--show-config", action="store_true",
                help=("Print out the config file path."),
                )
        subparser.add_argument(
                "--show-default-project-account", action="store_true",
                help=("Show the default project account."),
                )
        subparser.add_argument(
                "--show-default-pseudos-dir", action="store_true",
                help=("Show the default pseudos directory."),
                )
        subparser.add_argument(
                "--show-package-dest-root", action="store_true",
                help=(
                    "Shows the package destination root."))
        subparser.add_argument(
                "--show-package-src-root", action="store_true",
                help=(
                    "Shows the package source root."))
        subparser.add_argument(
                "--show-queuing-system", action="store_true",
                help=("Show the queuing system."),
                )
        subparser.add_argument(
                "--show-username", action="store_true",
                help=("Show the username."),
                )
        subparser.add_argument(
                "--show-qe", action="store_true",
                help=("Show QE default values."),
                )
        subparser.set_defaults(func=abisuite_config)
        return subparser

    def get_db_subparser(self, parser):
        """Gets the 'db' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "db",
                help=(
                    "Access database attributes for the given calculations. "
                    "This command can also manage a databse in itself."
                    ),
                )
        subparser.add_argument(
                "--add", action="store_true",
                help="Add the calculation(s) to the database.")
        subparser.add_argument(
                "--all", action="store_true",
                help="Show all calculations from the database.")
        subparser.add_argument(
                "--all-monitored", action="store_true",
                help="Show all monitored calculations from the database.")
        subparser.add_argument(
                "--clean", action="store_true",
                help=("Cleans calculations that are stored in the database but"
                      " that don't exist anymore."))
        subparser.add_argument(
                "--check-integrity", action="store_true",
                help=("Checks database integrity then leave."))
        subparser.add_argument(
                "--download", action="store_true",
                help=("Download remote database. If not set, I'll try to use "
                      "a cached previously downloaded database."))
        subparser.add_argument(
                "--force", action="store_true",
                help=("Forces abidb operations and bypass integrity checks."))
        subparser.add_argument(
                "--follow-symlinks", action="store_true",
                help=("Follows symlinks."),
                )
        # get arguments to get or set data from database
        subparser.add_argument(
                "--get-id", action="store_true",
                help="Get the calculation(s) id(s) from the database.")
        subparser.add_argument(
                "--get-status", "--get-calculation-status",
                action="store_true",
                help="Get calculation status from database.")
        subparser.add_argument(
                "--get-monitored", action="store_true",
                help="Returns True if calculation is monitored.")
        subparser.add_argument(
                "--get-packaged", action="store_true",
                help="Get the 'packaged' attribute.")
        subparser.add_argument(
                "--get-project", action="store_true",
                help="Returns the project tag of the calculation.")
        subparser.add_argument(
                "--get-walltime", action="store_true",
                help="Prints the calculation walltime if it is finished.")
        subparser.add_argument(
                "--set-monitored-false", action="store_true",
                help="Set the 'monitored' attribute to False.")
        subparser.add_argument(
                "--set-monitored-true", action="store_true",
                help="Set the 'monitored' attribute to True.")
        subparser.add_argument(
                "--set-packaged-false", action="store_true",
                help="Set the 'packaged' attribute to False.")
        subparser.add_argument(
                "--set-packaged-true", action="store_true",
                help="Set the 'packaged' attribute to True.")
        subparser.add_argument(
                "--set-project", action="store", type=str, default=None,
                help="Set the project tag of a given calculation to this.")
        subparser.add_argument(
                "--reset-status", action="store_true",
                help=(
                    "Resets the 'status' (and also 'walltime') database "
                    "attributes for the given calculations."
                    ),
                )
        subparser.add_argument(
                "--update-calculation-status", action="store",
                type=ast.literal_eval,
                help="Update the calculation status with the given dict.",
                )
        subparser.add_argument(
                "--database", action="store", type=str,
                default="default",
                help="The database to use. The default is in config file.")
        subparser.add_argument(
                "--delete", action="store_true",
                help="Delete the calculation(s) from the database.")
        subparser.add_argument(
                "--remote", action="store", type=str,
                default=None,
                help="Use the default database from given remote.")
        subparser.add_argument(
                "--init-db", action="store", default=None,
                help=(
                    "If this flag is activated, a database is created here "
                    "with the name given by this flag. And then it exit."))
        subparser.add_argument(
                "--refresh", action="store_true",
                help=("Refreshes the calculation status database entry for "
                      "selected calculations. If remote is set, all "
                      "monitored calculations will be refreshed on the remote "
                      "machine."))
        subparser.add_argument(
                "--show-db", action="store_true",
                help="Print the database file path and exit.")
        subparser.set_defaults(func=abisuite_db)
        return subparser

    def get_debug_subparser(self, parser):
        """Gets the 'debug' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "debug",
                help=(
                    "Prints out debug information for a calculation that "
                    "errored. Uses 'seff' for slurm systems. Also prints "
                    "content of the '.stderr' file if it exists."
                    ),
                )
        subparser.add_argument(
                "--show-input-file", "--input-file",
                "--in", action="store_true",
                help=("Prints out the content of the input file on screen."),
                )
        subparser.add_argument(
                "--show-log-file", "--print-log",
                "--print-log-file", "--log",
                action="store_true",
                help=("Prints out the content of the log file on screen."),
                )
        subparser.add_argument(
                "--all", "-a", action="store_true",
                help=("If set, all calculations are debugged, regardless if "
                      "they errored or not."),
                )
        subparser.add_argument(
                "--stderr-line-limit", action="store", type=int, default=30,
                help=(
                    "Maximum number of lines to be printed from stderr file."),
                )
        subparser.set_defaults(func=abisuite_debug)
        return subparser

    def get_grep_subparser(self, parser):
        """Gets the 'grep' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "grep",
                help=(
                    "Prints out some information that is 'grepped' from the "
                    "given calculations."
                    ),
                )
        subparser.add_argument(
                "--input-variable", action="append", type=str,
                default=None,
                help=(
                    "Search calculation(s) for this input variable. This "
                    "argument can be used multiple times."),
                )
        subparser.add_argument(
                "--log-file-attribute", action="store", type=str,
                default=None,
                help=("Search the calculation(s) for this log file attribute."
                      ),
                )
        subparser.add_argument(
                "--output-file-attribute", action="store", type=str,
                default=None,
                help=("Search the calculation(s) for this output file "
                      " attribute (Abinit only)."
                      ),
                )
        subparser.add_argument(
                "--key", action="store", type=str,
                default=None,
                help=(
                    "If required attribute "
                    "is a dict, this will select one of it's key to print."
                    )
                )
        subparser.add_argument(
                "--item", action="store", type=int, default=None,
                help=("If requested attribute is list-like, print "
                      "this item number."))
        subparser.set_defaults(func=abisuite_grep)
        return subparser

    def get_inputrepair_subparser(self, parser):
        """Gets the 'inputrepair' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "inputrepair",
                help=(
                    "Attempts at repairing a possibly broken input file."
                    ),
                )
        subparser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, do nothing. Just print what happens.")
        subparser.set_defaults(func=abisuite_inputrepair)
        return subparser

    def get_math_subparser(self, parser):
        """Gets the 'math' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "math",
                help=(
                    "Can executes simple math queries using command line."
                    ),
                )
        subparser.add_argument(
                "--find-divisors", action="store", type=int, default=None,
                help="Prints out the divisors of the given int.")
        subparser.set_defaults(func=abisuite_math)
        return subparser

    def get_metadatarepair_subparser(self, parser):
        """Gets the 'metadatarepair' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "metadatarepair",
                help=(
                    "Attempts at repairing a possibly broken meta data file."
                    ),
                )
        subparser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, do nothing. Just print what happens.")
        subparser.set_defaults(func=abisuite_metadatarepair)
        return subparser

    def get_mv_subparser(self, parser):
        """Gets the 'mv' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "mv",
                help=("Moves a calculation and make sure to update "
                      "parent and children calculation. Also tries to update "
                      "paths within calculation."
                      ),
                )
        # add this one here to make sure the positional arg comes before the
        # output positional arg.
        subparser.add_argument(
            "calculations", action="store", type=str,
            help=(
                "Calculation(s) root(s) that we want to manipulate."),
            default=None,
            nargs="*",
            )
        subparser.add_argument(
                "output", action="store", type=str,
                help=("Where to move the calculation(s). If dir does not exist"
                      " it is created"))
        subparser.add_argument(
                "--dry-run", "-d", action="store_true",
                help="Dry run mode, don't do nothing. Just print what happens")
        subparser.add_argument(
                "--completed-only", "-c", action="store_true",
                help="Move only completed calculations.")
        subparser.add_argument(
                "--recursive", "-r", action="store_true",
                help="Recursive mode.")
        subparser.set_defaults(func=abisuite_mv)
        return subparser

    def get_package_subparser(self, parser):
        """Gets the 'package' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "package",
                help=(
                    "Saves important files into a packaging directory from "
                    "a packaging src while keeping the same directory/file "
                    "hierarchy."),
                )
        subparser.add_argument(
                "--dry", "--dry-run", action="store_true",
                help=("Dry mode: only displays what will be done."),
                )
        subparser.add_argument(
                "--store-package-sources", action="store", default=None,
                type=str,
                help=(
                    "When dry-running, store the files/calculations to package"
                    " into the file given by this argument."
                    ),
                )
        subparser.add_argument(
                "--overwrite", action="store_true",
                help="Overwrite already packaged files with the same path.")
        subparser.add_argument(
                "--all", action="store_true",
                help=("Package all calculations "
                      "(default is only successfull ones)."),
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=(
                    "Force packaging even if calculation was already packaged"
                    " before. This option sets the 'overwrite' option to True."
                    ),
                )
        subparser.add_argument(
                "--from-file", action="store", default=None, type=str,
                help=(
                    "Load calculation(s) to package from this file."
                    ),
                )
        subparser.add_argument(
                "--replace-with-symlink", action="store_true",
                help=(
                    "Packaged calculations will be replaced with symlinks."
                    ),
                )
        subparser.add_argument(
                "--keep-density", action="store_true",
                help=(
                    "Package charge density files that would otherwise be "
                    "deleted or left out."
                    ),
                )
        subparser.add_argument(
                "--keep-wavefunctions", action="store_true",
                help=(
                    "Package wavefunction files that would otherwise be "
                    "deleted or left out."
                    ),
                )
        subparser.add_argument(
                "--use-slurm-files-to-delete", action="store", type=str,
                help=(
                    "If this flag is activated, the 'calculations' given is "
                    "not taken into account. Instead, the file stored in this "
                    "flag will be parsed as a 'slurm files to be deleted file'"
                    " which stores all files that will be purged. "
                    "All files that should be packaged in this file will be."
                    ),
                )
        subparser.add_argument(
                "-s", "--skip-packaging-other-files", action="store_true",
                help=(
                    "Skips packaging files outside calculations."
                    ),
                )
        subparser.set_defaults(func=abisuite_package)
        return subparser

    def get_plot_subparser(self, parser):
        """Gets the 'plot' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "plot",
                help=(
                    "Handles plot pickles directly from the command line. "
                    "It can show plot and modify it."),
                )
        subparser.add_argument(
                "plot_pickle", action="store", type=str, nargs="*",
                help="The plot pickle(s) to handle.", default=None)
        subparser.add_argument(
                "--show", action="store_true",
                help="Shows the plot in the end."
                )
        subparser.add_argument(
                "--ylims", action="store", nargs=2, type=float,
                help=(
                    "Sets the ylims of the plot. Exemple usage --ylims -1 1 "
                    "will set the ylims from -1 to 1."),
                default=None)
        subparser.add_argument(
                "--xlims", action="store", nargs=2, type=float,
                help=(
                    "Sets the xlims of the plot. Exemple usage --xlims -1 1 "
                    "will set the xlims from -1 to 1."),
                default=None)
        subparser.add_argument(
                "--save", action="store_true",
                help=("Saves the plot in pdf format to the same pickle path "
                      "but with the pdf extension."))
        subparser.add_argument(
                "--save-pickle", action="store_true",
                help=(
                    "Will overwrite the previous pickle that was used to "
                    "load the plot."))
        subparser.set_defaults(func=abisuite_plot)
        return subparser

    def get_queue_subparser(self, parser):
        """Gets the 'queue' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "queue",
                help=(
                    "Handles the queue system. Can cancel jobs and do other "
                    "stuff system wise."
                    ),
                )
        subparser.add_argument(
                "--tablefmt", action="store", type=str,
                default="simple",
                help=("[OPTIONAL]: table format from tabulate. See: "
                      "https://pypi.org/project/tabulate/"))
        subparser.add_argument(
                "--cancel", action="store_true",
                help=(
                    "Cancel jobs of calculations if they are running."
                    ),
                )
        subparser.add_argument(
                "--cancel-all", action="store_true",
                help=("Cancel all jobs."),
                )
        subparser.add_argument(
                "--show-queue", action="store_true",
                help=(
                    "Shows the queue."
                    ),
                )
        subparser.add_argument(
                "--no-shortening", action="store_false",
                help=(
                    "When showing queue, if this flag is active, do not "
                    "shorten the path to the calculation(s)."
                    ),
                )
        subparser.add_argument(
                "--dry", action="store_true",
                help=(
                    "When cancelling calculations, only do a dry run."),
                )
        subparser.set_defaults(func=abisuite_queue)
        return subparser

    def get_relaunch_subparser(self, parser):
        """Gets the 'relaunch' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "relaunch",
                help=("Relaunch calculation(s). Only calculations that did not"
                      " successfully complete will be relaunched unless "
                      "the --force option is used. Thus, running calculations "
                      "and calculations in the queue will be skipped."),
                )
        subparser.add_argument(
                "--build", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this build."),
                )
        subparser.add_argument(
                "--command-arguments", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with these new command arguments."
                    ),
                )
        subparser.add_argument(
                "--dry", action="store_true",
                help=(
                    "Dry mode: only what would be done is printed on screen "
                    "then the script exits."))
        subparser.add_argument(
                "--follow-symlinks", action="store_true",
                help=("Follows symlinks."),
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=(
                    "Force restart calculations even if they successfully "
                    "finished."))
        subparser.add_argument(
                "--input-variables-pop", action="store",
                nargs="+", default=None,
                help=("Pops the list of variables given to this arg."))
        subparser.add_argument(
                "--input-variables-update", "--update-input-variables",
                "--input-variable-update",
                type=ast.literal_eval,
                help="Update the input variables with the given dict.",
                )
        subparser.add_argument(
                "--jobname", action="store", type=str, default=None,
                help=(
                    "Relaunch job with this new jobname."
                    ),
                )
        subparser.add_argument(
                "--memory-per-cpu", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this new memory-per-cpu "
                    "attribute."))
        subparser.add_argument(
                "--mpi-command", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this new mpi-command "
                    "attribute."))
        subparser.add_argument(
                "--nodes", action="store", type=int,
                help=("Relaunch with this number of nodes. "
                      "Incompatible with ntasks. Needs to set 'ppn' as well.")
                )
        subparser.add_argument(
                "--ntasks", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this new ntasks attribute.")
                )
        subparser.add_argument(
                "--ppn", action="store", type=int,
                help=("Relaunch with this number of tasks per nodes. "
                      "Incompatible with ntasks. Needs to set 'nodes' as well."
                      )
                )
        subparser.add_argument(
                "--project-account", "--project_account", action="store",
                type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this project account."),
                )
        subparser.add_argument(
                "--queuing_system", "--queuing-system", action="store",
                type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this new queuing_system "
                    "attribute."))
        subparser.add_argument(
                "--walltime", action="store", type=str,
                default=None,
                help=(
                    "Relaunch calculation(s) with this new walltime "
                    "attribute."))
        subparser.add_argument(
                "--only-unstarted", action="store_true",
                help=(
                    "Only (re)launch unstarted calculations."),
                )
        subparser.add_argument(
                "--relaunch-unstarted", action="store_true",
                help=(
                    "Allow to relaunch unstarted calculations. Watch out "
                    "if they're still in queue though! You could launch twice"
                    " a calculation it is still in queue."
                    ),
                )
        subparser.add_argument(
                "--relaunch-finished", action="store_true",
                help=(
                    "Allow to relaunch finished calculations."),
                )
        subparser.add_argument(
                "--ignore-calculation-limit", action="store_true",
                help=(
                    "If this flag is activated, we ignore the calculation "
                    "set in the config file. Otherwise, we stop launching "
                    "calculations after the limit is reached."
                    ),
                )
        subparser.set_defaults(func=abisuite_relaunch)
        return subparser

    def get_rm_subparser(self, parser):
        """Gets the 'rm' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "rm",
                help=(
                    "Deletes calculation(s) and removes them from the database"
                    " if it is present in it."
                    ),
                )
        subparser.add_argument(
                "--dry", action="store_true",
                help="Shows what will be done and then stop."
                )
        subparser.add_argument(
                "--force", "-f", action="store_true",
                help=(
                    "Whatever happens, try to delete the calculation(s). "
                    "USE WITH CAUTION! USE --dry BEFOREHAND!"
                    ),
                )
        subparser.add_argument(
                "--only-errored", action="store_true",
                help="Only delete calculations that 'errored'.",
                )
        subparser.add_argument(
                "--follow-symlinks", action="store_true",
                help=("Follows symlinks."),
                )
        subparser.set_defaults(func=abisuite_rm)
        return subparser

    def get_status_subparser(self, parser):
        """Gets the 'status' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "status",
                help=("Gives status of calculation(s)."),
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=(
                    "Force recomputing status instead of looking into the DB."
                    ),
                )
        subparser.add_argument(
                "--force-errored", action="store_true",
                help=(
                    "Same as --force but only for errored calculation."
                    ),
                )
        subparser.add_argument(
                "--sort-by-walltime", action="store_true",
                help="Sort calculations by walltime.",
                )
        subparser.add_argument(
                "--no-shortening", action="store_true",
                help="Don't shorten calculation paths.",
                )
        subparser.set_defaults(func=abisuite_status)
        return subparser

    def get_touch_subparser(self, parser):
        """Gets the 'touch' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "touch",
                help=(
                    "Touch all the files from a slur 'files_to_delete' doc."
                    ),
                )
        subparser.add_argument(
                "slurm_files_to_delete", action="store", type=str,
                default=None,
                help="The file containing all the files to touch.")
        subparser.set_defaults(func=abisuite_touch)
        return subparser

    def get_visualize_charge_subparser(self, parser):
        """Gets the 'visualize_charge' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "visualize_charge",
                help=("Prepare a DEN file for visualization by the VESTA "
                      "program if available."),
                )
        subparser.add_argument(
                "calculation_or_den_file",
                action="store",
                help=("Calculation whose DEN file we want to visualize or "
                      "directly the path towards the DEN file to viusalize."),
                nargs=1,
                type=str,
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=("If XSF file already exists, force to regenerate."))
        subparser.set_defaults(func=abisuite_visualize_charge)
        return subparser

    def get_visualize_structure_subparser(self, parser):
        """Gets the 'visualize_structure' subparser.

        Parameters
        ----------
        subparsers: Action instance
            The object returned by the 'parser.add_subparsers(...)' method.

        Returns
        -------
        The subparser.
        """
        subparser = parser.add_parser(
                "visualize_structure",
                help=("Prepare a DEN file for visualization by the VESTA "
                      "program if available."),
                )
        subparser.add_argument(
                "calculation_or_den_file",
                action="store",
                help=("Calculation whose DEN file we want to visualize or "
                      "directly the path towards the DEN file to viusalize."),
                nargs=1,
                type=str,
                )
        subparser.add_argument(
                "--force", action="store_true",
                help=("If XSF file already exists, force to regenerate."))
        subparser.add_argument(
                "--vesta", action="store", type=str,
                help=(
                    "Path to vesta executable. If not given, will search "
                    "PATH variable for vesta/VESTA."),
                default=None,
                )
        subparser.set_defaults(func=abisuite_visualize_structure)
        return subparser

    def parse_args(self):
        """Parses command line arguments.

        Returns
        -------
        list: The list of command line arguments·
        """
        return self.parsers["main"].parse_args()


def get_abisuite_argparser():
    """Returns the abisuite arg parser."""
    return AbisuiteArgParser()


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_clean(args):
    """Executes the 'clean' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.clean import AbisuiteCleanScript
    loglevel = args.loglevel
    script = AbisuiteCleanScript(loglevel=loglevel)
    await script.load_calculations(args.calculations)
    await script.clean(dry=args.dry, force=args.force)


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
def abisuite_config(args):
    """Executes the 'config' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.config import AbisuiteConfigScript
    script = AbisuiteConfigScript(loglevel=args.loglevel)
    script.set_config(
            default_db_path=args.set_default_db_path,
            default_project_account=args.set_default_project_account,
            default_pseudos_dir=args.set_default_pseudos_dir,
            max_jobs_in_queue=args.set_max_jobs_in_queue,
            package_dest_root=args.set_package_dest_root,
            package_src_root=args.set_package_src_root,
            queuing_system=args.set_queuing_system,
            username=args.set_username,
            )
    script.show_config(
            build=args.show_build,
            builds=args.show_builds,
            config=args.show_config,
            db_path=args.show_db_path,
            default_project_account=args.show_default_project_account,
            default_pseudos_dir=args.show_default_pseudos_dir,
            max_jobs_in_queue=args.show_max_jobs_in_queue,
            package_dest_root=args.show_package_dest_root,
            package_src_root=args.show_package_src_root,
            queuing_system=args.show_queuing_system,
            username=args.show_username,
            show_qe=args.show_qe,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_db(args):
    """Executes the 'db' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.colors import Colors
    from abisuite.routines import full_abspath
    from abisuite.scripts.db import AbisuiteDBScript
    script = AbisuiteDBScript(loglevel=args.loglevel)
    if args.init_db is not None:
        script.init_database(args.init_db)
        return
    script.load_database(
            args.database, remote=args.remote, download=args.download,
            refresh=args.refresh)
    if args.show_db:
        script.show_database()
        return
    if args.clean:
        script.clean_database()
        return
    if args.check_integrity:
        script.check_integrity()
        return
    if args.calculations and not args.all and not args.all_monitored:
        await script.load_calculations(
                args.calculations, follow_symlinks=args.follow_symlinks)
        if args.add:
            script.add_calculations_to_database()
    if args.all or args.all_monitored:
        # get all calculations from database and discard the ones given as args
        script.load_calculations_from_database(
                all_calculations=args.all,
                all_monitored_calculations=args.all_monitored)
    if args.delete:
        if args.force:
            # set calculations list to the one explicitly
            # given in arg
            script.calculations = [
                    full_abspath(x) for x in args.calculations]
        script.delete_calculations_from_database()
        return
    if not script.calculations:
        print(Colors.bold_text("No calculations to handle."))
        return
    if args.reset_status:
        await script.reset_status()
    # deal with the 'sets' arguments at first
    script.set(
            set_monitored_true=args.set_monitored_true,
            set_monitored_false=args.set_monitored_false,
            set_project=args.set_project,
            set_packaged_true=args.set_packaged_true,
            set_packaged_false=args.set_packaged_false,
            update_calculation_status=args.update_calculation_status)
    # refresh calculations if requested (only for local dbs)
    if args.refresh and args.remote is None:
        script.refresh_calculations()
    # now deal with the 'gets'
    script.get(
            get_id=args.get_id, get_status=args.get_status,
            get_monitored=args.get_monitored, get_packaged=args.get_packaged,
            get_project=args.get_project,
            get_walltime=args.get_walltime)


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_debug(args: argparse.Namespace) -> None:
    """Executes the 'debug' part of the abisuite script.

    Parameters
    ----------
    args: Namespace instance
        The Namespace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.debug import AbisuiteDebugScript
    loglevel = args.loglevel
    script = AbisuiteDebugScript(loglevel=loglevel)
    await script.load_calculations(
            args.calculations, all_calculations=args.all)
    await script.debug(
            show_input_file=args.show_input_file,
            show_log_file=args.show_log_file,
            stderr_line_limit=args.stderr_line_limit,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_grep(args: argparse.Namespace) -> None:
    """Executes the 'grep' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.grep import AbisuiteGrepScript
    loglevel = args.loglevel
    script = AbisuiteGrepScript(loglevel=loglevel)
    await script.load_calculations(args.calculations)
    await script.grep(
            input_variable=args.input_variable,
            log_file_attribute=args.log_file_attribute,
            output_file_attribute=args.output_file_attribute,
            key=args.key, item=args.item,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_inputrepair(args):
    """Executes the 'inputrepair' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.input_file_repair import (
            AbisuiteInputFileRepairScript)
    loglevel = args.loglevel
    script = AbisuiteInputFileRepairScript(loglevel=loglevel)
    await script.load_calculations(args.calculations)
    await script.repair(
            dry_run=args.dry_run,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
def abisuite_math(args):
    """Executes the 'math' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.maths import (
            AbisuiteMathScript)
    loglevel = args.loglevel
    script = AbisuiteMathScript(loglevel=loglevel)
    script.find_divisors(args.find_divisors)


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_metadatarepair(args):
    """Executes the 'metadatarepair' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.meta_data_file_repair import (
            AbisuiteMetaDataFileRepairScript)
    loglevel = args.loglevel
    script = AbisuiteMetaDataFileRepairScript(loglevel=loglevel)
    await script.load_calculations(args.calculations)
    await script.repair(
            dry_run=args.dry_run,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_mv(args):
    """Executes the 'mv' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.linux_tools import mkdir
    from abisuite.routines import full_abspath

    calculations = args.calculations
    output = full_abspath(args.output)
    dry_run = args.dry_run
    recursive = args.recursive
    completed_only = args.completed_only
    if len(calculations) > 1:
        # if more than one calc dir to move, move them all in output with same
        # basename
        if not await aiofiles.os.path.exists(output):
            await mkdir(output)
        if not await aiofiles.os.path.isdir(output):
            raise NotADirectoryError(output)

    await asyncio.gather(
        *(_mv_calculation(
            full_abspath(path), output, completed_only, recursive, dry_run)
          for path in calculations)
        )


# @keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_package(args: argparse.Namespace) -> None:
    """Executes the 'package' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.package import AbisuitePackageScript
    loglevel = args.loglevel
    script = AbisuitePackageScript(loglevel=loglevel)

    use_slurm_files_to_delete = args.use_slurm_files_to_delete
    from_file = args.from_file
    dry = args.dry
    overwrite = args.overwrite
    force = args.force
    if use_slurm_files_to_delete is not None:
        await script.use_slurm_files_to_delete(use_slurm_files_to_delete)
        if not dry:
            await script.create_backup_file()
    elif from_file is not None:
        await script.from_file(from_file)
    else:
        script.calculations_src = args.calculations
    await script.load_calculations(
            force=args.force,
            only_successfull=not args.all,
            )
    # add try except in order to print out summary in case we kill
    try:
        await script.package_calculations(
                dry=dry, force=force, overwrite=overwrite,
                replace_with_symlink=args.replace_with_symlink,
                keep_density=args.keep_density,
                keep_wavefunctions=args.keep_wavefunctions,
                )
        if not args.skip_packaging_other_files:
            await script.package_other_files(
                    overwrite=overwrite, dry=dry,
                    )
        if not dry:
            script.print_summary(
                    final=True,
                    replace_with_symlink=args.replace_with_symlink)
            if args.replace_with_symlink:
                script.print_summary(
                    final=True, final_cleared=True,
                    replace_with_symlink=args.replace_with_symlink)
            if use_slurm_files_to_delete is not None:
                await script.delete_backup_file()
        else:
            await script.write_package_sources(
                    store_package_sources=args.store_package_sources)
    except KeyboardInterrupt:
        # print out summary so far
        if not dry:
            script._logger.info(
                    "KeyboardInterrupt: print summary up to death.")
            script.print_summary(
                    final=True,
                    replace_with_symlink=args.replace_with_symlink)
            if args.replace_with_symlink:
                script.print_summary(
                    final=True, final_cleared=True,
                    replace_with_symlink=args.replace_with_symlink)
        raise KeyboardInterrupt("\nYou killed me using your keyboard :(")


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_plot(args):
    """Executes the 'plot' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.plot import AbisuitePlotScript
    script = AbisuitePlotScript(loglevel=args.loglevel)
    if args.plot_pickle is None:
        raise ValueError("Need to give paths to plot pickles.")
    for path in args.plot_pickle:
        script.load_plot(path)
        if any([args.ylims, args.xlims]):
            script.modify_plot(
                ylims=args.ylims, xlims=args.xlims)
        if args.show:
            script.show_plot()
        if args.save:
            await script.save_plot(path.replace(".pickle", ".pdf"))
        if args.save_pickle:
            await script.save_pickle(path)


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_queue(args):
    """Executes the 'queue' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.queue import AbisuiteQueueScript
    script = AbisuiteQueueScript(loglevel=args.loglevel)
    if args.cancel:
        await script.load_calculations(
                args.calculations,
                load_running=True,
                load_unstarted=True,
                load_errored=True,
                )
        await script.cancel_calculations(dry=args.dry)
    if args.cancel_all:
        await script.cancel_all_calculations(dry=args.dry)
    if args.show_queue:
        await script.print_queue(
                tablefmt=args.tablefmt,
                workdir_shortening=args.no_shortening,
                )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_relaunch(args):
    """Executes the 'relaunch' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.colors import Colors
    from abisuite.scripts.relaunch import AbisuiteRelaunchScript
    script = AbisuiteRelaunchScript(loglevel=args.loglevel)
    if args.only_unstarted:
        script._logger.info(
                "Loading " + Colors.color_text("UNSTARTED", "cyan", "bold") +
                " calculations only.")
        await script.load_calculations(
                args.calculations,
                follow_symlinks=args.follow_symlinks,
                load_converged=False, load_errored=False,
                load_finished=False, load_running=False,
                load_unstarted=True,
                print_info=False,
                )
    else:
        await script.load_calculations(
                args.calculations,
                follow_symlinks=args.follow_symlinks,
                load_converged=args.force or args.relaunch_finished,
                load_errored=True,
                load_finished=args.force or args.relaunch_finished,
                load_running=args.force,
                load_unstarted=args.force or args.relaunch_unstarted,
                )
    await script.relaunch(
            build=args.build,
            command_arguments=args.command_arguments,
            dry=args.dry,
            input_variables_pop=args.input_variables_pop,
            input_variables_update=args.input_variables_update,
            jobname=args.jobname,
            memory_per_cpu=args.memory_per_cpu,
            mpi_command=args.mpi_command,
            nodes=args.nodes,
            ntasks=args.ntasks,
            ppn=args.ppn,
            project_account=args.project_account,
            queuing_system=args.queuing_system,
            walltime=args.walltime,
            ignore_calculation_limit=args.ignore_calculation_limit,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_rm(args: argparse.Namespace) -> None:
    """Executes the 'rm' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.rm import AbisuiteRmScript
    script = AbisuiteRmScript(loglevel=args.loglevel)
    try:
        await script.load_calculations(
                args.calculations, follow_symlinks=args.follow_symlinks,
                force=args.force,
                load_converged=not args.only_errored,
                load_errored=True,
                load_finished=not args.only_errored,
                load_running=not args.only_errored,
                load_unstarted=not args.only_errored,
                filter_calculations=True,
                )
    except Exception:
        if args.force:
            # there is an error while loading calculations but we want to
            # force deletion.
            from abisuite.routines import full_abspath
            script.calculations = [
                    full_abspath(calc) for calc in args.calculations]
        else:
            raise
    await script.rm(
            force=args.force,
            dry=args.dry,
            only_errored=args.only_errored,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_status(
        args: argparse.Namespace
        ) -> None:
    """Executes the 'status' part of the abisuite script.

    Parameters
    ----------
    args: Namespace instance
        The Namespace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.status import AbisuiteStatusScript

    script = AbisuiteStatusScript(loglevel=args.loglevel)
    await script.load_calculations(
            args.calculations, follow_symlinks=True,
            filter_calculations=False)
    await script.status(
            force=args.force, force_errored=args.force_errored,
            sort_by="walltime" if args.sort_by_walltime else "path",
            no_shortening=args.no_shortening,
            )


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_touch(args):
    """Executes the 'touch' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.touch import (
            AbisuiteTouchScript)

    script = AbisuiteTouchScript(loglevel=args.loglevel)
    await script.load_files_to_touch(args.slurm_files_to_delete)
    await script.touch_files()


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_visualize_charge(args):
    """Executes the 'visualize_charge' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.visualize_charge import (
            AbisuiteVisualizeChargeScript)

    script = AbisuiteVisualizeChargeScript(loglevel=args.loglevel)
    await script.load_data(args.calculation_or_den_file[0], force=args.force)
    await script.visualize_charge()


@keyboard_interrupt_wrap
@time_function_execution(print_arguments=False)
async def abisuite_visualize_structure(args):
    """Executes the 'visualize_structure' part of the abisuite script.

    Parameters
    ----------
    args: NameSpace instance
        The NameSpace object containing all command line arguments passed
        to the script.
    """
    from abisuite.scripts.visualize_structure import (
            AbisuiteVisualizeStructureScript)

    script = AbisuiteVisualizeStructureScript(loglevel=args.loglevel)
    await script.load_data(args.calculation_or_den_file[0], force=args.force)
    await script.visualize_structure(vesta=args.vesta)


async def _mv_calculation(to_move, target, completed_only, recursive, dry_run):
    """Actually moves one directory to another place."""
    from abisuite.colors import Colors
    from abisuite.handlers import CalculationDirectory, GenericFile

    if os.path.basename(target) != os.path.basename(to_move):
        # add basename to target
        target = os.path.join(target, os.path.basename(to_move))
    if await CalculationDirectory.is_calculation_directory(to_move):
        async with await CalculationDirectory.from_calculation(
                to_move) as calc:
            try:
                calc.connect_to_database()
            except Exception:
                pass
            if not (await calc.status)["calculation_finished"] and (
                    completed_only):
                # don't move since calculation is not completed
                return
            print(Colors.bold_text("Moving:") + f" '{to_move}' -> '{target}'.")
            if dry_run:
                return
            await calc.move(target, _display_progress_bar=True)
            print(Colors.color_text("Moving Done", "green", "bold"))
        return
    if await aiofiles.os.path.isfile(to_move):
        print(Colors.bold_text("Moving:") + f" '{to_move}' -> '{target}'.")
        if dry_run:
            return
        # move the file
        async with await GenericFile.from_file(to_move) as file_:
            await file_.move(target)
        print(Colors.color_text("Moving Done", "green", "bold"))
        return
    if await aiofiles.os.path.isdir(to_move) and recursive:
        # move content of directory by keeping same basename
        basename = os.path.basename(to_move)
        await asyncio.gather(
            *(_mv_calculation(
                    os.path.join(to_move, name),
                    os.path.join(target, basename),
                    completed_only,
                    recursive)
                for name in await aiofiles.os.listdir(to_move))
            )


async def parse_command_line_and_call_abisuite_script():
    """Main script function."""
    print(logo)
    parser = get_abisuite_argparser()
    args = parser.parse_args()
    if args.version:
        # print version then exit
        from abisuite import __version__
        print(f"abisuite version: {__version__}")
        return
    # handle remote first
    # TODO: test this part!!!!
    if hasattr(args, "remote"):
        if args.remote is not None:
            from abisuite.scripts.routines import AbisuiteScriptRemoteClient
            client = AbisuiteScriptRemoteClient.from_hostname(
                    args.remote, loglevel=args.loglevel)
            # remove the 'remote' part of the command line
            command_line = []
            iremote = None
            for iarg, arg in enumerate(sys.argv):
                if "--remote" in arg:
                    iremote = iarg
                    break
            else:
                raise LookupError(sys.argv)
            command_line = sys.argv[:iremote] + sys.argv[iremote + 1:]
            client.remote_script_calculation(" ".join(command_line))
            parser.exit()
    if args.profile:
        print("Profiling mode enabled. A 'profile.prof' file will be created "
              "at the end.")
        import cProfile
        import pstats
        with cProfile.Profile() as pr:
            args.func(args)
        stats = pstats.Stats(pr)
        stats.sort_stats(pstats.SortKey.TIME)
        stats.print_stats()
        stats.dump_stats(filename="profile.prof")
    else:
        if asyncio.iscoroutinefunction(args.func):
            await args.func(args)
        else:
            args.func(args)


def main() -> None:
    """Main abisuite script function."""
    asyncio.run(parse_command_line_and_call_abisuite_script())


if __name__ == "__main__":
    main()
