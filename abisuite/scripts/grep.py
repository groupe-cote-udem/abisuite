import logging
import os
from typing import Any

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..handlers import AbinitOutputFile
from ..routines import (
        is_2d_arr,
        is_dict, is_dict_like,
        is_list_like,
        is_scalar_or_str,
        )
from ..utils import TerminalTable


class AbisuiteGrepScript(BaseAbisuiteSubScript):
    """Executes the 'grep' subcommand for the abisuite script."""

    _loggername = "AbisuiteGrepScript"
    _subscript_color = "Blue"
    _subscript_name = "grep"

    async def load_calculations(self, *args, **kwargs):
        """Load the calculations to grep."""
        await super().load_calculations(*args, follow_symlinks=True, **kwargs)

    async def grep(
            self, input_variable: str = None, log_file_attribute: str = None,
            output_file_attribute: str = None, key: str = None,
            item: int = None,
            ) -> None:
        """Greps data from calculations and prints them.

        Parameters
        ----------
        input_variable: list-like, optional
            If not None, gets the given list of input variable in the input
            file to grep.
        log_file_attribute: str, optional
            If not None, gets the given 'log file attribute'. Can be anything
            defined in the corresponding structure file.
        output_file_attribute: str, optional
            If not None, gets the given 'output file attribute'.
            Can be anything defined in the corresponding structure file.
            (Abinit only)
        key: str, optional
            If requested attribute is a dict, will return only this
            key's value. Otherwise prints out whole dict.
        item: int, optional
            If requested attribute is a list, will return only this
            index.
        """
        if item is not None:
            self._logger.info(f"Grepping item # {item}")
        if key is not None:
            self._logger.info(f"Grepping key '{key}'.")
        column_names = ["Calculation(s)"]
        column_alignments = ["left"]
        if input_variable is not None:
            for input_var in input_variable:
                header = input_var
                if key is not None:
                    header += f"['{key}']"
                column_names.append(header + " (input file)")
                column_alignments.append("right")
        if log_file_attribute is not None:
            column_names.append(log_file_attribute + " (log)")
            column_alignments.append("right")
        if output_file_attribute is not None:
            column_names.append(output_file_attribute + " (output)")
            column_alignments.append("right")
        table = TerminalTable(
                column_names, column_alignments=column_alignments,
                bold_column_headers=True,
                loglevel=self._loglevel)
        self.sort_calculations()
        for calc_info in self.calculations:
            calculation = calc_info["calculation_directory"]
            async with calculation as calc:
                # use calc info[path] here in case we have a symlink
                # we don't care where it leads
                relpath = os.path.relpath(calc_info["path"], self.pwd)
                if len(relpath) > 50:
                    relpath = relpath[:25] + "[...]" + relpath[-25:]
                row = [relpath]
                if input_variable is not None:
                    # print the input variable
                    async with calc.input_file as inp:
                        for input_var in input_variable:
                            if input_var not in inp.input_variables:
                                # print that it is not defined
                                row.append("NOT DEFINED")
                            else:
                                var = inp.input_variables[input_var].value
                                row.append(self._format_input_var(
                                    var, key=key))
                if log_file_attribute is not None:
                    await self._get_data_insert_row(
                            await calc.log_file, log_file_attribute, row,
                            item=item, key=key,
                            )
                if output_file_attribute is not None:
                    if not calc.calctype.startswith("abinit"):
                        row.append("NOT ABINIT")
                    else:
                        await self._get_data_insert_row(
                                await AbinitOutputFile.from_calculation(
                                    calc, loglevel=self._loglevel),
                                output_file_attribute, row)
            table.add_row(row)
        table.print()

    def sort_calculations(self):
        """Sort the calculations by path."""
        paths = [calc_info["path"] for calc_info in self.calculations]
        order = [paths.index(path) for path in sorted(paths)]
        self.calculations = [self.calculations[i] for i in order]

    async def _get_data_insert_row(self, handler, attr, row, item, key):
        # get an attr for handler and append to row.
        # also handle exceptions.
        try:
            async with handler:
                attribute = getattr(handler, attr)
                if is_list_like(attribute) and item is not None:
                    attribute = attribute[item]
                if is_dict_like(attribute) and key is not None:
                    attribute = attribute[key]
                row.append(attribute)
        except Exception as err:
            if attr not in handler._structure_class.all_attributes:
                self._logger.error(
                        f"No attribute named '{attr}' in structure class of "
                        f"handler: '{handler}'.")
            row.append(Colors.bold_text("UNAVAILABLE"))
            if self._loglevel <= logging.DEBUG:
                self._logger.exception(err)

    def _format_input_var(self, value: Any, key: str = None) -> str:
        # formats the input variable value such that it fits nicely into the
        # table
        if is_list_like(value):
            if is_2d_arr(value):
                return "\n".join([str(x) for x in value])
            else:
                return str(value)
        elif is_scalar_or_str(value):
            return str(value)
        elif is_dict(value):
            if key is None or key not in value:
                return str(value)
            return value[key]
        else:
            raise NotImplementedError(value)
