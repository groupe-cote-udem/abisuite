import asyncio
import datetime
import json
import os

import aiofiles.os

from .bases import _BaseAbisuiteSubScriptMinimalLoadingReqs
from ..colors import Colors
from ..handlers import CalculationDirectory, MetaDataFile, PBSFile
from ..routines import command_to_calctype


class AbisuiteMetaDataFileRepairScript(
        _BaseAbisuiteSubScriptMinimalLoadingReqs):
    """Executes the 'metadatarepair' part of the abisuite script.

    Attempts at repairing a possibly broken meta data file in a calculation
    directory.
    """

    _loggername = "AbisuiteMetaDataFileRepairScript"
    _subscript_color = "yellow"
    _subscript_name = "meta data repair"

    async def repair(self, dry_run: bool = False) -> None:
        """Actually does the repair.

        Parameters
        ----------
        dry_run: bool, optional
            If True, a dry run is done: only what calculations that would
            be affected are printed out.
        """
        if dry_run:
            print("Meta data file repair: dry run only.")
        if not self.calculations:
            print(Colors.bold_text("No calculations to attempt to repair."))
        coros = []
        for calc_info in self.calculations:
            calculation = calc_info["path"]
            coros.append(
                    self._repair_single_calculation(
                        calculation, dry_run=dry_run))
        await asyncio.gather(*coros)

    async def _repair_single_calculation(
            self, calculation: CalculationDirectory,
            dry_run: bool = False) -> None:
        print(Colors.bold_text("Attempt repairing:") +
              f" '{calculation}'")
        if dry_run:
            return
        try:
            async with await MetaDataFile.from_calculation(
                            calculation, loglevel=self._loglevel) as meta:
                await meta.delete()
                meta.calc_workdir = calculation
            self.success(f"Repairing attempt done for {calculation}.")
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            # meta data file needs to be generated again
            await self.regenerate_meta_data_file(calculation)
        except Exception as err:
            self.error(err)
            self._logger.exception(err)

    async def regenerate_meta_data_file(
            self, calculation: CalculationDirectory | str,
            ) -> None:
        """Regenerated completely the meta data file for this calculation."""
        print(Colors.bold_text("REGENERATING") + " meta data file.")
        file_exists = False
        calc_path = calculation
        if isinstance(calculation, CalculationDirectory):
            calc_path = calculation.path
        try:
            meta = await MetaDataFile.from_calculation(
                calculation, loglevel=self._loglevel)
            await meta.delete()
            file_exists = True
        except FileNotFoundError:
            meta = MetaDataFile(loglevel=self._loglevel)
        meta.calc_workdir = calc_path
        # need to get the calctype
        # do this by reading the pbs file and getting the command
        for filename in await aiofiles.os.listdir(
                os.path.join(calc_path, "run")):
            if filename.endswith(".sh"):
                break
        else:
            self.error("While looking for pbs file: could not find it.")
            return
        async with await PBSFile.from_file(
                os.path.join(calc_path, "run", filename),
                loglevel=self._loglevel) as pbs:
            jobname = pbs.basename.replace(".sh", "")
            if not file_exists:
                await meta.set_path(
                        os.path.join(calc_path, f".{jobname}.meta"))
            meta.jobname = jobname
            meta.calctype = command_to_calctype(pbs.command)
            meta.pbs_file_path = pbs.path
        # put all the following to empty lists as we can't know for sure
        # which files were linked to this calculation or are linked from
        # this calculation
        meta.children = []
        meta.copied_files = []
        meta.parents = []
        meta.linked_files = []
        meta.input_data_dir = os.path.join(calculation, "input_data")
        meta.rundir = os.path.join(calculation, "run")
        meta.output_data_dir = os.path.join(calculation, "run", "output_data")
        meta.submit_time = str(datetime.datetime.now())
        meta.last_modified = str(datetime.datetime.now())
        # determine input file name
        for filename in await aiofiles.os.listdir(calculation):
            if filename.endswith(".in") or (filename.endswith(".abi") or
                                            filename.endswith(".win")):
                meta.input_file_path = os.path.join(calculation, filename)
                break
        else:
            self.error("Could not find input file.")
            return
        meta.script_input_file_path = meta.input_file_path
        await meta.write()
        self.success("Regenerated meta data file.")
