import asyncio
from typing import Any

import aiofiles.os

from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..databases import DBCalculation
from ..handlers import CalculationDirectory
from ..routines import connect_to_database


class AbisuiteRmScript(BaseAbisuiteSubScript):
    """Executes the 'rm' part of the abisuite script."""

    _loggername = "AbisuiteRmScript"
    _subscript_color = "orange"
    _subscript_name = "rm"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        connect_to_database()
        self.tot_nfiles = 0
        self.tot_ngbs = 0

    async def rm(
            self,
            only_errored: bool = False,
            dry: bool = False,
            force: bool = False,
            ) -> None:
        """Deletes the calculations stored in the 'calculations' attribute.

        If calculation is running, it is deleted from the queue also.

        Parameters
        ----------
        dry: bool, optional
            If True, only what will be deleted will be printed on screen
            but nothing is deleted.
        force: bool, optional
            If True, whatever happens, the script will try to delete the
            calculation"
        only_errored: bool, optional
            If True, only calculations that errored are removed.
        """
        if dry:
            print(Colors.color_text(
                "Dry run! This is what will happen:", "yellow", "bold"))
        if only_errored:
            print("Only deleting " +
                  Colors.color_text("errored", "bold", "red") +
                  " calculations.")
        cancel_coros = []
        delete_coros = []
        for calculation_info in self.calculations:
            path = calculation_info["path"]
            is_symlink = calculation_info["is_symlink"]
            calculation = calculation_info["calculation_directory"]
            try:
                path = calculation.path
                if dry:
                    print(Colors.bold_text("Will delete:") + path)
                model = DBCalculation.get_or_none(
                            DBCalculation.calculation == path)
                if model is not None and dry:
                    print(Colors.bold_text("Will be removed from DB"))
                if dry:
                    continue
                print(Colors.bold_text("DELETING:") + path)
                if model is not None:
                    model.delete_instance()
                    print(Colors.color_text(
                        "SUCCESSFULLY Removed from db ", "green", "bold"))
                # if calc is running, stop it!
                cancel_coros.append(self.cancel_job(calculation, dry=dry))
            except Exception as err:
                if not force:
                    self._logger.exception(err)
                    raise err
                if dry:
                    self._logger.error(
                            "An error occured while treating "
                            f"'{path}' but dry run.")
                    continue
                if force:
                    self._logger.error(
                            "An error occured while handling: "
                            f"'{path}' but force.")
            delete_coros.append(
                    self.delete_calculation(calculation))
            if is_symlink:
                delete_coros.append(self.delete_symlink(path))
        await asyncio.gather(*cancel_coros)
        await asyncio.gather(*delete_coros)
        if not dry:
            print(Colors.bold_text("\n=== FINAL RM SUMMARY ==="))
            self.print_summary(self.tot_ngbs, self.tot_nfiles)

    async def delete_symlink(self, path):
        """Deletes a symlink."""
        await aiofiles.os.remove(path)

    def print_summary(self, *args) -> None:
        """Print removal summary on screen."""
        # print total summary
        nfiles, funits, ngbs, munits = self._get_data_to_print(*args)
        print(Colors.bold_text(
            f"{nfiles}{funits} for {ngbs:.1f} {munits} cleared."))

    def _get_data_to_print(self, ngbs: float, nfiles: int) -> tuple:
        funits = " files"
        munits = "Gb"
        if ngbs >= 1024:
            ngbs /= 1024
            munits = "Tb"
        elif ngbs <= 0.1:
            ngbs *= 1024
            munits = "Mb"
            # downsize again if needed
            if ngbs <= 0.1:
                ngbs *= 1024
                munits = "kb"
        if nfiles > 1000:
            nfiles /= 1000
            funits = "k files"
        return nfiles, funits, ngbs, munits

    async def delete_calculation(
            self, calculation: CalculationDirectory) -> None:
        """Delete a single calculation."""
        # delete directory
        async with calculation:
            ngbs = await calculation.directory_size
            nfiles = len(await calculation.walk(paths_only=False))
        self.tot_ngbs += ngbs
        self.tot_nfiles += nfiles
        await calculation.delete()
        print(Colors.color_text(
            "SUCCESSFULLY deleted", "green", "bold"))
        self.print_summary(ngbs, nfiles)
        calculation.clear_content()

    async def skip_this_calculation(
            self, calc_info: dict[str, Any],
            force: bool = False,
            **kwargs) -> bool:
        """Override the mother's method in case there is a problem."""
        try:
            return await super().skip_this_calculation(
                    calc_info, **kwargs)
        except Exception as err:
            if force:
                # delete calc whatever happens therefore do not skip this one
                return (calc_info, False)
            raise err
