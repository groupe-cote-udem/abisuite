from .bases import BaseAbisuiteSubScript
from ..colors import Colors
from ..handlers import CalculationDirectory
from ..linux_tools import which
from ..routines import check_output_with_retries


class AbisuiteDebugScript(BaseAbisuiteSubScript):
    """Executes the 'debug' subcommand of the abisuite script."""

    _loggername = "AbisuiteDebugScript"
    _subscript_color = "red"
    _subscript_name = "debug"

    async def load_calculations(
            self, *args, all_calculations: bool = False) -> None:
        """Loads calculations to debug.

        Parameters
        ----------
        all_calculations: bool, optional
            If True, all calculations go through the script. Otherwise,
            only 'errored' calculation are debugged.
        """
        await super().load_calculations(
                *args,
                load_converged=all_calculations,
                load_errored=True,
                load_finished=all_calculations,
                load_running=all_calculations,
                load_unstarted=all_calculations,
                )

    async def debug(
            self,
            show_input_file: bool = False,
            force: bool = False,
            show_log_file: bool = False,
            stderr_line_limit: int = 30,
            ) -> None:
        """Prints debugging information for all desired calculations.

        Parameters
        ----------
        force: bool, optional
            If True, all calculations go through the script. Otherwise,
            only 'errored' calculation are debugged.
        show_input_file: bool, optional
            If True, the input file's content is printed on screen.
        show_log_file: bool, optional
            If True, the content of the log file is printed on screen.
        """
        for calc_info in self.calculations:
            calculation = calc_info["calculation_directory"]
            self._logger.info(
                    Colors.bold_text("DEBUGGING:") + f" '{calculation.path}'.")
            await self._print_stderr_content(
                    calculation, line_limit=stderr_line_limit)
            await self._print_crash_file(calculation)
            await self._print_pbs_file(calculation)
            if show_input_file:
                await self._print_input_file(calculation)
            if show_log_file:
                await self._print_log_file(calculation)
            if calculation.pbs_file.queuing_system == "slurm":
                # print out slurm data
                await self._print_slurm_job_report(calculation)
            calculation.clear_content()

    async def _print_crash_file(self, calculation):
        # if abinit calc and an __ABI_ABORTFILE__ is present: cat it
        calctype = await calculation.calctype
        if calctype.startswith("abinit"):
            async with calculation.run_directory as run:
                async for handler in run:
                    basename = handler.basename
                    if "ABORTFILE" in basename and not (
                            basename.endswith(".lock")):
                        # self._logger.info(
                        #     "This is an abinit run and a ABORTFILE"
                        #     " is present.")
                        print()
                        print(Colors.bold_text(
                            "Printing ABORTFILE content:"))
                        await handler.cat(line_limit=30)
        elif calctype.startswith("qe_"):
            # check for a CRASH file and expose it
            # it might be in the calcdir or run dir
            async with calculation:
                async for handler in calculation:
                    if handler.basename == "CRASH":
                        print()
                        print(Colors.bold_text(
                            "Printing CRASH file content:"))
                        await handler.cat()
            async with calculation.run_directory as run:
                async for handler in run:
                    if handler.basename == "CRASH":
                        print()
                        print(Colors.bold_text(
                            "Printing CRASH file content:"))
                        await handler.cat()

    async def _print_pbs_file(self, calculation):
        async with calculation.pbs_file as pbs:
            # print calculation parametersx
            print()
            print(Colors.bold_text("Calculation parameters:"))
            print(pbs)
            print()

    async def _print_log_file(self, calculation):
        print(Colors.bold_text("Log file content:"))
        try:
            await (await calculation.log_file).cat()
        except FileNotFoundError:
            print("Log file does not exists.")

    async def _print_stderr_content(
            self, calculation: CalculationDirectory,
            line_limit: int = 30) -> None:
        stderr = await calculation.stderr_file
        if not await stderr.exists:
            return
        self._logger.info(
                Colors.bold_text("Printing stderr file content:") +
                f" '{stderr.path}'.")
        await stderr.cat(line_limit=line_limit)

    async def _print_input_file(self, calc):
        print(Colors.bold_text("Input file content:"))
        await calc.input_file.cat()

    async def _print_slurm_job_report(self, calculation):
        # get the jobid. get all the ids that was used for this calc and
        # call seff on the highest one
        ids = []
        async for handler in calculation.run_directory:
            if handler.basename.startswith("slurm"):
                ids.append(int(handler.basename.split("-")[-1].split(".")[0]))
        if not ids:
            self._logger.error("No slurm ID found -> cannot show seff")
            return
        if await which("seff") is None:
            print(Colors.color_text("ERROR:", "red", "bold") +
                  " we're on a slurm system but seff is not found.")
            return
        print("\n" + Colors.bold_text(
            "Calling 'seff' for this calculation:"))
        print(check_output_with_retries(
            ["seff", str(max(ids))]).decode("utf-8"))
