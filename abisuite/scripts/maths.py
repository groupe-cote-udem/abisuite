from .bases import BaseAbisuiteSubScript
from ..bases import DevError
from ..colors import Colors
from ..maths import find_divisors


class AbisuiteMathScript(BaseAbisuiteSubScript):
    """Handles the 'math' sub script of abisuite."""

    _loggername = "AbisuiteMathScript"
    _subscript_color = "cyan"
    _subscript_name = "math"

    def load_calculations(self, *args, **kwargs):  # noqa: D102
        raise DevError("Not relevant for this script.")

    def find_divisors(self, n):
        """Prints out the divisors of n."""
        if n is None:
            return
        print(Colors.bold_text(f"Divisors of {n}:\n") +
              str(find_divisors(n)))
