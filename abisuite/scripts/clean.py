import asyncio

import tqdm

from .bases import BaseAbisuiteSubScript
from ..colors import Colors


class AbisuiteCleanScript(BaseAbisuiteSubScript):
    """Executes the 'clean' subcommand of the abisuite script."""

    _loggername = "AbisuiteCleanScript"
    _subscript_color = "orange"
    _subscript_name = "clean"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.summary = {}

    async def clean(self, dry=False, **kwargs):
        """Clean the calculations.

        Parameters
        ----------
        dry: bool, optional
            If True, a dry run is done and only what will be cleaned will
            be shown.
        """
        total_nfiles_cleaned = 0
        total_ngbs_cleaned = 0
        total_nlinks_cleaned = 0
        if dry:
            print(Colors.bold_text("DRY RUN ONLY:") +
                  " printing files to delete.")
        coros = [self._clean_single_calc(calc_info, dry=dry, **kwargs)
                 for calc_info in self.calculations]
        for coro in tqdm.tqdm(
                asyncio.as_completed(coros),
                desc="Cleaning calculations.",
                unit="calcs", total=len(coros),
                ):
            nfc, nlc, ngc = await coro
            total_nfiles_cleaned += nfc
            total_ngbs_cleaned += ngc
            total_nlinks_cleaned += nlc
        info = self.get_cleaning_info(
                total_nfiles_cleaned, total_nlinks_cleaned, total_ngbs_cleaned,
                dry=dry, total=True)
        # print all info
        for calc_info in self.calculations:
            print(Colors.bold_text(calc_info["path"] + " cleaned:"))
            if calc_info["path"] not in self.summary:
                continue
            print(self.summary[calc_info["path"]])
        print(info)

    async def _clean_single_calc(self, calc_info, dry=False, **kwargs):
        calculation = calc_info["calculation_directory"]
        self._logger.debug(
                Colors.bold_text("CLEANING") + f" '{calculation.path}'.")
        try:
            await calculation.clean(dry=dry, **kwargs)
            cleaner = await calculation.cleaner
            info = self.get_cleaning_info(
                    cleaner.nfiles_cleaned,
                    cleaner.nlinks_cleaned,
                    cleaner.ngb_cleaned,
                    total=False, **kwargs,
                    )
            self.summary[calc_info["path"]] = info
            return (
                    cleaner.nfiles_cleaned,
                    cleaner.nlinks_cleaned, cleaner.ngb_cleaned,
                    )
        except Exception:
            self._logger.exception(Colors.color_text("FAILED", "red"))
        return 0, 0, 0

    def get_cleaning_info(
            self, nfiles_cleaned, nlinks_cleaned, ngb_cleaned, dry=False,
            total=False):
        """Returns summary of cleaning info.

        Parameters
        ----------
        nfiles_cleaned: int
            Number of files cleaned.
        nlinks_cleaned: int
            Number of links pruned.
        ngb_cleaned: float
            Total file size cleaned (in Gb). This number, if more than a Tb,
            will be converted to Tb before printing.
        dry: bool, optional
            If True, a dry run was executed. Changes format of print message.
            If total is False and dry is True => nothing is printed.
        total: bool, optional
            If True, the total summary of the cleaning run is printed.
            This option just changes the printing format.
        """
        if not total and dry:
            return
        ngb_units = "Gb"
        if nfiles_cleaned >= 1000 and nfiles_cleaned:
            nfiles_cleaned = str(nfiles_cleaned // 1000) + "k"
        if nlinks_cleaned >= 1000:
            nlinks_cleaned = str(nlinks_cleaned // 1000) + "k"
        if ngb_cleaned >= 1024:
            ngb_cleaned /= 1024
            ngb_units = "Tb"
        string = ""
        if not total:
            string += (
                  Colors.color_text("SUCCESSFULLY CLEANED", "green") +
                  f" {nfiles_cleaned} files deleted, "
                  f" {nlinks_cleaned} links pruned and "
                  f"{ngb_cleaned:.2f} {ngb_units} cleaned.\n")
        else:
            string += ("---------------------------------------\n")
            if dry:
                first = "Will clean in total:"
                second = "Will free:"
            else:
                first = "Cleaned in total:"
                second = "Freed:"
            string += (
                  Colors.bold_text(first) +
                  f" {nfiles_cleaned} files and " +
                  f"{nlinks_cleaned} links.\n"
                  )
            string += (
                  Colors.bold_text(second) +
                  f" {ngb_cleaned:.2f} {ngb_units} of disk space.\n")
