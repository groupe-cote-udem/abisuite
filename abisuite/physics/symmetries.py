# These classes are inspired (read here mostly copy-pasted) from pymatgen
# I don't load pymatgen since it is a huge library and I only need small part
import logging
import re

import numpy as np

from ..bases import BaseUtility
from ..handlers import CalculationDirectory
from ..routines import is_2d_arr, is_int, is_vector


# dict of all implemented space groups
# keys are schonflies notation (C4, Oh, ...)
# see here to see examples:
# https://raw.githubusercontent.com/materialsproject/pymatgen/fcb84a1e129da3a3010b9f6b9cf18beddf39f245/pymatgen/symmetry/symm_ops.json
__ALL_POINT_GROUPS__ = {
        "D2h": [
            "x,y,z", "-x,-y,z", "x,-y,-z", "-x,y,-z", "-x,-y,-z", "x,y,-z",
            "-x,y,z", "x,-y,z",
            ],
        "D2h^18": [
            "x,y,z", "-x,-y,-z",
            ],
        "D4h": [
            "x,y,z", "-y,x,z", "-x,-y,z", "y,-x,z", "x,-y,-z", "y,x,-z",
            "-x,y,-z", "-y,-x,-z", "-x,-y,-z", "y,-x,-z", "x,y,-z",
            "-y,x,-z", "-x,y,z", "-y,-x,z", "x,-y,z", "y,x,z",
            ],
        "Oh": [
            "x,y,z", "-y,x,z", "-x,-y,z", "y,-x,z", "x,-y,-z",
            "y,x,-z", "-x,y,-z", "-y,-x,-z", "z,x,y", "-x,z,y", "-z,-x,y",
            "x,-z,y", "z,-x,-y", "x,z,-y", "-z,x,-y", "-x,-z,-y", "y,z,x",
            "y,-z,-x", "z,y,-x", "-y,z,-x", "-z,-y,-x", "-y,-z,x",
            "z,-y,x", "-z,y,x", "-x,-y,-z", "y,-x,-z", "x,y,-z", "-y,x,-z",
            "-x,y,z", "-y,-x,z", "x,-y,z", "y,x,z", "-z,-x,-y", "x,-z,-y",
            "z,x,-y", "-x,z,-y", "-z,x,y", "-x,-z,y", "z,-x,y", "x,z,y",
            "-y,-z,-x", "-y,z,x", "-z,-y,x", "y,-z,x", "z,y,x", "y,z,-x",
            "-z,y,-x", "z,-y,-x",
            ],
        "Oh^5": [
            "x,y,z", "-y,x,z", "-x,-y,z", "y,-x,z", "x,-y,-z", "y,x,-z",
            "-x,y,-z", "-y,-x,-z", "z,x,y", "-x,z,y", "-z,-x,y", "x,-z,y",
            "z,-x,-y", "x,z,-y", "-z,x,-y", "-x,-z,-y", "y,z,x", "y,-z,-x",
            "z,y,-x", "-y,z,-x", "-z,-y,-x", "-y,-z,x", "z,-y,x", "-z,y,x",
            "-x,-y,-z", "y,-x,-z", "x,y,-z", "-y,x,-z", "-x,y,z", "-y,-x,z",
            "x,-y,z", "y,x,z", "-z,-x,-y", "x,-z,-y", "z,x,-y", "-x,z,-y",
            "-z,x,y", "-x,-z,y", "z,-x,y", "x,z,y", "-y,-z,-x", "-y,z,x",
            "-z,-y,x", "y,-z,x", "z,y,x", "y,z,-x", "-z,y,-x", "z,-y,-x",
            ],
        }


def space_group_to_point_group(spgroup):
    """Return the point group name from a spgroup number."""
    # see here to convert
    # https://en.wikipedia.org/wiki/List_of_space_groups
    if not is_int(spgroup):
        raise TypeError("spgroup should be an int.")
    if spgroup == 64:
        return "D2h^18"
    if spgroup == 139:
        return "D4h^17"
    if spgroup == 225:
        return "Oh^5"
    raise NotImplementedError(spgroup)


# The list of all implemented space groups
# keys are the space group international number
# values are the list of symmetry operations
__ALL_SPACE_GROUPS__ = {
        64: {
            "name": "Cmce",
            "symmetries": [
                "x,y,z", "-x,-y+1/2,z", "x+1/2,-y+1/2,-z", "-x+1/2,y,-z",
                "-x,-y,-z", "x,y+1/2,-z", "-x+1/2,y+1/2,z", "x+1/2,-y,z",
                ],
            },
        139: {
            "name": "I4/mmm",
            "symmetries": [
                "x,y,z", "-y,x,z", "-x,-y,z", "y,-x,z", "x,-y,-z", "y,x,-z",
                "-x,y,-z", "-y,-x,-z", "-x,-y,-z", "y,-x,-z", "x,y,-z",
                "-y,x,-z", "-x,y,z", "-y,-x,z", "x,-y,z", "y,x,z",
                "x+1/2,y+1/2,z+1/2", "-y+1/2,x+1/2,z+1/2",
                "-x+1/2,-y+1/2,z+1/2", "y+1/2,-x+1/2,z+1/2",
                "x+1/2,-y+1/2,-z+1/2", "y+1/2,x+1/2,-z+1/2",
                "-x+1/2,y+1/2,-z+1/2", "-y+1/2,-x+1/2,-z+1/2",
                "-x+1/2,-y+1/2,-z+1/2", "y+1/2,-x+1/2,-z+1/2",
                "x+1/2,y+1/2,-z+1/2", "-y+1/2,x+1/2,-z+1/2",
                "-x+1/2,y+1/2,z+1/2", "-y+1/2,-x+1/2,z+1/2",
                "x+1/2,-y+1/2,z+1/2", "y+1/2,x+1/2,z+1/2",
                ],
            }
        }


class PointGroup(BaseUtility):
    """Class that defines a point group."""

    def __init__(self, point_group_name, *args, **kwargs):
        """Generate the point group object from a space group name.

        Parameters
        ----------
        point_group_number: int
            The space group number.
        """
        if point_group_name not in __ALL_POINT_GROUPS__:
            raise NotImplementedError(point_group_name)
        self.symmetries = [
                SymmetryOperation.from_xyz_string(string)
                for string in (
                    __ALL_POINT_GROUPS__[point_group_name])
                ]

    @property
    def nsymmetries(self):
        """Return the number of symmetries."""
        return len(self.symmetries)

    @classmethod
    async def from_calculation(cls, calculation, *args, **kwargs):
        """Create a PointGroup object from a calculation."""
        if not isinstance(calculation, CalculationDirectory):
            calculation = await CalculationDirectory.from_calculation(
                    calculation, loglevel=kwargs.get("loglevel", logging.INFO))
        async with calculation as calc:
            if await calc.calctype == "qe_pw":
                return await cls.from_qe_pw_calculation(calc, *args, **kwargs)
            elif await calc.calctype == "abinit":
                async with await calc.log_file as log:
                    point_group = space_group_to_point_group(
                            log.input_variables["spgroup"])
                return cls(point_group, *args, **kwargs)
            else:
                raise NotImplementedError(await calc.calctype)

    @classmethod
    async def from_qe_pw_calculation(cls, calculation, *args, **kwargs):
        """Return a PointGroup object from a qe_pw calculation."""
        if not isinstance(calculation, CalculationDirectory):
            calculation = CalculationDirectory(
                    calculation, loglevel=kwargs.get("loglevel", logging.INFO))
        async with calculation as calc:
            # check ibrav in input file
            async with calc.input_file as inf:
                ibrav = inf.input_variables["ibrav"]
            if ibrav == 2:
                point_group = "Oh"
            else:
                raise NotImplementedError(ibrav)
        return cls(point_group, *args, **kwargs)


class SpaceGroup(BaseUtility):
    """Class that defines space groups."""

    def __init__(self, space_group_number, *args, **kwargs):
        """Generate the space group object from a space group name.

        Parameters
        ----------
        space_group_number: int
            The space group number.
        """
        super().__init__(*args, **kwargs)
        if space_group_number not in __ALL_SPACE_GROUPS__:
            raise NotImplementedError(space_group_number)
        self.symmetries = [
                SymmetryOperation.from_xyz_string(string)
                for string in __ALL_SPACE_GROUPS__[space_group_number]
                ]


class SymmetryOperation(BaseUtility):
    """Class that defines a symmetry operation in real space.

    A symmetry operation is defined by a rotation/reflection operation
    with (possibly) a translation. A transformed point P is thus given by:

    P' = R.P + b where R is the rotation/reflection matrix and b the
    translation vector.

    See Also
    --------
    Super good book about symmetries (esp. Chapter 9)
    https://link.springer.com/content/pdf/10.1007%2F978-3-540-32899-5.pdf
    """

    def __init__(self, r_matrix, b_translation, *args, **kwargs):
        """Initialize the symmetry operation object.

        Parameters
        ----------
        r_matrix: 3x3 array
            The rotation or reflection matrix defining the symmetry operation.
        b_translation: length-3 vector
            The translation vector defining the symmetry operation.
        """
        if not is_2d_arr(r_matrix, size=(3, 3)):
            raise ValueError(
                    "'r_matrix' should be a 3x3 array but "
                    f"got '{r_matrix}'.")
        if not is_vector(b_translation, length=3):
            raise ValueError(
                    "'b_translation' should be a length-3 vector but got:"
                    f"'{b_translation}'.")
        self.transform = np.zeros((4, 4))
        self.transform[1:4][:, 1:4] = r_matrix
        self.transform[:, 0][1:4] = b_translation

    def __mul__(self, other):
        """Multiply two symmetry operation.

        Equivalent of applying the 'other' before the 'self'.
        """
        if not isinstance(other, SymmetryOperation):
            raise TypeError("Can only multyply SymOps together.")
        return SymmetryOperation(
                self.r_matrix.dot(other.r_matrix),
                (self.translation_vector +
                 self.r_matrix.dot(other.translation_vector)),
                loglevel=min((self._loglevel, other._loglevel)),
                )

    @property
    def r_matrix(self):
        """Return the rotation/inversion/reflection matrix."""
        return self.transform[1:4][:, 1:4]

    @property
    def translation_vector(self):
        """Return the translation vector."""
        return self.transform[:, 0][1:4]

    @property
    def inverse(self):
        """Return the inverse symmetry operation."""
        r_inv = np.linalg.inv(self.r_matrix)
        return SymmetryOperation(
                r_inv, -np.dot(r_inv, self.translation_vector),
                loglevel=self._loglevel,
                )

    def apply_symmetry(self, point):
        """Apply the symmetry operation on a point.

        Parameters
        ----------
        point: length-3 vector
            The cartesian point to operate on.

        Returns
        -------
        list: The coordinate of the final point.
        """
        if not is_vector(point, length=3):
            raise ValueError(
                    "'point' should be a length-3 vector but got: '{point}'.")
        point_4 = np.ones(4)
        point_4[1:4] = point
        return np.dot(self.transform, point_4)[1:4]

    @classmethod
    def from_xyz_string(cls, string, *args, **kwargs):
        """Create a symmetry operation object from a string defining it.

        Parameters
        ----------
        string: str
            The xyz string defining the operation. E.g.: 'x, y, z' or
            '-2y+1/2, 3x+1/2, z-y+1/2'.

        Other Parameters
        ----------------
        Are passed to the __init__ method.

        Returns
        -------
        SymmetryOperation: The symmetry operation object.
        """
        # Taken from:
        # https://github.com/materialsproject/pymatgen/blob/fcb84a1e129da3a3010b9f6b9cf18beddf39f245/pymatgen/core/operations.py#L416
        rot = np.zeros((3, 3))
        trans = np.zeros(3)
        # split the string into list of clean elements (no spaces, no comas)
        toks = string.strip().replace(" ", "").lower().split(",")
        if len(toks) != 3:
            raise ValueError(f"Invalid xyz symmetry string: '{string}'.")
        # use regexp because it is much easier
        # for rotations: group 1 = sign, group 2 / 3 = fraction value,
        # group 4 = direction of symmetry (x,y or z)
        re_rot = re.compile(r"([+-]?)([\d\.]*)/?([\d\.]*)([x-z])")
        # for translations:
        # group 1 = sign, group 2/3 = fraction, group 4 = discard xyz
        re_trans = re.compile(r"([+-]?)([\d\.]+)/?([\d\.]*)(?![x-z])")
        for i, tok in enumerate(toks):
            # build the rotation matrix
            for m in re_rot.finditer(tok):
                if m.group(1) == "":
                    sign = 1.0
                elif m.group(1) == "-":
                    sign = -1.0
                if m.group(2) != "":
                    factor = float(m.group(2))
                else:
                    factor = 1.0
                if m.group(3) != "":
                    factor /= float(m.group(3))
                xyz = m.group(4)
                if xyz == "x":
                    j = 0
                elif xyz == "y":
                    j = 1
                elif xyz == "z":
                    j = 2
                rot[i, j] = sign * factor
            # build the translation vector
            for m in re_trans.finditer(tok):
                if m.group(1) == "-":
                    sign = -1.0
                else:
                    sign = 1.0
                # there is necessarily a factor here otherwise we loop
                # over an empty list
                factor = float(m.group(2))
                if m.group(3) != "":
                    factor /= float(m.group(3))
                trans[i] = sign * factor
        return SymmetryOperation(rot, trans, *args, **kwargs)
