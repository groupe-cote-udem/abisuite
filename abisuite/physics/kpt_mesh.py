"""KptMesh utility to manipulate ensemble of kpts."""

import itertools
import math
from typing import Sequence

import numpy as np

# from scipy.spatial import Voronoi, Delaunay

import tqdm.autonotebook as tqdm

from .lattice import Lattice
from .symmetries import PointGroup, space_group_to_point_group
from ..bases import BaseUtility
from ..handlers import AbinitEIGFile, AbinitOutputFile
from ..plotters import Plot
from ..routines import is_2d_arr, is_int, is_str


class KptMesh(BaseUtility):
    """Define a kpt mesh on the full BZ from a list of kpts and a Pt Group.

    Kpts must be in cartesian coordinates.
    """

    _loggername = "KptMesh"

    def __init__(self, *args, **kwargs):
        """Kpoint Mesh init method."""
        super().__init__(*args, **kwargs)
        self.tolerance = 1e-10  # numerical tolerance
        self._coordinate_system = None
        self._extended_mapping = None
        self._kpts = None
        self._pt_group = None
        self._lattice = None
        self._nns = None
        self._nns_norms = None
        self._nns_norms2 = None
        self._second_nns = None
        # self._bz_vertices = None  # the list of the BZ vertices

    # @property
    # def bz_vertices(self):
    #     """The vertices coordinates of the first BZ boundary."""
    #     if self._bz_vertices is not None:
    #         return self._bz_vertices
    #     vor = Voronoi(self.nearest_reciprocal_lattice_points)
    #     faces = []
    #     for r in vor.ridge_dict:
    #         if r[0] == 13 or r[1] == 13:
    #             faces += [vor.vertices[i] for i in vor.ridge_dict[r]]
    #     self._bz_vertices = np.unique(faces, axis=0)
    #     return self._bz_vertices

    @property
    def coordinate_system(self):
        """Return the kpts coordinate system."""
        if self._coordinate_system is None:
            raise ValueError("Need to set 'coordinate_system'.")
        return self._coordinate_system

    @coordinate_system.setter
    def coordinate_system(self, system):
        if system not in ("reduced", "cartesian"):
            raise ValueError(f"Invalid system: '{system}'.")
        self._coordinate_system = system

    @property
    def decimal_precision(self):
        """Return the number of decimal precision.

        (How many decimals to round).
        """
        prec = -int(math.floor(math.log10(self.tolerance)))
        if prec < 0:
            raise ValueError("Decimal precision yields negative...")
        return prec

    @property
    def extended_mapping(self):
        """Return the extended array mapping when extending to full bz."""
        if self._extended_mapping is not None:
            return self._extended_mapping
        raise RuntimeError(
                "'extended_mappin' is not set yet. Set it by calling the "
                "'extend_full_bz()' method.")

    @property
    def kpts(self):
        """Return the kpt list in cartesian coordinates."""
        if self._kpts is None:
            raise ValueError("Need to set 'kpts'.")
        return self._kpts

    @property
    def lattice(self):
        """Return the Lattice object for this kmesh."""
        return self._lattice

    @lattice.setter
    def lattice(self, lat):
        if not isinstance(lat, Lattice):
            raise TypeError("'lattice' must be a 'Lattice' object.")
        self._lattice = lat

    @property
    def nearest_reciprocal_lattice_points(self):
        """Return the nearest reciprocal lattice points."""
        if self._nns is not None:
            return self._nns
        self._nns = []
        for i, j, k in itertools.product([-1, 0, 1], [-1, 0, 1], [-1, 0, 1]):
            # if i == 0 and j == 0 and k == 0:
            #     continue
            self._nns.append(
                    i * self.lattice.reciprocal_vectors[0] +
                    j * self.lattice.reciprocal_vectors[1] +
                    k * self.lattice.reciprocal_vectors[2])
        return self._nns

    @property
    def nearest_reciprocal_lattice_points_norms(self):
        """Return the distance to the nearest reciprocal lattice points."""
        if self._nns_norms is not None:
            return self._nns_norms
        self._nns_norms = [
                np.sqrt(v.dot(v))
                for v in self.nearest_reciprocal_lattice_points]
        return self._nns_norms

    @property
    def nearest_reciprocal_lattice_points_norms2(self):
        """Return the squared-distance to the nearest rec. lat. points."""
        if self._nns_norms2 is not None:
            return self._nns_norms2
        self._nns_norms2 = [
                norm ** 2
                for norm in self.nearest_reciprocal_lattice_points_norms]
        return self._nns_norms2

    @property
    def point_group(self):
        """Return the PointGroup object associated with this kmesh."""
        if self._pt_group is None:
            raise ValueError("Need to set 'point_group'.")
        return self._pt_group

    @point_group.setter
    def point_group(self, pt):
        if isinstance(pt, PointGroup):
            self._pt_group = pt
            return
        elif is_str(pt):
            self._pt_group = PointGroup(pt, loglevel=self._loglevel)
            return
        raise ValueError(f"Invalid point group: '{pt}'.")

    @property
    def second_nearest_reciprocal_lattice_points(self):
        """Return the second nearest reciprocal lattice points."""
        if self._second_nns is not None:
            return self._second_nns
        self._second_nns = []
        for i, j, k in itertools.product(
                [-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2], [-2, -1, 0, 1, 2]):
            if i in (-1, 0, 1) and j in (-1, 0, 1) and k in (-1, 0, 1):
                # not 2nd nearest
                continue
            self._second_nns.append(
                    i * self.lattice.reciprocal_vectors[0] +
                    j * self.lattice.reciprocal_vectors[1] +
                    k * self.lattice.reciprocal_vectors[2])
        return self._second_nns

    def get_kpts_cartesian_coordinates(self, rounded=None):
        """Return the list of kpts in cartesian coordinates.

        Parameters
        ----------
        rounded: int, optional
            If not None, round the kpts to this number of decimals.
        """
        if self.coordinate_system == "reduced":
            kpts = self.lattice.reduced_to_cartesian_coordinates(
                    self.kpts)
        else:
            kpts = self.kpts
        if rounded is not None:
            return np.round(kpts, rounded)
        return kpts

    def get_kpts_reduced_coordinates(self, rounded: int = None) -> np.ndarray:
        """Return the array of kpts in reduced coordinates.

        Parameters
        ----------
        rounded: int, optional
            If not None, round the kpts to this number of decimals.
        """
        if self.coordinate_system == "cartesian":
            kpts = self.lattice.cartesian_to_reduced_coordinates(
                    self.kpts)
        else:
            kpts = self.kpts
        if rounded is not None:
            return np.round(kpts, rounded)
        return kpts

    def init_lattice_from_abinit_output_file(
            self, filepath, return_handler=False):
        """Initialize the lattice object from an abinit output file.

        Parameters
        ----------
        filepath: str
            The path to the abinit output file.
        return_handler: bool, optional
            If True, the AbinitOutputFile handler is returned.
            Use only for conveniance.

        Returns
        -------
        AbinitOutputFile handler:
            Only if 'return_handler' is set to True. Otherwise
            nothing is returned.
        """
        with AbinitOutputFile.from_file(
                filepath, loglevel=self._loglevel) as out:
            self.init_lattice_from_abinit_input_variables(
                    rprim=out.input_variables["rprim"],
                    acell=out.input_variables["acell"],
                    )
            self.point_group = space_group_to_point_group(
                    out.input_variables["spgroup"]
                    )
        if return_handler:
            return out

    def init_kpts_from_abinit_eig_file(self, filepath):
        """Initialize kpts from an abinit EIG file."""
        with AbinitEIGFile.from_file(filepath) as eig:
            self.init_kpts_reduced_coordinates(eig.k_points)

    def init_kpts(self, kpts, coordinate_system, **kwargs):
        """Initialize the kpt list.

        Parameters
        ----------
        kpts: 2d array-like
            The list of kpts.
        coordinate_system: str, {'reduced', 'cartesian'}
            The kpts coordinate system.
        """
        if coordinate_system not in ("cartesian", "reduced"):
            raise ValueError(coordinate_system)
        if coordinate_system == "reduced":
            self.init_kpts_reduced_coordinates(kpts, **kwargs)
        else:
            self.init_kpts_cartesian_coordinates(kpts, **kwargs)

    def init_kpts_reduced_coordinates(self, *args, **kwargs) -> None:
        """Initialize kpt list from a list of kpts in reduced coordinates.

        Parameters
        ----------
        kpts: 2d array-like
            The list of kpts in reduced coordinates.
        rounded: int, optional
            If not None, keep kpt coordinates with this many decimals.
        """
        self._init_kpts(*args, **kwargs)
        self.coordinate_system = "reduced"

    def init_kpts_cartesian_coordinates(self, *args, **kwargs) -> None:
        """Initialize kpt list from a list of kpts in cartesian coordinates.

        Parameters
        ----------
        kpts: 2d array-like
            The list of kpts in cartesian coordinates.
        rounded: int, optional
            If not None, keep kpt coordinates with this many decimals.
        """
        self._init_kpts(*args, **kwargs)
        self.coordinate_system = "cartesian"

    def _init_kpts(self, kpts, rounded: int = None) -> None:
        self._check_kpts(kpts)
        if rounded is not None:
            kpts = np.round(kpts, rounded)
        self._kpts = np.array(kpts)
        self._extended_mapping = None

    def init_lattice_from_abinit_input_variables(self, *args, **kwargs):
        """Initialize the lattice object from abinit input variables."""
        self.lattice = Lattice.from_abinit_input_variables(
                *args, loglevel=self._loglevel, **kwargs)

    def init_lattice_from_qe_pw_calculation(self, *args, **kwargs):
        """Initialize the lattice object from a qe_pw calculation."""
        self.lattice = Lattice.from_qe_pw(*args, **kwargs)

    def inside_first_bz(
            self, kpt, reduced_coordinates=True,
            progress_bar: bool = False) -> bool | Sequence[bool]:
        """Check if a kpt lies inside the first BZ or not.

        Parameters
        ----------
        kpt: vector, 2d array
            The kpt to check. Or the list of kpts to check.
        reduced_coordinates: bool, optional
            If True, the kpts given are in reduced coordinates.
            If False they are in cartesian.
        progress_bar: bool, optional
            If True, prints out a progress bar with tqdm.

        Returns
        -------
        bool, array of bools: True if kpt (or kpts) lies inside first BZ.
        """
        if not is_2d_arr(kpt):
            kpt = [kpt]
        self._check_kpts(kpt)
        kpt = np.array(kpt)
        if reduced_coordinates:
            # convert to cartesian
            kpt = self.lattice.reduced_to_cartesian_coordinates(kpt)
        # d = Delaunay(self.bz_vertices)
        # a kpt is in the first BZ if k.b_i / ||b_i|| <= ||b_i|| / 2
        # for each b_i in the reciprocal basis
        results = []
        # # build all possible lattice nearest neighbors vectors
        nns = self.nearest_reciprocal_lattice_points
        nns_norms2 = self.nearest_reciprocal_lattice_points_norms2
        iterable = kpt
        if progress_bar:
            iterable = tqdm.tqdm(
                kpt, total=len(kpt),
                desc="Detecting kpts outside first BZ.")
        for k in iterable:
            # results.append(d.find_simplex(k) >= 0)
            for vector, norm in zip(nns, nns_norms2):
                if k.dot(vector) - norm / 2 > self.tolerance:
                    results.append(False)
                    break
            else:
                results.append(True)
        if len(results) == 1:
            return results[0]
        return results

    def purge_duplicates(self):
        """Remove duplicated kpts."""
        self._logger.debug("Purging duplicated kpts.")
        self._kpts, uniques_idx = np.unique(
                np.round(self.kpts, self.decimal_precision),
                axis=0, return_index=True)
        if self._extended_mapping is not None:
            self._extended_mapping = self._extended_mapping[uniques_idx]
        else:
            self._extended_mapping = uniques_idx
        self._logger.debug("Purging done!")

    def purge_kpts_outside_first_bz(
            self, return_purged_indices=False, return_index_mapping=False):
        """Remove kpts that are outside the first BZ.

        Parameters
        ----------
        return_purged_indices: bool, optional
            If True, the indices of the purged kpts are returned.
        return_index_mapping: bool, optional
            If True, the mapping of the non-purged array indices to the
            purged one is returned. I.e.: given an index of the non-purged
            kpts list I, the index_mapping[I] will return the index of the
            kpts in the purged list. If the kpt is purged, index_mapping[I]
            is set to None.

        Returns
        -------
        tuple: (purged_indices, index_mapping)
            If either of those option is None, a length-1 tuple is returned
            with what we want to be returned.
        """
        indices = np.array(self.inside_first_bz(
                self.kpts,
                reduced_coordinates=self.coordinate_system == "reduced"))
        self._kpts = self.kpts[indices]
        where = np.argwhere(indices)[:, 0]
        if self._extended_mapping is not None:
            self._extended_mapping = self.extended_mapping[where]
        else:
            self._extended_mapping = where
        to_return = []
        if return_index_mapping:
            mapping = []
            count = 0
            for i in indices:
                if i:
                    mapping.append(count)
                    count += 1
                else:
                    # calling this index will result in a numpy error
                    # an index error to be precise
                    mapping.append(None)
            to_return.append(np.array(mapping))
        if return_purged_indices:
            wherenot = np.argwhere(np.logical_not(indices))[:, 0]
            to_return.append(wherenot)
        return tuple(to_return)

    def plot(self, show=True):
        """Plot the kpt mesh.

        Parameters
        ----------
        show: bool, optional
            If True, the plot is displayed.

        Returns
        -------
        Plot object:
            The plot object representing the kpt mesh.
        """
        if self.lattice is not None:
            plot = self.lattice.get_brillouin_zone_plot()
        else:
            plot = Plot(loglevel=self._loglevel)
        if self.coordinate_system == "reduced":
            kpts = self.lattice.reduced_to_cartesian_coordinates(self.kpts)
        else:
            kpts = self.kpts
        plot.add_scatter(
                # flatten in case we have multi-dim arrays
                kpts[..., 0].flatten(),
                kpts[..., 1].flatten(),
                kpts[..., 2].flatten(),
                color="r",
                )
        # plot.add_scatter(
        #         self.bz_vertices[:, 0],
        #         self.bz_vertices[:, 1],
        #         self.bz_vertices[:, 2],
        #         markersize=5,
        #         color="g",
        #         )
        if show:
            plot.plot()
        return plot

    def extend_full_bz(
            self, show_progress=True, invariant_axis=None,
            drop_duplicates=True, internal_loop="symmetries",
            ):
        """Generate kpts in the full BZ from the initial kmesh.

        Parameters
        ----------
        drop_duplicates: bool, optional
            If True, duplicated kpts are removed. In other words, only unique
            kpts are kept.
        show_progress: bool, optional
            If True, a progress bar is shown when computing full kpt grid.
        invariant_axis: int, optional
            If not None, specify which axis we want to keep unchanged.
            E.g.: if invariant_axis = 2, we want to keep all z coordinates
            the same.
        internal_loop: str, {'kpts', 'symmetries'}, optional
            In this function, there is a double-loop: first one is on kpts
            and the other one on symmetries. This argument tells what loop
            is the innermost.
        """
        if self.coordinate_system == "reduced":
            kpts = self.lattice.reduced_to_cartesian_coordinates(self.kpts)
        else:
            kpts = self.kpts
        if internal_loop not in ("symmetries", "kpts"):
            raise ValueError(internal_loop)
        if invariant_axis is not None:
            if invariant_axis not in (0, 1, 2):
                raise ValueError("'invariant_axis' must be an int 0, 1, 2.")
        if internal_loop == "symmetries":
            iterable1 = list(enumerate(kpts))
            iterable2 = self.point_group.symmetries
        else:
            iterable1 = self.point_group.symmetries
            iterable2 = list(enumerate(kpts))
        new_kpts = []
        extended_mapping = []
        if show_progress:
            iterable1 = tqdm.tqdm(
                iterable1, total=len(iterable1),
                desc="Applying symmetries to all kpts")
        for obj1 in iterable1:
            for obj2 in iterable2:
                if internal_loop == "symmetries":
                    kpt = obj1[1]
                    ikpt = obj1[0]
                    symmetry = obj2
                else:
                    kpt = obj2[1]
                    ikpt = obj2[0]
                    symmetry = obj1
                new_kpt = symmetry.apply_symmetry(kpt)
                if invariant_axis is not None:
                    # check if we changed the axis we want.
                    # if yes, skip this symmetry
                    if new_kpt[invariant_axis] != kpt[invariant_axis]:
                        continue
                new_kpts.append(new_kpt)
                extended_mapping.append(ikpt)
        # all of this was done in cartesian coordinates
        # convert back if necessary
        new_kpts = np.array(new_kpts)
        if self.coordinate_system == "reduced":
            self._kpts = self.lattice.cartesian_to_reduced_coordinates(
                    new_kpts)
        else:
            self._kpts = new_kpts
        self._extended_mapping = np.array(extended_mapping)
        if drop_duplicates:
            self.purge_duplicates()

    def fold_inside_first_bz(
            self, kpts=None, coordinate_system=None, rounded=None,
            progress_bar: bool = True,
            ) -> np.ndarray:
        """Fold kpts inside the first BZ.

        Parameters
        ----------
        kpts: 2d array, optional
            The list of kpts to fold inside the first BZ.
            If None, the kptmesh kpts are taken and reset folded.
        coordinate_system: str, optional
            If kpts is not None, this would specify their coordinate system.
        rounded: int, optional
            If not None, all folded kpts will be rounded to this number of
            decimal.
        progress_bar: bool, optional
            If True, a progress bar is displayed.

        Returns
        -------
        folded_kpts: np.ndarray
            If kpts is not None, the folded kpts are returned.
        """
        if kpts is None:
            store_results = True
            kpts = self.kpts
            coordinate_system = self.coordinate_system
        else:
            if coordinate_system not in ("reduced", "cartesian"):
                raise ValueError(
                        "Need to specify 'coordinate_system'.")
            kpts = np.array(kpts)
            store_results = False
        if coordinate_system == "reduced":
            kpts = self.lattice.reduced_to_cartesian_coordinates(kpts)
        where = np.where(
                np.logical_not(
                    self.inside_first_bz(
                        kpts, reduced_coordinates=False,
                        progress_bar=progress_bar)))[0]
        newkpts = []
        wkpts = kpts[where]
        if progress_bar:
            iterable = tqdm.tqdm(
                wkpts,
                total=len(wkpts),
                desc="Folding kpts outside first BZ",
                )
        else:
            iterable = wkpts
        for kpt in iterable:
            # try folding the kpt by using nearest reciprocal lattice vectors
            # (the smallest possible)
            for displ in self.nearest_reciprocal_lattice_points:
                new = kpt + displ
                if self.inside_first_bz(
                        new, reduced_coordinates=False,
                        progress_bar=progress_bar):
                    # found a possible folding
                    newkpts.append(new)
                    break
            else:
                # try using second nearest neighbors
                for displ in self.second_nearest_reciprocal_lattice_points:
                    new = kpt + displ
                    if self.inside_first_bz(
                            new, reduced_coordinates=False,
                            progress_bar=progress_bar):
                        # found a possible folding
                        newkpts.append(new)
                        break
                else:
                    lat = self.lattice
                    raise LookupError(
                            "Could not find folding for "
                            f"'{lat.cartesian_to_reduced_coordinates(kpt)}'.")
        newkpts = np.array(newkpts)
        if len(newkpts):
            kpts[where] = newkpts
        if rounded is not None:
            if not is_int(rounded, positive=True):
                raise ValueError("'rounded' must be a positive integer.")
            kpts = np.round(kpts, rounded)
        if coordinate_system == "reduced":
            kpts = self.lattice.cartesian_to_reduced_coordinates(kpts)
        if store_results:
            self.init_kpts(kpts, coordinate_system, rounded=rounded)
        else:
            return kpts

    def _check_kpts(self, ks):
        # allow for any types of arrays. last axis should be 3D
        shape = np.shape(ks)
        if shape[-1] != 3:
            raise ValueError("kpts should be made of 3D vectors.")
