import math

from ..routines import is_int


def find_divisors(n):
    """Get all divisors of a given integer.

    Inspired from:
    https://www.geeksforgeeks.org/find-divisors-natural-number-set-1/

    Parameters
    ----------
    n: int
        The number from which we want to get all divisors.

    Returns
    -------
    list: The list of divisors. It will include at least 1 and n.

    Raises
    ------
    TypeError: If n is not an integer.
    """
    if not is_int(n, strickly_positive=True):
        raise TypeError(f"n={n} should be > 0.")
    divs = [1]
    if n == 1:
        return divs
    divs.append(n)
    i = 2
    # above sqrt(n), there are no more divisors for sure
    # apart from n itself
    while i <= math.sqrt(n):
        if n % i == 0:
            # i divides n
            if n / i == i:
                # i ^ 2 = n => only add this divisor
                divs.append(i)
            else:
                # add the paired divisor
                divs += [i, int(n / i)]
        i += 1
    return list(sorted(divs))
