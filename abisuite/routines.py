import ast
import asyncio
import os
import warnings
from subprocess import check_output
from typing import Sequence

import aiofiles
import aiofiles.os

import numpy as np

from . import __USER_DATABASE__  # noqa: F401
from .exceptions import CalculationNotFoundError


def check_output_with_retries(
        *args, max_retries=5, allowed_exceptions=None, **kwargs):
    """Similar to the 'check_output' function from the subprocess module.

    But with retries.

    Parameters
    ----------
    max_retries: int, optional
        The maximal number of retries.
    allowed_exceptions: list-like, optional
        The allowed exception types to happen. If None, all exceptions
        are allowed. Basically this would set this parameter to [Exception, ].
    """
    from .retryer import try_with_retries_sync
    return try_with_retries_sync(
            check_output, func_args=args, func_kwargs=kwargs,
            max_retries=max_retries, allowed_exceptions=allowed_exceptions)


def check_abspath(path, join_before=None):
    """Check if a path is absolute.

    If not, make it absolute
    and if needed, join a path to it.

    Parameters
    ----------
    path : str
           The path to check if it is absolute.
    join_before : str, optional
                  If path is not absolute, join this path before
                  making it absolute.
    """
    # check if path is absolute, if not, join workdir and return absolute
    if not os.path.isabs(path):
        if join_before is not None:
            path = os.path.join(join_before, path)
        return full_abspath(path)
    return path


def connect_to_database(
        database: str = None,
        ) -> None:
    """Connect abisuite to the given database.

    On startup, abisuite is not connected to any database. Use this function
    to connect to one.

    Parameters
    ----------
    database: str, optional
        The path to the database we want to connect to. If None,
        the default database path from the config file is taken.

    Raises
    ------
    TypeError: if database is not None and not a string.
    ValueError: If database is None and no database is defined in config.
    RuntimeError: If a database is already connected and it's not the same.
    FileNotFoundError: If the provided database path does not exist.
    ImportError: When 'peewee' module cannot be imported.
    """
    if not is_str(database):
        if database == "default" or database is None:
            from . import __USER_CONFIG__
            database = __USER_CONFIG__.DATABASE.path
        if "__TEST__" in database:
            # we use a testing database which we assume is connected already
            return
        if database is None:
            # database can still be nont if config entry is not set
            raise ValueError(
                    "No database to connect to and none defined in config.")
        if not is_str(database):
            raise TypeError(database)
    if not os.path.isfile(database):
        raise FileNotFoundError(
                f"No databases here: '{database}'.")
    global __USER_DATABASE__
    if __USER_DATABASE__ is None:
        # couldnt load peewee (in main __init__ file).
        raise ImportError("Install 'peewee' in order to connect to databases.")
    if __USER_DATABASE__.database is None:
        __USER_DATABASE__.init(database)
    if __USER_DATABASE__.database != database:
        # raise RuntimeError(
        #         f"Current database is '{__USER_DATABASE__.database}'. "
        #         f"Cannot connect to different database '{database}'.")
        # load a different db
        del __USER_DATABASE__
        from peewee import SqliteDatabase
        __USER_DATABASE__ = SqliteDatabase(None)
        __USER_DATABASE__.init(database)
    __USER_DATABASE__.connect(reuse_if_open=True)
    from .databases import DBCalculation
    from .retryer import try_with_retries_sync
    try_with_retries_sync(
            __USER_DATABASE__.create_tables,
            func_args=([DBCalculation], ),
            func_kwargs={"safe": True},
            delay=5)


def command_to_calctype(command):
    """From a command line (or path to an executable), returns the calctype.

    Parameters
    ----------
    command: str
        The command path.

    Returns
    -------
    str: The calctype associated with the command.

    Raises
    ------
    NotImplementedError: In case it's not possible to determine calctype.
    """
    script_name = os.path.basename(command)
    if script_name == "abinit":
        return "abinit"
    if script_name.startswith("epw"):
        return "qe_epw"
    if script_name.startswith("pw"):
        return "qe_pw"
    raise NotImplementedError(
            f"Could not determine calctype from: '{command}'.")


def decompose_line(line, float_dtype=float, split=" "):
    """Decompose a line of text / string into its fundamental constituants.

    Parameters
    ----------
    line: str
        The line to decompose.
    float_dtype: callable, optional
        The float type in which floats will be converted.
        Default is the builtin float class from python.
    split: str, optional
        Separator on which we split the line. By default it's an empty space.

    Returns
    -------
    A tuple of 3 lists:
        - First list is the strings (or non-floats and non-integers).
        - Second is the list of integers in the line.
        - Third is the list of floats in the line casted using the
          'float_dtype' argument.
    """
    # from here we have the lines from either format
    # varname         value
    # value
    splitted = line.split(split)
    strings = []
    ints = []
    floats = []
    for element in splitted:
        try:
            if element == "":
                # drop the empty strings
                continue
            # check if element is an integer
            try:
                i = int(element)
            except ValueError:
                # not an integer
                pass
            else:
                ints.append(i)
                continue
            # check if element is a float
            try:
                f = float_dtype(element)
            except ValueError:
                # not a float
                pass
            else:
                floats.append(f)
                continue
            # check if it is a fraction or radical exposed as a string
            try:
                if element in ("None", ):
                    strings.append("None")  # return as a string
                    continue
                f = ast.literal_eval(element)
            except (NameError, SyntaxError, ValueError):
                # not a fraction-like string
                pass
            else:
                # append only if it is a float
                if callable(f):
                    # return it to a string. This means the word was eval
                    # as a valid python function like "max" or "min" or etc.
                    strings.append(f.__name__)
                elif isinstance(f, int):
                    ints.append(f)
                elif isinstance(f, float):
                    floats.append(float_dtype(f))
                else:
                    strings.append(f)
                continue
            if "E-" in element and "." in element:
                # maybe there is a bug with abinit and two floats are
                # glued together. try to separate them
                f1, f2 = _try_debug_2floats(element, float_dtype)
                if f1 is not None:
                    # it worked !
                    floats.append(f1)
                    floats.append(f2)
                    continue
            # from here, element is neither a float nor an int => string
            strings.append(element)
        except SyntaxError as e:
            # if we're here, something unexpected happened in the file.
            # return None and alert user
            warnings.warn(
                    "Can't parse line for some reason: %s" % line,
                    stacklevel=2)
            raise e
    return strings, ints, floats


def full_abspath(path):
    """Same as abspath + expanduser."""
    return os.path.abspath(os.path.expanduser(path))


def full_path_split(path):
    """Returns a tuple of all the part of a path.

    E.g.: /path/to/a/file will return ('path', 'to', 'a', 'file').
    """
    toreturn = []
    split = os.path.split(path)
    while split[0]:
        toreturn.append(split[-1])
        split = os.path.split(split[0])
    toreturn.append(split[-1])
    return tuple(reversed(toreturn))


async def get_calculation_directory_from_calculation_file(
        filepath: str,
        max_layers: int = None,
        ):
    """Get the calcdir path from a file within a calc dir.

    This method recursively goes up inside the directory tree
    until it finds a calculation directory.

    Parameters
    ----------
    filepath: str or BaseFileHandler
        The path of the file to get its calcdir from.
    max_layers: int, optional
        If not None, specifies how high we can climb up into the
        directory tree. For instance, a file inside an output data directory
        would require max_layers=2 only. If None, we go up to to top!

    Returns
    -------
    str: The path of the calcdir that contains the input file.

    Raises
    ------
    CalculationNotFoundError:
        If the given file does not lie inside a directory or if we
        reached the maximum number of layers we can go up as specified
        by the 'max_layers' argument.
    """
    from .handlers.file_handlers.bases import BaseFileHandler
    from .handlers import SymLinkFile
    from .handlers import is_calculation_directory
    if isinstance(filepath, SymLinkFile):
        async with filepath:
            dirname = os.path.dirname(filepath.source)
    elif isinstance(filepath, BaseFileHandler) and not (
            isinstance(filepath, SymLinkFile)):
        dirname = os.path.dirname(filepath.path)
    else:
        dirname = os.path.dirname(filepath)
    n_layer = 0
    while not await is_calculation_directory(dirname):
        if max_layers is not None:
            if n_layer > max_layers:
                raise CalculationNotFoundError(
                        "Reached maximum layers ({max_layers}) we can go up to"
                        f" find the calculation dir of '{filepath}'."
                        )
        if dirname == os.path.dirname(dirname):
            raise CalculationNotFoundError(
                    "Reached uppermost root directory we can go up while "
                    f"trying to find calcdir of '{filepath}'."
                    )
        dirname = os.path.dirname(dirname)
        n_layer += 1
    return dirname


def is_dict(obj):
    """Return True if the given object is dict-like."""
    return isinstance(obj, dict)


is_dict_like = is_dict


def is_list_like(obj, length=None):
    """Return True if the given object is list-like.

    Parameters
    ----------
    length: int, optional
        If length is not None, the object must also have this length
        in order for this function to return True.

    Returns
    -------
    bool: True if object respects all aforementioned conditions.
          False otherwise.
    """
    if length is not None:
        if not is_int(length, strickly_positive=True):
            raise ValueError("'length' should be an integer > 0.")
    allowed_types = (list, tuple, np.ndarray, set)
    for allowed_type in allowed_types:
        if not isinstance(obj, allowed_type):
            continue
        if length is not None:
            if len(obj) != length:
                return False
        return True
    from .variables.bases import BaseInputVariable
    if isinstance(obj, BaseInputVariable):
        return is_list_like(obj.value)
    return False


def is_integer(obj, positive=False, strickly_positive=False):
    """Return True of obj is an integer type.

    Parameters
    ----------
    obj: The object to test.
    positive: bool, optional
        If True, the integer is also checked to be positive. Thus, negative
        integer will return False. 0 is counted as both positive and
        negative.
    strickly_positive: bool, optional
        Same as the 'positive' arg but does not count 0 as positive.

    Returns
    -------
    bool: True if given obj is an integer. False otherwise.
    """
    # bools are not integers even though they are instance of ints
    if isinstance(obj, bool):
        return False
    for typ_ in (int, np.int32, np.int64):
        if isinstance(obj, typ_):
            if positive and obj < 0:
                return False
            if strickly_positive and obj <= 0:
                return False
            return True
    return False


is_int = is_integer  # alias


def is_none_numpy_safe(obj):
    """Return True if obj is None or a numpy array of None."""
    if obj is None:
        return True
    if isinstance(obj, np.ndarray):
        if obj.ndim > 0:
            return False
    if obj == np.array(None):
        return True
    return False


def is_scalar(
        obj, strickly_positive: bool = False, positive: bool = False,
        ) -> bool:
    """Tells if an object is a scalar (int or float).

    Parameters
    ----------
    obj : The object to classify.
    strickly_positive: bool, optional
        If True, the scalar must also be > 0 in order for this function
        to return True.
    positive: bool, optional
        If True, the scalar must also be >= 0.

    Returns
    -------
    True : If the object is an int or a float.
    False : if not.
    """
    if positive and strickly_positive:
        raise ValueError(
                "Cannot check both 'positive' and 'strickly_positive'.")
    if is_integer(obj, positive=positive, strickly_positive=strickly_positive):
        return True
    allowed_float_types = (
            float, np.number, np.float32, np.float64,
            )
    for typ in allowed_float_types:
        if isinstance(obj, typ):
            if positive and obj < 0:
                return False
            if strickly_positive and obj <= 0:
                return False
            return True
    return False


def is_scalar_or_str(obj):
    """Tells if an object is a scalar (int or float) or a str.

    Parameters
    ----------
    obj : The object to classify.

    Returns
    -------
    True : If the object is an int, a float or a str
    False : if not.
    """
    if is_str(obj):
        return True
    return is_scalar(obj)


def is_str(obj):
    """Return True if object is a string."""
    return isinstance(obj, str)


def is_vector(obj, length=None, dtype: type = None, **kwargs) -> bool:
    """Returns True if 'obj' is a vector (i.e.: a 1D list-like obj).

    Parameters
    ----------
    obj: The object to test out.
    length: int
       If not None, adds a check on the length of the vector. If
       vector does not have this length, False is returned.
    dtype: type, optional
        If not None, checks the type of every element.
        If a mismatch occur, it returns False. Can be set to
        "str" or "scalar":

         * if "str" or str: elements are determined to be a string
           using the 'is_str' function.
         * if int, float or "scalar": elements are determined to
           be a scalar using the "is_scalar" function.
         * In both cases above, other kwargs arguments are
           passed to the underlying checking function.

    Returns
    -------
    bool: True if obj respects all the abovementioned conditions.
    """
    if not is_list_like(obj):
        return False
    # do this as numpy will not support anymore creating nested
    # arrays of object soon
    shape = np.array(list(obj), dtype=object).shape
    if len(shape) != 1:
        return False
    if length is not None:
        if not isinstance(length, int):
            raise TypeError("Length must be an integer!")
        if len(obj) != length:
            return False
    if dtype is not None:
        for element in obj:
            if dtype in ("scalar", int, float):
                if not is_scalar(element, **kwargs):
                    return False
            elif dtype in ("str", str):
                if not is_str(element, **kwargs):
                    return False
            else:
                if not isinstance(element, dtype):
                    return False
    return True


def vectors_equal(*args) -> bool:
    """Check if given vectors are equals.

    Return true if all entries are vectors, if they are same length
    and if they are element wise equal.
    """
    if len(args) < 2:
        raise ValueError("Need to compare at least 2 vectors.")
    n = None  # number of elements
    # check first if all element are vectors and they are same length
    for vec in args:
        if n is None:
            if not is_vector(vec):
                return False
            n = len(vec)
        else:
            if not is_vector(vec, length=n):
                return False
    # check elementwise equality
    for vec in args[1:]:
        for el1, el2 in zip(args[0], vec):
            if el1 != el2:
                return False
    return True


def is_2d_arr(obj, size=None):
    """Tells if an object is a 2d array or not.

    Parameters
    ----------
    obj : The object to test.
    size: length-2 tuple, optional
        If not None, adds a check on the shape of the 2d array.
        If the array does not match this size, False is returned.

    Returns
    -------
    True : If the object is 2-dimensionnal array.
    False : If not.
    """
    if not is_list_like(obj):
        return False
    shape = np.shape(list(obj))
    if len(shape) != 2:
        return False
    if size is not None:
        if not is_vector(size, length=2):
            raise ValueError("'size' must be a length-2 tuple.")
        if shape[0] != size[0] or shape[1] != size[1]:
            return False
    return True


is_2d_array = is_2d_arr  # alias


def return_list(arg):
    """Given the argument, returns a list.

    If it is a single string, returns a list of the string.
    If it is None, returns an empty list.
    """
    if arg is None:
        return []
    if isinstance(arg, str):
        return [arg]
    return arg


def sort_data(tosort, *args):
    """Sort all args according to the first array.

    Parameters
    ----------
    tosort : array
             The array to sort. All other arrays will be sorted the
             same as this one.
    """
    # check all arrays has the same len
    all_arrays = [tosort] + list(args)
    for array in all_arrays:
        if not is_list_like(array):
            raise TypeError("Can only sort list-like objects.")
    if is_2d_arr(tosort):
        raise TypeError("Cannot sort 2d array.")
    length = len(tosort)
    for array in args:
        if len(array) != length:
            raise ValueError("Arrays should have the same length.")
    sorted_indices = np.argsort(tosort)
    sorted_arrays = []
    for array in all_arrays:
        # manual sort to avoid numpy arrays
        newarr = []
        for index in sorted_indices:
            newarr.append(array[index])
        sorted_arrays.append(newarr)
    # sorted_args = [x for _, *x in sorted(zip(tosort, *args))]
    # sorted_args = [arr for arr in sorted_args.T]
    # tosort = sorted(tosort)
    # return [tosort] + sorted_args
    return sorted_arrays


def suppress_warnings(func):
    """Decorator to suppress warnings.

    Warnings that would be created during the
    function's call.
    """
    if asyncio.iscoroutinefunction(func):
        async def suppressed_func(*args, **kwargs):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                return await func(*args, **kwargs)
    else:
        def suppressed_func(*args, **kwargs):
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                return func(*args, **kwargs)
    return suppressed_func


def _try_debug_2floats(string, float_dtype):
    # try to separate two floats glued in a string e.g. 1.52E-0210.000
    idx = string.index("E")
    # there is always two numbers in the exponent
    split = idx + 4
    f1 = string[:split]
    f2 = string[split:]
    try:
        f1 = float_dtype(f1)
        f2 = float_dtype(f2)
    except Exception:
        warnings.warn(
                "%s could not be decomposed into 2 floats." % string,
                stacklevel=2)
        return None, None
    else:
        # it worked
        return f1, f2


async def expand_symlink(path: str) -> str:
    """Expands a symlink to a true path if necessary.

    This goes recursive in the list of dirnames.
    """
    if path == "/":
        # certainly not a symlink
        return path
    if await aiofiles.os.path.islink(path):
        real = await aiofiles.os.readlink(path)
        if not os.path.isabs(real):
            # relative link
            return full_abspath(os.path.join(os.path.dirname(path), real))
        return real
    expanded_dirname = await expand_symlink(os.path.dirname(path))
    return os.path.join(expanded_dirname, os.path.basename(path))


async def expand_symlinks(paths: Sequence[str]) -> Sequence[str]:
    """Returns true paths from a given list of paths.

    Parameters
    ----------
    paths: list-like
        The list of paths to expand.

    Returns
    -------
    list: The expanded list of paths.
    """
    coros = []
    for path in paths:
        coros.append(expand_symlink(path))
    return await asyncio.gather(*coros)
