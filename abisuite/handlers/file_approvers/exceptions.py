# This file contains custom exceptions for approvers


class PseudosError(Exception):
    """Custom error for a pseudopotential file."""

    pass


class InputFileError(Exception):
    """Custom error for an input file."""

    pass


class MetaDataError(Exception):
    """Custom error for a meta data file."""

    pass


class MPIError(Exception):
    """Custom error for mpi errors."""

    pass


class PBSError(Exception):
    """Custom error for a pbs file."""

    pass


class SymLinkError(Exception):
    """Custom error for a symlink file."""

    pass
