from ..bases import BaseApprover
from ..exceptions import InputFileError
from ...file_structures import QEKpointsInputStructure


class QEKpointsInputApprover(BaseApprover):
    """Input approver for the input file of the kpoint QE utility."""

    _exception = InputFileError
    _loggername = "QEKpointsInputApprover"
    _structure_class = QEKpointsInputStructure

    async def validate(self):
        """Validate input file."""
        await super().validate()
