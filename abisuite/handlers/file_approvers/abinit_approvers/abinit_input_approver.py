import os
from typing import Any, Sequence

from async_property import async_property

import numpy as np

from ..bases import BaseInputApprover, BaseInputParalApprover
from ..exceptions import InputFileError
from ...bases import BaseUtility
from ...file_structures import AbinitInputStructure
from ....routines import (
        is_2d_arr,
        is_list_like, is_vector,
        )
from ....variables import ALL_ABINIT_VARIABLES, InputVariableDict

# there is utilities in abipy to validate input files but the only thing they
# do is to call abinit in dry run and check for errors. This approver class
# is more passive as it does not call abinit.

TOLERANCES_VARS = ("toldfe", "tolwfr", "toldff", "tolrff", "tolvrs")
ATOMIC_POSITION_VARS = ("xcart", "xred")
KPTS_DEF_VARS = ["ngkpt", "kptrlatt", "kptbounds", "nkpt"]
PARAL_VARS = ("npfft", "npkpt", "npband", "npspinor", "nphf")


class DefaultAbinitVariableGetter(BaseUtility):
    """Compute the default variable of a given variable."""

    _loggername = "DefaultAbinitVariableGetter"
    _variables_that_need_pseudos = (
            "ixc", "nband", "usepaw",
            )

    def __init__(
            self, input_variables: InputVariableDict,
            pseudopotential_handlers: Sequence = None,
            **kwargs):
        """Init method.

        Parameters
        ----------
        input_variables: InputVariableDict
            The dict of input variables given to abinit.
        pseudopotential_handlers: list, optional
            For some variables, their default values depend on the pseudo
            potential. This is the list of corresponding handlers.
        """
        self.input_variables = input_variables
        self.pseudo_files = pseudopotential_handlers
        self._all_values = {}

    def __getitem__(self, varname: str) -> Any:
        """Return the requested variable value."""
        return self.get_abinit_variable_value(varname)

    def get_abinit_variable_value(self, varname: str) -> Any:
        """Get or return the value of an abinit variable.

        Will look first in given input variables dict and return that
        value if found. Otherwise, uses all available info to compute
        the value.

        Parameters
        ----------
        varname: str
            The variable we want.

        Returns
        -------
        The value of the variable we want or default value.
        """
        if varname in self._all_values:
            # don't recompute if already recomputed
            return self._all_values[varname]
        # return the variable value. if it is not defined in the input
        # variables, the default value is computed
        if varname in self.input_variables:
            return self.input_variables[varname]
        if varname in self._variables_that_need_pseudos and (
                self.pseudo_files is None):
            raise ValueError(
                    f"{varname} needs pseudos files but they are not given.")
        getfuncname = f"_get_{varname}"
        if not hasattr(self, getfuncname):
            raise NotImplementedError(
                    f"Getter function not written for '{varname}'.")
        get_func = getattr(self, getfuncname)
        var = get_func()
        self._all_values[varname] = var
        return var

    def _get_ecutsigx(self):
        return 0.0

    def _get_fftgw(self):
        return 21

    def _get_gwcalctyp(self):
        return 0

    def _get_iscf(self):
        # default is 17 if %usepaw == 1, 0 if usewvl == 1, 7 otherwise
        usepaw = self.get_abinit_variable_value("usepaw")
        if usepaw == 1:
            return 17
        usewvl = self.get_abinit_variable_value("usewvl")
        if usewvl == 1:
            return 0
        return 7

    def _get_ixc(self):
        # the default is set from the pseudo potential files
        # all functionals should be the same between pseudos
        ixcs = [pseudo.ixc for pseudo in self.pseudo_files]
        if len(ixcs) > 1:
            for ixc, pseudo in zip(
                    ixcs[1:], self.pseudo_files[1:]):
                if ixc != ixcs[0]:
                    raise ValueError(
                            f"pseudo '{pseudo.basename}' ixc ({ixc})"
                            f" is not the same as the others ({ixcs[0]})!")
        return ixcs[0]

    def _get_kpt(self):
        return [[0, 0, 0]]

    def _get_kptbounds(self):
        return None

    def _get_kptnrm(self):
        return 1.0

    def _get_kptopt(self):
        # default is 4 if nspden == 4, 1 otherwise
        nspden = self["nspden"]
        if nspden == 4:
            return 4
        return 1

    def _get_kptrlatt(self):
        return 0

    def _get_nband(self):
        # default is computed from abinit
        # which is total number of valence electrons + 1
        # (actually possibly a more complicated formula)
        # from pseudos compute total number of valence electrons
        # count number of each type of atoms
        # only for occopt != 2 that nband may be ommitted
        if self["occopt"] == 2:
            raise ValueError("nband should be provided.")
        natom_per_type = [0] * self["ntypat"]
        for typat in self["typat"]:
            natom_per_type[typat - 1] += 1
        nval = 0
        for pseudo, natom in zip(self.pseudo_files, natom_per_type):
            nval += natom * pseudo.zion
        return nval // 2 + 1

    def _get_ngkpt(self):
        return [0, 0, 0]

    def _get_nkpt(self):
        # 1 if kptopt == 0, 0 otherwise
        if self["kptopt"] == 0:
            return 1
        return 0

    def _get_nfreqre(self):
        return 0

    def _get_nshiftk(self):
        return 1

    def _get_nspden(self):
        # default is nsppol
        return self["nsppol"]

    def _get_nsppol(self):
        return 1

    def _get_occ(self):
        # default is a 2d array whose size depends of occopt
        occopt = self["occopt"]
        nband = self["nband"]
        if occopt == 0:
            # occ is a 1 x nband array
            return [0] * nband
        elif occopt == 2:
            # occ is a nkpt x nband x nsppol array
            nsppol = self["nsppol"]
            nkpt = self["nkpt"]
            return [[0] * nband] * (nkpt * nsppol)
        # this var is ignored in other cases
        return 0

    def _get_occopt(self):
        return 1

    def _get_optdriver(self):
        return 0

    def _get_pawprtdos(self):
        return 0

    def _get_pawxcdev(self):
        # https://docs.abinit.org/variables/paw/#pawxcdev
        return 1

    def _get_prtdosm(self):
        return 0

    def _get_rfatpol(self):
        return [1, 1]

    def _get_shiftk(self):
        nshiftk = self["nshiftk"]
        if nshiftk <= 1:
            return [[0.5, 0.5, 0.5]]
        return None

    def _get_usepaw(self):
        # this variable is set when reading the pseudopotentials
        # for paw pseudos, it is set to 1 otherwise 0
        for pseudo in self.pseudo_files:
            if pseudo.is_paw:
                return 1
        return 0

    def _get_usewvl(self):
        return 0

    def _get_vdw_xc(self):
        return 0


class AbinitInputApprover(BaseInputApprover):
    """Input approver for an Abinit calculation.

    Class that checks if a set of input variable is
    valid for an abinit calculation. The 'valid' attribute states
    if the input should be good. If it is False, abinit will raise an error
    if it is launched with these variables.
    """

    _loggername = "AbinitInputApprover"
    _variables_db = ALL_ABINIT_VARIABLES
    _structure_class = AbinitInputStructure

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._is_gw_run = None
        self._is_bse_run = None
        self._default_var_getter = None

    @async_property
    async def is_bse_run(self):
        """Return True if the input variables depict a BSE run."""
        if self._is_bse_run is not None:
            return self._is_bse_run
        optdriver = await self._get_var("optdriver")
        if optdriver == 99:
            self._is_bse_run = True
        else:
            self._is_bse_run = False
        return self._is_bse_run

    @async_property
    async def is_gw_run(self):
        """Return True if the input variables depict a GW run."""
        if self._is_gw_run is not None:
            return self._is_gw_run
        optdriver = await self._get_var("optdriver")
        if optdriver in (3, 4):
            self._is_gw_run = True
        else:
            self._is_gw_run = False
        return self._is_gw_run

    async def validate(self):
        """Validate the input file."""
        ndtset = self.input_variables.get("ndtset", 1)
        if ndtset > 1:
            self._logger.warning("Input approval is not implemented"
                                 " for multidtset.")
            return
        await self._check_basics()
        await self._check_bse()
        self._check_cell()
        await self._check_gw()
        await self._check_bse()
        self._check_relax()
        self._check_nscf_ok()
        await self._check_kpts()
        await self._check_nbands()
        await self._check_dfpt()
        await self._check_printing()
        await self._check_iscf()
        await self._check_occupations()
        await super().validate()

    async def _validate_pseudos(self):
        await super()._validate_pseudos()
        await self._check_nspinor()
        await self._check_znucl()
        await self._check_nbands()
        # if pseudos are paw ones, check input variables have been set correct
        await self._check_paw()
        await self._check_vdw()

    async def _check_basics(self) -> None:
        keys = list(self.input_variables.keys())
        if not await self.is_gw_run and not await self.is_bse_run:
            check2, presents2 = self._only_one_in(TOLERANCES_VARS, keys)
        else:
            check2, presents2 = True, []
        check3, presents3 = self._only_one_in(ATOMIC_POSITION_VARS, keys)
        check4, presents4 = self._only_one_in(
                KPTS_DEF_VARS,
                keys,
                )
        for check, presents, vars_ in zip(
                [check2, check3, check4],
                [presents2, presents3, presents4],
                [TOLERANCES_VARS, ATOMIC_POSITION_VARS,
                 KPTS_DEF_VARS,
                 ]):
            if check:
                continue
            self.errors.append(
                    f"{presents} are presents in the input file but"
                    f" there should be only one from {vars_}.")
        # check that ntypat is equal to number of elements in znucl
        ntypat = self.input_variables.get("ntypat", 1).value
        if "znucl" not in self.input_variables:
            self.errors.append("znucl must be defined.")
            return
        znucl = self.input_variables["znucl"].value
        if isinstance(znucl, int) or isinstance(znucl, float):
            znucl = (znucl, )
        if ntypat != len(znucl):
            self.errors.append(f"Number of atom types ({repr(ntypat)}) doesn't"
                               f" match the znucl variable ({repr(znucl)}).")
        # check that natom = len(typat)
        natom = self.input_variables.get("natom", 1)
        natrd = self.input_variables.get("natrd", None)
        typat = self.input_variables.get("typat", [])
        # typat default is [] same reason as znucl above
        if isinstance(typat.value, int):
            typat.value = (typat.value, )
        if natrd.value is None:
            # compare natom to length of typat
            if natom != len(typat):
                self.errors.append(f"Number of atoms ({natom.value}) "
                                   "does not match the typat variable "
                                   f"({typat.value}).")
        else:
            # compare typat to natrd
            if len(typat) != natrd:
                self.errors.append(
                        f"Length of typat ({typat.value}) does not match "
                        f"natrd ({natrd.value}).")
            if natrd != natom and ("spgroup" not in self.input_variables and
                                   "nsym" not in self.input_variables):
                self.errors.append(
                        f"Number of atoms ({natom.value}) must match the "
                        f"number of atoms to read natrd ({natrd.value})")
        # compare natom to number of coordinates given
        for varname in ATOMIC_POSITION_VARS:
            if varname not in self.input_variables:
                continue
            var = self.input_variables[varname].value
            if not is_2d_arr(var):
                var = [var]
            if len(var) != natom:
                self.errors.append(
                        f"Number of atoms ({natom.value}) does not match "
                        f"the number of atomic coordinates given ({varname}).")

    def _check_cell(self):
        acell_in = "acell" in self.input_variables
        scalecart_in = "scalecart" in self.input_variables
        if not acell_in and not scalecart_in:
            self.errors.append(
                    "Either one in 'acell' and 'scalecart' should be defined.")
        if "rprim" in self.input_variables:
            # check that primitive vectors give a non-zero unit cell volume
            rprim = np.array(self.input_variables["rprim"]).astype(float)
            if acell_in:
                acell = self.input_variables["acell"]
                rprim[0] *= float(acell[0])
                rprim[1] *= float(acell[1])
                rprim[2] *= float(acell[2])
            if scalecart_in:
                scalecart = self.input_variables["scalecart"]
                rprim[:, 0] *= scalecart[0]
                rprim[:, 1] *= scalecart[1]
                rprim[:, 2] *= scalecart[2]
            if rprim[0].dot(np.cross(rprim[1], rprim[2])) == 0.0:
                # volume is zero, raise error
                self.errors.append(
                        "The choice of 'rprim+acell+scalecart' gives a unit "
                        "cell volume of 0.0. Rectify this it is an ordah!")
        if "tnons" in self.input_variables:
            # check that tnons is a 3xN array
            tnons = np.array(self.input_variables["tnons"])
            if len(tnons.shape) != 2 or tnons.shape[-1] != 3:
                self.errors.append(
                        f"'tnons' must be a 3xN array but we got: '{tnons}'.")

    async def _check_dfpt(self):
        # checks for response function calculations
        rfphon = self.input_variables.get("rfphon", 0).value
        rfelfd = self.input_variables.get("rfelfd", 0).value
        if not rfphon and not rfelfd:
            # not a dfpt calc
            return
        qpt = self.input_variables.get("qpt", [0.0, 0.0, 0.0]).value
        qpt = list(qpt)
        if qpt != [0.0, 0.0, 0.0]:
            if rfelfd:
                self.errors.append(
                        "Cannot compute electric field response function for "
                        "non-gamma qpts => "
                        f"rfelfd = '{rfelfd}' and qpt = '{qpt}'.")
        # rfatpol (len=2) should match number of atoms
        # that means rfatpol(:) >= 1 and <= natom
        # also rfatpol(1) <= rfatpol(2)
        rfatpol = await self._get_var("rfatpol")
        natom = await self._get_var("natom")
        if rfatpol[0] > rfatpol[1]:
            self.errors.append("rfatpol[0] should be <= rfatpol[1]")
        if rfatpol[0] > natom or rfatpol[1] > natom:
            self.errors.append("rfatpol[:] should be <= natom")
        if rfatpol[0] < 1 or rfatpol[1] < 1:
            self.errors.append("rfatpol[:] should be >= 1")
        # if doing DFPT + PAW + GGA calc, pawxcdev must be set to 0
        # it is also recommended to use it for GS run.
        usepaw = await self._get_var("usepaw")
        if usepaw != 1:
            return
        ixc = await self._get_var("ixc")
        pawxcdev = await self._get_var("pawxcdev")
        if pawxcdev == 0:
            return
        # get xclevel (from 56_xc/m_xclevel.F90)
        # if xclevel == 2 => we use GGA
        xclevel = 1
        if isinstance(ixc, str):
            if ixc.lower() == "pbe":
                xclevel = 2
        else:
            if (ixc >= 11 and ixc <= 19) or (ixc >= 23 and ixc <= 29) or (
                    ixc == 1402000):
                xclevel = 2
            else:
                # I tried to determine the algorithm but abinit
                # calls libxc at this
                # point and since my knowledge of C is limited...
                # worst case is that abinit crashes on start
                self._logger.info(
                        "Could not analyze ixc type to get xclevel...")
                return
        if xclevel >= 2:
            self.errors.append(
                    "Need to set 'pawxcdev'=0 because. N.B.: It is "
                    "recommended to use pawxcdev=0 in GS run as well. "
                    "See: https://docs.abinit.org/variables/paw/#pawxcdev")
            return

    async def _check_gw(self) -> None:
        if not await self.is_gw_run:
            return
        optdriver = await self._get_var("optdriver")
        if optdriver == 3:
            if "ecuteps" not in self.input_variables:
                self.errors.append(
                        "optdriver == 3 but 'ecuteps' is not defined.")
            gwcalctyp = await self._get_var("gwcalctyp")
            nfreqre = await self._get_var("nfreqre")
            if "ppmfrq" not in self.input_variables and (
                    gwcalctyp in (0, 10, 20) and nfreqre != 1):
                self.errors.append(
                        "optdriver == 3 but 'ppmfrq' is not defined.")
        if optdriver == 4:
            # check that bdgw matches nkptgw
            nkptgw = await self._get_var("nkptgw")
            bdgw = await self._get_var("bdgw")
            if len(np.shape(bdgw)) == 1:
                bdgw = [bdgw]
            if np.shape(bdgw) != (nkptgw, 2):
                self.errors.append(
                        f"bdgw's shape {np.shape(bdgw)} don't match nkptgw x 2"
                        f" ({nkptgw} x 2)."
                        )
            ecutsigx = await self._get_var("ecutsigx")
            if ecutsigx <= 0.0:
                self.errors.append("Need to set 'ecutsigx' > 0.0.")
            await self._check_gw_self_energy_kptgw()

    async def _check_gw_self_energy_kptgw(self) -> None:
        # check that kptgw is a computed scf kpt
        from ...file_handlers import AbinitLogFile
        kptgw = await self._get_var("kptgw")
        for parent in self.parents:
            async with await AbinitLogFile.from_calculation(
                    parent, loglevel=self._loglevel) as log:
                optdriver = log.input_variables.get("optdriver", 0)
                if optdriver != 0:
                    continue
                kpts = log.input_variables["kpt"]
                break
        else:
            raise InputFileError(
                    "Could not find GS calculation amongst "
                    f"parents: {self.parents}")
        for kpt in kpts:
            if (np.array(kptgw) == kpt).all():
                return
        else:
            if len(kpts) == 50:
                # probably abinit didn't print out all kpts
                self._logger.warning(
                        f"Could not find kptgw={kptgw} within kpts of GS calc."
                        " Maybe we reached limit of 50 printed kpts by abinit."
                        " Assuming it's fine but maybe double check."
                        )
                return
        self.errors.append(
                f"kptgw = {kptgw} not in list of scf kpts.")

    async def _check_bse(self):
        if not await self.is_bse_run:
            return
        # check that fftgw matches
        from ...file_handlers import AbinitInputFile
        fftgw = await self._get_var("fftgw")
        for parent in self.parents:
            async with await AbinitInputFile.from_calculation(
                    parent, loglevel=self._loglevel) as inp:
                if "optdriver" not in inp.input_variables:
                    continue
                optdriver = inp.input_variables["optdriver"]
                if optdriver != 3:
                    continue
                parent_fftgw = inp.input_variables.get(
                        "fftgw", 21).value
                break
        else:
            raise FileNotFoundError(
                    "Could not find parent GW screening run.")
        if hasattr(fftgw, "value"):
            fftgw = fftgw.value
        if str(parent_fftgw) != str(fftgw):
            self.errors.append(
                    f"fftgw={fftgw} != GW fftgw = {parent_fftgw} "
                    "they should match.")

    async def _check_iscf(self):
        # check iscf
        iscf = await self._get_var("iscf")
        kptopt = await self._get_var("kptopt")
        if kptopt < 0 and iscf != -2:
            # iscf must be set to -2
            self.errors.append("If kptopt < 0, iscf must be set to -2.")

    async def _check_kpts(self):
        # check kpts
        kptopt = await self._get_var("kptopt")
        if kptopt == 0:
            # need to check kpt, nkpt, kptnrm matches
            nkpt = await self._get_var("nkpt")
            kpt = await self._get_var("kpt")
            if not is_2d_arr(kpt):
                kpt = [kpt]
            kptnrm = await self._get_var("kptnrm")
            if np.any(np.abs(kpt) > kptnrm):
                self.errors.append(
                        "All kpt coordinates must lie between "
                        "[-kptnrm, kptnrm].")
            if nkpt != len(kpt):
                self.errors.append(
                        "nkpt does not match total number of kpts.")
        elif kptopt in (1, 2, 3, 4):
            # need to check nshiftk and shiftk
            nshiftk = await self._get_var("nshiftk")
            if nshiftk < 1 or nshiftk > 210:
                self.errors.append(
                        f"nshiftk = {nshiftk} while it should be >= 1"
                        " and <= 210.")
            shiftk = await self._get_var("shiftk")
            if hasattr(shiftk, "value"):
                shiftk = shiftk.value
            if is_vector(shiftk):
                shiftk = [list(shiftk)]
            if len(shiftk) != nshiftk:
                self.errors.append(
                        f"nshiftk = {nshiftk} but there is"
                        f" {len(shiftk)} shifts given: {shiftk}.")
            if "ngkpt" in self.input_variables and (
                    "kptrlatt" in self.input_variables):
                self.errors.append("Cannot use both 'ngkpt' and 'kptrlatt'.")
                return
            # check that if nkpt is defined, it matches the number of kpts
            ngkpt = await self._get_var("ngkpt")
            kptrlatt = await self._get_var("kptrlatt")
            if await self._get_var("nkpt") == 1:
                if "ngkpt" in self.input_variables:
                    # ngkpt has been defined in input file
                    if ngkpt != [1, 1, 1]:
                        self.errors.append("nkpt = 1 but ngkpt != [1, 1, 1]")
            if "kptrlatt" in self.input_variables:
                if not is_2d_arr(kptrlatt.value, size=(3, 3)):
                    self.errors.append(
                            f"kptrlatt must be a 3x3 array but got {kptrlatt}."
                            )
        elif kptopt < 0:
            # check kptbounds is well defined
            await self._check_kptbounds(kptopt)

    async def _check_kptbounds(self, kptopt):
        kptbounds = await self._get_var("kptbounds")
        ndivk = await self._get_var("ndivk")
        if kptbounds is None:
            self.errors.append("kptopt < 0. Need to define kptopt.")
            return
        if ndivk is None:
            self.errors.append("kptopt < 0. -> Need to define ndivk.")
        for ikpt, kpt in enumerate(kptbounds):
            # try except with raise to continue the outer loop when
            # an error is found
            try:
                for kcomponent in kpt:
                    if kcomponent > 1 or kcomponent < -1:
                        self.errors.append(
                                f"kpts #{ikpt} components must be between "
                                "-1 and 1: "
                                f"{kpt} in kptbounds is wrong.")
                        raise StopIteration
            except StopIteration:
                continue
        if hasattr(kptopt, "value"):
            kptopt = kptopt.value
        if hasattr(ndivk, "value"):
            ndivk = ndivk.value
        if not is_list_like(ndivk):
            self.errors.append(
                    f"ndivk={ndivk} must be a list of len={abs(kptopt)}")
            return
        if len(ndivk) != abs(kptopt):
            self.errors.append(
                    f"len of ndivk must match abs(kptopt) = {abs(kptopt)}")

    async def _check_nbands(self):
        # there must be enough bands to contain electrons.
        if "nband" not in self.input_variables:
            occopt = self.input_variables.get("occopt", 1)
            if occopt not in (0, 2):
                # nband can be ommitted
                # https://docs.abinit.org/variables/basic/#nband
                self._logger.debug(
                        f"Bypass nband check because it is not set"
                        f" and not mandatory given occopt={occopt.value}.")
                return
            # else, nband must be set
            self.errors.append(
                    f"nband must be set because occopt={occopt.value}.")
            return
        nband = self.input_variables["nband"]
        # result will depend if there is SO coupling
        nspinor = self.input_variables.get("nspinor", 1)
        # need to compute total number of electrons in the system
        if "ntypat" not in self.input_variables:
            self.errors.append("Need to set 'ntypat'.")
            return
        ntypat = self.input_variables["ntypat"]
        nval_per_pseudos = [pseudo.zion for pseudo in await self.pseudo_files]
        if len(nval_per_pseudos) != ntypat:
            raise ValueError(
                    f"Number of pseudos ({len(nval_per_pseudos)}) does not "
                    f"match number of types of atoms ({ntypat}).")
        nelec = 0
        if "typat" not in self.input_variables:
            self.errors.append("Need to set 'typat'.")
            return
        typat = self.input_variables["typat"]
        for at in typat:
            # fortran indices start at 1
            nelec += nval_per_pseudos[at - 1]
        # nelec is the total number of electrons in the system.
        # the number of bands needed must half this number at least
        # so add an error if it is less.
        # multiply nelec by nspinor if so coupling
        nsppol = await self._get_var("nsppol")
        if nsppol != 1:
            # we have spin polarization -> nband is an array of bands per spin
            # each band
            for band in nband:
                if band < nelec / 2:
                    self.errors.append(
                            f"Number of bands {nband} insufficient for"
                            f"number of electrons in the"
                            f" system {nelec} for each spin.")
            return
        if nband < nspinor * nelec / 2:
            if nspinor == 1:
                self.errors.append(f"Number of bands {nband} insufficient for"
                                   f"number of electrons in the"
                                   f" system {nelec}.")
            else:
                self.errors.append(f"Number of bands {nband} insufficient for"
                                   f"number of electrons in the"
                                   f" system {nelec} with SO coupling.")

    async def _check_occupations(self):
        # check occupations, if defined, are well defined
        occopt = await self._get_var("occopt")
        if occopt == 0:
            # occupations should be a single list with length = number of bands
            occ = await self._get_var("occ")
            nband = await self._get_var("nband")
            if is_2d_arr(occ):
                if len(occ) != 1:
                    self.errors.append("occupations should be a single list.")
                    return
                occ = occ[0]
            if len(occ) != nband:
                self.errors.append(
                        "Length of occ should match number of bands.")
        if occopt in (0, 2):
            iscf = await self._get_var("iscf")
            if iscf < 0 and iscf != -3:
                self.errors.append(
                        "occ is defined but iscf < 0 and != -3. for iscf < 0, "
                        "it needs to be set to -3 in order to read the occ!")

    async def _check_paw(self):
        # check if all pseudos are paw ones
        # all should be paw or all should be non-paw
        is_paw = [pseudo.is_paw for pseudo in await self.pseudo_files]
        if any(is_paw) and not all(is_paw):
            # at least one of the pseudo is paw but not all of them
            self.errors.append(
                    "At least one of the pseudos is PAW but not all of them.")
            return
        if not all(is_paw):
            # not a paw calculation
            return
        # paw calculations, check variables have been set
        if "pawecutdg" not in self.input_variables:
            self.errors.append(
                    "Pseudos are PAW: 'pawecutdg' must be defined.")
            return
        pawecutdg = self.input_variables["pawecutdg"].value
        # pawecutdg must be > ecut
        if "ecut" not in self.input_variables:
            self.errors.append("'ecut' should be present in input file.")
            return
        ecut = self.input_variables["ecut"].value
        if pawecutdg < ecut:
            self.errors.append(
                f"'pawecutdg'='{pawecutdg}' should be > than 'ecut'='{ecut}'.")

    async def _check_printing(self):
        if self.input_variables.get("prtprocar", 0) > 0:
            if self.input_variables.get("prtdos", 0) != 3:
                self.errors.append(
                        "'prtprocar' != 0 => 'prtdos' must be set to 3.")
        if await self._get_var("prtdosm") == 2 and (
                await self._get_var("pawprtdos") != 2):
            self.errors.append(
                    "'prtdosm=2' and 'pawprtdos/=2' are incompatible.")

    def _check_relax(self):
        ionmov = self.input_variables.get("ionmov", 0)
        if ionmov == 0:
            return
        optcell = self.input_variables.get("optcell", 0)
        if optcell > 0:
            if "ecutsm" not in self.input_variables:
                self.errors.append("'optcell > 0 => need to set 'ecutsm'.")

    async def _check_znucl(self):
        # check that znucl given in input matches znucl in pseudo files
        if "znucl" not in self.input_variables:
            self.errors.append("znucl must be defined.")
            return
        znucl = self.input_variables["znucl"].value
        pseudos = await self.pseudo_files
        if isinstance(znucl, int) or isinstance(znucl, float):
            znucl = (znucl, )
        znucl_per_pseudos = [pseudo.zatom for pseudo in pseudos]
        if len(znucl) != len(znucl_per_pseudos):
            self.errors.append(f"Number of pseudos ({len(znucl_per_pseudos)})"
                               f" does not match znucl ({znucl}).")
            return
        for i, (in_z, pseudo_z) in enumerate(zip(znucl, znucl_per_pseudos)):
            if in_z != pseudo_z:
                self.errors.append(f"Pseudo #{i + 1} with Z={pseudo_z} does"
                                   f" not match corresponding"
                                   f" znucl={repr(in_z)}.")

    def _check_nscf_ok(self):
        iscf = self.input_variables.get("iscf", 0)
        if iscf < 0 and iscf != -3:
            # iscf < 0 and iscf != -3 => nscf calculation
            # check that tolwfr > 0
            tolwfr = self.input_variables.get("tolwfr", 0.0)
            if tolwfr <= 0.0:
                self.errors.append("for iscf < 0 and != -3, tolwfr"
                                   "  must be > 0.")

    def _only_one_in(self, lst1, lst2):
        # check that there is exactly one item of list 1 in list 2
        presents = []
        result = True
        for item in lst1:
            if item in lst2:
                if len(presents):
                    result = False
                presents.append(item)
        if not len(presents):
            # no item is present in list2
            return False, presents
        return result, presents

    def _at_least_one_in(self, lst1, lst2):
        # check that at least one item of list 1 is in list 2
        for item in lst1:
            if item in lst2:
                return True
        return False

    async def _check_nspinor(self):
        # check spinors vs pseudos
        nspinor = self.input_variables.get("nspinor", 1).value
        if nspinor == 1:
            # all pseudos should support this mode a priori?
            return
        # nspinor is 2, all pseudos should support SOC
        for pseudo in await self.pseudo_files:
            if not pseudo.has_so:
                self.errors.append(
                        f"pseudo '{os.path.basename(pseudo.path)}' does not "
                        "support SOC while 'nspinor' is set to 2.")

    async def _check_vdw(self):
        # vdw functionals are only available for certain types of xc.
        ixc = await self._get_var("ixc")
        vdw_xc = await self._get_var("vdw_xc")
        if vdw_xc == 0:
            # no vdw correction
            return
        if vdw_xc in (6, 7):
            # DFT-D correction os only available for certain types of xc
            # the ixc availables are enumerated in abinit src files
            if ixc not in (
                    11, -101130, -130101, 18, -106131, -131106, 19, -106132,
                    -132106, -202231, -231202, 14, -102130, -130102, -170,
                    41, -406):
                self.errors.append(
                        f"vdw_xc={vdw_xc} is not implemented for xc functional"
                        f" ixc={ixc}.")

    async def _get_var(self, var):
        if self._default_var_getter is None:
            pseudo_files = await self.pseudo_files
            self._default_var_getter = DefaultAbinitVariableGetter(
                    self.input_variables, pseudo_files,
                    loglevel=self._loglevel)
        return self._default_var_getter[var]


class AbinitInputParalApprover(BaseInputParalApprover):
    """Input approver which analyses mpi parallelism as well.

    Class that compares the parallelization variables of Abinit with
    the mpi settings.
    """

    _loggername = "AbinitInputParalApprover"
    _input_approver_class = AbinitInputApprover
    _structure_class = AbinitInputStructure

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._default_var_getter = None

    async def validate(self):
        """Validate the input file."""
        await super().validate()
        self._compare_nprocs_with_paralvars()
        await self._check_gw_parallelization()

    async def _check_gw_parallelization(self):
        optdriver = self._get_var("optdriver")
        if optdriver != 4:
            return
        nband = self._get_var("nband")
        if self.ntasks is None:
            return
        if self.ntasks > nband:
            self.errors.append(
                    f"# mpi process ({self.ntasks}) > nband ({nband})"
                    f" which is not possible for GW-Sigma calculations.")

    def _compare_nprocs_with_paralvars(self):
        paralvars = {var: int(self.input_variables.get(var, 1).value)
                     for var in PARAL_VARS}
        product = np.prod(list(paralvars.values()))
        if product > self.total_ncpus:
            self.errors.append(
                    f"Parallelism scheme ask for too much proc:"
                    f" {paralvars} => {product} > {self.total_ncpus}")

    def _get_var(self, var):
        if self._default_var_getter is None:
            self._default_var_getter = DefaultAbinitVariableGetter(
                    self.input_variables,
                    loglevel=self._loglevel)
        return self._default_var_getter[var]
