from ..bases import BaseApprover
from ..exceptions import InputFileError


# TODO: finish me properly
class AbinitCut3DInputApprover(BaseApprover):
    """Input approver for a cut3D job.

    Note
    ----
    This approver is dummy and does not do anything actually.
    """

    _exception = InputFileError

    def validate(self):
        """Validate input file."""
        super().validate()
