import os

import aiofiles.os

from async_property import async_property

from ..bases import BaseUtility
from ..exceptions import DevError


class BaseHandler:
    """Base class for any handler object."""

    isfile = None
    _is_writable = False

    def __init__(self, *args, **kwargs):
        if self.isfile is None:
            raise DevError(
                    "Need to specify if handler represents a file or not.")
        self._path = None
        self.has_been_read = False
        self._basename = None

    async def __aenter__(self):
        exists = await self.exists
        if not self._is_writable and not exists:
            raise FileNotFoundError(
                    f"File/directory does not exist: '{self.path}'.")
        if not self.has_been_read and exists:
            try:
                await self.read()
            except Exception as err:
                self._logger.error(
                        f"An error occured while reading {self.path}")
                self._logger.exception(err)
                raise err
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        pass

    @property
    def basename(self):
        """Return the handler's basename."""
        if self._path is None:
            raise ValueError(
                    "Need to set path before getting the basename for: "
                    f"'{self.__class__}'.")
        if os.path.basename(self._path) != self._basename:
            raise LookupError()
        return self._basename

    @async_property
    async def exists(self):
        """Return True if the handler represents an existing file or dir."""
        try:
            return await aiofiles.os.path.exists(self.path)
        except (ValueError, TypeError):
            # path no set => return False
            return False

    @property
    def isdir(self):
        """Return True if handler represents a directory."""
        return not self.isfile

    @async_property
    async def islink(self):
        """Return True if the handler represents a symlink."""
        return await aiofiles.os.path.islink(self.path)

    @property
    def path(self):
        """Return the handler's path."""
        if self._path is not None:
            return self._path
        raise ValueError(f"Need to set 'path' for '{self.__class__}'.")

    def set_path(self, path):
        """Sets the path."""
        self._path = path
        self._basename = os.path.basename(self.path)

    def read(self):
        """Read the file or the directory."""
        self.has_been_read = True


class BaseStructuredObject(BaseUtility):
    """Base class for all objects that supports a 'Structure' object."""

    _structure_class = None
    _mpi_structure_class = None

    def __init__(self, *args, **kwargs):
        if self._structure_class is None:
            raise DevError(
                    "_structure_class attribute should be set in "
                    f"{self.__class__}.")
        self._structure = self._structure_class()
        self._mpi_structure = None
        if self._mpi_structure_class is not None:
            self._mpi_structure = self._mpi_structure_class()
        super().__init__(*args, **kwargs)

    def __getattr__(self, attr):
        struc = self.structure
        mpistruc = self.mpi_structure
        if attr in struc.all_attributes or attr in dir(struc):
            return getattr(self.structure, attr)
        if self.mpi_structure is not None:
            if attr in mpistruc.all_attributes or attr in dir(mpistruc):
                return getattr(self.mpi_structure, attr)
        raise AttributeError(f"'{self.__class__}' object has no attribute "
                             f"'{attr}'.")

    def __delattr__(self, attr):
        if attr in self.structure.all_attributes:
            delattr(self.structure, attr)
            return
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                delattr(self.mpi_structure, attr)
                return
        super().__delattr__(attr)

    def __setattr__(self, attr, value):
        # check this first to prevent infinite recursions
        if attr.endswith("structure") or attr in dir(self):
            super().__setattr__(attr, value)
            return
        if attr in self.structure.all_attributes or attr in dir(
                self.structure):
            setattr(self.structure, attr, value)
            # nest here because same attribute can be defined in both structurs
            if self.mpi_structure is not None:
                if attr in self.mpi_structure.all_attributes or attr in dir(
                        self.mpi_structure):
                    setattr(self.mpi_structure, attr, value)
            return
        # in case attr is only defined in mpi structure
        if self.mpi_structure is not None:
            if attr in self.mpi_structure.all_attributes:
                setattr(self.mpi_structure, attr, value)
                return
        super().__setattr__(attr, value)

    @property
    def structure(self):
        """Return the structure object associated with the file."""
        return self._structure

    @structure.setter
    def structure(self, structure):
        if not isinstance(structure, self._structure_class):
            raise TypeError(f"Expected '{self._structure_class}' instance but "
                            f"got '{structure}'")
        self._structure = structure

    @property
    def mpi_structure(self):
        """Return the mpi structure object associated with the file."""
        return self._mpi_structure

    @mpi_structure.setter
    def mpi_structure(self, structure):
        if not isinstance(structure, self._mpi_structure_class):
            raise TypeError(f"Expected '{self._mpi_structure_class}' "
                            f"instance but got: '{structure}'")
        self._mpi_structure = structure
