from ..bases import BaseWriter
from ...file_structures import QEKpointsInputStructure


class QEKpointsInputWriter(BaseWriter):
    """Writer class for the 'kpoints.x' utility of Quantum Espresso."""

    _files_suffix = ".in"
    _loggername = "QEKpointsInputWriter"
    _structure_class = QEKpointsInputStructure

    def _get_lines(self):
        """Returns the lines to write."""
        lines = []
        lines.append(f"{self.ibrav}\n")
        lines.append(f"{self.filout}\n")
        if self.ibrav == 4:
            lines.append(f"{self.celldm3}\n")
        lines.append(' '.join([str(m) for m in self.mesh]) + "\n")
        lines.append(' '.join([str(m) for m in self.shift]) + "\n")
        lines.append(f"{self.ibz_only}\n")
        return lines
