from .bases import BaseQEInputWriter
from ...file_structures import QEMatdynInputStructure


class QEMatdynInputWriter(BaseQEInputWriter):
    """Class that can write Quantum Espresso matdyn.x input files."""

    _loggername = "QEMatdynInputWriter"
    _structure_class = QEMatdynInputStructure

    def _get_lines(self):
        """Return the lines to write."""
        lines = []
        self._append_var_block(lines, "input")
        self._append_others(lines)
        return lines
