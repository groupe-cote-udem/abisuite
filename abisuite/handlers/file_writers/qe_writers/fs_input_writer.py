from .bases import BaseQEInputWriter
from ...file_structures import QEFSInputStructure


class QEFSInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso fs.x input file."""

    _loggername = "QEFSInputWriter"
    _structure_class = QEFSInputStructure

    def _get_lines(self):
        """Return the lines to write."""
        lines = []
        self._append_var_block(lines, "fermi")
        return lines
