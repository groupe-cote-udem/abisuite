from .bases import BaseQEInputWriter
from ...file_structures import QEEpsilonInputStructure


class QEEpsilonInputWriter(BaseQEInputWriter):
    """Class that can write a Quantum Espresso epsilon.x input file."""

    _loggername = "QEEpsilonInputWriter"
    _structure_class = QEEpsilonInputStructure

    def _get_lines(self):
        """Return the lines to write."""
        lines = []
        self._append_var_block(lines, "inputpp")
        self._append_var_block(lines, "energy_grid")
        return lines
