import os

import aiofiles

from .bases import BaseStreamLineWriter
from ..file_structures import SymLinkStructure
from ...linux_tools import mkdir


class SymLinkWriter(BaseStreamLineWriter):
    """Writer class for a symlink file."""

    _loggername = "SymLinkWriter"
    _structure_class = SymLinkStructure

    def _get_lines(self):
        pass

    async def _do_write(self):
        dirname = os.path.dirname(self.path)
        if not await aiofiles.os.path.isdir(dirname):
            await mkdir(dirname)
        await aiofiles.os.symlink(
                self.source, self.path,
                target_is_directory=self.source_is_directory,
                )
