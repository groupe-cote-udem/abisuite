import abc
import os

import aiofiles

from ..bases import BaseStructuredObject
from ...routines import full_abspath


# TODO: rename these classes (FG: 2021/10/28)
class BaseStreamLineWriter(BaseStructuredObject, abc.ABC):
    """Base class for writers without enforcing approvers.

    Or with custom
    writing properties / methods.
    """

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._workdir = None
        self._filename = None
        self._path = None
        self._lines = None

    @property
    def structure(self):
        """The structure object."""
        return super().structure

    @structure.setter
    def structure(self, structure):
        BaseStructuredObject.structure.fset(self, structure)
        # reset lines if changing structure
        self._lines = None

    @property
    def lines(self):
        """Return the lines to write."""
        if self._lines is not None:
            return self._lines
        self._lines = self._get_lines()
        return self._lines

    @abc.abstractmethod
    def _get_lines(self):
        # need to compute lines
        pass  # pragma: no cover

    @property
    def path(self):
        """Return the file path to write."""
        if self._path is not None:
            return self._path
        raise ValueError("Need to setup file path attr.")

    @path.setter
    def path(self, path):
        self._path = path
        self._logger.debug(f"Setting file path to {self.path}")

    # TODO: delete this property
    @property
    def workdir(self):
        """Return the dirname of the file to write."""
        if self._workdir is not None:
            return self._workdir
        self._workdir = full_abspath(os.path.dirname(self.path))
        self._logger.debug(f"Assigning workdir to {self.workdir}")
        return self.workdir

    async def write(
            self, overwrite: bool = False,
            recompute_lines: bool = False,
            ) -> None:
        """Write the file.

        Parameters
        ----------
        overwrite: bool, optional
                   If False, an error is raised when attempting to write a
                   file that already exists under the same name.
        recompute_lines: bool, optional
            If file was already written but we overwrite, we will use
            the same lines. Set to True to recompute the lines.
        """
        if recompute_lines:
            self._lines = None
        if await aiofiles.os.path.exists(self.path) or (
                await aiofiles.os.path.islink(self.path)):
            # second condition exists cause broken link will return False
            # on os.path.exists where as the broken symlink actually exists
            if not overwrite:
                # raise the error
                raise FileExistsError(f"{self.path} already exists.")
            # compute lines to write before erasing in case
            # an error is thrown before writing the file.
            self.lines
            # else, erase file and rewrite
            await aiofiles.os.remove(self.path)
        self._logger.debug(f"Writing file at: {self.path}")
        await self._do_write()

    @abc.abstractmethod
    async def _do_write(self, *args, **kwargs):  # pragma: no cover
        pass


class BaseWriter(BaseStreamLineWriter, abc.ABC):
    """Base class for writers that have a custom syntax.

    For these, the lines are computed and written manually.
    """

    async def _do_write(self):
        # ready to write at this point
        async with aiofiles.open(self.path, "w") as f:
            for line in self.lines:
                await f.write(line)


# TODO: get rid of this class
class BaseInputWriter(BaseWriter):
    """Base class for input file writers."""

    pass
