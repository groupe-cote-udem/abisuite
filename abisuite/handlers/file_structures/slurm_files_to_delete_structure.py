from .bases import BaseStructure


class SlurmFilesToDeleteStructure(BaseStructure):
    """Structure class for a slurm file listing the files to be deleted."""

    all_attributes = (
            "files_to_delete",
            )
