from ..bases import BaseSCFLogStructure


class AbinitLogStructure(BaseSCFLogStructure):
    """Structure class for an abinit log file.

    Looks very similar to the abinit output structure.
    """

    all_attributes = (
            "dtsets", "errors",
            "input_variables", "irreducible_perturbations",
            "output_variables",
            "walltime", "occ",
            )
    dict_attributes = ("input_variables", "output_variables", )
    list_attributes = ("dtsets", "errors", )
    optional_attributes = (
            "irreducible_perturbations", "occ", "walltime", )

    @property
    def etot(self):
        """Return the total energy stored in the output variables."""
        return self.output_variables["etotal"]

    @etot.setter
    def etot(self, etot):
        self._etot = etot
