from ..bases import BaseStructure


class AbinitOpticLogStructure(BaseStructure):
    """Structure class for an optic log file."""

    all_attributes = tuple([])

    @property
    def walltime(self):  # noqa: D102
        # walltime is not printed out in the optic log file.
        return None
