from ..bases import BaseStructure


class AbinitGSRStructure(BaseStructure):
    """Structure for a '_GSR' file created by Abinit."""

    all_attributes = ("fermi_energy", "occupations", )
    optional_attributes = ("fermi_energy", "occupations", )
