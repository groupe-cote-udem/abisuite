import os

from ..bases import BaseInputStructure
from ....variables import ALL_ABINIT_VARIABLES


class AbinitInputStructure(BaseInputStructure):
    """Structure for an abinit input file."""

    _variables_db = ALL_ABINIT_VARIABLES

    def _get_pseudos_from_input_variables(self):
        if "pp_dirpath" in self.input_variables:
            dirname = self.input_variables["pp_dirpath"].value
        else:
            dirname = ""
        if "pseudos" not in self.input_variables:
            raise ValueError("'pseudos' not defined in input variables.")
        pseudos = []
        for pseudo in self.input_variables["pseudos"]:
            pseudos.append(os.path.join(dirname, pseudo))
        return pseudos
