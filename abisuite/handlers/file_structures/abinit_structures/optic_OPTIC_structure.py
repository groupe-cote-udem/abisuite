from ..bases import BaseStructure


class AbinitOpticOPTICStructure(BaseStructure):
    """Structure class for an abinit optic _OPTIC.nc file."""

    all_attributes = (
            "linopt_matrix_elements", "linopt_renorm_eigs",
            "linopt_occupations", "linopt_epsilon",
            "linopt_components", "linopt_wkpts",
            )
