from ..bases import BaseStructure


class AbinitBSEMDFStructure(BaseStructure):
    """Structure class for a Bethe-Salpeter Equation MDF file.

    This structure supports the EXC, the RPA_NLF and GW_NLF files.

    MDF stands for Macroscopic Dielectric Function.
    """

    all_attributes = (
            "broadening", "dos", "idos", "epsilon", "frequencies",
            "bs_nstates", "haydock_tolerance",
            "loband", "nbands",
            "nkbz", "nkibz", "npweps", "npwwfn",
            "qpoints", "scissor_shift",
            )
    optional_attributes = (
            "bs_nstates", "haydock_tolerance",
            "dos", "idos", "scissor_shift",
            )
