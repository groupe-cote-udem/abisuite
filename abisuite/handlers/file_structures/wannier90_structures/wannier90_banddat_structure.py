from ..bases import BaseStructure


class Wannier90BandDatStructure(BaseStructure):
    """Structure class for a Wannier90 seedname_band.dat file.

    This file is produced by wannier90.x when bands_plot=True.
    """

    all_attributes = ("bands", "nkpt", )
