from ..bases import BaseStructure


class QEPHDyn0Structure(BaseStructure):
    """Structure class for a .dyn0 file that was produced in a ph.x calc."""

    all_attributes = ("qgrid", "nqpt", "qpts", )
