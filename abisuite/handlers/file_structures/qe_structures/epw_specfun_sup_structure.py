from ..bases import BaseStructure


class QEEPWSpecfunSupStructure(BaseStructure):
    """Structure class for an EPW electron self energy file."""

    all_attributes = (
            "energies", "frequencies", "frequencies",
            "self_energies", "nkpts", "nbands",
            )
