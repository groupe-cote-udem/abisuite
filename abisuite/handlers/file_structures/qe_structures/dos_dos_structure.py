from ..bases import BaseStructure


class QEDOSDOSStructure(BaseStructure):
    """Structure class for a dos file produced by the dos.x script."""

    all_attributes = (
            "energies", "dos", "integrated_dos", "fermi_energy",
            )
