from ..bases import BaseStructure


class QEEPWInvTauStructure(BaseStructure):
    """Structure class for a inv_tau.fmt epw file."""

    all_attributes = (
            "relaxation_times", "energies", "nbands", "ntemperatures",
            "nkpts", "nmodes", "frequencies", "nfrequencies",
            )
    optional_attributes = (
            "ntemperatures", "nmodes", "frequencies", "nfrequencies",
            )
