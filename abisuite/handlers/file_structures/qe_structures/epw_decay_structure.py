from ..bases import BaseStructure


class QEEPWDecayStructure(BaseStructure):
    """Structure class for a decay.* epw file."""

    all_attributes = ("decay", "wannier_centers")
