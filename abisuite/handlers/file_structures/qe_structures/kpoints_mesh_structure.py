from ..bases import BaseStructure


class QEKpointsMeshStructure(BaseStructure):
    """Structure class for a mesh file produced by the kpoints.x script."""

    all_attributes = ("kpoints", "nkpoints", "weights")
