from ..bases import BaseStructure


class QEEPWSpecfunStructure(BaseStructure):
    """Structure class for an EPW spectral function file."""

    all_attributes = ("energies", "nkpts", "spectral_function")
