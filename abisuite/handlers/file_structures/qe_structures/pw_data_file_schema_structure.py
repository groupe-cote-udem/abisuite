from ..bases import BaseStructure


class QEPWDataFileSchemaStructure(BaseStructure):
    """Structure class for a data_file_schema.xml file produced by pw.x."""

    all_attributes = ("k_points", "eigenvalues", "weights", "occupations")
