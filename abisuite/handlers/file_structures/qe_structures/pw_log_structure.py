from .bases import BaseQELogStructure
from ..bases import BaseSCFLogStructure


class QEPWLogStructure(BaseQELogStructure, BaseSCFLogStructure):
    """Structure for a pw.x log file."""

    all_attributes = (
            "atomic_positions", "crystal_axes", "eigenvalues", "etot",
            "fermi_energy", "final_atomic_positions",
            "final_geometry",
            "input_variables", "k_points", "lattice_parameters",
            "parallelization", "pressure", "nelectrons",
            "stress_tensor", "one_electron_energy",
            "hartree_energy", "xc_energy", "ewald_energy",
            "timing", "total_energy", "smearing_contribution",
            "internal_energy",
            )
    optional_attributes = (
            "eigenvalues", "fermi_energy", "final_atomic_positions",
            "final_geometry",  "one_electron_energy",
            "hartree_energy", "xc_energy", "ewald_energy",
            "parallelization", "pressure", "stress_tensor",
            "timing", "total_energy", "smearing_contribution",
            "internal_energy",
            )

    @property
    def etot(self):
        """Return the total energy."""
        return self.total_energy

    @etot.setter
    def etot(self, etot):
        self._etot = etot
