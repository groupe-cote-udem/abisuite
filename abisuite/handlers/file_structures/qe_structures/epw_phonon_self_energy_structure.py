from ..bases import BaseStructure


class QEEPWPhononSelfEnergyStructure(BaseStructure):
    """Structure class for a phonon self energy file.

    This file is produced by the
    epw.x executable from Quantum Espresso.
    """

    all_attributes = ("energies", "eigenvalues", "self_energy")
