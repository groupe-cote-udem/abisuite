from .bases import BaseQELogStructure


class QEEPWLogStructure(BaseQELogStructure):
    """Structure for a epw.x log file from Quantum Espresso."""

    all_attributes = (
            "adaptative_smearings",
            "conductivities", "gkk", "serta_conductivities", "timing",
            )
    optional_attributes = (
            "adaptative_smearings",
            "conductivities",
            "gkk", "serta_conductivities", "timing",
            )
