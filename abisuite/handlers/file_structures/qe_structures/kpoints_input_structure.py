from typing import Any

from ..bases import BaseInputStructureNoInputVariables


class QEKpointsInputStructure(BaseInputStructureNoInputVariables):
    """Structure class for the kpoints.x utility of Quantum Espresso."""

    all_attributes = (
            "celldm3", "ibrav", "filout", "mesh", "shift", "ibz_only",
            )
    optional_attributes = (
            "celldm3",
            )
    list_attributes = ("mesh", "shift")

    @property
    def input_variables(self) -> dict[str, Any]:
        """The dict of input variables."""
        class DummyVar:
            def __init__(self, name, value):
                self.name = name
                self.value = value

            def __eq__(self, other):
                if self.name == "ibz_only":
                    # compare string values as it has many possibilities
                    return str(self.value) == str(other.value)
                return self.value == other.value
        ivars = {}
        for attr in self.all_attributes:
            if attr in self.optional_attributes and not hasattr(self, attr):
                continue
            ivars[attr] = DummyVar(attr, getattr(self, attr))
        return ivars
