from .bases import BaseQELogStructure


class QEKpointsLogStructure(BaseQELogStructure):
    """Structure class for a kpoints.x log file."""

    all_attributes = tuple()
