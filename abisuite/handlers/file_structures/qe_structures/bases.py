from ..bases import BaseLogStructure, BaseStructure


class BaseQELogStructure(BaseLogStructure):
    """Base structure class for Quantum Espresso log files."""


class BaseQEBandFreqStructure(BaseStructure):
    """Base structure for all files with dispersions from QE."""

    all_attributes = ("coordinates", "eigenvalues", "nbands", "npts")
