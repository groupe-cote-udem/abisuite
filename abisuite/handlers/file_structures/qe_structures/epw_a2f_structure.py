from ..bases import BaseStructure


class QEEPWa2FStructure(BaseStructure):
    """Structure class for a a2F file produced by the epw.x."""

    all_attributes = ("frequencies", "a2F")
