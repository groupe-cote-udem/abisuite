import os

from ..bases import BaseCalculationFileHandler


class BaseWannier90FileHandler(BaseCalculationFileHandler):
    """Base file handler class for a wannier90 file."""

    @classmethod
    async def _get_file_from_meta_data_file(cls, meta):
        # add a special case for EPW calculations which can produce (and
        # usually will) wannier90 files.
        async with meta:
            if meta.calctype == "wannier90":
                return await super()._get_file_from_meta_data_file(meta)
            elif meta.calctype == "qe_epw":
                return await cls._get_file_from_meta_data_file_epw(meta)
            else:
                raise ValueError(f"Unsupported calctype: '{meta.calctype}'.")

    @classmethod
    async def _get_file_from_meta_data_file_epw(cls, meta):
        # get the file produced by a EPW calculation
        # By default it is located in the rundir/jobname + expected ending
        async with meta:
            return os.path.join(
                    meta.rundir, meta.jobname +
                    cls._parser_class._expected_ending)
