from async_property import async_property

from .bases import BaseApprovableFileHandler, PathConvertibleFileHandler
from ..file_approvers import SymLinkApprover
from ..file_parsers import SymLinkParser
from ..file_structures import SymLinkStructure
from ..file_writers import SymLinkWriter


class SymLinkFile(BaseApprovableFileHandler, PathConvertibleFileHandler):
    """File handler for all symlink files."""

    _approver_class = SymLinkApprover
    _loggername = "SymLinkFile"
    _parser_class = SymLinkParser
    _structure_class = SymLinkStructure
    _writer_class = SymLinkWriter

    @async_property
    async def is_broken_link(self):
        """Return True if the symlink is broken."""
        if not await self.exists:
            return True
        try:
            await self.read()
        except ValueError:
            # broken link
            return True
        return False

    async def move(self, path, overwrite=False):
        """Move the symlink."""
        # need to override this since its a particular type of file
        path = await self._prepare_path_for_file_transfer(
                path, overwrite=overwrite)
        # delete file and redo symlink pointing at same source
        await self.delete()
        await self.set_path(path)
        await self.write(overwrite=overwrite)
