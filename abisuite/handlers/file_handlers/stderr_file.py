from .bases import BaseCalculationFileHandler
from ..file_parsers import StderrParser
from ..file_structures import StderrStructure


class StderrFile(BaseCalculationFileHandler):
    """File handler class for a stderr file."""

    _loggername = "StderrFile"
    _parser_class = StderrParser
    _structure_class = StderrStructure
