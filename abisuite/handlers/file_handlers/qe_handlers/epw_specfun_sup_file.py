from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWSpecfunSupParser
from ...file_structures import QEEPWSpecfunSupStructure


class QEEPWSpecfunSupFile(BaseCalculationFileHandler):
    """Class that represents an electron self energy file by EPW."""

    _loggername = "QEEPWSpecfunSupFile"
    _parser_class = QEEPWSpecfunSupParser
    _structure_class = QEEPWSpecfunSupStructure
