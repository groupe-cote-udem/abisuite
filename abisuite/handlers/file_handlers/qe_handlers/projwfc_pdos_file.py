import logging

from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEProjwfcPDOSParser
from ...file_structures import QEProjwfcPDOSStructure


class QEProjwfcPDOSFile(BaseCalculationFileHandler):
    """Class that represents a pdos function file produced by projwfc.x."""

    _loggername = "QEProjwfcPDOSFile"
    _parser_class = QEProjwfcPDOSParser
    _structure_class = QEProjwfcPDOSStructure

    # need to override from_meta_data since there are many files and we want
    # to be able to choose the ones we want at init time. this is already
    # done in the parser
    @classmethod
    async def from_meta_data_file(cls, metadatafile, *args, **kwargs):
        """Create a QEProjwfcPDOSFile object from a MetaDataFile object.

        Parameters
        ----------
        metadatafile: MetaDataFile object

        Returns
        -------
        dict: The dict whose keys are the wfc projections and values are
              are the corresponding objects.
        """
        paths = await cls._parser_class._filepath_from_meta(
                        metadatafile, *args, **kwargs)
        return {key: await cls.from_file(
                            path, loglevel=kwargs.get("loglevel", logging.INFO)
                            )
                for key, path in paths.items()}
