from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEKpointsMeshParser
from ...file_structures import QEKpointsMeshStructure


class QEKpointsMeshFile(BaseCalculationFileHandler):
    """File handler class for a mesh file by kpoints.x script."""

    _loggername = "QEKpointsMeshFile"
    _parser_class = QEKpointsMeshParser
    _structure_class = QEKpointsMeshStructure
