from ..bases import BaseLogFileHandler
from ...file_parsers import QEKpointsLogParser
from ...file_structures import QEKpointsLogStructure


class QEKpointsLogFile(BaseLogFileHandler):
    """Log file handler for a kpoints.x log file."""

    _loggername = "QEKpointsLogFile"
    _parser_class = QEKpointsLogParser
    _structure_class = QEKpointsLogStructure
