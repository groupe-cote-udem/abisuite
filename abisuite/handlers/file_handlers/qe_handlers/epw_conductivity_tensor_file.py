from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWConductivityTensorParser
from ...file_structures import QEEPWConductivityTensorStructure


class QEEPWConductivityTensorFile(BaseCalculationFileHandler):
    """Class that represents a conductivity tensor file produced by EPW."""

    _loggername = "QEEPWConductivityTensorFile"
    _parser_class = QEEPWConductivityTensorParser
    _structure_class = QEEPWConductivityTensorStructure
