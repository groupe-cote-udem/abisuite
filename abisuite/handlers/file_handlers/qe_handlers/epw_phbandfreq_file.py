from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWPHBandFreqParser
from ...file_structures import QEEPWPHBandFreqStructure


class QEEPWPHBandFreqFile(BaseCalculationFileHandler):
    """File handler class for a 'phband.freq' file produced by epw.x script."""

    _loggername = "QEEPWPHBandFreqFile"
    _parser_class = QEEPWPHBandFreqParser
    _structure_class = QEEPWPHBandFreqStructure
