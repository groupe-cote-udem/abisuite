from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEMatdynEigParser
from ...file_structures import QEMatdynEigStructure


class QEMatdynEigFile(BaseCalculationFileHandler):
    """File handler class for a '.eig' file produced by the matdyn.x script."""

    _loggername = "QEMatdynEigFile"
    _parser_class = QEMatdynEigParser
    _structure_class = QEMatdynEigStructure
