from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWDecayParser
from ...file_structures import QEEPWDecayStructure


class QEEPWDecayFile(BaseCalculationFileHandler):
    """File handler class for a decay.* epw file."""

    _loggername = "QEEPWDecayFile"
    _parser_class = QEEPWDecayParser
    _structure_class = QEEPWDecayStructure
