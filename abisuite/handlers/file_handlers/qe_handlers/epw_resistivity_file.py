from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWResistivityParser
from ...file_structures import QEEPWResistivityStructure


class QEEPWResistivityFile(BaseCalculationFileHandler):
    """Class that represents a resistivity function file."""

    _loggername = "QEEPWResistivityFile"
    _parser_class = QEEPWResistivityParser
    _structure_class = QEEPWResistivityStructure
