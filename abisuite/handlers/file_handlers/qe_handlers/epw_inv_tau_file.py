from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWInvTauParser
from ...file_structures import QEEPWInvTauStructure


class QEEPWInvTauFile(BaseCalculationFileHandler):
    """Class that represents a inv_tau.fmt file."""

    _loggername = "QEEPWInvTauFile"
    _parser_class = QEEPWInvTauParser
    _structure_class = QEEPWInvTauStructure
