from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEPHDyn0Parser
from ...file_structures import QEPHDyn0Structure


class QEPHDyn0File(BaseCalculationFileHandler):
    """Class that represents a .dyn0 file produced by a ph.x calculation."""

    _loggername = "QEPHDyn0File"
    _parser_class = QEPHDyn0Parser
    _structure_class = QEPHDyn0Structure
