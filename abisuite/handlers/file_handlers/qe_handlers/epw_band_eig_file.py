from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWBandEigParser
from ...file_structures import QEEPWBandEigStructure


class QEEPWBandEigFile(BaseCalculationFileHandler):
    """File handler class for a 'band.eig' file produced by EPW."""

    _loggername = "QEEPWBandEigFile"
    _parser_class = QEEPWBandEigParser
    _structure_class = QEEPWBandEigStructure
