from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWPhononSelfEnergyParser
from ...file_structures import QEEPWPhononSelfEnergyStructure


class QEEPWPhononSelfEnergyFile(BaseCalculationFileHandler):
    """Class that represents a phonon self energy file from EPW."""

    _loggername = "QEEPWPhononSelfEnergyFile"
    _parser_class = QEEPWPhononSelfEnergyParser
    _structure_class = QEEPWPhononSelfEnergyStructure
