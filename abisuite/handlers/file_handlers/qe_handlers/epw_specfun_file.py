from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWSpecfunParser
from ...file_structures import QEEPWSpecfunStructure


class QEEPWSpecfunFile(BaseCalculationFileHandler):
    """Class that represents an electron spectral function file by EPW."""

    _loggername = "QEEPWSpecfunFile"
    _parser_class = QEEPWSpecfunParser
    _structure_class = QEEPWSpecfunStructure
