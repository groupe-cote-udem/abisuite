from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWSpecFunPhonParser
from ...file_structures import QEEPWSpecFunPhonStructure


class QEEPWSpecFunPhonFile(BaseCalculationFileHandler):
    """Class that represents a phonon spectral function file by EPW."""

    _loggername = "QEEPWSpecFunPhonFile"
    _parser_class = QEEPWSpecFunPhonParser
    _structure_class = QEEPWSpecFunPhonStructure
