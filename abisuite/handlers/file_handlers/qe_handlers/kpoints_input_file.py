from ..bases import BaseInputFileHandlerNoInputVariables
from ...file_approvers import QEKpointsInputApprover
from ...file_parsers import QEKpointsInputParser
from ...file_structures import QEKpointsInputStructure
from ...file_writers import QEKpointsInputWriter


class QEKpointsInputFile(BaseInputFileHandlerNoInputVariables):
    """File handler class for the input file of the kpoints utility of QE."""

    _approver_class = QEKpointsInputApprover
    _loggername = "QEKpointsInputFile"
    _parser_class = QEKpointsInputParser
    _structure_class = QEKpointsInputStructure
    _writer_class = QEKpointsInputWriter
