from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEMatdynDOSParser
from ...file_structures import QEMatdynDOSStructure


class QEMatdynDOSFile(BaseCalculationFileHandler):
    """Class that represents a dos file produced by the matdyn.x script."""

    _loggername = "QEMatdynDOSFile"
    _parser_class = QEMatdynDOSParser
    _structure_class = QEMatdynDOSStructure
