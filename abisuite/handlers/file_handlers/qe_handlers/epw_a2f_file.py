from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEEPWa2FParser
from ...file_structures import QEEPWa2FStructure


class QEEPWa2FFile(BaseCalculationFileHandler):
    """Class that represents a a2F function file produced by epw.x."""

    _loggername = "QEEPWa2FFile"
    _parser_class = QEEPWa2FParser
    _structure_class = QEEPWa2FStructure
