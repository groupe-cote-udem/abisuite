from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEPWDataFileSchemaParser
from ...file_structures import QEPWDataFileSchemaStructure


class QEPWDataFileSchemaHandler(BaseCalculationFileHandler):
    """File handler class for a data-file-schema.xml file produced by pw.x."""

    _loggername = "QEPWDataFileSchemaHandler"
    _parser_class = QEPWDataFileSchemaParser
    _structure_class = QEPWDataFileSchemaStructure
