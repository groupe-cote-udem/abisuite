from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEDOSDOSParser
from ...file_structures import QEDOSDOSStructure


class QEDOSDOSFile(BaseCalculationFileHandler):
    """Class that represents a dos function file produced by dos.x."""

    _loggername = "QEDOSDOSFile"
    _parser_class = QEDOSDOSParser
    _structure_class = QEDOSDOSStructure
