from ..bases import BaseCalculationFileHandler
from ...file_parsers import QEMatdynFreqParser
from ...file_structures import QEMatdynFreqStructure


class QEMatdynFreqFile(BaseCalculationFileHandler):
    """File handler class for a '.freq' file produced by matdyn."""

    _loggername = "QEMatdynFreqFile"
    _parser_class = QEMatdynFreqParser
    _structure_class = QEMatdynFreqStructure
