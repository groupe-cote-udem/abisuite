from .bases import BaseFileHandler, BaseInputFileHandler
from ..file_approvers import GenericInputApprover
from ..file_parsers import GenericInputParser, GenericParser
from ..file_structures import GenericInputStructure, GenericStructure
from ..file_writers import GenericInputWriter


class GenericFile(BaseFileHandler):
    """A file handler for a generic file.

    For when we're not interested
    in the content of the file. This class can be used for basic file
    manipulations.
    """

    _loggername = "GenericFile"
    _parser_class = GenericParser
    _structure_class = GenericStructure


# generic input file
class GenericInputFile(BaseInputFileHandler):
    """Generic file handler for input files."""

    _approver_class = GenericInputApprover
    _loggername = "GenericInputFile"
    _parser_class = GenericInputParser
    _structure_class = GenericInputStructure
    _writer_class = GenericInputWriter
