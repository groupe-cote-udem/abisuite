import os

import aiofiles.os

from .bases import BaseCalculationFileHandler
from ..file_parsers import PseudoParser
from ..file_structures import PseudoStructure
from ... import USER_CONFIG
from ...routines import full_abspath


async def get_true_pseudo_path(path):
    """Return the true pseudo path based on user config."""
    if await aiofiles.os.path.isfile(path):
        return full_abspath(path)
    # not directly a file
    if os.path.isabs(path):
        # absolute path => impossible to find a file
        raise FileNotFoundError(path)
    # try joining with the default pseudo dir
    default_ps_dir = USER_CONFIG.DEFAULTS.default_pseudos_dir
    if default_ps_dir is None:
        raise ValueError(
                "Cannot detect automatically pseudo path because "
                "'default_pseudo_dir' in config is set to None.")
    if not await aiofiles.os.path.isdir(default_ps_dir):
        raise ValueError(
                "Config value for 'default_pseudos_dir' is not a dir: "
                f"'{default_ps_dir}'.")
    with_default_ps_dir = os.path.join(default_ps_dir, path)
    if not await aiofiles.os.path.isfile(with_default_ps_dir):
        raise FileNotFoundError(path)
    return with_default_ps_dir


class PseudoFile(BaseCalculationFileHandler):
    """Class that represents a pseudopotential file."""

    # empty file suffix because it can be anything
    _loggername = "PseudoFile"
    _parser_class = PseudoParser
    _structure_class = PseudoStructure

    @classmethod
    async def from_file(cls, path, *args, **kwargs):
        """Initialize the pseudo file handler from a file that exists.

        Parameters
        ----------
        path : str
               The path to the file.
        """
        # override this method in order to automatically detect path
        return await super().from_file(
                await get_true_pseudo_path(path), *args, **kwargs)
