from .bases import BaseFileHandler
from ..file_parsers import SlurmFilesToDeleteParser
from ..file_structures import SlurmFilesToDeleteStructure


class SlurmFilesToDeleteFile(BaseFileHandler):
    """Handler class for the slurm file that lists the files to be deleted."""

    _loggername = "SlurmFilesToDeleteFile"
    _parser_class = SlurmFilesToDeleteParser
    _structure_class = SlurmFilesToDeleteStructure
