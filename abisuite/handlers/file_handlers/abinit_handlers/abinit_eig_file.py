from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitEIGParser
from ...file_structures import AbinitEIGStructure


class AbinitEIGFile(BaseCalculationFileHandler):
    """File Handler class for an abinit EIG file."""

    _loggername = "AbinitEIGFile"
    _parser_class = AbinitEIGParser
    _structure_class = AbinitEIGStructure
