from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitOpticLincompParser
from ...file_structures import AbinitOpticLincompStructure


class AbinitOpticLincompFile(BaseCalculationFileHandler):
    """File Handler class for an optic dielectric tensor file."""

    _loggername = "AbinitOpticLincompFile"
    _parser_class = AbinitOpticLincompParser
    _structure_class = AbinitOpticLincompStructure
