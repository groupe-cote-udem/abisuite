from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitOpticOPTICParser
from ...file_structures import AbinitOpticOPTICStructure


class AbinitOpticOPTICFile(BaseCalculationFileHandler):
    """File handler class for an _OPTIC.nc file."""

    _loggername = "AbinitOpticOPTICFile"
    _parser_class = AbinitOpticOPTICParser
    _structure_class = AbinitOpticOPTICStructure
