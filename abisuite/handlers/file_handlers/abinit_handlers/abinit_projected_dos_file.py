from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitProjectedDOSParser
from ...file_structures import AbinitProjectedDOSStructure


class AbinitProjectedDOSFile(BaseCalculationFileHandler):
    """File handler class for a DOS_AT file produced by the abinit software."""

    _loggername = "AbinitProjectedDOSFile"
    _parser_class = AbinitProjectedDOSParser
    _structure_class = AbinitProjectedDOSStructure
