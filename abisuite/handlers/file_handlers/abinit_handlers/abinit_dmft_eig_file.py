from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitDMFTEigParser
from ...file_structures import AbinitDMFTEigStructure


class AbinitDMFTEigFile(BaseCalculationFileHandler):
    """File Handler class for an abinit eig file."""

    _loggername = "AbinitDMFTEigFile"
    _parser_class = AbinitDMFTEigParser
    _structure_class = AbinitDMFTEigStructure
