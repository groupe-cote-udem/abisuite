from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitOutputParser
from ...file_structures import AbinitOutputStructure


class AbinitOutputFile(BaseCalculationFileHandler):
    """File handler class for an abinit output file."""

    _loggername = "AbinitOutputFile"
    _parser_class = AbinitOutputParser
    _structure_class = AbinitOutputStructure
