from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitFatbandParser
from ...file_structures import AbinitFatbandStructure


class AbinitFatbandFile(BaseCalculationFileHandler):
    """File handler class for an abinit _FATBAND_* file."""

    _loggername = "AbinitFatbandFile"
    _parser_class = AbinitFatbandParser
    _structure_class = AbinitFatbandStructure
