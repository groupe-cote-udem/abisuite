from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitDMFTProjectorsParser
from ...file_structures import AbinitDMFTProjectorsStructure


class AbinitDMFTProjectorsFile(BaseCalculationFileHandler):
    """File Handler class for an abinit DMFT projectors file."""

    _loggername = "AbinitDMFTProjectorsFile"
    _parser_class = AbinitDMFTProjectorsParser
    _structure_class = AbinitDMFTProjectorsStructure
