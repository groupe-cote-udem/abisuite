from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitProcarParser
from ...file_structures import AbinitProcarStructure


class AbinitProcarFile(BaseCalculationFileHandler):
    """File handler class for a PROCAR file produced by abinit."""

    _loggername = "AbinitProcarFile"
    _parser_class = AbinitProcarParser
    _structure_class = AbinitProcarStructure
