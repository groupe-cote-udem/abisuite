from ..bases import BaseCalculationFileHandler
from ...file_parsers import AbinitBSEMDFParser
from ...file_structures import AbinitBSEMDFStructure


class AbinitBSEMDFFile(BaseCalculationFileHandler):
    """File handler class for a MDF file produced in a BSE abinit run."""

    _loggername = "AbinitBSEMDFFile"
    _parser_class = AbinitBSEMDFParser
    _structure_class = AbinitBSEMDFStructure
