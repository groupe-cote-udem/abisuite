import abc
import os

import aiofiles.os

import aioshutil

from async_property import async_property

from tqdm import tqdm

from ..bases import BaseHandler
from ..file_handlers import GenericFile, SymLinkFile
from ..file_handlers.bases import BaseFileHandler
from ...bases import BaseUtility
from ...linux_tools import mkdir, rmdir
from ...retryer import try_with_retries
from ...routines import full_abspath, full_path_split


class BaseDirectoryHandler(BaseHandler, BaseUtility, abc.ABC):
    """Base class for all directory handlers."""

    _is_writable = True
    isfile = False

    def __init__(self, *args, **kwargs):
        BaseHandler.__init__(self, *args, **kwargs)
        BaseUtility.__init__(self, *args, **kwargs)
        self._content = DirectoryContent(loglevel=self._loglevel)
        self._directory_size_Gb = None

    def __repr__(self):
        st = f"< {self.__class__} : {self.path}:"
        for fil in self._content:
            if isinstance(fil, BaseDirectoryHandler):
                st += f"\n - {fil}"
            else:
                st += f"\n - {fil.path}"
        return st

    async def contains(self, item):
        """Check of an item is contained within directory handler."""
        # if item is a str, probably a path
        if isinstance(item, str):
            async for x in self:
                if item == x.path or item == x.basename:
                    return True
            return False
        # if item not a str, it's another file or dir object
        if item in await self.content:
            return True
        return False

    async def equals(self, other):
        """Compares two directory handlers."""
        # two dir are equal if their path and content are equals
        if self.path != other.path:
            return False
        async for item1 in self:
            if not await other.contains(item1.path):
                return False
            # same path is in other directory handler, get th other item
            item2 = await other.get(item1.basename)
            # read files if they were not read yet
            for item in (item1, item2):
                # FG 08/04/2021 removing force reading
                # and only read if item exists
                if await item.exists:
                    await item.read()
                    # item.read(force=True)
            # don't compare these items if they don't exists otherwise an
            # error will be thrown that they miss some properties.
            if not (await item1.exists or await item2.exists) and (
                    not item1._is_writable):
                continue
            if await item1.not_equals(item2):
                self._logger.debug("NOT EQUAL:\n", item1, "!=\n", item2)
                return False
        # if self.content != other.content:
        #     return False
        return True

    async def not_equals(self, other):
        """Asynchronous != equivalent between dir handlers."""
        return not await self.equals(other)

    async def get(self, path):
        """Get an item handler contained in the directory handler."""
        if not isinstance(path, str):
            raise TypeError("Need path for directory getitem.")
        async for item in self:
            if item.basename == path:
                return item
        raise FileNotFoundError(path)

    async def __aiter__(self):
        for item in await self.content:
            yield item

    async def len(self):
        """Returns the number of items in the directory."""
        return len(await self.content)

    async def str(self):
        """Returns a string representation of the directory."""
        # if not self.has_been_read:
        #    self.read()
        return f"{self.path}:\n {str(await self.content)}"

    @async_property
    async def content(self):
        """Return the directory's content."""
        return self._content

    def delete_content(self):
        """Delete all handler objects in this directory."""
        # for item in self.content:
        #     item.delete()
        self._content.clear()

    @async_property
    async def directory_size(self):
        """Directory size in Gb."""
        if self._directory_size_Gb is not None:
            return self._directory_size_Gb
        self._directory_size_Gb = 0
        async with self:
            for handler in await self.walk(paths_only=False):
                if await handler.islink:
                    continue
                try:
                    self._directory_size_Gb += await handler.file_size
                except FileNotFoundError:
                    # file does not exist (yet perhaps). count it as 0 but
                    # warn user
                    self._logger.warning(
                            "Cannot accurately determine directory size "
                            f"because '{handler.path}' does not exists.")
        return self._directory_size_Gb

    @property
    def path(self):
        """Return the directory's path."""
        return BaseHandler.path.fget(self)

    async def set_path(self, path):
        """Set the path property."""
        if not isinstance(path, str):
            raise TypeError(f"path should be a str but got: {path}")
        if not await aiofiles.os.path.isdir(path) and not self._is_writable:
            raise ValueError(f"Is not a directory: {path}")
        if await aiofiles.os.path.isfile(path):
            raise FileExistsError("It's a file: {path}")
        super().set_path(path)

    async def add(self, item):
        """Add a file or a directory handler to the directory handler."""
        if not isinstance(
                item,
                BaseDirectoryHandler
                ) and not isinstance(item, BaseFileHandler):
            raise TypeError(f"Expected a dir or file handler but got: {item}")
        if item.path in [x.path for x in await self.content]:
            raise ValueError(f"Item path already in contents: {item.path}")
        # check that item's dirname is equal to the directory handler
        if os.path.dirname(item.path) != self.path:
            raise ValueError("Item's path not in directory.")
        self._content.append(item)

    def clear_content(self):
        """Delete content but not directory in itself."""
        self.delete_content()
        self.has_been_read = False

    async def copy(self, path, overwrite=False):
        """Copy the directory to a new location.

        Parameters
        ----------
        path : str
            Where to copy the directory's files.
        overwrite: bool, optional
            If True and the same directory or a file
            is present at destination,
            it will be removed before this one is copied.
        """
        path = full_abspath(path)
        if await aiofiles.os.path.isfile(path):
            if overwrite:
                await aiofiles.os.remove(path)
            else:
                raise FileExistsError(f"Is an existing file: {path}")
        # destination is already a directory. Check if empty. If not raise err.
        if await aiofiles.os.path.isdir(path):
            if len(await aiofiles.os.listdir(path)):
                if overwrite:
                    await aioshutil.rmtree(path)
                else:
                    raise IsADirectoryError(
                            "Destination is a directory that "
                            f"already exists: {path}")
        else:
            # directory doesn't exists => create it
            await mkdir(path)
        self._logger.debug(f"Copying directory {self.path} -> {path}")
        async for item in self:
            if not await item.exists:
                if item._is_writable:
                    # write item before copying it
                    await item.write()
                else:
                    # nothing to copy since file cannot be written
                    continue
                    # raise RuntimeError(f"Item does not exist can't copy: "
                    #                    f"{item.path}")
            name = item.basename
            newpath = os.path.join(path, name)
            if await aiofiles.os.path.isdir(item.path):
                self._logger.debug(f"Copying subdirectory: {item.path} -> "
                                   f"{newpath}")
                await item.copy(newpath)
            else:
                self._logger.debug(f"Copying file: {item.path} -> "
                                   f"{newpath}")
                await item.copy(newpath)

    async def copy_handler(self):
        """Copy the directory handler without copying the actual data."""
        new = self._get_new_instance()
        await new.set_path(self.path)
        # use the '_content' prop instead of 'content' because the latter can
        # be extended in subclass...
        # self.read(force=True)  # call read before copying
        for x in self._content:
            if not x.has_been_read:
                await x.read()
            new._content.append(await x.copy_handler())
        return new

    async def delete(self) -> None:
        """Delete directory and its content."""
        self._logger.debug(f"Deleting directory: {self.path}")
        # do nothing if directory does not exists
        if not await aiofiles.os.path.exists(self.path):
            return
        await try_with_retries(
                aioshutil.rmtree, func_args=[self.path],
                allowed_exceptions=[OSError],
                delay=1,
                )
        self.has_been_read = False

    async def move(self, path, _display_progress_bar=False):
        """Move directory to new location.

        Parameters
        ----------
        path : str
               The path where to move the directory. If path is a directory,
        _display_progress_bar : bool, optional
                                If True, a progress bar is displayed to show
                                file transfer status.
        """
        # WE USE A GLOBAL WALK HERE TO MOVE FILES TO PRESERVE 'internal'
        # symlinks
        # TODO: check if there is a way to bypass the use of walk() and
        # iterate over self.
        # Thinking it now I'm not sure it's possible (28/03/2019)
        path = full_abspath(path)
        self._logger.debug(f"Moving directory {self.path} -> {path}")
        if await aiofiles.os.path.isdir(path):
            if len(await aiofiles.os.listdir(path)):
                self._logger.error(f"Found {os.listdir(path)} in {path}")
                raise FileExistsError(f"Target directory not empty: {path}")
        elif await aiofiles.os.path.isfile(path):
            raise FileExistsError(f"Target directory is an already existing "
                                  f"file: {path}")
        if not await aiofiles.os.path.exists(path):
            await mkdir(path)
        link_sources = {}  # key=path before, value=target before
        moved_files = {}   # key=path before, value=where it was moved
        # make a first iteration with the walk method to get all links
        # to make sure all links are well rerouted even if they point very
        # far into the top dir
        walk = await self.walk(paths_only=False)
        for item in walk:
            subdir = os.path.dirname(os.path.relpath(
                item.path,
                start=self.path))
            if isinstance(item, SymLinkFile):
                # read file if not done yet
                if not item.has_been_read:
                    await item.read()
                link_sources[item.path] = item.source
            # compute newpath
            newpath = os.path.join(path, subdir, item.basename)
            # store where the file will be moved for future ref
            moved_files[item.path] = newpath
            # make subdir if necessary
            dirname = os.path.dirname(newpath)
            if not await aiofiles.os.path.exists(dirname):
                await mkdir(dirname)
        # now move everything
        if _display_progress_bar:
            walk = tqdm(
                    walk, unit=" files", unit_scale=False, leave=False,
                    dynamic_ncols=True,
                    desc=f"Moving calculation {self.basename}")
        for item in walk:
            newpath = moved_files[item.path]
            if await aiofiles.os.path.islink(item.path):
                # don't need to reread as it should have been done earlier
                if item.source in moved_files:
                    # that file was targeting a file that will be moved
                    # need to redirect source to new target
                    item.source = moved_files[item.source]
                # extra care now if target is a directory we need to check if
                # any of its files is moved. if its the case, change the target
                if await aiofiles.os.path.isdir(item.source):
                    for oldpath in moved_files:
                        if item.source in oldpath:
                            # one of its file is moved, change the source
                            old_rel_path = os.path.relpath(
                                    item.source,
                                    start=self.path)
                            item.source = os.path.join(path, old_rel_path)
                            break
            oldpath = item.path
            if not await item.exists and not await item.islink:
                # only change the path of the file handler
                item.set_path(newpath)
            else:
                await item.move(newpath)
            await self._post_file_move(oldpath, newpath)
            if _display_progress_bar:
                walk.update()
        if _display_progress_bar:
            walk.close()
        # now remove all empty directories
        # use os.walk here cause all paths might have scrambled and all hell
        # breaks loose
        for dirname, _subdirname, files in os.walk(self.path, topdown=False):
            if len(files):
                self._logger.error(f"Can't delete dir cause there are still"
                                   f" files inside: {files}")
                raise FileExistsError(f"There are still files in old dir after"
                                      f" moving them: {self.path}")
            await rmdir(dirname)
            # if they were empty also need to recreate empty dirs in target
            newpath = os.path.join(
                    path,
                    os.path.relpath(dirname, start=self.path))
            if not await aiofiles.os.path.exists(newpath):
                await mkdir(newpath)
        # need to reset all file paths
        self._logger.debug(f"Reset directory global path to {path}")
        await self.set_path(path)
        self.clear_content()
        self.has_been_read = False
        await self.read()

    async def read(self, recursive=False, force=False):
        """Read the directory and create corresponding File handler objects.

        Parameters
        ----------
        recursive : bool, optional
            If True, the subdirectories are read too. Not the default behavior.
        force : bool, optional
                If True, the directory is reread even if it was already read
                before.
        """
        if self.has_been_read and not force:
            if not recursive:
                return
            else:
                # recursive read, check for all subdirs
                for item in self:
                    if item.isdir:
                        await item.read(recursive=recursive)
        self._logger.debug(f"Reading directory: {self.path}")
        if not await self.exists:
            self._logger.debug(
                    "Nothing to read since directory doesn't exists.")
            super().read()
            return
        subpaths = await aiofiles.os.listdir(self.path)
        self.clear_content()
        for path in subpaths:
            fullpath = os.path.join(self.path, path)
            # start by symlink since it can point to both a dir or a file
            if await aiofiles.os.path.islink(fullpath):
                # self._logger.debug(f"Found a symlink: {fullpath}.")
                await self._add_symlink_to_content(self._content, fullpath)
                continue
            if await aiofiles.os.path.isdir(fullpath):
                # use same directory handler class
                # self._logger.debug(f"Found a subdir: {fullpath}.")
                await self._add_subdir_to_content(self._content, fullpath)
                continue
            # else it's a regular file
            # self._logger.debug(f"Found a file: {fullpath}.")
            await self._add_file_to_content(self._content, fullpath)
        super().read()

    async def walk(self, paths_only=True, allow_read_content=True):
        """Walk recursively through the directory.

        Returns a list with all the paths inside directory and in
        all subdirectories if any.

        All the paths returned are absolute!

        Parameters
        ----------
        paths_only: bool, optional
                    If True, only the file paths are returned. Else it's the
                    handlers which are returned.
        allow_read_content: bool, optional
            If True and directory exists and that it was not read, it allows
            to call the read() method before walking the directory tree.
        """
        allcontents = []
        if not self.has_been_read and await self.exists and allow_read_content:
            # if dir exists, read it to make sure everything is included in
            # content
            await self.read()
        async for x in self:
            if not isinstance(x, BaseDirectoryHandler):
                if paths_only:
                    allcontents.append(x.path)
                else:
                    allcontents.append(x)
                continue
            # is a directory, recursive walk
            allcontents += await x.walk(
                    paths_only=paths_only,
                    allow_read_content=allow_read_content)
        return allcontents

    async def _add_file_to_content(self, content, path):
        # for base class, only add file as a Generic File.
        handler = await GenericFile.from_file(path, loglevel=self._loglevel)
        # handler.read()
        content.append(handler, overwrite=True)

    async def _add_subdir_to_content(self, content, path):
        subdir = self.__class__(loglevel=self._loglevel)
        await subdir.set_path(path)
        # subdir.read()
        content.append(subdir, overwrite=True)

    async def _add_symlink_to_content(self, content, path):
        link = await SymLinkFile.from_file(path, loglevel=self._loglevel)
        # link.read()
        content.append(link, overwrite=True)

    def _get_new_instance(self):
        return self.__class__(loglevel=self._loglevel)

    async def _post_file_move(self, oldpath, newpath):
        # nothing to do at the top
        # this method is here mainly because of CalculationDirectory class
        # TODO: find a better way to code this
        pass


class BaseWritableDirectoryHandler(BaseDirectoryHandler, abc.ABC):
    """Writable directory handler class.

    Samething as a Normal Directory Handler except that it does not complain
    if the directory does not exist.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._items_to_copy = []

    async def get(self, item):
        """Get an item handler from the directory."""
        # get the content's item whose filename is 'item'
        for obj in await self.content:
            if obj.basename == item:
                return obj
        # if we are here, object does not exists
        raise KeyError(f"{item} does not exists inside directory.")

    @property
    def path(self):
        """Return the directory's path."""
        return self._path

    async def set_path(self, path):
        """Set the path property."""
        if not isinstance(path, str):
            raise TypeError(f"path should be a str but got: {path}")
        if await aiofiles.os.path.isfile(path):
            raise FileExistsError("It's a file: {path}")
        await super().set_path(path)
        await self._set_subpath()  # if needed for content

    async def add_symlink(self, path, source):
        """Add a file to symlink inside the directory.

        Parameters
        ----------
        path : str
               The absolute path of the symlink.
        source : str
                 The path to the source file (where the symlink points to).
        """
        self._logger.debug(f"Adding file to symlink: {path} -> {source}.")
        await self._add_file(path, source, symlink=True)

    async def add_copied_file(self, path, source):
        """Add a file to copy inside the directory.

        Parameters
        ----------
        path : str
               The absolute path of where the file will be copied.
        source : str
                 The path to the source file (where the symlink points to).
        """
        self._logger.debug(f"Adding file to copy: {path} -> {source}.")
        await self._add_file(path, source, symlink=False)

    async def write(self, **kwargs):
        """Create the directory and write its content."""
        await mkdir(self.path)
        await self.write_content(**kwargs)

    async def write_content(self, *args, **kwargs):
        """Write the content of the directory."""
        from .generic_directory import GenericDirectory
        if not await aiofiles.os.path.isdir(self.path):
            raise NotADirectoryError("Create directory using the 'write'"
                                     " method.")
        for item in await self.content:
            if isinstance(item, BaseFileHandler):
                if item in self._items_to_copy:
                    # copy the file from it's artificial 'source' attribute
                    try:
                        # need to temporary swap the path and source attribute
                        path = item.path
                        source = item.source
                        await item.set_path(source)
                        await item.copy(path, **kwargs)
                        await item.set_path(path)
                    except AttributeError:
                        self._logger.error(f"Cannot copy file: {item.path}.")
                        raise
                elif isinstance(item, GenericFile):
                    # cannot write generic file, skip it
                    continue
                elif item._is_writable:
                    await item.write(**kwargs)
            else:  # directory
                if item in self._items_to_copy:
                    try:
                        await item.copy(item.source, **kwargs)
                    except AttributeError:
                        self._logger.error(f"Cannot copy directory: "
                                           f"{item.path}.")
                        raise
                elif isinstance(item, GenericDirectory):
                    # cannot write generic directory, so skip it
                    continue
                else:
                    await item.write(*args, **kwargs)

    async def _add_file(self, path, source, symlink=False):
        path = full_abspath(path)
        source = full_abspath(source)
        # if self is not inside the path, raise error
        if not os.path.dirname(path).startswith(self.path):
            raise ValueError(f"File '{path}' not inside directory {self.path}")
        dirname = os.path.dirname(path)
        if await aiofiles.os.path.isdir(dirname) and (
                await aiofiles.os.path.isdir(self.path)):
            same = os.path.samefile(dirname, self.path)
        else:
            same = self.path == dirname
        if same:
            # file directly inside self, append it to content
            if await self.contains(os.path.basename(path)):
                self._logger.debug(
                        f"Already in directory: {path}\n"
                        f"Directory content:\n{self}")
                raise FileExistsError(f"Already in directory: {path}")
            if symlink:
                toadd = SymLinkFile(loglevel=self._loglevel)
            else:
                toadd = GenericFile(loglevel=self._loglevel)
                self._items_to_copy.append(toadd)
            await toadd.set_path(path)
            toadd.source = source
            self._content.append(toadd)
            return
        # else, the file is inside a subdir of self. Try getting the subdir if
        # it exists. if not, create it and retry
        first_subdir = full_path_split(os.path.relpath(path, self.path))[0]
        if not await self.contains(first_subdir):
            # create directory using same class as self if it does not exists
            generic = self.__class__(loglevel=self._loglevel)
            await generic.set_path(os.path.join(self.path, first_subdir))
            self._content.append(generic)
            directory = generic
        else:
            directory = await self.get(first_subdir)
        await directory._add_file(path, source, symlink=symlink)

    async def _reset_special_subfile_path(self, subfile):
        # reset the path of a file if it exists inside the directory
        # when a change in the directory path occured
        try:
            path = subfile.path
        except (AttributeError, ValueError):
            # file path not set yet, don't need to reset
            return
        else:
            # need to reset
            name = os.path.basename(path)
            await subfile.set_path(os.path.join(self.path, name))

    async def _set_subpath(self):
        pass


class DirectoryContent(BaseUtility):
    """Custom list for a directory's content.

    Equivalent of an ordered set but for the content of a directory handler.
    """

    _loggername = "DirectoryContent"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._container = []

    def __add__(self, other):
        if not isinstance(other, DirectoryContent):
            raise TypeError(f"Cannot add Directory Content with {other}.")
        for item in other:
            if item._path is not None:
                if item in self:
                    raise ValueError(
                            f"{item.path} already in directory content.")
        if len(other) and len(self):
            if os.path.dirname(self[0].path) != os.path.dirname(other[0].path):
                raise ValueError("Not same directory.")
        new = DirectoryContent(loglevel=self._loglevel)
        for item in self:
            new.append(item)
        for item in other:
            new.append(item)
        return new

    def __contains__(self, item):
        # only compare basenames
        allpaths = []
        for it in self:
            if it._path is not None:
                allpaths.append(it.basename)
        if isinstance(item, str):
            return item in allpaths
        return item.basename in allpaths

    def __getitem__(self, item):
        return self._container[item]

    def __iter__(self):
        for item in self._container:
            yield item

    def __len__(self):
        return len(self._container)

    async def str(self):
        """Return the string representation asynchronously."""
        if not len(self):
            return "Directory content: empty"
        if self[0]._path is None:
            first_path = f"{self[0].__class__} UNKNOWN PATH"
        else:
            first_path = self[0].path + "/"
        string = f"DirectoryContent: {first_path}\n"
        for item in self:
            if isinstance(item, BaseDirectoryHandler):
                if not item.has_been_read:
                    await item.read()
                substring = await item.content.str()
                split = substring.split("\n")
                split[0] = f"- {os.path.basename(item.path) + '/'}"
                if len(split) == 1:
                    split[0] += "\n"
                string += "\n".join([" " + x for x in split])
                continue
            if item._path is not None:
                string += f" - {os.path.basename(item.path)}\n"
            else:
                string += f" - {item.__class__} (UNKNOWN PATH)\n"
        return string
        # return (f"DirectoryContent: {os.path.dirname(self[0].path)}\n - " +
        #        "\n - ".join([os.path.basename(x.path) for x in self]))

    def append(self, item, overwrite=False):
        """Append an item to the directory's content."""
        isdirh = isinstance(item, BaseDirectoryHandler)
        isfileh = isinstance(item, BaseFileHandler)
        if not isdirh and not isfileh:
            raise TypeError("Only dir or file handlers in DirectoryContent.")
        if item._path is not None:
            if item in self:
                if not overwrite:
                    raise ValueError(f"{item} already in directory content.")
                else:
                    self.remove(item.path)
        # REMOVED since I add sometimes intentional symlinks between calc dirs.
        # if len(self):
        #     # check that its the same dirname
        #     if os.path.dirname(item.path) != os.path.dirname(self[0].path):
        #         raise ValueError(
        #                   f"{item.path} not same directory as other "
        #                   f"items in directory content:\n {str(self)}.")
        self._container.append(item)

    def clear(self):
        """Clear the directory's content."""
        del self._container
        self._container = []

    def remove(self, path):
        """Remove an item from the directory's content."""
        try:
            index = [x.path for x in self].index(path)
        except ValueError:
            # item not in self for some reason...
            return
        item = self._container.pop(index)
        del item
