import os

from async_property import async_property

from .bases import BaseWritableDirectoryHandler, DirectoryContent
from .generic_directory import GenericDirectory
from .output_data_directory import OutputDataDirectory
from ..file_handlers import PBSFile


class RunDirectory(BaseWritableDirectoryHandler):
    """Class that represents the 'run' directory of a calculation.

    It contains the PBS file which is served as the batch file to
    launch calculations.

    Also contains the OutputDataDirectory.
    """

    _loggername = "RunDirectory"
    _output_data_dir_name = "output_data"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._pbs_file = PBSFile(loglevel=self._loglevel)
        self.output_data_directory = OutputDataDirectory(
                loglevel=self._loglevel)
        self.check_pbs_file = True

    @async_property
    async def content(self):
        """Return the directory's content."""
        # get content + add pbs file
        content = await super().content
        extra = DirectoryContent(loglevel=self._loglevel)
        extra.append(self.pbs_file)
        extra.append(self.output_data_directory)
        return content + extra

    @property
    def pbs_file(self):
        """Return the pbs file handler."""
        # if self._pbs_file.exists:
        #     self._pbs_file.read()
        return self._pbs_file

    @pbs_file.setter
    def pbs_file(self, pbs_file):
        if not isinstance(pbs_file, PBSFile):
            raise TypeError(
                    f"Expected a 'PBSFile' object but got: {pbs_file}.")
        self._pbs_file = pbs_file

    def clear_content(self):
        """Clear the directory's content."""
        super().clear_content()
        self.output_data_directory.clear_content()

    async def copy_handler(self, *args, **kwargs):
        """Copy the directory handler object."""
        new = await super().copy_handler(*args, **kwargs)
        new.pbs_file = await self.pbs_file.copy_handler()
        await new.pbs_file.set_path(self.pbs_file.path)
        new.output_data_directory = (
                await self.output_data_directory.copy_handler())
        return new

    async def write_content(self, **kwargs):
        """Write the content of the directory."""
        # first check if the pbs file is actually inside the run dir
        if not self.pbs_file.path.startswith(
                self.path) and self.check_pbs_file:
            raise ValueError(f"PBS file is not in Run directory: "
                             f"{self.pbs_file.path}.")
        await super().write_content(**kwargs)

    async def _add_file_to_content(self, content, path):
        # if a pbs file, add as a pbs file object. else rely on base class
        if path.endswith(PBSFile._parser_class._expected_ending):
            await self.pbs_file.set_path(path)
            await self.pbs_file.read()
        else:
            await super()._add_file_to_content(content, path)

    async def _add_subdir_to_content(self, content, path):
        if path.endswith(self._output_data_dir_name):
            await self.output_data_directory.set_path(path)
            # await self.output_data_directory.read()
            return
        subdir = GenericDirectory(loglevel=self._loglevel)
        await subdir.set_path(path)
        # don't read since it can cause huge wait if filesystem is slow.
        # only read when asked (FG: 2021/12/21)
        # subdir.read()
        content.append(subdir)

    async def _set_subpath(self):
        await super()._set_subpath()
        outpath = os.path.join(self.path, self._output_data_dir_name)
        await self.output_data_directory.set_path(outpath)
        await self._reset_special_subfile_path(self.pbs_file)
