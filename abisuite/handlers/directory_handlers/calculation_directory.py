import json
import logging
import os

import aiofiles.os

from async_property import async_property

from .bases import (
        BaseDirectoryHandler, BaseWritableDirectoryHandler, DirectoryContent,
        )
from .generic_directory import GenericDirectory
from .input_data_directory import InputDataDirectory
from .run_directory import RunDirectory
from ..file_handlers import (
        CALCTYPES_TO_INPUT_FILE_CLS,
        CALCTYPES_TO_LOG_FILE_CLS,
        MetaDataFile, StderrFile, SymLinkFile,
        )
from ..file_handlers.bases import BaseInputFileHandlerNoInputVariables
from ... import __USER_DATABASE__
from ...cleaners import __CALCTYPES_TO_CLEANER_CLS__
from ...databases import DBCalculation
from ...packagers import (
        __CALCTYPES_TO_PACKAGER_CLS__,
        compute_packaging_path,
        get_packaging_roots,
        )
from ...retryer import (
        try_with_retries_sync,
        )
from ...routines import (
        connect_to_database, full_abspath, is_str,
        )
from ...status_checkers import CALCTYPES_TO_STATUS_CHECKER_CLS


__DOES_NOT_EXIST__ = "does not exist"


class CalculationDirectory(BaseWritableDirectoryHandler):
    """A calculation directory handler.

    A directory that contains:
        - input data sub directory
        - output data directory
        - run data directory
        - input file and meta data file
        - batch script file (in RunDirectory)
    """

    _loggername = "CalculationDirectory"
    _input_data_dir_name = "input_data"
    _run_dir_name = "run"

    def __init__(self, input_file_cls, *args, **kwargs):
        """Calculation Directory init method.

        Parameters
        ----------
        input_file_cls : class for the input file.
        """
        if not issubclass(
                input_file_cls, BaseInputFileHandlerNoInputVariables):
            raise ValueError("Need an input file class but got: "
                             f"{input_file_cls}")
        super().__init__(*args, **kwargs)
        self.run_directory = RunDirectory(loglevel=self._loglevel)
        self.input_data_directory = InputDataDirectory(loglevel=self._loglevel)
        self.meta_data_file = MetaDataFile(loglevel=self._loglevel)
        self.input_file = input_file_cls(loglevel=self._loglevel)
        self.connected_to_database = False
        self._db_model = None
        self._log_file = None
        self._stderr_file = None
        self._status_checker = None
        self._cleaner = None
        self._packager = None

    @async_property
    async def calctype(self):
        """Return the calculation directory's calctype."""
        if not await self.meta_data_file.exists:
            # meta data file does not exists. Check if calctype was defined
            try:
                return self.meta_data_file.calctype
            except ValueError:
                raise FileNotFoundError(
                    "Need to write 'meta data file' in order to get its "
                    "calctype.")
        async with self.meta_data_file as meta:
            return meta.calctype

    def set_calctype(self, calctype):
        """Sets calctype."""
        self.meta_data_file.calctype = calctype

    @async_property
    async def cleaner(self):
        """Return the cleaner class for this calculation directory."""
        if self._cleaner is not None:
            return self._cleaner
        try:
            cleaner_cls = __CALCTYPES_TO_CLEANER_CLS__[await self.calctype]
        except KeyError:
            raise NotImplementedError(
                    f"Cleaner not implemented for {self.calctype}")
        self._cleaner = cleaner_cls(loglevel=self._loglevel)
        await self._cleaner.set_calculation_directory(self)
        return self._cleaner

    @async_property
    async def content(self):
        """Return the directory's content."""
        content = await super().content
        extra = DirectoryContent(loglevel=self._loglevel)
        extra.append(self.input_data_directory)
        extra.append(self.run_directory)
        extra.append(self.meta_data_file)
        extra.append(self.input_file)
        if self._log_file is not None:
            extra.append(await self.log_file)
        if self._stderr_file is not None:
            extra.append(await self.stderr_file)
        return content + extra

    @property
    def db_model(self):
        """Return the database model for this calculation.

        If no model exists, the __DOES_NOT_EXIST__ string is returned instead.
        Also, the CalculationDirectory object must be connected to a database.
        """
        if self._db_model is not None:
            return self._db_model
        self._init_db_model()
        return self.db_model

    @property
    def is_in_database(self):
        """Return True if the calculation directory is in the database."""
        if self.db_model == __DOES_NOT_EXIST__:
            return False
        return True

    @async_property
    async def is_packaged(self) -> bool:
        """Return True if this calc dir has been packaged."""
        if not self.connected_to_database:
            raise RuntimeError("Need to connect to database first!")
        if not self.is_in_database:
            return False
        db_packaged = self.db_model.packaged
        if await aiofiles.os.path.islink(self.path):
            return db_packaged
        src_root, dest_root = get_packaging_roots()
        if dest_root in self.path:
            if db_packaged is False:
                self._logger.warning(
                        f"Root dest is in path of '{self.path}'. "
                        "But, db says it is not 'packaged'. Correcting it.")
                self.db_model.update_packaged(True)
                return True
        else:
            dest = compute_packaging_path(self.path, src_root, dest_root)
            if not await aiofiles.os.path.exists(dest) and db_packaged is True:
                self._logger.debug(
                        f"Packaging dest '{dest}' has nothing while calc '"
                        f"{self.path}' is packaged in DB. Fixing it in DB.")
                self.db_model.update_packaged(False)
                return False
        return db_packaged

    @async_property
    async def jobname(self):
        """Return the jobname of the calculation."""
        async with self.meta_data_file as meta:
            return meta.jobname

    async def set_jobname(self, jobname: str) -> None:
        """Set the jobname.

        Parameters
        ----------
        jobname: str
            The jobname of the calculation.
        """
        async with self.meta_data_file as meta:
            meta.jobname = jobname
        async with self.pbs_file as pbs:
            pbs.jobname = jobname

    @async_property
    async def log_file(self):
        """Return the calculation's log file handler."""
        if self._log_file is not None:
            return self._log_file
        try:
            self._log_file = await self._get_log_file()
        except ValueError:
            # try reading calc dir
            await self.read()
            try:
                self._log_file = await self._get_log_file()
            except ValueError:
                raise FileNotFoundError(
                    f"Not able to get log file in {self.path}.")
        return self._log_file

    async def _get_log_file(self):
        # need to get log file
        logcls = CALCTYPES_TO_LOG_FILE_CLS[await self.calctype]
        # the class method will do the init
        self._logger.debug(f"Getting log object for calc: '{self.path}'.")
        return await logcls.from_meta_data_file(
            self.meta_data_file, loglevel=self._loglevel
            )

    # some aliases
    @property
    def output_data_directory(self):
        """Return the Output data dir handler."""
        return self.run_directory.output_data_directory

    @async_property
    async def packager(self):
        """Return the packager object of this calculation directory."""
        if self._packager is not None:
            return self._packager
        try:
            packager_cls = __CALCTYPES_TO_PACKAGER_CLS__[await self.calctype]
        except KeyError:
            raise NotImplementedError(
                    f"Packager not implemented for {await self.calctype}")
        self._packager = packager_cls(loglevel=self._loglevel)
        await self._packager.set_calculation_directory(self)
        return self._packager

    @async_property
    async def parents(self):
        """Return the parents list of this calculation."""
        async with self.meta_data_file as meta:
            return meta.parents

    @property
    def path(self):
        """Return the calculation directory's path."""
        return super().path

    async def set_path(self, path):
        """Sets the calculation directory handler's path."""
        await super().set_path(path)
        # then set the meta data file with all the paths
        self.meta_data_file.calc_workdir = self.path
        await self.run_directory.set_path(
                os.path.join(self.path, self._run_dir_name))
        await self.input_data_directory.set_path(os.path.join(
                self.path, self._input_data_dir_name
                ))
        for item in (self.input_file, self.meta_data_file):
            await self._reset_special_subfile_path(item)
        self.meta_data_file.input_data_dir = self.input_data_directory.path
        self.meta_data_file.rundir = self.run_directory.path
        odd = self.run_directory.output_data_directory.path  # alias
        self.meta_data_file.output_data_dir = odd
        if await self.meta_data_file.exists:
            if not self.meta_data_file.has_been_read:
                await self.meta_data_file.read()
            # set input file and pbs file path already
            await self.input_file.set_path(self.meta_data_file.input_file_path)
            await self.pbs_file.set_path(self.meta_data_file.pbs_file_path)

    @property
    def pbs_file(self):
        """Return the pbs file handler."""
        # read run directory before to make sure pbs_file exists
        # NOTE: don't do this as it is allowed to have a pbs file object
        # without having a real file existing.
        # if self.run_directory.exists:
        #     self.run_directory.read()
        return self.run_directory.pbs_file

    @pbs_file.setter
    def pbs_file(self, pbs_file):
        self.run_directory.pbs_file = pbs_file

    @async_property
    async def status(self):
        """Return the calculation's status dict."""
        if not self.connected_to_database:
            try:
                self.connect_to_database()
            except (RuntimeError, ImportError):
                # cannot access database module
                pass
        if self.connected_to_database:
            if not self.is_in_database:
                # we were able to connect to DB.
                self.add_to_database()
        return await (await self.status_checker).calculation_status

    @async_property
    async def status_checker(self):
        """Return the status checker object for this calculation directory."""
        if self._status_checker is not None:
            return self._status_checker
        try:
            stat_cls = CALCTYPES_TO_STATUS_CHECKER_CLS[await self.calctype]
        except KeyError:
            raise NotImplementedError(f"Status checker not implemented for "
                                      f"{await self.calctype}")
        self._status_checker = stat_cls(loglevel=self._loglevel)
        await self._status_checker.set_calculation_directory(self)
        return self._status_checker

    @async_property
    async def stderr_file(self):
        """Return the stderr file handler."""
        if self._stderr_file is not None:
            return self._stderr_file
        # the class method will do the init
        stderr_file = StderrFile(loglevel=self._loglevel)
        # .from_meta_data_file(
        #         self.meta_data_file, loglevel=self._loglevel
        #         )
        try:
            if await self.pbs_file.exists:
                await self.pbs_file.read()
            await stderr_file.set_path(self.pbs_file.stderr_path)
        except ValueError:
            # stderr file path not set in pbs file yet
            pass
        try:
            if await self.meta_data_file.exists:
                await self.meta_data_file.read()
            await stderr_file.set_path(os.path.join(
                        self.path,
                        self.meta_data_file.jobname + ".stderr"))
        except ValueError:
            # jobname not defined
            raise FileNotFoundError("Cannot instanciate stderr file.")
        self._stderr_file = stderr_file
        return self._stderr_file

    @property
    def walltime(self):
        """Return the walltime of the calculation as stored in the database.

        If not connected to database or not in a database, the calculation
        directory is connected and/or added to database.
        """
        if not self.connected_to_database:
            self.connect_to_database()
        if not self.is_in_database:
            self.add_to_database()
        return self.db_model.get_walltime(refresh=True)

    async def add_copied_file(self, path, source, *args, **kwargs):
        """Add a file to copy inside calculation diectory.

        Parameters
        ----------
        path: str
              The path of the new file to copy. Path must start with the
              input data directory path.
        source : str
                 The path to the file to copy.
        """
        await super().add_copied_file(path, source, *args, **kwargs)
        # also add the path to the meta data file
        self.meta_data_file.add_copied_file(source)

    async def add_symlink(self, path, source, *args, **kwargs):
        """Add a symlink into the directory.

        Parameters
        ----------
        path: str
              The path of the new file to symlink.
        source : str
                 The path to the file to link to.
        """
        # add the synlinked file to the meta data file
        await super().add_symlink(path, source, *args, **kwargs)
        self.meta_data_file.add_linked_file(source)

    def add_to_database(self):
        """Add calculation to the connected database."""
        if not self.connected_to_database:
            self.connect_to_database()
        self._db_model = DBCalculation.create(self)

    async def clean(self, *args, **kwargs):
        """Uses the cleaner object to clean calculation directory.

        All arguments are passed to the corresponding 'clean' method.
        """
        await (await self.cleaner).clean(*args, **kwargs)

    def clear_content(self):
        """Clear the content of this directory handler."""
        super().clear_content()
        self.run_directory.clear_content()
        self.input_data_directory.clear_content()

    async def copy_handler(self):
        """Copy the handler."""
        new = await super().copy_handler()
        new.meta_data_file = await self.meta_data_file.copy_handler()
        new.run_directory = await self.run_directory.copy_handler()
        new.input_file = await self.input_file.copy_handler()
        new.input_data_directory = (
                await self.input_data_directory.copy_handler())
        if self._log_file is not None:
            new._log_file = await self.log_file.copy_handler()
        if self._stderr_file is not None:
            async with await self.stderr_file:
                new._stderr_file = await (
                        await self.stderr_file).copy_handler()
        return new

    def connect_to_database(self, **kwargs):
        """Connect this calculation handler to a database."""
        try:
            connect_to_database(**kwargs)
            self.connected_to_database = True
            if not self.is_in_database and self.exists:
                self.add_to_database()
        except Exception:
            self.connected_to_database = False

    def delete_from_database(self):
        """Delete calculation from the connected database."""
        if not self.connected_to_database:
            self.connect_to_database()
        if not self.is_in_database:
            # nothing to do
            return
        self.db_model.delete_instance()

    async def move(self, newpath, *args, **kwargs):
        """Move the calculation directory."""
        # call the base class's method and change the content of the meta
        # data file if needed
        # check if calculation was in a connected database.
        oldpath = self.path
        if self.connected_to_database:
            if __USER_DATABASE__ is None:
                raise RuntimeError(
                        "'connected to database but user database is None...")
            db_path = __USER_DATABASE__.database
            if db_path in await self.walk(
                    paths_only=True, allow_read_content=True):
                # if db inside moving directory, we won't be able to update it
                # TODO: maybe there is a way to do it, just change the path
                # of the db object
                raise FileExistsError(
                    f"DB: '{db_path}' inside moving directory '{self.path}'.")
            await self.update_database_calculation(newpath, force=True)
        await super().move(newpath, *args, **kwargs)
        # make subdirs to reread themselve cause we looped over the walk() meth
        # for subdir in (self.run_directory, self.input_data_directory):
        #     subdir.path = self.path + os.path.basename(subdir.path)
        #     subdir.read()
        await self.update_new_meta_data_file(newpath, oldpath)

    async def package(self, *args, **kwargs):
        """Packages results from this calculation directory."""
        await (await self.packager).package(*args, **kwargs)

    async def read(self, *args, **kwargs):
        """Read the calculation directory."""
        await super().read(*args, **kwargs)
        # also read run dir if it exists
        if self.run_directory.exists:
            await self.run_directory.read(*args, **kwargs)

    def remove_from_database(self):
        """Remove the calculation from the database."""
        self.delete_from_database()

    async def reset_status(self) -> None:
        """Resets the calculation status in order to recompute.

        Useful after
        relaunching for instance.

        Also reset walltime while we're here.
        """
        if self.is_in_database:
            await self.db_model.update_calculation_status(
                    new_status=(
                        await self.status_checker).get_initial_status_dict()
                    )
            await self.db_model.update_walltime(
                    new_walltime=None, force=True)
        # reset status checker
        del self._status_checker
        self._status_checker = None

    async def update_database_calculation(self, new_path, **kwargs):
        """Updates the database entry for the path of this calculation.

        Parameters
        ----------
        new_path: str
            The new path of the databse entry for this calculation.

        Other Parameters
        ----------------
        kwargs:
            Other kwargs are passed to the
            database :meth:`.DBCalculation.update_calculation` method.
        """
        if not self.is_in_database:
            raise RuntimeError("Need to add calculation to database first.")
        self._logger.info("Updating database calculation path entry.")
        # check if new path is already in db
        await self.db_model.update_calculation(new_path, **kwargs)

    def update_database_monitored(self, monitored):
        """Sets the monitored status to the given value.

        Parameters
        ----------
        monitored: bool
            The monitored status to set to.
        """
        self._update_database("monitored", monitored)

    def update_database_packaged(self, packaged):
        """Sets the pacakged tag to the given value in the database.

        Parameters
        ----------
        packaged: str
            The new packaged tag.
        """
        self._update_database("packaged", packaged)

    def update_database_project(self, project):
        """Sets the project tag to the given value in the database.

        Parameters
        ----------
        project: str
            The project tag.
        """
        self._update_database("project", project)

    def update_database_status(self):
        """Update the calculation status in the database."""
        self._update_database("status", self.status)

    async def update_new_meta_data_file(
            self,
            newpath: str,
            oldpath: str,
            update_parents: bool = True,
            ) -> None:
        """Updates the content of a new meta data file.

        This new file is located
        at destination after moving or packaging.

        Parameters
        ----------
        newpath: str
            The path where the new calculation is stored.
        oldpath: str
            The path where the old calculation was stored.
        update_parents: bool, optional
            If True, any parents meta data file will be updated
            with their new child's path.
        """
        async with await MetaDataFile.from_calculation(
                newpath, loglevel=self._loglevel) as meta:
            meta.calc_workdir = newpath
            to_remove = []
            for child in meta.children:
                # need to change the parent's location for these children
                if not await self.is_calculation_directory(child):
                    # it was removed at some point
                    to_remove.append(child)
                    continue
                async with await MetaDataFile.from_calculation(
                        child, loglevel=self._loglevel) as childmeta:
                    if oldpath not in childmeta.parents:
                        self._logger.warning(
                                "Expected to change a dir location in "
                                " children of newly packaged/moved dir. But "
                                "calc was not found in parents of children: "
                                f"'{child}'.")
                        continue
                    childmeta.parents.remove(oldpath)
                    childmeta.parents.append(newpath)
            for child in to_remove:
                meta.children.pop(meta.children.index(child))
            if not update_parents:
                return
            # change parent calculation too
            for parent in meta.parents:
                async with await MetaDataFile.from_calculation(
                        parent, loglevel=self._loglevel) as parentmeta:
                    if oldpath not in parentmeta.children:
                        self._logger.warning(
                                "Expected to change a dir location in parents"
                                " of newly packaged/moved dir. "
                                "But old calc was not found in children of "
                                f"parent: '{parent}'.")
                        continue
                    parentmeta.children.remove(oldpath)
                    parentmeta.children.append(newpath)

    def _update_database(self, attribute, value):
        # actually updates the property attribute of the database.
        try:
            import peewee
        except ImportError:
            raise RuntimeError("Cannot update database without peewee.")
        if not self.is_in_database:
            self.add_to_database()
        query = self.db_model.update(**{attribute: value})
        try_with_retries_sync(
                query.execute, delay=5,
                allowed_exceptions=(peewee.OperationalError, ),
                max_retries=5,
                )
        # need to refresh model
        self._init_db_model()

    async def write_content(self, *args, **kwargs):
        """Write content of directory."""
        # if log/stderr file is not None, files should not be written
        if self._log_file is not None:
            if await (await self.log_file).exists:
                self._logger.error(
                        "Log file exists. Should not write a directory")
                raise FileExistsError((await self.log_file).path)
        if self._stderr_file is not None:
            if await (await self.stderr_file).exists:
                self._logger.error(
                    "Stderr file exists. Should not write a directory")
                raise FileExistsError((await self.stderr_file).path)
        if not self.meta_data_file.path.startswith(self.path):
            raise ValueError(f"meta data file not in calculation dir: "
                             f"{self.meta_data_file.path}")
        if not self.input_file.path.startswith(self.path):
            raise ValueError(f"Input file not in calculation dir: "
                             f"{self.input_file.path}")
        # set some data in meta data
        self.meta_data_file.input_file_path = self.input_file.path
        sifp = self.pbs_file.input_file_path  # alias
        self.meta_data_file.script_input_file_path = sifp
        self.meta_data_file.pbs_file_path = self.pbs_file.path
        await super().write_content(*args, **kwargs)

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Create a CalculationDirectory object from a path.

        The directory should be an already existing
        calculation directory only from it's path.

        Parameters
        ----------
        path: str or CalculationDirectory instance
            Gives the path towards the calc dir. If a CalculationDirectory
            instance is given instead, this method becomes trivial!

        Returns
        -------
        CalculationDirectory: The instance we want.

        Raises
        ------
        TypeError: If 'path' is not a str or CalcDir instance.
        """
        if is_str(path):
            path = full_abspath(path)
        elif isinstance(path, cls):
            # if already a calc dir, return it
            return path
        else:
            raise TypeError(
                    "Expected a path or a CalculationDirectory instance.")
        loglevel = kwargs.get("loglevel", logging.INFO)
        try:
            async with await MetaDataFile.from_calculation(
                    path, loglevel=loglevel) as meta:
                obj = await cls.from_meta_data_file(meta, *args, **kwargs)
        except json.decoder.JSONDecodeError as err:
            # something happened to the meta data file.
            logger = logging.getLogger(cls._loggername)
            logger.setLevel(loglevel)
            logger.debug(
                    f"The meta data file at '{path}' cannot be read for some "
                    "reason... Cannot create a CalculationDirectory instance.")
            # logger.exception(err)
            raise err
        # check if the calc workdir is good.
        # I check this for calculations that may have been moved without
        # the meta data file being changed (FG: 2021/10/22)
        if obj.path != path and not await aiofiles.os.path.islink(path):
            # the calc dir was moved but the meta data file didnt change
            # reset it and inform user
            obj._logger.warning(
                    f"Calculation Directory's path ('{path}') does not match "
                    f"it's meta data file ('{obj.path}'). I corrected the meta"
                    " data file."
                    )
            async with meta:
                meta.calc_workdir = path
            await obj.set_path(path)
        return obj

    @classmethod
    async def from_meta_data_file(cls, meta, *args, **kwargs):
        """Create a Calculation Directory object from a MetaDataFile object."""
        ifcls = CALCTYPES_TO_INPUT_FILE_CLS[meta.calctype]
        cd = cls(ifcls, *args, **kwargs)
        # set meta data file first since setting the calc path will need to
        # know the current location of the meta data file.
        await cd.meta_data_file.set_path(meta.path)
        await cd.set_path(meta.calc_workdir)
        # cd.read()
        return cd

    @staticmethod
    async def is_calculation_directory(
            directory, loglevel=None):
        """Returns True if directory is a calculation directory.

        Returns False otherwise.

        What this method do: try to create a calculation directory
        and tries to read it. If somethings wrong, aborts and returns False.

        Parameters
        ----------
        directory: str
            The path to a directory.
        loglevel: int, optional
            If not None, a logger is created with this loglevel.
        """
        logger = None
        calc_loglevel = logging.INFO
        if loglevel is not None:
            logging.basicConfig()
            logger = logging.getLogger(CalculationDirectory._loggername)
            logger.setLevel(loglevel)
            calc_loglevel = loglevel
            logger.debug(f"Checking if {directory} is a calc dir.")
        directory = full_abspath(directory)
        try:
            if logger is not None:
                logger.debug(
                    f"Try creating calc dir: {directory}")
            calc = await CalculationDirectory.from_calculation(
                    directory, loglevel=calc_loglevel)
            if logger is not None:
                logger.debug("Was able to create calc dir.")
            try:
                # check that all import file exists
                await calc.read()
                if logger is not None:
                    logger.debug("Try to create input file object.")
                    try:
                        logger.debug(
                            "Checking for input file at "
                            f"{calc.input_file.path}")
                    except ValueError as err:
                        logger.debug("Input file path not set.")
                        raise err
                if not await aiofiles.os.path.isfile(calc.input_file.path):
                    if logger is not None:
                        logger.debug(
                            f"No input file found at: {calc.input_file.path}")
                    return False
                if logger is not None:
                    logger.debug(
                        f"Checking for meta file at "
                        f"{calc.meta_data_file.path}")
                if not await aiofiles.os.path.isfile(calc.meta_data_file.path):
                    if logger is not None:
                        logger.debug(
                            f"No meta file found at: "
                            f"{calc.meta_data_file.path}")
                    return False
                # FG: 2021/05/12 I disabled the pbs file check since
                # we can remove or modify this file without taking out
                # any actual calculation properties. Also, I am trying to
                # make the calc dir more flexible by not necessarily relying
                # onto the pbs file and more on the meta data file.
                # if logger is not None:
                #     logger.debug(
                #         f"Checking for pbs file at "
                #         f"{calc.pbs_file.path}")
                # if not os.path.isfile(calc.pbs_file.path):
                #     if logger is not None:
                #         logger.debug(
                #             f"No pbs file found at: {calc.pbs_file.path}")
                #     return False
                # NOTE: had problem with this check as wannier90 links
                # files directly in workdir this input data dir is empty
                # even though it has linked/copied files
                # if logger is not None:
                #     logger.debug(
                #         f"Checking for input data dir at "
                #         f"{calc.input_data_directory.path}")
                # if not os.path.isdir(calc.input_data_directory.path):
                #     # it's ok to lack a run directory if there's nothing
                #     # linked to the calculation
                #     linked_files = calc.meta_data_file.linked_files
                #     copied_files = calc.meta_data_file.copied_files
                #     if linked_files or copied_files:
                #         if logger is not None:
                #             logger.debug(
                #                 f"No input data dir found at: "
                #                 f"{calc.input_data_directory.path}")
                #         return False
                if logger is not None:
                    logger.debug(
                        f"Checking for run directory at "
                        f"{calc.run_directory.path}")
                if not await aiofiles.os.path.isdir(calc.run_directory.path):
                    if logger is not None:
                        logger.debug(
                            f"No run dir found at: {calc.run_directory.path}")
                    return False
            except ValueError:
                # some paths not set, return False
                if logger is not None:
                    logger.debug(
                        "Some paths not set.")
                return False
        except (NotADirectoryError, FileNotFoundError):
            # if any error occurs, just return False
            if logger is not None:
                logger.debug(
                    "An error occured.")
            return False
        except json.decoder.JSONDecodeError:
            # a meta data file was found but for some reason,
            # it is not readable. declare this a calc dir anyway
            if logger is not None:
                logger.warning(
                    "A calc dir was found but meta file is not readable: "
                    f"'{directory}'.")
            return True
        # if everything works, return True
        if logger is not None:
            logger.debug(
                    f"Is a calc dir: {directory}")
        return True

    async def _add_file_to_content(self, content, path):
        # if an input file, add as the input file
        if path.endswith(self.input_file._parser_class._expected_ending):
            await self.input_file.set_path(path)
            # self.input_file.read()
            return
        # do samething for meta data file
        elif path.endswith(
                self.meta_data_file._parser_class._expected_ending
                ):
            if self.meta_data_file._path is not None:
                # file already set, don't reread it
                # set because log file was found first
                return
            self._logger.debug(f"Found meta data file: {path}")
            await self.meta_data_file.set_path(path)
            # self.meta_data_file.read()
            return

        async def create_meta():
            # this temporary method creates a meta data file if needed
            if self.meta_data_file._path is None:
                # set meta file if not already done
                self.meta_data_file = await MetaDataFile.from_calculation(
                        self.path, loglevel=self._loglevel)
                await self.meta_data_file.read()

        # samething for stderr file
        if path.endswith(
                StderrFile._parser_class._expected_ending):
            # if meta data file was not created yet, create it
            await create_meta()
            await (await self.stderr_file).set_path(path)
            return
        # TODO: find a better way to dothis
        # proposition: first get meta file when reading. then gather input and
        # log files if they exist. Then get all other files.
        elif path.endswith("log") or path.endswith("wout"):
            # MOST PROBABLY A LOG FILE, therefore, meta data file must already
            # be written prior to the read. if its not the case everything will
            # break down anyway.
            await create_meta()
            # call log_file to set it and continue
            await self.log_file
            # just a check, if log file path is different from this one, append
            # it as a regular file
            if not await aiofiles.os.path.samefile(
                    (await self.log_file).path, path):
                await super()._add_file_to_content(content, path)
            return
        else:
            await super()._add_file_to_content(content, path)

    async def _add_subdir_to_content(self, content, path):
        if path.endswith(self._run_dir_name):
            await self.run_directory.set_path(path)
            # self.run_directory.read()
            return
        elif path.endswith(self._input_data_dir_name):
            await self.input_data_directory.set_path(path)
            # self.input_data_directory.read()
            return
        subdir = GenericDirectory(loglevel=self._loglevel)
        await subdir.set_path(path)
        # subdir.read()
        content.append(subdir)

    def _get_new_instance(self):
        return self.__class__(self.input_file.__class__,
                              loglevel=self._loglevel)

    def _init_db_model(self):
        """(Re)initialize DB model."""
        if not self.connected_to_database:
            raise RuntimeError("Need to connect to database first!")
        try:
            import peewee
        except ImportError:
            raise RuntimeError("Cannot update database without peewee.")
        paths = [self.path]
        # take into account possible symlinks for the DB
        if os.path.islink(self.path):
            paths.append(os.readlink(self.path))
        db_model = DBCalculation.get_or_none(
                DBCalculation.calculation.in_(paths))
        if db_model is None:
            self._db_model = __DOES_NOT_EXIST__
        else:
            self._db_model = db_model
            if os.path.islink(self.path) and db_model.calculation != paths[-1]:
                # reset db model to link source
                self._logger.info(
                        f"DB path ('{self.path}') was not the source of the "
                        f"symlink ('{paths[-1]}'). I fixed it.")
                query = DBCalculation.update(
                        {DBCalculation.calculation: paths[-1]}).where(
                                DBCalculation.calculation == paths[0]
                                )
                try_with_retries_sync(
                        query.execute, delay=5,
                        allowed_exceptions=(peewee.OperationalError, ),
                        max_retries=5,
                        )

    async def _post_file_move(self, oldpath, newpath):
        # after moving a file because directory is moving, make checks
        # in all children to see if we need to change links
        for child in self.meta_data_file.children:
            async with await MetaDataFile.from_calculation(
                    child, loglevel=self._loglevel) as child_meta:
                if oldpath in child_meta.copied_files:
                    # just change the reference and continue
                    child_meta.copied_files.remove(oldpath)
                    child_meta.copied_files.append(newpath)
                    continue
                if oldpath not in child_meta.linked_files:
                    continue
                # moved file was a target of another calculation. change this
                # other calculation to make sure all links stays the good
                # first we change the file in meta data
                child_meta.linked_files.remove(oldpath)
                child_meta.linked_files.append(newpath)
                # then parse the whole calculation to find the corresonding
                # links and change them
                async with await CalculationDirectory.from_calculation(
                        child, loglevel=self._loglevel) as calc:
                    for path in await calc.walk():
                        if not await aiofiles.os.path.islink(path):
                            continue
                        target = await aiofiles.os.readlink(path)
                        if oldpath not in target:
                            continue
                        # ok we found a file pointing at the moved file.
                        # change it now
                        sym = SymLinkFile(loglevel=self._loglevel)
                        await sym.set_path(path)
                        sym.source = newpath
                        await sym.write(overwrite=True)
                        # continue to loop just in case there are other files
                        # pointing at the same thing


class CalculationTree(BaseDirectoryHandler):
    """Represents a tree of directories which contain calculations."""

    _loggername = "CalculationTree"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.calculations = []

    def __add__(self, calctree):
        # defines the addition between 2 calc trees
        # just return the concatenated list of the calculations
        self.calculations += calctree.calculations
        return self

    def __len__(self):
        return len(self.calculations)

    @property
    def status(self):
        """Return the status for all calculations in the tree."""
        # return the list of status of all calculations
        return [calc["calculation_directory"].status
                for calc in self.calculations]

    async def build_tree(self, follow_symlinks=True):
        """Build the calculation tree.

        Fills the 'calculations' attribute
        of the tree.

        The calculations attribute is a list of dict containing the
        CalculationDirectory object, the path in the tree and a flag to
        tell if it is a symlink. We do this way to handle symlinked calcdir.

        Parameters
        ----------
        follow_symlinks: bool, optional
            If True, symlinks are followed and the calculation it points to
            is added to the tree. Otherwise, symlinks are ignored.
        """
        await self.read()
        if await CalculationDirectory.is_calculation_directory(
                self.path):
            if await aiofiles.os.path.islink(self.path):
                if not follow_symlinks:
                    return
                path = await aiofiles.os.readlink(self.path)
                is_symlink = True
            else:
                path = self.path
                is_symlink = False
            calc = await CalculationDirectory.from_calculation(
                            path, loglevel=self._loglevel)
            self.calculations.append(
                    {"calculation_directory": calc,
                     "path": self.path,
                     "is_symlink": is_symlink,
                     })
            return

        def analyse_item(item):
            # returns True if we analyse item. otherwise return false
            for calculation in self.calculations:
                # make sure training slash is present by joining nothing
                if item.path.startswith(os.path.join(calculation["path"], "")):
                    return False
            return True

        async for item in self:
            # check if a calculation path is already contained in the path
            # if so don't bother checking the item
            if not analyse_item(item):
                continue
            if isinstance(item, CalculationDirectory):
                self.calculations.append(
                        {"calculation_directory": item,
                         "is_symlink": False,
                         "path": item.path,
                         })
                continue
            elif isinstance(item, SymLinkFile) and follow_symlinks:
                try:
                    async with item:
                        if await CalculationDirectory.is_calculation_directory(
                                item.source):
                            calc = await CalculationDirectory.from_calculation(
                                    item.source, loglevel=self._loglevel)
                            self.calculations.append({
                                "calculation_directory": calc,
                                "is_symlink": True,
                                "path": item.path,  # path is the symlink path
                                }
                                )
                except ValueError:
                    # broken symlink we don't use for building a tree but still
                    # log in debug
                    self._logger.debug(f"Symlink '{item.path}' broken?")
                    continue
            elif isinstance(item, CalculationTree):
                await item.build_tree(follow_symlinks=follow_symlinks)
                self.calculations += item.calculations  # concatenate trees

    def sort_tree(self):
        """Sort calculation tree according to calculation name."""
        if not len(self.calculations):
            # trivial sorting haha
            return
            # raise RuntimeError("Calculation tree not built yet.")
        # sort calculations within each subdirs separately
        # first build calcs_per_subdir blocks
        block_names = []
        for calc in self.calculations:
            dirname = os.path.dirname(calc["path"])
            if dirname not in block_names:
                block_names.append(dirname)
        blocks = {}
        for block_name in block_names:
            blocks.setdefault(block_name, [])
            for calc in self.calculations:
                if os.path.dirname(calc["path"]) == block_name:
                    blocks[block_name].append(calc)
        # sort every block
        newblocks = {}
        for block_name, block in blocks.items():
            calcnames = [os.path.basename(x["path"]) for x in block]
            calcnames = sorted(calcnames)
            newblock = []
            for calcname in calcnames:
                for calc in block:
                    if os.path.basename(calc["path"]) == calcname:
                        newblock.append(calc)
                        break
            newblocks[block_name] = newblock
        # reset self.calculations with sorted calculations
        newcalcs = []
        for block in newblocks.values():
            newcalcs += block
        if len(newcalcs) != len(self.calculations):
            self._logger.error(
                f"Number of ordered calculations = {len(newcalcs)}\n "
                f"Number of calculations = {len(self.calculations)}")
            self._logger.error(
                "Ordered calculations: "
                f"{[x.basename for x in newcalcs]}")
            self._logger.error(
                "Calculations: "
                f"{[x.basename for x in self.calculations]}")
            raise LookupError("Something happened :(")
        if len(newcalcs) != len(self.calculations):
            raise LookupError(
                    "Sorted tree don't match number of calculations.")
        self.calculations = newcalcs

    async def _add_subdir_to_content(self, content, path):
        if await CalculationDirectory.is_calculation_directory(path):
            self._logger.debug(f"Found a calculation dir at: {path}")
            calc = await CalculationDirectory.from_calculation(
                    path, loglevel=self._loglevel)
            # self.calculations.append(calc)
            content.append(calc)
            return
        # else add a new Sub calculation tree (just call super())
        await super()._add_subdir_to_content(content, path)
