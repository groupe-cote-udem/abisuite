from .subparsers.anaddb_log_subparsers import (
        AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser,
        AbinitAnaddbLogTimingSubparser,
        )
from ..bases import BaseLogParser
from ...file_structures import AbinitAnaddbLogStructure


class AbinitAnaddbLogParser(BaseLogParser):
    """Log parser class for the anaddb script."""

    _loggername = "AbinitAnaddbLogParser"
    _structure_class = AbinitAnaddbLogStructure
    _subparsers = (
        AbinitAnaddbLogTimingSubparser,
        AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser,
        )
