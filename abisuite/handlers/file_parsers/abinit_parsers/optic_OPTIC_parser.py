import numpy as np

from .bases import BaseAbinitNCParser
from ...file_structures import AbinitOpticOPTICStructure


class AbinitOpticOPTICParser(BaseAbinitNCParser):
    """Parser class for OPTIC.nc files."""

    _loggername = "AbinitOpticOPTICParser"
    _structure_class = AbinitOpticOPTICStructure

    def _extract_data_from_netcdf_dataset(self, dtset):
        np_keys = [
                "linopt_matrix_elements", "linopt_renorm_eigs",
                "linopt_occupations", "linopt_epsilon",
                "linopt_components", "linopt_wkpts",
                ]
        return {key: np.array(dtset[key]) for key in np_keys}
