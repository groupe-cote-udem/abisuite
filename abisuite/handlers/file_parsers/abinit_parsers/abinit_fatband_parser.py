import os

import aiofiles
import aiofiles.os

import numpy as np

from ..bases import BaseCalculationFileParser
from ...file_structures import AbinitFatbandStructure


class AbinitFatbandParser(BaseCalculationFileParser):
    """Parser class for an abinit _FATBANDS_* file."""

    _expected_ending = ""
    _loggername = "FatbandParser"
    _structure_class = AbinitFatbandStructure

    async def _extract_data(self):
        async with aiofiles.open(self.path, "r") as f:
            lines = await f.readlines()
        data = np.loadtxt(lines, comments=["#", "@", "&"])
        # count number of bands and rechape data
        nbands = 0
        for line in lines:
            if "# BAND" in line:
                nbands += 1
        data = np.reshape(
                data, (nbands, data.shape[0] // nbands, data.shape[1]))
        nkpts = len(data[0])
        characters = data[:, :, 2]
        eigs = data[:, :, 1]
        self._set_attributes_from_data(
                {"nbands": nbands, "nkpts": nkpts, "characters": characters,
                 "eigenvalues": eigs})

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # FATBANDS files are located in the output data directory
        # there might be a lot of fatbands file.
        files = {}
        for path in await aiofiles.os.listdir(meta.output_data_dir):
            if "_FATBANDS_" not in path:
                continue
            key = path.split("_FATBANDS_")[-1]
            files[key] = os.path.join(meta.output_data_dir, path)
        if not files:
            raise FileNotFoundError("No FATBANDS files found.")
        return files
