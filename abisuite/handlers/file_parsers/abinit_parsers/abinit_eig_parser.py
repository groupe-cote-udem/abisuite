import numpy as np

from .bases import BaseAbinitNCParser
from .subparsers import EIGSubParser
from ...file_structures import AbinitEIGStructure


class AbinitEIGParser(BaseAbinitNCParser):
    """Parser class for an abinit EIG file."""

    _expected_ending = "_EIG"
    _loggername = "AbinitEIGParser"
    _structure_class = AbinitEIGStructure

    def _extract_data_from_lines(self, lines):
        # use EIGSubParser to get eigenvalues
        subparser = EIGSubParser(lines, loglevel=self._loglevel)
        data = subparser.data
        # first line looks like this usually:
        # Eigenvalues (   eV  ) for nkpt= 108  k points
        # extract units from it
        units = lines[0].split("(")[1].split(")")[0].strip()
        data["units"] = units
        return data

    def _extract_data_from_netcdf_dataset(self, dtset):
        return {
                "eigenvalues": np.array(dtset.variables["Eigenvalues"])[0],
                "k_points": np.array(dtset.variables["Kptns"]),
                "units": None,
                }
