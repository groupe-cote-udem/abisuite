from ...bases import BaseSubParser
from .....routines import decompose_line


class AbinitLogErrorsSubParser(BaseSubParser):
    """Log subparser that parses error messages."""

    can_be_recalled = True
    recalled_behavior = "append"
    not_mandatory = True
    trigger = ("--- !DilatmxError", )
    subject = "errors"
    _loggername = "AbinitLogErrorsSubParser"

    def _extract_data_from_lines(self, lines):
        # an example:
        # --- !DilatmxError
        # src_file: m_precpred_1geo.F90
        # src_line: 248
        # mpi_rank: 0
        # message: |
        #     Dilatmx has been exceeded too many times (4)
        #     See the description of dilatmx and chkdilatmx input variables.
        #     Action: either first do a calculation with chkdilatmx=0, or
        #     restart your calculation with a larger dilatmx, or larger lattice
        #     vectors.
        #     Warning: With chkdilatmx = 0 the final computation of lattice
        #     parameters might be inaccurate.
        # ...
        data = {}
        for iline, line in enumerate(lines):
            if "--- !" in line:
                data["name"] = line.replace("--- !", "").strip("\n")
                continue
            if "..." in line:
                self.ending_relative_index = iline
                return data
            if "src_file:" in line:
                s, i, f = decompose_line(line)
                data["src_file"] = s[-1]
                continue
            if "src_line:" in line:
                s, i, f = decompose_line(line)
                data["src_line"] = i[0]
                continue
            if "mpi_rank:" in line:
                s, i, f = decompose_line(line)
                data["mpi_rank"] = i[0]
                continue
            if "message" in line:
                data["message"] = self._parse_error_message(lines[iline:])
                continue
        raise RuntimeError("EOF when parsing error data")

    def _parse_error_message(self, lines):
        msg_lines = []
        for line in lines:
            if "..." in line:
                return " ".join(msg_lines)
            msg_lines.append(line.strip().strip("\n"))
        raise RuntimeError("EOF while parsing error message")


class AbinitLogIrreduciblePerturbationsSubParser(BaseSubParser):
    """Log subparser for the irreps printed in a DFPT calculation."""

    trigger = "--- !IrredPerts"
    subject = "irreducible_perturbations"
    _loggername = "AbinitDtsetIrreduciblePerturbationsSubParser"

    def _extract_data_from_lines(self, lines):
        # data look like this:
        # --- !IrredPerts
        # # List of irreducible perturbations
        # irred_perts:
        #    - qpt: [   0.5000000000000000,   0.1666666670000000,   0.0000000000000000]  # noqa
        #      ipert: 1
        #      idir: 1
        #    - qpt: [   0.5000000000000000,   0.1666666670000000,   0.0000000000000000]  # noqa
        #      ipert: 1
        #      idir: 2
        #    - qpt: [   0.5000000000000000,   0.1666666670000000,   0.0000000000000000]  # noqa
        #      ipert: 1
        #      idir: 3
        # ...
        irreps = []
        skip = 0
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if "..." in line:
                self.ending_relative_index = iline
                return irreps
            if "qpt" in line:
                irrep, rel_skip = self._extract_irrep(lines[iline:])
                skip = iline + rel_skip
                irreps.append(irrep)
        raise LookupError("Could not find end of irreps.")

    def _extract_irrep(self, lines):
        # an irrep data block looks like this:
        #    - qpt: [   0.5000000000000000,   0.1666666670000000,   0.0000000000000000]  # noqa
        #      ipert: 1
        #      idir: 3
        data = {}
        rel_skip = 2
        data["qpt"] = [
                float(x)
                for x in lines[0].split("[")[-1].split("]")[0].split(",")]
        data["iatpol"] = int(lines[1].split(":")[-1])
        data["idir"] = int(lines[2].split(":")[-1])
        return data, rel_skip


class AbinitLogOccSubParser(BaseSubParser):
    """Subparser that gets the occupations from the log file."""

    subject = "occ"
    trigger = "chkneu: initialized the occupation numbers"
    _loggername = "AbinitLogOccSubParser"

    def _extract_data_from_lines(self, lines):
        # data looks like this
        # chkneu: initialized the occupation numbers for occopt= 1, spin-unpolarized or antiferromagnetic case:  # noqa: E501
        # 2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  2.00  0.00  0.00  # noqa: E501
        # 0.00  0.00
        occ = []
        for iline, line in enumerate(lines[1:]):
            s, i, f = decompose_line(line)
            if s:
                # end here
                self.ending_relative_index = iline + 1
                return occ
            occ += f
        # if we here reached the end of the file...
        raise LookupError("Occupations...")


class AbinitLogWalltimeSubParser(BaseSubParser):
    """Subparser for an Abinit log file (or out file) to get the walltime."""

    subject = "walltime"
    trigger = ("Proc.   0 individual time (sec):", "abinit_abort:", )
    _loggername = "AbinitLogWalltimeSubParer"

    def _extract_data_from_lines(self, lines):
        self.ending_relative_index = 0
        if "abinit_abort:" in lines[0]:
            # aborted without any walltime info
            return None
        s, i, f = decompose_line(lines[0])
        return f[-1]
