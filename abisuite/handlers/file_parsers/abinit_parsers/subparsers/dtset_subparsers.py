from typing import Any, Sequence

import numpy as np

from .anaddb_log_subparsers import (
        AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser,
        )
from ...bases import BaseSubParser
from .....routines import decompose_line


class BaseAbinitDtsetSubParser(BaseSubParser):
    """Base class for abinit dtset subparsers.

    Since all the subparsers are maybe not relevent
    depending on the type of calculation, all of them
    are not mandatory info.
    """

    not_mandatory = True


class AbinitDtsetRelaxationConvergenceSubParser(BaseSubParser):
    """Dtset subparser for the relaxation convergence data."""

    trigger = "Etot       = :"
    can_be_recalled = True
    recalled_behavior = "append"
    subject = "relaxation_convergence"
    _loggername = "AbinitDtsetRelaxationConvergenceSubParser"

    def _extract_data_from_lines(self, lines: Sequence[str]) -> dict[str, Any]:
        data = {}
        self.ending_relative_index = 0  # since it can be recalled don't skip
        stoppers = (
                "--- !DilatmxError",  # if this is there, return what we have
                "--- Iteration:",
                "----iterations are completed",
                )
        for iline, line in enumerate(lines):
            for stopper in stoppers:
                if stopper in line:
                    return data
            if "Cartesian components of stress tensor (hartree" in line:
                data["stress_tensor"] = self._get_stress_tensor(
                        lines[iline + 1: iline + 4])
                continue
            if "Total energy " in line:
                s, i, f = decompose_line(line)
                data["etotal"] = f[0]
                continue
            if "Cartesian forces" in line:
                data["forces"] = self._get_forces(lines[iline + 1:])
                continue
        raise LookupError("EOF while parsing relaxation convergence data.")

    def _get_stress_tensor(self, lines: Sequence[str]) -> np.ndarray:
        s, i, f = decompose_line(lines[0])
        t00, t12 = f[0], f[1]
        s, i, f = decompose_line(lines[1])
        t11, t02 = f[0], f[1]
        s, i, f = decompose_line(lines[2])
        t22, t01 = f[0], f[1]
        return np.array([[t00, t01, t02], [t01, t11, t12], [t02, t12, t22]])

    def _get_forces(self, lines: Sequence[str]) -> np.ndarray:
        # just determine the end and call numpy to get forces array
        end = None
        for iline, line in enumerate(lines):
            if "Gradient" in line:
                end = iline
                break
        else:
            raise LookupError(
                    "EOF while parsing forces for relaxation convergence.")
        return np.genfromtxt(lines[:end])


class AbinitDtsetEtotConvergenceSubParser(BaseSubParser):
    """Dtset subparser for the etot convergence data."""

    trigger = "ITER STEP NUMBER"
    subject = "etot_convergence"
    can_be_recalled = True
    recalled_behavior = "append"
    _loggername = "AbinitDtsetEtotConvergenceSubParser"

    def _extract_data_from_lines(self, lines: Sequence[str]) -> dict[str, Any]:
        data = {}
        s, i, f = decompose_line(lines[0])
        data["iter_step_number"] = i[0]
        self.ending_relative_index = 0
        # don't skip anything in case we do relaxation
        for line in lines[1:]:
            if "ETOT" in line:
                s, i, f = decompose_line(line)
                data["etot"] = f[0]
                data["deltae(h)"] = f[1]
                data["residm"] = f[2]
                data["vres2"] = f[3]
                continue
            if self.trigger in line:
                return data
            if "At SCF step" in line:
                # converged
                return data
            if "was not enough SCF cycles to converge":
                # not converged
                return data
        raise LookupError("EOF while parsing etot convergence data.")


class AbinitDtsetFermiEnergySubParser(BaseAbinitDtsetSubParser):
    """Dtset subparser for the fermi level."""

    trigger = "fermie"
    subject = "fermi_energy"
    _loggername = "AbinitDtsetFermiEnergySubParser"

    def _extract_data_from_lines(self, lines):
        # fermie    :   2.06955281E-01
        self.ending_relative_index = 0
        s, i, f = decompose_line(lines[0])
        return f[0]


class AbinitDtsetPhononFrequenciesSubParser(BaseAbinitDtsetSubParser):
    """Subparser for phonon frequencies."""

    trigger = "phonon wavelength (reduced coordinates)"
    subject = "phonon_frequencies"
    _loggername = "AbinitDtsetPhononFrequenciesSubParser"

    def _extract_data_from_lines(self, lines):
        # lines looks exactly like for anaddb phonon frequencies
        # phonon wavelength (reduced coordinates) , norm, and energies in hartree              # noqa: E501
        # 0.00 0.00 0.00 1.00
        # 2.542183335E-06  2.542183695E-06  2.542184774E-06  1.568559216E-03  1.568559216E-03  # noqa: E501
        # 1.568559216E-03
        # FIXME: this is not really beautiful haha
        cls = AbinitAnaddbLogNonAnalyticalGammaCorrectionsSubParser
        return cls._extract_data_from_lines(self, lines)


class AbinitDtsetGWSelfEnergySubParser(BaseAbinitDtsetSubParser):
    """Subparser for GW self energy data."""

    trigger = "--- !SelfEnergy_ee"
    subject = "gw_self_energy"
    _loggername = "AbinitDtsetGWSelfEnergySubParser"

    def _extract_data_from_lines(self, lines):
        # --- !SelfEnergy_ee
        # Iteration_state: {dtset: 1, }
        # Kpoint     : [   0.000,    0.000,    0.000, ]
        # Spin       : 1
        # KS_gap     :    2.521
        # QP_gap     :    3.162
        # Delta_QP_KS:    0.641
        # Data: !SigmaeeData |
        #      Band     E0 <VxcDFT>   SigX SigC(E0)      Z dSigC/dE  Sig(E)    E-E0       E  # noqa: E501
        #         2   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         3   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         4   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         5   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        #         6   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        #         7   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        # ...
        data = {}
        skip = 0
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if "Kpoint" in line:
                data["Kpoint"] = [
                        float(x) for x in line.split("[")[-1].split(",")[:-1]]
                continue
            if "Spin" in line:
                s, i, f = decompose_line(line)
                data["Spin"] = i[0]
                continue
            if "KS_gap" in line:
                s, i, f = decompose_line(line)
                data["KS_gap"] = f[0]
                continue
            if "QP_gap" in line:
                s, i, f = decompose_line(line)
                data["QP_gap"] = f[0]
                continue
            if "Delta_QP_KS" in line:
                s, i, f = decompose_line(line)
                data["Delta_QP_KS"] = f[0]
                continue
            if "!SigmaeeData" in line:
                data["Sigma_ee_data"], relskip = self._extract_sigma_ee_data(
                        lines[iline:])
                skip = iline + relskip
                continue
            if "..." in line:
                self.ending_relative_index = iline
                return data

    def _extract_sigma_ee_data(self, lines):
        # Data: !SigmaeeData |
        #      Band     E0 <VxcDFT>   SigX SigC(E0)      Z dSigC/dE  Sig(E)    E-E0       E  # noqa: E501
        #         2   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         3   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         4   4.763 -11.251 -13.229   1.239   0.764  -0.308 -11.816  -0.565   4.198  # noqa: E501
        #         5   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        #         6   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        #         7   7.284 -10.033  -5.552  -4.382   0.765  -0.308  -9.958   0.076   7.360  # noqa: E501
        # ...
        data = []
        for iline, line in enumerate(lines[2:]):
            if "..." in line:
                return data, iline + 2
            s, i, f = decompose_line(line)
            data.append({
                "band": i[0], "E0": f[0], "<VxcDFT>": f[1], "SigX": f[2],
                "SigC(E0)": f[3], "Z": f[4], "dSigC/dE": f[5], "Sig(E)": f[6],
                "E-E0": f[7], "E": f[8],
                })
        raise LookupError("Reached EOF while parsing sigma_ee_data.")
