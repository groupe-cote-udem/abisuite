from .dtset_subparsers import (
        AbinitDtsetEtotConvergenceSubParser,
        AbinitDtsetFermiEnergySubParser,
        AbinitDtsetGWSelfEnergySubParser,
        AbinitDtsetPhononFrequenciesSubParser,
        AbinitDtsetRelaxationConvergenceSubParser,
        )
from .eig_subparser import EIGSubParser
from ...bases import (
        BaseParserWithSubParsers,
        BaseSubParser,
        )
from ....file_structures import GenericStructure


# TODO: I felt long time ago that this was a good idea to make a subparser with
# subparsers. Perhaps this would need to be cleaned out to make it more like
# the other subparsers. FG 03/08/2020
class DtsetSubParser(BaseSubParser, BaseParserWithSubParsers):
    """Class that parses a dtset from an abinit output file."""

    can_be_recalled = True
    recalled_behavior = "append"
    subject = "dtsets"
    trigger = "== DATASET"
    _expected_ending = ""  # need to do this because of multiple inheritance
    _loggername = "DtsetSubParser"
    _subparsers = (
            AbinitDtsetEtotConvergenceSubParser,
            EIGSubParser,
            AbinitDtsetFermiEnergySubParser,
            AbinitDtsetGWSelfEnergySubParser,
            AbinitDtsetPhononFrequenciesSubParser,
            AbinitDtsetRelaxationConvergenceSubParser,
            )
    _structure_class = GenericStructure  # since not directly a file

    def __init__(self, *args, **kwargs):
        BaseParserWithSubParsers.__init__(self, *args, **kwargs)
        BaseSubParser.__init__(self, *args, **kwargs)

    def _extract_data_from_lines(self, lines):
        # self._logger.debug("=== Parsing DATASET ===")
        if self.trigger in lines[0]:
            lines = lines[1:]
        data = BaseParserWithSubParsers._extract_data_from_lines(self, lines)
        if self._ending_relative_index is None:
            self._logger.warning("Could not get all dtset data.")
            raise LookupError()
        return data

    def _stop_parsing_at(self, line, index):
        stoppers = (
                self.trigger,
                "--- !DilatmxError",
                "== END DATASET(S)",
                )
        for stopper in stoppers:
            if stopper in line:
                # dataset ends here
                self._ending_relative_index = index - 1
                return True
        return False

    @classmethod
    def from_string(cls, string, **kwargs):
        """Parses a dtset from a single string instead of a list of lines."""
        lines = string.split("\n")
        return cls(lines, **kwargs)

    @classmethod
    async def _filepath_from_meta(cls, *args, **kwargs):
        # irrelevent here
        raise IOError("This parser cannot be instanciated with a meta file.")
