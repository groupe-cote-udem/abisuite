import os

import aiofiles.os

import numpy as np

from .bases import BaseAbinitFormattedWithHeaderCalculationFileParser
from ...file_structures import AbinitBSEMDFStructure
from ....routines import decompose_line


class AbinitBSEMDFParser(
        BaseAbinitFormattedWithHeaderCalculationFileParser):
    """Parser for an abinit BSE MDF file.

    This parser class supports EXC, RPA_NLF and GW_NLF files.
    """

    _expected_ending = "_MDF"
    _loggername = "AbinitBSEMDFParser"
    _structure_class = AbinitBSEMDFStructure

    @classmethod
    async def _filepath_from_meta(cls, meta):
        # file is located inside output data dir
        # there are many MDF files, return all of them
        files = {}
        async with meta:
            odd = meta.output_data_dir
        for filename in await aiofiles.os.listdir(odd):
            if filename.endswith(cls._expected_ending):
                for suffix in ("EXC", "GW_NLF", "RPA_NLF"):
                    if suffix in filename:
                        files[suffix.lower()] = os.path.join(odd, filename)
        if len(files) != 3:
            raise FileNotFoundError(f"No MDF files found in '{odd}'.")
        return files

    def _get_core_data(self, lines):
        data = np.loadtxt(lines)
        # first 3 q vectors are along primitive lattice vectors
        # last 3 are parallel to reciprocal lattice vectors
        epsilon1 = data[:, 1] + 1.0j * data[:, 2]
        epsilon2 = data[:, 3] + 1.0j * data[:, 4]
        epsilon3 = data[:, 5] + 1.0j * data[:, 6]
        epsilon4 = data[:, 7] + 1.0j * data[:, 8]
        epsilon5 = data[:, 9] + 1.0j * data[:, 10]
        epsilon6 = data[:, 11] + 1.0j * data[:, 12]
        toreturn = {
                "frequencies": data[:, 0],
                # this array is not really relevent
                # TODO: rearrange into a simple list
                "epsilon": np.array(
                    [[epsilon1, epsilon6, epsilon5],
                     [epsilon6, epsilon2, epsilon4],
                     [epsilon5, epsilon4, epsilon3]]),
                }
        if data.shape[-1] > 13:
            # dos and idos are present
            toreturn["dos"] = data[:, 13]
            toreturn["idos"] = data[:, 14]
        return toreturn

    def _get_header_from_lines(self, lines):
        # Macroscopic dielectric function obtained with the BS equation.
        #  RPA L0 with KS energies and KS wavefunctions     LOCAL FIELD EFFECTS INCLUDED  # noqa: E501
        # RESONANT-ONLY calculation
        # Coulomb term constructed with full W(G1,G2)
        # Scissor operator energy = 21.7691 [eV]
        # Tolerance =  0.0500 0.0000  <- only for bs_algorithm = 2
        # nstates included in the diagonalization = 1  <- only for bs_algorithm = 1  # noqa: E501
        # npweps  = 27
        # npwwfn  = 531
        # nbands  = 15
        # loband  = 2
        # nkibz   = 64
        # nkbz    = 64
        # Lorentzian broadening =  0.1500 [eV]
        # List of q-points for the optical limit:
        # q =  0.938821, 0.000000, 0.000000, [Reduced coords]
        # q =  0.000000, 0.938821, 0.000000, [Reduced coords]
        # q =  0.000000, 0.000000, 0.938821, [Reduced coords]
        # q =  0.000000, 0.813043, 0.813043, [Reduced coords]
        # q =  0.813043, 0.000000, 0.813043, [Reduced coords]
        # q =  0.813043, 0.813043, 0.000000, [Reduced coords]
        # omega [eV]    RE(eps(q=1)) IM(eps(q=1) RE(eps(q=2) ) ...
        data = {"qpoints": []}
        for line in lines:
            if not line.startswith("#"):
                # rearrange qpoints before returning it
                # rearrange into a 3x3x3 tensor (matching epsilon array)
                qpt1 = data["qpoints"][0]
                qpt2 = data["qpoints"][1]
                qpt3 = data["qpoints"][2]
                qpt4 = data["qpoints"][3]
                qpt5 = data["qpoints"][4]
                qpt6 = data["qpoints"][5]
                data["qpoints"] = np.array(
                        [[qpt1, qpt6, qpt5],
                         [qpt6, qpt2, qpt4],
                         [qpt5, qpt4, qpt3]])
                return data
            if "q =" in line:
                s, i, f = decompose_line(line.replace(",", " "))
                data["qpoints"].append(f)
                continue
            if "broadening" in line:
                s, i, f = decompose_line(line)
                data["broadening"] = f[0]
            if "nkbz" in line:
                s, i, f = decompose_line(line)
                data["nkbz"] = i[0]
            if "nkibz" in line:
                s, i, f = decompose_line(line)
                data["nkibz"] = i[0]
            if "loband" in line:
                s, i, f = decompose_line(line)
                data["loband"] = i[0]
            if "nbands" in line:
                s, i, f = decompose_line(line)
                data["nbands"] = i[0]
                continue
            if "nstates" in line:
                s, i, f = decompose_line(line)
                data["bs_nstates"] = i[0]
            if "npwwfn" in line:
                s, i, f = decompose_line(line)
                data["npwwfn"] = i[0]
                continue
            if "Scissor" in line:
                s, i, f = decompose_line(line)
                data["scissor_shift"] = f[0]
            if "Tolerance" in line:
                s, i, f = decompose_line(line)
                data["haydock_tolerance"] = f
                continue
            if "npweps" in line:
                s, i, f = decompose_line(line)
                data["npweps"] = i[0]
        raise LookupError("Unexpected EOF while parsing header.")
