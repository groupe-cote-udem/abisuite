import json

import aiofiles

from .bases import BaseCalculationFileParser
from ..file_structures import MetaDataStructure
from ...utils import AbisuiteJSONDecoder


class MetaDataParser(BaseCalculationFileParser):
    """Parser for a Meta data file."""

    _loggername = "MetaDataParser"
    _expected_ending = ".meta"
    _structure_class = MetaDataStructure

    async def _extract_data(self):
        try:
            async with aiofiles.open(self.path, "r") as f:
                content = await f.read()
                data = json.loads(content, cls=AbisuiteJSONDecoder)
        except NameError as e:
            self._logger.exception(
                    "Could not open meta data file for some reason: "
                    f"'{self.path}'.")
            raise e
        except json.JSONDecodeError:
            self._logger.error(f"Can't read meta data file: {self.path}")
            self._logger.error("Check path or file integrity.")
            raise
        if not isinstance(data, dict):
            raise TypeError(f"Expected a dict but read '{data}'")
        for k, v in data.items():
            if k not in self.structure.all_attributes:
                self._logger.info(f"Unexpected data key extracter: '{k}'")
                # legacy conversions
                if k == "submission_time":
                    self.submit_time = v
                continue
            setattr(self, k, v)

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # trivial method
        return meta.path
