from .bases import BaseQELogTimingSubParser


class QEMatdynLogTimingSubParser(BaseQELogTimingSubParser):
    """Timing subparser for a matdyn log file."""

    _loggername = "QEMatdynLogTimingSubParser"
    trigger = "MATDYN       :"
