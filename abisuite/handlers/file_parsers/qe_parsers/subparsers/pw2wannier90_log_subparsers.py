from .bases import BaseQELogTimingSubParser


class QEPW2Wannier90LogTimingSubParser(BaseQELogTimingSubParser):
    """Timing subparser for a pw2wannier90 log file."""

    _loggername = "QEPW2Wannier90LogTimingSubParser"
    trigger = "PW2WANNIER   :"
