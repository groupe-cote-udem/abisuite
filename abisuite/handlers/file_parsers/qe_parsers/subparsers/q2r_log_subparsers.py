from .bases import BaseQELogTimingSubParser


class QEQ2RLogTimingSubParser(BaseQELogTimingSubParser):
    """Timing subparser for a q2r log file."""

    _loggername = "QEQ2RLogTimingSubParser"
    trigger = "Q2R          :"
