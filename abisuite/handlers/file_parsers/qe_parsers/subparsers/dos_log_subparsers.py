from .bases import BaseQELogTimingSubParser


class QEDOSLogTimingSubParser(BaseQELogTimingSubParser):
    """Timing subparser for a dos log file."""

    _loggername = "QEDOSLogTimingSubParser"
    trigger = "DOS          :"
