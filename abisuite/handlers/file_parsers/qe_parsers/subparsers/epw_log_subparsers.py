from typing import Any, Mapping, Sequence

import numpy as np

from .bases import BaseQELogTimingSubParser
from ...bases import BaseSubParser
from .....exceptions import DevError
from .....routines import decompose_line


class QEEPWLogAdaptativeSmearingSubParser(BaseSubParser):
    """Adaptative smearing sub parser in epw log files."""

    can_be_recalled = True
    subject = "adaptative_smearings"
    recalled_behavior = "append"
    trigger = "Adaptative smearing el-phonon"
    _loggername = "QEEPWLogAdaptativeSmearingSubParser"

    def _extract_data_from_lines(
            self, lines: Sequence[str]) -> Mapping[str, Any]:
        """Extract adaptative smearing from lines starting at trigger."""
        # exemple:
        # Adaptative smearing el-phonon = Min:     1.414214 meV
        #                                 Max:   120.677922 meV
        s, i, f = decompose_line(lines[0])
        self.ending_relative_index = 1
        if not f:
            data = {"min": np.nan}
        else:
            data = {"min": f[0]}
        s, i, f = decompose_line(lines[1])
        if not f:
            data["max"] = np.nan
        else:
            data["max"] = f[0]
        return data


class QEEPWLogTimingSubParser(BaseQELogTimingSubParser):
    """Timing subparser for an epw log file."""

    can_be_recalled = True  # only get the last one
    recalled_behavior = "overwrite"
    trigger = "EPW          :"
    _loggername = "QEEPWLogTimingSubParser"


class QEEPWLogGkkSubParser(BaseSubParser):
    """Subparser class for gkk matrix elements printed out in log file."""

    can_be_recalled = False
    subject = "gkk"
    trigger = "Electron-phonon vertex |g| (meV)"
    recalled_behavior = "append"
    _loggername = "QEEPWLogGkkSubParser"

    def _extract_data_from_lines(
            self, lines: Sequence[str]) -> Mapping[str, Any]:
        """Extract data.

        #          Electron-phonon vertex |g| (meV)
        #
        # iq =    5200 coord.:    0.0750000   0.2250000   0.9750000
        # ik =       1 coord.:    0.0000000   0.0000000   0.0000000
        #  ibnd     jbnd     imode   enk[eV]    enk+q[eV]  omega(q)[meV]   |g|[meV]
        # ------------------------------------------------------------------------------
        #    2        2        1     19.1383     13.6146        2.9856698546    0.0000000000E+00
        #    2        2        2     19.1383     13.6146        3.4418954776    0.0000000000E+00
        #    2        2        3     19.1383     13.6146        6.8521530677    0.0000000000E+00
        #    2        3        1     19.1383     16.9534        2.9856698546    0.0000000000E+00
        #    2        3        2     19.1383     16.9534        3.4418954776    0.0000000000E+00
        #    2        3        3     19.1383     16.9534        6.8521530677    0.0000000000E+00
        #    2        4        1     19.1383     18.0409        2.9856698546    0.0000000000E+00
        #    2        4        2     19.1383     18.0409        3.4418954776    0.0000000000E+00
        #    2        4        3     19.1383     18.0409        6.8521530677    0.0000000000E+00
        #    3        2        1     19.1383     13.6146        2.9856698546    0.0000000000E+00
        #    3        2        2     19.1383     13.6146        3.4418954776    0.0000000000E+00
        #    3        2        3     19.1383     13.6146        6.8521530677    0.0000000000E+00
        #    3        3        1     19.1383     16.9534        2.9856698546    0.0000000000E+00
        #    3        3        2     19.1383     16.9534        3.4418954776    0.0000000000E+00
        #    3        3        3     19.1383     16.9534        6.8521530677    0.0000000000E+00
        #    3        4        1     19.1383     18.0409        2.9856698546    0.0000000000E+00
        #    3        4        2     19.1383     18.0409        3.4418954776    0.0000000000E+00
        #    3        4        3     19.1383     18.0409        6.8521530677    0.0000000000E+00
        #    4        2        1     19.1383     13.6146        2.9856698546    0.0000000000E+00
        #    4        2        2     19.1383     13.6146        3.4418954776    0.0000000000E+00
        #    4        2        3     19.1383     13.6146        6.8521530677    0.0000000000E+00
        #    4        3        1     19.1383     16.9534        2.9856698546    0.0000000000E+00
        #    4        3        2     19.1383     16.9534        3.4418954776    0.0000000000E+00
        #    4        3        3     19.1383     16.9534        6.8521530677    0.0000000000E+00
        #    4        4        1     19.1383     18.0409        2.9856698546    0.0000000000E+00
        #    4        4        2     19.1383     18.0409        3.4418954776    0.0000000000E+00
        #    4        4        3     19.1383     18.0409        6.8521530677    0.0000000000E+00
        # ------------------------------------------------------------------------------
        #         ik =       2 coord.:    0.0000000   0.0000000   0.0250000
        #  ibnd     jbnd     imode   enk[eV]    enk+q[eV]  omega(q)[meV]   |g|[meV]
        # ------------------------------------------------------------------------------
        #    2        2        1     18.9699     13.8672        2.9856698546    0.0000000000E+00
        #    2        2        2     18.9699     13.8672        3.4418954776    0.0000000000E+00
        #    2        2        3     18.9699     13.8672        6.8521530677    0.0000000000E+00
        #    2        3        1     18.9699     17.4307        2.9856698546    0.0000000000E+00
        #    2        3        2     18.9699     17.4307        3.4418954776    0.0000000000E+00
        #    2        3        3     18.9699     17.4307        6.8521530677    0.0000000000E+00
        #    2        4        1     18.9699     18.0748        2.9856698546    0.0000000000E+00
        #    2        4        2     18.9699     18.0748        3.4418954776    0.0000000000E+00
        #    2        4        3     18.9699     18.0748        6.8521530677    0.0000000000E+00
        #    3        2        1     19.1216     13.8672        2.9856698546    0.0000000000E+00
        #    3        2        2     19.1216     13.8672        3.4418954776    0.0000000000E+00
        #    3        2        3     19.1216     13.8672        6.8521530677    0.0000000000E+00
        #    3        3        1     19.1216     17.4307        2.9856698546    0.0000000000E+00
        #    3        3        2     19.1216     17.4307        3.4418954776    0.0000000000E+00
        #    3        3        3     19.1216     17.4307        6.8521530677    0.0000000000E+00
        #    3        4        1     19.1216     18.0748        2.9856698546    0.0000000000E+00
        #    3        4        2     19.1216     18.0748        3.4418954776    0.0000000000E+00
        #    3        4        3     19.1216     18.0748        6.8521530677    0.0000000000E+00
        #    4        2        1     19.1216     13.8672        2.9856698546    0.0000000000E+00
        #    4        2        2     19.1216     13.8672        3.4418954776    0.0000000000E+00
        #    4        2        3     19.1216     13.8672        6.8521530677    0.0000000000E+00
        #    4        3        1     19.1216     17.4307        2.9856698546    0.0000000000E+00
        #    4        3        2     19.1216     17.4307        3.4418954776    0.0000000000E+00
        #    4        3        3     19.1216     17.4307        6.8521530677    0.0000000000E+00
        #    4        4        1     19.1216     18.0748        2.9856698546    0.0000000000E+00
        #    4        4        2     19.1216     18.0748        3.4418954776    0.0000000000E+00
        #    4        4        3     19.1216     18.0748        6.8521530677    0.0000000000E+00
        # ------------------------------------------------------------------------------
        """  # noqa: E501, D400, RST305, RST219
        data = []
        skip = 0
        # data is organized into qpt blocks. read 1 qpt at a time
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if self._trigger in line:
                qdata, subskip = self._extract_data_this_qpt(lines[iline + 1:])
                data.append(qdata)
                skip = iline + subskip + 1
                continue
        self.ending_relative_index = skip
        return self._reformat_data(data)

    def _extract_data_this_qpt(self, lines):
        # each qpt block is organized in kpt subblocks
        # read 1 kpt at a time
        skip = 0
        data = {}
        for iline, line in enumerate(lines):
            if skip > iline:
                continue
            if "iq" in line:
                s, i, f = decompose_line(line)
                data["q"] = np.asarray(f)
                continue
            if "ik" in line:
                data["gkk_data"], subskip = (
                        self._extract_gkk_data(lines[iline:]))
                skip = iline + subskip
            if self._trigger in line:
                return data, iline
        return data, iline

    def _reformat_data(self, data: Mapping[str, Any]) -> Mapping[str, Any]:
        # reshape arrays into single numpy arrays instead of lists of dicts
        newdata = {}
        newdata["nqpts"] = len(data)
        newdata["qpts"] = np.asarray([d["q"] for d in data])  # all qpts coords
        newdata["nkpts"] = len(data[0]["gkk_data"])
        newdata["kpts"] = np.asarray([d["k"] for d in data[0]["gkk_data"]])
        newdata["nbands"] = data[0]["gkk_data"][0]["nbnd"]
        newdata["nmodes"] = data[0]["gkk_data"][0]["nmodes"]
        nbnd = newdata["nbands"]
        nmodes = newdata["nmodes"]
        # same ph freqs for all k+q for a given q -> just read from first one
        # stop at nmodes since they are repeated
        omegaqnu = np.asarray(
                [d["gkk_data"][0]["omegaqnu"][:nmodes]
                 for d in data])
        nqpts = newdata["nqpts"]
        newdata["omegaqnu"] = omegaqnu.reshape((nqpts, nmodes))
        # enks -> same eigs for all k -> just read from first q
        # read every nmodes * nbnd as they are repeated
        enk = np.asarray(
                [d["enk"][::nmodes * nbnd] for d in data[0]["gkk_data"]])
        newdata["enk"] = enk.reshape((newdata["nkpts"], nbnd))
        # emkqs: read every nmodes but stop after nmodes * nbnd
        emkq = np.asarray(
                [d["gkk_data"]["emkq"][:nmodes * nbnd:nmodes] for d in data])
        newdata["emkq"] = emkq.reshape(
                (newdata["nqpts"], newdata["nkpts"], nbnd))
        # read everything
        gkk = np.asarray([d["gkk_data"]["gkk"] for d in data])
        newdata["gkk"] = gkk.reshape(
                (newdata["nqpts"], newdata["nkpts"], nbnd, nbnd, nmodes))
        return {"gkk": newdata}

    def _extract_gkk_data(self, lines: Sequence[str]) -> Mapping[str, Any]:
        skip = 0
        # data is organized in blocks. 1 block = 1kpt
        datas = []
        for iline, line in enumerate(lines):
            if iline < skip:
                continue
            if "ik" in line:
                data, subskip = self._extract_gkk_data_single_k(lines[iline:])
                skip = iline + subskip
                datas.append(data)
                continue
            if self._trigger in line:
                return datas, iline
        return datas, iline

    def _extract_gkk_data_single_k(self, lines):
        # extract kpt coords
        s, i, f = decompose_line(lines[0])
        data = {"k": np.asarray(f), "enk": [], "emkq": [],
                "gkk": [], "omegaqnu": []}
        modes = []
        bands = []
        start_extracting = False

        def _finalize_block(data: Mapping[str, Any]) -> Mapping[str, Any]:
            data["nbnd"] = np.max(bands) - np.min(bands)
            data["nmodes"] = np.max(modes)
            return data

        # extract rest of data for this block
        for iline, line in enumerate(lines):
            if "-------" in line:
                if not start_extracting:
                    start_extracting = True
                    continue
                # we're done
                return _finalize_block(data), iline
            if not start_extracting:
                continue
            s, i, f = decompose_line(line)
            ibnd, nu = i[0], i[2]
            data["enk"].append(f[0])
            data["emkq"].append(f[1])
            data["omegaqnu"].append(f[2])
            data["gkk"].append(f[3])
            modes.append(nu)
            bands.append(ibnd)
        return _finalize_block(data), iline


class BaseQEEPWLogConductivitiesSubParser(BaseSubParser):
    """Base class for conductivities subparsers.

    Essentially does all the job
    and the subclasses just change in the trigger.
    """

    can_be_recalled = False
    _iterative = None

    def _extract_data_from_lines(self, lines):
        if self._iterative is None:
            raise DevError("Neet to set '_iterative'.")
        # data is organised as follows, for each iteration:
        # there is a small difference for metals
        # Iteration number:         1
        #
        # =============================================================================================  # noqa: E501
        #   Temp     Fermi    Population SR                  Conductivity
        #    [K]      [eV]  [carriers per cell]              [V.s.cm]^-1
        # =============================================================================================  # noqa: E501
        #
        #   10.000   0.11645E+02  -0.28422E-13    0.446918E+03   -0.120917E-14    0.169284E-13           # noqa: E501
        #                                        -0.120917E-14    0.446918E+03    0.120917E-14           # noqa: E501
        #                                         0.278109E-13    0.120917E-14    0.446918E+03           # noqa: E501
        #   60.000   0.11642E+02   0.10663E-13    0.557159E+02   -0.151220E-15    0.743184E-16           # noqa: E501
        #                                         0.113360E-15    0.557159E+02    0.113360E-15           # noqa: E501
        #                                        -0.128474E-14    0.147604E-18    0.557159E+02           # noqa: E501
        # [...]
        data = {"holes": [], "electrons": [], "metals": [], "error": []}
        skip = 0
        for index, line in enumerate(lines):
            if index < skip:
                continue
            if "Temp" in line:
                self._logger.debug("Parsing iteration block.")
                iter_data, relskip = self._extract_iteration_block(
                        lines[index:])
                skip = index + relskip
                if "converged" in iter_data:
                    # the IBTE did not converge
                    # stop here
                    self.ending_relative_index = 10
                    return data
                for typ in ("holes", "electrons", "metals", "error"):
                    if typ in iter_data:
                        data[typ].append(iter_data[typ])
                if not self._iterative:
                    # serta -> stop after one block
                    # move forward at least 10 lines
                    self.ending_relative_index = 10
                    return data
                continue
            # continue until end of file (not sure where this block ends).
        self.ending_relative_index = 10  # move forward at least 10 lines
        # because Yoda said so
        return data

    def _extract_iteration_block(self, lines):
        # each iteration block has electrons+holes conductivities
        data = {}
        skip = 0
        for index, line in enumerate(lines):
            if index < skip:
                continue
            if "The iteration reached the maximum but did not converge" in (
                    line):
                # IBTE did not converge for some reason
                data["converged"] = False
                return data, index
            if "Hole conductivity" in line:
                self._logger.debug("Parsing holes conductivities block.")
                hole_data, relend = self._extract_conductivities(lines[
                    index + 3:])
                skip = index + relend + 3
                data["holes"] = hole_data
                continue
            elif "Elec conductivity" in line:
                self._logger.debug("Parsing electrons conductivities block.")
                elec_data, relend = self._extract_conductivities(
                        lines[index + 3:])
                skip = index + relend + 3
                data["electrons"] = elec_data
                continue
            elif "Conductivity" in line:
                # metal
                self._logger.debug("Parsing electrons conductivities block.")
                metal_data, relend = self._extract_conductivities(
                        lines[index + 3:])
                skip = index + relend + 3
                data["metals"] = metal_data
                continue
            elif "Max error" in line:
                self._logger.debug("Parsing error line.")
                s, i, f = decompose_line(line)
                if not f:
                    # error is probably so huge that it is not a float
                    # e.g.: 1.6+156 instead of 1.6E+156
                    data["error"] = np.nan
                else:
                    data["error"] = f[0]
                return data, index + 1
        # if we are here something bad happened
        raise LookupError(
                "Error while parsing EPW log file for a conductivity "
                "iteration block.")

    def _extract_conductivities(self, lines):
        # parses a block of conductivities vs Temperatures
        data = []
        seen_first_line = False
        for j, line in enumerate(lines):
            s, i, f = decompose_line(line)
            if len(f) > 4:
                # we are at the first line
                if len(f) == 7:
                    newdata = {
                            "temperature": f[0],
                            "chemical_potential": f[1],
                            "dos": f[2],
                            "population": f[3],
                            "xx": f[4],
                            "xy": f[5],
                            "xz": f[6]
                            }
                else:
                    # an error occured
                    # sometimes this happens because numbers are too big
                    # and 0.01111111+100 was meant to be 0.01111E+100
                    newdata = {
                            "temperature": f[0],
                            "chemical_potential": f[1],
                            "xx": np.nan, "xy": np.nan, "xz": np.nan}
                seen_first_line = True
                data.append(newdata)
                continue
            elif len(f) == 3:
                if seen_first_line:
                    # we are at the second line now
                    newdata = {
                            "yx": f[0],
                            "yy": f[1],
                            "yz": f[2],
                            }
                    data[-1].update(newdata)
                    seen_first_line = False
                    continue
                else:
                    # we are at the third line now
                    newdata = {
                            "zx": f[0],
                            "zy": f[1],
                            "zz": f[2],
                            }
                    data[-1].update(newdata)
                    continue
            elif len(f) == 4:
                if seen_first_line:
                    # we are at the second line now
                    newdata = {
                            "yx": f[1],
                            "yy": f[2],
                            "yz": f[3],
                            }
                    data[-1].update(newdata)
                    seen_first_line = False
                    continue
                else:
                    if not data:
                        # for some reason we are actually at the first line
                        # probably because of Nans. Alert user to fix it.
                        self._logger.error(
                            f"The line: '{line}' contains NaNs but have 4 "
                            "correct floats which is confusing with the 2nd / "
                            "3rd line of a conductivity block. Please fix."
                            )
                        raise LookupError
                    # we are at the third line now
                    newdata = {
                            "zx": f[1],
                            "zy": f[2],
                            "zz": f[3],
                            }
                    data[-1].update(newdata)
                    continue
            elif (
                    "Max error" in line or "Iteration number" in line or
                    "========" in line
                    ):
                # return what we got
                return data, j
            elif len(f) in (1, 2):
                # sometimes this happens because numbers are too big
                # and 0.01111111+100 was meant to be 0.01111E+100
                if seen_first_line:
                    newdata = {"yx": np.nan, "yy": np.nan, "yz": np.nan}
                    data[-1].update(newdata)
                    seen_first_line = False
                    continue
                else:
                    newdata = {"xz": np.nan, "zy": np.nan, "zz": np.nan}
                    data[-1].update(newdata)
                    continue
        # else we got a problem
        raise LookupError(
                "An error occured when extracting conductivities of holes or "
                "electrons for a given iteration.")


class QEEPWLogConductivitiesSubParser(BaseQEEPWLogConductivitiesSubParser):
    """Subparser for the IBTE conductivities."""

    subject = "conductivities"
    trigger = "iterative Boltzmann Transport Equation"
    _iterative = True
    _loggername = "QEEPWLogConductivitiesSubParser"


class QEEPWLogSERTAConductivitiesSubParser(
        BaseQEEPWLogConductivitiesSubParser):
    """Subparser for the SERTA conductivities."""

    subject = "serta_conductivities"
    trigger = "SERTA"
    _iterative = False
    _loggername = "QEEPWLogSERTAConductivitiesSubParser"
