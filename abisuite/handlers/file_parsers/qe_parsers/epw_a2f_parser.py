import os

import aiofiles.os

from .bases import BaseQEParser
from ...file_structures import QEEPWa2FStructure
from ....routines import decompose_line


class QEEPWa2FParser(BaseQEParser):
    """Parser for an a2F function file produced by the epw.x script."""

    _expected_ending = ""
    _loggername = "QEEPWa2FParser"
    _structure_class = QEEPWa2FStructure

    def _extract_data_from_lines(self, lines):
        # TODO: this could be done more efficiently with numpy.loadtxt
        freqs = []
        a2f = []
        for line in lines:
            s, i, f = decompose_line(line)
            if "#" in line or len(f) == 0:
                # reached the end of the data
                self.frequencies = freqs
                self.a2F = a2f
                return
            freqs.append(f[0])
            a2f.append(f[1:])
        data = {}
        data["frequencies"] = freqs
        data["a2F"] = a2f
        return data

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        rd = meta.rundir
        paths = {}
        for path in await aiofiles.os.listdir(rd):
            if ".a2f." in path:
                paths["a2f"] = os.path.join(rd, path)
                continue
            if ".a2f_tr." in path:
                paths["a2f_tr"] = os.path.join(rd, path)
                continue
        return paths
