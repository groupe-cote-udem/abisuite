import os
from typing import Any
from xml.etree.ElementTree import Element

import aiofiles.os

import numpy as np

from .bases import BaseQEParser
from ..bases import BaseXMLCalculationFileParser
from ...file_structures import QEPWDataFileSchemaStructure


class QEPWDataFileSchemaParser(BaseXMLCalculationFileParser, BaseQEParser):
    """Parser class for a data_file_schema.xml file produced by pw.x."""

    _loggername = "QEPWDataFileSchemaParser"
    _structure_class = QEPWDataFileSchemaStructure

    def _extract_data_from_xml_tree_list(
            self, tree: list[Element]) -> dict[str, Any]:
        # extract kpts / eigs / occupations / weights data
        kpts = []
        eigs = []
        occs = []
        weights = []
        nkpts, nbands = None, None
        output_elements = self._get_xml_element(tree, "output")
        bs_elements = self._get_xml_element(output_elements, "band_structure")
        for element in bs_elements:
            if element.tag == "nbnd":
                nbands = int(element.text)
                continue
            if element.tag == "nks":
                nkpts = int(element.text)
                continue
            # if element.tag == "spinorbit":
            #     so = True if element.text == "true" else False
            #     continue
            if element.tag == "ks_energies":
                for subelement in element:
                    if subelement.tag == "k_point":
                        weights.append(float(subelement.get("weight")))
                        coords = subelement.text.split(" ")
                        coords = [c.rstrip("\n") for c in coords if c]
                        coords = [c for c in coords if c]
                        kpts.append([float(c) for c in coords if c])
                        continue
                    if subelement.tag == "eigenvalues":
                        eig = subelement.text.split(" ")
                        eig = [x.rstrip("\n") for x in eig if x]
                        eigs.append([float(x) for x in eig if x])
                        continue
                    if subelement.tag == "occupations":
                        occ = subelement.text.split(" ")
                        occ = [x.rstrip("\n") for x in occ if x]
                        occ = [x for x in occ if x]
                        occs.append([float(x) for x in occ])
                        continue
        # assert everything is good
        kpts = np.asarray(kpts)
        eigs = np.asarray(eigs)
        occs = np.asarray(occs)
        weights = np.asarray(weights)
        assert kpts.shape == (nkpts, 3), (
                f"kpts shape = {kpts.shape}, nks = {nkpts}")
        assert eigs.shape == (nkpts, nbands), (
                f"eigs shape = {eigs.shape}, nbnd = {nbands}")
        assert occs.shape == (nkpts, nbands), f"occs shape = {occs.shape}"
        assert weights.shape == (nkpts, ), f"weights shape = {weights.shape}"
        return {
                "k_points": kpts,
                "eigenvalues": eigs,
                "weights": weights,
                "occupations": occs,
                }

    @classmethod
    async def _filepath_from_meta(
            cls, meta, *args, **kwargs) -> str:
        expected = os.path.join(
                meta.output_data_dir,
                meta.jobname + ".save",
                "data-file-schema.xml",
                )
        if not await aiofiles.os.path.isfile(expected):
            raise FileNotFoundError(expected)
        return expected
