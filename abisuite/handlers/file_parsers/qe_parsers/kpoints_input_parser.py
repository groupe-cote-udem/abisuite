from ..bases import BaseInputParser
from ...file_structures import QEKpointsInputStructure
from ....routines import decompose_line


class QEKpointsInputParser(BaseInputParser):
    """Parser class for the kpoints.x input file."""

    _expected_ending = ".in"
    _loggername = "QEKpointsInputParser"
    _structure_class = QEKpointsInputStructure

    async def _extract_data(self) -> None:
        await super()._extract_data(strip_newlines=True)

    def _extract_data_from_lines(self, lines):
        ibrav = int(lines[0])
        data = {"ibrav": ibrav}
        data["filout"] = lines[1]
        if ibrav == 4:
            data["celldm3"] = float(lines[2])
            lineshift = 1
        elif ibrav == 2:
            lineshift = 0
        else:
            raise NotImplementedError(ibrav)
        s, mesh, f = decompose_line(lines[2 + lineshift])
        s, shift, f = decompose_line(lines[3 + lineshift])
        data["mesh"] = mesh
        data["shift"] = shift
        data["ibz_only"] = str(lines[4 + lineshift])
        return data
