import os

import aiofiles.os

import numpy as np

from ..bases import BaseFileParser
from ...file_structures import QEKpointsMeshStructure


class QEKpointsMeshParser(BaseFileParser):
    """Parser class for a mesh_k file produced by kpoints.x script."""

    _expected_ending = ".mesh"
    _loggername = "QEKpointsMeshParser"
    _structure_class = QEKpointsMeshStructure

    async def _extract_data(self):
        array = np.loadtxt(self.path, skiprows=1)
        self._set_attributes_from_data(
                {"kpoints": array[:, 1:4],
                 "weights": array[:, 4] / sum(array[:, 4]),
                 "nkpoints": len(array),
                 })

    @classmethod
    async def _filepath_from_meta(cls, meta):
        async with meta:
            for filename in await aiofiles.os.listdir(
                    meta.run_directory):
                if filename.endswith(cls._expected_ending):
                    return os.path.join(meta.run_directory, filename)
        raise FileNotFoundError("Kpoints mesh parser.")
