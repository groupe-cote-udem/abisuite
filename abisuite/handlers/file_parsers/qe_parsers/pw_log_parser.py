from .bases import BaseQELogParser
from .subparsers.pw_log_subparsers import (
        AtomicPositionsSubParser,
        EigenvaluesSubParser,
        EnergySubParser,
        KGridSubParser,
        ParallelizationSubParser,
        QEPWLogCrystalAxesSubParser,
        QEPWLogFermiEnergySubParser,
        QEPWLogFinalAtomicPositionsSubParser,
        QEPWLogFinalGeometrySubParser,
        QEPWLogInputParametersSubParser,
        QEPWLogLatticeParametersSubParser,
        QEPWLogNelectronsSubParser,
        QEPWLogPressureStressSubParser,
        QEPWLogTimingSubParser,
        )
from ...file_structures import QEPWLogStructure


class QEPWLogParser(BaseQELogParser):
    """A log file parser that extracts data from a log file."""

    _loggername = "QEPWLogParser"
    _structure_class = QEPWLogStructure
    _subparsers = (
            ParallelizationSubParser, EnergySubParser,
            EigenvaluesSubParser, QEPWLogCrystalAxesSubParser,
            QEPWLogLatticeParametersSubParser, QEPWLogFermiEnergySubParser,
            QEPWLogPressureStressSubParser,
            QEPWLogFinalAtomicPositionsSubParser,
            QEPWLogFinalGeometrySubParser,
            QEPWLogInputParametersSubParser, QEPWLogTimingSubParser,
            QEPWLogNelectronsSubParser,
            KGridSubParser,
            AtomicPositionsSubParser,
            )
