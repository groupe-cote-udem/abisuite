import os
from typing import Sequence

import aiofiles.os

import numpy as np

from ..bases import BaseListedDataFileParser
from ...file_structures import QEEPWDecayStructure


class QEEPWDecayParser(BaseListedDataFileParser):
    """Parser class for a decay.* epw file."""

    _expected_ending = (".H", ".epmate", ".epmatp", ".v")
    _loggername = "QEEPWDecayParser"
    _structure_class = QEEPWDecayStructure

    def _extract_data_from_array(
            self, array: np.ndarray) -> dict[str, np.ndarray]:
        return {"decay": array[:, 1], "wannier_centers": array[:, 0]}

    @classmethod
    async def _filepath_from_meta(cls, meta) -> Sequence[str]:
        async with meta:
            if meta.calctype != "qe_epw":
                raise ValueError(meta.calctype)
            rundir = meta.run_directory
        rtn = {}
        for filename in await aiofiles.os.listdir(rundir):
            if not filename.startswith("decay"):
                continue
            for ending in cls._expected_ending:
                if filename.endswith(ending):
                    rtn[ending[1:]] = os.path.join(rundir, filename)
                    break
        return rtn
