from .bases import BaseQELogParser
from ...file_structures import QEKpointsLogStructure


class QEKpointsLogParser(BaseQELogParser):
    """Parser class for a kpoints.x log file."""

    _loggername = "QEKpointsLogParser"
    _structure_class = QEKpointsLogStructure
    _subparsers = tuple()
