from .bases import BaseQEInputParser
from ...file_structures import QEPWInputStructure
from ....routines import decompose_line


class QEPWInputParser(BaseQEInputParser):
    """Class that can parse a Quantum Espresso Input File."""

    _loggername = "QEPWInputParser"
    _structure_class = QEPWInputStructure

    def _check_for_special_input_var(self, i, lines):
        # check if special input var is defined on this line
        # if yes, return the related data
        lowered = lines[i].lower()
        if "atomic_species" in lowered:
            self._logger.debug("Found ATOMIC_SPECIES varblock.")
            # atomic species: the following lines are a list of atom types
            return self._extract_atomic_species(lines[i + 1:])
        elif "atomic_positions" in lowered:
            self._logger.debug("Found ATOMIC_POSITIONS varblock.")
            parameter = lines[i].split(" ")[-1]
            return self._extract_atomic_positions(lines[i + 1:], parameter)
        elif "k_points" in lowered:
            self._logger.debug("Found K_POINTS varblock.")
            parameter = lines[i].split(" ")[-1]
            return self._extract_kpts(lines[i + 1:], parameter)
        elif "cell_parameters" in lowered:
            parameter = lines[i].split(" ")[-1]
            return self._extract_cell_parameters(lines[i + 1:], parameter)
        # else return super method
        return super()._check_for_special_input_var(i, lines)

    def _extract_kpts(self, lines, parameter):
        # lines should be starting with the list of kpts parameters
        if parameter not in ("automatic",
                             "tpiba", "crystal", "crystal_b"):
            raise NotImplementedError(f"kpt param: {parameter}")
        if parameter == "automatic":
            data = None
            for i, line in enumerate(lines):
                if "atomic" in line.lower():  # next data block
                    return {"k_points": {"parameter": parameter,
                                         "k_points": data}}, i
                s, i, f = decompose_line(line)
                data = i
            # end of file
            return {"k_points": {"parameter": parameter,
                                 "k_points": data}}, len(lines)
        elif parameter in ("tpiba", "crystal", "crystal_b"):
            data = {
                "k_points": {
                    "parameter": parameter,
                    }, }
            kpts = []
            weights = []
            nks = None
            for j, line in enumerate(lines):
                if "k_points" in line.lower():
                    continue
                if "atomic" in line.lower():
                    if nks is None:
                        raise LookupError(
                                "Number of kpts was not determined.")
                    if nks != len(kpts):
                        raise LookupError(
                                "Number of kpts stated don't match number of"
                                "kpts read.")
                    if nks != len(weights):
                        raise LookupError(
                                "Number of kpts don't match number of weights"
                                )
                    data["k_points"]["k_points"] = kpts
                    data["k_points"]["nks"] = len(kpts)
                    data["k_points"]["weights"] = weights
                    return data, j
                s, i, f = decompose_line(line)
                if len(i) == 1 and len(f) == 0:
                    # nks
                    nks = i[0]
                    continue
                # else is a kpt + weight
                kpts.append(f[:3])
                if len(i) == 1:
                    # weight is an int
                    weights.append(i[0])
                    continue
                # else its a float
                if len(f) != 4:
                    raise LookupError(
                            "Expected 4 floats when reading kpts list: "
                            "(coords + weight).")
                weights.append(f[-1])
            # if we reach end of file, return what we got
            if nks is None:
                raise LookupError(
                        "Reached end of file but could not determine number of"
                        "kpts.")
            if nks != len(kpts):
                raise LookupError(
                        "Reached end of file and we miss some kpts.")
            if nks != len(weights):
                raise LookupError(
                        "Reached end of file and we miss some weights.")
            data["k_points"]["k_points"] = kpts
            data["k_points"]["nks"] = len(kpts)
            data["k_points"]["weights"] = weights
            return data, len(lines)

    def _extract_atomic_positions(self, lines, parameter):
        # lines should be starting with a list of atomic positions
        data = {}
        for j, line in enumerate(lines):
            if any([string in line.lower()
                    for string in ["atomic", "k_points", "cell"]]):
                # Reached new section => return data gathered
                return {"atomic_positions": {"parameter": parameter,
                                             "positions": data}}, j
            s, i, f = decompose_line(line)
            atom_type = s[0]
            if not len(f):
                coordinates = i
            else:
                coordinates = f
            if atom_type not in data:
                data[atom_type] = [coordinates]
            else:
                data[atom_type].append(coordinates)
        # reached end of file, return everything
        return {"atomic_positions": {"parameter": parameter,
                                     "positions": data}}, len(lines)

    def _extract_cell_parameters(self, lines, parameter):
        data = {"parameter": parameter, "cell_parameters": []}
        for line in lines[:3]:
            s, i, f = decompose_line(line)
            data["cell_parameters"].append(f)
        return {"cell_parameters": data}, 3

    def _extract_atomic_species(self, lines):
        # lines should be starting with a list of atomic species
        data = []
        for j, line in enumerate(lines):
            if any([string in line.lower()
                    for string in ["atomic", "k_points", "cell"]]):
                # reached new section => return data gathered
                return {"atomic_species": data}, j
            s, i, f = decompose_line(line)
            atom = s[0]
            atomic_mass = f[0]
            pseudo = s[1]
            data.append({"atom": atom,
                         "atomic_mass": atomic_mass,
                         "pseudo": pseudo})
        # reached end of file
        return {"atomic_species": data}, len(lines)
