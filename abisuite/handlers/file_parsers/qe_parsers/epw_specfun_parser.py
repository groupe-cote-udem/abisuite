import os

import aiofiles

import numpy as np

from .bases import BaseQEParser
from ...file_structures import QEEPWSpecfunStructure


class QEEPWSpecfunParser(BaseQEParser):
    """Parser for an electron spectral function file produced by the epw.x."""

    _expected_ending = ""
    _loggername = "QEEPWSpecFunParser"
    _structure_class = QEEPWSpecfunStructure

    def _extract_data_from_lines(self, lines):
        # data is organized like
        # kpt idx | energy | A(k, w)
        data = np.loadtxt(lines)
        nkpts = len(np.unique(data[:, 0]))
        nfreqs = len(np.unique(data[:, 1]))
        return {"energies": data[:nfreqs, 1],
                "nkpts": nkpts,
                "spectral_function": data[:, 2].reshape((nkpts, nfreqs)),
                }

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        async with meta:
            rd = meta.rundir
        # there is one file per temperature
        paths = {}
        for filename in await aiofiles.os.listdir(rd):
            if filename.startwith("specfun.elself"):
                temperature = filename.split("specfun.elself")[-1]
                paths[temperature] = os.path.join(rd, filename)
        return paths
