import os

import aiofiles.os

import numpy as np

from ..bases import BaseListedDataFileParser
from ...file_structures import QEEPWInvTauStructure


class QEEPWInvTauParser(BaseListedDataFileParser):
    """Parser class for an inv_tau.fmt or inv_taucb.fmt file."""

    _expected_ending = ".fmt"
    _loggername = "QEEPWInvTauParser"
    _structure_class = QEEPWInvTauStructure

    def _extract_data_from_array(self, array):
        # the given array is organized as columns (depending of filename):
        if not len(array):
            # array empty for some reason (prob. implementation)
            return {"nkpts": 0, "nbands": 0, "nfrequencies": 0,
                    "frequencies": [], "energies": [], "relaxation_times": [],
                    }
        if self.path.endswith("_freq.fmt"):
            #  kpt      ibnd    energy [Ry]  freq (meV)    relaxation time [Ry]
            data = {
                    "nkpts": len(np.unique(array[:, 0])),
                    "nbands": len(np.unique(array[:, 1])),
                    "nfrequencies": len(np.unique(array[:, 3])),
                    }
            newshape = (data["nfrequencies"], data["nkpts"], data["nbands"])
            energies = array[:, 2].reshape(newshape)
            frequencies = array[:, 3].reshape(newshape)
            relax = array[:, 4].reshape(newshape)
            data["frequencies"] = frequencies
        elif self.path.endswith("_mode.fmt"):
            # itemp    kpt      ibnd     imode   energy [Ry]   relaxation time [Ry]  # noqa: E501
            data = {
                    "nkpts": len(np.unique(array[:, 1])),
                    "nbands": len(np.unique(array[:, 2])),
                    "ntemperatures": len(np.unique(array[:, 0])),
                    "nmodes": len(np.unique(array[:, 3])),
                    }
            newshape = (
                    data["ntemperatures"], data["nkpts"], data["nbands"],
                    data["nmodes"])
            energies = array[:, 4].reshape(newshape)
            relax = array[:, 5].reshape(newshape)
        else:
            # itemp ikpt ibnd energy [Ry] relaxation_time [Ry]
            data = {"ntemperatures": len(np.unique(array[:, 0])),
                    "nkpts": len(np.unique(array[:, 1])),
                    "nbands": len(np.unique(array[:, 2])),
                    }
            newshape = (data["ntemperatures"], data["nkpts"], data["nbands"])
            energies = array[:, 3].reshape(newshape)
            relax = array[:, 4].reshape(newshape)
        data["energies"] = energies
        data["relaxation_times"] = relax
        return data

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        """Creates the dict of inv_tau parsers. 1 for cb 1 for vb."""
        async with meta:
            if meta.calctype != "qe_epw":
                raise ValueError(meta.calctype)
            rundir = meta.run_directory
        paths = {}
        # check for mode decompositon / frequency decomposition
        for name, path in zip(
                ["conduction_frequence_decomposition",
                 "conduction_mode_decomposition", "conduction",
                 "valence_frequence_decomposition",
                 "valence_mode_decomposition", "valence"],
                ["inv_taucb_freq.fmt", "inv_taucb_mode.fmt", "inv_taucb.fmt",
                 "inv_tau_freq.fmt", "inv_tau_mode.fmt", "inv_tau.fmt"],
                ):
            fpath = os.path.join(rundir, path)
            if await aiofiles.os.path.isfile(fpath):
                paths[name] = fpath
        return paths
