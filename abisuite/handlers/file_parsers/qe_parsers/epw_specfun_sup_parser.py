import os

import aiofiles

import numpy as np

from .bases import BaseQEParser
from ...file_structures import QEEPWSpecfunSupStructure


class QEEPWSpecfunSupParser(BaseQEParser):
    """Parser for an electron self energy function file produced by epw.x."""

    _expected_ending = ""
    _loggername = "QEEPWSpecFunSupParser"
    _structure_class = QEEPWSpecfunSupStructure

    def _extract_data_from_lines(self, lines):
        # data is organized like
        # kpt idx | energy | A(k, w)
        data = np.loadtxt(lines)
        nkpts = len(np.unique(data[:, 0]))
        nbands = len(np.unique(data[:, 1]))
        freqs = np.unique(data[:, 3])
        nfreqs = len(freqs)
        energies = data[::nfreqs, 2]
        se = data[:, 4] + 1.0j * data[:, 5]
        se = se.reshape((nbands, nkpts, nfreqs))
        se = np.moveaxis(se, 1, 0)  # transpose
        return {"nkpts": nkpts, "nbands": nbands,
                "energies": energies.reshape((nbands, nkpts)).T,
                "frequencies": freqs,
                "self_energies": se,
                }

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # this file is written in the run directory where the batch file is
        # where the epw.x script has been called
        async with meta:
            rd = meta.rundir
        # there is one file per temperature
        paths = {}
        for filename in await aiofiles.os.listdir(rd):
            if filename.startwith("specfun_sup.elself"):
                temperature = filename.split("specfun_sup.elself")[-1]
                paths[temperature] = os.path.join(rd, filename)
        return paths
