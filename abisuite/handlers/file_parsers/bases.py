import abc
import logging
import os
from typing import Any
from xml.etree.ElementTree import (
        Element as XMLElement, fromstring as xml_fromstring,
        )

import aiofiles
# from file_read_backwards import FileReadBackwards

from async_property import async_property

import numpy as np

from ..bases import BaseStructuredObject
from ...bases import BaseUtility
from ...exceptions import DevError
from ...routines import is_list_like


async def _get_lines_backwards_generator(path, buf_size=8192):
    # taken from:
    # https://stackoverflow.com/a/23646049/6362595
    async with aiofiles.open(path) as fh:
        segment = None
        offset = 0
        await fh.seek(0, os.SEEK_END)
        file_size = remaining_size = await fh.tell()
        while remaining_size > 0:
            offset = min(file_size, offset + buf_size)
            await fh.seek(file_size - offset)
            buffer = await fh.read(min(remaining_size, buf_size))
            remaining_size -= buf_size
            lines = buffer.split('\n')
            # The first line of the buffer is probably not a complete line so
            # we'll save it and append it to the last line of the next buffer
            # we read
            if segment is not None:
                # If the previous chunk starts right from the beginning of line
                # do not concat the segment to the last line of new chunk.
                # Instead, yield the segment first
                if buffer[-1] != '\n':
                    lines[-1] += segment
                else:
                    yield segment
            segment = lines[0]
            for index in range(len(lines) - 1, 0, -1):
                if lines[index]:
                    yield lines[index]
        # Don't yield None if the file was empty
        if segment is not None:
            yield segment


class BaseFileParser(BaseStructuredObject, abc.ABC):
    """Base class for any file.

    Files which belongs in a calculation
    directory or not.
    """

    _expected_ending = None

    def __init__(self, *args, **kwargs):
        super().__init__(**kwargs)
        if self._expected_ending is None:
            raise DevError("Need to set _expected_ending.")
        self._path = None

    @async_property
    async def is_empty(self):
        """False if file contains at least one line of data."""
        lines = await self.get_lines_from_file(self.path, line_limit=1)
        if lines:
            return False
        return True

    @property
    def path(self):
        """Return the file path."""
        return self._path

    async def set_path(self, path):
        """Sets the path property."""
        # if not os.path.exists(path):
        #     raise FileNotFoundError(f"Nothing found here: {path}.")
        # only raise an error when reading
        # if not os.path.isfile(path):
        #     raise FileNotFoundError(f"Not a file: {path}.")
        if self._expected_ending is not None:
            if not is_list_like(self._expected_ending):
                endings = (self._expected_ending, )
            else:
                endings = self._expected_ending
            for ending in endings:
                if path.endswith(ending):
                    break
            else:
                self._logger.warning(f"file {path} has its extension different"
                                     f" from expected ({self._expected_ending}"
                                     ")")
        self._path = path

    async def read(self, *args, extract_lines_only=False, **kwargs):
        """Read and parse file set in the 'path' attribute.

        Parameters
        ----------
        extract_lines_only: bool, optional
            If `True`, all args and kwargs are passed to the
            :meth:`extract the lines <
            abisuite.handlers.file_parsers.base.BaseFileParser.get_lines_from_file>`
            from the file. The list of lines is returned.
            If `False`, the data is extracted as well (default) and
            lines not returned.

        Other Parameters
        ----------------
        args/kwargs:
            Other args and kwargs are passed the the ``_extract_data`` private
            method or ``get_lines_from_file`` depending on what is requested.

        Raises
        ------
        FileNotFoundError:
            If the path given to the handler does not point towards an
            actual file or the file does not exists.

        Returns
        -------
        list: The list of lines if ``extract_lines_only`` is set to `True`.
        """
        islink = await aiofiles.os.path.islink(self.path)
        if not await aiofiles.os.path.exists(self.path) and not islink:
            raise FileNotFoundError(self.path)
        if not await aiofiles.os.path.isfile(self.path) and not islink:
            raise FileNotFoundError(f"Not a file: {self.path}.")
        if extract_lines_only:
            if "cleanfunc" not in kwargs:
                # add it artificially
                return await self.get_lines_from_file(
                    self.path,
                    *args,
                    cleanfunc=self._clean_lines,
                    logger=self._logger,
                    **kwargs)
            return await self.get_lines_from_file(
                    self.path, *args, logger=self._logger, **kwargs)
        else:
            await self._extract_data(*args, **kwargs)

    @abc.abstractmethod
    async def _extract_data(self):
        pass

    @classmethod
    async def from_file(cls, path, *args, **kwargs):
        """Initialize parser directly from a file."""
        instance = cls(*args, **kwargs)
        await instance.set_path(path)
        return instance

    @classmethod
    async def _instance_from_filepath(cls, filepath, *args, **kwargs):
        """Return a file parser object from a file path.

        Given a str, a list or a dict of filepaths, returns either, a single
        instance, a list of instances or a dict of instances.

        The dict return (if a dict is given for filepath) will have the same
        keys. Other args and kwargs are passed to the init method for each
        instances created.
        """
        if is_list_like(filepath):
            # many files, return a list of parsers if needed
            if len(filepath) > 1:
                return [await cls.from_file(x, *args, **kwargs)
                        for x in filepath]
            filepath = filepath[0]
        elif isinstance(filepath, dict):
            # many files organized in a dict, return a dict
            return {k: await cls.from_file(v, *args, **kwargs)
                    for k, v in filepath.items()}
        return await cls.from_file(filepath, *args, **kwargs)

    @staticmethod
    async def get_lines_from_file(
            path,
            backwards=False,
            cleanfunc=None,
            cleanfunc_kwargs=None,
            line_limit=None,
            logger=None,
            ):
        """Extract lines from an actual file.

        Parameters
        ----------
        path: str
            The path of the file to extract data from.
        backwards: bool, optional
            If True, the file is read starting from the end instead of starting
            from the beginning. Especially usefull when we just want to
            read data printed at the end of a file to make it faster to
            read big files. Useful when combined with the 'line_limit' kwarg.
        cleanfunc: function, optional
            If not None, the list of lines will be cleaned using this function.
            What this function returns will be returned.
        cleanfunc_kwargs: dict, optional
            If not None, this is a dict that contains the arguments to pass
            to the cleanfunc function while cleaning the lines.
        line_limit: bool, optional
            If not None, specifies the number of lines to return.
        logger: Logger object, optional
            If not None, this logger object will be used in case an error
            is thrown while parsing the file.

        Returns
        -------
        List: The list of lines in the file (possibly cleaned).
        """
        # just extract the lines from the actual file
        try:
            if backwards:
                lines = await BaseFileParser._get_lines_backwards(
                        path, line_limit=line_limit)
                # reverse the line ordering since they were appended backwards
                lines = lines[::-1]
            else:
                lines = await BaseFileParser._get_lines_normal(
                        path, line_limit=line_limit)
        except UnicodeDecodeError as err:
            if logger is not None:
                logger.critical(f"Cannot read file '{path}'.")
                logger.exception(err)
            raise err
        # clean the lines if needed
        if cleanfunc is None:
            return lines
        if cleanfunc_kwargs is None:
            cleanfunc_kwargs = {}
        return cleanfunc(lines, **cleanfunc_kwargs)

    @staticmethod
    async def _get_lines_normal(path, line_limit=None):
        lines = []
        iline = 0
        async with aiofiles.open(path, "r") as f:
            async for line in f:
                lines.append(line)
                if line_limit is not None:
                    if line_limit <= iline + 1:
                        break
                iline += 1
        return lines

    @staticmethod
    async def _get_lines_backwards(path, line_limit=None):
        lines = []
        async for line in _get_lines_backwards_generator(path):
            lines.append(line)
            if line_limit is not None:
                if line_limit < len(lines):
                    return lines
        return lines

    @staticmethod
    def _clean_lines(
            lines, strip_newlines=False, strip_spaces=False,
            strip_comas=False,
            ignore_python_comments=False, ignore_fortran_comments=False,
            ignore_empty_lines=False,
            ignore_lines_starting_with=None,
            ):
        """Processes a list of lines extracted from a file.

        Parameters
        ----------
        strip_comas: bool, optional
            If True, the comas will be stripped from each lines.
        strip_newlines: bool, optional
            If True, the newlines will be stripped from each lines.
        strip_spaces: bool, optional
            If True, the surrounding spaces of all lines will be stripped.
        ignore_empty_lines: bool, optional
            If True, empty lines will be discarded.
        ignore_python_comments: bool, optional
            If True, the lines starting with '#' will be discarded.
        ignore_fortran_comments: bool, optional
            If True, the lines starting with '!' will be discarded.
        ignore_lines_starting_with: list, optional
            If given, gives a list of what lines to ignore.
        """
        newlines = []
        if ignore_lines_starting_with is None:
            ignore_lines_starting_with = []
        if not is_list_like(ignore_lines_starting_with):
            raise DevError(
                    "'ignore_lines_starting_with' should be a list but "
                    f"got '{ignore_lines_starting_with}'.")
        ignore_lines_starting_with = list(ignore_lines_starting_with)
        if ignore_python_comments:
            ignore_lines_starting_with.append("#")
        if ignore_fortran_comments:
            ignore_lines_starting_with.append("!")

        def line_startswith_one_forbidden_char(line):
            for char in ignore_lines_starting_with:
                if line.startswith(char):
                    return True
            return False

        for line in lines:
            if strip_newlines:
                line = line.strip("\n")
            if strip_spaces:
                line = line.strip()
            if strip_comas:
                line = line.strip(",")
            if line_startswith_one_forbidden_char(line):
                continue
            if ignore_empty_lines and len(line) == 0:
                continue
            newlines.append(line)
        del lines
        return newlines

    def _set_attributes_from_data(self, data):
        """Sets attributes to the File Parser object from a dict of data."""
        for attr, value in data.items():
            setattr(self, attr, value)


class BaseFormattedFileParser(BaseFileParser, abc.ABC):
    """Base class for formatted files.

    File don't need to be part of a calculation directory.
    """

    async def _extract_data(self, backwards=False, line_limit=None, **kwargs):
        """Extract data from file.

        For this class, the data is read from the formatted file directly.
        This method parses the lines in the file, cleans the lines and parse
        the data from the lines. Then, the attributes are set.

        Parameters
        ----------
        backwards: bool, optional
            If True, the file is read from the end (but lines will have same
            order). Usefull when we only want to parse the end of a big file.
        line_limit: int, optional
            The maximum number of lines to parse and put in memory
            Set to `None` for no maximum.

        Other Parameters
        ----------------
        kwargs to this function are passed as the 'cleanfunc_kwargs' argument
        of the 'get_lines_from_file' function.
        """
        lines = await self.read(
                    extract_lines_only=True,
                    backwards=backwards,
                    cleanfunc=self._clean_lines,
                    cleanfunc_kwargs=kwargs,
                    line_limit=line_limit,
                    )
        self._set_attributes_from_data(
                self._extract_data_from_lines(lines))

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass


class BaseCalculationFileParser(BaseFileParser, abc.ABC):
    """Parser class for files belonging in a calculation directory."""

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Return a Parser from a calculation directory."""
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        lvl = kwargs.get("loglevel", logging.INFO)
        async with await MetaDataFile.from_calculation(
                path, loglevel=lvl) as meta:
            return await cls.from_metadatafile(meta)

    @classmethod
    async def from_metadatafile(cls, meta, *args, **kwargs):
        """Initialize parser from a meta data file object."""
        # import here to prevent import loops
        from ..file_handlers import MetaDataFile
        if not isinstance(meta, MetaDataFile):
            raise TypeError("Requires a Meta data file object.")
        # if cls._calctype is not None:
        #     if meta.calctype != cls._calctype:
        #         raise ValueError(f"Meta calctype '{meta.calctype}' differs"
        #                          f" from Parser calctype "
        #                          f"'{cls._calctype}'")
        filepath = await cls._filepath_from_meta(meta, *args, **kwargs)
        return await cls._instance_from_filepath(filepath, *args, **kwargs)

    @abc.abstractclassmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        # returns the expected filepath from a meta file
        pass


class BaseListedDataFileParser(BaseCalculationFileParser, abc.ABC):
    """Base class for parsers that just needs a simple numpy.loadtxt call."""

    async def _extract_data(self):
        # override this method as the data is simply organised
        async with aiofiles.open(self.path, "r") as f:
            lines = await f.readlines()
        data = np.loadtxt(lines)
        data = self._extract_data_from_array(data)
        for k, v in data.items():
            setattr(self, k, v)

    @abc.abstractmethod
    def _extract_data_from_array(self):
        pass


class BaseFormattedCalculationFileParser(
        BaseCalculationFileParser, BaseFormattedFileParser, abc.ABC):
    """Base class for parsers that have a custom syntax (most parsers).

    Only for files belonging in a calculation directory.
    """

    # avoid DDD
    async def _extract_data(self, *args, **kwargs) -> None:
        await BaseFormattedFileParser._extract_data(
                self, *args, **kwargs)


class BaseXMLParser(BaseFormattedFileParser, abc.ABC):
    """Base class for xml files."""

    _expected_ending = ".xml"

    async def _extract_data(self) -> None:
        # don't waste time striping newlines as the final lines will be
        # revert back to a single string which will be passed to a xml parser
        await super()._extract_data(strip_newlines=False)

    def _extract_data_from_lines(
            self, lines: list[str]) -> dict[str, Any]:
        # convert lines into single string
        string = "".join(lines)
        tree_list = list(xml_fromstring(string))
        return self._extract_data_from_xml_tree_list(tree_list)

    @abc.abstractmethod
    def _extract_data_from_xml_tree_list(self, *args, **kwargs):
        pass

    def _get_xml_element(
            self, tree_list: list[XMLElement],
            element_name: str) -> XMLElement:
        # return the xml element with the tagName 'element_name'
        for element in tree_list:
            if element.tag == element_name:
                return element
        raise LookupError(f"Could not get '{element_name}'.")


class BaseXMLCalculationFileParser(BaseCalculationFileParser, BaseXMLParser):
    """Base class for calculation xml files."""

    async def _extract_data(self, *args, **kwargs) -> None:
        await BaseXMLParser._extract_data(self, *args, **kwargs)


class BaseSubParser(BaseUtility, abc.ABC):
    """SubParser Base class."""

    can_be_recalled = False
    """If can_be_recalled is True, then the subparser will be updated
    each time the trigger is found in the file.
    """
    recalled_behavior = None
    """Specify what happens to the data when this sub parser is recalled.
    Can be either 'append' or 'overwrite'.
    """
    subject = None
    trigger = None
    not_mandatory = False

    def __init__(self, lines, **kwargs):
        super().__init__(**kwargs)
        self._ending_relative_index = None
        # TODO: check that this is not necessary!
        self.lines = self.preprocess_lines(lines)
        # self._logger.debug(f"SubParser triggered for: '{self.subject}'.")
        self.data = self._extract_data_from_lines(self.lines)
        if self.can_be_recalled and self.recalled_behavior is None:
            raise DevError(
                    "Need to set the 'recalled_behavior' for the subparser.")

    @abc.abstractmethod
    def _extract_data_from_lines(self, lines):
        pass

    @property
    def ending_relative_index(self):
        """Return the ending relative index."""
        if hasattr(self, "_ending_relative_index"):
            if self._ending_relative_index is not None:
                return self._ending_relative_index
        # if we are here, there is a dev error
        raise DevError(
                f"'ending_relative_index' must be set: {self.__class__} - "
                f"{self.subject}.")

    @ending_relative_index.setter
    def ending_relative_index(self, index):
        self._ending_relative_index = index

    @staticmethod
    def preprocess_lines(lines):
        """Preprocess the lines."""
        # TODO: lines should already have been cleaned up. This is redundant!
        return [x.strip("\n").strip() for x in lines]


class BaseParserWithSubParsers(BaseFormattedCalculationFileParser, abc.ABC):
    """Base class for parsers that use subparsers to parse their data."""

    _subparsers = None

    def __init__(self, *args, **kwargs):
        if self._subparsers is None:
            raise DevError("Need to set subparsers list.")
        super().__init__(*args, **kwargs)
        self.subparsers = SubParsersList(self._subparsers)

    def _extract_data_from_lines(self, lines: list[str]) -> dict:
        skip = 0
        data = {}
        # copy subparsers to not erase from the class attribute
        subparsers = self.subparsers.copy()
        for index, line in enumerate(lines):
            if index < skip:
                # don't work on this line
                continue
            for triggers, subparser in subparsers.items():
                # there can be multiple triggers for one type of data
                # e.g.:fermi_level and highest_occupied_level for QEPWLog
                if not is_list_like(triggers):
                    triggers = (triggers, )
                for trigger in triggers:
                    if trigger not in line:
                        continue
                    # this line is a trigger for the subparser to work
                    # parse the rest of the lines from here
                    s = subparser(lines[index:],
                                  loglevel=self._logger.level)
                    self._set_data_from_subparser(data, s)
                    if not subparser.can_be_recalled:
                        # remove the subparser from the list
                        # to not parse again
                        subparsers.remove(subparser)
                    # modify the skip to not reparse the parsed lines
                    skip = index + s.ending_relative_index
                    # stop parsing the active line
                    break
            if self._stop_parsing_at(line, index):
                # TODO: this is weird, find a way to make this better
                break
        self._warn_if_missing_info(subparsers)
        return data

    def _warn_if_missing_info(self, subparsers: list) -> None:
        if not subparsers:
            return
        # possibly couldnt get some information
        missed = []
        for subparser in subparsers:
            if subparser.can_be_recalled:
                # this one never removed from list
                continue
            if subparser.not_mandatory:
                # exceptional flag
                continue
            subjects = subparser.subject
            if not is_list_like(subjects):
                subjects = [subjects]
            for subject in subjects:
                if subject not in self.structure.optional_attributes:
                    missed.append(subparser.subject)
        if not missed:
            return
        self._logger.warning(
           f"Some mandatory info couldn't be parsed: {missed} from "
           f"{self.path}.")

    def _set_data_from_subparser(self, data, subparser):
        subjects = subparser.subject
        # support for many subjects in a single subparser
        # different subjects are ordered in the data dicts
        # as the keys
        if not is_list_like(subjects):
            subjects = [subjects]
            subparser.data = {subjects[0]: subparser.data}
        for subject, subdata in subparser.data.items():
            if not subparser.can_be_recalled:
                if subject not in data:
                    data[subject] = subdata
                    continue
            if subparser.recalled_behavior == "overwrite":
                data[subject] = subdata
            elif subparser.recalled_behavior == "append":
                if subject not in data:
                    data[subject] = [subdata]
                else:
                    data[subject].append(subdata)
            else:
                raise NotImplementedError(subparser.recalled_behavior)

    def _stop_parsing_at(self, line, index):
        # use this if there's a need to stop parsing at a certain line
        return False


class SubParsersList:
    """Class that takes a list of subparsers and manages it."""

    def __init__(self, slist):
        self.subparsers = list(slist)

    def copy(self):
        """Copy the subparser list."""
        return SubParsersList(self.subparsers.copy())

    def items(self):
        """Iterate over the subparsers."""
        for subparser in self.subparsers:
            yield subparser.trigger, subparser

    def remove(self, parser):
        """Remove a subparser from the list."""
        self.subparsers.remove(parser)

    def __len__(self):
        return len(self.subparsers)

    def __iter__(self):
        for subparser in self.subparsers:
            yield subparser

    def __repr__(self):
        return str(self.subparsers)


class BaseInputParserNoInputVariables(
        BaseFormattedCalculationFileParser, abc.ABC):
    """Base class for an input file parser that does not have input variables.

    This class is more general than the 'BaseInputParser' class.
    """

    _expected_ending = ".in"

    async def _extract_data(
            self, strip_newlines: bool = True,
            strip_spaces: bool = True,
            ignore_python_comments: bool = True,
            ignore_fortran_comments: bool = True,
            **kwargs) -> None:
        await super()._extract_data(
                strip_newlines=strip_newlines,
                strip_spaces=strip_spaces,
                ignore_python_comments=ignore_python_comments,
                ignore_fortran_comments=ignore_fortran_comments,
                **kwargs)

    @classmethod
    async def _filepath_from_meta(cls, meta):
        return meta.input_file_path


class BaseInputParser(BaseInputParserNoInputVariables, abc.ABC):
    """Base class for an input file parser."""

    pass


class BaseLogParser(BaseParserWithSubParsers):
    """Base class for log parsers."""

    _expected_ending = ".log"

    async def _extract_data(self):
        await super()._extract_data(
                strip_newlines=True, strip_spaces=True)

    def _extract_data_from_lines(self, *args, **kwargs):
        return BaseParserWithSubParsers._extract_data_from_lines(
                self, *args, **kwargs)

    @classmethod
    async def _filepath_from_meta(cls, meta, *args, **kwargs):
        try:
            # log path is located in PBS files.
            # import here to prevent import loops.
            from ..file_handlers import PBSFile
            async with await PBSFile.from_meta_data_file(
                    meta, loglevel=kwargs.get(
                        "loglevel", logging.INFO)) as pbs:
                if await aiofiles.os.path.isfile(pbs.log_path):
                    return pbs.log_path
                # log file does not exists. Check if there is a '.log' file
                # next to the meta data file
                for filename in await aiofiles.os.listdir(meta.dirname):
                    joined = os.path.join(meta.dirname, filename)
                    if filename.endswith(".log") and joined != pbs.log_path:
                        # try to see if both files are the same
                        # in case both paths exist
                        try:
                            if os.path.samefile(joined, pbs.log_path):
                                # samefile and both exists => don't need to
                                # rename/rewrite
                                return pbs.log_path
                        except FileNotFoundError:
                            # pbs.log_path does not exists
                            pass
                        # modify pbs file
                        pbs._logger.warning(
                            f"'{pbs.log_path}' does not exists but found log "
                            f"file at '{joined}'. I corrected batch job file.")
                        pbs.log_path = joined
                        await pbs.write(overwrite=True)
                        return joined
                else:
                    # not log file found next to meta file
                    # and log file listed in pbs does not exist
                    # perhaps the calculation never ran or is in queue
                    # return expected location then
                    return pbs.log_path
        except FileNotFoundError as err:
            # PBS don't exists => perhaps it was cleaned
            # fallback to guessing where the log is.
            # usually it is located next to the meta data file
            async with meta:
                workdir = meta.calc_workdir
            for filename in await aiofiles.os.listdir(workdir):
                if filename.endswith(cls._expected_ending):
                    return os.path.join(workdir, filename)
            raise err
