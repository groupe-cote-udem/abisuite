import aiofiles.os

from .bases import BaseFileParser
from ..file_structures import SymLinkStructure


class SymLinkParser(BaseFileParser):
    """Parser for a symlink file or a directory containing symlinked files."""

    _expected_ending = ""
    _loggername = "SymLinkParser"
    _structure_class = SymLinkStructure

    async def _extract_data(self):
        if not await aiofiles.os.path.islink(self.path):
            raise FileNotFoundError(f"Not a symlink: {self.path}")
        self.source = await aiofiles.os.readlink(self.path)
