from .abinit_cleaners import (
        AbinitCleaner, AbinitAnaddbCleaner, AbinitMrgddbCleaner,
        AbinitOpticCleaner,
        __ABINIT_CALCTYPES_TO_CLEANER_CLS__,
        )
from .qe_cleaners import (
        QEEPWCleaner, QEMatdynCleaner, QEPHCleaner, QEPWCleaner, QEQ2RCleaner,
        )


__CALCTYPES_TO_CLEANER_CLS__ = {
        "qe_epw": QEEPWCleaner,
        "qe_matdyn": QEMatdynCleaner,
        "qe_ph": QEPHCleaner,
        "qe_pw": QEPWCleaner,
        "qe_q2r": QEQ2RCleaner,
        }
__CALCTYPES_TO_CLEANER_CLS__.update(
        __ABINIT_CALCTYPES_TO_CLEANER_CLS__)
