import abc
import asyncio

from ..bases import BaseCalctypedUtility
from ..status_checkers.exceptions import (
        CalculationNotConvergedError,
        CalculationNotFinishedError,
        )


class BaseCleaner(BaseCalctypedUtility, abc.ABC):
    """Base class for any cleaner utility."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._calculation_directory = None
        self.nfiles_cleaned = 0
        self.nlinks_cleaned = 0
        self.ngb_cleaned = 0

    @property
    def calculation_directory(self):
        """Return the CalculationDirectory associated with the cleaner."""
        if self._calculation_directory is not None:
            return self._calculation_directory
        raise ValueError("Need to set the 'calculation_directory'.")

    async def set_calculation_directory(self, calc):
        """Sets calculation directory."""
        if isinstance(calc, str):
            from ..handlers import CalculationDirectory
            calc = await CalculationDirectory.from_calculation(
                    calc, loglevel=self._loglevel)
        self._calculation_directory = calc

    async def clean(self, force=False, dry=False):
        """Clean the calculation directory of cumbersome files.

        Cleans all non-necessary files after a calculation is done
        (wafefunctions, densities, etc.).

        If calculation has errored or is not finished, an error is thrown.

        Parameters
        ----------
        dry: bool, optional
            If True, dry mode is activated: files that will be deleted will
            only be printed on screen. No files are actually deleted.
        force: bool, optional
            If True, whatever the calculation status, the cleaning process
            will be engaged.
        """
        status = (await self.calculation_directory.status)[
                "calculation_finished"]
        self.nfiles_cleaned = 0
        self.ngb_cleaned = 0
        if status is not True and not force:
            raise CalculationNotFinishedError(self.calculation_directory)
        if "calculation_converged" in await self.calculation_directory.status:
            converged = (await self.calculation_directory.status)[
                    "calculation_converged"]
            if not converged and not force:
                raise CalculationNotConvergedError(self.calculation_directory)
        # calculation is ready to clean
        await asyncio.gather(
                *(self._clean_file_handler(handler, dry)
                    for handler in await self.get_files_to_delete()))

    async def _clean_file_handler(self, handler, dry):
        if not await handler.islink:
            self.ngb_cleaned += await handler.file_size
            self.nfiles_cleaned += 1
        else:
            self.nlinks_cleaned += 1
        if dry:
            print(handler.path)
        else:
            await handler.delete()

    async def get_files_to_delete(self):
        """Return the list of file handlers to delete.

        Returns
        -------
        list: The list of file handlers to delete.
        """
        files = []
        for handler in await self.calculation_directory.walk(paths_only=False):
            if await self.delete_this_file(handler):
                files.append(handler)
        return files

    async def delete_this_file(self, handler):
        """Return True if we want to delete this file.

        Parameters
        ----------
        handler: Handler object
            The file handler to investigate.

        Returns
        -------
        bool: True if we want to delete handler. False otherwise.
        """
        if await handler.islink:
            if await handler.is_broken_link:
                return True
        if handler.basename.startswith("core."):
            # delete core files
            return True
        return False
