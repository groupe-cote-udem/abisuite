from ..bases import BaseCleaner


class BaseQECleaner(BaseCleaner):
    """Base class for QE cleaners."""

    async def delete_this_file(self, handler):
        """Tells wether or not we clean a given file.

        Parameters
        ----------
        handler: File Handler object associated with the file we want to
                 delete.

        Returns
        -------
        bool: True if we delete, False otherwise.
        """
        if ".wfc" in handler.basename:
            return True
        if handler.basename.startswith("wfc") and (
                handler.basename.endswith(".dat")):
            return True
        return await super().delete_this_file(handler)
