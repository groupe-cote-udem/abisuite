from .bases import BaseQECleaner


class QEMatdynCleaner(BaseQECleaner):
    """Cleaner class for a qe_matdyn calculation.

    For now this class cleans nothing!
    """

    _calctype = "qe_matdyn"
    _loggername = "QEMatdynCleaner"

    async def get_files_to_delete(self):
        """For now this method only returns an empty list.

        Todo
        ----
        Implement me!
        """
        return []
