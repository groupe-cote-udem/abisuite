from .bases import BaseQECleaner


class QEQ2RCleaner(BaseQECleaner):
    """Cleaner class for a q2r calculation.

    For now this class cleans nothing!
    """

    _calctype = "qe_q2r"
    _loggername = "QEQ2RCleaner"

    async def get_files_to_delete(self):
        """For now this method returns only an empty list.

        Todo
        ----
        Implement me!
        """
        return []
