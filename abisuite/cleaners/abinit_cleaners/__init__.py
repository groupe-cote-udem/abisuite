from .abinit_cleaner import AbinitCleaner
from .anaddb_cleaner import AbinitAnaddbCleaner
from .mrgddb_cleaner import AbinitMrgddbCleaner
from .optic_cleaner import AbinitOpticCleaner


__ABINIT_CALCTYPES_TO_CLEANER_CLS__ = {
        "abinit": AbinitCleaner,
        "abinit_anaddb": AbinitAnaddbCleaner,
        "abinit_mrgddb": AbinitMrgddbCleaner,
        "abinit_optic": AbinitOpticCleaner,
        }
