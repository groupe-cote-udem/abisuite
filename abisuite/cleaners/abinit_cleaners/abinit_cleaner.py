"""Cleaners for abinit calculations."""
from ..bases import BaseCleaner


class AbinitCleaner(BaseCleaner):
    """Cleaner class for abinit calculations."""

    _calctype = "abinit"
    _loggername = "AbinitCleaner"

    async def delete_this_file(self, handler):
        """Tells wether or not we delete a file.

        Parameters
        ----------
        handler: The File handler object associated with the file.

        Returns
        -------
        bool: True if we delete the file. False otherwise.
        """
        for suffix in ("_WFK", "_DEN", "_1WF"):
            if suffix in handler.basename:
                return True
        return await super().delete_this_file(handler)
