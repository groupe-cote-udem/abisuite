"""Cleaners for abinit's anaddb calculations."""
from ..bases import BaseCleaner


class AbinitAnaddbCleaner(BaseCleaner):
    """Cleaner class for an anaddb calculation."""

    _calctype = "abinit_anaddb"
    _loggername = "AbinitAnaddbCleaner"

    def delete_this_file(self, handler):
        """Tells if wether or not we delete the file.

        Parameters
        ----------
        handler: FileHandler object

        Returns
        -------
        bool: True if we delete it. False otherwise.
        """
        return super().delete_this_file(handler)
