"""Cleaners for mrgddb calculations."""
from ..bases import BaseCleaner


class AbinitMrgddbCleaner(BaseCleaner):
    """Cleaner class for a mrgddb calculation."""

    _calctype = "abinit_mrgddb"
    _loggername = "AbinitMrgddbCleaner"

    def delete_this_file(self, handler):
        """Tells wether or not we delete this handler.

        Parameters
        ----------
        handler: File Handler object associated with the file.

        Returns
        -------
        bool: True if we delete. False otherwise.
        """
        return super().delete_this_file(handler)
