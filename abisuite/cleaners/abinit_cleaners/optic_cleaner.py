"""Cleaners for abinit optic calculations."""
from ..bases import BaseCleaner


class AbinitOpticCleaner(BaseCleaner):
    """Cleaner class for an optic calculation."""

    _calctype = "abinit_optic"
    _loggername = "AbinitOpticCleaner"

    async def delete_this_file(self, handler):
        """Tells wether or not we delete this handler.

        Parameters
        ----------
        handler: File Handler object representing a file.

        Returns
        -------
        bool: True if we delete. False otherwise.
        """
        for suffix in ("_1WF", "_WFK", ):
            if suffix in handler.basename:
                return True
        return await super().delete_this_file(handler)
