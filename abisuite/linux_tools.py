import os

import aiofiles.os

import aioshutil

from .colors import Colors
from .exceptions import DirectoryExistsError
from .routines import full_abspath


class cd:  # noqa: N801
    """Context manager for changing the current working directory.

    Taken from https://stackoverflow.com/a/13197763/6362595
    """

    def __init__(self, newpath):
        self.newPath = full_abspath(newpath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


async def which(name):
    """Python equivalent to the which Linux command."""
    return await aioshutil.which(name)


async def touch(path, sync_open: bool = False) -> None:
    """Python equivalent to the 'touch' linux script.

    Parameters
    ----------
    sync_open: bool, optional
        If True, file is touched 'synchronously' but only for file
        creation part. This is useful if one fears to have
        too many files open because this method is called
        a lot of times.
    """
    if await aiofiles.os.path.isdir(path):
        raise DirectoryExistsError(path)
    if sync_open:
        with open(path, "a"):
            pass
    else:
        async with aiofiles.open(path, "a"):
            pass


async def rmdir(dirpath):
    """Remove an empty directory.

    Raises an error if directory is not empty.
    """
    dirpath = full_abspath(dirpath)
    if not await aiofiles.os.path.isdir(dirpath):
        raise NotADirectoryError(dirpath)
    if len(await aiofiles.os.listdir(dirpath)):
        raise FileExistsError(f"There are files inside '{dirpath}'")
    await aiofiles.os.rmdir(dirpath)


def mkdir_sync(path: str) -> None:
    """Same as mkdir but not async."""
    if os.path.isfile(path):
        raise FileExistsError(f"{path} is a file!")
    if os.path.isdir(path):
        return
    os.makedirs(full_abspath(path), exist_ok=True)


async def mkdir(path):
    """Python equivalent to mkdir linux tool with a few checks."""
    if await aiofiles.os.path.isfile(path):
        raise FileExistsError(f"{path} is a file!")
    if await aiofiles.os.path.isdir(path):
        return
    await aiofiles.os.makedirs(full_abspath(path), exist_ok=True)


async def cat(*paths, pipe=None, overwrite=False, line_limit=None):
    """Similar to the cat linux tool.

    It prints the content of a file on
    screen.

    Parameters
    ----------
    paths: the list of paths to use cat on.
    pipe: str, optional
        Where to pipe the files. If None, everything is printed on screen.
    overwrite: bool, optional
        If True and the piped file already exists, it will be overwritten.
    line_limit: bool, optional
        If not None anc 'pipe' is None, then only 'line_limit' lines
        maximum will be printed on sreen. If the file (or concatanation
        of files) is longer than this
        limit, it will be stated on screen.

    Raises
    ------
    FileNotFoundError: If one of the path to cat is not found.
    FileExistsError: If the piped file exists and is not overwritten.
    TypeError: If 'line_limit' is not an int when it is not None.
    """
    if not len(paths):
        # nothing to do
        return
    if line_limit is not None:
        if not isinstance(line_limit, int):
            raise TypeError(
                    f"Expected int for 'line_limit' but got '{line_limit}'.")
    lines = []
    for path in paths:
        if not await aiofiles.os.path.isfile(path):
            raise FileNotFoundError(path)
        async with aiofiles.open(path, "r") as f:
            try:
                lines += await f.readlines()
            except UnicodeDecodeError:
                print(Colors.color_text("ENCONDING ERROR IN:", "red", "bold") +
                      f"'{path}'.")
    if pipe is None:
        lines = [x.rstrip("\n") for x in lines]
        if not lines:
            echo("< nothing to echo >")
        if line_limit is None:
            echo(*lines)
        else:
            if len(lines) < line_limit:
                echo(*lines)
            else:
                echo(*lines[:line_limit // 2])
                print()
                print(Colors.bold_text("[...]"))
                print()
                echo(*lines[-line_limit // 2:])
                print("The number of lines to print exceeded the given line "
                      f"limit of {line_limit} lines.")
        return
    if await aiofiles.os.path.isfile(pipe):
        if overwrite:
            await aiofiles.os.remove(path)
        else:
            raise FileExistsError(pipe)
    async with aiofiles.open(pipe, "w") as f:
        await f.write("".join(lines))


def echo(*args):
    """Prints all the args given."""
    for arg in args:
        print(arg)


async def ln_s(source, target):
    """Creates a symlink at location 'target' that points to 'source'."""
    dirname = os.path.dirname(target)
    if not os.path.exists(dirname):
        await mkdir(dirname)
    await aiofiles.os.symlink(
            source, target,
            target_is_directory=await aiofiles.os.path.isdir(source),
            )
