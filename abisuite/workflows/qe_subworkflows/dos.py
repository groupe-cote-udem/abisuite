from ..bases_subworkflows import BaseDOSSubWorkflow


class QEDOSSubWorkflow(BaseDOSSubWorkflow):
    """Subworkflow class for dos subworkflows."""

    _loggername = "QEDOSSubWorkflow"
