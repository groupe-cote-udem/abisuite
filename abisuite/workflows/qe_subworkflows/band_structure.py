from ..bases_subworkflows import BaseBandStructureSubWorkflow


class QEBandStructureSubWorkflow(BaseBandStructureSubWorkflow):
    """SubWorkflow class for QE Band Structure."""

    _loggername = "QEBandStructureSubWorkflow"
