import os
from typing import Any

from ..bases_subworkflows import (
        BaseSubWorkflowWithPhononDispersionPart, subparts_checkpoint)
from ...sequencers.bases import BaseSequencer


class QEPhononDispersionSubWorkflow(BaseSubWorkflowWithPhononDispersionPart):
    """QE subworkflow for phonon dispersions."""

    _loggername = "QEPhononDispersionSubWorkflow"

    def __init__(
            self, *args,
            asr: str = None,
            q2r_input_variables: dict[str, Any] = None,
            q2r_calculation_parameters: dict[str, Any] = None,
            matdyn_calculation_parameters: dict[str, Any] = None,
            **kwargs,
            ):
        BaseSubWorkflowWithPhononDispersionPart.__init__(self, *args, **kwargs)
        self.asr = asr
        self.q2r_input_variables = q2r_input_variables
        self.q2r_calculation_parameters = q2r_calculation_parameters
        self.matdyn_calculation_parameters = matdyn_calculation_parameters
        self.checks["q2r"] = False
        self.checks["matdyn"] = False

    def check(
            self, skip_to_scf: bool = False, check_matdyn: bool = True,
            check_q2r: bool = True, check_phonons: bool = True,
            **kwargs,
            ) -> None:
        """Check that everything is consistent and all variables are set."""
        BaseSubWorkflowWithPhononDispersionPart.check(
                self,
                skip_to_scf=skip_to_scf, check_phonons=check_phonons,
                **kwargs,
                )
        if skip_to_scf:
            # don't check all phonon disp stuff.
            self.ready_to_set_sequencer = True
            return
        if check_phonons:
            self.check_phonons()
        else:
            self.checks["phonons"] = True
        if check_q2r:
            self.check_q2r()
        else:
            self.checks["q2r"] = True
        if check_matdyn:
            self.check_matdyn()
        else:
            self.checks["matdyn"] = True

    def check_q2r(self) -> None:
        """Check q2r part of subworkflow."""
        if self.q2r_input_variables is None:
            raise ValueError("Need to set 'q2r_input_variables'.")
        if self.q2r_calculation_parameters is None:
            raise ValueError(
                    "Need to set 'q2r_calculation_parameters'.")
        self.checks["q2r"] = True

    def check_matdyn(self) -> None:
        """Check matdyn part of subworkflow."""
        if self.asr is None:
            raise ValueError("Need to set 'asr'.")
        if self.matdyn_calculation_parameters is None:
            raise ValueError(
                    "Need to set 'matdyn_calculation_parameters'.")
        if self.qpoint_path is None:
            raise ValueError("Need to set 'qpoint_path'.")
        if self.qpoint_path_density is None:
            raise ValueError("Need to set 'qpoint_path_density.'")
        self.checks["matdyn"] = True

    async def set_sequencer(self):
        """Set the sequencer."""
        await BaseSubWorkflowWithPhononDispersionPart.set_sequencer(self)
        await self.set_q2r_part()
        await self.set_matdyn_part()

    @subparts_checkpoint("q2r")
    async def set_q2r_part(
            self, sequencer: BaseSequencer = None,
            q2r_workdir: str = None,
            ) -> None:
        """Set the q2r part of the sequencer."""
        if q2r_workdir is None:
            q2r_workdir = os.path.join(self.root_workdir, "q2r_run")
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.q2r_workdir = q2r_workdir
        sequencer.q2r_input_variables = self.q2r_input_variables
        self._add_calculation_parameters(
                "q2r_", self.q2r_calculation_parameters, sequencer)

    @subparts_checkpoint("matdyn")
    async def set_matdyn_part(
            self, sequencer: BaseSequencer = None,
            matdyn_workdir: str = None,
            ) -> None:
        """Set the matdyn part of the sequencer."""
        if matdyn_workdir is None:
            matdyn_workdir = os.path.join(self.root_workdir, "matdyn_run")
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.matdyn_workdir = matdyn_workdir
        sequencer.asr = self.asr
        self._add_calculation_parameters(
                "matdyn_", self.matdyn_calculation_parameters, sequencer)
        sequencer.qpoint_path = self.qpoint_path
        sequencer.qpoint_path_density = self.qpoint_path_density
        sequencer.matdyn_input_variables = {}
