from ..bases_subworkflows import BaseSubWorkflowWithSCFPart


class QEGSSubWorkflow(BaseSubWorkflowWithSCFPart):
    """Subworkflow class for a QE GS calculation."""

    _loggername = "QEGSSubWorkflow"
