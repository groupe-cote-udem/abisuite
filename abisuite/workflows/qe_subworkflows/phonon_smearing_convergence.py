from ..bases_subworkflows import (
        BaseSubWorkflowWithPhononSmearingConvergencePart,
        )


class QEPhononSmearingConvergenceSubWorkflow(
        BaseSubWorkflowWithPhononSmearingConvergencePart):
    """Subworkflow class for phonon smearing convergence."""

    _loggername = "QEPhononSmearingConvergenceSubWorkflow"
