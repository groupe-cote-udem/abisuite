import os
from typing import Sequence

from .epw_wannierization import QEEPWWannierizationSubWorkflow
from .ibte import QEIBTESubWorkflow
from .ibte_convergence_bases import (
        _BaseQEIBTEConvergencePostProcessSubWorkflow)
from ..bases_subworkflows import subparts_checkpoint
from ...exceptions import DevError
from ...handlers import QEEPWInputFile
from ...routines import is_list_like, vectors_equal
from ...sequencers import (
        QEEPWIBTESequencer,
        QEEPWWithPhononInterpolationSequencer,
        )
from ...sequencers.bases import BaseSequencer


class _BaseQEIBTECoarseGridConvergenceSubWorkflow(
        QEEPWWannierizationSubWorkflow, QEIBTESubWorkflow):

    def __init__(self, *args, **kwargs):
        QEIBTESubWorkflow.__init__(
                self, *args,
                sequencer=kwargs.pop("sequencer", None),
                workflow=kwargs.pop("workflow", None),
                ibte_calculation_parameters=kwargs.pop(
                    "ibte_calculation_parameters", None),
                ibte_input_variables=kwargs.pop(
                    "ibte_input_variables", None),
                use_ibte_converged_fsthick=kwargs.pop(
                    "use_ibte_converged_fsthick", None),
                )
        QEEPWWannierizationSubWorkflow.__init__(
                self, *args, **kwargs)

    async def check(self, *args, **kwargs) -> None:
        """Check every part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.check(
                self, *args, **kwargs)
        await QEIBTESubWorkflow.check(self)

    async def check_ibte(self) -> None:
        """Check ibte part of subworkflow."""
        await QEIBTESubWorkflow.check_ibte(
                self, check_epw_wannierization_sequencer=False)

    async def apply_converged_quantities(self):
        """Apply converged quantities."""
        await QEEPWWannierizationSubWorkflow.apply_converged_quantities(self)
        await QEIBTESubWorkflow.apply_converged_quantities(self)

    @subparts_checkpoint("plot", propagate=True)
    def set_plots(
            self, *args, subdir: str = "", **kwargs):
        pcp = self.plot_calculation_parameters.copy()
        pcp.update(
                {"save": os.path.join(
                    self.root_workdir, subdir, "results",
                    "epw_wannierization.pdf"),
                 "save_pickle": os.path.join(
                    self.root_workdir, subdir, "results",
                    "epw_wannierization.pickle")
                 })
        QEEPWWannierizationSubWorkflow.set_plots(
                self, *args, plot_calculation_parameters=pcp, **kwargs)


class QEIBTECoarseKgridConvergenceSubWorkflow(
        _BaseQEIBTECoarseGridConvergenceSubWorkflow):
    """Subworkflow class for an IBTE coarse kgrid convergence."""

    _loggername = "QEIBTECoarseKgridConvergenceSubWorkflow"
    _plot_name_extension = "ibte_coarse_kgrid_convergence"

    def __init__(
            self, *args, nscf_kgrids: Sequence[Sequence[int]] = None,
            **kwargs,
            ) -> None:
        _BaseQEIBTECoarseGridConvergenceSubWorkflow.__init__(
                self, *args, **kwargs)
        self.nscf_kgrids = nscf_kgrids

    async def check_nscf(self) -> None:
        """Check nscf part of subworkflow."""
        if not is_list_like(self.nscf_kgrids):
            raise ValueError(
                    "nscf_kgrids should be a list of kgrids "
                    f"but got {self.nscf_kgrids}.")
        # kgrid must be commensurate with qgrid
        if self.use_phonon_dispersion:
            seq = self.workflow.phonon_dispersion_sequencer
            qgrid = seq.phonons_qpoint_grid
        else:
            qgrid = self.phonons_qpoint_grid
        for nscf_kgrid in self.nscf_kgrids:
            for k, q in zip(nscf_kgrid, qgrid):
                if k % q:
                    raise ValueError(
                            f"kgrid {nscf_kgrid} not commensurate with qgrid "
                            f"{qgrid}")
        await QEEPWWannierizationSubWorkflow.check_nscf(
                self, check_nscf_kgrid=False)

    async def set_sequencer(self) -> None:
        """Set sequencer."""
        for nscf_kgrid in self.nscf_kgrids:
            subdir = "coarse_kgrid_" + "x".join((str(x) for x in nscf_kgrid))
            epwsequencer = QEEPWWithPhononInterpolationSequencer(
                loglevel=self._loglevel)
            await self.set_scf_part(
                    sequencer=epwsequencer,
                    scf_workdir=os.path.join(
                        self.root_workdir, subdir, "scf_run"),
                    set_parts=False,
                    )
            await self.set_nscf_part(
                    sequencer=epwsequencer,
                    nscf_workdir=os.path.join(
                        self.root_workdir, subdir, "nscf_run"),
                    set_parts=False, nscf_kgrid=nscf_kgrid,
                    )
            await self.set_band_structure_part(
                    sequencer=epwsequencer,
                    band_structure_workdir=os.path.join(
                        self.root_workdir, subdir, "band_structure_run"),
                    set_parts=False,
                    )
            await self.set_phonons_part(
                    sequencer=epwsequencer,
                    phonons_workdir=os.path.join(
                        self.root_workdir, subdir, "phonons_runs", "ph"),
                    set_parts=False,
                    )
            await self.set_q2r_part(
                    sequencer=epwsequencer,
                    q2r_workdir=os.path.join(
                        self.root_workdir, subdir, "q2r_run"),
                    set_parts=False)
            await self.set_matdyn_part(
                    sequencer=epwsequencer,
                    matdyn_workdir=os.path.join(
                        self.root_workdir, subdir, "matdyn_run"),
                    set_parts=False,
                    )
            await self.set_epw_wannierization_part(
                    sequencer=epwsequencer,
                    epw_wannierization_workdir=os.path.join(
                        self.root_workdir, subdir, "epw_wannierization_run"),
                    set_parts=False,
                    )
            await self.set_epw_dispersion_interpolation_part(
                    sequencer=epwsequencer,
                    epw_dispersion_interpolation_workdir=os.path.join(
                        self.root_workdir, subdir,
                        "epw_dispersion_interpolation_run",
                        ),
                    set_parts=False,
                    )
            self.set_plots(
                    sequencer=epwsequencer, set_parts=False,
                    subdir=subdir,
                    )
            # epw wannierization part done, append seq to list of seqs
            self.sequencer.append(epwsequencer)
            # now do ibte part
            ibtesequencer = QEEPWIBTESequencer(loglevel=self._loglevel)
            await self.set_ibte_part(
                sequencer=ibtesequencer,
                ibte_workdir=os.path.join(
                    self.root_workdir, subdir, "ibte_run"),
                set_parts=False,
                epw_wannierization_workdir=epwsequencer.epw_workdir,
                set_epw_wannierization_workdir=(
                    await epwsequencer.sequence_completed),
                )
            self.set_plots(
                    sequencer=ibtesequencer,
                    subdir=subdir,
                    set_parts=False,
                    )
            self.sequencer.append(ibtesequencer)


class _BaseQEIBTECoarseGridConvergencePostProcessSubWorkflow(
        _BaseQEIBTEConvergencePostProcessSubWorkflow):
    _label_symbol: str = None

    def __init__(self, *args, **kwargs):
        _BaseQEIBTEConvergencePostProcessSubWorkflow.__init__(
                self, *args, **kwargs)
        if self._label_symbol is None:
            raise DevError("Need to set '_label_symbol'.")


class QEIBTECoarseKgridConvergencePostProcessSubWorkflow(
        _BaseQEIBTECoarseGridConvergencePostProcessSubWorkflow):
    """Post process subworkflow class for ibte coarse kgrid convergence."""

    _loggername = "QEIBTECoarseKgridConvergencePostProcessSubWorkflow"
    _label_symbol = r"$N_{\mathbf{k}}$"

    async def _extract_label_from_sequencer(
            self, sequencer: QEEPWIBTESequencer) -> str:
        async with await QEEPWInputFile.from_calculation(
                sequencer.epw_wannierization_calculation,
                loglevel=self._loglevel) as inp:
            iv = inp.input_variables
            kgrid = [iv["nk1"].value, iv["nk2"].value, iv["nk3"].value]
        return f"{self._label_symbol} = " + "x".join(
                [str(k) for k in kgrid])


class QEIBTECoarseQgridConvergenceSubWorkflow(
        _BaseQEIBTECoarseGridConvergenceSubWorkflow):
    """Subworkflow class for an IBTE coarse qgrid convergence."""

    _loggername = "QEIBTECoarseQgridConvergenceSubWorkflow"
    _plot_name_extension = "ibte_coarse_qgrid_convergence"

    def __init__(
            self, *args,
            match_nscf_kgrid_if_not_commensurate: bool = None,
            pause_after_epw: bool = False,
            phonons_qpoint_grids: Sequence[Sequence[int]] = None,
            use_ibte_coarse_kgrid_convergence: bool = False,
            **kwargs,
            ) -> None:
        _BaseQEIBTECoarseGridConvergenceSubWorkflow.__init__(
                self, *args, **kwargs)
        self.match_nscf_kgrid_if_not_commensurate = (
                match_nscf_kgrid_if_not_commensurate)
        self.pause_after_epw = pause_after_epw
        self.phonons_qpoint_grids = phonons_qpoint_grids
        self.use_ibte_coarse_kgrid_convergence = (
                use_ibte_coarse_kgrid_convergence)

    async def check(self) -> None:
        """Check ibte coarse qgrid convergence sub workflow."""
        await _BaseQEIBTECoarseGridConvergenceSubWorkflow.check(
                self, force_check_phonons=True,
                force_check_matdyn=True,
                force_check_q2r=True,
                )

    async def check_nscf(self) -> None:
        """Check nscf part of subworkflow."""
        if self.checks["nscf"]:
            return
        await _BaseQEIBTECoarseGridConvergenceSubWorkflow.check_nscf(self)
        if self.use_ibte_coarse_kgrid_convergence:
            # use data from ibte coarse kgrid convergence
            # check that the nscf_kgrid is tested in the coarse kgrid test
            self._check_nscf_kgrid_from_kgrid_convergence(self.nscf_kgrid)

    def _check_nscf_kgrid_from_kgrid_convergence(self, nscf_kgrid) -> None:
        seqs = self.workflow.ibte_coarse_kgrid_convergence_sequencer
        for seq in seqs:
            if not isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                continue
            if vectors_equal(nscf_kgrid, seq.nscf_kgrid):
                break
        else:
            self.checks["nscf"] = False
            raise ValueError(
                    "Cannot 'use_ibte_coarse_kgrid_convergence' because "
                    "nscf_kgrid={nscf_kgrid} is not tested in coarse "
                    "kgrid convergence.")

    def check_phonons(self) -> None:
        """Check phonon part of subworkflow."""
        if self.phonons_qpoint_grids is None:
            raise ValueError("Need to set 'phonons_qpoint_grids'.")
        if not is_list_like(self.phonons_qpoint_grids):
            raise ValueError(
                    "phonons_qpoint_grids should be a list of qgrids "
                    f"but got {self.phonons_qpoint_grids}.")
        if self.use_phonon_dispersion:
            seq = self.workflow.phonon_dispersion_sequencer
            phdisp_grid = seq.phonons_qpoint_grid
            for qgrid in self.phonons_qpoint_grids:
                if vectors_equal(qgrid, phdisp_grid):
                    break
            else:
                raise ValueError(
                        "'use_phonon_dispersion' is True but the phonon_disp"
                        "grid {phdisp_grid} is not present in list of qgrids"
                        " to test convergence on.")
        self.checks["phonons"] = True

    @subparts_checkpoint("nscf", propagate=True)
    async def set_nscf_part(
            self, *args, qgrid: Sequence[int] = None, **kwargs) -> None:
        """Set nscf part of subworkflow."""
        # kgrid must be commensurate with qgrid
        try:
            for k, q in zip(self.nscf_kgrid, qgrid):
                if k % q:
                    raise ValueError(
                        f"kgrid {self.nscf_kgrid} not commensurate with qgrid "
                        f"{qgrid}")
        except ValueError:
            if not self.match_nscf_kgrid_if_not_commensurate:
                raise
            # just adapt nscf kgrid in case it is too coarse
            nscf_kgrid = qgrid
            if self.use_ibte_coarse_kgrid_convergence:
                # make sure beforehand that it is tested in kgrid convergence
                try:
                    self._check_nscf_kgrid_from_kgrid_convergence(nscf_kgrid)
                except ValueError as err:
                    self._logger.error(
                            "Matching kgrid to qgrid is not computed in "
                            "ibte coarse kgrid convergence test.")
                    raise err
        else:
            nscf_kgrid = self.nscf_kgrid
        if self.use_ibte_coarse_kgrid_convergence:
            # if calculation was done already in coarse kgrid convergence
            # link it here
            seqs = self.workflow.ibte_coarse_kgrid_convergence_sequencer
            for seq in seqs:
                if not isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                    continue
                if vectors_equal(nscf_kgrid, seq.nscf_kgrid):
                    cls = _BaseQEIBTECoarseGridConvergenceSubWorkflow
                    await cls.set_nscf_part(
                            self, *args, nscf_kgrid=nscf_kgrid,
                            use_nscf_from=seq.nscf_workdir,
                            **kwargs)
                    return
        await _BaseQEIBTECoarseGridConvergenceSubWorkflow.set_nscf_part(
                self, *args, nscf_kgrid=nscf_kgrid, **kwargs)

    @subparts_checkpoint("epw", propagate=True)
    async def set_epw_wannierization_part(
            self, *args, sequencer: BaseSequencer = None, **kwargs) -> None:
        """Set epw wannierization part."""
        cls = _BaseQEIBTECoarseGridConvergenceSubWorkflow
        if not self.use_ibte_coarse_kgrid_convergence:
            return await cls.set_epw_wannierization_part(
                    self, *args, sequencer=sequencer, **kwargs)
        # check if a matching epw calc exists. if so link it
        kgrid = sequencer.nscf_kgrid
        qgrid = sequencer.phonons_qpoint_grid
        seqs = self.workflow.ibte_coarse_kgrid_convergence_sequencer
        for seq in seqs:
            if not isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                continue
            if vectors_equal(kgrid, seq.nscf_kgrid) and vectors_equal(
                    qgrid, seq.phonons_qpoint_grid):
                self._logger.info(
                        "Using epw wannierization calculation from the ibte "
                        "coarse kgrid part of workflow for "
                        f"qgrid={qgrid} and kgrid={kgrid}")
                return await cls.set_epw_wannierization_part(
                        self,
                        *args, sequencer=sequencer,
                        use_epw_wannierization_from=seq.epw_workdir,
                        **kwargs)
        # no matching calculation
        self._logger.info(
                "Need new wannierization calculation for qgrid="
                f"{qgrid} and kgrid={kgrid}")
        return await cls.set_epw_wannierization_part(
                self,
                *args, sequencer=sequencer, **kwargs)

    @subparts_checkpoint("epw_dispersion_interpolation", propagate=True)
    async def set_epw_dispersion_interpolation_part(
            self, *args, sequencer: BaseSequencer = None, **kwargs) -> None:
        """Set epw wannierization part."""
        cls = _BaseQEIBTECoarseGridConvergenceSubWorkflow
        if not self.use_ibte_coarse_kgrid_convergence:
            return await cls.set_epw_dispersion_interpolation_part(
                    self,
                    *args, sequencer=sequencer, **kwargs)
        # check if a matching epw calc exists. if so link it
        kgrid = sequencer.nscf_kgrid
        qgrid = sequencer.phonons_qpoint_grid
        seqs = self.workflow.ibte_coarse_kgrid_convergence_sequencer
        for seq in seqs:
            if not isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                continue
            if vectors_equal(kgrid, seq.nscf_kgrid) and vectors_equal(
                    qgrid, seq.phonons_qpoint_grid):
                return await cls.set_epw_dispersion_interpolation_part(
                        self,
                        *args, sequencer=sequencer,
                        use_epw_dispersion_interpolation_from=(
                            seq.epwdispinterpolation_workdir),
                        **kwargs)
        # no matching calculation
        return await cls.set_epw_dispersion_interpolation_part(
                self,
                *args, sequencer=sequencer, **kwargs)

    @subparts_checkpoint("ibte", propagate=True)
    async def set_ibte_part(
            self, *args, sequencer: BaseSequencer = None,
            epwsequencer: BaseSequencer = None,
            **kwargs) -> None:
        """Set ibte part."""
        cls = _BaseQEIBTECoarseGridConvergenceSubWorkflow
        if not self.use_ibte_coarse_kgrid_convergence:
            return await cls.set_ibte_part(
                    self,
                    *args, sequencer=sequencer,
                    epw_wannierization_workdir=epwsequencer.epw_workdir,
                    **kwargs)
        # check if a matching ibte calc exists. if so link it
        seqs = self.workflow.ibte_coarse_kgrid_convergence_sequencer
        # find ibte sequencer with this specific epw sequencer linked
        for seq in seqs:
            if not isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                continue
            if seq.epw_workdir != epwsequencer.epw_workdir:
                continue
            # we have a matching calculation here
            # thus there should be a corresponding ibte calc
            # (unless not written yet)
            if not await seq.sequence_completed:
                self._logger.info(
                        "EPW wannierization sequence not completed but found "
                        "matching calculations to ibte coarse q grid conv.")
                return  # dont set yet
            # there should be a corresponding ibte calc -> find it
            for seq2 in seqs:
                if isinstance(seq, QEEPWWithPhononInterpolationSequencer):
                    continue
                try:
                    if seq2.epw_wannierization_calculation == (
                            epwsequencer.epw_workdir):
                        # got it
                        return await cls.set_ibte_part(
                                self,
                                *args, sequencer=sequencer,
                                use_ibte_from=seq2.ibte_workdir,
                                epw_wannierization_workdir=seq.epw_workdir,
                                **kwargs)
                except ValueError:
                    # epw wannierization calculation not set
                    self._logger.error(
                            f"{kwargs.get('ibte_workdir', 'ibte')} not set.")
                    continue
            else:
                return  # don't set nothing else there SHOULD be one...
        # no matching ibte calculation from ibte coarse kgrid convergence
        return await cls.set_ibte_part(
                self,
                *args, sequencer=sequencer,
                epw_wannierization_workdir=epwsequencer.epw_workdir,
                **kwargs)

    async def set_sequencer(self) -> None:
        """Set sequencer."""
        phdisp_qgrid = None
        if self.use_phonon_dispersion:
            seq = self.workflow.phonon_dispersion_sequencer
            phdisp_qgrid = seq.phonons_qpoint_grid
        for qgrid in self.phonons_qpoint_grids:
            subdir = "coarse_qgrid_" + "x".join((str(x) for x in qgrid))
            epwsequencer = QEEPWWithPhononInterpolationSequencer(
                loglevel=self._loglevel)
            await self.set_scf_part(
                    sequencer=epwsequencer,
                    scf_workdir=os.path.join(
                        self.root_workdir, subdir, "scf_run"),
                    set_parts=False,
                    )
            await self.set_nscf_part(
                    sequencer=epwsequencer,
                    nscf_workdir=os.path.join(
                        self.root_workdir, subdir, "nscf_run"),
                    set_parts=False, qgrid=qgrid,
                    )
            # same band structure calc for all
            await self.set_band_structure_part(
                    sequencer=epwsequencer,
                    band_structure_workdir=os.path.join(
                        self.root_workdir, "band_structure_run"),
                    set_parts=False,
                    )
            if not vectors_equal(qgrid, phdisp_qgrid):
                # manually set phonon parts
                if self.use_phonon_dispersion:
                    self._logger.info(
                            "Not using 'phonon_dispersion' calculations for "
                            f"qgrid={qgrid}")
                await self.set_phonons_part(
                        sequencer=epwsequencer,
                        phonons_workdir=os.path.join(
                            self.root_workdir, subdir, "phonons_runs", "ph"),
                        use_phonon_dispersion=False,
                        set_parts=False, phonons_qpoint_grid=qgrid,
                        )
                await self.set_q2r_part(
                        sequencer=epwsequencer,
                        q2r_workdir=os.path.join(
                            self.root_workdir, subdir, "q2r_run"),
                        use_phonon_dispersion=False,
                        set_parts=False)
                await self.set_matdyn_part(
                        sequencer=epwsequencer,
                        matdyn_workdir=os.path.join(
                            self.root_workdir, subdir, "matdyn_run"),
                        use_phonon_dispersion=False,
                        set_parts=False,
                        )
            else:
                # use phonon dispersion part of workflow
                self._logger.info(
                    "Using phonons calculations from phonon_dispersion for "
                    f"qgrid={qgrid}")
                await self.set_phonons_part(
                        sequencer=epwsequencer,
                        phonons_workdir=seq.phonons_workdir,
                        set_parts=False, phonons_qpoint_grid=qgrid,
                        use_phonon_dispersion=True,
                        )
                await self.set_q2r_part(
                        sequencer=epwsequencer,
                        q2r_workdir=seq.q2r_workdir,
                        set_parts=False,
                        use_phonon_dispersion=True,
                        )
                await self.set_matdyn_part(
                        sequencer=epwsequencer,
                        matdyn_workdir=seq.matdyn_workdir,
                        set_parts=False,
                        use_phonon_dispersion=False,
                        )
            await self.set_epw_wannierization_part(
                    sequencer=epwsequencer,
                    epw_wannierization_workdir=os.path.join(
                        self.root_workdir, subdir, "epw_wannierization_run"),
                    set_parts=False,
                    )
            await self.set_epw_dispersion_interpolation_part(
                    sequencer=epwsequencer,
                    epw_dispersion_interpolation_workdir=os.path.join(
                        self.root_workdir, subdir,
                        "epw_dispersion_interpolation_run",
                        ),
                    set_parts=False,
                    )
            self.set_plots(
                    sequencer=epwsequencer, set_parts=False,
                    subdir=subdir,
                    )
            # epw wannierization part done, append seq to list of seqs
            self.sequencer.append(epwsequencer)
            if self.pause_after_epw:
                # allow for double checks by user
                continue
            # now do ibte part
            ibtesequencer = QEEPWIBTESequencer(loglevel=self._loglevel)
            await self.set_ibte_part(
                sequencer=ibtesequencer,
                ibte_workdir=os.path.join(
                    self.root_workdir, subdir, "ibte_run"),
                set_parts=False,
                epwsequencer=epwsequencer,
                # epw_wannierization_workdir=epwsequencer.epw_workdir,
                set_epw_wannierization_workdir=(
                    await epwsequencer.sequence_completed),
                )
            self.set_plots(
                    sequencer=ibtesequencer,
                    set_parts=False,
                    subdir=subdir,
                    )
            self.sequencer.append(ibtesequencer)


class QEIBTECoarseQgridConvergencePostProcessSubWorkflow(
        _BaseQEIBTECoarseGridConvergencePostProcessSubWorkflow):
    """Post process subworkflow class for ibte coarse qgrid convergence."""

    _loggername = "QEIBTECoarseQgridConvergencePostProcessSubWorkflow"
    _label_symbol = r"$N_{\mathbf{q}}$"

    async def _extract_label_from_sequencer(
            self, sequencer: QEEPWIBTESequencer) -> str:
        async with await QEEPWInputFile.from_calculation(
                sequencer.ibte_workdir,
                loglevel=self._loglevel) as inp:
            iv = inp.input_variables
            qgrid = [iv["nq1"].value, iv["nq2"].value, iv["nq3"].value]
        return f"{self._label_symbol} = " + "x".join(
                [str(q) for q in qgrid])
