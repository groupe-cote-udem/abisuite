import os
from typing import Any, Sequence

from .band_structure import QEBandStructureSubWorkflow
from .phonon_dispersion import QEPhononDispersionSubWorkflow
from ..bases_subworkflows import (
        BaseSubWorkflowWithNSCFPart, subparts_checkpoint,
        )
from ...sequencers.bases import BaseSequencer


class QEEPWWannierizationSubWorkflow(
        QEPhononDispersionSubWorkflow, BaseSubWorkflowWithNSCFPart,
        QEBandStructureSubWorkflow,
        ):
    """Subworkflow class for the epw_wannierization part of a workflow."""

    _loggername = "QEEPWWannerizationSubWorkflow"
    _have_plots = True
    _plot_name_extension = "epw_wannierization"

    def __init__(
            self, *args,
            nscf_kgrid: Sequence[int] = None,
            epw_input_variables: dict[str, Any] = None,
            epw_calculation_parameters: dict[str, Any] = None,
            use_phonon_dispersion: bool = False,
            **kwargs,
            ):
        nscf_kwargs = {}
        for name in [
                "use_nscf_from", "nscf_calculation_parameters",
                "nscf_input_variables", "sequencer", "workflow",
                ]:
            if name in kwargs:
                nscf_kwargs[name] = kwargs.pop(name)
        bs_kwargs = {}
        for name in [
                "use_band_structure_from", "use_band_structure",
                "band_structure_input_variables",
                "band_structure_calculation_parameters",
                "band_structure_kpoint_path",
                "band_structure_kpoint_path_density",
                ]:
            if name in kwargs:
                bs_kwargs[name] = kwargs.pop(name)
        BaseSubWorkflowWithNSCFPart.__init__(
                self, *args, **nscf_kwargs)
        QEBandStructureSubWorkflow.__init__(
                self, *args, **bs_kwargs)
        QEPhononDispersionSubWorkflow.__init__(
                self, *args, **kwargs)

        self.nscf_kgrid = nscf_kgrid
        self.epw_input_variables = epw_input_variables
        self.epw_calculation_parameters = epw_calculation_parameters
        self.use_phonon_dispersion = use_phonon_dispersion
        self.checks["epw"] = False

    @property
    def gs_sequencer(self):
        """The GS sequencer."""
        return self.workflow.gs_sequencer

    @property
    def phonon_dispersion_sequencer(self):
        """The phonon dispersion sequencer."""
        return self.workflow.phonon_dispersion_sequencer

    async def apply_converged_quantities(self):
        """Apply converged quantities to given dicts of input variables."""
        await QEPhononDispersionSubWorkflow.apply_converged_quantities(self)
        await BaseSubWorkflowWithNSCFPart.apply_converged_quantities(self)
        await QEBandStructureSubWorkflow.apply_converged_quantities(self)
        if not self.use_band_structure:
            await self.workflow._use_converged_quantities(
                    self.band_structure_input_variables,
                    use_converged_phonon_ecut=self.use_converged_phonon_ecut,
                    use_converged_phonon_smearing=(
                        self.use_converged_phonon_smearing),
                    use_relaxed_geometry=self.use_relaxed_geometry,
                    )

    async def apply_converged_quantities_band_structure(
            self, *args, **kwargs) -> None:
        """Apply converged quantities to band structure part."""
        cls = QEBandStructureSubWorkflow
        await cls.apply_converged_quantities_band_structure(
                self, *args,
                use_converged_phonon_ecut=self.use_converged_phonon_ecut,
                use_converged_phonon_smearing=(
                    self.use_converged_phonon_smearing),
                **kwargs,
                )

    async def apply_converged_quantities_nscf(self, *args, **kwargs):
        """Apply converged quantities to nscf part of subworkflow."""
        # allow for phonon quantities
        await BaseSubWorkflowWithNSCFPart.apply_converged_quantities_nscf(
                self, *args,
                use_converged_phonon_ecut=self.use_converged_phonon_ecut,
                use_converged_phonon_smearing=(
                    self.use_converged_phonon_smearing),
                **kwargs,
                )

    async def check(
            self, force_check_phonons: bool = False,
            force_check_q2r: bool = False,
            force_check_matdyn: bool = False,
            ):
        """Check that everything is consistent and all variables are set."""
        QEPhononDispersionSubWorkflow.check(
                self,
                check_q2r=not self.use_phonon_dispersion or force_check_q2r,
                check_matdyn=(
                    not self.use_phonon_dispersion or force_check_matdyn),
                check_phonons=(
                    not self.use_phonon_dispersion or force_check_phonons),
                )
        await self.check_nscf()
        QEBandStructureSubWorkflow.check_band_structure(self)
        await self.check_epw()

    async def check_epw(self, set_check: bool = True) -> None:
        """Check epw part of subworkflow."""
        if self.epw_input_variables is None:
            raise ValueError("Need to set 'epw_input_variables'.")
        if self.use_band_structure:
            if not await (
                    self.workflow.workflow_part_completed("band_structure")):
                self._logger.info("Band structure part not completed yet.")
                return
        if self.use_phonon_dispersion:
            if not self.workflow._compute_phonon_dispersion:
                raise ValueError(
                        "'use_phonon_dispersion' is set to True but phonon "
                        "dispersion is not computed!. Set 'phonon_disperison' "
                        "to True upon init.")
            if not await self.phonon_dispersion_sequencer.sequence_completed:
                self.stop_at_workflow = "phonon_dispersion"
                return
        if set_check:
            self.checks["epw"] = True

    async def check_nscf(self, check_nscf_kgrid: bool = True) -> None:
        """Check nscf part of subworkflow."""
        if self.nscf_kgrid is None and check_nscf_kgrid:
            raise ValueError("Need to set 'nscf_kgrid'.")
        return BaseSubWorkflowWithNSCFPart.check_nscf(self)

    async def set_sequencer(self):
        """Set the sequencer."""
        await QEPhononDispersionSubWorkflow.set_sequencer(self)
        await BaseSubWorkflowWithNSCFPart.set_sequencer(self)
        await QEBandStructureSubWorkflow.set_sequencer(self)
        await self.set_epw_wannierization_part()
        await self.set_epw_dispersion_interpolation_part()

    @subparts_checkpoint("nscf", propagate=True)
    async def set_nscf_part(
            self, nscf_kgrid: Sequence[int] = None,
            sequencer: BaseSequencer = None,
            **kwargs,
            ):
        """Set the nscf part of the sequencer."""
        if nscf_kgrid is None:
            nscf_kgrid = self.nscf_kgrid
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.nscf_kgrid = nscf_kgrid
        await BaseSubWorkflowWithNSCFPart.set_nscf_part(
                self, sequencer=sequencer, **kwargs)

    @subparts_checkpoint("phonons", propagate=True)
    async def set_phonons_part(
            self, *args, sequencer: BaseSequencer = None,
            set_parts: bool = True, use_phonon_dispersion: bool = None,
            **kwargs) -> None:
        """Set the phonons part of the sequencer."""
        if use_phonon_dispersion is None:
            use_phonon_dispersion = self.use_phonon_dispersion
        if not use_phonon_dispersion:
            await QEPhononDispersionSubWorkflow.set_phonons_part(
                    self, *args, sequencer=sequencer,
                    set_parts=set_parts,
                    **kwargs)
            return
        # phonons part
        phseq = self.phonon_dispersion_sequencer
        if not await phseq.sequence_completed:
            self.stop_at_workflow = "phonon_dispersion"
            return
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.phonons_workdir = phseq.phonons_workdir
        sequencer.phonons_qpoint_grid = phseq.phonons_qpoint_grid
        sequencer.phonons_input_variables = (
                phseq.phonons_input_variables.copy())

    @subparts_checkpoint("q2r", propagate=True)
    async def set_q2r_part(
            self, sequencer: BaseSequencer = None,
            use_phonon_dispersion: bool = None,
            **kwargs,
            ):
        """Setup Q2R part of subworkflow."""
        if use_phonon_dispersion is None:
            use_phonon_dispersion = self.use_phonon_dispersion
        if not use_phonon_dispersion:
            await QEPhononDispersionSubWorkflow.set_q2r_part(
                    self, sequencer=sequencer,
                    **kwargs)
            return
        # q2r part
        phseq = self.phonon_dispersion_sequencer
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.q2r_workdir = phseq.q2r_workdir
        sequencer.q2r_input_variables = (
                phseq.q2r_input_variables.copy())

    @subparts_checkpoint("matdyn", propagate=True)
    async def set_matdyn_part(
            self, sequencer: BaseSequencer = None,
            use_phonon_dispersion: bool = None,
            **kwargs) -> None:
        """Setup matdyn part of subworkflow."""
        if use_phonon_dispersion is None:
            use_phonon_dispersion = self.use_phonon_dispersion
        if sequencer is None:
            sequencer = self.sequencer
        if not use_phonon_dispersion:
            await QEPhononDispersionSubWorkflow.set_matdyn_part(
                    self, sequencer=sequencer,
                    **kwargs)
            return
        # matdyn part
        phseq = self.phonon_dispersion_sequencer
        sequencer.matdyn_workdir = phseq.matdyn_workdir
        sequencer.qpoint_path = phseq.qpoint_path
        sequencer.qpoint_path_density = (
                phseq.qpoint_path_density)
        sequencer.asr = phseq.asr
        sequencer.matdyn_input_variables = (
                phseq.matdyn_input_variables.copy())

    @subparts_checkpoint("epw_wannierization")
    async def set_epw_wannierization_part(
            self, sequencer: BaseSequencer = None,
            epw_wannierization_workdir: str = None,
            use_epw_wannierization_from: str = None,
            ) -> None:
        """Set the epw wannierization part of the sequencer."""
        if epw_wannierization_workdir is None:
            epw_wannierization_workdir = os.path.join(
                self.root_workdir, "epw_wannierization_run")
        if sequencer is None:
            sequencer = self.sequencer
        await self.use_any_converged_calculation(
                sequencer, "epw_",
                use_from=use_epw_wannierization_from,
                default_workdir=epw_wannierization_workdir,
                default_input_variables=self.epw_input_variables.copy(),
                default_calculation_parameters=(
                    self.epw_calculation_parameters.copy()),
                )

    @subparts_checkpoint("epw_dispersion_interpolation")
    async def set_epw_dispersion_interpolation_part(
            self, sequencer: BaseSequencer = None,
            epw_dispersion_interpolation_workdir: str = None,
            use_epw_dispersion_interpolation_from: str = None,
            ) -> None:
        """Set the epw dispersion interpolation part of the sequencer."""
        if sequencer is None:
            sequencer = self.sequencer
        if epw_dispersion_interpolation_workdir is None:
            epw_dispersion_interpolation_workdir = os.path.join(
                self.root_workdir, "epw_dispersion_interpolation_run")
        await self.use_any_converged_calculation(
                sequencer, "epwdispinterpolation_",
                use_from=use_epw_dispersion_interpolation_from,
                add_input_variables=False,
                default_workdir=epw_dispersion_interpolation_workdir,
                default_calculation_parameters=(
                    self.epw_calculation_parameters.copy()),
                )
