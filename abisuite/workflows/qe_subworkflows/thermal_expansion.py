import os
from typing import Sequence

from .band_structure import QEBandStructureSubWorkflow
from .dos import QEDOSSubWorkflow
from .phonon_dispersion import QEPhononDispersionSubWorkflow


class QEThermalExpansionSubWorkflow(
        QEPhononDispersionSubWorkflow, QEBandStructureSubWorkflow,
        QEDOSSubWorkflow,
        ):
    """SubWorkflow class for themal expansion part of workflow."""

    _loggername = "QEThermalExpansionSubWorkflow"
    _have_plots = True
    _plot_name_extension = "lattice_expansion"

    def __init__(
            self, *args,
            deltas_volumes: Sequence[float] = None,
            temperatures: Sequence[float] = None,
            bulk_modulus_initial_guess: float = None,
            compute_electronic_free_energy: bool = False,
            only_compute_electronic_contributions: bool = False,
            phonons_fine_qpoint_grid: Sequence[int] = None,
            maximum_relaxations: int = None,
            compute_band_structures: bool = False,
            compute_dosses: bool = False,
            **kwargs,
            ):
        self.compute_band_structures = compute_band_structures
        self.compute_dosses = compute_dosses
        doskwargs = {
                "nscf_input_variables": kwargs.pop(
                    "nscf_input_variables", None),
                "nscf_calculation_parameters": kwargs.pop(
                    "nscf_calculation_parameters", None),
                "dos_input_variables": kwargs.pop(
                    "dos_input_variables", None),
                "dos_calculation_parameters": kwargs.pop(
                    "dos_calculation_parameters", None),
                "dos_fine_kpoint_grid_variables": kwargs.pop(
                    "dos_fine_kpoint_grid_variables", None),
                }
        sequencer = kwargs.pop("sequencer", None)
        workflow = kwargs.pop("workflow", None)
        bsiv = kwargs.pop("band_structure_input_variables", None)
        bscp = kwargs.pop("band_structure_calculation_parameters", None)
        bskp = kwargs.pop("band_structure_kpoint_path", None)
        bskpd = kwargs.pop("band_structure_kpoint_path_density", None)
        if self.compute_band_structures:
            QEBandStructureSubWorkflow.__init__(
                    self, *args,
                    sequencer=sequencer, workflow=workflow,
                    band_structure_input_variables=bsiv,
                    band_structure_calculation_parameters=bscp,
                    band_structure_kpoint_path=bskp,
                    band_structure_kpoint_path_density=bskpd,
                    )
        else:
            self.use_band_structure = False
        if self.compute_dosses:
            QEDOSSubWorkflow.__init__(self, *args, **doskwargs)
        if sequencer is not None:
            kwargs["sequencer"] = sequencer
        if workflow is not None:
            kwargs["workflow"] = workflow
        QEPhononDispersionSubWorkflow.__init__(self, *args, **kwargs)
        self.phonons_fine_qpoint_grid = phonons_fine_qpoint_grid
        self.deltas_volumes = deltas_volumes
        self.temperatures = temperatures
        self.bulk_modulus_initial_guess = bulk_modulus_initial_guess
        self.compute_electronic_free_energy = compute_electronic_free_energy
        self.only_compute_electronic_contributions = (
                only_compute_electronic_contributions)
        self.maximum_relaxations = maximum_relaxations
        self.checks["thermal_expansion"] = False

    async def apply_converged_quantities(self) -> None:
        """Apply converged quantities."""
        if self.compute_band_structures:
            await QEBandStructureSubWorkflow.apply_converged_quantities(self)
        await QEPhononDispersionSubWorkflow.apply_converged_quantities(self)

    def check(self) -> None:
        """Checks everything is set properly."""
        QEPhononDispersionSubWorkflow.check(
                self, skip_to_scf=self.only_compute_electronic_contributions)
        self.check_thermal_expansion()
        if self.compute_band_structures:
            self.check_band_structure()
        if self.compute_dosses:
            QEDOSSubWorkflow.check(self)

    def check_thermal_expansion(self) -> None:
        """Check the thermal expansion part of the subworkflow."""
        if self.deltas_volumes is None:
            raise ValueError("Need to set 'deltas_volumes'.")
        if self.temperatures is None:
            raise ValueError("Need to set 'temperatures'.")
        if self.only_compute_electronic_contributions:
            self.checks["thermal_expansion"] = True
            return
        if self.bulk_modulus_initial_guess is None:
            raise ValueError("Need to set 'bulk_modulus_initial_guess'.")
        if self.phonons_fine_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_fine_qpoint_grid'.")
        if self.maximum_relaxations is None:
            ibrav = self.scf_input_variables.get("ibrav", None)
            if ibrav is None:
                # check relaxed geometry
                ibrav = self.workflow.relaxed_geometry["ibrav"]
            if ibrav != 2:
                raise ValueError("Need to set 'maximum_relaxations'.")
        self.checks["thermal_expansion"] = True

    async def set_scf_part(self, scf_workdir: str = None) -> None:
        """Set the scf part of the sequencer."""
        await QEPhononDispersionSubWorkflow.set_scf_part(
                self,
                scf_workdir=os.path.join(
                    self.root_workdir, "scf_runs", "scf"))
        sequencer = self.sequencer
        if self.maximum_relaxations is not None:
            sequencer.maximum_relaxations = self.maximum_relaxations
        sequencer.deltas_volumes = self.deltas_volumes
        sequencer.temperatures = self.temperatures
        sequencer.bulk_modulus_initial_guess = self.bulk_modulus_initial_guess
        ibrav = self.scf_input_variables["ibrav"]
        if ibrav not in (2, 4):
            raise NotImplementedError(ibrav)
        sequencer.relaxed_lattice_parameters = {
                "celldm(1)": self.scf_input_variables["celldm(1)"],
                }
        if ibrav == 4:
            sequencer.relaxed_lattice_parameters.update({
                "celldm(3)": self.scf_input_variables["celldm(3)"]})
        # DOS part if needed
        if self.compute_electronic_free_energy:
            sequencer.compute_electronic_contributions = True
        sequencer.only_compute_electronic_contributions = (
                self.only_compute_electronic_contributions)

    async def set_band_structure_part(self, *args, **kwargs) -> None:
        """Sets the band structure part of the sequencer."""
        await QEBandStructureSubWorkflow.set_band_structure_part(
                self, *args,
                band_structure_workdir=os.path.join(
                    self.root_workdir, "band_structure_runs",
                    "band_structure"),
                **kwargs)
        self.sequencer.compute_band_structures = self.compute_band_structures

    async def set_nscf_part(self, *args, **kwargs) -> None:
        """Sets the nscf part of the subworkflow."""
        await QEDOSSubWorkflow.set_nscf_part(
                self, *args,
                nscf_workdir=os.path.join(
                    self.root_workdir, "nscf_runs", "nscf"),
                **kwargs)

    async def set_dos_part(self, *args, **kwargs) -> None:
        """Sets the dos part of the subworkflow."""
        await QEDOSSubWorkflow.set_dos_part(
                self, *args,
                dos_workdir=os.path.join(
                    self.root_workdir, "dos_runs", "dos"),
                **kwargs)
        self.sequencer.compute_dosses = self.compute_dosses

    async def set_sequencer(self):
        """Set the sequencer."""
        await QEPhononDispersionSubWorkflow.set_sequencer(self)
        if self.compute_band_structures:
            await self.set_band_structure_part()
        if self.compute_dosses:
            await self.set_nscf_part()
            await self.set_dos_part()
        if self.only_compute_electronic_contributions:
            # no need for phonons
            return
        await self.set_kpoints_part()
        await self.set_matdyngrid_part()

    async def set_phonons_part(self, **kwargs):
        """Set the phonon part of the sequencer."""
        if self.only_compute_electronic_contributions:
            # no need for phonons
            return
        await QEPhononDispersionSubWorkflow.set_phonons_part(
                self,
                phonons_workdir=os.path.join(
                    self.root_workdir, "phonons_runs", "phonons"))

    async def set_q2r_part(self):
        """Set the q2r part of the sequencer."""
        if self.only_compute_electronic_contributions:
            # no need for phonons
            return
        await QEPhononDispersionSubWorkflow.set_q2r_part(
                self,
                q2r_workdir=os.path.join(self.root_workdir, "q2r_runs", "q2r")
                )

    async def set_matdyn_part(self):
        """Set the matdyn part of the sequencer."""
        if self.only_compute_electronic_contributions:
            # no need for phonons
            return
        await QEPhononDispersionSubWorkflow.set_matdyn_part(
                self,
                matdyn_workdir=os.path.join(
                    self.root_workdir, "matdyn_runs", "matdyn")
                )

    async def set_kpoints_part(self):
        """Set the kpoints part of the workflow."""
        # KPOINTS PART
        self.sequencer.kpoints_workdir = os.path.join(
                self.root_workdir, "fine_qpoint_grid")

    async def set_matdyngrid_part(self):
        """Set the matdyngrid part of the workflow."""
        # MATDYN GRID PART
        if self.only_compute_electronic_contributions:
            # no need for phonons
            return
        self.sequencer.phonons_fine_qpoint_grid = self.phonons_fine_qpoint_grid
        self.sequencer.matdyngrid_workdir = os.path.join(
                self.root_workdir, "matdyngrid_runs", "matdyngrid")
        self._add_calculation_parameters(
                "matdyngrid_", self.matdyn_calculation_parameters,
                self.sequencer)
