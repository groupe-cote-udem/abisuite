import os
from typing import Any, Mapping

from ..bases_subworkflows import BaseSubWorkflow, subparts_checkpoint
from ...sequencers.bases import BaseSequencer


class QEIBTESubWorkflow(BaseSubWorkflow):
    """SubWorkflow class for IBTE calculations."""

    _have_plots = True
    _loggername = "QEIBTESubWorkflow"
    _plot_name_extension = "ibte"

    def __init__(
            self, *args, ibte_input_variables: dict[str, Any] = None,
            ibte_calculation_parameters: dict[str, Any] = None,
            use_ibte_converged_fsthick: bool = False,
            use_epw_wannierization_from: str = None,
            **kwargs):
        BaseSubWorkflow.__init__(self, *args, **kwargs)
        self.ibte_input_variables = ibte_input_variables
        self.ibte_calculation_parameters = ibte_calculation_parameters
        self.use_ibte_converged_fsthick = use_ibte_converged_fsthick
        self.use_epw_wannierization_from = use_epw_wannierization_from
        self.checks["ibte"] = False

    @property
    def converged_ibte_fsthick(self):
        """The converged value of ibte fsthick."""
        return self.workflow.converged_ibte_fsthick

    @property
    def epw_wannierization_sequencer(self):
        """The epw wannierization sequencer."""
        return self.workflow.epw_wannierization_sequencer

    async def apply_converged_quantities(self):
        """Applies converged quantities."""
        await self.apply_converged_quantities_ibte_part()

    async def apply_converged_quantities_ibte_part(self):
        """Apply converged quantities for ibte part."""
        if self.use_ibte_converged_fsthick:
            self.ibte_input_variables.update(
                    {"fsthick": self.workflow.converged_ibte_fsthick})

    async def check(self):
        """Check that everything is set correctly."""
        BaseSubWorkflow.check(self)
        await self.check_ibte()

    async def check_ibte(
            self, check_epw_wannierization_sequencer: bool = True,
            set_check: bool = True,
            ) -> None:
        """Check ibte part of subworkflow."""
        if self.checks["ibte"]:
            return
        if check_epw_wannierization_sequencer and (
                self.use_epw_wannierization_from is None):
            seq = self.workflow.epw_wannierization_sequencer
            if not await seq.sequence_completed:
                self.stop_at_workflow = "epw_wannierization"
                return
        if self.ibte_input_variables is None:
            raise ValueError("Need to set 'ibte_input_variables'.")
        if self.ibte_calculation_parameters is None:
            raise ValueError("Need to set 'ibte_calculation_parameters'.")
        if self.use_ibte_converged_fsthick:
            if self.workflow.converged_ibte_fsthick is None:
                raise ValueError(
                        "Need to set 'converged_ibte_fsthick' upon init.")
        if set_check:
            self.checks["ibte"] = True

    async def set_sequencer(self):
        """Set the sequencer."""
        await BaseSubWorkflow.set_sequencer(self)
        await self.set_ibte_part()

    @subparts_checkpoint("ibte")
    async def set_ibte_part(
            self, ibte_workdir: str = None,
            epw_wannierization_workdir: str = None,
            sequencer: BaseSequencer = None,
            set_epw_wannierization_workdir: bool = True,
            ibte_input_variables: Mapping[str, Any] = None,
            use_ibte_from: str = None,
            ) -> None:
        """Sets ibte part of subworkflow."""
        if ibte_workdir is None:
            ibte_workdir = os.path.join(
                self.root_workdir, "ibte_run")
        if sequencer is None:
            sequencer = self.sequencer
        if set_epw_wannierization_workdir:
            if epw_wannierization_workdir is None:
                if self.use_epw_wannierization_from is not None:
                    epw_wannierization_workdir = (
                            self.use_epw_wannierization_from)
                else:
                    epw_wannierization_workdir = (
                        self.epw_wannierization_sequencer.epw_workdir)
            await sequencer.set_epw_wannierization_calculation(
                    epw_wannierization_workdir)
        if ibte_input_variables is None:
            ibte_input_variables = self.ibte_input_variables.copy()
        await self.use_any_converged_calculation(
                sequencer, "ibte_",
                default_workdir=ibte_workdir,
                default_input_variables=ibte_input_variables,
                default_calculation_parameters=(
                    self.ibte_calculation_parameters.copy()),
                use_from=use_ibte_from,
               )
