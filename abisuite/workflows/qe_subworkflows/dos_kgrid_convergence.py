import os
from typing import Any, Mapping, Sequence

from .dos import QEDOSSubWorkflow
from ..bases_subworkflows import BaseWorkflowSubUnit
from ...routines import is_dict_like, is_list_like
from ...sequencers import QEDOSSequencer


class QEDOSKgridConvergenceSubWorkflow(QEDOSSubWorkflow):
    """Subworkflow class for a dos kgrid convergence run with QE."""

    _loggername = "QEDOSKgridConvergenceSubWorkflow"

    def __init__(
            self, *args,
            dos_fine_kpoint_grids_variables: Sequence[
                Mapping[str, Any]] = None,
            **kwargs) -> None:
        QEDOSSubWorkflow.__init__(self, *args, **kwargs)
        self.dos_fine_kpoint_grids_variables = dos_fine_kpoint_grids_variables

    def check_dos(self) -> None:
        """Check dos part of subworkflow."""
        QEDOSSubWorkflow.check_dos(
                self, set_check=False,
                check_dos_fine_kpoint_grid_variables=False,
                )
        if self.dos_fine_kpoint_grids_variables is None:
            raise ValueError("Need to set dos_fine_kpoint_grids_variables.")
        if not is_list_like(self.dos_fine_kpoint_grids_variables):
            raise TypeError(
                    "'dos_fine_kpoint_grids_variables' should be list-like but"
                    f" got: '{self.dos_fine_kpoint_grids_variables}'.")
        for kgrid in self.dos_fine_kpoint_grids_variables:
            if not is_dict_like(kgrid):
                raise TypeError(
                        "dos_kpoint_grids_variables should be a list of dicts."
                        )
        self.checks["dos"] = True

    async def set_sequencer(self, *args, **kwargs) -> None:
        """Set sequencer."""
        self._logger.info("Setting dos kgrid convergence sequencer.")
        self.sequencer.root_workdir = self.root_workdir
        for ikgrid, kgrid in enumerate(self.dos_fine_kpoint_grids_variables):
            subdir = f"kgrid_{ikgrid}"
            sequencer = QEDOSSequencer(loglevel=self._loglevel)
            await self.set_scf_part(
                    sequencer=sequencer,
                    scf_workdir=os.path.join(
                        self.root_workdir, subdir, "scf_run"),
                    set_parts=False,
                    )
            await self.set_nscf_part(
                    sequencer=sequencer,
                    nscf_workdir=os.path.join(
                        self.root_workdir, subdir, "nscf_run",
                        ),
                    set_parts=False,
                    )
            await self.set_dos_part(
                    sequencer=sequencer,
                    dos_workdir=os.path.join(
                        self.root_workdir, subdir, "dos_run"),
                    dos_fine_kpoint_grid_variables=kgrid,
                    set_parts=False,
                    )
            plot_calculation_parameters = (
                    self.plot_calculation_parameters.copy())
            plot_calculation_parameters["save"] = os.path.join(
                    self.root_workdir, subdir, "results", "dos.pdf")
            plot_calculation_parameters["save_pickle"] = os.path.join(
                    self.root_workdir, subdir, "results", "dos.pickle")
            self.set_plots(
                    sequencer=sequencer, set_parts=False,
                    plot_calculation_parameters=plot_calculation_parameters)
            self.sequencer.append(sequencer)
        self.parts_set += ["scf", "nscf", "dos", "plots"]


class QEDOSKgridConvergencePostProcessSubWorkflow(BaseWorkflowSubUnit):
    """Post process subworkflow class for a dos kgrid convergence with QE."""

    _loggername = "QEDOSKgridConvergencePostProcessSubWorkflow"

    async def make_dos_kgrid_convergence_plots(self):
        """Make the final dos kgrid convergence plots."""
        main_root = self.sequencer.root_workdir
        results_dir = os.path.join(main_root, "results")
        dos_plots = []
        for iseq, seq in enumerate(
                self.sequencer,
                ):
            root = os.path.dirname(seq.dos_workdir)
            dos_plot = seq.dos_plot
            curve = dos_plot.plot_objects["curves"][0]
            curve.color = f"C{iseq}"
            curve.label = os.path.basename(root)
            dos_plots.append(dos_plot)
        sumplot = sum(dos_plots[1:], dos_plots[0])
        sumplot.title = "DOS kgrid convergence"
        show = seq.plot_calculation_parameters.get(
                "show", True)
        save = seq.plot_calculation_parameters.get(
                "save", None)
        sumplot.plot(show=show)
        if save is not None:
            save_pdf = os.path.join(results_dir, "dos_comparison.pdf")
            save_pickle = save_pdf.replace(".pdf", ".pickle")
            await sumplot.save(save_pdf, overwrite=True)
            await sumplot.save_pickle(save_pickle, overwrite=True)
