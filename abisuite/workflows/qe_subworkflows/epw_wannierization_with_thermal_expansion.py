import os
from typing import Any, Sequence

import aiofiles.os

from .epw_wannierization import QEEPWWannierizationSubWorkflow
from ..bases_subworkflows import subparts_checkpoint
from ...exceptions import DevError
from ...plotters import Plot
from ...routines import is_list_like, is_vector
from ...sequencers import QEEPWWithPhononInterpolationSequencer


class QEEPWWannierizationWithThermalExpansionSubWorkflow(
        QEEPWWannierizationSubWorkflow,
        ):
    """SubWorkflow class for EPW with thermal expansion part of workflow."""

    _loggername = "QEEPWWannierizationWithThermalExpansionSubWorkflow"

    def __init__(
            self, *args,
            lattice_parameters: Sequence[float] = None,
            temperatures: Sequence[float] = None,
            compute_electronic_nesting: bool = False,
            electronic_nesting_fine_kgrid: Sequence[int] = None,
            **kwargs):
        QEEPWWannierizationSubWorkflow.__init__(self, *args, **kwargs)
        self.lattice_parameters = lattice_parameters
        self.temperatures = temperatures
        self.compute_electronic_nesting = compute_electronic_nesting
        self.electronic_nesting_fine_kgrid = electronic_nesting_fine_kgrid

    async def check_epw(self):
        """Check epw part of subworkflow."""
        if self.checks["epw"]:
            return
        await QEEPWWannierizationSubWorkflow.check_epw(self, set_check=False)
        if self.lattice_parameters is not None:
            if self.sequencer:
                raise DevError("Sequencer was already set...")
            if not is_list_like(self.lattice_parameters):
                raise TypeError("'lattice_parameters' must be a list.")
            if self.temperatures is None:
                raise ValueError("Need to set 'temperatures'.")
            if not is_vector(self.temperatures, dtype="scalar", positive=True):
                raise TypeError(
                        "'temperatures' must be a list of positive scalars.")
            if len(self.lattice_parameters) != len(self.temperatures):
                raise ValueError("Length mismatch between 'temperatures' "
                                 "and 'lattice_parameters'.")
            for _ in self.temperatures:
                self.sequencer.append(
                    QEEPWWithPhononInterpolationSequencer(
                        loglevel=self._loglevel)
                    )
        else:
            if self.temperatures is not None:
                raise ValueError(
                        "lattice_parameters not set. That means the lattice "
                        "parameters will be taken from the 'lattice_expansion'"
                        " part of the workflows with the corresponding "
                        "temperatures. Therefore -> do not set temperatures.")
        if self.compute_electronic_nesting:
            # use kpath as 'qpath' in EPW.
            # check fine kpt grid is set
            if self.electronic_nesting_fine_kgrid is None:
                raise ValueError(
                        "Need to set 'electronic_nesting_fine_kgrid'.")
        self.checks["epw"] = True

    async def load_lattice_parameters(self):
        """Loads T-dependent lattice parameters if needed."""
        if self.lattice_parameters is not None:
            return
        self._logger.info("Loading lattice parameters for epw wannierization")
        if not self.workflow._compute_lattice_expansion:
            raise ValueError(
                    "No lattice_parameters given and computation of "
                    "lattice parameters expansion not requested.")
        # if thermal expansion is completed, load data from the
        # lattice_expansion plot.
        seq = self.workflow.lattice_expansion_sequencer
        if not await seq.sequence_completed:
            self._logger.info("Waiting for lattice exp to be done")
            self.stop_at_workflow = "lattice_expansion"
            self.checks["epw"] = False
            return
        # determine ibrav first
        if seq.ibrav == 2:
            # cubic -> just check lattice_expansion file exists
            await self._load_lattice_parameters_cubic()
        elif seq.ibrav == 4:
            await self._load_lattice_parameters_hexagonal()
        else:
            raise NotImplementedError(seq.ibrav)
        self.temperatures = [float(t) for t in self.temperatures]

    async def _load_lattice_parameters_hexagonal(self) -> None:
        resdir = os.path.dirname(
                self.workflow.lattice_expansion_sequencer.plot_save_pickle)
        lattice_plot_path = os.path.join(
                resdir,
                "lattice_expansion_lattice_expansion_lattice_parameters.pickle"
                )
        if not await aiofiles.os.path.exists(lattice_plot_path):
            # run results
            await self.workflow.run_lattice_expansion()
        # if something is wrong the plot won't show up
        if not await aiofiles.os.path.exists(lattice_plot_path):
            self._logger.error(
                    "No data found for lattice thermal expansion."
                    " Nothing done"
                    " for epw with thermal expansion. "
                    "Waiting for relaunch...")
            self.stop_at_workflow = "lattice_expansion"
            self.checks["epw"] = False
            return
        lattice_plot = Plot.load_plot(lattice_plot_path)
        curves = lattice_plot.plot_objects["curves"]
        temps = None
        a_s, c_s = None, None
        for curve in curves:
            if temps is None:
                temps = curve.xdata
            assert (temps == curve.xdata).all()
            if "a" in curve.label:
                a_s = curve.ydata
            else:
                c_s = curve.ydata
        if a_s is None:
            raise LookupError("No a...")
        if c_s is None:
            raise LookupError("No c...")
        self.lattice_parameters = [
                {"celldm(1)": a, "celldm(3)": c / a} for a, c in zip(a_s, c_s)]

    async def _load_lattice_parameters_cubic(self) -> None:
        seq = self.workflow.lattice_expansion_sequencer
        resdir = os.path.dirname(
                self.workflow.lattice_expansion_sequencer.plot_save_pickle)
        lattice_plot_path = os.path.join(
                resdir,
                "lattice_expansion_lattice_expansion.pickle"
                )
        if not await aiofiles.os.path.exists(lattice_plot_path):
            # run results
            await self.workflow.run_lattice_expansion()
        # if something is wrong the plot won't show up
        if not await aiofiles.os.path.exists(lattice_plot_path):
            self._logger.error(
                    "No data found for lattice thermal expansion."
                    " Nothing done"
                    " for epw with thermal expansion. "
                    "Waiting for relaunch...")
            self.stop_at_workflow = "lattice_expansion"
            self.checks["epw"] = False
            return
        lattice_plot = Plot.load_plot(lattice_plot_path)
        curves = lattice_plot.plot_objects["curves"]
        if len(curves) == 1:
            self.temperatures = curves[0].xdata
            self.lattice_parameters = [
                    {"celldm(1)": a} for a in curves[0].ydata]
        else:
            if seq.compute_electronic_contributions:
                # take curve from Fe + Fph
                if len(curves) != 2:
                    raise NotImplementedError
                for curve in lattice_plot.plot_objects["curves"]:
                    if "ph only" in curve.label:
                        continue
                    self.temperatures = curve.xdata
                    self.lattice_parameters = [
                            {"celldm(1)": y} for y in curve.ydata]
                    return
                else:
                    raise RuntimeError

    async def set_sequencer(self):
        """Sets the sequencer for this part of the workflow."""
        self._logger.info(
                "Setting epw wannierization with thermal expansion sequencers."
                )
        for sequencer, lattice_parameters, temperature in zip(
                self.sequencer,
                self.lattice_parameters,
                self.temperatures):
            self._logger.info(
                    f"Setting epw seq with lat. params={lattice_parameters} | "
                    f"T={temperature}K")
            await self.set_scf_part(
                    sequencer, lattice_parameters,
                    temperature, set_parts=False)
            await self.set_nscf_part(sequencer, temperature, set_parts=False)
            await self.set_band_structure_part(
                    sequencer, temperature, set_parts=False)
            await self.set_phonons_part(
                    sequencer, temperature, set_parts=False)
            await self.set_q2r_part(sequencer, temperature, set_parts=False)
            await self.set_matdyn_part(sequencer, temperature, set_parts=False)
            await self.set_epw_wannierization_part(
                    sequencer, temperature, set_parts=False)
            await self.set_epw_dispersion_interpolation_part(
                    sequencer, temperature, set_parts=False)
            self._set_subsequencer_epw_electronic_nesting_part(
                    sequencer, temperature)
            self.set_plots(sequencer, temperature, set_parts=False)
        self.parts_set += [
                "scf", "nscf", "band_structure", "phonons", "q2r", "matdyn",
                "epw_wannierization", "epw_dispersion_interpolation", "plots"]

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            lattice_parameters: dict[str, Any],
            temperature: float,
            **kwargs,
            ) -> None:
        """Set scf part of subworkflow."""
        invars = self.scf_input_variables.copy()
        invars.update(lattice_parameters)
        await QEEPWWannierizationSubWorkflow.set_scf_part(
                self,
                sequencer=sequencer,
                scf_workdir=os.path.join(
                    self.root_workdir, "scf_runs", f"{temperature}K"),
                scf_input_variables=invars,
                **kwargs,
                )

    @subparts_checkpoint("nscf", propagate=True)
    async def set_nscf_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float,
            **kwargs) -> None:
        """Set nscf part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_nscf_part(
                self,
                sequencer=sequencer,
                nscf_workdir=os.path.join(
                    self.root_workdir, "nscf_runs", f"{temperature}K"),
                **kwargs,
                )

    @subparts_checkpoint("band_structure", propagate=True)
    async def set_band_structure_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set band structure part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_band_structure_part(
                self, sequencer=sequencer,
                band_structure_workdir=os.path.join(
                    self.root_workdir, "band_structure_runs", f"{temperature}K"
                    ),
                **kwargs,
                )

    @subparts_checkpoint("phonons", propagate=True)
    async def set_phonons_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set phonons part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_phonons_part(
                self, sequencer=sequencer,
                phonons_workdir=os.path.join(
                    self.root_workdir, "phonons_runs", f"{temperature}K",
                    "ph"),
                **kwargs,
                )

    @subparts_checkpoint("q2r", propagate=True)
    async def set_q2r_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set q2r part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_q2r_part(
                self, sequencer=sequencer,
                q2r_workdir=os.path.join(
                    self.root_workdir, "q2r_runs", f"{temperature}K"),
                **kwargs,
                )

    @subparts_checkpoint("matdyn", propagate=True)
    async def set_matdyn_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set matdyn part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_matdyn_part(
                self, sequencer=sequencer,
                matdyn_workdir=os.path.join(
                    self.root_workdir, "matdyn_runs", f"{temperature}K"),
                **kwargs,
                )

    @subparts_checkpoint("epw_wannierization", propagate=True)
    async def set_epw_wannierization_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs,
            ) -> None:
        """Set epw wannerization part of subworkflow."""
        await QEEPWWannierizationSubWorkflow.set_epw_wannierization_part(
                self, sequencer=sequencer,
                epw_wannierization_workdir=os.path.join(
                    self.root_workdir, "epw_wannierization_runs",
                    f"{temperature}K"),
                **kwargs,
                )

    @subparts_checkpoint("epw_dispersion_interpolation", propagate=True)
    async def set_epw_dispersion_interpolation_part(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set epw dispersion interpolation part of subworkflow."""
        sw = QEEPWWannierizationSubWorkflow
        await sw.set_epw_dispersion_interpolation_part(
                self, sequencer=sequencer,
                epw_dispersion_interpolation_workdir=os.path.join(
                    self.root_workdir, "epw_dispersion_interpolation_runs",
                    f"{temperature}K"),
                **kwargs)

    def _set_subsequencer_epw_electronic_nesting_part(
            self, sequencer, temperature):
        if not self.compute_electronic_nesting:
            return
        sequencer.epwelectronicnesting_workdir = os.path.join(
                self.root_workdir, "epw_electronic_nesting_runs",
                f"{temperature}K")
        sequencer.epwelectronicnesting_fine_kgrid = (
                self.electronic_nesting_fine_kgrid)
        self._add_calculation_parameters(
                "epwelectronicnesting_", self.epw_calculation_parameters,
                sequencer)

    @subparts_checkpoint("plots", propagate=True)
    def set_plots(
            self, sequencer: QEEPWWithPhononInterpolationSequencer,
            temperature: float, **kwargs) -> None:
        """Set plots part of subworkflow."""
        # ######################## plot part ##############################
        plot_params = self.plot_calculation_parameters.copy()
        # add title to plots
        plot_params.update(
                {"title": f"{temperature}K",
                 "save": os.path.join(
                     self.root_workdir, "results",
                     f"{temperature}K", "interpolation.pdf"),
                 "save_pickle": os.path.join(
                     self.root_workdir, "results", f"{temperature}K",
                     "interpolation.pickle"),
                 })
        QEEPWWannierizationSubWorkflow.set_plots(
                self,
                sequencer=sequencer,
                plot_calculation_parameters=plot_params,
                **kwargs,
                )
