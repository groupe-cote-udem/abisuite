import abc
import os
from typing import Mapping

import aiofiles.os

import numpy as np

from ..bases_subworkflows import BaseWorkflowSubUnit
from ...plotters import Plot
from ...sequencers import QEEPWIBTESequencer


class _BaseQEIBTEConvergencePostProcessSubWorkflow(
        BaseWorkflowSubUnit, abc.ABC):
    """Base class for ibte convergence study post processor."""

    def __init__(self, *args, **kwargs) -> None:
        BaseWorkflowSubUnit.__init__(self, *args, **kwargs)
        self._label_dict = {}

    async def make_decay_plots(self):
        """Make the decay convergence plots."""
        self._logger.info("Plotting decay convergence plots.")
        conv_plots = {}
        for iseq, sequencer in enumerate(
                self.sequencer):
            if not isinstance(sequencer, QEEPWIBTESequencer):
                # only work with ibte sequencers for generality
                continue
            root = os.path.dirname(os.path.dirname(
                    sequencer.plot_calculation_parameters["save"]))
            res_dir = os.path.join(root, "results")
            kgrid_label = await self._get_label(sequencer)
            for plotname in await aiofiles.os.listdir(res_dir):
                if "decay" not in plotname:
                    continue
                if not plotname.endswith(".pickle"):
                    continue
                decay_type = plotname.replace(".pickle", "").replace(
                        "interpolation", "")
                decay_plot = Plot.load_plot(os.path.join(res_dir, plotname))
                # usually each plot only have one scatter curve
                scatter = decay_plot.plot_objects["scatters"][0]
                scatter.label = kgrid_label
                color = f"C{iseq}"
                scatter.color = color
                scatter.markerfacecolor = color
                if decay_type not in conv_plots:
                    conv_plots[decay_type] = decay_plot
                else:
                    conv_plots[decay_type].add_scatter(scatter)
        final_resdir = os.path.join(
                os.path.dirname(root), "results")
        await self._save_plots(conv_plots, final_resdir)

    async def _save_plots(
            self, conv_plots: Mapping[str, Plot], final_resdir: str) -> None:
        for plotkey, plot in conv_plots.items():
            plot.plot(show=False)
            await plot.save(
                    os.path.join(final_resdir, plotkey + ".pdf"),
                    overwrite=True)
            await plot.save_pickle(
                    os.path.join(final_resdir, plotkey + ".pickle"),
                    overwrite=True)

    async def make_ibte_plots(self) -> None:
        """Make ibte convergence plots."""
        conv_plots = {}
        self._logger.info("Making IBTE convergence plots.")
        for iseq, sequencer in enumerate(self.sequencer):
            if not isinstance(sequencer, QEEPWIBTESequencer):
                # use ibte sequencers only for generality
                continue
            ibte_dir = sequencer.ibte_workdir
            root = os.path.dirname(ibte_dir)
            res_dir = os.path.join(root, "results")
            label = await self._get_label(sequencer)
            for plotname in await aiofiles.os.listdir(res_dir):
                if "ibte" not in plotname:
                    continue
                if not plotname.endswith(".pickle"):
                    continue
                plot_basekey = plotname.replace(".pickle", "")
                resplot = Plot.load_plot(os.path.join(res_dir, plotname))
                for curve in resplot.plot_objects["curves"]:
                    element = curve.label.split("{")[-1].split("}")[0]
                    plotkey = plot_basekey + f"_{element}"
                    conv_plots.setdefault(
                        plotkey,
                        Plot(loglevel=self._loglevel))
                    newplot = conv_plots[plotkey]
                    newplot.add_curve(
                            curve.xdata, curve.ydata,
                            label=label,
                            color=f"C{iseq}",
                            linewidth=2,
                            )
        final_resdir = os.path.join(
                os.path.dirname(root), "results")
        await self._save_plots(conv_plots, final_resdir)
        await self._make_ibte_convergence_plots(
                conv_plots, final_resdir)

    async def _make_ibte_convergence_plots(
            self, conv_plots: Mapping[str, Plot], final_resdir: str) -> None:
        """Make ibte abs and rel convergence plots."""
        absrel_conv_plots = {}
        for plotkey, plot in conv_plots.items():
            rel_plot = Plot()
            rel_plot.xlabel = plot.xlabel
            rel_plot.ylabel = plot.ylabel + " relative convergence [%]"
            rel_plot.title = plot.title
            abs_plot = Plot()
            abs_plot.xlabel = plot.xlabel
            abs_plot.ylabel = plot.ylabel + " absolute convergence"
            abs_plot.title = plot.title
            # need to find highest parameter curve
            highest = self._get_highest_number_idx(plot)
            # compute relative convergence relative to the highest parameter
            highest_curve = plot.plot_objects["curves"][highest]
            ydata = np.asarray(highest_curve.ydata)
            for curve in plot.plot_objects["curves"]:
                if curve is highest_curve:
                    continue
                try:
                    delta = 100 * np.divide(
                            np.asarray(curve.ydata) - ydata, ydata)
                except ValueError as err:
                    self._logger.error(
                            f"Cannot compute ibte delta for '{curve.label}'.")
                    self._logger.exception(err)
                    continue
                rel_plot.add_curve(
                        curve.xdata, delta,
                        label=curve.label,
                        color=curve.color,
                        linewidth=curve.linewidth,
                        marker=curve.marker,
                        markerfacecolor=curve.markerfacecolor,
                        markersize=curve.markersize,
                        )
                abs_plot.add_curve(
                        curve.xdata,
                        np.abs(np.asarray(curve.ydata) - ydata),
                        label=curve.label,
                        color=curve.color,
                        linewidth=curve.linewidth,
                        marker=curve.marker,
                        markerfacecolor=curve.markerfacecolor,
                        markersize=curve.markersize,
                        )
            rel_plot.add_hline(0, linewidth=1, linestyle="--", color="k")
            abs_plot.ylims = [0, None]
            absrel_conv_plots[plotkey + "_relative"] = rel_plot
            absrel_conv_plots[plotkey + "_absolute"] = abs_plot
        await self._save_plots(absrel_conv_plots, final_resdir)

    def _get_highest_number_idx(self, plot: Plot) -> int:
        # find curve index of highest grid
        highest = 0
        highest_prod = 1
        for icurve, curve in enumerate(plot.plot_objects["curves"]):
            label = curve.label
            # everthing contained in label
            # generally look like this
            # Nk=XxXxX or Nk=XxXxX Nq=YxYxY
            # mutiply X and Y numbers. highest bidder is returned
            splitted = label.split("=")
            if len(splitted) > 2:
                # 2 = signs
                splitted = [splitted[1].split(" ")[0], splitted[-1]]
            else:
                splitted = [splitted[-1]]
            numbers = []
            for s in splitted:
                numbers += s.split("x")
            product = 1
            for number in numbers:
                product *= int(number.replace("$", ""))
            if product > highest_prod:
                highest = icurve
                highest_prod = product
        return highest

    async def _get_label(self, sequencer: QEEPWIBTESequencer) -> str:
        if sequencer.ibte_workdir in self._label_dict:
            return self._label_dict[sequencer.ibte_workdir]
        label = await self._extract_label_from_sequencer(sequencer)
        self._label_dict[sequencer.ibte_workdir] = label
        return label

    @abc.abstractmethod
    async def _extract_label_from_sequencer(
            self, sequencer: QEEPWIBTESequencer) -> str:
        pass
