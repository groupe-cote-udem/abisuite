import os

from .phonon_dispersion import QEPhononDispersionSubWorkflow
from ..bases_subworkflows import (
        BasePhononDispersionQgridConvergencePostProcessSubWorkflow,
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart,
        )


class QEPhononDispersionQgridConvergenceSubWorkflow(
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart,
        QEPhononDispersionSubWorkflow):
    """Subworkflow class for a phonon dispersion qgrid convergence."""

    _loggername = "QEPhononDispersionQgridConvergenceSubWorkflow"

    def __init__(self, *args, **kwargs) -> None:
        asr = kwargs.pop("asr", None)
        matdyn_cp = kwargs.pop("matdyn_calculation_parameters", None)
        q2r_iv = kwargs.pop("q2r_input_variables", None)
        q2r_cp = kwargs.pop("q2r_calculation_parameters", None)
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart.__init__(
                self, *args,
                phonons_qpoint_grids=kwargs.pop("phonons_qpoint_grids", None),
                **kwargs,
                )
        QEPhononDispersionSubWorkflow.__init__(
                self, *args,
                asr=asr,
                matdyn_calculation_parameters=matdyn_cp,
                q2r_input_variables=q2r_iv,
                q2r_calculation_parameters=q2r_cp,
                **kwargs)

    async def set_sequencer(self, *args, **kwargs) -> None:
        """Set the phonon part of the subworkflow."""
        cls = BaseSubWorkflowWithPhononDispersionQgridConvergencePart
        await cls.set_sequencer(self, *args, **kwargs)
        for subsequencer in self.sequencer:
            subroot_workdir = self._get_subroot_workdir(
                    subsequencer.phonons_qpoint_grid)
            await self.set_q2r_part(
                    sequencer=subsequencer,
                    q2r_workdir=os.path.join(
                        subroot_workdir, "q2r_run"),
                    set_parts=False,
                    )
            await self.set_matdyn_part(
                    sequencer=subsequencer,
                    matdyn_workdir=os.path.join(
                        subroot_workdir, "matdyn_run"),
                    set_parts=False,
                    )
        self.parts_set += ["q2r", "matdyn"]


class QEPhononDispersionQgridConvergencePostProcessSubWorkflow(
        BasePhononDispersionQgridConvergencePostProcessSubWorkflow):
    """QE phonon dispersion qgrid convergence post processor sub workflow."""

    _loggername = "QEPhononDispersionQgridConvergencePostProcessSubWorkflow"
