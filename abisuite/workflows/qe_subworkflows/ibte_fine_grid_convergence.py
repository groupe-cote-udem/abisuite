import asyncio
import os
from typing import Sequence

from .ibte import QEIBTESubWorkflow
from .ibte_convergence_bases import (
        _BaseQEIBTEConvergencePostProcessSubWorkflow)
from ..bases_subworkflows import subparts_checkpoint
from ...routines import is_list_like
from ...sequencers import QEEPWIBTESequencer


class QEIBTEFineGridConvergenceSubWorkflow(QEIBTESubWorkflow):
    """Subworkflow class for an IBTE fine grid convergence."""

    _plot_name_extension = "ibte_fine_grid_convergence"

    def __init__(
            self, *args, ibte_fine_grids: Sequence[Sequence[int]] = None,
            **kwargs,
            ) -> None:
        QEIBTESubWorkflow.__init__(
                self, *args, **kwargs
                )
        self.ibte_fine_grids = ibte_fine_grids

    async def check_ibte(self, *args, **kwargs) -> None:
        """Check ibte part of subworkflow."""
        if self.checks["ibte"]:
            return
        if self.ibte_fine_grids is None:
            raise ValueError("Need to set 'ibte_fine_grids'.")
        if not is_list_like(self.ibte_fine_grids):
            raise TypeError("'ibte_fine_grids' must be list-like.")
        all_varnames = ["nqf1", "nqf2", "nqf3", "nkf1", "nkf2", "nkf3"]
        for fine_grid in self.ibte_fine_grids:
            if not isinstance(fine_grid, dict):
                raise TypeError(
                        "Every fine grid in 'ibte_fine_grids' must be a dict "
                        "containing all fine grids variables.")
            for varname in all_varnames:
                if varname not in fine_grid:
                    raise ValueError(f"Missing '{varname}'.")
                if not isinstance(fine_grid[varname], int):
                    raise TypeError(f"'{varname}' must be an int.")
        await QEIBTESubWorkflow.check_ibte(
                self, *args, set_check=False, **kwargs)
        if self.use_ibte_converged_fsthick:
            for fine_grid in self.ibte_fine_grids:
                fine_grid.update({"fsthick": self.converged_ibte_fsthick})
        self.checks["ibte"] = True

    @subparts_checkpoint("ibte")
    async def set_sequencer(self, *args, **kwargs):
        """Sets ibte part of subworkflow."""
        coros = []
        for fine_grid in self.ibte_fine_grids:
            coros.append(self._set_ibte_part_fine_grid(
                fine_grid, *args, **kwargs))
        sequencers = await asyncio.gather(*coros)
        for seq in sequencers:
            self.sequencer.append(seq)
        self.parts_set.append("plots")

    async def _set_ibte_part_fine_grid(self, fine_grid, *args, **kwargs):
        sequencer = QEEPWIBTESequencer(loglevel=self._loglevel)
        nkf1, nkf2, nkf3 = (
                fine_grid["nkf1"], fine_grid["nkf2"], fine_grid["nkf3"])
        nqf1, nqf2, nqf3 = (
                fine_grid["nqf1"], fine_grid["nqf2"], fine_grid["nqf3"])
        name = f"nk{nkf1}_{nkf2}_{nkf3}_nq{nqf1}_{nqf2}_{nqf3}"
        ivars = self.ibte_input_variables.copy()
        ivars.update(fine_grid)
        await QEIBTESubWorkflow.set_ibte_part(
                self, *args,
                sequencer=sequencer,
                ibte_workdir=os.path.join(self.root_workdir, name, "ibte_run"),
                ibte_input_variables=ivars,
                set_parts=False,
                **kwargs,
                )
        plot_calculation_parameters = self.plot_calculation_parameters.copy()
        plot_calculation_parameters["save"] = os.path.join(
                self.root_workdir, name, "results", "ibte.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                self.root_workdir, name, "results",
                "ibte.pickle")
        QEIBTESubWorkflow.set_plots(
                self, sequencer=sequencer,
                plot_calculation_parameters=plot_calculation_parameters,
                set_parts=False,
                )
        return sequencer


class QEIBTEFineGridConvergencePostProcessSubWorkflow(
        _BaseQEIBTEConvergencePostProcessSubWorkflow):
    """Post process object to plot ibte fine grid convergence."""

    _loggername = "QEIBTEFineFridConvergencePostProcessSubWorkflow"""

    async def _extract_label_from_sequencer(
            self, sequencer: QEEPWIBTESequencer) -> str:
        iv = sequencer.ibte_input_variables
        nkf1, nkf2, nkf3 = iv["nkf1"], iv["nkf2"], iv["nkf3"]
        nqf1, nqf2, nqf3 = iv["nqf1"], iv["nqf2"], iv["nqf3"]
        return (r"$N_{\mathbf{k}}=$" + f"{nkf1}x{nkf2}x{nkf3} " +
                r"$N_{\mathbf{q}}=$" + f"{nqf1}x{nqf2}x{nqf3}")
    # # TODO: DRY this with the fsthick method
    # async def _post_process_ibte_fine_grid_convergence(self):
    #     # plot final results for ibte fsthick convergence
    #     # plots for resistivity and conductivity
    #     all_res_plots = []
    #     all_cond_plots = []
    #     fine_grids = []
    #     seqs = self.ibte_fine_grid_convergence_sequencer
    #     varnames = ["nqf1", "nqf2", "nqf3", "nkf1", "nkf2", "nkf3"]
    #     colors = []
    #     for iseq, sequencer in enumerate(seqs):
    #         colors.append(f"C{iseq}")
    #         results_dir = os.path.dirname(sequencer.plot_save_pickle)
    #         fine_grids.append(
    #                 {x: sequencer.ibte_input_variables[x] for x in varnames})
    #         for filename in await aiofiles.os.listdir(results_dir):
    #             if ".pickle" not in filename:
    #                 continue
    #             if "serta" in filename.lower():
    #                 continue
    #             if "resistivity" in filename:
    #                 all_res_plots.append(
    #                         Plot.load_plot(
    #                             os.path.join(results_dir, filename)))
    #             else:
    #                 all_cond_plots.append(
    #                         Plot.load_plot(
    #                             os.path.join(results_dir, filename)))
    #     # got all plots. now make 1 plot for each tensor element present.
    #     show = sequencer.plot_calculation_parameters.get("show", True)
    #     results_dir = os.path.join(
    #             os.path.dirname(os.path.dirname(sequencer.ibte_workdir)),
    #             "results")
    #     await asyncio.gather(
    #             *(self._generate_and_save_ibte_plot(
    #                 plot_container, tag, colors, show, fine_grids,
    #                 results_dir)
    #                 for plot_container, tag in zip(
    #                     [all_res_plots, all_cond_plots],
    #                     ["resistivity", "conductivity"],
    #                     )),
    #             )

    # async def _generate_and_save_ibte_plot(
    #         self, plot_container, tag, colors, show, fine_grids, results_dir,
    #         ):
    #     n_curves = len(plot_container[0].curves)
    #     await asyncio.gather(
    #             *(self._save_ibte_plot_for_curve(
    #                 i, plot_container, tag, colors, show, fine_grids,
    #                 results_dir)
    #               for i in range(n_curves)),
    #             )

    # async def _save_ibte_plot_for_curve(
    #         self, i, plot_container, tag, colors, show, fine_grids,
    #         results_dir,
    #         ):
    #     final_plot = Plot(loglevel=self._loglevel)
    #     el = plot_container[
    #             0].curves[i].label.split("{")[-1].split("}")[0]
    #     for (iplot, plot), fine_grid in zip(
    #             enumerate(plot_container), fine_grids):
    #         curve = plot.curves[i]
    #         if len(plot_container) > 1:
    #             color = colors[iplot]
    #         else:
    #             color = curve.color
    #         nkf1, nkf2, nkf3 = (
    #             fine_grid["nkf1"], fine_grid["nkf2"],
    #             fine_grid["nkf3"])
    #         nqf1, nqf2, nqf3 = (
    #             fine_grid["nqf1"], fine_grid["nqf2"],
    #             fine_grid["nqf3"])
    #         label = (f"Nk: {nkf1}x{nkf2}x{nkf3} "
    #                  f"Nq: {nqf1}x{nqf2}x{nqf3}")
    #         final_plot.add_curve(
    #                 curve.xdata, curve.ydata,
    #                 color=color,
    #                 linestyle=curve.linestyle,
    #                 linewidth=curve.linewidth,
    #                 label=label)
    #     final_plot.xlabel = plot.xlabel
    #     final_plot.ylabel = plot.ylabel
    #     final_plot.title = (
    #             f"IBTE fine grid convergence {tag} {el} component")
    #     final_plot.legend = True
    #     final_plot.grid = True
    #     final_plot.plot(show=show)
    #     self._logger.info(
    #             "Saving final IBTE fine grid convergence plots in: "
    #             f"'{results_dir}'")
    #     await final_plot.save(
    #             os.path.join(
    #                 results_dir,
    #                 f"ibte_fine_grid_convergence_{tag}_{el}.pdf"),
    #             overwrite=True,
    #             )
    #     await final_plot.save_pickle(
    #             os.path.join(
    #                 results_dir,
    #                 f"ibte_fine_grid_convergence_{tag}_{el}.pickle"),
    #             overwrite=True,
    #             )
