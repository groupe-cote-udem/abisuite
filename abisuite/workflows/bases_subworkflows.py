import abc
import asyncio
import os
from typing import Any, Mapping, Sequence

from .bases import BaseWorkflow, SequencersList
from ..bases import BaseUtility
from ..exceptions import DevError
from ..plotters import Plot
from ..routines import is_vector
from ..sequencers.bases import BaseSequencer


def subparts_checkpoint(name: str, propagate: bool = False) -> callable:
    """Generates a decorator that handles subpart checkpoints."""
    def decorator(func: callable) -> callable:
        async def wrapped_async(*args, **kwargs) -> None:
            """Wrapped 'setting' async function."""
            subworkflow = args[0]
            if propagate:
                # allow to propagate setting parts in case the overridden
                # method is called
                set_parts = kwargs.get("set_parts", True)
            else:
                set_parts = kwargs.pop("set_parts", True)
            if name in subworkflow.parts_set:
                # do nothing
                subworkflow._logger.info(
                        f"'{name}' subworkflow part already set: "
                        "do not reset.")
                return
            await func(*args, **kwargs)
            if set_parts and name not in subworkflow.parts_set:
                subworkflow.parts_set.append(name)

        def wrapped_sync(*args, **kwargs) -> None:
            """Wrapped 'setting' sync function."""
            subworkflow = args[0]
            if propagate:
                # allow to propagate setting parts in case the overridden
                # method is called
                set_parts = kwargs.get("set_parts", True)
            else:
                set_parts = kwargs.pop("set_parts", True)
            if name in subworkflow.parts_set:
                # do nothing
                subworkflow._logger.info(
                        f"'{name}' subworkflow part already set: "
                        "do not reset.")
                return
            # call setting func
            func(*args, **kwargs)
            if set_parts and name not in subworkflow.parts_set:
                subworkflow.parts_set.append(name)

        if asyncio.iscoroutinefunction(func):
            return wrapped_async
        return wrapped_sync

    return decorator


class BaseWorkflowSubUnit(BaseUtility):
    """Base class for calculation subworkflows and post process ones."""

    def __init__(
            self, *args,
            sequencer: SequencersList | BaseSequencer = None,
            workflow: BaseWorkflow = None,
            **kwargs,
            ) -> None:
        BaseUtility.__init__(self, *args, **kwargs)
        if sequencer is None:
            if not hasattr(self, "sequencer"):
                raise DevError("Need to set sequencer object for subworkflow.")
        else:
            self.sequencer = sequencer
        if workflow is None:
            if not hasattr(self, "workflow"):
                raise DevError("Need to set workflow object for subworkflow.")
        else:
            self.workflow = workflow


class BaseSubWorkflow(BaseWorkflowSubUnit):
    """Base class for subworkflows."""

    _have_plots = False
    _plot_name_extension = None
    _plot_title = None

    def __init__(
            self, *args,
            root_workdir: str = None,
            plot_calculation_parameters: dict[str, Any] = None,
            **kwargs,
            ):
        """Init method for BaseSubWorkflow.

        Parameters
        ----------
        sequencer: SequencersList or BaseSequencer instance.
            The sequencer to set by the subworkflow object.
        workflow: BaseWorkflow or it's subclasses instance.
            The workflow that controls the sequencer.
        root_workdir: str
            The root workdir for the sequencer.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        BaseWorkflowSubUnit.__init__(self, *args, **kwargs)
        self.root_workdir = root_workdir
        if not hasattr(self, "plot_calculation_parameters"):
            self.plot_calculation_parameters = plot_calculation_parameters
        else:
            if self.plot_calculation_parameters is None:
                self.plot_calculation_parameters = plot_calculation_parameters
        if not hasattr(self, "checks"):
            self.checks = {}
        self.checks["root_workdir"] = False
        if not hasattr(self, "parts_set"):
            self.parts_set = []
            # to prevent setting again in case of multiclass

    @property
    def ready_to_set_sequencer(self):
        """True if all checks passed."""
        for checkname, checkvalue in self.checks.items():
            if not checkvalue:
                self._logger.info(
                        f"{checkname} check did not pass yet.")
                return False
        return True

    @property
    def stop_at_workflow(self):
        """Return the stopping part of the workflow."""
        return self.workflow.stop_at_workflow

    @stop_at_workflow.setter
    def stop_at_workflow(self, stop):
        self.workflow.stop_at_workflow = stop

    def check(self):
        """Checks that everything is set correctly for the subworkflow.

        Raises
        ------
        ValueError: If root_workdir was not set.
        """
        self.check_root_workdir()

    def check_root_workdir(self) -> None:
        """Check root workdir has been set."""
        if self.root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        self.checks["root_workdir"] = True
        if self.plot_calculation_parameters is None:
            self.plot_calculation_parameters = {}
        if self._have_plots:
            if self._plot_name_extension is None:
                raise DevError("Need to set '_plot_name_extension'.")
            self.plot_calculation_parameters["save"] = os.path.join(
                    self.root_workdir, "results",
                    f"{self._plot_name_extension}.pdf")
            self.plot_calculation_parameters["save_pickle"] = os.path.join(
                    self.root_workdir, "results",
                    f"{self._plot_name_extension}.pickle")

    async def set_sequencer(self):
        """Set sequencer."""
        self.set_plots()

    @subparts_checkpoint("plots")
    def set_plots(
            self, sequencer: BaseSequencer = None,
            plot_calculation_parameters: Mapping[str, Any] = None,
            plot_extension_extra_prefix: str = "",
            **kwargs,
            ) -> None:
        """Set plot parameters if needed."""
        if not self._have_plots:
            return
        if sequencer is None:
            sequencer = self.sequencer
        if plot_calculation_parameters is None:
            plot_calculation_parameters = (
                    self.plot_calculation_parameters.copy())
        if self._plot_title is not None:
            plot_calculation_parameters["title"] = self._plot_title
        self._add_calculation_parameters(
            "plot_", plot_calculation_parameters.copy(), sequencer)

    async def use_converged_quantities(self, *args, **kwargs) -> None:
        """Use converged quantities from a different part of the workflow."""
        await self.workflow._use_converged_quantities(*args, **kwargs)

    def _add_calculation_parameters(self, *args, **kwargs):
        return self.workflow._add_calculation_parameters(*args, **kwargs)

    async def use_converged_calculations(
            self, sequencer: BaseSequencer = None, *args, **kwargs,
            ) -> None:
        """Use converged calculations."""
        await self.workflow._use_converged_calculations(
                sequencer if sequencer is not None else self.sequencer,
                *args, **kwargs)

    async def use_any_converged_calculation(self, *args, **kwargs):
        """Use any converged calculation for any sequencer part."""
        await self.workflow._use_any_converged_calculation(
                *args, **kwargs)


class BaseSubWorkflowWithSCFPart(BaseSubWorkflow, abc.ABC):
    """Base class for subworkflows with scf parts.

    Containing a SCF part means we can converge quantities for it.
    Therfore this subworkflow contains all converging options as well.
    """

    def __init__(
            self, *args,
            scf_input_variables: dict[str, Any] = None,
            scf_calculation_parameters: dict[str, Any] = None,
            use_converged_phonon_smearing: bool = False,
            use_converged_phonon_ecut: bool = False,
            use_converged_gs_ecut: bool = False,
            use_converged_gs_kgrid: bool = False,
            use_relaxed_geometry: bool = False,
            use_gs: bool = False,
            use_gs_from: str = None,
            **kwargs):
        BaseSubWorkflow.__init__(self, *args, **kwargs)
        self.scf_input_variables = scf_input_variables
        self.scf_calculation_parameters = scf_calculation_parameters
        self.use_converged_phonon_smearing = use_converged_phonon_smearing
        self.use_converged_phonon_ecut = use_converged_phonon_ecut
        self.use_relaxed_geometry = use_relaxed_geometry
        self.use_converged_gs_ecut = use_converged_gs_ecut
        self.use_converged_gs_kgrid = use_converged_gs_kgrid
        self.use_gs = use_gs
        self.use_gs_from = use_gs_from
        self.checks["scf"] = False

    def check(self, **kwargs):
        """Checks that everything is set correctly for the subworkflow.

        Raises
        ------
        ValueError: If scf_input_variables was not set.
        """
        BaseSubWorkflow.check(self)
        self.check_scf_part(**kwargs)

    def check_scf_part(
            self, check_scf_input_variables: bool = True,
            set_check: bool = True,
            ) -> None:
        """Check SCF part of subworkflow."""
        if self.checks["scf"]:
            # already checked and confirmed
            return
        if self.scf_input_variables is None and check_scf_input_variables and (
                not self.use_gs):
            raise ValueError("Need to set 'scf_input_variables'.")
        if self.use_gs and self.use_gs_from is not None:
            raise ValueError(
                    "Cannot set 'use_gs=True' and "
                    "'use_gs/scf_from' at same time.")
        if self.use_gs:
            self.use_gs_from = "gs"
        if set_check:
            self.checks["scf"] = True

    async def apply_converged_quantities(self, *args, **kwargs):
        """Apply converged quantities to given dicts of input variables."""
        if self.use_relaxed_geometry and (
                self.workflow.relaxed_geometry is None):
            if not await self.workflow.relaxation_sequencer.sequence_completed:
                self.stop_at_workflow = "relaxation"
                return
        await self.apply_converged_quantities_scf(*args, **kwargs)

    async def apply_converged_quantities_scf(self, **kwargs):
        """Apply converged quantities to the scf part of sequencer."""
        if self.use_gs:
            return
        await self.workflow._use_converged_quantities(
                self.scf_input_variables,
                use_relaxed_geometry=self.use_relaxed_geometry,
                use_converged_gs_ecut=self.use_converged_gs_ecut,
                use_converged_phonon_ecut=self.use_converged_phonon_ecut,
                use_converged_phonon_smearing=(
                    self.use_converged_phonon_smearing),
                **kwargs,
                )

    async def set_sequencer(self, scf_workdir: str = None, **kwargs) -> None:
        """Sets sequencer."""
        await BaseSubWorkflow.set_sequencer(self, **kwargs)
        await self.set_scf_part(scf_workdir=scf_workdir)

    @subparts_checkpoint("scf")
    async def set_scf_part(
            self, scf_workdir: str = None,
            scf_input_variables: Mapping[str, Any] = None,
            sequencer: BaseSequencer = None,
            ) -> None:
        """Sets the scf part of sequencer."""
        if scf_workdir is None:
            scf_workdir = self.root_workdir
        if sequencer is None:
            sequencer = self.sequencer
        if scf_input_variables is None:
            scf_input_variables = self.scf_input_variables
        if self.use_gs:
            await self.use_converged_calculation(
                    sequencer=sequencer, scf_workdir=scf_workdir,
                    )
            return
        if self.use_gs_from is not None:
            await self.use_any_converged_calculation(
                    sequencer, "scf_",
                    default_workdir=scf_workdir,
                    use_from=self.use_gs_from,
                    default_input_variables=scf_input_variables,
                    default_calculation_parameters=(
                        self.scf_calculation_parameters),
                    )
            return
        sequencer.scf_workdir = scf_workdir
        sequencer.scf_input_variables = scf_input_variables
        self._add_calculation_parameters(
                "scf_", self.scf_calculation_parameters,
                sequencer,
                )

    async def use_converged_calculation(
            self, *args, scf_workdir: str = None, **kwargs) -> None:
        """Use converged calculation."""
        await BaseSubWorkflow.use_converged_calculations(
                self, use_gs=self.use_gs,
                use_converged_gs_ecut=self.use_converged_gs_ecut,
                use_converged_gs_kgrid=self.use_converged_gs_kgrid,
                use_converged_phonon_ecut=self.use_converged_phonon_ecut,
                use_relaxed_geometry=self.use_relaxed_geometry,
                scf_input_variables=self.scf_input_variables,
                scf_calculation_parameters=self.scf_calculation_parameters,
                scf_workdir=scf_workdir,
                **kwargs,
                )


class BaseSubWorkflowWithNSCFPart(BaseSubWorkflowWithSCFPart):
    """Subworkflow class for calculations with NSCF + SCF parts."""

    def __init__(
            self, *args, nscf_input_variables: dict[str, Any] = None,
            use_nscf_from: bool = None,
            nscf_calculation_parameters: dict[str, Any] = None,
            **kwargs,
            ):
        BaseSubWorkflowWithSCFPart.__init__(self, *args, **kwargs)
        self.use_nscf_from = use_nscf_from
        self.nscf_calculation_parameters = nscf_calculation_parameters
        self.nscf_input_variables = nscf_input_variables
        self.checks["nscf"] = False

    async def apply_converged_quantities(
            self, sequencer: BaseSequencer = None):
        """Apply converged quantities."""
        await BaseSubWorkflowWithSCFPart.apply_converged_quantities(
                self, sequencer=sequencer)
        await self.apply_converged_quantities_nscf(sequencer=sequencer)

    async def apply_converged_quantities_nscf(
            self, sequencer: BaseSequencer = None, **kwargs):
        """Apply converged quantities to nscf part."""
        if self.use_nscf_from is not None:
            return
        await self.use_converged_quantities(
                self.nscf_input_variables,
                sequencer=(
                    sequencer if sequencer is not None else self.sequencer),
                use_relaxed_geometry=self.use_relaxed_geometry,
                **kwargs,
                )

    def check(self, skip_nscf_check: bool = False) -> None:
        """Check everything is ready for setup."""
        BaseSubWorkflowWithSCFPart.check(self)
        if skip_nscf_check:
            return
        self.nscf_input_variables = self.check_nscf()

    def check_nscf(
            self, nscf_input_variables: Mapping[str, Any] = None,
            nscf_calculation_parameters: Mapping[str, Any] = None,
            ) -> Mapping[str, Any]:
        """Check nscf part of subworkflow.

        Parameters
        ----------
        nscf_input_variables: dict, optional
            The dict of input variables to check. If None, the instance
            attribute of the same name is taken.
        nscf_calculation_parameters: dict, optional
            The dicf of nscf calculation parameters to check. If None,
            the instance attribute of the same name is taken.

        Returns
        -------
        dict: The dict of updated nscf input variables.
        """
        # nscf variables can be none.
        # in that case, they will be loaded from GS
        if nscf_input_variables is None:
            nscf_input_variables = self.nscf_input_variables
        if nscf_input_variables is None:
            if self.use_gs:
                nscf_input_variables = (
                        self.workflow.gs_sequencer.scf_input_variables.copy())
            else:
                if self.use_nscf_from is None:
                    raise ValueError("Need to set 'nscf_input_variables'.")
        else:
            # otherwise they are loaded
            if self.use_gs:
                nscf_vars = (
                        self.workflow.gs_sequencer.scf_input_variables.copy())
                nscf_vars.update(nscf_input_variables)
                self.nscf_input_variables = nscf_vars
        if nscf_calculation_parameters is None:
            nscf_calculation_parameters = self.nscf_calculation_parameters
        if nscf_calculation_parameters is None:
            if self.use_nscf_from is not None:
                self.nscf_calculation_parameters = {}
            else:
                raise ValueError("Need to set 'nscf_calculation_parameters'.")
        self.checks["nscf"] = True
        return nscf_input_variables

    async def set_sequencer(self):
        """Setup sequencer."""
        await BaseSubWorkflowWithSCFPart.set_sequencer(self)
        await self.set_nscf_part()

    @subparts_checkpoint("nscf")
    async def set_nscf_part(
            self, nscf_part_name: str = "nscf_",
            nscf_workdir: str = None,
            sequencer: BaseSequencer = None,
            nscf_input_variables: Mapping[str, Any] = None,
            nscf_calculation_parameters: Mapping[str, Any] = None,
            use_nscf_from: str = None,
            add_nscf_input_variables: bool = True,
            ) -> None:
        """Setup nscf part of subworkflow."""
        if nscf_workdir is None:
            nscf_workdir = os.path.join(self.root_workdir, "nscf_run")
        if sequencer is None:
            sequencer = self.sequencer
        if nscf_input_variables is None:
            nscf_input_variables = self.nscf_input_variables
        if nscf_input_variables is not None:
            nscf_input_variables = nscf_input_variables.copy()
        if nscf_calculation_parameters is None:
            nscf_calculation_parameters = self.nscf_calculation_parameters
        if use_nscf_from is None:
            use_nscf_from = self.use_nscf_from
        self._logger.info(
                "Setting nscf part of subworkflow with "
                f"workdir='{nscf_workdir}'.")
        await self.use_any_converged_calculation(
            sequencer, nscf_part_name,
            use_from=use_nscf_from,
            default_workdir=nscf_workdir,
            default_input_variables=nscf_input_variables,
            default_calculation_parameters=(
                nscf_calculation_parameters.copy()),
            add_input_variables=add_nscf_input_variables,
            )


class BaseDOSSubWorkflow(BaseSubWorkflowWithNSCFPart):
    """Base class for dos subworkflows."""

    _have_plots = True
    _plot_name_extension = "dos"
    _plot_title = "Electronic DOS"

    def __init__(
            self, *args,
            dos_input_variables: Mapping[str, Any] = None,
            dos_calculation_parameters: Mapping[str, Any] = None,
            dos_fine_kpoint_grid_variables: Mapping[str, Any] = None,
            **kwargs) -> None:
        BaseSubWorkflowWithNSCFPart.__init__(self, *args, **kwargs)
        self.dos_input_variables = dos_input_variables
        self.dos_calculation_parameters = dos_calculation_parameters
        self.dos_fine_kpoint_grid_variables = dos_fine_kpoint_grid_variables
        self.checks["dos"] = False

    def check(self) -> None:
        """Check all parts of subworkflow."""
        BaseSubWorkflowWithNSCFPart.check(self)
        self.check_dos()

    def check_dos(
            self, set_check: bool = True,
            check_dos_fine_kpoint_grid_variables: bool = True,
            ) -> None:
        """Check dos part of subworkflow."""
        if self.dos_input_variables is None:
            raise ValueError("Need to set dos_input_variables.")
        if self.dos_calculation_parameters is None:
            raise ValueError("Need to set dos_calculation_parameters.")
        if self.dos_fine_kpoint_grid_variables is None and (
                check_dos_fine_kpoint_grid_variables):
            raise ValueError("Need to set dos_fine_kpoint_grid_variables.")
        if set_check:
            self.checks["dos"] = True

    def set_sequencer(self, *args, **kwargs) -> None:
        """Sets the sequencer."""
        BaseSubWorkflowWithNSCFPart.set_sequencer(self, *args, **kwargs)
        self.set_dos_part()

    @subparts_checkpoint("nscf", propagate=True)
    async def set_nscf_part(self, *args, **kwargs) -> None:
        """Set nscf part of subworkflow."""
        await BaseSubWorkflowWithNSCFPart.set_nscf_part(
                self, *args, add_nscf_input_variables=False, **kwargs)

    @subparts_checkpoint("dos")
    async def set_dos_part(
            self, sequencer: BaseSequencer = None,
            dos_workdir: str = None,
            dos_fine_kpoint_grid_variables: Mapping[str, Any] = None,
            ) -> None:
        """Set dos part of subworkflow."""
        if dos_workdir is None:
            dos_workdir = os.path.join(self.root_workdir, "dos")
        if sequencer is None:
            sequencer = self.sequencer
        if dos_fine_kpoint_grid_variables is None:
            dos_fine_kpoint_grid_variables = (
                    self.dos_fine_kpoint_grid_variables)
        sequencer.dos_workdir = dos_workdir
        sequencer.dos_input_variables = self.dos_input_variables.copy()
        sequencer.dos_fine_kpoint_grid_variables = (
                dos_fine_kpoint_grid_variables)
        sequencer.dos_calculation_parameters = self.dos_calculation_parameters


class BaseBandStructureSubWorkflow(BaseSubWorkflowWithSCFPart):
    """Base class for band structure subworkflows."""

    _have_plots = True
    _plot_name_extension = "band_structure"
    _plot_title = "Band Structure"

    def __init__(
            self, *args,
            band_structure_input_variables: dict[str, Any] = None,
            band_structure_calculation_parameters: dict[str, Any] = None,
            band_structure_kpoint_path: (
                Sequence[Mapping[str, Sequence[float]]]) = None,
            band_structure_kpoint_path_density: int = None,
            use_band_structure_from: str = None,
            use_band_structure: bool = False,
            **kwargs,
            ) -> None:
        BaseSubWorkflowWithSCFPart.__init__(
                self, *args, **kwargs)
        self.band_structure_input_variables = band_structure_input_variables
        self.band_structure_calculation_parameters = (
                band_structure_calculation_parameters)
        self.band_structure_kpoint_path = band_structure_kpoint_path
        self.band_structure_kpoint_path_density = (
                band_structure_kpoint_path_density)
        self.use_band_structure = use_band_structure
        self.use_band_structure_from = use_band_structure_from
        self.checks["band_structure"] = False

    @property
    def default_band_structure_workdir(self):
        """The default band structure workdir."""
        return os.path.join(
                self.root_workdir, "band_structure",
                "band_structure_run",
                )

    def check(self):
        """Check everything is consistent with band structure subworkflow."""
        # skip nscf check
        BaseSubWorkflowWithSCFPart.check(self)
        self.check_band_structure()

    def check_band_structure(
            self, *args, pop_variables: Sequence[str] = None,
            **kwargs,
            ) -> None:
        """Check band structure part of subworkflow."""
        if self.use_band_structure:
            seq = self.workflow.band_structure_sequencer
            self.use_band_structure_from = seq.band_structure_workdir
            self.checks["band_structure"] = True
            return
        if self.band_structure_input_variables is None:
            if self.use_gs:
                self.band_structure_input_variables = (
                        self.workflow.gs_sequencer.scf_input_variables.copy())
            else:
                if self.use_band_structure_from is None:
                    raise ValueError(
                            "Need to set 'band_structure_input_variables'.")
        else:
            # otherwise they are loaded
            if self.use_gs:
                bs_vars = (
                        self.workflow.gs_sequencer.scf_input_variables.copy())
                bs_vars.update(self.band_structure_input_variables)
                self.band_structure_input_variables = bs_vars
                if pop_variables is not None:
                    for pop_variable in pop_variables:
                        bs_vars.pop(pop_variable, None)
        if self.band_structure_kpoint_path is None:
            raise ValueError("Need to set 'band_structure_kpoint_path'.")
        if self.band_structure_kpoint_path_density is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path_density'.")
        if self.use_band_structure and (
                self.use_band_structure_from is not None):
            raise ValueError(
                    "Cannot set 'use_band_structure=True' and "
                    "'use_band_structure_from' at same time.")
        self.checks["band_structure"] = True

    async def apply_converged_quantities(self):
        """Apply converged quantities."""
        await BaseSubWorkflowWithSCFPart.apply_converged_quantities(self)
        await self.apply_converged_quantities_band_structure()

    async def apply_converged_quantities_band_structure(
            self, *args, **kwargs):
        """Apply converged quantities to band structure part."""
        if self.use_band_structure:
            return
        await self.use_converged_quantities(
                self.band_structure_input_variables,
                sequencer=self.sequencer,
                use_relaxed_geometry=self.use_relaxed_geometry,
                **kwargs,
                )

    async def set_sequencer(self) -> None:
        """Set the sequencer."""
        await BaseSubWorkflowWithSCFPart.set_sequencer(self)
        await self.set_band_structure_part()

    @subparts_checkpoint("band_structure")
    async def set_band_structure_part(
            self, *args,
            sequencer: BaseSequencer = None,
            band_structure_workdir: str = None,
            **kwargs):
        """Set the band structure part of the subworkflow."""
        if sequencer is None:
            sequencer = self.sequencer
        if self.use_band_structure:
            bsseq = self.workflow.band_structure_sequencer
            sequencer.band_structure_workdir = (
                    bsseq.band_structure_workdir)
            sequencer.band_structure_kpoint_path = (
                    bsseq.band_structure_kpoint_path)
            sequencer.band_structure_kpoint_path_density = (
                    bsseq.band_structure_kpoint_path_density)
            sequencer.band_structure_input_variables = (
                    bsseq.band_structure_input_variables)
            return
        sequencer.band_structure_kpoint_path = (
                self.band_structure_kpoint_path)
        bskpd = self.band_structure_kpoint_path_density
        sequencer.band_structure_kpoint_path_density = bskpd
        if band_structure_workdir is None:
            band_structure_workdir = os.path.join(
                    self.root_workdir, "band_structure_run")
        await self.use_any_converged_calculation(
            sequencer, "band_structure",
            use_from=self.use_band_structure_from,
            default_workdir=band_structure_workdir,
            default_input_variables=self.band_structure_input_variables,
            default_calculation_parameters=(
                self.band_structure_calculation_parameters),
            add_input_variables=True,
            )


class BaseSubWorkflowWithPhononsPart(BaseSubWorkflowWithSCFPart):
    """Subworkflow base class for phonons calcs.

    They at least contain a scf part as well.
    """

    def __init__(
            self, *args,
            phonons_input_variables: dict[str, Any] = None,
            phonons_calculation_parameters: dict[str, Any] = None,
            phonons_qpoints_split_by_irreps: Sequence[int] = None,
            **kwargs,
            ):
        BaseSubWorkflowWithSCFPart.__init__(self, *args, **kwargs)
        self.phonons_input_variables = phonons_input_variables
        self.phonons_calculation_parameters = phonons_calculation_parameters
        self.phonons_qpoints_split_by_irreps = phonons_qpoints_split_by_irreps
        self.checks["phonons"] = False

    def check(
            self, skip_to_scf: bool = False, check_phonons: bool = True,
            ) -> None:
        """Check that everything is consistent and all variables are set."""
        BaseSubWorkflowWithSCFPart.check(self)
        if skip_to_scf:
            self.checks["phonons"] = True
            return
        if check_phonons:
            self.check_phonons()
        else:
            self.checks["phonons"] = True

    def check_phonons(self, set_check: bool = True):
        """Check phonons part of subworkflow."""
        if self.phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        if self.phonons_calculation_parameters is None:
            raise ValueError(
                    "Need to set 'phonons_calculation_parameters'.")
        if set_check:
            self.checks["phonons"] = True

    async def set_sequencer(
            self, phonons_workdir: str = None, **kwargs) -> None:
        """Set the sequencer."""
        await BaseSubWorkflowWithSCFPart.set_sequencer(self, **kwargs)
        await self.set_phonons_part(phonons_workdir=phonons_workdir)

    @subparts_checkpoint("phonons")
    async def set_phonons_part(
            self,
            sequencer: BaseSequencer = None,
            phonons_workdir: str = None,
            ) -> None:
        """Set the phonon part of sequencer."""
        # manage the phonon part
        if phonons_workdir is None:
            phonons_workdir = self.root_workdir
        if sequencer is None:
            sequencer = self.sequencer
        sequencer.phonons_workdir = phonons_workdir
        sequencer.phonons_input_variables = self.phonons_input_variables.copy()
        if self.phonons_qpoints_split_by_irreps is not None:
            sequencer.phonons_qpoints_split_by_irreps = (
                    self.phonons_qpoints_split_by_irreps)
        self._add_calculation_parameters(
                "phonons_", self.phonons_calculation_parameters,
                sequencer)


class BaseSubWorkflowWithPhononDispersionPart(
        BaseSubWorkflowWithPhononsPart):
    """Base subworkflow class for phonon dispersion subworkflows."""

    _have_plots = True
    _plot_name_extension = "phonon_dispersion"

    def __init__(
            self, *args,
            phonons_qpoint_grid: Sequence[int] = None,
            qpoint_path: Sequence[dict] = None,
            qpoint_path_density: int = None,
            **kwargs,
            ) -> None:
        BaseSubWorkflowWithPhononsPart.__init__(self, *args, **kwargs)
        self.phonons_qpoint_grid = phonons_qpoint_grid
        self.qpoint_path = qpoint_path
        self.qpoint_path_density = qpoint_path_density

    def check(
            self, skip_to_scf: bool = False,
            check_phonons: bool = True,
            **kwargs,
            ) -> None:
        """Check that everything is consistent and all variables are set."""
        BaseSubWorkflowWithPhononsPart.check(
                self,
                skip_to_scf=skip_to_scf, check_phonons=check_phonons,
                **kwargs,
                )
        if skip_to_scf:
            # don't check all phonon disp stuff.
            self.ready_to_set_sequencer = True
            return
        if check_phonons:
            self.check_phonons()

    def check_phonons(
            self, phonons_qpoint_grid: Sequence[int] = None,
            set_check: bool = True,
            ) -> None:
        """Checkt phonons part of subworkflow."""
        BaseSubWorkflowWithPhononsPart.check_phonons(self, set_check=False)
        if phonons_qpoint_grid is None:
            phonons_qpoint_grid = self.phonons_qpoint_grid
        if phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        if not is_vector(phonons_qpoint_grid, length=3):
            raise ValueError(
                    "'phonons_qpoint_grid' should be a length 3 vector.")
        if set_check:
            self.checks["phonons"] = True

    async def set_sequencer(self):
        """Set the sequencer."""
        await BaseSubWorkflowWithPhononsPart.set_sequencer(
                self,
                scf_workdir=os.path.join(self.root_workdir, "gs"),
                phonons_workdir=os.path.join(
                    self.root_workdir, "phonons_runs/ph_q"))

    @subparts_checkpoint("phonons", propagate=True)
    async def set_phonons_part(
            self, *args, sequencer: BaseSequencer = None,
            phonons_qpoint_grid: Sequence[int] = None,
            **kwargs) -> None:
        """Set the phonon part of the sequencer."""
        if sequencer is None:
            sequencer = self.sequencer
        if phonons_qpoint_grid is None:
            phonons_qpoint_grid = self.phonons_qpoint_grid
        sequencer.phonons_qpoint_grid = phonons_qpoint_grid
        await BaseSubWorkflowWithPhononsPart.set_phonons_part(
                self, *args, sequencer=sequencer, **kwargs)


class BaseSubWorkflowWithPhononDispersionQgridConvergencePart(
        BaseSubWorkflowWithPhononDispersionPart):
    """Base class for phonon dispersion qgrid convergence subworkflows."""

    def __init__(
            self, *args, phonons_qpoint_grids: Sequence[Sequence[int]] = None,
            **kwargs,
            ) -> None:
        BaseSubWorkflowWithPhononDispersionPart.__init__(self, *args, **kwargs)
        self.phonons_qpoint_grids = phonons_qpoint_grids

    def check_phonons(self, set_check: bool = True) -> None:
        """Check phonons part of subworkflow."""
        if self.phonons_qpoint_grids is None:
            raise ValueError("Need to set 'phonons_qpoint_grids'.")
        for qpoint_grid in self.phonons_qpoint_grids:
            BaseSubWorkflowWithPhononDispersionPart.check_phonons(
                    self, phonons_qpoint_grid=qpoint_grid,
                    set_check=False,
                    )
        if set_check:
            self.checks["phonons"] = True

    async def set_sequencer(self, *args, **kwargs) -> None:
        """Set the phonon part of the subworkflow."""
        seq_cls = self.workflow._phonon_dispersion_sequencer_cls
        self._logger.info(
                "Setting phonon dispersion qgrid convergence sequencer.")
        for phonons_qpoint_grid in self.phonons_qpoint_grids:
            sequencer = seq_cls(loglevel=self._loglevel)
            subroot_workdir = self._get_subroot_workdir(phonons_qpoint_grid)
            await self.set_scf_part(
                sequencer=sequencer,
                scf_workdir=os.path.join(subroot_workdir, "scf_run"),
                set_parts=False,
                )
            await self.set_phonons_part(
                    sequencer=sequencer,
                    phonons_qpoint_grid=phonons_qpoint_grid,
                    phonons_workdir=os.path.join(
                        subroot_workdir, "phonons_runs", "ph_q"),
                    set_parts=False,
                    )
            plot_params = self.plot_calculation_parameters.copy()
            resdir = os.path.join(subroot_workdir, "results")
            plot_params["save"] = os.path.join(
                    resdir, os.path.basename(plot_params["save"]))
            plot_params["save_pickle"] = os.path.join(
                    resdir, os.path.basename(plot_params["save_pickle"]))
            self.set_plots(
                sequencer=sequencer, set_parts=False,
                plot_calculation_parameters=plot_params,
                )
            self.sequencer.append(sequencer)
        self.parts_set += ["phonons", "plots"]

    def _get_subroot_workdir(self, qpoint_grid: Sequence[int]) -> str:
        return os.path.join(
                    self.root_workdir,
                    "qgrid_" + "_".join([str(q) for q in qpoint_grid]))


class BasePhononDispersionQgridConvergencePostProcessSubWorkflow(
        BaseWorkflowSubUnit):
    """Base post processor class for phonon qgrid convergence sub workflows."""

    async def make_convergence_plots(self):
        """Make phonon dispersion qgrid convergence plots."""
        # plot final results
        all_plots = []
        seqs = self.workflow.phonon_dispersion_qgrid_convergence_sequencer
        for iseq, sequencer in enumerate(seqs):
            plot = Plot.load_plot(sequencer.plot_save_pickle)
            plot.plot_objects["curves"][0].label = (
                    r"$N_q = $" + " ".join(
                        [str(x) for x in sequencer.phonons_qpoint_grid]))
            for curve in plot.plot_objects["curves"]:
                curve.color = f"C{iseq}"
            all_plots.append(plot)
        final_plot = sum(all_plots[1:], all_plots[0])
        show = sequencer.plot_calculation_parameters.get("show", True)
        final_plot.plot(show=show)
        results_dir = os.path.join(
                os.path.dirname(
                    os.path.dirname(
                        os.path.dirname(
                            sequencer.phonons_workdir))),
                "results")
        await final_plot.save(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pdf"),
                overwrite=True,
                )
        await final_plot.save_pickle(
                os.path.join(
                    results_dir,
                    "phonon_dispersion_qpoint_grid_convergence.pickle"),
                overwrite=True)


class BaseSubWorkflowWithGSSmearingConvergencePart(
        BaseSubWorkflowWithSCFPart):
    """Base class for gs smearing convergence subworkflows."""

    _have_plots = True
    _plot_name_extension = "gs_smearing_convergence"

    def __init__(
            self, *args,
            scf_smearings: Sequence[float] = None,
            scf_kgrids: Sequence[Mapping[str, Any]] = None,
            scf_convergence_criterion: float = None,
            **kwargs,
            ) -> None:
        BaseSubWorkflowWithSCFPart.__init__(
                self, *args,
                # different gs for all calcs
                use_gs=False, use_gs_from=None,
                **kwargs,
                )
        self.scf_kgrids = scf_kgrids
        self.scf_smearings = scf_smearings
        self.scf_convergence_criterion = scf_convergence_criterion

    def check_scf_part(
            self, set_check: bool = True,
            check_scf_convergence_criterion: bool = False,
            ) -> None:
        """Check scf part of subworkflow."""
        if self.checks["scf"]:
            return
        BaseSubWorkflowWithSCFPart.check_scf_part(self, set_check=False)
        if self.scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        if self.scf_smearings is None:
            raise ValueError("Need to set 'scf_smearings'.")
        if self.scf_convergence_criterion is None and (
                check_scf_convergence_criterion):
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        if set_check:
            self.checks["scf"] = True

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(
            self, *args, scf_workdir: str = None, **kwargs) -> None:
        """Set scf part of subworkflow."""
        if scf_workdir is None:
            scf_workdir = self.root_workdir
        await BaseSubWorkflowWithSCFPart.set_scf_part(
                self, *args,
                scf_workdir=scf_workdir,
                **kwargs)
        seq = self.sequencer
        seq.scf_kgrids = self.scf_kgrids
        seq.scf_smearings = self.scf_smearings
        seq.scf_convergence_criterion = self.scf_convergence_criterion


class BaseSubWorkflowWithPhononSmearingConvergencePart(
        BaseSubWorkflowWithGSSmearingConvergencePart,
        BaseSubWorkflowWithPhononsPart):
    """Base class for phonon smearing convergence subworkflows."""

    _plot_name_extension = "phonon_smearing_convergence"

    def __init__(
            self, *args,
            phonons_convergence_criterion: float = None,
            phonons_qpt: Sequence[float] = None,
            **kwargs,
            ) -> None:
        BaseSubWorkflowWithGSSmearingConvergencePart.__init__(
                self,
                sequencer=kwargs.pop("sequencer", None),
                scf_kgrids=kwargs.pop("scf_kgrids", None),
                scf_smearings=kwargs.pop("scf_smearings", None),
                workflow=kwargs.pop("workflow", None),
                )
        BaseSubWorkflowWithPhononsPart.__init__(
                self, *args,
                # only 1 phonon so disable this
                phonons_qpoints_split_by_irreps=None,
                # different gs for all calcs
                use_gs=False, use_gs_from=None,
                **kwargs)
        self.phonons_qpt = phonons_qpt
        self.phonons_convergence_criterion = phonons_convergence_criterion

    def check_phonons_part(self) -> None:
        """Check phonons part of workflow."""
        if self.checks["phonons"]:
            return
        BaseSubWorkflowWithPhononsPart.check_phonons_part(
                self, set_check=False)
        if self.phonons_qpt is None:
            raise ValueError("Need to set 'phonons_qpt'.")
        if self.phonons_convergence_criterion is None:
            raise ValueError("Need to set 'phonons_convergence_criterion'.")
        self.checks["phonons"] = True

    def check_scf_part(self, set_check: bool = True) -> None:
        """Check scf part of subworkflow."""
        if self.checks["scf"]:
            return
        BaseSubWorkflowWithPhononsPart.check_scf_part(
                self, set_check=False)
        BaseSubWorkflowWithGSSmearingConvergencePart.check_scf_part(
                self, set_check=False, check_scf_convergence_criterion=False,
                )
        if set_check:
            self.checks["scf"] = True

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(
            self, *args, scf_workdir: str = None, **kwargs) -> None:
        """Set scf part of subworkflow."""
        if scf_workdir is None:
            scf_workdir = os.path.join(self.root_workdir, "scf_runs")
        await BaseSubWorkflowWithGSSmearingConvergencePart.set_scf_part(
                self, *args, scf_workdir=scf_workdir,
                **kwargs)

    @subparts_checkpoint("phonons", propagate=True)
    async def set_phonons_part(self, *args, **kwargs) -> None:
        """Set phonons part of subworkflow."""
        kwargs.pop("phonons_workdir", None)
        await BaseSubWorkflowWithPhononsPart.set_phonons_part(
                self, *args,
                phonons_workdir=os.path.join(
                    self.root_workdir, "phonons_runs"),
                **kwargs)
        seq = self.sequencer
        seq.phonons_qpt = self.phonons_qpt
        seq.phonons_convergence_criterion = self.phonons_convergence_criterion
