import os

from ..bases_subworkflows import (
        BasePhononDispersionQgridConvergencePostProcessSubWorkflow,
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart,
        )
from ...routines import is_2d_arr


class AbinitPhononDispersionQgridConvergenceSubWorkflow(
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart):
    """Subworkflow class for an abinit phonon dispersion qgrid convergence."""

    _loggername = "AbinitPhononDispersionQgridConvergenceSubWorkflow"""

    def __init__(
            self, *args,
            anaddb_input_variables=None,
            anaddb_calculation_parameters=None,
            compute_electric_field_response=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            mrgddb_calculation_parameters=None,
            phonons_qgrid_generation_input_variable_name=None,
            anaddb_equivalent_ngqpts=None,
            **kwargs,
            ) -> None:
        """Sets the phonon_dispersion_qgrid_convergence part of the workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (ddk) will be computed.
            Used to compute the LO-TO splitting.
        anaddb_input_variables: dict
            The dict of input variables for anaddb script.
        anaddb_equivalent_ngqpts: list, optional
            If 'phonons_qgrid_generation_input_variable_name' is not 'ngkpt',
            then user needs to define equivalent 'ngqpt' for anaddb for each
            qpt grid.
        anaddb_calculation_parameters: dict
            The dict of calculation parameters for the anaddb script.
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.
        phonons_qgrid_generation_input_variable_name: str
            States the input variable name (ngkpt, kptrlatt, etc.) that is used
            to generate the qgrid.

        Other kwargs are passed to the mother's class method.
        """
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart.__init__(
                self, *args, **kwargs)
        self.anaddb_calculation_parameters = anaddb_calculation_parameters
        self.anaddb_input_variables = anaddb_input_variables
        self.compute_electric_field_response = compute_electric_field_response
        self.ddk_input_variables = ddk_input_variables
        self.ddk_calculation_parameters = ddk_calculation_parameters
        self.mrgddb_calculation_parameters = mrgddb_calculation_parameters
        self.phonons_qgrid_generation_input_variable_name = (
                phonons_qgrid_generation_input_variable_name)
        self.anaddb_equivalent_ngqpts = anaddb_equivalent_ngqpts
        self.checks.update({"anaddb": False, "mrgddb": False, "ddk": False})

    def check(self, *args, **kwargs) -> None:
        """Check subworkflow is good before setting sequencer."""
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart.check(
                self, *args, **kwargs)
        self.check_ddk()
        self.check_mrgddb()
        self.check_anaddb()

    def check_phonons(self, *args, **kwargs) -> None:
        """Check phonons part of subworkflow."""
        BaseSubWorkflowWithPhononDispersionQgridConvergencePart.check_phonons(
                self, *args, set_check=False, **kwargs)
        if self.phonons_qgrid_generation_input_variable_name is None:
            raise ValueError(
                    "Need to set "
                    "'phonons_qgrid_generation_input_variable_name'.")
        self.checks["phonons"] = True

    def check_ddk(self) -> None:
        """Check ddk part of subworkflow."""
        if self.compute_electric_field_response is None:
            raise ValueError("Need to set 'compute_electric_field_response'.")
        if self.compute_electric_field_response:
            if self.ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
        self.checks["ddk"] = True

    def check_anaddb(self) -> None:
        """Check anaddb part of subworkflow."""
        if self.anaddb_input_variables is None:
            raise ValueError("Need to set 'anaddb_input_variables'.")
        if self.phonons_qgrid_generation_input_variable_name != "ngkpt":
            if self.anaddb_equivalent_ngqpts is None:
                raise ValueError(
                    "Need to define 'anaddb_equivalent_ngqpts' since "
                    "'phonons_qgrid_generation_input_variable_name' != "
                    "'ngkpt'.")
            if not is_2d_arr(
                    self.anaddb_equivalent_ngqpts,
                    size=(len(self.phonons_qpoint_grids), 3)):
                raise TypeError(
                        "'anaddb_equivalent_ngqpts' should be 2d array of "
                        "size=(ngrids, 3).")
        self.checks["anaddb"] = True

    def check_mrgddb(self) -> None:
        """Check mrgddb part of subworkflow."""
        if self.mrgddb_calculation_parameters is None:
            raise ValueError("Need to set 'mrgddb_calculation_parameters'.")
        self.checks["mrgddb"] = True

    async def set_sequencer(self, *args, **kwargs):
        """Set sequencer."""
        cls = BaseSubWorkflowWithPhononDispersionQgridConvergencePart
        await cls.set_sequencer(self, *args, **kwargs)
        for iseq, sequencer in enumerate(self.sequencer):
            root_workdir = self._get_subroot_workdir(
                    sequencer.phonons_qpoint_grid)
            # PHONONS PART
            sequencer.phonons_qgrid_generation_input_variable_name = (
                    self.phonons_qgrid_generation_input_variable_name)
            # DDK PART
            sequencer.compute_electric_field_response = (
                    self.compute_electric_field_response)
            if self.compute_electric_field_response:
                sequencer.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
                sequencer.ddk_input_variables = self.ddk_input_variables.copy()
                self._add_calculation_parameters(
                        "ddk_", self.ddk_calculation_parameters, sequencer)
            # MRGDDB PART
            sequencer.mrgddb_workdir = os.path.join(root_workdir, "mrgddb_run")
            self._add_calculation_parameters(
                    "mrgddb_", self.mrgddb_calculation_parameters, sequencer)
            # ANADDB PART
            sequencer.qpoint_path = self.qpoint_path
            sequencer.qpoint_path_density = self.qpoint_path_density
            sequencer.anaddb_workdir = os.path.join(root_workdir, "anaddb_run")
            copy = self.anaddb_input_variables.copy()
            if self.anaddb_equivalent_ngqpts is not None:
                copy["ngqpt"] = self.anaddb_equivalent_ngqpts[iseq]
            sequencer.anaddb_input_variables = copy
            self._add_calculation_parameters(
                    "anaddb_", self.anaddb_calculation_parameters, sequencer)


class AbinitPhononDispersionQgridConvergencePostProcessSubWorkflow(
        BasePhononDispersionQgridConvergencePostProcessSubWorkflow):
    """Abinit phonon dispersion qgrid convergence post processor."""

    _loggername = (
            "AbinitPhononDispersionQgridConvergencePostProcessSubWorkflow")
