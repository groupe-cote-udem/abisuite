from ..bases_subworkflows import BaseBandStructureSubWorkflow


class AbinitBandStructureSubWorkflow(BaseBandStructureSubWorkflow):
    """Subworklow class for an abinit band structure."""

    _loggername = "AbinitBandStructureSubWorkflow"

    def check_band_structure(self, *args, **kwargs) -> None:
        """Check band structure part of the subworkflow."""
        BaseBandStructureSubWorkflow.check_band_structure(
                self, *args,
                # these variables (if loaded from SCF run) are incompatible
                # for a band structure run
                pop_variables=[
                    "ngkpt", "shiftk", "nshiftk", "kptrlatt", "toldfe",
                    ],
                **kwargs)
