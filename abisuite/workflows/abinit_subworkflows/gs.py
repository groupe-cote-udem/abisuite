from ..bases_subworkflows import BaseSubWorkflowWithSCFPart


class AbinitGSSubWorkflow(BaseSubWorkflowWithSCFPart):
    """Subworkflow class for an abinit GS calculation."""

    _loggername = "AbinitGSSubWorkflow"
