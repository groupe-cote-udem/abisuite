from .gs_smearing_convergence import AbinitGSSmearingConvergenceSubWorkflow
from ..bases_subworkflows import (
        BaseSubWorkflowWithPhononSmearingConvergencePart,
        subparts_checkpoint,
        )


class AbinitPhononSmearingConvergenceSubWorkflow(
        AbinitGSSmearingConvergenceSubWorkflow,
        BaseSubWorkflowWithPhononSmearingConvergencePart,
        ):
    """Subworkflow class for phonon smearing convergence with abinit."""

    _loggername = "AbinitPhononSmearingConvergenceSubWorkflow"

    def __init__(self, *args, **kwargs) -> None:
        AbinitGSSmearingConvergenceSubWorkflow.__init__(
                self, *args,
                sequencer=kwargs.pop("sequencer", None),
                workflow=kwargs.pop("workflow", None),
                scf_kgrids_input_variable_name=kwargs.pop(
                    "scf_kgrids_input_variable_name", None),
                )
        BaseSubWorkflowWithPhononSmearingConvergencePart.__init__(
                self, *args, **kwargs)

    def check_scf_part(self, set_check: bool = True) -> None:
        """Check the scf part of subworkflow."""
        if self.checks["scf"]:
            return
        BaseSubWorkflowWithPhononSmearingConvergencePart.check_scf_part(
                self, set_check=False)
        AbinitGSSmearingConvergenceSubWorkflow.check_scf_part(
                self, set_check=False)
        if set_check:
            self.checks["scf"] = True

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(self, *args, **kwargs) -> None:
        """Set scf part of subworkflow."""
        self.sequencer.kgrids_input_variable_name = (
                self.scf_kgrids_input_variable_name)
        await BaseSubWorkflowWithPhononSmearingConvergencePart.set_scf_part(
                self, *args, **kwargs)
