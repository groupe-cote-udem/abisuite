from ..bases_subworkflows import (
        BaseSubWorkflowWithGSSmearingConvergencePart,
        subparts_checkpoint,
        )


class AbinitGSSmearingConvergenceSubWorkflow(
        BaseSubWorkflowWithGSSmearingConvergencePart,
        ):
    """Subworkflow class for an abinit gs smearing convergence."""

    _loggername = "AbinitGSSmearingConvergenceSubWorkflow"

    def __init__(
            self, *args,
            scf_kgrids_input_variable_name: str = None,
            **kwargs) -> None:
        BaseSubWorkflowWithGSSmearingConvergencePart.__init__(
                self, *args, **kwargs,
                )
        self.scf_kgrids_input_variable_name = scf_kgrids_input_variable_name

    def check_scf_part(self, set_check: bool = True) -> None:
        """Check scf part of subworkflow."""
        BaseSubWorkflowWithGSSmearingConvergencePart.check_scf_part(
                self, set_check=False)
        if self.scf_kgrids_input_variable_name is None:
            raise ValueError("Need to set 'scf_kgrids_input_variable_name'.")
        if set_check:
            self.checks["scf"] = True

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(self, *args, **kwargs) -> None:
        """Set scf part of subworkflow."""
        await BaseSubWorkflowWithGSSmearingConvergencePart.set_scf_part(
                self, *args, **kwargs)
        self.sequencer.kgrids_input_variable_name = (
                self.scf_kgrids_input_variable_name)
