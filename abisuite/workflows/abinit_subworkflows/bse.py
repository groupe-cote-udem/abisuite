import os
from typing import Any

from ..bases_subworkflows import (
        BaseSubWorkflowWithNSCFPart, subparts_checkpoint,
        )


class AbinitBSESubWorkflow(BaseSubWorkflowWithNSCFPart):
    """Subworkflow class for an abinit BSE calculation."""

    _loggername = "AbinitBSESubWorkflow"
    _have_plots = True
    _plot_name_extension = "bse"

    def __init__(
            self, *args,
            nscfnosym_input_variables: dict[str, Any] = None,
            nscfnosym_calculation_parameters: dict[str, Any] = None,
            screening_input_variables: dict[str, Any] = None,
            screening_calculation_parameters: dict[str, Any] = None,
            bse_input_variables: dict[str, Any] = None,
            bse_calculation_parameters: dict[str, Any] = None,
            use_converged_bse_kgrid: bool = False,
            use_converged_gw_screening_nband: bool = False,
            use_converged_gw_ecuteps: bool = False,
            use_converged_bse_nband: bool = False,
            use_converged_bse_ecuteps: bool = False,
            use_bse_resonant_block_from: str = None,
            use_screening_from: str = None,
            use_nscfnosym_from: str = None,
            **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.nscfnosym_input_variables = nscfnosym_input_variables
        self.nscfnosym_calculation_parameters = (
                nscfnosym_calculation_parameters)
        self.screening_input_variables = screening_input_variables
        self.screening_calculation_parameters = (
                screening_calculation_parameters)
        self.bse_input_variables = bse_input_variables
        self.bse_calculation_parameters = bse_calculation_parameters
        self.use_nscfnosym_from = use_nscfnosym_from
        self.use_screening_from = use_screening_from
        self.use_converged_gw_screening_nband = (
                use_converged_gw_screening_nband)
        self.use_converged_bse_kgrid = use_converged_bse_kgrid
        self.use_converged_gw_ecuteps = use_converged_gw_ecuteps
        self.use_converged_bse_nband = use_converged_bse_nband
        self.use_converged_bse_ecuteps = use_converged_bse_ecuteps
        self.use_bse_resonant_block_from = use_bse_resonant_block_from
        self.checks["nscfnosym"] = False
        self.checks["screening"] = False
        self.checks["bse"] = False

    async def apply_converged_quantities(self):
        """Apply converged quantitites to all subworkflow parts."""
        await BaseSubWorkflowWithNSCFPart.apply_converged_quantities(self)
        await self.apply_converged_quantities_nscfnosym()
        await self.apply_converged_quantities_screening()
        await self.apply_converged_quantities_bse()

    async def apply_converged_quantities_scf(self, *args, **kwargs):
        """Apply converged quantities for scf part."""
        await BaseSubWorkflowWithNSCFPart.apply_converged_quantities_scf(
                self, *args,
                use_converged_bse_kgrid=self.use_converged_bse_kgrid,
                **kwargs,
                )

    async def apply_converged_quantities_nscf(self, *args, **kwargs):
        """Apply converged quantities to nscf part."""
        await BaseSubWorkflowWithNSCFPart.apply_converged_quantities_nscf(
                self, *args,
                use_converged_bse_kgrid=self.use_converged_bse_kgrid,
                **kwargs,
                )

    async def apply_converged_quantities_nscfnosym(self):
        """Apply converged quantitites to nscfnosym part of subworkflow."""
        await self.use_converged_quantities(
                self.nscfnosym_input_variables,
                sequencer=self.sequencer,
                use_converged_bse_kgrid=self.use_converged_bse_kgrid,
                )

    async def apply_converged_quantities_screening(self):
        """Apply converged quantities to screening part of subworkflow."""
        # Screening tuneup
        await self.use_converged_quantities(
                self.screening_input_variables,
                sequencer=self.sequencer,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=(
                    self.use_converged_gw_screening_nband),
                use_converged_gw_ecuteps=self.use_converged_gw_ecuteps,
                )

    async def apply_converged_quantities_bse(self):
        """Apply converged quantities to bse part of subworkflow."""
        await self.use_converged_quantities(
                self.bse_input_variables,
                sequencer=self.sequencer,
                use_converged_bse_nband=self.use_converged_bse_nband,
                use_converged_bse_ecuteps=self.use_converged_bse_ecuteps,
                use_relaxed_geometry=self.use_relaxed_geometry,
                use_converged_gs_ecut=self.use_converged_gs_ecut,
                use_converged_bse_kgrid=self.use_converged_bse_kgrid,
                )

    def check(self):
        """Check that everything is ready before setup."""
        super().check()
        self.check_nscfnosym()
        self.check_screening()
        self.check_bse()

    def check_nscfnosym(self):
        """Check the nscfnosym part of subworkflow."""
        if self.use_nscfnosym_from:
            self.checks["nscfnosym"] = True
            return
        if self.nscfnosym_input_variables is None:
            raise ValueError("Need to set nscfnosym_input_variables")
        if self.nscfnosym_calculation_parameters is None:
            raise ValueError("Need to set nscfnosym_calculation_parameters")
        self.checks["nscfnosym"] = True

    def check_screening(self):
        """Check the screening part of sequencer."""
        if self.use_screening_from is not None:
            self.checks["screening"] = True
            return
        if self.screening_input_variables is None:
            raise ValueError("Need to set 'screening_input_variables'.")
        if self.screening_calculation_parameters is None:
            raise ValueError("Need to set screening_calculation_parameters.")
        self.checks["screening"] = True

    def check_bse(self):
        """Check the bse part of sequencer."""
        if self.bse_input_variables is None:
            raise ValueError("Need to set bse_input_variables.")
        if self.bse_calculation_parameters is None:
            raise ValueError("Need to set bse_calculation_parameters.")
        self.checks["bse"] = True

    async def set_sequencer(self):
        """Setup the sequencer."""
        await BaseSubWorkflowWithNSCFPart.set_sequencer(self)
        await self.set_nscfnosym_part()
        await self.set_screening_part()
        await self.set_bse_part()

    @subparts_checkpoint("scf", propagate=True)
    async def set_scf_part(self, scf_workdir: str = None) -> None:
        """Setup the scf part of the subworkflow."""
        if scf_workdir is None:
            scf_workdir = os.path.join(self.root_workdir, "scf")
        await BaseSubWorkflowWithNSCFPart.set_scf_part(
                self, scf_workdir=scf_workdir,
                )

    async def set_nscfnosym_part(self):
        """Setup nscfnosym part of sequencer."""
        await self.use_any_converged_calculation(
                self.sequencer, "nscfnosym_",
                use_from=self.use_nscfnosym_from,
                default_workdir=os.path.join(
                    self.root_workdir, "nscfnosym_run"),
                default_input_variables=self.nscfnosym_input_variables,
                default_calculation_parameters=(
                    self.nscfnosym_calculation_parameters),
                )

    async def set_screening_part(self):
        """Setup screening part of sequencer."""
        await self.use_any_converged_calculation(
                self.sequencer, "screening_",
                use_from=self.use_screening_from,
                default_workdir=os.path.join(
                    self.root_workdir, "screening_run"),
                default_input_variables=self.screening_input_variables,
                default_calculation_parameters=(
                    self.screening_calculation_parameters),
                )

    async def set_bse_part(self):
        """Setup bse part of sequencer."""
        await self.use_any_converged_calculation(
                self.sequencer, "bse_",
                use_from=None,
                default_workdir=os.path.join(
                    self.root_workdir, "bse_run"),
                default_input_variables=self.bse_input_variables,
                default_calculation_parameters=self.bse_calculation_parameters,
                )
        if self.use_bse_resonant_block_from is not None:
            self.sequencer.link_bs_reso_from = self.use_bse_resonant_block_from
