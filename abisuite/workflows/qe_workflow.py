import asyncio
import os
from typing import Any, Mapping, Sequence

import aiofiles.os

import numpy as np

from .bases import BaseWorkflow, SequencersList
from .qe_subworkflows import (
        QEBandStructureSubWorkflow,
        QEDOSKgridConvergencePostProcessSubWorkflow,
        QEDOSKgridConvergenceSubWorkflow,
        QEEPWWannierizationSubWorkflow,
        QEEPWWannierizationWithThermalExpansionSubWorkflow,
        QEGSSubWorkflow,
        QEIBTECoarseKgridConvergencePostProcessSubWorkflow,
        QEIBTECoarseKgridConvergenceSubWorkflow,
        QEIBTECoarseQgridConvergencePostProcessSubWorkflow,
        QEIBTECoarseQgridConvergenceSubWorkflow,
        QEIBTEFineGridConvergencePostProcessSubWorkflow,
        QEIBTEFineGridConvergenceSubWorkflow,
        QEPhononDispersionQgridConvergencePostProcessSubWorkflow,
        QEPhononDispersionQgridConvergenceSubWorkflow,
        QEPhononDispersionSubWorkflow,
        QEPhononSmearingConvergenceSubWorkflow,
        )
from ..handlers import CalculationDirectory
from ..launchers.launchers.qe_launchers.pw_launcher import (
        __ALL_QE_GEOMETRY_VARIABLES__,
        )
from ..plotters import MultiPlot, Plot
from ..post_processors import ConductivityTensor
from ..routines import is_list_like
from ..sequencers import (
        QEBandStructureSequencer,
        QEDOSSequencer,
        QEEPWIBTESequencer,
        QEEPWWithPhononInterpolationSequencer,
        QEEPWZimanSequencer,
        QEEcutConvergenceSequencer,
        QEEcutPhononConvergenceSequencer,
        QEFermiSurfaceSequencer,
        QEKgridConvergenceSequencer, QEKgridPhononConvergenceSequencer,
        QEPhononDispersionSequencer,
        QERelaxationSequencer, QESCFSequencer, QESmearingConvergenceSequencer,
        QESmearingPhononConvergenceSequencer,
        QEThermalExpansionSequencer,
        )


class QEWorkflow(BaseWorkflow):
    """Workflow for Quantum Espresso.

    For now supports:
        - :meth:`gs_ecut_convergence<.BaseWorkflow.set_gs_ecut_convergence>`
        - :meth:`gs_kgrid_convergence<.BaseWorkflow.set_gs_kgrid_convergence>`
        - :meth:`gs_smearing_convergence<.BaseWorkflow.set_gs_smearing_convergence>`
        - :meth:`relaxation<.set_relaxation>`
        - :meth:`phonon_ecut_convergence<.BaseWorkflow.set_phonon_ecut_convergence>`
        - :meth:`phonon_kgrid_convergence<.set_phonon_kgrid_convergence>`
        - :meth:`phonon_smearing_convergence<.BaseWorkflow.set_phonon_smearing_convergence>`
        - :meth:`gs<.set_gs>`
        - :meth:`band_structure<.BaseWorkflow.set_band_structure>`
        - :meth:`dos_kgrid_convergence<.set_dos_kgrid_convergence>`
        - :meth:`dos<.set_dos>`
        - :meth:`fermi_surface<.set_fermi_surface>`
        - :meth:`phonon_dispersion_qgrid_convergence<.set_phonon_dispersion_qgrid_convergence>`
        - :meth:`phonon_dispersion<.set_phonon_dispersion>`
        - :meth:`lattice_expansion<.set_lattice_expansion>`
        - :meth:`epw_wannierization<.set_epw_wannierization>`
        - :meth:`ibte_fsthick_convergence<.set_ibte_fsthick_convergence>`
        - :meth:`ibte_coarse_kgrid_convergence<.set_ibte_coarse_kgrid_convergence>`
        - :meth:`ibte_coarse_qgrid_convergence<.set_ibte_coarse_qgrid_convergence>`
        - :meth:`ibte_fine_grid_convergence<.set_ibte_fine_grid_convergence>`
        - :meth:`ibte<.set_ibte>`
        - :meth:`epw_wannierization_with_thermal_expansion<.set_epw_wannierization_with_thermal_expansion>`
        - :meth:`ibte_with_thermal_expansion<.set_ibte_with_thermal_expansion>`
        - :meth:`ziman<.set_ziman>`
    """  # noqa: 501

    _all_geometry_variables = __ALL_QE_GEOMETRY_VARIABLES__
    _band_structure_sequencer_cls = QEBandStructureSequencer
    _band_structure_subworkflow_cls = QEBandStructureSubWorkflow
    _dos_sequencer_cls = QEDOSSequencer
    _fermi_surface_sequencer_cls = QEFermiSurfaceSequencer
    _loggername = "QEWorkflow"
    _gs_sequencer_cls = QESCFSequencer
    _gs_subworkflow_cls = QEGSSubWorkflow
    _gs_ecut_convergence_sequencer_cls = QEEcutConvergenceSequencer
    _gs_kgrid_convergence_sequencer_cls = QEKgridConvergenceSequencer
    _gs_smearing_convergence_sequencer_cls = QESmearingConvergenceSequencer
    _phonon_ecut_convergence_sequencer_cls = QEEcutPhononConvergenceSequencer
    _phonon_kgrid_convergence_sequencer_cls = QEKgridPhononConvergenceSequencer
    _phonon_dispersion_sequencer_cls = QEPhononDispersionSequencer
    _phonon_dispersion_qgrid_convergence_subworkflow_cls = (
            QEPhononDispersionQgridConvergenceSubWorkflow)
    _phonon_dispersion_qgrid_convergence_post_processor_cls = (
            QEPhononDispersionQgridConvergencePostProcessSubWorkflow)
    _phonon_smearing_convergence_sequencer_cls = (
            QESmearingPhononConvergenceSequencer)
    _phonon_smearing_convergence_subworkflow_cls = (
            QEPhononSmearingConvergenceSubWorkflow)
    _relaxation_sequencer_cls = QERelaxationSequencer
    workflow = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence", "phonon_kgrid_convergence",
            "phonon_smearing_convergence", "gs",
            "band_structure", "dos_kgrid_convergence", "dos", "fermi_surface",
            "phonon_dispersion_qgrid_convergence", "phonon_dispersion",
            "lattice_expansion", "epw_wannierization",
            "ibte_fsthick_convergence", "ibte_coarse_kgrid_convergence",
            "ibte_coarse_qgrid_convergence",
            "ibte_fine_grid_convergence", "ibte",
            "epw_wannierization_with_thermal_expansion",
            "ibte_with_thermal_expansion", "ziman"
            )

    def __init__(
            self,
            epw_wannierization: bool = False,
            epw_wannierization_with_thermal_expansion: bool = False,
            dos_kgrid_convergence: bool = False,
            ibte_fsthick_convergence: bool = False,
            converged_ibte_fsthick: float = None,
            ibte_coarse_kgrid_convergence: bool = False,
            converged_ibte_coarse_kgrid: Sequence[int] = None,
            ibte_coarse_qgrid_convergence: bool = False,
            converged_ibte_coarse_qgrid: Sequence[int] = None,
            ibte_fine_grid_convergence: dict[str, int] = False,
            converged_ibte_fine_grid: bool = None,
            ibte: bool = False,
            ibte_with_thermal_expansion: bool = False,
            lattice_expansion: bool = False,
            ziman: bool = False,
            *args, **kwargs):
        """QE Workflow init method.

        Parameters
        ----------
        dos_kgrid_convergence: bool, optional
            If True, the dos nscf kgrid convergence will be executed.
        epw_wannierization: bool, optional
            If True, the Wannier function interpolation with the EPW module
            will be executed.
        epw_wannierization_with_thermal_expansion: bool, optional
            If True, the interpolations using wanner function will be done
        ibte: bool, optional
            If True, IBTE transport calculation is executed.
            'epw_wannierization' must be set to True as well.
        ibte_fsthick_convergence: bool, optional
            If True, IBTE transport calculation is converged on the 'fsthick'
            parameter. 'epw_wannierization' must be set to True as well.
        converged_ibte_fsthick: float, optional
            The converged fsthick value. This variable is stored in case other
            parts of the workflow use it.
        ibte_coarse_kgrid_convergence: bool, optional
            If True, IBTE transport calculation is converged on the
            'coarse kgrid' parameters.
        converged_ibte_coarse_kgrid: list, optional
            The converged coarse kgrid values.
            These variable are stored in case other
            parts of the workflow use it.
        ibte_coarse_qgrid_convergence: bool, optional
            If True, IBTE transport calculation is converged on the
            'coarse qgrid' parameters.
        converged_ibte_coarse_qgrid: list, optional
            The converged coarse qgrid values.
            These variable are stored in case other
            parts of the workflow use it.
        ibte_fine_grid_convergence: bool, optional
            If True, IBTE transport calculation is converged on the 'fine grid'
            parameters. 'epw_wannierization' must be set to True as well.
        converged_ibte_fine_grid: dict, optional
            The converged fine grids values.
            These variable are stored in case other
            parts of the workflow use it.
        ibte_with_thermal_expansion: bool, optional
            If True, IBTE transport data is computed considering thermal
            expansion as well. 'epw_wannierization_with_thermal_expansion'
            must be set to True as well.
        lattice_expansion: bool, optional
            If True, the lattice expansion is computed.
            considering lattice expansion.
        ziman: bool, optional
            If True, the ziman resistivity formula is computed from epw
            calculation. 'epw_wannierization' must be set to True as well.
        """
        super().__init__(*args, **kwargs)
        # DOS KGRID CONVERGENCE PART
        self._compute_dos_kgrid_convergence = dos_kgrid_convergence
        self._dos_kgrid_convergence_sequencer = None
        # LATTICE THERMAL EXPANSION PART
        self._compute_lattice_expansion = lattice_expansion
        self._lattice_expansion_sequencer = None
        # EPW WANNIERIZATION PART
        self._compute_epw_wannierization = epw_wannierization
        self._epw_wannierization_sequencer = None
        # EPW INTERPOLATION WITH THERMAL EXPANSION PART
        eiwte = epw_wannierization_with_thermal_expansion
        self._compute_epw_wannierization_with_thermal_expansion = eiwte
        self._epw_wannierization_with_thermal_expansion_sequencer = None
        # IBTE FSTHICK CONVERGENCE PART
        self._compute_ibte_fsthick_convergence = ibte_fsthick_convergence
        self._ibte_fsthick_convergence_sequencer = None
        self.converged_ibte_fsthick = converged_ibte_fsthick
        # IBTE COARSE KGRID CONVERGENCE PART
        self._compute_ibte_coarse_kgrid_convergence = (
                ibte_coarse_kgrid_convergence)
        self.converged_ibte_coarse_kgrid = converged_ibte_coarse_kgrid
        self._ibte_coarse_kgrid_convergence_sequencer = None
        # IBTE COARSE QGRID CONVERGENCE PART
        self._compute_ibte_coarse_qgrid_convergence = (
                ibte_coarse_qgrid_convergence)
        self.converged_ibte_coarse_qgrid = converged_ibte_coarse_qgrid
        self._ibte_coarse_qgrid_convergence_sequencer = None
        # IBTE FINE GRID CONVERGENCE PART
        self._compute_ibte_fine_grid_convergence = ibte_fine_grid_convergence
        self._ibte_fine_grid_convergence_sequencer = None
        self.converged_ibte_fine_grid = converged_ibte_fine_grid
        # IBTE PART
        self._compute_ibte = ibte
        self._ibte_sequencer = None
        # IBTE WITH THERMAL EXPANSION PART
        self._compute_ibte_with_thermal_expansion = ibte_with_thermal_expansion
        self._ibte_with_thermal_expansion_sequencer = None
        # ZIMAN PART
        self._compute_ziman = ziman
        self._ziman_sequencer = None

    @property
    def dos_kgrid_convergence_sequencer(self) -> SequencersList:
        """The list of dos sequencers for the kgrid convergence."""
        if self._dos_kgrid_convergence_sequencer is not None:
            return self._dos_kgrid_convergence_sequencer
        self._dos_kgrid_convergence_sequencer = SequencersList([])
        return self._dos_kgrid_convergence_sequencer

    @property
    def epw_wannierization_sequencer(self):
        """Return the epw wannierization sequencer."""
        if self._epw_wannierization_sequencer is not None:
            return self._epw_wannierization_sequencer
        self._epw_wannierization_sequencer = (
                QEEPWWithPhononInterpolationSequencer(loglevel=self._loglevel))
        return self._epw_wannierization_sequencer

    @property
    def epw_wannierization_with_thermal_expansion_sequencer(self):
        """Return the epw wannierization with thermal expansion sequencer."""
        _ = self._epw_wannierization_with_thermal_expansion_sequencer
        if _ is not None:
            return self._epw_wannierization_with_thermal_expansion_sequencer
        self._epw_wannierization_with_thermal_expansion_sequencer = (
                SequencersList([]))
        if not self._compute_lattice_expansion:
            # sequencers will be set by subworkflow
            return self._epw_wannierization_with_thermal_expansion_sequencer
        if self._lattice_expansion_sequencer is None:
            raise RuntimeError(
                    "Please set lattice_expansion calculation first.")
        for _ in self.lattice_expansion_sequencer.temperatures:
            self._epw_wannierization_with_thermal_expansion_sequencer.append(
                QEEPWWithPhononInterpolationSequencer(loglevel=self._loglevel)
                )
        return self.epw_wannierization_with_thermal_expansion_sequencer

    @property
    def ibte_sequencer(self):
        """Return the IBTE sequencer."""
        if self._ibte_sequencer is not None:
            return self._ibte_sequencer
        if not self._compute_epw_wannierization:
            raise ValueError(
                    "Need to set 'epw_wannierization' to True upon init.")
        self._ibte_sequencer = QEEPWIBTESequencer(loglevel=self._loglevel)
        return self._ibte_sequencer

    @property
    def ibte_fsthick_convergence_sequencer(self):
        """Return the ibte fsthick convergence sequencer."""
        if self._ibte_fsthick_convergence_sequencer is not None:
            return self._ibte_fsthick_convergence_sequencer
        self._ibte_fsthick_convergence_sequencer = SequencersList(
                [], loglevel=self._loglevel)
        return self.ibte_fsthick_convergence_sequencer

    @property
    def ibte_coarse_kgrid_convergence_sequencer(self) -> SequencersList:
        """The ibte coarse kgrid convergence sequencer."""
        if self._ibte_coarse_kgrid_convergence_sequencer is not None:
            return self._ibte_coarse_kgrid_convergence_sequencer
        self._ibte_coarse_kgrid_convergence_sequencer = SequencersList(
                [], loglevel=self._loglevel)
        return self._ibte_coarse_kgrid_convergence_sequencer

    @property
    def ibte_coarse_qgrid_convergence_sequencer(self) -> SequencersList:
        """The ibte coarse qgrid convergence sequencer."""
        if self._ibte_coarse_qgrid_convergence_sequencer is not None:
            return self._ibte_coarse_qgrid_convergence_sequencer
        self._ibte_coarse_qgrid_convergence_sequencer = SequencersList(
                [], loglevel=self._loglevel)
        return self._ibte_coarse_qgrid_convergence_sequencer

    @property
    def ibte_fine_grid_convergence_sequencer(self):
        """Return the ibte fine grid convergence sequencer."""
        if self._ibte_fine_grid_convergence_sequencer is not None:
            return self._ibte_fine_grid_convergence_sequencer
        self._ibte_fine_grid_convergence_sequencer = SequencersList(
                [], loglevel=self._loglevel)
        return self.ibte_fine_grid_convergence_sequencer

    @property
    def ibte_with_thermal_expansion_sequencer(self):
        """Return the ibte with thermal expansion sequencer."""
        if self._ibte_with_thermal_expansion_sequencer is not None:
            return self._ibte_with_thermal_expansion_sequencer
        if self._epw_wannierization_with_thermal_expansion_sequencer is None:
            raise RuntimeError(
                    "Please set epw with thermal expansion calculations first."
                    )
        ncalcs = len(self.epw_wannierization_with_thermal_expansion_sequencer)
        self._ibte_with_thermal_expansion_sequencer = SequencersList([
                QEEPWIBTESequencer(loglevel=self._loglevel)
                for i in range(ncalcs)])
        return self.ibte_with_thermal_expansion_sequencer

    @property
    def lattice_expansion_sequencer(self):
        """Return the lattice thermal expansion sequencer."""
        if self._lattice_expansion_sequencer is not None:
            return self._lattice_expansion_sequencer
        self._lattice_expansion_sequencer = QEThermalExpansionSequencer(
                loglevel=self._loglevel)
        return self.lattice_expansion_sequencer

    @property
    def ziman_sequencer(self):
        """Return the Ziman sequencer."""
        if self._ziman_sequencer is not None:
            return self._ziman_sequencer
        self._ziman_sequencer = QEEPWZimanSequencer(loglevel=self._loglevel)
        return self.ziman_sequencer

    def clean_dos_kgrid_convergence(self):
        """Clean the 'dos_kgrid_convergence' part of the workflow."""
        self.dos_kgrid_convergence_sequencer.clean()

    def clean_epw_wannierization(self):
        """Clean the 'epw_wannierization' part of the workflow."""
        self.epw_wannierization_sequencer.clean()

    def clean_epw_wannierization_with_thermal_expansion(self):
        """Clean the epw with thermal expansion part of the workflow."""
        self.epw_wannierization_with_thermal_expansion_sequencer.clean()

    def clean_ibte(self):
        """Clean the 'ibte' part of the workflow."""
        self.ibte_sequencer.clean()

    def clean_ibte_fsthick_convergence(self):
        """Clean the 'ibte_fsthick_convergence' part of the workflow."""
        self.ibte_fsthick_convergence_sequencer.clean()

    def clean_ibte_coarse_kgrid_convergence(self):
        """Clean the 'ibte_coarse_kgrid_convergence' part of the workflow."""
        self.ibte_coarse_kgrid_convergence_sequencer.clean()

    def clean_ibte_coarse_qgrid_convergence(self):
        """Clean the 'ibte_coarse_qgrid_convergence' part of the workflow."""
        self.ibte_coarse_qgrid_convergence_sequencer.clean()

    def clean_ibte_fine_grid_convergence(self):
        """Clean the 'ibte_fine_grid_convergence' part of the workflow."""
        self.ibte_fine_grid_convergence_sequencer.clean()

    def clean_ibte_with_thermal_expansion(self):
        """Clean the 'ibte_with_thermal_expansion' part of the workflow."""
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        seqs.clean()

    def clean_lattice_expansion(self):
        """Clean the 'lattice_expansion' part of the workflow."""
        self.lattice_expansion_sequencer.clean()

    def clean_ziman(self):
        """Clean the 'ziman' part of the workflow."""
        self.ziman_sequencer.clean()

    async def run_dos_kgrid_convergence(self):
        """Run the 'dos_kgrid_convergence' part of the workflow."""
        await self.dos_kgrid_convergence_sequencer.run()
        if not await self.dos_kgrid_convergence_sequencer.sequence_completed:
            return
        post_processor = QEDOSKgridConvergencePostProcessSubWorkflow(
                sequencer=self.dos_kgrid_convergence_sequencer,
                workflow=self,
                loglevel=self._loglevel)
        await post_processor.make_dos_kgrid_convergence_plots()

    async def run_epw_wannierization(self):
        """Run the epw wannierization part of the workflow."""
        await self.epw_wannierization_sequencer.run()

    async def run_epw_wannierization_with_thermal_expansion(self):
        """Run the epw interpolation with thermal exp part of the workflow."""
        if self._compute_lattice_expansion:
            if not await self.lattice_expansion_sequencer.sequence_completed:
                self._logger.info(
                        "Lattice expansion sequencer not completed. "
                        "=> EPW wannierization not run.")
                return
            self._logger.info(
                    "Thermal lattice expansion sequence completed. "
                    "Now running epw with thermal "
                    "lattice expansion sequences.")
        await self.epw_wannierization_with_thermal_expansion_sequencer.run()
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        me = self._plot_final_results_epw_wannierization_with_thermal_expansion
        if await seqs.sequence_completed:
            try:
                await me()
            except Exception as err:
                self._logger.error(
                        "An error occured while plotting final epw "
                        "wannierization with thermal expansion results")
                self._logger.exception(err)

    async def run_ibte(self):
        """Run the IBTE part of the workflow."""
        await self.ibte_sequencer.run()

    async def run_ibte_fsthick_convergence(self):
        """Run the 'ibte_fsthick_convergence' part of the workflow."""
        await self.ibte_fsthick_convergence_sequencer.run()
        if not await (
                self.ibte_fsthick_convergence_sequencer.sequence_completed):
            return
        # make final resistivity plots
        await self._post_process_ibte_fsthick_convergence()

    async def run_ibte_coarse_kgrid_convergence(self):
        """Run the 'ibte_coarse_kgrid_convergence' part of the workflow."""
        await self._run_ibte_coarse_grid_convergence(
                postprocessor_cls=(
                    QEIBTECoarseKgridConvergencePostProcessSubWorkflow),
                sequencer=self.ibte_coarse_kgrid_convergence_sequencer,
                )

    async def run_ibte_coarse_qgrid_convergence(self):
        """Run the 'ibte_coarse_qgrid_convergence' part of the workflow."""
        await self._run_ibte_coarse_grid_convergence(
                postprocessor_cls=(
                    QEIBTECoarseQgridConvergencePostProcessSubWorkflow),
                sequencer=self.ibte_coarse_qgrid_convergence_sequencer,
                )

    async def _run_ibte_coarse_grid_convergence(
            self, postprocessor_cls, sequencer) -> None:
        all_epw_wann_seqs = []
        all_ibte_seqs = []
        for seq in sequencer:
            if isinstance(seq, QEEPWIBTESequencer):
                all_ibte_seqs.append(seq)
                continue
            all_epw_wann_seqs.append(seq)
        epw_wann_seqs = SequencersList(
                all_epw_wann_seqs, loglevel=self._loglevel)
        await epw_wann_seqs.run()
        ibte_seqs = SequencersList(
                all_ibte_seqs, loglevel=self._loglevel)
        for ibte_seq in ibte_seqs:
            try:
                calc = ibte_seq.epw_wannierization_calculation
            except ValueError:
                # not set already
                continue
            for epw_wann_seq in epw_wann_seqs:
                # use samefile in case one is a symlink and not the other
                if not os.path.samefile(calc, epw_wann_seq.epw_workdir):
                    continue
                if await epw_wann_seq.sequence_completed:
                    # epw seq completed -> run ibte one
                    await ibte_seq.run()
                    break
            else:
                raise LookupError(
                    f"tried to launch ibte seq {ibte_seq.ibte_workdir} "
                    "but couldn't find corresponding epw one...")
        # post process using post_process_subworkflow
        postprocessor = postprocessor_cls(
                sequencer=sequencer,
                workflow=self,
                loglevel=self._loglevel,
                )
        if await epw_wann_seqs.sequence_completed:
            self._logger.info(
                "All ibte coarse grid EPW wannierization runs completed.")
            await postprocessor.make_decay_plots()
        if not await sequencer.sequence_completed:
            self._logger.info(
                    "IBTE runs not completed. Do not make ibte plots.")
            return
        await postprocessor.make_ibte_plots()

    async def run_ibte_fine_grid_convergence(self):
        """Run the 'ibte_fine_grid_convergence' part of the workflow."""
        sequencer = self.ibte_fine_grid_convergence_sequencer
        await sequencer.run()
        if not await sequencer.sequence_completed:
            return
        # make final resistivity plots
        postprocessor = QEIBTEFineGridConvergencePostProcessSubWorkflow(
                sequencer=sequencer,
                workflow=self,
                loglevel=self._loglevel,
                )
        await postprocessor.make_decay_plots()
        await postprocessor.make_ibte_plots()

    async def run_ibte_with_thermal_expansion(self):
        """Run the ibte with thermal expansion part of the workflow."""
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        if not await seqs.sequence_completed:
            return
        for seq in self.ibte_with_thermal_expansion_sequencer:
            # don't show plot (only 1 point plots are uninteresting)
            show = seq.plot_calculation_parameters.pop("show", True)
            seq.plot_show = False
            await seq.run()
        if await self.ibte_with_thermal_expansion_sequencer.sequence_completed:
            self._logger.info("Plotting final IBTE results.")
            await self._plot_final_results_ibte_with_thermal_expansion(show)
        else:
            self._logger.info(
                    "Waiting for IBTE sequence to complete to plot final "
                    "results.")

    async def run_lattice_expansion(self):
        """Run the lattice expansion part of the workflow."""
        deltas = self.lattice_expansion_sequencer.deltas_volumes
        zero = 0 in deltas or 0.0 in deltas
        if self._compute_gs and zero:
            if not await self.gs_sequencer.sequence_completed:
                # wait for gs
                return
        await self.lattice_expansion_sequencer.run()

    async def run_ziman(self):
        """Run the 'ziman' part of the workflow."""
        await self.ziman_sequencer.run()

    async def set_dos_kgrid_convergence(self, *args, **kwargs) -> None:
        """Sets the dos_kgrid_convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        dos_fine_kpoint_grids_variables: list-like
            The list of kpoint grid variables dicts to look at.
        dos_input_variables: dict
            The dict of the dos input variables.
        dos_calculation_parameters: dict
            The dict of the calculation parameters for the dos calculation.
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf part of the sequence.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used instead of
            running a new scf calculation. This option is incompatible
            with the other 'use_*' flags and will thus override them.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_phonon_converged_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_dos_kgrid_convergence:
            return
        subworkflow = QEDOSKgridConvergenceSubWorkflow(
                sequencer=(
                    self.dos_kgrid_convergence_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_dos(
            self, root_workdir: str = None,
            nscf_calculation_parameters: dict[str, Any] = None,
            **kwargs,
            ) -> None:
        """Sets the dos part of the workflow.

        Parameters
        ----------
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf part of the sequence.
        """
        await super().set_dos(root_workdir=root_workdir, **kwargs)
        self.dos_sequencer.nscf_workdir = os.path.join(
                root_workdir, "nscf_run",
                )
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, self.dos_sequencer)

    async def set_epw_wannierization(self, **kwargs):
        """Set the epw wannierization part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the whole sequence.
        asr: str, optional
            Needed for the matdyn calculations to get the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        scf_input_variables: dict, optional
            The dict of scf input variables. Ignored if 'use_gs' is True.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters. Ignored
            if 'use_gs' is True.
        nscf_kgrid: list-like
            The kgrid for the nscf calculation.
        nscf_input_variables: dict, optional
            The input variables for the NSCF calculations.
            If 'use_gs' is True, these variables will update the variables
            from the gs run. If None, the gs run input variables are used.
            If None but 'use_gs' is False, an error is thrown.
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like, optional
            The kpoint path for the band structure calculation.
            Ignored if 'use_band_structure' is True.
        band_structure_kpoint_path_density: int, optional
            The number of points between points in the band structure.
            Ignored if 'use_band_structure' is True.
        band_structure_input_variables: dict, optional
            The dict of the band structures input variables.
            Ignored if 'use_band_structure' is True.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
            Ignored if 'use_band_structure' is True.
        phonons_input_variables: dict, optional
            The phonons input variables.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_calculation_parameters: dict, optional
            The calculation parameters for the phonons calculations.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_qpoint_grid: list, optional
            The phonon qpoint grid.
            Ignored if 'use_phonon_dispersion' is True.
        q2r_calculation_parameters: dict, optional
            The q2r calculation parameters.
            Ignored if 'use_phonon_dispersion' is True.
        matdyn_calculation_parameters: dict, optional
            The matdyn calculation parmeters.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_calculation_parameters: dict, optional
            Plot parameters for the interpolations.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf calculation
            for this sequence.
        use_converged_gs_ecut: bool, optional
            If True and 'use_gs' is not, the gs converged ecut is used for the
            scf calculation.
        use_converged_gs_kgrid: bool, optional
            If True and 'use_gs' is not, the gs converged kgrid is used for the
            scf calculation.
        use_converged_gs_smearing: bool, optional
            If True and 'use_gs' is not, the gs converged smearing
            is used for the scf calculation.
        use_phonon_converged_ecut: bool, optional
            If True and 'use_gs' is not, the phonon converged ecut
            is used for the scf calculation.
        use_phonon_converged_smearing: bool, optional
            If True and 'use_gs' is not, the phonon converged smearing
            is used for the scf calculation.
        use_band_structure: bool, optional
            If True, the band structure part of the workflow is used as the
            band structure part of the sequence.
        use_phonon_dispersion: bool, optional
            If True, the phonon dispersion part of the workflow is used as
            the phonon + q2r + matdyn part of the sequence.
        """
        if not self._compute_epw_wannierization:
            return
        subworkflow = QEEPWWannierizationSubWorkflow(
                sequencer=(
                    self.epw_wannierization_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_epw_wannierization_with_thermal_expansion(
            self, **kwargs):
        """Set the epw wannierization with thermal exp part of the workflow.

        The SCF input variables are taken from the Thermal Expansion Sequencer.
        The temperatures are taken from this sequencer as well.
        Same as the phonons, q2r and matdyn input variables.

        Parameters
        ----------
        root_workdir: str
            The root workdir for these Sequencers.
        lattice_parameters: list, optional
            Gives the list of lattice parameters to use for the lattice
            expansion. If None, the ones from the lattice expansion
            calculation are taken. This computation option must thus
            be set to True otherwise an error is thrown.
        temperatures: list, optional
            Gives the temperatures corresponding to the lattice parameters.
            If None, they are taken from the lattice expansion calculations.
        asr: str
            The acoustic sum rule to impose (matdyn input variables).
        scf_input_variables: dict
            The scf input variables.
        scf_calculation_parameters: dict, optional
            If not None, sets the calculation parameters for the scf
            calculations.
        nscf_kgrid: list-like
            The kgrid for the nscf calculation.
        nscf_input_variables: dict
            The input variables for the NSCF calculations.
        nscf_calculation_parameters: dict, optional
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like
            The kpoint path for the band structure calculation.
        band_structure_kpoint_path_density: int
            The number of points between points in the band structure.
        band_structure_input_variables: dict
            If not None, states the input variables for the band structure
            run.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
        phonons_input_variables: dict
            The phonons input variables.
        phonons_calculation_parameters: dict
            The calculation parameters for the phonons calculations.
        phonons_qpoint_grid: list, optional
            The phonon qpoint grid.
        q2r_input_variables: dict
            The dict of q2r input variables.
        q2r_calculation_parameters: dict
            The q2r calculation parameters.
        matdyn_calculation_parameters: dict
            The matdyn calculation parmeters.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_calculation_parameters: dict, optional
            Plot parameters for the interpolations.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut is used for the scf part,
            the nscf part and the band structure part as well.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing is used for the scf part,
            the nscf part and the band structure part.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry is used as a starting point
            to define the unit cell of the system. The 'celldm(1)' parameter
            will be adjusted by the Workflow object so don't need to worry
            about it.
            This parameter is used for the scf, the nscf and the band structure
            part of the workflow.
        """
        if not self._compute_epw_wannierization_with_thermal_expansion:
            self._logger.info(
                "Not setting epw wannierization with thermal expansion as it "
                "is set to False upon init.")
            return
        subworkflow = QEEPWWannierizationWithThermalExpansionSubWorkflow(
                sequencer=(
                    self.epw_wannierization_with_thermal_expansion_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        await subworkflow.load_lattice_parameters()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()
        else:
            self._logger.info(
                    "EPW wannierization with thermal expansion is not ready"
                    " to be set.")

    async def set_ibte(self, *args, **kwargs):
        """Set the ibte calculation.

        Parameters
        ----------
        root_workdir: str
            The root directory where the ibte calculation will run and results
            will be stored.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE run.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE run.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        use_ibte_converged_fsthick: bool, optional
            If True, will use the converged fsthick value for the IBTE part.
        """
        if not self._compute_ibte:
            return
        from .qe_subworkflows import QEIBTESubWorkflow
        subworkflow = QEIBTESubWorkflow(
                *args,
                sequencer=self.ibte_sequencer,
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_ibte_fsthick_convergence(
            self, root_workdir=None,
            ibte_input_variables=None,
            ibte_calculation_parameters=None,
            ibte_fsthicks=None,
            plot_calculation_parameters=None,
            ):
        """Set the 'ibte_fsthick_convergence' part of the workflow.

        Parameters
        ----------
        ibte_workdir: str
            The root directory where the ibte calculation will run and results
            will be stored.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE run.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE run.
        ibte_fsthicks: list-like
            The list of fsthicks to test.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        """
        if not self._compute_ibte_fsthick_convergence:
            return
        if not self.epw_wannierization_sequencer.sequence_completed:
            self.stop_at_workflow = "epw_wannierization"
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        if ibte_fsthicks is None:
            raise ValueError("Need to set 'ibte_fsthicks'.")
        if not is_list_like(ibte_fsthicks):
            raise TypeError("'ibte_fsthicks' must be list-like.")
        if ibte_input_variables is None:
            raise ValueError("Need to set 'ibte_input_variables'.")
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        await asyncio.gather(
                *(self._add_ibte_sequencer_for_fsthick_convergence(
                    fsthick, root_workdir, ibte_input_variables,
                    ibte_calculation_parameters, plot_calculation_parameters)
                    for fsthick in ibte_fsthicks),
                )

    async def _add_ibte_sequencer_for_fsthick_convergence(
            self, fsthick, root_workdir, ibte_input_variables,
            ibte_calculation_parameters, plot_calculation_parameters):
        sequencer = QEEPWIBTESequencer(loglevel=self._loglevel)
        sequencer.ibte_workdir = os.path.join(
                root_workdir, f"fsthick{fsthick}", "ibte_run")
        await sequencer.set_epw_wannierization_calculation(
            self.epw_wannierization_sequencer.epw_workdir)
        ivars = ibte_input_variables.copy()
        ivars.update({"fsthick": fsthick})
        sequencer.ibte_input_variables = ivars
        self._add_calculation_parameters(
                "ibte_", ibte_calculation_parameters, sequencer)
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, f"fsthick{fsthick}", "results", "ibte.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, f"fsthick{fsthick}", "results",
                "ibte.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)
        self.ibte_fsthick_convergence_sequencer.append(sequencer)

    async def set_ibte_coarse_kgrid_convergence(self, *args, **kwargs) -> None:
        """Set the ibte coarse kgrid convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the whole sequence.
        asr: str, optional
            Needed for the matdyn calculations to get the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        scf_input_variables: dict, optional
            The dict of scf input variables. Ignored if 'use_gs' is True.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters. Ignored
            if 'use_gs' is True.
        nscf_kgrids: list-like
            The coarse kgrids to test for the nscf calculations.
        nscf_input_variables: dict, optional
            The input variables for the NSCF calculations.
            If 'use_gs' is True, these variables will update the variables
            from the gs run. If None, the gs run input variables are used.
            If None but 'use_gs' is False, an error is thrown.
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like, optional
            The kpoint path for the band structure calculation.
            Ignored if 'use_band_structure' is True.
        band_structure_kpoint_path_density: int, optional
            The number of points between points in the band structure.
            Ignored if 'use_band_structure' is True.
        band_structure_input_variables: dict, optional
            The dict of the band structures input variables.
            Ignored if 'use_band_structure' is True.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
            Ignored if 'use_band_structure' is True.
        phonons_input_variables: dict, optional
            The phonons input variables.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_calculation_parameters: dict, optional
            The calculation parameters for the phonons calculations.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_qpoint_grid: list, optional
            The phonon qpoint grid.
            Ignored if 'use_phonon_dispersion' is True.
        q2r_calculation_parameters: dict, optional
            The q2r calculation parameters.
            Ignored if 'use_phonon_dispersion' is True.
        matdyn_calculation_parameters: dict, optional
            The matdyn calculation parmeters.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_calculation_parameters: dict, optional
            Plot parameters for the interpolations.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf calculation
            for this sequence.
        use_converged_gs_ecut: bool, optional
            If True and 'use_gs' is not, the gs converged ecut is used for the
            scf calculation.
        use_converged_gs_kgrid: bool, optional
            If True and 'use_gs' is not, the gs converged kgrid is used for the
            scf calculation.
        use_converged_gs_smearing: bool, optional
            If True and 'use_gs' is not, the gs converged smearing
            is used for the scf calculation.
        use_converged_phonon_ecut: bool, optional
            If True and 'use_gs' is not, the phonon converged ecut
            is used for the scf calculation.
        use_converged_phonon_smearing: bool, optional
            If True and 'use_gs' is not, the phonon converged smearing
            is used for the scf calculation.
        use_band_structure: bool, optional
            If True, the band structure part of the workflow is used as the
            band structure part of the sequence.
        use_phonon_dispersion: bool, optional
            If True, the phonon dispersion part of the workflow is used as
            the phonon + q2r + matdyn part of the sequence.
        use_ibte_converged_fsthick: bool, optional
            If True, will use the converged fsthick value for the IBTE part.
        """
        if not self._compute_ibte_coarse_kgrid_convergence:
            return
        subworkflow = QEIBTECoarseKgridConvergenceSubWorkflow(
                *args,
                sequencer=(
                    self.ibte_coarse_kgrid_convergence_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_ibte_coarse_qgrid_convergence(self, *args, **kwargs):
        """Set the ibte coarse qgrid convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the whole sequence.
        asr: str, optional
            Needed for the matdyn calculations to get the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        scf_input_variables: dict, optional
            The dict of scf input variables. Ignored if 'use_gs' is True.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters. Ignored
            if 'use_gs' is True.
        nscf_kgrid: list-like
            The coarse kgrid to use for the nscf calculations.
        nscf_input_variables: dict, optional
            The input variables for the NSCF calculations.
            If 'use_gs' is True, these variables will update the variables
            from the gs run. If None, the gs run input variables are used.
            If None but 'use_gs' is False, an error is thrown.
        nscf_calculation_parameters: dict
            The calculation parameters for the nscf calculations.
        band_structure_kpoint_path: list-like, optional
            The kpoint path for the band structure calculation.
            Ignored if 'use_band_structure' is True.
        band_structure_kpoint_path_density: int, optional
            The number of points between points in the band structure.
            Ignored if 'use_band_structure' is True.
        band_structure_input_variables: dict, optional
            The dict of the band structures input variables.
            Ignored if 'use_band_structure' is True.
        band_structure_calculation_parameters: dict, optional
            The calculation parameters for the band structure calculations.
            Ignored if 'use_band_structure' is True.
        phonons_input_variables: dict, optional
            The phonons input variables.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_calculation_parameters: dict, optional
            The calculation parameters for the phonons calculations.
            Ignored if 'use_phonon_dispersion' is True.
        phonons_qpoint_grids: list, optional
            The phonon qpoint grids to test.
        q2r_calculation_parameters: dict, optional
            The q2r calculation parameters.
            Ignored if 'use_phonon_dispersion' is True.
        matdyn_calculation_parameters: dict, optional
            The matdyn calculation parmeters.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path: list-like
            The qpoint path for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        qpoint_path_density: int
            The qpoint path density for the phonon dispersion.
            Ignored if 'use_phonon_dispersion' is True.
        epw_input_variables: dict
            The epw input variables for the interpolation.
        epw_calculation_parameters: dict, optional
            The epw calculation parameters for the interpolation.
        plot_calculation_parameters: dict, optional
            Plot parameters for the interpolations.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf calculation
            for this sequence.
        use_converged_gs_ecut: bool, optional
            If True and 'use_gs' is not, the gs converged ecut is used for the
            scf calculation.
        use_converged_gs_kgrid: bool, optional
            If True and 'use_gs' is not, the gs converged kgrid is used for the
            scf calculation.
        use_converged_gs_smearing: bool, optional
            If True and 'use_gs' is not, the gs converged smearing
            is used for the scf calculation.
        use_phonon_converged_ecut: bool, optional
            If True and 'use_gs' is not, the phonon converged ecut
            is used for the scf calculation.
        use_phonon_converged_smearing: bool, optional
            If True and 'use_gs' is not, the phonon converged smearing
            is used for the scf calculation.
        use_band_structure: bool, optional
            If True, the band structure part of the workflow is used as the
            band structure part of the sequence.
        use_ibte_converged_fsthick: bool, optional
            If True, will sue the converged fsthick value for the IBTE part.
        pause_after_epw: bool, optional
            If True, will hold on after epw calcs in order for user to check
            out interpolated dispersions before launching ibte runs.
        """
        if not self._compute_ibte_coarse_qgrid_convergence:
            return
        self._logger.info("Setting 'ibte_coarse_qgrid_convergence'")
        subworkflow = QEIBTECoarseQgridConvergenceSubWorkflow(
                *args,
                sequencer=(
                    self.ibte_coarse_qgrid_convergence_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            self._logger.info(
                    "Setting sequencers for 'ibte_coarse_qgrid_convergence'.")
            await subworkflow.set_sequencer()

    async def set_ibte_fine_grid_convergence(self, *args, **kwargs):
        """Set the 'ibte_fine_grid_convergence' part of the workflow.

        Parameters
        ----------
        ibte_workdir: str
            The root directory where the ibte calculation will run and results
            will be stored.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE run.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE run.
        ibte_fine_grids: list of dicts
            The list of dicts of fine grids input variables to test.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        use_ibte_converged_fsthick: bool, optional
            If True, will use the converged fsthick parameter
            as declared upon init.
        """
        if not self._compute_ibte_fine_grid_convergence:
            return
        subworkflow = QEIBTEFineGridConvergenceSubWorkflow(
                *args,
                sequencer=(
                    self.ibte_fine_grid_convergence_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        await subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_ibte_with_thermal_expansion(
            self, root_workdir: str = None,
            ibte_input_variables: Mapping[str, Any] = None,
            ibte_calculation_parameters: Mapping[str, Any] = None,
            plot_calculation_parameters: Mapping[str, Any] = None):
        """Set the ibte calculations that considers the lattice expansion.

        Parameters
        ----------
        root_workdir: str
            The root directory where the ibte calculations will run.
        ibte_input_variables: dict
            The dictionary of the EPW input variables for the IBTE runs.
        ibte_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            IBTE runs.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        """
        if not self._compute_ibte_with_thermal_expansion:
            return
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        if not await seqs.sequence_completed:
            self._logger.info(
                    "EPW with lattice expansion not completed. Nothing done "
                    "for IBTE with lattice expansion.")
            self.stop_at_workflow = "epw_wannierization_with_thermal_expansion"
            return
        results_dir = os.path.dirname(seqs[0].plot_save)
        if not await aiofiles.os.path.exists(results_dir):
            # sequence finished but no results => run epw to get them
            self.run_epw_wannierization_with_thermal_expansion()
        if not await aiofiles.os.path.exists(results_dir):
            # something happend don't do anything
            self._logger.info("Somthing happened for epw results do nothing.")
            return
        if root_workdir is None:
            raise ValueError("Need to set 'ibte_workdir'.")
        if ibte_input_variables is None:
            raise ValueError("Need to set 'ibte_input_variables'.")
        if ibte_calculation_parameters is None:
            raise ValueError("Need to set 'ibte_calculation_parameters'.")
        inp = ibte_input_variables.copy()
        if self._compute_lattice_expansion:
            temperatures = self.lattice_expansion_sequencer.temperatures
        else:
            # get temperatures from dirnames of epw sequencers
            temperatures = []
            # keep as strings for the dirname since we can be confused
            # with floats and integers
            for seq in (
                    self.epw_wannierization_with_thermal_expansion_sequencer):
                scf_dirname = os.path.basename(seq.scf_workdir)
                temperatures.append(scf_dirname.split("K")[0])
        coro = []
        for sequencer, epw_seq, temperature in zip(
                self.ibte_with_thermal_expansion_sequencer,
                self.epw_wannierization_with_thermal_expansion_sequencer,
                temperatures):
            sequencer.ibte_workdir = os.path.join(
                    root_workdir, "ibte_runs", f"{temperature}K")
            coro.append(sequencer.set_epw_wannierization_calculation(
                    epw_seq.epw_workdir))
        await asyncio.gather(*coro)
        for sequencer, temperature in zip(
                self.ibte_with_thermal_expansion_sequencer,
                temperatures):
            # update temperature in input variables
            inp = ibte_input_variables.copy()
            # convert temperature to float from here
            inp.update({"temps": [float(temperature)], "nstemp": 1})
            sequencer.ibte_input_variables = inp
            self._add_calculation_parameters(
                    "ibte_", ibte_calculation_parameters, sequencer)
            sequencer.plot_save = os.path.join(
                    root_workdir, "results", f"ibte_{temperature}K.pdf")
            sequencer.plot_save_pickle = os.path.join(
                    root_workdir, "results", f"ibte_{temperature}K.pickle")
            self._add_calculation_parameters(
                    "plot_", plot_calculation_parameters, sequencer)

    async def set_lattice_expansion(
            self, *args, **kwargs,
            ) -> None:
        """Set the ThermalExpansionSequencer.

        One needs to set scf, phonons, deltas_volumes. q2r, matdyn and
        plot attributes.

        Parameters
        ----------
        compute_electronic_free_energy: bool, optional
            If True, enables the computation of electronic free energies.
            This will require to set the dos-related arguments.
        deltas_volumes: list-like
            The percentage of volumes of change to operate from
            the lattice parameter given in the scf_input_variables.
        asr: str
            The acoustic sum rule to fulfill in the matdyn calculation.
        temperatures: list-like
            The list of temperatures to compute the lattice expansion to.
        bulk_modulus_initial_guess: float
            Initial guess for the bulk modulus to make the fit work with the
            equation of state.
        root_workdir: str
            Root worikdir for the sequencer and all the calculations in it.
        scf_input_variables: dict, optional
            The scf input variables. This parameter can be None. In that
            case, the input variables from the SCFSequencer from the gs run
            will be used instead.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters. This parameter can be None. In that
            case, the calculation parameters from the SCFSequencer
            from the gs run  will be used instead.
        phonons_input_variables: dict
            The phonons input variables.
        phonons_calculation_parameters:
            The phonons calculation parameters
        phonons_qpoint_grid: list
            The phonon qpoint grid.
        phonons_fine_qpoint_grid: list
            The fine phonon qpoint grid used to interpolate
            the phonon free energy.
        q2r_input_variables: dict
            The q2r input variables.
        q2r_calculation_parameters: dict
            The q2r calculation parameters.
        qpoint_path: list
            The list of qpoints that forms the phonon dispersion.
        qpoint_path_density: int
            The number of qpoints between the points given in the qpoint path.
        matdyn_calculation_parameters: dict
            The matdcyn calculation parameters.
        plot_calculation_parameters: dict, optional
            The plot parameters for the sequencer.
        use_relaxed_geometry: bool, optional
            If True, the relxed geometry will be used in the
            scf_input_variables and as the 'relaxed parameter' for benchmarks.
        use_phonon_converged_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_phonon_converged_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        only_compute_electronic_contributions: bool, optional
            If True, we only compute electronic contributions and leave out
            phonon contributions.

        N.B.: there are no matdyn input variables because they are
              automatically computed by the sequencer.
        """
        if not self._compute_lattice_expansion:
            return
        from .qe_subworkflows import (
                QEThermalExpansionSubWorkflow)
        subworkflow = QEThermalExpansionSubWorkflow(
                *args,
                sequencer=(
                    self.lattice_expansion_sequencer),
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_phonon_dispersion(self, *args, **kwargs):
        """Set the phonon dispersion part of the workflow.

        See :class:`<QEPhononDispersionSubWorkflow>` for documentation.
        """
        if not self._compute_phonon_dispersion:
            return
        subworkflow = QEPhononDispersionSubWorkflow(
                sequencer=self.phonon_dispersion_sequencer,
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        subworkflow.check()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_phonon_ecut_convergence(
            self, *args, compute_electric_field_response=False, **kwargs):
        """Sets the phonon ecut convergence part of the workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (for LO-TO splitting) will be
            computed (Irrelevent for metals sor set to False).
        """
        await super().set_phonon_ecut_convergence(*args, **kwargs)
        seq = self.phonon_ecut_convergence_sequencer
        if compute_electric_field_response:
            try:
                seq.phonons_input_variables.update({
                    "epsil": True, "zeu": True})
            except ValueError:
                return

    async def set_phonon_kgrid_convergence(
            self, *args, compute_electric_field_response=False, **kwargs):
        """Sets the phonon kgrid convergence part of the workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (for LO-TO splitting) will be
            computed (Irrelevent for metals sor set to False).
        """
        await super().set_phonon_kgrid_convergence(*args, **kwargs)
        seq = self.phonon_kgrid_convergence_sequencer
        if compute_electric_field_response:
            try:
                seq.phonons_input_variables.update({
                    "epsil": True, "zeu": True})
            except ValueError:
                return

    async def set_relaxation(
            self, *args,
            maximum_relaxations=None, **kwargs):
        """Set the relaxation part of the workflow.

        Parameters
        ----------
        maximum_relaxations: int
            Maximum number cell relaxations to do in case relax_cell is set
            to True.
        """
        if not self._compute_relaxation:
            return
        await super().set_relaxation(*args, **kwargs)
        if maximum_relaxations is None:
            if self.relaxation_sequencer.relax_cell:
                raise ValueError("Need to set 'maximum_relaxations'.")
            maximum_relaxations = 0  # dummy
        self.relaxation_sequencer.maximum_relaxations = maximum_relaxations

    async def set_ziman(
            self, root_workdir=None, ziman_input_variables=None,
            ziman_calculation_parameters=None,
            plot_calculation_parameters=None):
        """Set the 'ziman' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root directory where the calculation will run and results
            will be stored.
        ziman_input_variables: dict
            The dictionary of the EPW input variables for the Ziman run.
        ziman_calculation_parameters: dict
            The dictionary of the EPW calculation parameters for the
            Ziman run.
        plot_calculation_parameters: dict, optional
            The plot parameters for the ibte plots.
        """
        if not self._compute_ziman:
            return
        sequencer = self.ziman_sequencer
        if not self.epw_wannierization_sequencer.sequence_completed:
            self.stop_at_workflow = "epw_wannierization"
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.ziman_workdir = os.path.join(
                root_workdir, "ziman_run")
        await sequencer.set_epw_wannierization_calculation(
                self.epw_wannierization_sequencer.epw_workdir)
        if ziman_input_variables is None:
            raise ValueError("Need to set 'ziman_input_variables'.")
        sequencer.ziman_input_variables = ziman_input_variables
        self._add_calculation_parameters(
                "ziman_", ziman_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "ziman.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "ziman.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def write_dos_kgrid_convergence(self):
        """Wride the 'dos_kgrid_convergence' part of the workflow."""
        await self.dos_kgrid_convergence_sequencer.write()
        if not await self.dos_kgrid_convergence_sequencer.sequence_completed:
            return
        post_processor = QEDOSKgridConvergencePostProcessSubWorkflow(
                sequencer=self.dos_kgrid_convergence_sequencer,
                workflow=self,
                loglevel=self._loglevel)
        await post_processor.make_dos_kgrid_convergence_plots()

    async def write_epw_wannierization(self):
        """Write the epw wannierization part of the workflow."""
        await self.epw_wannierization_sequencer.write()

    async def write_epw_wannierization_with_thermal_expansion(self):
        """Write the epw interpolation w/ thermal exp part of the workflow."""
        if self._compute_lattice_expansion:
            if not await self.lattice_expansion_sequencer.sequence_completed:
                return
        await self.epw_wannierization_with_thermal_expansion_sequencer.write()

    async def write_ibte(self):
        """Write the ibte part of the workflow."""
        await self.ibte_sequencer.write()

    async def write_ibte_fsthick_convergence(self):
        """Write the ibte fsthick convergence part of the workflow."""
        await self.ibte_fsthick_convergence_sequencer.write()

    async def write_ibte_fine_grid_convergence(self):
        """Write the ibte fine grid convergence part of the workflow."""
        await self.ibte_fine_grid_convergence_sequencer.write()

    async def write_ibte_coarse_kgrid_convergence(self) -> None:
        """Write the ibte coarse kgrid convergence part of the workflow."""
        await self._write_ibte_coarse_grid_convergence(
                sequencer=self.ibte_coarse_kgrid_convergence_sequencer)

    async def _write_ibte_coarse_grid_convergence(self, sequencer) -> None:
        all_epw_wann_seqs = []
        all_ibte_seqs = []
        for seq in sequencer:
            if isinstance(seq, QEEPWIBTESequencer):
                all_ibte_seqs.append(seq)
                continue
            all_epw_wann_seqs.append(seq)
        epw_wann_seqs = SequencersList(
                all_epw_wann_seqs, loglevel=self._loglevel)
        await epw_wann_seqs.write()
        if not await epw_wann_seqs.sequence_completed:
            return
        ibte_seqs = SequencersList(
                all_ibte_seqs, loglevel=self._loglevel)
        await ibte_seqs.write()

    async def write_ibte_coarse_qgrid_convergence(self):
        """Write the ibte coarse qgrid convergence part of the workflow."""
        await self._write_ibte_coarse_grid_convergence(
                sequencer=self.ibte_coarse_qgrid_convergence_sequencer)

    async def write_ibte_with_thermal_expansion(self):
        """Write the ibte with thermal expansion part of the workflow."""
        if self._compute_lattice_expansion:
            if not await self.lattice_expansion_sequencer.sequence_completed:
                return
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        if not await seqs.sequence_completed:
            return
        await self.ibte_with_thermal_expansion_sequencer.write()

    async def write_lattice_expansion(self):
        """Write the lattice expansion sequencer."""
        deltas = self.lattice_expansion_sequencer.deltas_volumes
        zero = 0 in deltas or 0.0 in deltas
        completed = await self.gs_sequencer.sequence_completed
        if self._compute_gs and not completed and zero:
            # wait for gs
            return
        await self.lattice_expansion_sequencer.write()

    async def write_ziman(self):
        """Write the 'ziman' part of the workflow."""
        await self.ziman_sequencer.write()

    async def _plot_final_results_epw_wannierization_with_thermal_expansion(
            self):
        """Plot final results for the epw with thermal expansion calcs.

        Basically makes a multiplot with all interpolations to easily see
        results.
        """
        final_plot_bs = MultiPlot()
        final_plot_bs.title = "Band Structures"
        final_plot_ph = MultiPlot()
        final_plot_ph.title = "Phonon dispersions"
        ncolumns = 2
        # the final plots will be ncolumns max.
        row_ph = 0
        row_bs = 0
        seqs = self.epw_wannierization_with_thermal_expansion_sequencer
        for sequencer in seqs:
            # we need to get ph path and bs path
            results_dir = os.path.dirname(sequencer.plot_save)
            temperature = os.path.basename(results_dir).split("K")[0]
            for filename in os.listdir(results_dir):
                if not filename.endswith(".pickle"):
                    continue
                plot = Plot.load_plot(os.path.join(results_dir, filename))
                plot.title = f"{temperature}K"
                if "phonon_dispersion" in filename:
                    final_plot_ph.add_plot(plot, row=row_ph // ncolumns)
                    row_ph += 1
                elif "band_structure" in filename:
                    final_plot_bs.add_plot(plot, row=row_bs // ncolumns)
                    row_bs += 1
        final_plot_bs.plot(
                show=sequencer.plot_calculation_parameters.get("show", True),
                show_legend_on=[[0, 0]],
                )
        final_plot_ph.plot(
                show=sequencer.plot_calculation_parameters.get("show", True),
                show_legend_on=[[0, 0]],
                )
        # save plot in the dirname of the dirname
        dirname = os.path.dirname(os.path.dirname(
            sequencer.plot_save_pickle))
        nameph = "final_results_ph_disp_with_wannier_interpolation.pdf"
        namebs = "final_results_bs_with_wannier_interpolation.pdf"
        await asyncio.gather(
                *(final_plot_ph.save(os.path.join(dirname, nameph)),
                  final_plot_bs.save(os.path.join(dirname, namebs)),
                  ),
                )

    async def _plot_final_results_ibte_with_thermal_expansion(self, show):
        conductivity_tensor = ConductivityTensor()
        conductivity_tensor.conductivity_tensor = []
        conductivity_tensor.serta_conductivity_tensor = []
        conductivity_tensor.temperatures = []
        for sequencer in self.ibte_with_thermal_expansion_sequencer:
            # get temperatures from directory names
            temperature = float(
                    os.path.basename(sequencer.ibte_workdir).split("K")[0])
            async with await CalculationDirectory.from_calculation(
                    sequencer.ibte_workdir) as calc:
                logpath = (await calc.log_file).path
            this_cond = await ConductivityTensor.from_file(logpath)
            conductivity_tensor.conductivity_tensor.append(
                    this_cond.conductivity_tensor[0])
            conductivity_tensor.serta_conductivity_tensor.append(
                    this_cond.serta_conductivity_tensor[0])
            conductivity_tensor.temperatures.append(temperature)
        conductivity_tensor.conductivity_tensor = np.array(
                conductivity_tensor.conductivity_tensor)
        conductivity_tensor.serta_conductivity_tensor = np.array(
                conductivity_tensor.serta_conductivity_tensor)
        for serta in [True, False]:
            await asyncio.gather(
                    *(self._create_ibte_plot(serta, show, conductivity_tensor)
                        for serta in (True, False)))

    async def _create_ibte_plot(self, serta, show, conductivity_tensor):
        await asyncio.gather(
                *(self._create_ibte_plot_resistivity(
                    serta, resistivity, factor, unit, title, name, show,
                    conductivity_tensor)
                    for resistivity, factor, unit, title, name in zip(
                        [True, False], [1e6, 1e-6],
                        [r"$\mu\Omega$cm", r"$\mu\Omega$cm$^{-1}$"],
                        ["Resistivity tensor", "Conductivity tensor"],
                        ["resistivity", "conductivity"])))

    async def _create_ibte_plot_resistivity(
            self, serta, resistivity, factor, unit, title, name, show,
            conductivity_tensor):
        sequencer = self.ibte_with_thermal_expansion_sequencer[0]
        dirsave = os.path.dirname(sequencer.plot_save)
        elements = sequencer.plot_calculation_parameters.get("elements", None)
        if serta:
            title += " (SERTA)"
            name += "_serta"
        else:
            title += " (IBTE)"
            name += "_ibte"
        plot = conductivity_tensor.get_plot(
                resistivity=resistivity, conversion_factor=factor,
                xunits="K", yunits=unit, linestyle="-", title=title,
                linewidth=2, elements=elements)
        plot.plot(show=show)
        await plot.save(
                   os.path.join(dirsave, name + ".pdf"),
                   overwrite=True)
        await plot.save_plot(
                   os.path.join(dirsave, name + ".pickle"),
                   overwrite=True)
        plot.reset()

    async def _post_process_ibte_fsthick_convergence(self):
        # plot final results for ibte fsthick convergence
        # plots for resistivity and conductivity
        all_res_plots = []
        all_cond_plots = []
        fsthicks = []
        seqs = self.ibte_fsthick_convergence_sequencer
        colors = [f"C{i}" for i, _ in enumerate(seqs)]
        for sequencer in seqs:
            results_dir = os.path.dirname(sequencer.plot_save_pickle)
            fsthicks.append(
                    sequencer.ibte_input_variables["fsthick"])
            for filename in await aiofiles.os.listdir(results_dir):
                if ".pickle" not in filename:
                    continue
                if "serta" in filename.lower():
                    continue
                if "resistivity" in filename:
                    all_res_plots.append(
                            Plot.load_plot(
                                os.path.join(results_dir, filename)))
                else:
                    all_cond_plots.append(
                            Plot.load_plot(
                                os.path.join(results_dir, filename)))
        # got all plots. now make 1 plot for each tensor element present.
        show = sequencer.plot_calculation_parameters.get("show", True)
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(sequencer.ibte_workdir)),
                "results")
        for plot_container, tag in zip(
                [all_res_plots, all_cond_plots],
                ["resistivity", "conductivity"]):
            curves = plot_container[0].plot_objects["curves"]
            for i, curve in enumerate(curves):
                final_plot = Plot(loglevel=self._loglevel)
                el = curve.label.split("{")[-1].split("}")[0]
                for (iplot, plot), fsthick in zip(
                        enumerate(plot_container), fsthicks):
                    curve = plot.plot_objects["curves"][i]
                    final_plot.add_curve(
                            curve.xdata, curve.ydata,
                            color=colors[iplot],
                            linestyle=curve.linestyle,
                            linewidth=curve.linewidth,
                            label=f"fsthick={fsthick}")
                final_plot.xlabel = plot.xlabel
                final_plot.ylabel = plot.ylabel
                final_plot.title = (
                        f"IBTE fsthick convergence {tag} {el} component")
                final_plot.legend = True
                final_plot.grid = True
                final_plot.plot(show=show)
                self._logger.info(
                        "Saving final IBTE fsthick convergence plots in: "
                        f"'{results_dir}'")
                await final_plot.save(
                        os.path.join(
                            results_dir,
                            f"ibte_fsthick_convergence_{tag}_{el}.pdf"),
                        overwrite=True,
                        )
                await final_plot.save_pickle(
                        os.path.join(
                            results_dir,
                            f"ibte_fsthick_convergence_{tag}_{el}.pickle"),
                        overwrite=True,
                        )
