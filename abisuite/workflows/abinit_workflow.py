import asyncio
import os
from typing import Any

import aiofiles.os

from .abinit_subworkflows import (
        AbinitBandStructureSubWorkflow,
        AbinitGSSmearingConvergenceSubWorkflow,
        AbinitGSSubWorkflow,
        AbinitPhononDispersionQgridConvergencePostProcessSubWorkflow,
        AbinitPhononDispersionQgridConvergenceSubWorkflow,
        AbinitPhononSmearingConvergenceSubWorkflow,
        )
from .bases import BaseWorkflow, SequencersList
from ..constants import EV_TO_HARTREE
from ..launchers.launchers.abinit_launchers.abinit_launcher import (
        __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__,
        __ALL_ABINIT_GEOMETRY_VARIABLES__,
        )
from ..plotters import MultiPlot, Plot, rand_cmap
from ..routines import is_2d_arr, is_list_like
from ..sequencers import (
        AbinitBSEKgridConvergenceSequencer,
        AbinitBSESequencer,
        AbinitBSEecutepsConvergenceSequencer,
        AbinitBSEnbandConvergenceSequencer,
        AbinitBandStructureComparatorSequencer,
        AbinitBandStructureSequencer,
        AbinitDOSSequencer,
        AbinitEcutConvergenceSequencer, AbinitEcutPhononConvergenceSequencer,
        AbinitFermiSurfaceSequencer,
        AbinitGWKgridConvergenceSequencer,
        AbinitGWScreeningnbandConvergenceSequencer,
        AbinitGWSelfEnergynbandConvergenceSequencer,
        AbinitGWSequencer,
        AbinitGWecutepsConvergenceSequencer,
        AbinitKgridConvergenceSequencer,
        AbinitKgridPhononConvergenceSequencer,
        AbinitOpticSequencer,
        AbinitPhononDispersionSequencer,
        AbinitRelaxationSequencer,
        AbinitSCFSequencer,
        AbinitSmearingConvergenceSequencer,
        AbinitSmearingPhononConvergenceSequencer,
        )


def is_geometry_defined(input_vars):
    """Return True if geometry is fully defined in this dict of input vars."""
    for var in __ALL_ABINIT_GEOMETRY_VARIABLES__:
        if any([var in sublist
                for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__]):
            # variable in a sublist for 'one_of_them'. deal with it later
            continue
        if var not in input_vars:
            return False
    for sublist in __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__:
        npresent = 0
        for var in sublist:
            if var in input_vars:
                npresent += 1
        if npresent != 1:
            return False
    return True


class AbinitWorkflow(BaseWorkflow):
    """Workflow for Abinit.

    Workflow order:
      - :meth:`gs_ecut_convergence<.set_gs_ecut_convergence>`
      - :meth:`gs_kgrid_convergence<.set_gs_kgrid_convergence>`
      - :meth:`gs_smearing_convergence<.set_gs_smearing_convergence>`
      - :meth:`relaxation<.BaseWorkflow.set_relaxation>`
      - :meth:`phonon_ecut_convergence<.set_phonon_ecut_convergence>`
      - :meth:`phonon_kgrid_convergence<.set_phonon_kgrid_convergence>`
      - :meth:`phonon_smearing_convergence<.set_phonon_smearing_convergence>`
      - :meth:`gs<.set_gs>`
      - :meth:`band_structure<.set_band_structure>`
      - :meth:`dos<.set_dos>`
      - :meth:`fermi_surface<.set_fermi_surface>`
      - :meth:`phonon_dispersion_qgrid_convergence<.set_phonon_dispersion_qgrid_convergence>`
      - :meth:`phonon_dispersion<.set_phonon_dispersion>`
      - :meth:`band_structure_convergence<.set_band_structure_convergence>`
      - :meth:`optic<.set_optic>`
      - :meth:`optic_nscf_convergence<.set_optic_nscf_convergence>`
      - :meth:`gw_self_energy_nband_convergence<.set_gw_self_energy_nband_convergence>`
      - :meth:`gw_screening_nband_convergence<.set_gw_screening_nband_convergence>`
      - :meth:`gw_ecuteps_convergence<.set_gw_ecuteps_convergence>`
      - :meth:`gw_kgrid_convergence<.set_gw_kgrid_convergence>`
      - :meth:`gw<.set_gw>`
      - :meth:`bse_nband_convergence<.set_bse_nband_convergence>`
      - :meth:`bse_ecuteps_convergence<.set_bse_ecuteps_convergence>`
      - :meth:`bse_kgrid_convergence<.set_bse_kgrid_convergence>`
      - :meth:`bse<.set_bse>`
    """  # noqa: E501

    _all_geometry_variables = __ALL_ABINIT_GEOMETRY_VARIABLES__
    _band_structure_sequencer_cls = AbinitBandStructureSequencer
    _band_structure_subworkflow_cls = AbinitBandStructureSubWorkflow
    _dos_sequencer_cls = AbinitDOSSequencer
    _fermi_surface_sequencer_cls = AbinitFermiSurfaceSequencer
    _loggername = "AbinitWorkflow"
    _gs_subworkflow_cls = AbinitGSSubWorkflow
    _gs_sequencer_cls = AbinitSCFSequencer
    _gs_ecut_convergence_sequencer_cls = AbinitEcutConvergenceSequencer
    _gs_kgrid_convergence_sequencer_cls = AbinitKgridConvergenceSequencer
    _gs_smearing_convergence_sequencer_cls = AbinitSmearingConvergenceSequencer
    _phonon_ecut_convergence_sequencer_cls = (
            AbinitEcutPhononConvergenceSequencer)
    _phonon_dispersion_sequencer_cls = AbinitPhononDispersionSequencer
    _phonon_dispersion_qgrid_convergence_subworkflow_cls = (
            AbinitPhononDispersionQgridConvergenceSubWorkflow)
    _phonon_dispersion_qgrid_convergence_post_processor_cls = (
            AbinitPhononDispersionQgridConvergencePostProcessSubWorkflow)
    _phonon_kgrid_convergence_sequencer_cls = (
        AbinitKgridPhononConvergenceSequencer)
    _phonon_smearing_convergence_sequencer_cls = (
            AbinitSmearingPhononConvergenceSequencer)
    _phonon_smearing_convergence_subworkflow_cls = (
            AbinitPhononSmearingConvergenceSubWorkflow)
    _relaxation_sequencer_cls = AbinitRelaxationSequencer
    workflow = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence", "phonon_kgrid_convergence",
            "phonon_smearing_convergence", "gs", "band_structure", "dos",
            "fermi_surface",
            "phonon_dispersion_qgrid_convergence", "phonon_dispersion",
            "band_structure_convergence",
            "optic",
            "optic_nscf_convergence",
            "gw_self_energy_nband_convergence",
            "gw_screening_nband_convergence", "gw_ecuteps_convergence",
            "gw_kgrid_convergence", "gw", "bse_nband_convergence",
            "bse_ecuteps_convergence", "bse_kgrid_convergence", "bse",
            )

    def __init__(
            self, *args,
            band_structure_convergence=False,
            bse=False,
            bse_ecuteps_convergence=False,
            bse_nband_convergence=False,
            bse_kgrid_convergence=False,
            converged_bse_nband=None,
            converged_bse_ecuteps=None,
            converged_bse_kgrid=None,
            converged_gw_self_energy_nband=None,
            converged_gw_screening_nband=None,
            converged_gw_ecuteps=None,
            converged_gw_kgrid=None,
            gw=False,
            gw_ecuteps_convergence=False,
            gw_kgrid_convergence=False,
            gw_screening_nband_convergence=False,
            gw_self_energy_nband_convergence=False,
            optic=False, optic_nscf_convergence=False,
            **kwargs):
        """Init method.

        Parameters
        ----------
        band_structure_convergence: bool, optional
            If True, will compute a band structure convergence
            with respect a given input variable parameter.
        bse: bool, optional
            If True, the BSE calculation is enabled.
        bse_nband_convergence: bool, optional
            If True, the BSE nband convergence will be computed.
        bse_kgrid_convergence: bool, optional
            If True, the BSE ngkpt convergence will be computed.
        bse_ecuteps_convergence: bool, optional
            If True, the BSE ecuteps convergence will be computed.
        converged_bse_nband: dict, optional
            The converged nband value for a BSE calculation. It is a dict that
            contains the 'bs_loband' and 'nband' parameters.
        converged_bse_ecuteps: dict, optional
            The converged ecuteps value for a BSE calculation.
            It is a dict that contains the 'ecuteps' parameter.
        converged_gw_self_energy_nband: dict, optional
            If not None, this dict contains the converged GW self energy nband.
            It is used if the 'gw_self_energy_nband_convergence' is not
            activated.
        converged_gw_screening_nband: dict, optional
            If not None, this dict contains the converged GW screening nband.
            It is used if the 'gw_screening_nband_convergence' is not
            activated.
        converged_gw_ecuteps: dict, optional
            If not None, this dict contains the converged GW screening ecuteps.
            It is used if the 'gw_ecuteps_convergence' is not
            activated.
        converged_gw_kgrid: dict, optional
            If not None, this dict contains the converged GW ngkpt.
            It is used if the 'gw_kgrid_convergence' is not
            activated.
        gw_kgrid_convergence: bool, optional
            If True, the ngkpt convergence is computed.
        gw_screening_energy_nband_convergence: bool, optional
            If True, the screening nband convergence is computed.
        gw_self_energy_nband_convergence: bool, optional
            If True, the gw self energy nband convergence is computed.
        gw_ecuteps_convergence: bool, optional
            If True, the gw ecuteps convergence is computed.
        gw: bool, optional
            If True, gw is computed.
        optic: bool, optional
            If True, will compute the optic response of the material.
        optic_nscf_convergence: bool, optional
            If True, will compute the nscf grid convergence to the optic
            response of the material.
        """
        super().__init__(*args, **kwargs)
        # BAND STRUCTURE CONVERGENCE PART
        self._compute_band_structure_convergence = band_structure_convergence
        self._band_structure_convergence_sequencer = None
        # OPTIC PART
        self._compute_optic = optic
        self._optic_sequencer = None
        # OPTIC NSCF CONVERGENCE PART
        self._optic_nscf_convergence_sequencer = None
        self._compute_optic_nscf_convergence = optic_nscf_convergence
        # GW SELF ENERGY NBAND CONVERGENCE PART
        self._gw_self_energy_nband_convergence_sequencer = None
        self._compute_gw_self_energy_nband_convergence = (
                gw_self_energy_nband_convergence)
        self.converged_gw_self_energy_nband = converged_gw_self_energy_nband
        # GW SCREENING NBAND CONVERGENCE PART
        self._gw_screening_nband_convergence_sequencer = None
        self._compute_gw_screening_nband_convergence = (
                gw_screening_nband_convergence)
        self.converged_gw_screening_nband = converged_gw_screening_nband
        # GW ECUTEPS CONVERGENCE PART
        self._gw_ecuteps_convergence_sequencer = None
        self._compute_gw_ecuteps_convergence = gw_ecuteps_convergence
        self.converged_gw_ecuteps = converged_gw_ecuteps
        # GW KGRID CONVERGENCE PART
        self._gw_kgrid_convergence_sequencer = None
        self._compute_gw_kgrid_convergence = gw_kgrid_convergence
        self.converged_gw_kgrid = converged_gw_kgrid
        # GW PART
        self._gw_sequencer = None
        self._compute_gw = gw
        # BSE NBAND CONVERGENCE PART
        self._bse_nband_convergence_sequencer = None
        self._compute_bse_nband_convergence = bse_nband_convergence
        self.converged_bse_nband = converged_bse_nband
        # BSE ECUTEPS CONVERGENCE PART
        self._bse_ecuteps_convergence_sequencer = None
        self._compute_bse_ecuteps_convergence = bse_ecuteps_convergence
        self.converged_bse_ecuteps = converged_bse_ecuteps
        # BSE NGKPT CONVERGENCE PART
        self._bse_kgrid_convergence_sequencer = None
        self._compute_bse_kgrid_convergence = bse_kgrid_convergence
        self.converged_bse_kgrid = converged_bse_kgrid
        # BSE PART
        self._bse_sequencer = None
        self._compute_bse = bse

    @property
    def band_structure_convergence_sequencer(self):
        """Return the band structure convergence sequencer."""
        if self._band_structure_convergence_sequencer is not None:
            return self._band_structure_convergence_sequencer
        bs = AbinitBandStructureComparatorSequencer(loglevel=self._loglevel)
        self._band_structure_convergence_sequencer = bs
        return self.band_structure_convergence_sequencer

    @property
    def bse_sequencer(self):
        """Return the bse sequencer."""
        if self._bse_sequencer is not None:
            return self._bse_sequencer
        seq = AbinitBSESequencer(loglevel=self._loglevel)
        self._bse_sequencer = seq
        return seq

    @property
    def bse_ecuteps_convergence_sequencer(self):
        """Return the bse ecuteps convergence sequencer."""
        if self._bse_ecuteps_convergence_sequencer is not None:
            return self._bse_ecuteps_convergence_sequencer
        seq = AbinitBSEecutepsConvergenceSequencer(loglevel=self._loglevel)
        self._bse_ecuteps_convergence_sequencer = seq
        return seq

    @property
    def bse_kgrid_convergence_sequencer(self):
        """Return the bse ngkpt convergence sequencer."""
        if self._bse_kgrid_convergence_sequencer is not None:
            return self._bse_kgrid_convergence_sequencer
        seq = AbinitBSEKgridConvergenceSequencer(loglevel=self._loglevel)
        self._bse_kgrid_convergence_sequencer = seq
        return seq

    @property
    def bse_nband_convergence_sequencer(self):
        """Return the bse nband convergence sequencer."""
        if self._bse_nband_convergence_sequencer is not None:
            return self._bse_nband_convergence_sequencer
        seq = AbinitBSEnbandConvergenceSequencer(loglevel=self._loglevel)
        self._bse_nband_convergence_sequencer = seq
        return seq

    @property
    def gw_ecuteps_convergence_sequencer(self):
        """Return the GW ecuteps convergence sequencer."""
        if self._gw_ecuteps_convergence_sequencer is not None:
            return self._gw_ecuteps_convergence_sequencer
        self._gw_ecuteps_convergence_sequencer = (
                AbinitGWecutepsConvergenceSequencer(
                    loglevel=self._loglevel))
        return self.gw_ecuteps_convergence_sequencer

    @property
    def gw_screening_nband_convergence_sequencer(self):
        """Return the GW screening nband convergence sequencer."""
        if self._gw_screening_nband_convergence_sequencer is not None:
            return self._gw_screening_nband_convergence_sequencer
        self._gw_screening_nband_convergence_sequencer = (
                AbinitGWScreeningnbandConvergenceSequencer(
                    loglevel=self._loglevel))
        return self.gw_screening_nband_convergence_sequencer

    @property
    def gw_self_energy_nband_convergence_sequencer(self):
        """Return the GW self energy nband convergence sequencer."""
        if self._gw_self_energy_nband_convergence_sequencer is not None:
            return self._gw_self_energy_nband_convergence_sequencer
        self._gw_self_energy_nband_convergence_sequencer = (
                AbinitGWSelfEnergynbandConvergenceSequencer(
                    loglevel=self._loglevel))
        return self.gw_self_energy_nband_convergence_sequencer

    @property
    def gw_kgrid_convergence_sequencer(self):
        """Return the GW kgrid convergence sequencer."""
        if self._gw_kgrid_convergence_sequencer is not None:
            return self._gw_kgrid_convergence_sequencer
        self._gw_kgrid_convergence_sequencer = (
                AbinitGWKgridConvergenceSequencer(
                    loglevel=self._loglevel))
        return self.gw_kgrid_convergence_sequencer

    @property
    def gw_sequencer(self):
        """Return the GW sequencer."""
        if self._gw_sequencer is not None:
            return self._gw_sequencer
        self._gw_sequencer = AbinitGWSequencer(loglevel=self._loglevel)
        return self.gw_sequencer

    @property
    def optic_sequencer(self):
        """Return the optic sequencer."""
        if self._optic_sequencer is not None:
            return self._optic_sequencer
        self._optic_sequencer = AbinitOpticSequencer(loglevel=self._loglevel)
        return self.optic_sequencer

    @property
    def optic_nscf_convergence_sequencer(self):
        """Return the optic nscf convergence sequencer."""
        if self._optic_nscf_convergence_sequencer is not None:
            return self._optic_nscf_convergence_sequencer
        self._optic_nscf_convergence_sequencer = (
            SequencersList([], loglevel=self._loglevel))
        return self.optic_nscf_convergence_sequencer

    async def run_band_structure(self):
        """Run the band structure part of the workflow."""
        if self._compute_optic:
            # optic can use the bandgap if requested
            # it needs to run the sequencer
            # don't rerun if already run
            if self.band_structure_sequencer.has_been_run:
                return
        await super().run_band_structure()

    def clean_band_structure_convergence(self):
        """Cleans the 'band_structure_convergence' part of the workflow."""
        self.band_structure_convergence_sequencer.clean()

    def clean_bse(self):
        """Clean the 'bse' part of the workflow."""
        self.bse_sequencer.clean()

    def clean_bse_ecuteps_convergence(self):
        """Clean the 'bse_ecuteps_convergence' part of the workflow."""
        self.bse_ecuteps_convergence_sequencer.clean()

    def clean_bse_nband_convergence(self):
        """Clean the 'bse_nband_convergence' part of the workflow."""
        self.bse_nband_convergence_sequencer.clean()

    def clean_bse_kgrid_convergence(self):
        """Clean the 'bse_kgrid_convergence' part of the workflow."""
        self.bse_kgrid_convergence_sequencer.clean()

    def clean_gw(self):
        """Cleans the 'gw' part of the workflow."""
        self.gw_sequencer.clean()

    def clean_gw_ecuteps_convergence(self):
        """Cleans the 'gw_ecuteps_convergence' part of the workflow."""
        self.gw_ecuteps_convergence_sequencer.clean()

    def clean_gw_self_energy_nband_convergence(self):
        """Clean the 'gw_self_energy_nband_convergence' part of the flow."""
        self.gw_self_energy_nband_convergence_sequencer.clean()

    def clean_gw_screening_nband_convergence(self):
        """Cleans the 'gw_screening_nband_convergence' part of the workflow."""
        self.gw_screening_nband_convergence_sequencer.clean()

    def clean_gw_kgrid_convergence(self):
        """Cleans the 'gw_kgrid_convergence' part of the workflow."""
        self.gw_kgrid_convergence_sequencer.clean()

    def clean_optic(self):
        """Cleans the 'optic' part of the workflow."""
        self.optic_sequencer.clean()

    def clean_optic_nscf_convergence(self):
        """Cleans the 'optic_nscf_convergence_part' of the workflow."""
        self.optic_nscf_sequencer.clean()

    async def run_band_structure_convergence(self):
        """Run the band structure convergence part of the workflow."""
        await self.band_structure_convergence_sequencer.run()

    async def run_bse(self):
        """Run the 'bse' part of the workflow."""
        await self.bse_sequencer.run()

    async def run_bse_ecuteps_convergence(self):
        """Run the 'bse_ecuteps_convergence' part of the workflow."""
        await self.bse_ecuteps_convergence_sequencer.run()

    async def run_bse_nband_convergence(self):
        """Run the 'bse_nband_convergence' part of the workflow."""
        await self.bse_nband_convergence_sequencer.run()

    async def run_bse_kgrid_convergence(self):
        """Run the 'bse_kgrid_convergence' part of the workflow."""
        await self.bse_kgrid_convergence_sequencer.run()

    async def run_gw(self):
        """Run the GW part of the workflow."""
        await self.gw_sequencer.run()

    async def run_gw_ecuteps_convergence(self):
        """Run the GW ecuteps convergence part of the workflow."""
        await self.gw_ecuteps_convergence_sequencer.run()

    async def run_gw_screening_nband_convergence(self):
        """Run the GW screening nband convergence part of the workflow."""
        await self.gw_screening_nband_convergence_sequencer.run()

    async def run_gw_self_energy_nband_convergence(self):
        """Run the GW self energy nband convergence part of the workflow."""
        await self.gw_self_energy_nband_convergence_sequencer.run()

    async def run_gw_kgrid_convergence(self):
        """Run the GW ngkpt convergence part of the workflow."""
        await self.gw_kgrid_convergence_sequencer.run()

    async def run_optic(self):
        """Run the optic part of the workflow."""
        await self.optic_sequencer.run()

    async def run_optic_nscf_convergence(self):
        """Runs the 'optic_nscf_convergence' part of the workflow.

        Also generates the convergence plots.
        """
        await self.optic_nscf_convergence_sequencer.run()
        if self.optic_nscf_convergence_sequencer.sequence_completed:
            await self._post_process_optic_nscf_convergence()

    async def set_band_structure(
            self, fatbands=False,
            band_structure_input_variables=None,
            **kwargs):
        """Sets the BandStructureSequencer.

        Parameters
        ----------
        band_structure_input_variables: dict
            The dict of the band structure input variables.
        fatbands: bool, optional
            If True, the orbital projections will be computed in order to draw
            the fatbands structure.

        Note
        ----
        all kwargs are passed to the mother class method of the same name.
        """
        if not self._compute_band_structure:
            return
        if band_structure_input_variables is None:
            raise ValueError("Need to set 'band_structure_input_variables'.")
        if fatbands:
            if "pawecutdg" in band_structure_input_variables:
                # paw calc, use pawfatbnd
                band_structure_input_variables["pawfatbnd"] = 2
            else:
                band_structure_input_variables["prtprocar"] = 1
                band_structure_input_variables["prtdos"] = 3
        await super().set_band_structure(
                band_structure_input_variables=band_structure_input_variables,
                **kwargs)

    async def set_band_structure_convergence(
            self, root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_specific_input_variables=None,
            band_structure_input_variables=None,
            band_structure_calculation_parameters=None,
            band_structure_kpoint_path=None,
            band_structure_kpoint_path_density=None,
            use_converged_gs_ecut=False,
            use_converged_gs_kgrid=False,
            use_relaxed_geometry=False,
            plot_calculation_parameters=None):
        """Sets the BandStructureComparatorSequencer.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict
            The scf calculation parameters.
        scf_specific_input_variables: dict, optional
            The specific scf input variables that differ for each scf calc.
        band_structure_input_variables: dict, optional
            The dict of the band structure input variables.
        band_structure_calculation_parameters: dict, optional
            The dict of the calculation parameters for the band structure
            calculation.
        band_structure_kpoint_path: list, optional
            The list of kpoints that forms the band structure.
        band_structure_kpoint_path_density: int, optional
            The number of points between kpoints in the list of
            the kpoints that forms the band structure.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study
            for the scf calculations.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study
            for the scf calculations.
            'gs_kgrid_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run for the scf calculations.
            'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_band_structure_convergence:
            return
        bsseq = self.band_structure_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # SCF PART
        bsseq.scf_workdir = os.path.join(
                root_workdir, "scf_runs", "scf")
        await self._use_converged_quantities(
                scf_input_variables,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_converged_gs_kgrid=use_converged_gs_kgrid,
                use_relaxed_geometry=use_relaxed_geometry,
                )
        bsseq.scf_input_variables = scf_input_variables
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, bsseq)
        if scf_specific_input_variables is None:
            raise ValueError(
                    "Need to set 'scf_specific_input_variables'.")
        bsseq.scf_specific_input_variables = scf_specific_input_variables

        # BAND STRUCTURE PART
        bsseq.band_structure_workdir = os.path.join(
                root_workdir, "band_structure_runs", "band_structure")
        if band_structure_kpoint_path is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path'.")
        bsseq.band_structure_kpoint_path = band_structure_kpoint_path
        if band_structure_kpoint_path_density is None:
            raise ValueError(
                    "Need to set 'band_structure_kpoint_path_density'.")
        bskpd = band_structure_kpoint_path_density
        bsseq.band_structure_kpoint_path_density = bskpd
        if band_structure_input_variables is None:
            raise ValueError("Need to set 'band_structure_input_variables'.")
        bsseq.band_structure_input_variables = band_structure_input_variables
        self._add_calculation_parameters(
                "band_structure_",
                band_structure_calculation_parameters, bsseq)
        # PLOTTING PART
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["title"] = "Band Structure"
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "band_structure.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "band_structure.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, bsseq)

    async def set_bse(self, *args, **kwargs) -> None:
        """Set the 'bse' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the bse nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        nscfnosym_input_variables: dict
            The dict of input variables for the nscfnosym part.
        nscfnosym_calculation_parameters: dict
            The dict of calculation parameters for the nscfnosym part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        bse_input_variables: dict
            The dict of input variables for the BSE part.
        bse_calculation_parameters: dict
            The dict of calculation parameters for the BSE part.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the BSE nband convergence sequence.
        use_gs_from: str, optional
            If not None, defines where we get the scf calculation.
            Can be a path to a real calculation too.
        use_scf_from: str, optional
            Same as 'use_gs_from'.
        use_nscf_from: str, optional
            If not None, defines where we get the nscf calculation.
            Can be a path to a real calculation too.
        use_nscfnosym_from: str, optional
            If not None, defines where we get the nscfnosym calculation.
            Can be a path to a real calculation too.
        use_screening_from: str, optional
            If not None, defines where we get the screening calculation.
            Can be a path to a real calculation too.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        use_relaxed_geometry: bool, optional
            If True, we use the relaxed geometry.
        use_converged_bse_nband: bool, optional
            If True, we use the converged nband and bs_loband values for the
            BSE part of the sequence.
        use_converged_bse_kgrid: bool, optional
            If True, we use the converged ngkpt value for the BSE part of this
            sequencer.
        use_nscf_from: str, optional
            If not None, defines the nscf calculation we link to.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_bse:
            return
        from .abinit_subworkflows import AbinitBSESubWorkflow
        subworkflow = AbinitBSESubWorkflow(
                *args, sequencer=self.bse_sequencer,
                workflow=self,
                **kwargs, loglevel=self._loglevel)
        subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_bse_ecuteps_convergence(self, root_workdir=None, **kwargs):
        """Set the 'bse_ecuteps_convergence' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the bse nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        nscfnosym_input_variables: dict
            The dict of input variables for the nscfnosym part.
        nscfnosym_calculation_parameters: dict
            The dict of calculation parameters for the nscfnosym part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        bse_ecutepss: list-like
            The list of ecuteps to test.
        bse_input_variables: dict
            The dict of input variables for the BSE part.
        bse_calculation_parameters: dict
            The dict of calculation parameters for the BSE part.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the BSE nband convergence sequence.
        use_scf_from: str, optional
            If not None, use the scf part of this part of the workflow.
        use_nscf_from: str, optional
            If not None, use the nscf part of this part of the workflow.
        use_nscfnosym_from: str, optional
            If not None, use the nscfnosym part of this part of the workflow.
        use_screening_from: str, optional
            If not None, use the screening part of this part of the workflow.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        use_converged_gw_kgrid: bool, optional
            If True, we use the converged ngkpt value for the BSE nband
            convergence.
        use_relaxed_geometry: bool, optional
            If True, we use the relaxed geometry.
        use_converged_bse_nband: bool, optional
            If True, we use the converged nband and bs_loband values for the
            BSE part of the sequence.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_bse_ecuteps_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.bse_ecuteps_convergence_sequencer
        # SCF PART
        use_gs = kwargs.pop("use_gs", False)
        use_scf_from = kwargs.pop("use_scf_from", None)
        if use_gs:
            if use_scf_from is None:
                use_scf_from = "gs"
            if use_scf_from != "gs":
                raise ValueError(
                        "'use_scf_from' != 'gs' while 'use_gs' is True.")
        use_converged_gw_kgrid = kwargs.pop(
                    "use_converged_gw_kgrid", False)
        await self._use_any_converged_calculation(
                seq, "scf",
                use_from=use_scf_from,
                default_workdir=os.path.join(root_workdir, "scf_run"),
                default_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                use_relaxed_geometry=kwargs.pop(
                    "use_relaxed_geometry", False),
                )
        # NSCF PART
        await self._use_any_converged_calculation(
                seq, "nscf",
                use_from=kwargs.pop("use_nscf_from", None),
                default_workdir=os.path.join(root_workdir, "nscf_run"),
                default_input_variables=kwargs.pop(
                    "nscf_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "nscf_calculation_parameters", None),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        # NSCF NO SYM PART
        await self._use_any_converged_calculation(
                seq, "nscfnosym",
                use_from=kwargs.pop("use_nscfnosym_from", None),
                default_workdir=os.path.join(root_workdir, "nscfnosym_run"),
                default_input_variables=kwargs.pop(
                    "nscfnosym_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "nscfnosym_calculation_parameters", None),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                use_converged_gw_ecuteps=kwargs.pop(
                    "use_converged_gw_ecuteps", False)
                )
        # SCREENING PART
        use_converged_gw_ecuteps = kwargs.pop(
                "use_converged_gw_ecuteps", False)
        await self._use_any_converged_calculation(
                seq, "screening",
                use_from=kwargs.pop("use_screening_from", None),
                default_workdir=os.path.join(root_workdir, "screening_run"),
                default_input_variables=kwargs.pop(
                    "screening_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "screening_calculation_parameters", None),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                use_converged_gw_ecuteps=use_converged_gw_ecuteps,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                )
        if use_converged_gw_ecuteps:
            if not await (
                    self.gw_ecuteps_convergence_sequencer.sequence_completed):
                self.stop_at_workflow = "gw_ecuteps_convergence"
                return
        # BSE PART
        seq.bse_workdir = os.path.join(root_workdir, "bse_run")
        self._add_input_variables(
                "bse", kwargs.pop("bse_input_variables", None),
                seq)
        self._add_calculation_parameters(
                "bse",
                kwargs.pop("bse_calculation_parameters", None),
                seq)
        # BSE tuneup
        if "bse_ecutepss" not in kwargs:
            raise ValueError("Need to set 'bse_ecutepss'.")
        seq.bse_ecutepss = kwargs.pop("bse_ecutepss")
        await self._use_converged_quantities(
                seq.bse_input_variables,
                sequencer=seq,
                use_converged_bse_nband=kwargs.pop(
                    "use_converged_bse_nband", False))
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "bse_ecuteps_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "bse_ecuteps_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_bse_nband_convergence(self, root_workdir=None, **kwargs):
        """Set the 'bse_nband_convergence' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the bse nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        nscfnosym_input_variables: dict
            The dict of input variables for the nscfnosym part.
        nscfnosym_calculation_parameters: dict
            The dict of calculation parameters for the nscfnosym part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        bse_lobands: list-like
            The list of bs_lobands to test.
        bse_nbands: list-like
            The list of nbands to test.
        bse_input_variables: dict
            The dict of input variables for the BSE part.
        bse_calculation_parameters: dict
            The dict of calculation parameters for the BSE part.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the BSE nband convergence sequence.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        use_converged_gw_kgrid: bool, optional
            If True, we use the converged ngkpt value for the BSE nband
            convergence.
        use_relaxed_geometry: bool, optional
            If True, we use the relaxed geometry.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_bse_nband_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.bse_nband_convergence_sequencer
        # SCF PART
        use_converged_gw_kgrid = kwargs.pop(
                    "use_converged_gw_kgrid", False)
        await self._use_converged_calculations(
                seq,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                use_relaxed_geometry=kwargs.pop("use_relaxed_geometry", False),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        if use_converged_gw_kgrid and not (
                await self.gw_kgrid_convergence_sequencer.sequence_completed):
            return
        # NON-SCF PART
        for seq_part in ("nscf_", "nscfnosym_", "screening_", "bse_"):
            setattr(seq, f"{seq_part}workdir", os.path.join(
                root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    seq)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    seq)
        # NSCF tuneup
        await self._use_converged_quantities(
                seq.nscf_input_variables,
                sequencer=seq,
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        # NSCF no syms tuneup
        await self._use_converged_quantities(
                seq.nscfnosym_input_variables,
                sequencer=seq,
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        # Screening tuneup
        await self._use_converged_quantities(
                seq.screening_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                use_converged_gw_ecuteps=kwargs.pop(
                    "use_converged_gw_ecuteps", False),
                )
        # BSE tuneup
        for var in ("bse_lobands", "bse_nbands"):
            if var not in kwargs:
                raise ValueError(f"Need to set '{var}'.")
        seq.bse_lobands = kwargs.pop("bse_lobands", None)
        seq.bse_nbands = kwargs.pop("bse_nbands", None)
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "bse_nband_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "bse_nband_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_bse_kgrid_convergence(
            self, root_workdir: str = None,
            scf_kgrids_input_variable_name: str = None,
            **kwargs) -> None:
        """Set the 'bse_kgrid_convergence' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the bse nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        scf_kgrids: list-like
            The list of ngkpt kpoint grids to test.
        scf_kgrids_input_variable_name: str,
            The input variable name to converge.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        nscfnosym_input_variables: dict
            The dict of input variables for the nscfnosym part.
        nscfnosym_calculation_parameters: dict
            The dict of calculation parameters for the nscfnosym part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        bse_input_variables: dict
            The dict of input variables for the BSE part.
        bse_calculation_parameters: dict
            The dict of calculation parameters for the BSE part.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the BSE nband convergence sequence.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        use_relaxed_geometry: bool, optional
            If True, we use the relaxed geometry.
        use_converged_bse_nband: bool, optional
            If True, we use the converged nband and bs_loband values for the
            BSE part of the sequence.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_bse_kgrid_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.bse_kgrid_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError("Need to set 'scf_kgrids_input_variable_name'.")
        seq.kgrids_input_variable_name = scf_kgrids_input_variable_name
        # SCF PART
        await self._use_converged_calculations(
                seq,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                use_converged_gs_ecut=kwargs.pop(
                    "use_converged_gs_ecut", False),
                use_relaxed_geometry=kwargs.pop("use_relaxed_geometry", False),
                )
        if "scf_kgrids" not in kwargs:
            raise ValueError("Need to set 'scf_kgrids'.")
        seq.scf_kgrids = kwargs.pop("scf_kgrids")
        # NON-SCF PART
        for seq_part in ("nscf_", "nscfnosym_", "screening_", "bse_"):
            setattr(seq, f"{seq_part}workdir", os.path.join(
                root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    seq)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    seq)
        # Screening tuneup
        await self._use_converged_quantities(
                seq.screening_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                use_converged_gw_ecuteps=kwargs.pop(
                    "use_converged_gw_ecuteps", False),
                )
        # BSE tuneup
        await self._use_converged_quantities(
                seq.bse_input_variables,
                sequencer=seq,
                use_converged_bse_nband=kwargs.pop(
                    "use_converged_bse_nband", False),
                use_converged_bse_ecuteps=kwargs.pop(
                    "use_converged_bse_ecuteps", False),
                )
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "bse_kgrid_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "bse_kgrid_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_dos(
            self, *args,
            dos_input_variables: dict[str, Any] = None,
            use_converged_gs_ecut_for_dos: bool = False,
            use_phonon_converged_ecut_for_dos: bool = False,
            **kwargs,
            ) -> None:
        """Sets the dos part of the workflow.

        Parameters
        ----------
        use_converged_gs_ecut_for_dos: bool, optional
            If True, will use the converged ecut from the convergence study in
            the dos calculation.
            'gs_ecut_convergence' must be set to True at init.
        use_phonon_converged_ecut_for_dos: bool, optional
            If True, will use the converged ecut from the convergence study in
            the dos calculation.
            'phonon_ecut_convergence' must be set to True at init.
        """
        if dos_input_variables is None:
            raise ValueError("Need to set 'dos_input_variables'.")
        await self._use_converged_quantities(
                dos_input_variables,
                use_converged_gs_ecut=use_converged_gs_ecut_for_dos,
                use_converged_phonon_ecut=use_phonon_converged_ecut_for_dos,
                )
        await super().set_dos(
                *args, dos_input_variables=dos_input_variables, **kwargs)

    async def set_gs_ecut_convergence(
            self, *args,
            scf_pawecutdg=None,
            scf_pawecutdg_convergence_criterion=None,
            **kwargs):
        """Sets the gs_ecut_convergence workflow.

        Parameters
        ----------
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        scf_pawecutdg_convergence_criterion: float, optional
            The convergence criterion for the pawecutdg convergence test
            in meV/at.
        scf_pawecutdg: list-like, optional
            The list of pawecutdg to use for the second convergence test.
        plot_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gs_ecut_convergence:
            return
        # start with the ecut convergence sequencer
        # do ecutdg afterwards in case of PAW
        seq1 = self.gs_ecut_convergence_sequencer
        seq1.ecuts_input_variable_name = "ecut"
        await super().set_gs_ecut_convergence(*args, **kwargs)
        # create another sequencer and make a list of sequencers
        # so that all convergence sequencers are executed
        if not self._is_paw_calculation(seq1.scf_input_variables):
            return
        if not await seq1.sequence_completed:
            return
        list_ = SequencersList([seq1])
        # calc is paw, add another convergence calculation for pawecutdg
        if scf_pawecutdg is None:
            raise ValueError("Need to set 'scf_pawecutdg'.")
        # make sure pawecutdg is present (test case for ecut) in first seq
        if "pawecutdg" not in seq1.scf_input_variables:
            raise ValueError(
                    "'pawecutdg' must be present as a test case for the "
                    "'ecut' convergence.")
        # pawecutdg must be > than all ecut checked
        if seq1.scf_input_variables["pawecutdg"] < max(seq1.scf_ecuts):
            raise ValueError("'pawecutdg' must be > than all ecuts checked.")
        seq2 = AbinitEcutConvergenceSequencer(loglevel=self._loglevel)
        seq2.ecuts_input_variable_name = "pawecutdg"
        # only consider pawecutdg that are higher than the converged ecut
        converged_ecut = await seq1.scf_converged_ecut
        scf_pawecutdg = ([converged_ecut] +
                         [x for x in scf_pawecutdg if x > converged_ecut])
        if not len(scf_pawecutdg):
            raise ValueError(
                    "Converged ecut is higher than all proposed pawecutdg.")
        pawecutdg_vars = seq1.scf_input_variables.copy()
        await self._use_converged_quantities(
                pawecutdg_vars, use_converged_gs_ecut=True)
        await super().set_gs_ecut_convergence(
                root_workdir=os.path.dirname(seq1.scf_workdir),
                scf_calculation_parameters=seq1.scf_calculation_parameters,
                scf_ecuts=scf_pawecutdg,
                scf_convergence_criterion=scf_pawecutdg_convergence_criterion,
                scf_input_variables=pawecutdg_vars,
                _sequencer=seq2,
                plot_calculation_parameters=(
                    seq1.plot_calculation_parameters.copy()))
        list_.append(seq2)
        self._gs_ecut_convergence_sequencer = list_

    async def set_gs_kgrid_convergence(
            self, *args, scf_kgrids_input_variable_name=None, **kwargs):
        """Sets the gs_kgrid_convergence part of the workflow.

        Parameters
        ----------
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.

        Note
        ----
        All other kwargs and args are passed to the mother's class method with
        the same name.
        """
        if not self._compute_gs_kgrid_convergence:
            return
        sequencer = self.gs_kgrid_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'scf_kgrids_input_variable_name'.")
        sequencer.kgrids_input_variable_name = scf_kgrids_input_variable_name
        await super().set_gs_kgrid_convergence(*args, **kwargs)

    async def set_gs_smearing_convergence(self, *args, **kwargs) -> None:
        """Sets the gs_smearing_convergence part of the workflow."""
        if not self._compute_gs_smearing_convergence:
            return
        subworkflow = AbinitGSSmearingConvergenceSubWorkflow(
                *args, sequencer=self.gs_smearing_convergence_sequencer,
                workflow=self, loglevel=self._loglevel, **kwargs,
                )
        await subworkflow.apply_converged_quantities()
        subworkflow.check()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_gw(
            self, root_workdir=None,
            **kwargs,
            ):
        """Set the GW part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the GW sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        selfenergy_input_variables: dict
            The dict of input variables for the self energy part.
        selfenergy_calculation_parameters: dict
            The dict of calculation parameters for the self energy part.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the GW sequence.
        use_converged_gw_self_energy_nband: bool, optional
            If True, we use the converged nband value for the GW
            self energy.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        use_converged_gw_kgrid: bool, optional
            If True, we use the converged ngkpt value for the GW
            sequence.
        """
        if not self._compute_gw:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.gw_sequencer
        # SCF PART
        use_converged_gw_kgrid = kwargs.pop(
                    "use_converged_gw_kgrid", False)
        await self._use_converged_calculations(
                self.gw_sequencer,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                use_relaxed_geometry=kwargs.pop("use_relaxed_geometry", False),
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        # NON-SCF PART
        for seq_part in ("nscf_", "screening_", "selfenergy_"):
            setattr(seq, f"{seq_part}workdir", os.path.join(
                root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    self.gw_sequencer)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    self.gw_sequencer)
        # NSCF tunup
        await self._use_converged_quantities(
                seq.nscf_input_variables,
                sequencer=seq,
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                )
        await self._use_converged_quantities(
                seq.selfenergy_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=kwargs.pop(
                    "use_converged_gw_self_energy_nband", False),
                use_converged_gw_screening_nband=False,
                )
        await self._use_converged_quantities(
                seq.screening_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                use_converged_gw_ecuteps=kwargs.pop(
                    "use_converged_gw_ecuteps", False),
                )
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_gw_kgrid_convergence(
            self,
            root_workdir: str = None,
            scf_kgrids_input_variable_name: str = None,
            **kwargs,
            ) -> None:
        """Set the GW kgrid convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the GW ecuteps convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        selfenergy_input_variables: dict
            The dict of input variables for the self energy part.
        selfenergy_calculation_parameters: dict
            The dict of calculation parameters for the self energy part.
        scf_kgric_convergence_criterion: float
            The scf ngkpt convergence criterion (in meV).
        scf_kgrids: list
            The list of kgrids to test.
        use_converged_gw_self_energy_nband: bool, optional
            If True, we use the converged nband value for the GW
            self energy.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        use_converged_gw_ecuteps: bool, optional
            If True, we use the converged ecuteps value for the GW
            screening.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gw_kgrid_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.gw_kgrid_convergence_sequencer
        if scf_kgrids_input_variable_name is None:
            raise ValueError("Need to set 'scf_kgrids_input_variable_name'.")
        seq.kgrids_input_variable_name = scf_kgrids_input_variable_name
        for seq_part in ("scf_", "nscf_", "screening_", "selfenergy_"):
            setattr(seq, f"{seq_part}workdir",
                    os.path.join(root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    seq)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    seq)
        # EXTRA SCF STUFF
        await self._use_converged_quantities(
                seq.scf_input_variables,
                sequencer=seq,
                **{x: kwargs.pop(x, False) for x in (
                    "use_converged_gs_ecut", "use_converged_gs_smearing",
                    "use_relaxed_geometry",
                    "use_converged_phonon_ecut",
                    "use_converged_phonon_smearing",
                    )})
        criterion = kwargs.pop("scf_kgrid_convergence_criterion", None)
        if criterion is None:
            raise ValueError(
                    "Need to set 'scf_kgrid_convergence_criterion'.")
        seq.scf_convergence_criterion = criterion
        kgrids = kwargs.pop("scf_kgrids", None)
        if kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        seq.scf_kgrids = kgrids
        await self._use_converged_quantities(
                seq.selfenergy_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=kwargs.pop(
                    "use_converged_gw_self_energy_nband", False),
                use_converged_gw_screening_nband=False,
                )
        await self._use_converged_quantities(
                seq.screening_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                use_converged_gw_ecuteps=kwargs.pop(
                    "use_converged_gw_ecuteps", False),
                )
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "gw_kgrid_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "gw_kgrid_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_gw_ecuteps_convergence(
            self, root_workdir=None,
            **kwargs,
            ):
        """Set the GW ecuteps convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the GW ecuteps convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        selfenergy_input_variables: dict
            The dict of input variables for the self energy part.
        selfenergy_calculation_parameters: dict
            The dict of calculation parameters for the self energy part.
        screening_ecuteps_convergence_criterion: float
            The screening nband convergence criterion (in meV).
        screening_ecutepss: list
            The list of screening ecutepss to test.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the GW sequence.
        use_nscf_from: str, optional
            If not None, use the nscf part of the given workflow part.
        use_converged_gw_self_energy_nband: bool, optional
            If True, we use the converged nband value for the GW
            self energy.
        use_converged_gw_screening_nband: bool, optional
            If True, we use the converged nband value for the GW
            screening.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gw_ecuteps_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.gw_ecuteps_convergence_sequencer
        # SCF PART
        await self._use_converged_calculations(
                seq,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                )
        # NSCF PART
        await self._use_any_converged_calculation(
                seq, "nscf", use_from=kwargs.pop("use_nscf_from", None),
                default_workdir=os.path.join(root_workdir, "nscf_run"),
                default_input_variables=kwargs.pop(
                    "nscf_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "nscf_calculation_parameters", None),
                )
        # screening/self-energy PART
        for seq_part in ("screening_", "selfenergy_"):
            setattr(seq, f"{seq_part}workdir",
                    os.path.join(root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    seq)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    seq)
        criterion = kwargs.pop("screening_ecuteps_convergence_criterion", None)
        if criterion is None:
            raise ValueError(
                    "Need to set 'screening_ecuteps_convergence_criterion'.")
        seq.screening_ecuteps_convergence_criterion = criterion
        ecutepss = kwargs.pop("screening_ecutepss", None)
        if ecutepss is None:
            raise ValueError("Need to set 'screening_ecutepss'.")
        seq.screening_ecutepss = ecutepss
        await self._use_converged_quantities(
                seq.selfenergy_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=kwargs.pop(
                    "use_converged_gw_self_energy_nband", False),
                use_converged_gw_screening_nband=False,
                )
        await self._use_converged_quantities(
                seq.screening_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=False,
                use_converged_gw_screening_nband=kwargs.pop(
                    "use_converged_gw_screening_nband", False),
                )
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "gw_ecuteps_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "gw_ecuteps_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_gw_screening_nband_convergence(
            self, root_workdir=None,
            **kwargs,
            ):
        """Set the GW screening nband convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the GW self energy nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        selfenergy_input_variables: dict
            The dict of input variables for the self energy part.
        selfenergy_calculation_parameters: dict
            The dict of calculation parameters for the self energy part.
        screening_nband_convergence_criterion: float
            The screening nband convergence criterion (in meV).
        screening_nbands: list
            The list of screening nbands to test.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the GW sequence.
        use_nscf_from: str, optional
            If not None, specify which nscf run to use from another part
            of the workflow (e.g.: 'gw_self_energy_nband_convergence').
        use_converged_gw_self_energy_nband: bool, optional
            If True, we use the converged nband value for the GW
            self energy.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gw_screening_nband_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.gw_screening_nband_convergence_sequencer
        # SCF PART
        await self._use_converged_calculations(
                seq,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                )
        # NSCF PART
        await self._use_any_converged_calculation(
                seq, "nscf",
                default_workdir=os.path.join(root_workdir, "nscf_run"),
                default_input_variables=kwargs.pop(
                    "nscf_input_variables", None),
                default_calculation_parameters=kwargs.pop(
                    "nscf_calculation_parameters", None),
                use_from=kwargs.pop("use_nscf_from", None),
                )
        # SCR/SELF PART
        for seq_part in ("screening_", "selfenergy_"):
            setattr(seq, f"{seq_part}workdir",
                    os.path.join(root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    seq)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    seq)
        criterion = kwargs.pop("screening_nband_convergence_criterion", None)
        if criterion is None:
            raise ValueError(
                    "Need to set 'screening_nband_convergence_criterion'.")
        seq.screening_nband_convergence_criterion = criterion
        nbands = kwargs.pop("screening_nbands", None)
        if nbands is None:
            raise ValueError("Need to set 'screening_nbands'.")
        seq.screening_nbands = nbands
        await self._use_converged_quantities(
                seq.selfenergy_input_variables,
                sequencer=seq,
                use_converged_gw_self_energy_nband=kwargs.pop(
                    "use_converged_gw_self_energy_nband", False),
                )
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "gw_screening_nband_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "gw_screening_band_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_gw_self_energy_nband_convergence(
            self, root_workdir=None,
            **kwargs,
            ):
        """Set the GW self energy nband convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where the GW self energy nband convergence sequence will run.
        scf_input_variables: dict
            The dict of input variables for the scf part.
        scf_calculation_parameters: dict
            The dict of calculation parameters for the scf part.
        nscf_input_variables: dict
            The dict of input variables for the nscf part.
        nscf_calculation_parameters: dict
            The dict of calculation parameters for the nscf part.
        screening_input_variables: dict
            The dict of input variables for the screening part.
        screening_calculation_parameters: dict
            The dict of calculation parameters for the screening part.
        selfenergy_input_variables: dict
            The dict of input variables for the self energy part.
        selfenergy_calculation_parameters: dict
            The dict of calculation parameters for the self energy part.
        selfenergy_nband_convergence_criterion: float
            The self energy nband convergence criterion (in meV).
        selfenergy_nbands: list
            The list of self energy nbands to test.
        use_gs: bool, optional
            If True, the gs part of the workflow is used as the scf part
            of the GW sequence.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gw_self_energy_nband_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq = self.gw_self_energy_nband_convergence_sequencer
        # SCF PART
        await self._use_converged_calculations(
                self.gw_self_energy_nband_convergence_sequencer,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                scf_input_variables=kwargs.pop(
                    "scf_input_variables", None),
                scf_calculation_parameters=kwargs.pop(
                    "scf_calculation_parameters", None),
                use_gs=kwargs.pop("use_gs", False),
                )
        # NON-SCF PART
        for seq_part in ("nscf_", "screening_", "selfenergy_"):
            setattr(seq, f"{seq_part}workdir",
                    os.path.join(root_workdir, f"{seq_part}run"))
            self._add_input_variables(
                    seq_part, kwargs.pop(f"{seq_part}input_variables", None),
                    self.gw_self_energy_nband_convergence_sequencer)
            self._add_calculation_parameters(
                    seq_part,
                    kwargs.pop(f"{seq_part}calculation_parameters", None),
                    self.gw_self_energy_nband_convergence_sequencer)
        criterion = kwargs.pop("selfenergy_nband_convergence_criterion", None)
        if criterion is None:
            raise ValueError(
                    "Need to set 'selfenergy_nband_convergence_criterion'.")
        seq = self.gw_self_energy_nband_convergence_sequencer
        seq.selfenergy_nband_convergence_criterion = criterion
        nbands = kwargs.pop("selfenergy_nbands", None)
        if nbands is None:
            raise ValueError("Need to set 'selfenergy_nbands'.")
        seq.selfenergy_nbands = nbands
        # PLOT PART
        plot_calculation_parameters = kwargs.pop(
                "plot_calculation_parameters", None)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                "gw_self_energy_nband_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                "gw_self_energy_nband_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)
        if kwargs:
            raise ValueError(
                    f"Some args were not taken into account {kwargs}")

    async def set_optic(
            self,
            root_workdir: str = None,
            experimental_bandgap: float = None,
            compute_non_linear_optical_response: bool = None,
            skip_nscffbz_calculation: bool = False,
            scf_input_variables: dict = None,
            scf_calculation_parameters: dict = None,
            nscf_input_variables: dict = None,
            nscf_calculation_parameters: dict = None,
            nscf_link_den_only: bool = False,
            nscffbz_calculation_parameters: dict = None,
            ddk_input_variables: dict = None,
            ddk_calculation_parameters: dict = None,
            optic_input_variables: dict = None,
            optic_calculation_parameters: dict = None,
            optic_force_kptopt: bool = False,
            use_gs: bool = False,
            use_converged_gs_ecut: bool = False,
            use_converged_gs_kgrid: bool = False,
            use_relaxed_geometry: bool = False,
            plot_calculation_parameters: bool = None,
            ) -> None:
        """Sets the optic calculations sequencer.

        Parameters
        ----------
        root_workdir: str
            The workdir where the sequencer will operate.
        experimental_bandgap: float, optional
            The experimental band gap in eV. If given, the scissor shift in
            optic will be automatically computed if the band structure is
            computed.
        compute_non_linear_optical_response: bool
            If True, the non linear optical response will be computed.
        scf_input_variables: dict, optional
            The scf input variables. If None, the ones from the GS calculation
            will be used.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters. If None, the ones from the GS
            calculation will be used.
        skip_nscffbz_calculation: bool, optional
            If True, the nscf calculation over full BZ is skipped.
            It is not otherwise (default).
        nscf_input_variables: dict
            The dict of nscf input variables.
        nscf_calculation_parameters: dict
            The dict of nscf calculation parameters.
        nscf_link_den_only: bool, optional
            If True, for the nscf part of calculation, only link DEN from scf
            instead of linking DEN + WFK.
        nscffbz_calculation_parameters: dict
            The dict of calculation parameters for the fbz nscf run.
        ddk_input_variables: dict
            The dict of ddk input variables.
        ddk_calculation_parameters: dict
            The dict of ddk calculation parameters.
        optic_input_variables: dict
            The dict of optic input variables.
        optic_calculation_parameters: dict
            The dict of optic calculation parameters.
        optic_force_kptopt: bool, optional
            If False (default), kptopt is automatically set if wrongly set.
            Otherwise, it is set only if it is not defined. Useful to
            test kpt generation using something else than regular grids.
        use_gs: bool, optional
            If True, the gs part of the workflow is used.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_optic:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        optic_seq = self.optic_sequencer
        if compute_non_linear_optical_response is None:
            raise ValueError(
                    "Need to set 'compute_non_linear_optical_response'.")
        cnlor = compute_non_linear_optical_response
        optic_seq.compute_non_linear_optical_response = cnlor
        optic_seq.force_kptopt = optic_force_kptopt
        optic_seq.skip_nscffbz_calculation = skip_nscffbz_calculation
        # SCF PART
        await self._use_converged_calculations(
                optic_seq, use_gs=use_gs,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_converged_gs_kgrid=use_converged_gs_kgrid,
                use_relaxed_geometry=use_relaxed_geometry,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"))
        # NSCF PART
        optic_seq.nscf_workdir = os.path.join(root_workdir, "nscf_run")
        if nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        optic_seq.nscf_input_variables = nscf_input_variables
        if nscf_calculation_parameters is None:
            raise ValueError("Need to set 'nscf_calculation_parameters'.")
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, optic_seq)
        optic_seq.nscf_link_den_only = nscf_link_den_only
        # nscf_fbz part
        optic_seq.nscffbz_workdir = os.path.join(root_workdir, "nscf_fbz_run")
        if not skip_nscffbz_calculation:
            if nscffbz_calculation_parameters is None:
                raise ValueError(
                        "Need to set 'nscffbz_calculation_parameters'.")
            self._add_calculation_parameters(
                "nscffbz_", nscffbz_calculation_parameters, optic_seq)
        # ddk part
        optic_seq.ddk_workdir = os.path.join(root_workdir, "ddk_run")
        if ddk_input_variables is None:
            raise ValueError("Need to set 'ddk_input_variables'.")
        optic_seq.ddk_input_variables = ddk_input_variables
        if ddk_calculation_parameters is None:
            raise ValueError("Need to set 'ddk_calculation_parameters'.")
        self._add_calculation_parameters(
                "ddk_", ddk_calculation_parameters, optic_seq)
        # optic part
        optic_seq.optic_workdir = os.path.join(root_workdir, "optic_run")
        if optic_input_variables is None:
            raise ValueError("Need to set 'optic_input_variables'.")
        optic_seq.optic_input_variables = optic_input_variables
        if experimental_bandgap is not None and (
                "scissor" not in optic_input_variables):
            if not self._compute_band_structure:
                raise ValueError(
                        "Need to compute band structure in order to compute "
                        "scissor shift.")
            if not await self.band_structure_sequencer.sequence_completed:
                self.stop_at_workflow = "band_structure"
                return
            computed_bandgap = await self.band_structure_sequencer.bandgap
            scissor = experimental_bandgap - computed_bandgap
            self._logger.info(
                    f"Computed bandgap is {computed_bandgap} eV while "
                    f"experimental bandgap set to {experimental_bandgap} eV. "
                    f"Scissor shift is therefore: {scissor} eV.")
            scissor *= EV_TO_HARTREE
            optic_seq.optic_input_variables["scissor"] = scissor
        if optic_calculation_parameters is None:
            raise ValueError("Need to set 'optic_calculation_parameters'.")
        self._add_calculation_parameters(
                "optic_", optic_calculation_parameters, optic_seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "optic.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "optic.pickle")
        plot_calculation_parameters["legend"] = True
        plot_calculation_parameters["legend_outside"] = True
        plot_calculation_parameters["show_legend_on"] = [[0, 0]]
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, optic_seq)

    async def set_optic_nscf_convergence(
            self, root_workdir=None, experimental_bandgap=None,
            compute_non_linear_optical_response=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            nscf_kpoint_grids=None,
            nscf_kpoint_grids_input_variable_name=None,
            nscf_input_variables=None,
            nscf_calculation_parameters=None,
            nscffbz_calculation_parameters=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            optic_input_variables=None, optic_calculation_parameters=None,
            optic_force_kptopt=False,
            use_gs=False,
            use_converged_gs_ecut=False,
            use_converged_gs_kgrid=False,
            use_relaxed_geometry=False,
            plot_calculation_parameters=None):
        """Sets the optic calculations sequencer.

        Parameters
        ----------
        root_workdir: str
            The workdir where the sequencer will operate.
        experimental_bandgap: float, optional
            The experimental band gap in eV. If given, the scissor shift in
            optic will be automaticallyu computed if the band structure is
            computed.
        compute_non_linear_optical_response: bool
            If True, the non linear optical response will be computed.
        scf_input_variables: dict, optional
            The scf input variables. If None, the ones from the GS calculation
            will be used.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters. If None, the ones from the GS
            calculation will be used.
        nscf_kpoint_grids: list-like
            The list of the nscf grids input variables values to test.
        nscf_kpoint_grids_input_variable_name: str
            The input variable name of the nscf grid parameter (e.g.: ngkpt).
        nscf_input_variables: dict
            The dict of nscf input variables.
        nscf_calculation_parameters: dict
            The dict of nscf calculation parameters.
        nscffbz_calculation_parameters: dict, optional
            The dict of calculation parameters for the fbz nscf run.
        ddk_input_variables: dict
            The dict of ddk input variables.
        ddk_calculation_parameters: dict
            The dict of ddk calculation parameters.
        optic_input_variables: dict
            The dict of optic input variables.
        optic_calculation_parameters: dict
            The dict of optic calculation parameters.
        optic_force_kptopt: bool, optional
            If False (default), kptopt is automatically set if wrongly set.
            Otherwise, it is set only if it is not defined. Useful to
            test kpt generation using something else than regular grids.
        use_gs: bool, optional
            If True, the gs part of the workflow is used.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_optic_nscf_convergence:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        if compute_non_linear_optical_response is None:
            raise ValueError(
                    "Need to set 'compute_non_linear_optical_response'.")
        if nscf_kpoint_grids is None:
            raise ValueError("Need to set 'nscf_kpoint_grids'.")
        if not is_list_like(nscf_kpoint_grids):
            raise TypeError(
                    "nscf_kpoint_grids must be list-like.")
        if nscf_kpoint_grids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'nscf_kpoint_grids_input_variable_name'.")
        if nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        if nscf_calculation_parameters is None:
            raise ValueError("Need to set 'nscf_calculation_parameters'.")
        if nscffbz_calculation_parameters is None:
            if compute_non_linear_optical_response is True:
                raise ValueError(
                        "Need to set 'nscffbz_calculation_parameters'.")
            nscffbz_calculation_parameters = {}
        if ddk_input_variables is None:
            raise ValueError("Need to set 'ddk_input_variables'.")
        if ddk_calculation_parameters is None:
            raise ValueError("Need to set 'ddk_calculation_parameters'.")
        if optic_input_variables is None:
            raise ValueError("Need to set 'optic_input_variables'.")
        if experimental_bandgap is not None and (
                "scissor" not in optic_input_variables):
            if not self._compute_band_structure:
                raise ValueError(
                        "Need to compute band structure in order to compute "
                        "scissor shift.")
            if not self.band_structure_sequencer.sequence_completed:
                self.stop_at_workflow = "band_structure"
                return
            computed_bandgap = self.band_structure_sequencer.bandgap
            scissor = experimental_bandgap - computed_bandgap
            scissor *= EV_TO_HARTREE
            self._logger.info(
                    f"Applying scissor shift of {scissor} Ha to match "
                    f"experimental band gap.")
            optic_input_variables["scissor"] = scissor
        if optic_calculation_parameters is None:
            raise ValueError("Need to set 'optic_calculation_parameters'.")
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["legend"] = True
        plot_calculation_parameters["legend_outside"] = True
        plot_calculation_parameters["show_legend_on"] = [[0, 0]]

        for igrid, nscf_grid in enumerate(nscf_kpoint_grids):
            seq = AbinitOpticSequencer(loglevel=self._loglevel)
            seq.compute_non_linear_optical_response = (
                    compute_non_linear_optical_response)
            seq.force_kptopt = optic_force_kptopt
            if is_2d_arr(nscf_grid):
                workdir = os.path.join(
                    root_workdir,
                    f"{nscf_kpoint_grids_input_variable_name}_{igrid}")
            else:
                workdir = os.path.join(
                        root_workdir,
                        f"{nscf_kpoint_grids_input_variable_name}_" +
                        "_".join([str(x) for x in nscf_grid]))
            # SCF PART
            await self._use_converged_calculations(
                seq, use_gs=use_gs,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_converged_gs_kgrid=use_converged_gs_kgrid,
                use_relaxed_geometry=use_relaxed_geometry,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(workdir, "scf_run"))
            # NSCF PART
            seq.nscf_workdir = os.path.join(workdir, "nscf_run")
            nscf_vars = nscf_input_variables.copy()
            nscf_vars.update(
                    {nscf_kpoint_grids_input_variable_name: nscf_grid})
            seq.nscf_input_variables = nscf_vars
            self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, seq)
            # NSCFFBZ_PART
            seq.nscffbz_workdir = os.path.join(workdir, "nscf_fbz_run")
            self._add_calculation_parameters(
                "nscffbz_", nscffbz_calculation_parameters, seq)
            # DDK PART
            seq.ddk_workdir = os.path.join(workdir, "ddk_run")
            ddk_vars = ddk_input_variables.copy()
            ddk_vars.update(
                    {nscf_kpoint_grids_input_variable_name: nscf_grid})
            seq.ddk_input_variables = ddk_vars
            self._add_calculation_parameters(
                "ddk_", ddk_calculation_parameters, seq)
            # OPTIC PART
            seq.optic_workdir = os.path.join(workdir, "optic_run")
            seq.optic_input_variables = optic_input_variables
            self._add_calculation_parameters(
                "optic_", optic_calculation_parameters, seq)
            # PLOT PART
            plot_params = plot_calculation_parameters.copy()
            plot_params["save"] = os.path.join(
                workdir, "results", "optic.pdf")
            plot_params["save_pickle"] = os.path.join(
                workdir, "results", "optic.pickle")
            self._add_calculation_parameters(
                "plot_", plot_params, seq)
            self.optic_nscf_convergence_sequencer.append(seq)

    async def set_phonon_ecut_convergence(
            self, *args,
            compute_electric_field_response=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            phonons_pawecutdg_convergence_criterion=None,
            root_workdir=None,
            scf_pawecutdg=None,
            **kwargs):
        """Sets the phonon_ecut_convergence workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (for LO-TO splitting) will be
            computed (Irrelevent for metals sor set to False).
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.
        phonons_pawecutdg_convergence_criterion: float, optional
            The convergence criterion for the pawecutdg convergence test
            in cm-1.
        root_workdir: str
            The root workdir where all calculations will be run.
        scf_pawecutdg: list-like, optional
            The list of pawecutdg to use for the second convergence test.
        """
        if not self._compute_phonon_ecut_convergence:
            return
        if compute_electric_field_response is None:
            raise ValueError("Need to set 'compute_electric_field_response'.")
        # start with the ecut convergence sequencer
        # do ecutdg afterwards in case of PAW
        seq1 = self.phonon_ecut_convergence_sequencer
        seq1.ecuts_input_variable_name = "ecut"
        seq1.compute_electric_field_response = compute_electric_field_response
        await super().set_phonon_ecut_convergence(
                *args, root_workdir=root_workdir, **kwargs)
        if compute_electric_field_response:
            seq1.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            if ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
            seq1.ddk_input_variables = ddk_input_variables
            if ddk_calculation_parameters is None:
                raise ValueError("Need to set 'ddk_calculation_parameters'.")
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, seq1)
        # create another sequencer and make a list of sequencers
        # so that all convergence sequencers are executed
        if not self._is_paw_calculation(seq1.scf_input_variables):
            return
        if not await seq1.sequence_completed:
            # if first calc not finished, wait for it
            return
        list_ = SequencersList([seq1])
        # calc is paw, add another convergence calculation for pawecutdg
        if scf_pawecutdg is None:
            raise ValueError("Need to set 'scf_pawecutdg'.")
        # make sure pawecutdg is present (test case for ecut) in first seq
        if "pawecutdg" not in seq1.scf_input_variables:
            raise ValueError(
                    "'pawecutdg' must be present as a test case for the "
                    "'ecut' convergence.")
        if phonons_pawecutdg_convergence_criterion is None:
            raise ValueError(
                    "Must set 'phonons_pawecutdg_convergence_criterion'."
                    )
        # pawecutdg must be > than all ecut checked
        if seq1.scf_input_variables["pawecutdg"] < max(seq1.scf_ecuts):
            raise ValueError("'pawecutdg' must be > than all ecuts checked.")
        seq2 = AbinitEcutPhononConvergenceSequencer(loglevel=self._loglevel)
        seq2.ecuts_input_variable_name = "pawecutdg"
        seq2.compute_electric_field_response = compute_electric_field_response
        converged_ecut = await seq1.scf_converged_ecut
        scf_pawecutdg = ([converged_ecut] +
                         [x for x in scf_pawecutdg
                          if x > converged_ecut[
                                seq1.ecuts_input_variable_name]])
        if not len(scf_pawecutdg):
            raise ValueError(
                    "Converged ecut is higher than all proposed pawecutdg.")
        # use converged ecut for pawecutdg convergence test
        pawecutdg_input_vars = seq1.scf_input_variables.copy()
        await self._use_converged_quantities(
                pawecutdg_input_vars, use_converged_phonon_ecut=True)
        # pop out old variables
        kwargs.pop("scf_input_variables")
        kwargs.pop("phonons_convergence_criterion")
        await super().set_phonon_ecut_convergence(
                *args, root_workdir=root_workdir,
                scf_input_variables=pawecutdg_input_vars,
                phonons_convergence_criterion=(
                    phonons_pawecutdg_convergence_criterion),
                _sequencer=seq2,
                **kwargs)
        if compute_electric_field_response:
            seq2.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            seq2.ddk_input_variables = ddk_input_variables
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, seq2)
        list_.append(seq2)
        self._phonon_ecut_convergence_sequencer = list_

    async def set_phonon_dispersion(
            self, *args,
            root_workdir: str = None,
            anaddb_input_variables: dict = None,
            anaddb_calculation_parameters: dict = None,
            compute_electric_field_response: bool = None,
            ddk_input_variables: dict = None,
            ddk_calculation_parameters: dict = None,
            mrgddb_calculation_parameters: dict = None,
            phonons_qgrid_generation_input_variable_name: str = None,
            **kwargs,
            ) -> None:
        """Sets the phonon_dispersion part of the workflow.

        Parameters
        ----------
        root_workdir: str
            Where all the calculations will be executed.
        compute_electric_field_response: bool
            If True, the electric field response (ddk) will be computed.
            Used to compute the LO-TO splitting.
        anaddb_input_variables: dict
            The dict of input variables for anaddb script.
        anaddb_calculation_parameters: dict
            The dict of calculation parameters for the anaddb script.
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.
        phonons_qgrid_generation_input_variable_name: str
            States the input variable name used to generate the qpoint grid.

        Notes
        -----
        Other kwargs are passed to the mother's class
        :meth:`method<.BaseWorkflow.set_phonon_dispersion>`.
        """
        if not self._compute_phonon_dispersion:
            return
        await super().set_phonon_dispersion(
                *args, root_workdir=root_workdir, **kwargs)
        if compute_electric_field_response is None:
            raise ValueError("Need to set 'compute_electric_field_response'.")
        # PHONONS PART
        if phonons_qgrid_generation_input_variable_name is None:
            raise ValueError(
                    "Need to set "
                    "'phonons_qgrid_generation_input_variable_name'.")
        sequencer = self.phonon_dispersion_sequencer
        sequencer.phonons_qgrid_generation_input_variable_name = (
                phonons_qgrid_generation_input_variable_name)
        # DDK PART
        sequencer.compute_electric_field_response = (
                compute_electric_field_response)
        if compute_electric_field_response:
            sequencer.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            if ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
            sequencer.ddk_input_variables = ddk_input_variables
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, sequencer)
        # MRGDDB PART
        sequencer.mrgddb_workdir = os.path.join(root_workdir, "mrgddb_run")
        self._add_calculation_parameters(
                "mrgddb_", mrgddb_calculation_parameters, sequencer)
        # ANADDB PART
        sequencer.anaddb_workdir = os.path.join(root_workdir, "anaddb_run")
        if anaddb_input_variables is None:
            raise ValueError("Need to set 'anaddb_input_variables'.")
        sequencer.anaddb_input_variables = anaddb_input_variables
        if phonons_qgrid_generation_input_variable_name != "ngkpt":
            if "ngqpt" not in anaddb_input_variables:
                raise ValueError(
                        "'ngkpt' was not used to define qpt grid. Then, user "
                        "need to define equivalent 'ngqpt' in anaddb input "
                        "variables.")
        self._add_calculation_parameters(
                "anaddb_", anaddb_calculation_parameters, sequencer)

    async def set_phonon_kgrid_convergence(
            self, *args,
            compute_electric_field_response=None,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            root_workdir=None,
            scf_kgrids_input_variable_name=None,
            **kwargs):
        """Sets the phonon kgrid convergence part of the workflow.

        Parameters
        ----------
        compute_electric_field_response: bool
            If True, the electric field response (for LO-TO splitting) will be
            computed (Irrelevent for metals sor set to False).
        ddk_input_variables: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk input variables.
        ddk_calculation_parameters: dict, optional
            In the case where 'compute_electric_field_response' is True,
            this specifies the dict of ddk calculation_parameters.
        root_workdir: str
            The root workdir for the scf and phonon calculations.
        scf_kgrids_input_variable_name: str
            The kgrid input variable name to test.

        Other parameters are passed to the mother's class method.
        """
        await super().set_phonon_kgrid_convergence(
                *args, root_workdir=root_workdir, **kwargs)
        if not self._compute_phonon_kgrid_convergence:
            return
        seq = self.phonon_kgrid_convergence_sequencer
        # SCF PART
        if scf_kgrids_input_variable_name is None:
            raise ValueError(
                    "Need to set 'scf_kgrids_input_variable_name'.")
        seq.kgrids_input_variable_name = scf_kgrids_input_variable_name
        # DDK PART
        if compute_electric_field_response:
            seq.compute_electric_field_response = (
                    compute_electric_field_response)
            seq.ddk_workdir = os.path.join(root_workdir, "ddk_runs")
            if ddk_input_variables is None:
                raise ValueError("Need to set 'ddk_input_variables'.")
            seq.ddk_input_variables = ddk_input_variables
            if ddk_calculation_parameters is None:
                raise ValueError("Need to set 'ddk_calculation_parameters'.")
            self._add_calculation_parameters(
                    "ddk_", ddk_calculation_parameters, seq)

    async def write_band_structure_convergence(self):
        """Write the band structure convergence part of the workflow."""
        await self.band_structure_convergence_sequencer.write()

    async def write_bse(self):
        """Write the 'bse' part of the workflow."""
        await self.bse_sequencer.write()

    async def write_bse_ecuteps_convergence(self):
        """Write the 'bse_ecuteps_convergence' part of the workflow."""
        await self.bse_ecuteps_convergence_sequencer.write()

    async def write_bse_nband_convergence(self):
        """Write the 'bse_nband_convergence' part of the workflow."""
        await self.bse_nband_convergence_sequencer.write()

    async def write_bse_kgrid_convergence(self):
        """Write the 'bse_kgrid_convergence' part of the workflow."""
        await self.bse_kgrid_convergence_sequencer.write()

    async def write_gw(self):
        """Write the 'gw' part of the flow."""
        await self.gw_sequencer.write()

    async def write_gw_kgrid_convergence(self):
        """Write the 'gw_kgrid_convergence' part of the flow."""
        await self.gw_kgrid_convergence_sequencer.write()

    async def write_gw_ecuteps_convergence(self):
        """Write the 'gw_ecuteps_convergence' part of the flow."""
        await self.gw_ecuteps_convergence_sequencer.write()

    async def write_gw_screening_nband_convergence(self):
        """Write the 'gw_screening_nband_convergence' part of the flow."""
        await self.gw_screening_nband_convergence_sequencer.write()

    async def write_gw_self_energy_nband_convergence(self):
        """Write the 'gw_self_energy_nband_convergence' part of the flow."""
        await self.gw_self_energy_nband_convergence_sequencer.write()

    async def write_optic(self):
        """Write the optic part of the workflow."""
        await self.optic_sequencer.write()

    async def write_optic_nscf_convergence(self):
        """Write the optic nscf convergence part of the workflow."""
        await self.optic_nscf_convergence_sequencer.write()

    def _is_paw_calculation(self, input_variables):
        """Returns True if vars given correspond to a PAW calculation."""
        if "pawecutdg" in input_variables:
            return True
        return False

    async def _post_process_dos(self):
        # if we compute band structure as well, we can show side-by-side
        # band structure plot with DOS plot!
        await super()._post_process_dos()
        # do the same with projected dos plots if any
        await self._create_merged_projected_band_structure_and_dos_plot()

    async def _create_merged_projected_band_structure_and_dos_plot(
            self) -> None:
        if not await self.dos_sequencer.sequence_completed:
            return
        if not await self.band_structure_sequencer.sequence_completed:
            return
        natom = 1
        coro_save = []
        coro_save_pickle = []
        bs_plot = self.band_structure_sequencer.band_structure_plot
        dos_plot = self.dos_sequencer.dos_plot
        show = self.dos_sequencer.plot_calculation_parameters.get(
                "show", True)
        save = self.dos_sequencer.plot_calculation_parameters.get(
                "save", None)
        for projected_dos_plot in self.dos_sequencer.projected_dos_plots:
            multi_plot = MultiPlot(loglevel=self._loglevel)
            projected_dos_plot.ylabel = ""
            multi_plot.add_plot(bs_plot, row=0)
            multi_plot.add_plot(projected_dos_plot, row=0)
            multi_plot.align_yaxes(0)
            self.dos_sequencer.apply_plot_calculation_parameters(multi_plot)
            multi_plot.plot(show=show, show_legend_on=[[0, 1]])
            if save is None:
                continue
            title = projected_dos_plot.title
            at = int(projected_dos_plot.title.split("=")[-2].split(" ")[0])
            natom = max([at, natom])
            l_idx = int(title.split("=")[-1])
            ext = f"_with_band_structure_projected_at{at}_l{l_idx}"
            save_pdf = save.rstrip(".pdf") + ext + ".pdf"
            save_pickle = save.rstrip(".pickle") + ext + ".pickle"
            coro_save.append(multi_plot.save(save_pdf, overwrite=True))
            coro_save_pickle.append(
                    multi_plot.save_pickle(save_pickle, overwrite=True))
        # also plot total DOS + partial DOS (l resolved) (for each atom)
        multi_plots = []
        colors = ["b", "r", "g", "m", "c"]
        for at in range(1, natom + 1):
            multi = MultiPlot(loglevel=self._loglevel)
            multi.add_plot(bs_plot, row=0)
            new_dos = Plot(loglevel=self._loglevel)
            new_dos.title = f"DOS atom={at}"
            new_dos.xlabel = dos_plot.xlabel
            curve = dos_plot.plot_objects["curves"][0]
            new_dos.add_curve(
                    curve.xdata, curve.ydata,
                    color="k", linewidth=2, label="Total DOS")
            multi.add_plot(new_dos, row=0)
            multi_plots.append(multi)
        for projected_dos_plot in self.dos_sequencer.projected_dos_plots:
            title = projected_dos_plot.title
            at = int(title.split("=")[-2].split(" ")[0])
            l_idx = int(title.split("=")[-1])
            total_projected_dos_this_l = (
                    projected_dos_plot.plot_objects["curves"][0].xdata)
            new_dos = multi_plots[at - 1].plot_rows[0][-1]
            new_dos.add_curve(
                    total_projected_dos_this_l, curve.ydata,
                    color=colors[l_idx], label=f"l={l_idx}", linewidth=2)
        for at, multi in enumerate(multi_plots):
            self.dos_sequencer.apply_plot_calculation_parameters(multi)
            multi.plot(show=show, show_legend_on=[[0, 1]])
            if save is None:
                continue
            ext = (f"_with_band_structure_projected_at{at + 1}"
                   "_l_resolved_only")
            save_pdf = save.rstrip(".pdf") + ext + ".pdf"
            save_pickle = save.rstrip(".pickle") + ext + ".pickle"
            coro_save.append(multi.save(save_pdf, overwrite=True))
            coro_save_pickle.append(
                    multi.save_pickle(save_pickle, overwrite=True))
        await asyncio.gather(*coro_save)
        await asyncio.gather(*coro_save_pickle)

    # TODO: DRY this with the ibte convergence parts of QEWorkflow
    # These are exactly the same!!!!!
    async def _post_process_optic_nscf_convergence(self):
        # make a global result directory and put all separated convergence plot
        all_imag_plots = []
        all_real_plots = []
        nscf_grids = []
        seqs = self.optic_nscf_convergence_sequencer
        colors = rand_cmap(len(seqs))
        for sequencer in seqs:
            results_dir = os.path.dirname(sequencer.plot_save_pickle)
            nscf_grid_dirname = os.path.basename(os.path.dirname(results_dir))
            nscf_grid_varname = nscf_grid_dirname.split("_")[0]
            grid = nscf_grid_dirname.split("_")[1:]
            if len(grid) == 1:
                # nscf grid was not a simple list (like for ngkpt)
                nscf_grids.append(grid)
            else:
                nscf_grids.append([int(x) for x in grid])
            for filename in await aiofiles.os.listdir(results_dir):
                if ".pickle" not in filename:
                    continue
                if "imag" in filename.lower():
                    all_imag_plots.append(
                            Plot.load_plot(
                                os.path.join(results_dir, filename)))
                elif "real" in filename.lower():
                    all_real_plots.append(
                            Plot.load_plot(
                                os.path.join(results_dir, filename)))
        show = sequencer.plot_calculation_parameters.get("show", True)
        main_results_dir = os.path.join(
                os.path.dirname(os.path.dirname(sequencer.optic_workdir)),
                "results")
        coro_save = []
        coro_save_pickle = []
        for plot_container, tag in zip(
                [all_imag_plots, all_real_plots],
                ["imag", "real"],
                ):
            curves = plot_container[0].plot_objects["curves"]
            for i, curve in enumerate(curves):
                final_plot = Plot(loglevel=self._loglevel)
                element = curve.label.split("{")[-1]
                element = element.split("}")[0]
                for (iplot, plot), nscf_grid in zip(
                        enumerate(plot_container), nscf_grids):
                    curve = plot.plot_objects["curves"][i]
                    label = nscf_grid_varname + " "
                    if is_list_like(nscf_grid):
                        label += "x".join([str(x) for x in nscf_grid])
                    else:
                        label += nscf_grid
                    final_plot.add_curve(
                            curve.xdata, curve.ydata,
                            color=colors(iplot / len(plot_container)),
                            linestyle=curve.linestyle,
                            linewidth=curve.linewidth,
                            label=label)
                final_plot.xlabel = plot.xlabel
                final_plot.ylabel = plot.ylabel
                final_plot.title = (
                        f"{tag} part of dielectric tensor nscf "
                        f"grid convergence"
                        f" {element} component")
                final_plot.legend = True
                final_plot.grid = True
                final_plot.plot(show=show)
                coro_save.append(final_plot.save(
                        os.path.join(
                            main_results_dir,
                            (f"dielectric_tensor_nscf_grid_convergence_"
                             f"{tag}_part"
                             f"_{element}.pdf")),
                        overwrite=True,
                        ))
                coro_save_pickle.append(final_plot.save_pickle(
                        os.path.join(
                            main_results_dir,
                            (f"dielectric_tensor_nscf_grid_convergence_"
                             f"{tag}_part"
                             f"_{element}.pickle")),
                        overwrite=True,
                        ))
        await asyncio.gather(*coro_save)
        await asyncio.gather(*coro_save_pickle)

    async def _use_converged_calculations(
            self, sequencer, *args,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_workdir=None,
            use_converged_gw_kgrid=False,
            use_converged_bse_kgrid=False,
            use_converged_phonon_kgrid=False,
            use_gs=False,
            **kwargs,
            ):
        await super()._use_converged_calculations(
                sequencer, *args, use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=scf_workdir, **kwargs)
        if use_gs:
            if use_converged_gw_kgrid or use_converged_bse_kgrid:
                raise ValueError(
                    "'use_gs' cannot be used in combination with 'use_converge"
                    "d_*_ngkpt'.")
            return
        await self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer,
                use_converged_gw_kgrid=use_converged_gw_kgrid,
                use_converged_bse_kgrid=use_converged_bse_kgrid,
                use_converged_phonon_kgrid=use_converged_phonon_kgrid,
                )

    async def _use_converged_quantities(
            self, input_variables,
            sequencer=None,
            use_converged_gw_self_energy_nband=False,
            use_converged_gw_screening_nband=False,
            use_converged_gw_ecuteps=False,
            use_converged_gw_kgrid=False,
            use_converged_bse_nband=False,
            use_converged_bse_ecuteps=False,
            use_converged_bse_kgrid=False,
            use_converged_phonon_kgrid=False,
            **kwargs):
        await super()._use_converged_quantities(
                input_variables, sequencer=sequencer, **kwargs)
        if use_converged_gw_self_energy_nband:
            await self._use_converged_gw_self_energy_nband(input_variables)
        if use_converged_gw_screening_nband:
            self._use_converged_gw_screening_nband(
                    input_variables)
        if use_converged_gw_ecuteps:
            self._use_converged_gw_ecuteps(input_variables)
        if use_converged_gw_kgrid:
            self._use_converged_gw_kgrid(input_variables)
        if use_converged_bse_nband:
            if self.converged_bse_nband is None:
                raise ValueError(
                            "Need to set 'converged_bse_nband' upon init.")
            input_variables.update(self.converged_bse_nband)
        if use_converged_bse_ecuteps:
            if self.converged_bse_ecuteps is None:
                raise ValueError(
                            "Need to set 'converged_bse_ecuteps' upon "
                            "init.")
            input_variables.update(self.converged_bse_ecuteps)
        if use_converged_bse_kgrid:
            if self.converged_bse_kgrid is None:
                raise ValueError(
                            "Need to set 'converged_bse_kgrid' upon "
                            "init.")
            input_variables.update(self.converged_bse_kgrid)
        if use_converged_phonon_kgrid is None:
            self._use_converged_phonon_kgrid(input_variables)

    async def _use_converged_gw_self_energy_nband(self, input_variables):
        if not self._compute_gw_self_energy_nband_convergence:
            if self.converged_gw_self_energy_nband is None:
                raise ValueError(
                        "Need to set 'gw_self_energy_nband_convergence' to"
                        " True or 'converged_gw_self_energy_nband' upon "
                        "init.")
            input_variables.update(self.converged_gw_self_energy_nband)
        else:
            seq = self.gw_self_energy_nband_convergence_sequencer
            if not seq.sequence_completed:
                self.stop_at_workflow = "gw_self_energy_nband_convergence"
            else:
                input_variables.update(await seq.converged_self_energy_nband)

    def _use_converged_gw_screening_nband(self, input_variables):
        if not self._compute_gw_screening_nband_convergence:
            if self.converged_gw_screening_nband is None:
                raise ValueError(
                        "Need to set 'gw_screening_nband_convergence' to"
                        " True or 'converged_gw_screening_nband' upon "
                        "init.")
            input_variables.update(self.converged_gw_screening_nband)
        else:
            seq = self.gw_screening_nband_convergence_sequencer
            if not seq.sequence_completed:
                self.stop_at_workflow = "gw_screening_nband_convergence"
            else:
                input_variables.update(seq.converged_screening_nband)

    def _use_converged_gw_ecuteps(self, input_variables):
        if not self._compute_gw_ecuteps_convergence:
            if self.converged_gw_ecuteps is None:
                raise ValueError(
                        "Need to set 'gw_ecuteps_convergence' to"
                        " True or 'converged_gw_ecuteps' upon "
                        "init.")
            input_variables.update(self.converged_gw_ecuteps)
        else:
            seq = self.gw_ecuteps_convergence_sequencer
            if not seq.sequence_completed:
                self.stop_at_workflow = "gw_ecuteps_convergence"
            else:
                input_variables.update(seq.converged_ecuteps)

    def _use_converged_gw_kgrid(self, input_variables):
        if not self._compute_gw_kgrid_convergence:
            if self.converged_gw_kgrid is None:
                raise ValueError(
                        "Need to set 'gw_kgrid_convergence' to"
                        " True or 'converged_gw_kgrid' upon "
                        "init.")
            input_variables.update(self.converged_gw_kgrid)
        else:
            seq = self.gw_kgrid_convergence_sequencer
            if not seq.sequence_completed:
                self.stop_at_workflow = "gw_kgrid_convergence"
            else:
                input_variables.update(seq.converged_kgrid)

    def _use_converged_phonon_kgrid(self, input_variables):
        if not self._compute_phonon_kgrid_convergence:
            if self.converged_phonon_kgrid is None:
                raise ValueError(
                        "Need to set 'phonon_kgrid_convergence' to"
                        " True or 'converged_phonon_kgrid' upon "
                        "init.")
            input_variables.update(self.converged_phonon_kgrid)
        else:
            seq = self.phonon_kgrid_convergence_sequencer
            if not seq.sequence_completed:
                self.stop_at_workflow = "phonon_kgrid_convergence"
            else:
                input_variables.update(seq.converged_phonon_kgrid)
