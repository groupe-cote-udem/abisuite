import abc
import asyncio
import os
from typing import Any, Sequence

from async_property import async_property

from ..bases import BaseUtility
from ..colors import Colors
from ..exceptions import DevError
from ..handlers import CalculationDirectory, is_calculation_directory
from ..launchers.exceptions import TooManyCalculationsInQueueError
from ..plotters import MultiPlot
from ..routines import is_list_like
from ..sequencers.bases import BaseSequencer
from ..sequencers.exceptions import (
        SequenceNotCompletedError,
        SequenceNotConvergedError,
        )


class BaseWorkflow(BaseUtility, abc.ABC):
    """Base class for workflows.

    All functionalities implemented in this base
    class are implemented in subclasses as well.

    For now supports:
      - GS ecut convergence
      - GS kgrid convergence
      - GS smearing convergence
      - Lattice + Atomic relaxation
      - Phonon frequencies ecut convergence
      - Phonon frequencies kgrid convergence
      - Phonon frequencies smearing convergence
      - GS calculation
      - Band Structure calculation
      - DOS calculation
      - Fermi Surface calculation
      - Phonon dispersion qpoint grid convergence
      - :meth:`phonon_dispersion<.set_phonon_dispersion>`

    Note
    ----
    This class is abstract which means it cannot be instanciated directly.
    Use the corresponding subclass for the software you want to use.
    """

    _all_geometry_variables = None
    _band_structure_sequencer_cls = None
    _band_structure_subworkflow_cls = None
    _dos_sequencer_cls = None
    _fermi_surface_sequencer_cls = None
    _gs_sequencer_cls = None
    _gs_subworkflow_cls = None
    _gs_ecut_convergence_sequencer_cls = None
    _gs_kgrid_convergence_sequencer_cls = None
    _gs_smearing_convergence_sequencer_cls = None
    _phonon_ecut_convergence_sequencer_cls = None
    _phonon_kgrid_convergence_sequencer_cls = None
    _phonon_smearing_convergence_sequencer_cls = None
    _phonon_smearing_convergence_subworkflow_cls = None
    _phonon_dispersion_sequencer_cls = None
    _phonon_dispersion_qgrid_convergence_subworkflow_cls = None
    _phonon_dispersion_qgrid_convergence_post_processor_cls = None
    _relaxation_sequencer_cls = None
    workflow = None
    _base_implemented_workflows = (
            "gs_ecut_convergence", "gs_kgrid_convergence",
            "gs_smearing_convergence", "relaxation",
            "phonon_ecut_convergence", "phonon_kgrid_convergence",
            "phonon_smearing_convergence", "gs", "band_structure",
            "dos", "fermi_surface",
            "phonon_dispersion_qgrid_convergence", "phonon_dispersion",
            )

    def __init__(
            self,
            band_structure=False,
            # bypass_convergence_check=False,
            # bypass_sequence_comparison=False,
            converged_gs_ecut=None,
            converged_gs_kgrid=None,
            converged_gs_smearing=None,
            converged_phonon_ecut=None,
            converged_phonon_kgrid=None,
            converged_phonon_smearing=None,
            fermi_surface=False,
            dos=False,
            gs=False,
            gs_ecut_convergence=False,
            gs_kgrid_convergence=False,
            gs_smearing_convergence=False,
            phonon_ecut_convergence=False,
            phonon_kgrid_convergence=False,
            phonon_dispersion=False,
            phonon_dispersion_qgrid_convergence=False,
            phonon_smearing_convergence=False,
            relaxation=False,
            relaxed_geometry=None,
            **kwargs):
        """Workflow init method.

        Parameters
        ----------
        band_structure: bool, optional
            If True, will compute the band structure.
        converged_gs_ecut: dict, optional
            The dict of variables representing the converged ecut parameters.
            These variables are used if the gs_ecut_convergence is not
            computed.
        converged_gs_kgrid: dict, optional
            The dict of variables representing the converged kgrid parameters.
            These variables are used if the gs_kgrid_convergence is not
            computed.
        converged_gs_smearing: dict, optional
            The dict of variables representing
            the converged smearing parameters for gs.
            These variables are used
            if gs_smearing_convergence is not computed.
        converged_phonon_ecut: dict, optional
            The dict of variables representing
            the converged ecut parameters for phonons.
            These variables are used
            if phonon_ecut_convergence is not computed.
        converged_phonon_kgrid: dict, optional
            The dict of variables representing
            the converged kgrid parameters for phonons.
            These variables are used
            if phonon_kgrid_convergence is not computed.
        converged_phonon_smearing: dict, optional
            The dict of variables representing
            the converged smearing parameters for phonons.
            These variables are used
            if phonon_smearing_convergence is not computed.
        dos: bool, optional
            If True, the dos will be computed.
        fermi_surface: bool, optional
            If True, fermi surface calculation will be done in order to
            plot fermi surface.
        gs: bool, optional
            If True, requires to compute a GS calculation.
        gs_ecut_convergence: bool, optional
            If True, the ecut convergence with respect to etot will be
            inspected.
        gs_kgrid_convergence: bool, optional
            If True, the kgrid convergence with respect to etot will
            be inspected.
        gs_smearing_convergence: bool, optional
            If True, the smearing convergence will be done.
        phonon_ecut_convergence: bool, optional
            If True, the phonon ecut convergence is added to the workflow.
        phonon_kgrid_convergence: bool, optional
            If True, the phonon kgrid convergence is added to the workflow.
        phonon_dispersion: bool, optional
            If True, the phonon dispersion is added to the workflow.
        phonon_smearing_convergence: bool, optional
            If True, the phonon smearing convergence is added to the workflow.
        phonon_dispersion_qgrid_convergence: bool, optional
            If True, the phonon dispersion qgrid convergence is added to the
            workflow.
        relaxation: bool, optional
            If True, the atomic / bond lengths will be computed. This will
            set the geometry for all calculations if not specified.
        relaxed_geometry: dict, optional
            The dict of variables representing the relaxed geometry parameters.
            These variables are used if the relaxation is not
            computed.
        """
        # FG: 2021/04/30 I removed these global parameters in profit of fine
        # tuning them on each sequencers
        # bypass_convergence_check: bool, optional
        #     If True, we don't care if a calculation in the Workflow
        #     is not converged. Otherwise errors are thrown whenever
        #     calculations are not converged.
        # bypass_sequence_comparison: bool, optional
        #     If True, the sequence calculations comparisons is bypassed in
        #     order to get results faster. Useful when all calculations are
        #     successfull and we don't want to check that everything is the
        #     same each time we rerun the Workflow.
        # self.bypass_convergence_check = bypass_convergence_check
        # self.bypass_sequence_comparison = bypass_sequence_comparison
        super().__init__(**kwargs)
        # GENERAL PART
        if self.workflow is None:
            raise DevError("'workflow cls attr must be defined.")
        for workflow in self._base_implemented_workflows:
            if workflow not in self.workflow:
                raise DevError(
                        f"'{workflow}' is implemented in base class but is not"
                        f" in '{self.__class__}'.")
        self._stop_at_workflow = None
        # GS ECUT CONVERGENCE PART
        if self._gs_ecut_convergence_sequencer_cls is None:
            raise DevError("Need to set '_gs_ecut_convergence_sequencer_cls'.")
        if self._gs_subworkflow_cls is None:
            raise DevError("Need to set '_gs_subworkflow_cls'.")
        self._compute_gs_ecut_convergence = gs_ecut_convergence
        self._gs_ecut_convergence_sequencer = None
        self.converged_gs_ecut = converged_gs_ecut
        # GS KGRID CONVERGENCE PART
        if self._gs_kgrid_convergence_sequencer_cls is None:
            raise DevError(
                    "Need to set '_gs_kgrid_convergence_sequencer_cls'.")
        self._compute_gs_kgrid_convergence = gs_kgrid_convergence
        self._gs_kgrid_convergence_sequencer = None
        self.converged_gs_kgrid = converged_gs_kgrid
        # GS SMEARING CONVERGENCE PART
        if self._gs_smearing_convergence_sequencer_cls is None:
            raise DevError(
                "Need to set '_gs_smearing_convergence_sequencer_cls'.")
        self._compute_gs_smearing_convergence = gs_smearing_convergence
        self.converged_gs_smearing = converged_gs_smearing
        self._gs_smearing_convergence_sequencer = None
        # RELAXATION PART
        if self._relaxation_sequencer_cls is None:
            raise DevError("'_relaxation_sequencer_cls' need to be specified.")
        if self._all_geometry_variables is None:
            raise DevError("'_all_geometry_variables' needs to be specified.")
        self._compute_relaxation = relaxation
        self._relaxation_sequencer = None
        self.relaxed_geometry = relaxed_geometry
        # PHONON ECUT CONVERGENCE PART
        if self._phonon_ecut_convergence_sequencer_cls is None:
            raise DevError(
                    "'_phonon_ecut_convergence_sequencer_cls' needs to be"
                    "specified.")
        self._compute_phonon_ecut_convergence = phonon_ecut_convergence
        self._phonon_ecut_convergence_sequencer = None
        self.converged_phonon_ecut = converged_phonon_ecut
        # PHONON KGRID CONVERGENCE PART
        if self._phonon_kgrid_convergence_sequencer_cls is None:
            raise DevError(
                    "'_phonon_kgrid_convergence_sequencer_cls' needs to be "
                    "specified.")
        self._compute_phonon_kgrid_convergence = phonon_kgrid_convergence
        self._phonon_kgrid_convergence_sequencer = None
        self.converged_phonon_kgrid = converged_phonon_kgrid
        # PHONON SMEARING CONVERGENCE PART
        if self._phonon_smearing_convergence_sequencer_cls is None:
            raise DevError(
                    "'_phonon_smearing_convergence_sequencer_cls' needs to be "
                    "specified.")
        self._compute_phonon_smearing_convergence = phonon_smearing_convergence
        self._phonon_smearing_convergence_sequencer = None
        self.converged_phonon_smearing = converged_phonon_smearing
        if self._phonon_smearing_convergence_subworkflow_cls is None:
            raise DevError(
                    "'_phonon_smearing_convergence_subworkflow_cls' "
                    "must be sepicified.")
        # GS PART
        if self._gs_sequencer_cls is None:
            raise DevError("'_gs_sequencer_cls' need to be specified.")
        self._compute_gs = gs
        self._gs_sequencer = None
        # BAND STRUCTURE PART
        if self._band_structure_sequencer_cls is None:
            raise DevError(
                    "'_band_structure_sequencer_cls' must be specified.")
        if self._band_structure_subworkflow_cls is None:
            raise DevError(
                    "'_band_structure_subworkflow_cls' must be sepicified.")
        self._compute_band_structure = band_structure
        self._band_structure_sequencer = None
        # DOS
        if self._dos_sequencer_cls is None:
            raise DevError("'_dos_sequencer_cls' must be set.")
        self._compute_dos = dos
        self._dos_sequencer = None
        # FERMI SURFACE PART
        if self._fermi_surface_sequencer_cls is None:
            raise DevError(
                    "'_fermi_surface_sequencer_cls' must be specified.")
        self._compute_fermi_surface = fermi_surface
        self._fermi_surface_sequencer = None
        # PHONON DISPERSION QGRID CONVERGENCE PART
        self._compute_phonon_dispersion_qgrid_convergence = (
                phonon_dispersion_qgrid_convergence)
        self._phonon_dispersion_qgrid_convergence_sequencer = None
        if self._phonon_dispersion_qgrid_convergence_subworkflow_cls is None:
            raise DevError(
                    "'_phonon_dispersion_qgrid_convergence_subworkflow_cls' "
                    "need to be specified.")
        cls = self._phonon_dispersion_qgrid_convergence_post_processor_cls
        if cls is None:
            raise DevError(
                    "'_phonon_dispersion_qgrid_convergence_post_processor_cls'"
                    " need to be specified.")
        # PHONON DISPERSION PART
        if self._phonon_dispersion_sequencer_cls is None:
            raise DevError(
                    "'_phonon_dispersion_sequencer_cls' must be specified.")
        self._compute_phonon_dispersion = phonon_dispersion
        self._phonon_dispersion_sequencer = None

    @property
    def all_sequencers(self):
        """Return the list of all sequencers objects of the current workflow.

        Returns
        -------
        SequencersList instance.
        """
        sequencers = SequencersList([])
        for workflow in self.workflow:
            if getattr(self, f"_compute_{workflow}"):
                sequencers.append(getattr(self, f"{workflow}_sequencer"))
        return sequencers

    @property
    def band_structure_sequencer(self):
        """Return the band_structure sequencer workflow."""
        if self._band_structure_sequencer is not None:
            return self._band_structure_sequencer
        self._band_structure_sequencer = self._band_structure_sequencer_cls(
                loglevel=self._loglevel)
        return self.band_structure_sequencer

    @property
    def dos_sequencer(self):
        """Return the DOS sequencer."""
        if self._dos_sequencer is not None:
            return self._dos_sequencer
        self._dos_sequencer = self._dos_sequencer_cls(
                loglevel=self._loglevel)
        return self.dos_sequencer

    @property
    def fermi_surface_sequencer(self):
        """Return the fermi surface sequencer."""
        if self._fermi_surface_sequencer is not None:
            return self._fermi_surface_sequencer
        self._fermi_surface_sequencer = self._fermi_surface_sequencer_cls(
                loglevel=self._loglevel)
        return self._fermi_surface_sequencer

    @property
    def gs_sequencer(self):
        """Return the gs sequencer."""
        if self._gs_sequencer is not None:
            return self._gs_sequencer
        self._gs_sequencer = self._gs_sequencer_cls(loglevel=self._loglevel)
        return self.gs_sequencer

    @property
    def gs_ecut_convergence_sequencer(self):
        """Retyrb the gs ecut convergence sequencer."""
        if self._gs_ecut_convergence_sequencer is not None:
            return self._gs_ecut_convergence_sequencer
        seq = self._gs_ecut_convergence_sequencer_cls(loglevel=self._loglevel)
        self._gs_ecut_convergence_sequencer = seq
        return self.gs_ecut_convergence_sequencer

    @property
    def gs_kgrid_convergence_sequencer(self):
        """Return the gs kgrid convergence sequencer."""
        if self._gs_kgrid_convergence_sequencer is not None:
            return self._gs_kgrid_convergence_sequencer
        sequencer = self._gs_kgrid_convergence_sequencer_cls(
                loglevel=self._loglevel)
        self._gs_kgrid_convergence_sequencer = sequencer
        return self.gs_kgrid_convergence_sequencer

    @property
    def gs_smearing_convergence_sequencer(self):
        """Return the gs smearing convergence sequencer."""
        if self._gs_smearing_convergence_sequencer is not None:
            return self._gs_smearing_convergence_sequencer
        seq = self._gs_smearing_convergence_sequencer_cls(
                loglevel=self._loglevel)
        self._gs_smearing_convergence_sequencer = seq
        return self.gs_smearing_convergence_sequencer

    @property
    def phonon_ecut_convergence_sequencer(self):
        """Return the phonon ecut convergence sequencer."""
        if self._phonon_ecut_convergence_sequencer is not None:
            return self._phonon_ecut_convergence_sequencer
        self._phonon_ecut_convergence_sequencer = (
                self._phonon_ecut_convergence_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_ecut_convergence_sequencer

    @property
    def phonon_dispersion_sequencer(self):
        """Return the phonon dispersion sequencer."""
        if self._phonon_dispersion_sequencer is not None:
            return self._phonon_dispersion_sequencer
        self._phonon_dispersion_sequencer = (
                self._phonon_dispersion_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_dispersion_sequencer

    @property
    def phonon_dispersion_qgrid_convergence_sequencer(self):
        """Return the phonon qgrid convergence sequencer."""
        if self._phonon_dispersion_qgrid_convergence_sequencer is not None:
            return self._phonon_dispersion_qgrid_convergence_sequencer
        self._phonon_dispersion_qgrid_convergence_sequencer = (
                SequencersList([], loglevel=self._loglevel))
        return self.phonon_dispersion_qgrid_convergence_sequencer

    @property
    def phonon_kgrid_convergence_sequencer(self):
        """Return the phonon kgrid convergence sequencer."""
        if self._phonon_kgrid_convergence_sequencer is not None:
            return self._phonon_kgrid_convergence_sequencer
        self._phonon_kgrid_convergence_sequencer = (
                self._phonon_kgrid_convergence_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_kgrid_convergence_sequencer

    @property
    def phonon_smearing_convergence_sequencer(self):
        """Return the phonon smearing convergence sequencer."""
        if self._phonon_smearing_convergence_sequencer is not None:
            return self._phonon_smearing_convergence_sequencer
        self._phonon_smearing_convergence_sequencer = (
                self._phonon_smearing_convergence_sequencer_cls(
                    loglevel=self._loglevel))
        return self.phonon_smearing_convergence_sequencer

    @property
    def relaxation_sequencer(self):
        """Return the relaxation sequencer."""
        if self._relaxation_sequencer is not None:
            return self._relaxation_sequencer
        self._relaxation_sequencer = self._relaxation_sequencer_cls(
                loglevel=self._loglevel)
        return self.relaxation_sequencer

    @property
    def stop_at_workflow(self):
        """Return where the workflow will stop at."""
        return self._stop_at_workflow

    @stop_at_workflow.setter
    def stop_at_workflow(self, stop):
        """Set where the Workflow will stop."""
        if stop is None:
            # reset
            self._stop_at_workflow = None
            return
        if stop not in self.workflow:
            raise ValueError(f"'{stop}' not in workflow.")
        # get index number for current stop. if new stop is after previous
        # stop we don't change. otherwise we set new stop
        if self.stop_at_workflow is None:
            # no previous stop set this one
            self._stop_at_workflow = stop
            return
        if self.stop_at_workflow == stop:
            # same
            return
        previous_stop_idx = list(self.workflow).index(self.stop_at_workflow)
        new_stop_idx = list(self.workflow).index(stop)
        if new_stop_idx < previous_stop_idx:
            self._stop_at_workflow = stop

    @async_property
    async def workflow_completed(self):
        """Return True if all the workflow is finished. False otherwise."""
        return await self.all_sequencers.sequence_completed

    def clean(self):
        """Clean all parts of the workflow that are supposed to be computed."""
        self._logger.info("Cleaning whole workflow.")
        for workflow in self.workflow:
            if not getattr(self, f"_compute_{workflow}"):
                continue
            self._logger.info(f"Cleaning '{workflow}' part.")
            try:
                getattr(self, "clean_{workflow}")()
            except SequenceNotCompletedError:
                self._logger.info(
                        "'{workflow}' part not completed: abort clean.")
                continue

    def clean_band_structure(self):
        """Clean the 'band_structure' part of the workflow only."""
        self.band_structure_sequencer.clean()

    def clean_dos(self):
        """Cleans the 'dos' part of the workflow."""
        self.dos_sequencer.clean()

    def clean_fermi_surface(self):
        """Clean the fermi_surface part of the workflow."""
        self.fermi_surface_sequencer.clean()

    def clean_gs(self):
        """Clean the 'gs' part of the workflow only."""
        self.gs_sequencer.clean()

    def clean_gs_ecut_convergence(self):
        """Clean the 'gs_ecut_convergence' part of the workflow only."""
        self.gs_ecut_convergence_sequencer.clean()

    def clean_gs_kgrid_convergence(self):
        """Clean the 'gs_kgrid_convergence' part of the workflow only."""
        self.gs_kgrid_convergence_sequencer.clean()

    def clean_gs_smearing_convergence(self):
        """Clean the 'gs_smearing_convergence' part of the workflow only."""
        self.gs_smearing_convergence_sequencer.clean()

    def clean_phonon_ecut_convergence(self):
        """Clean the 'phonon_ecut_convergence' part of the workflow only."""
        self.phonon_ecut_convergence_sequencer.clean()

    def clean_phonon_dispersion(self):
        """Clean the 'phonon_dispersion' part of the workflow only."""
        self.phonon_dispersion_sequencer.clean()

    def clean_phonon_dispersion_qgrid_convergence(self):
        """Clean the phonon disp qgrid convergence part of the workflow."""
        self.phonon_dispersion_qgrid_convergence_sequencer.clean()

    def clean_phonon_kgrid_convergence(self):
        """Clean the 'phonon_kgrid_convergence' part of the workflow only."""
        self.phonon_kgrid_convergence_sequencer.clean()

    def clean_phonon_smearing_convergence(self):
        """Clean the phonon_smearing_convergence part of the workflow only."""
        self.phonon_smearing_convergence_sequencer.clean()

    def clean_relaxation(self):
        """Clean the 'relaxation' part of the workflow only."""
        self.relaxation_sequencer.clean()

    def get_sequencer(self, workflow_part):
        """Get the sequencer associated with the given workflow part.

        Parameters
        ----------
        workflow_part: str
            The workflow part for which we want the sequencer.

        Raises
        ------
        ValueError: If the workflow_part is invalid.

        Returns
        -------
        Sequencer object: the sequencer object associated with the part.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow part: '{workflow_part}'.")
        return getattr(self, f"{workflow_part}_sequencer")

    # just an alias
    async def launch(self, *args, **kwargs):
        """See the :meth:`run <abisuite.workflows.bases.run>` method."""
        await self.run(*args, **kwargs)

    async def run(self):
        """Run the workflow by running all requested sequencers."""
        try:
            await self._do_run_or_write("run")
        except TooManyCalculationsInQueueError:
            self._logger.error(
                    Colors.color_text(
                        "STOPPING WORKFLOW", "red", "bold") +
                    ": because too many calculations in queue."
                    )

    async def run_band_structure(self):
        """Run the 'band_structure' part of the workflow only."""
        await self.band_structure_sequencer.run()

    async def run_fermi_surface(self):
        """Run the fermi_surface part of the workflow."""
        await self.fermi_surface_sequencer.run()

    async def run_dos(self):
        """Run the DOS part of the workflow."""
        await self.dos_sequencer.run()
        await self._post_process_dos()

    async def run_gs(self):
        """Run the 'gs' part of the workflow only."""
        await self.gs_sequencer.run()

    async def run_gs_ecut_convergence(self):
        """Run the 'gs' part of the workflow only."""
        await self.gs_ecut_convergence_sequencer.run()

    async def run_gs_kgrid_convergence(self):
        """Run the 'gs_kgrid_convergence' part of the workflow only."""
        await self.gs_kgrid_convergence_sequencer.run()

    async def run_gs_smearing_convergence(self):
        """Run the 'gs_smearing_convergence' part of the workflow only."""
        await self.gs_smearing_convergence_sequencer.run()

    async def run_phonon_ecut_convergence(self):
        """Run the 'phonon_ecut_convergence' part of the workflow only."""
        await self.phonon_ecut_convergence_sequencer.run()

    async def run_phonon_dispersion(self):
        """Run the 'phonon_dispersion' part of the workflow only."""
        await self.phonon_dispersion_sequencer.run()

    async def run_phonon_dispersion_qgrid_convergence(self):
        """Run the phonon disp qgrid convergence part of the workflow only."""
        await self.phonon_dispersion_qgrid_convergence_sequencer.run()
        seqs = self.phonon_dispersion_qgrid_convergence_sequencer
        if not await seqs.sequence_completed:
            return
        cls = self._phonon_dispersion_qgrid_convergence_post_processor_cls
        post_processor = cls(
                sequencer=seqs, workflow=self, loglevel=self._loglevel)
        await post_processor.make_convergence_plots()

    async def run_phonon_kgrid_convergence(self):
        """Run the 'phonon_kgrid_convergence' part of the workflow only."""
        await self.phonon_kgrid_convergence_sequencer.run()

    async def run_phonon_smearing_convergence(self):
        """Runs the 'phonon_smearing_convergence' part of the workflow only."""
        await self.phonon_smearing_convergence_sequencer.run()

    async def run_relaxation(self):
        """Runs the 'relaxation' part of the workflow only."""
        await self.relaxation_sequencer.run()

    async def set_band_structure(self, *args, **kwargs):
        """Sets the BandStructureSequencer.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        band_structure_input_variables: dict
            The dict of the band structure input variables.
        band_structure_calculation_parameters: dict
            The dict of the calculation parameters for the band structure
            calculation.
        band_structure_kpoint_path: list
            The list of kpoints that forms the band structure.
        band_structure_kpoint_path_density: int
            The number of points between kpoints in the list of
            the kpoints that forms the band structure.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used instead of
            running a new scf calculation. This option is incompatible
            with the other 'use_*' flags and will thus override them.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_converged_phonon_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_band_structure:
            return
        subworkflow = self._band_structure_subworkflow_cls(
                *args,
                sequencer=self.band_structure_sequencer,
                workflow=self,
                loglevel=self._loglevel, **kwargs)
        subworkflow.check()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.apply_converged_quantities()
            await subworkflow.set_sequencer()

    async def set_dos(
            self,
            root_workdir: str = None,
            scf_input_variables: dict[str, Any] = None,
            scf_calculation_parameters: dict[str, Any] = None,
            dos_input_variables: dict[str, Any] = None,
            dos_fine_kpoint_grid_variables: dict[str, Any] = None,
            dos_calculation_parameters: dict[str, Any] = None,
            plot_calculation_parameters: dict[str, Any] = None,
            use_gs: bool = False,
            use_converged_gs_ecut: bool = False,
            use_converged_gs_kgrid: bool = False,
            use_converged_phonon_ecut: bool = False,
            use_relaxed_geometry: bool = False,
            ):
        """Sets the 'dos' part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir for the band structure sequence.
        scf_input_variables: dict, optional
            The scf input variables.
        scf_calculation_parameters: dict, optional
            The scf calculation parameters.
        dos_fine_kpoint_grid_variables: dict
            The dict of the dos variables used to define the fine kpoint grid
            used to compute the dos.
        dos_input_variables: dict
            The dict of the dos input variables.
        dos_calculation_parameters: dict
            The dict of the calculation parameters for the dos calculation.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used instead of
            running a new scf calculation. This option is incompatible
            with the other 'use_*' flags and will thus override them.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_converged_phonon_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_dos:
            return
        sequencer = self.dos_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # SCF PART
        await self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_converged_gs_kgrid=use_converged_gs_kgrid,
                use_converged_phonon_ecut=use_converged_phonon_ecut,
                use_relaxed_geometry=use_relaxed_geometry,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                )
        # DOS PART
        sequencer.dos_workdir = os.path.join(
                root_workdir, "dos_run")
        if dos_fine_kpoint_grid_variables is None:
            raise ValueError("Need to set 'dos_fine_kpoint_grid_variables'.")
        sequencer.dos_fine_kpoint_grid_variables = (
                dos_fine_kpoint_grid_variables)
        if dos_input_variables is None:
            raise ValueError("Need to set 'dos_input_variables'.")
        sequencer.dos_input_variables = dos_input_variables
        self._add_calculation_parameters(
                "dos_", dos_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "dos.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "dos.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def set_fermi_surface(
            self,
            fermi_surface_k_z=None,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            nscf_input_variables=None,
            nscf_calculation_parameters=None,
            nscf_kgrid=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_converged_phonon_ecut=False,
            use_converged_phonon_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Set the fermi_surface part of the workflow.

        Parameters
        ----------
        fermi_surface_k_z: list-like, optional
            If given, slices of the fermi surfaces will be generated for these
            k_z. These k_z must be part of the 'nscf_kgrid'.
        root_workdir: str
            The root workdir where the calculations will be run.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        nscf_input_variables: dict
            The dict of nscf input variables.
        nscf_calculation_parameters: dict
            The dict of nscf calculation parameters.
        nscf_kgrid: list-like
            The nscf k-point grid used to interpolate the fermi surface.
        plot_calculation_parameters: dict, optional
            The dict of plot calculations parameters.
        use_converged_phonon_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_converged_phonon_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        if not self._compute_fermi_surface:
            return
        if root_workdir is None:
            raise ValueError("Need to set the 'root_workdir'.")
        sequencer = self.fermi_surface_sequencer
        # SCF PART
        await self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                use_converged_phonon_ecut=use_converged_phonon_ecut,
                use_converged_phonon_smearing=use_converged_phonon_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        if use_gs and not self.gs_sequencer.sequence_completed:
            self.stop_at_worklow = "gs"
            return
        # NSCF PART
        sequencer.nscf_workdir = os.path.join(root_workdir, "nscf_run")
        if nscf_kgrid is None:
            raise ValueError("Need to set 'nscf_kgrid'.")
        sequencer.nscf_kgrid = nscf_kgrid
        if nscf_input_variables is None:
            raise ValueError("Need to set 'nscf_input_variables'.")
        sequencer.nscf_input_variables = nscf_input_variables
        self._add_calculation_parameters(
                "nscf_", nscf_calculation_parameters, sequencer)
        if fermi_surface_k_z is not None:
            sequencer.fermi_surface_k_z = fermi_surface_k_z
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "fermi_surface.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "fermi_surface.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def set_gs(self, *args, **kwargs):
        """Sets the gs SCF sequencer.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculation will be ran.
        scf_input_variables: dict
            The dict of input variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        use_converged_gs_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'gs_ecut_convergence' must be set to True at init.
        use_converged_gs_kgrid: bool, optional
            If True, will use the converged kgrid from the convergence study.
            'gs_kgrid_convergence' must be set to True at init.
        use_converged_gs_smearing: bool, optional
            If True, will use the converged smearing
            from the convergence study.
            'gs_smearing_convergence' must be set to True at init.
        use_converged_phonon_ecut: bool, optional
            If True, will use the converged ecut from the convergence study.
            'phonon_ecut_convergence' must be set to True at init.
        use_converged_phonon_smearing: bool, optional
            If True, will use the converged smaring+kgrid from the
            corresponding convergence study.
            'phonon_smearing_convergence' must be set to True at init.
        use_relaxed_geometry: bool, optional
            If True, will use the relaxed geometry as computed by the
            relaxation run. 'relaxation' must be set to True at init.

        Note
        ----
        other kwargs are passed to the '_use_converged_quantities' method.
        """
        if not self._compute_gs:
            return
        subworkflow = self._gs_subworkflow_cls(
                *args,
                sequencer=self.gs_sequencer,
                workflow=self,
                loglevel=self._loglevel, **kwargs)
        subworkflow.check()
        await subworkflow.apply_converged_quantities()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_gs_ecut_convergence(
            self,
            bypass_sequence_comparison=False,
            root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_convergence_criterion=None,
            scf_ecuts=None,
            plot_calculation_parameters=None,
            _sequencer=None,
            ):
        """Sets the gs_ecut_convergence workflow.

        Parameters
        ----------
        bypass_sequence_comparison: bool, optional
            If True, the workflow bypasses the check on sequences. That means
            if the calculations are different we don't care.
        root_workdir: str
            The workdir where the ecut convergence will be rooted.
        scf_input_variables: dict
            The dictionary of input variables.
        scf_calculation_parameters
            The ecut convergence calculation parameters.
        scf_convergence_criterion: float
            The convergence criterion to reach in meV/atom.
        scf_ecuts: list-like
            The list of ecuts to use for the convergence test.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gs_ecut_convergence:
            return
        # the _sequencer variable lets choose the sequencer to utilize
        # by default take the default one
        # useful when doing mutltiple ecut convergences for different cutoff
        # variables
        if _sequencer is None:
            sequencer = self.gs_ecut_convergence_sequencer
        else:
            sequencer = _sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = os.path.join(root_workdir, "scf_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set  'scf_input_variables'.")
        sequencer.scf_input_variables = scf_input_variables
        if scf_calculation_parameters is None:
            raise ValueError("Need to set 'scf_calculation_parameters'.")
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        sequencer.scf_ecuts = scf_ecuts
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        varname = sequencer.ecuts_input_variable_name
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                f"gs_{varname}_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                f"gs_{varname}_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)
        sequencer.bypass_sequence_comparison = bypass_sequence_comparison

    async def set_gs_kgrid_convergence(
            self,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_convergence_criterion=None,
            scf_kgrids=None,
            plot_calculation_parameters=None,
            use_converged_gs_ecut=False,
            use_geometry_from=None,
            ):
        """Sets the gs_kgrid_convergence workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the kgrid convergence will be rooted.
        scf_input_variables: dict
            The dict of input variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        scf_convergence_criterion: float
            The convergence criterion to reach in meV / atom.
        scf_kgrids: list-like
            The list of kgrids to test.
        use_converged_gs_ecut: bool, optional
            If True, the ecut used will be the converged one.
            'gs_ecut_convergence' must be set to True or
            the 'converged_gs_ecut' attribute must be set.
        use_geometry_from: str, optional
            If not None, gives the workflow part whose scf part contains
            the geometry we want to use for the scf part of the
            current workflow part. E.g.: if we want to use
            the 'gs_ecut_convergence' geometry, just set this argument
            to it.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        """
        if not self._compute_gs_kgrid_convergence:
            return
        sequencer = self.gs_kgrid_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = os.path.join(root_workdir, "scf_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        await self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_geometry_from=use_geometry_from)
        sequencer.scf_input_variables = scf_input_variables
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        sequencer.scf_kgrids = scf_kgrids
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "gs_kgrid_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "gs_kgrid_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def set_gs_smearing_convergence(
            self,
            root_workdir=None,
            scf_input_variables=None,
            scf_convergence_criterion=None,
            scf_kgrids=None,
            scf_smearings=None,
            scf_calculation_parameters=None,
            plot_calculation_parameters=None,
            use_converged_gs_ecut=False,
            use_geometry_from=None,
            ):
        """Sets the gs_smearing_convergence part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The path where the convergence will be rooted.
        scf_input_variables: dict, optional
            The dict of main input variables.
        scf_convergence_criterion: float
            The convergence criterion (in meV/at).
        scf_kgrids: list
            The list of kgrids to test.
        scf_smearings: list
            The list of smearing parameters to test.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        use_converged_gs_ecut: bool, optional
            If True, the ecut used will be the converged one.
            'gs_ecut_convergence' must be set to True upon init or
            the 'converged_gs_ecut' attribute must be set.
        use_geometry_from: str, optional
            If not None, gives the workflow part whose scf part contains
            the geometry we want to use for the scf part of the
            current workflow part. E.g.: if we want to use
            the 'gs_ecut_convergence' geometry, just set this argument
            to it.
        """
        if not self._compute_gs_smearing_convergence:
            return
        sequencer = self.gs_smearing_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = root_workdir
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        await self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer,
                use_geometry_from=use_geometry_from,
                use_converged_gs_ecut=use_converged_gs_ecut,
                )
        sequencer.scf_input_variables = scf_input_variables
        if scf_convergence_criterion is None:
            raise ValueError("Need to set 'scf_convergence_criterion'.")
        sequencer.scf_convergence_criterion = scf_convergence_criterion
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        sequencer.scf_kgrids = scf_kgrids
        if scf_smearings is None:
            raise ValueError("Need to set 'scf_smearings'.")
        sequencer.scf_smearings = scf_smearings
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "gs_smearing_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "gs_smearing_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def set_phonon_ecut_convergence(
            self, phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_convergence_criterion=None,
            phonons_qpt=None,
            plot_calculation_parameters=None,
            root_workdir=None,
            scf_input_variables=None, scf_ecuts=None,
            scf_calculation_parameters=None,
            use_converged_gs_kgrid=False,
            use_converged_gs_smearing=False,
            use_relaxed_geometry=False,
            _sequencer=None):
        """Sets the phonon ecut convergence part of the workflow.

        Parameters
        ----------
        phonons_calculation_parameters: dict
            The dict of phonons calculations parameters.
        phonons_convergence_criterion: float
            The phonons convergence criterion.
        phonons_input_variables: dict. optional
            The dict of phonons input variables.
        phonons_qpt: list
            The phonon qpt to study convergence against.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        root_workdir: str
            The root workdir for the scf and phonon calculations.
        scf_ecuts: list
            The list of ecuts to test.
        scf_input_variables: dict
            The dict of the scf calculation variables.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        use_converged_gs_kgrid: bool, optional
            If True, the gs converged kgrid variables are added to the
            scf_input_variables. 'gs_kgrid_convergence' must be set to True or
            the 'converged_gs_kgrid' attribute must be set.
        use_converged_gs_smearing: bool, optional
            If True, the gs converged kgrid+smearing variables are added to the
            scf_input_variables. 'gs_smearing_convergence'
            must be set to True or
            the 'converged_gs_smearing' attribute must be set.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used.
        """
        if not self._compute_phonon_ecut_convergence:
            return
        # the _sequencer variable lets choose the sequencer to utilize
        # by default take the default one
        # useful when doing mutltiple ecut convergences for different cutoff
        # variables
        if _sequencer is None:
            seq = self.phonon_ecut_convergence_sequencer
        else:
            seq = _sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        seq.scf_workdir = os.path.join(root_workdir, "scf_runs")
        seq.phonons_workdir = os.path.join(root_workdir, "phonons_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        await self._use_converged_quantities(
                scf_input_variables,
                use_converged_gs_kgrid=use_converged_gs_kgrid,
                use_converged_gs_smearing=use_converged_gs_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        seq.scf_input_variables = scf_input_variables
        if scf_ecuts is None:
            raise ValueError("Need to set 'scf_ecuts'.")
        seq.scf_ecuts = scf_ecuts
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, seq)
        if phonons_input_variables is None:
            phonons_input_variables = {}
        seq.phonons_input_variables = phonons_input_variables
        if phonons_qpt is None:
            raise ValueError("Need to set 'phonons_qpt'.")
        seq.phonons_qpt = phonons_qpt
        if phonons_convergence_criterion is None:
            raise ValueError("Need to set 'phonons_convergence_criterion'.")
        seq.phonons_convergence_criterion = phonons_convergence_criterion
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results",
                f"phonon_{seq.ecuts_input_variable_name}_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results",
                f"phonon_{seq.ecuts_input_variable_name}_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)

    async def set_phonon_dispersion(
            self, root_workdir=None,
            scf_calculation_parameters=None,
            scf_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_qpoint_grid=None,
            phonons_qpoints_split_by_irreps=None,
            qpoint_path=None,
            qpoint_path_density=None,
            plot_calculation_parameters=None,
            use_gs=False,
            use_converged_phonon_ecut=False,
            use_converged_phonon_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets the phonon dispersion part of the workflow.

        Parameters
        ----------
        root_workdir: str
            The root workdir where the calculations will run.
        scf_calculation_parameters: dict, optional
            The dict of scf calculation parameters.
        scf_input_variables: dict, optional
            The dict of scf input variables.
        phonons_calculation_parameters: dict
            The dict of phonons calculation parameters.
        phonons_input_variables: dict
            The dict of phonons input variables.
        phonons_qpoint_grid: list
            The qpoint grid with which the phonon dispersion will be computed.
        phonons_qpoints_split_by_irreps: list, optional
            A list of qpts number (startin' at 1) that we want to split by
            irreducible perturbations.
            Useful for qpts that are very hard to compute
            or for very large system with a lot of atoms.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        qpoint_path: list
            The list of qpoints that forms the phonon dispersion.
        qpoint_path_density: int
            The number of qpoints between the points given in the qpoint path.
        use_converged_phonon_ecut: bool, optional
            If True, the phonon converged ecut will be used. The
            'phonon_ecut_convergence' part of the workflow must be activated
            or the 'converged_phonon_ecut' must be set upon init.
        use_converged_phonon_smearing: bool, optional
            If True, the phonon converged smearing will be used. The
            'phonon_smearing_convergence' part of the workflow must be
            activated or the 'converged_phonon_smearing' must be set upon init.
        use_gs: bool, optional
            If True, the 'gs' part of the workflow is used. This option is
            incompatible with 'use_*' flags and will thus override them.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used. The 'relaxation' part
            of the workflow must be activated or the 'relaxed_geometry'
            attribute must be set upon init.
        """
        if not self._compute_phonon_dispersion:
            return
        sequencer = self.phonon_dispersion_sequencer
        if root_workdir is None:
            raise ValueError("The 'root_workdir' must be set.")
        # SCF
        await self._use_converged_calculations(
                sequencer, use_gs=use_gs,
                scf_input_variables=scf_input_variables,
                scf_calculation_parameters=scf_calculation_parameters,
                scf_workdir=os.path.join(root_workdir, "scf_run"),
                use_converged_phonon_ecut=use_converged_phonon_ecut,
                use_converged_phonon_smearing=use_converged_phonon_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        # PHONONS
        sequencer.phonons_workdir = os.path.join(
                root_workdir, "phonons_runs", "ph_q")
        if phonons_qpoint_grid is None:
            raise ValueError("Need to set 'phonons_qpoint_grid'.")
        sequencer.phonons_qpoint_grid = phonons_qpoint_grid
        if phonons_input_variables is None:
            raise ValueError("Need to set 'phonons_input_variables'.")
        sequencer.phonons_input_variables = phonons_input_variables
        if phonons_qpoints_split_by_irreps is not None:
            sequencer.phonons_qpoints_split_by_irreps = (
                    phonons_qpoints_split_by_irreps)
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, sequencer)
        # PHONON DISPERSION PART
        if qpoint_path is None:
            raise ValueError("Need to set 'qpoint_path'.")
        sequencer.qpoint_path = qpoint_path
        if qpoint_path_density is None:
            raise ValueError("Need to set 'qpoint_path_density'.")
        sequencer.qpoint_path_density = qpoint_path_density
        # PLOT
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "phonon_dispersion.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "phonon_dispersion.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, sequencer)

    async def set_phonon_dispersion_qgrid_convergence(
            self, *args, **kwargs) -> None:
        """Set the phonon dispersion qpoint grid convergence part.

        Launches a bunch of phonon dispersion sequencers
        to test out many qpoint grids.
        """
        if not self._compute_phonon_dispersion_qgrid_convergence:
            return
        cls = self._phonon_dispersion_qgrid_convergence_subworkflow_cls
        subworkflow = cls(
                *args,
                sequencer=self.phonon_dispersion_qgrid_convergence_sequencer,
                workflow=self,
                loglevel=self._loglevel,
                **kwargs,
                )
        subworkflow.check()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.set_sequencer()

    async def set_phonon_kgrid_convergence(
            self,
            ddk_input_variables=None,
            ddk_calculation_parameters=None,
            phonons_input_variables=None,
            phonons_calculation_parameters=None,
            phonons_convergence_criterion=None,
            phonons_qpt=None,
            plot_calculation_parameters=None,
            root_workdir=None,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_kgrids=None,
            use_converged_gs_smearing=False,
            use_converged_phonon_ecut=False,
            use_relaxed_geometry=False):
        """Sets the phonon kgrid convergence part of the workflow.

        Parameters
        ----------
        phonons_calculation_parameters: dict
            The dict of phonons calculations parameters.
        phonons_convergence_criterion: float
            The phonons convergence criterion.
        phonons_input_variables: dict. optional
            The dict of phonons input variables.
        phonons_qpt: list
            The phonon qpt to study convergence against.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        scf_input_variables: dict
            The dict of the scf calculation variables.
        scf_kgrids: list
            The list of kgrids to test.
        use_converged_phonon_ecut: bool, optional
            If True, the phonon converged ecut variables are added to the
            scf_input_variables. 'phonon_ecut_convergence'
            must be set to True or
            the 'converged_phonon_ecut' attribute must be set.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used.
        use_converged_gs_smearing: bool, optional
            If True, the converged gs smearing will be used.
        """
        if not self._compute_phonon_kgrid_convergence:
            return
        seq = self.phonon_kgrid_convergence_sequencer
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        # SCF PART
        seq.scf_workdir = os.path.join(root_workdir, "scf_runs")
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        try:
            await self._use_converged_quantities(
                    scf_input_variables,
                    use_converged_phonon_ecut=use_converged_phonon_ecut,
                    use_relaxed_geometry=use_relaxed_geometry,
                    use_converged_gs_smearing=use_converged_gs_smearing)
        except SequenceNotConvergedError as e:
            self._logger.exception(e)
            self._logger.error("Convergence not achieved: abort set.")
            if use_relaxed_geometry:
                self.stop_at_workflow = "relaxation"
            if use_converged_phonon_ecut:
                # need to reset if the one above was set
                self.stop_at_workflow = None
                self.stop_at_workflow = "phonon_ecut_convergence"
            return
        seq.scf_input_variables = scf_input_variables
        if scf_kgrids is None:
            raise ValueError("Need to set 'scf_kgrids'.")
        seq.scf_kgrids = scf_kgrids
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, seq)
        # PHONONS PART
        seq.phonons_workdir = os.path.join(root_workdir, "phonons_runs")
        if phonons_input_variables is None:
            phonons_input_variables = {}
        seq.phonons_input_variables = phonons_input_variables
        if phonons_qpt is None:
            raise ValueError("Need to set 'phonons_qpt'.")
        seq.phonons_qpt = phonons_qpt
        if phonons_convergence_criterion is None:
            raise ValueError("Need to set 'phonons_convergence_criterion'.")
        seq.phonons_convergence_criterion = phonons_convergence_criterion
        self._add_calculation_parameters(
                "phonons_", phonons_calculation_parameters, seq)
        if plot_calculation_parameters is None:
            plot_calculation_parameters = {}
        plot_calculation_parameters["save"] = os.path.join(
                root_workdir, "results", "phonon_kgrid_convergence.pdf")
        plot_calculation_parameters["save_pickle"] = os.path.join(
                root_workdir, "results", "phonon_kgrid_convergence.pickle")
        self._add_calculation_parameters(
                "plot_", plot_calculation_parameters, seq)

    async def set_phonon_smearing_convergence(self, *args, **kwargs):
        """Sets the phonon smearing convergence part of the workflow.

        Parameters
        ----------
        phonons_calculation_parameters: dict
            The dict of phonons calculations parameters.
        phonons_convergence_criterion: float
            The phonons convergence criterion.
        phonons_input_variables: dict. optional
            The dict of phonons input variables.
        phonons_qpt: list
            The phonon qpt to study convergence against.
        plot_calculation_parameters: dict, optional
            The dict of plot parameters.
        root_workdir: str
            The root workdir for the scf and phonon calculations.
        scf_calculation_parameters: dict
            The dict of calculation parameters.
        scf_input_variables: dict
            The dict of the scf calculation variables.
        scf_kgrids: list
            The list of kgrids to test.
        scf_smearings: list
            The list of smearings to test.
        use_converged_phonon_ecut: bool, optional
            If True, the phonon converged ecut variables are added to the
            scf_input_variables. 'phonon_ecut_convergence'
            must be set to True or
            the 'converged_phonon_ecut' attribute must be set.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be used.
        """
        if not self._compute_phonon_smearing_convergence:
            return
        subworkflow = self._phonon_smearing_convergence_subworkflow_cls(
                *args,
                sequencer=self.phonon_smearing_convergence_sequencer,
                workflow=self,
                loglevel=self._loglevel, **kwargs)
        subworkflow.check()
        if subworkflow.ready_to_set_sequencer:
            await subworkflow.apply_converged_quantities()
            await subworkflow.set_sequencer()

    async def set_relaxation(
            self,
            bypass_sequence_comparison=False,
            root_workdir=None, scf_input_variables=None,
            scf_calculation_parameters=None,
            relax_atoms=None,
            relax_cell=None,
            **kwargs):
        """Sets the relaxation sequencer.

        Parameters
        ----------
        bypass_sequence_comparison: bool, optional
            If True, the workflow bypasses the check on sequences. That means
            if the calculations are different we don't care.
        relax_atoms: bool
            If True, the atomic positions will be relaxed prior to the
            cell optimization (if relax_cell is True). Otherwise,
            only the atoms relaxation is done.
        relax_cell: bool
            If True, the cell optimization is done after a first
            atomic relaxation (if relax_atoms is True). Otherwise,
            only the cell optimization is done.
        root_workdir: str
            The root directory where the relaxation run(s) will be executed.
        scf_input_variables: dict
            The input variables dictionary.
        scf_calculation_parameters: dict
            The dict of calculation parameters.

        Note
        ----
        other kwargs are passed to the '_use_converged_quantities' method.
        """
        sequencer = self.relaxation_sequencer
        if not self._compute_relaxation:
            return
        if root_workdir is None:
            raise ValueError("Need to set 'root_workdir'.")
        sequencer.scf_workdir = root_workdir
        if relax_atoms is None:
            raise ValueError("Need to set 'relax_atoms'.")
        sequencer.relax_atoms = relax_atoms
        if relax_cell is None:
            raise ValueError("Need to set 'relax_cell'.")
        sequencer.relax_cell = relax_cell
        if scf_input_variables is None:
            raise ValueError("Need to set 'scf_input_variables'.")
        await self._use_converged_quantities(
                scf_input_variables, sequencer=sequencer, **kwargs)
        sequencer.scf_input_variables = scf_input_variables
        if scf_calculation_parameters is None:
            raise ValueError("Need to set 'scf_calculation_parameters'.")
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)
        sequencer.bypass_sequence_comparison = bypass_sequence_comparison

    async def workflow_part_completed(self, workflow_part):
        """Checks that the given workflow part has completed.

        If not, the workflow will be stopped at this part. If
        the part is not computed, an error is raised.

        Parameters
        ----------
        workflow_part: str
            The part of the workflow in question.

        Raises
        ------
        ValueError: If the workflow part is invalid.
        RuntimeError: If the workflow part is not computed.

        Returns
        -------
        bool: True if the part is completed. False otherwise.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow: '{workflow_part}'.")
        if not self.workflow_part_computed(workflow_part):
            raise RuntimeError(f"Not computed workflow: '{workflow_part}'.")
        sequencer = self.get_sequencer(workflow_part)
        if not await sequencer.sequence_completed:
            self.stop_at_workflow = workflow_part
            return False
        return True

    def workflow_part_computed(self, workflow_part):
        """Checks if workflow part is computed.

        Parameters
        ----------
        workflow_part: str
            The part of the workflow in question.

        Raises
        ------
        ValueError: if the workflow part is invalid.

        Returns
        -------
        bool: True if the part is computed. False otherwise.
        """
        if workflow_part not in self.workflow:
            raise ValueError(f"Invalid workflow: '{workflow_part}'.")
        return getattr(self, f"_compute_{workflow_part}")

    async def write(self):
        """Only writes the workflow files as far as it can go."""
        await self._do_run_or_write("write")

    async def write_band_structure(self):
        """Writes the 'band_structure' part of the workflow only."""
        await self.band_structure_sequencer.write()

    async def write_dos(self):
        """Write the DOS part of the workflow."""
        await self.dos_sequencer.write()
        await self._post_process_dos()

    async def write_fermi_surface(self):
        """Write the fermi surface part of the workflow."""
        await self.fermi_surface_sequencer.write()

    async def write_gs(self):
        """Writes the 'gs' part of the workflow only."""
        await self.gs_sequencer.write()

    async def write_gs_ecut_convergence(self):
        """Writes the 'gs_ecut_convergence' part of the workflow only."""
        await self.gs_ecut_convergence_sequencer.write()

    async def write_gs_kgrid_convergence(self):
        """Writes the 'gs_kgrid_convergence' part of the workflow only."""
        await self.gs_kgrid_convergence_sequencer.write()

    async def write_gs_smearing_convergence(self):
        """Writes the 'gs_smearing_convergence' part of the workflow only."""
        await self.gs_smearing_convergence_sequencer.write()

    async def write_phonon_ecut_convergence(self):
        """Writes the 'phonon_ecut_convergence' part of the workflow only."""
        await self.phonon_ecut_convergence_sequencer.write()

    async def write_phonon_dispersion(self):
        """Writes the 'phonon_dispersion' part of the workflow only."""
        await self.phonon_dispersion_sequencer.write()

    async def write_phonon_dispersion_qgrid_convergence(self):
        """Write the phonon disp qgrid convergence part of the workflow."""
        await self.phonon_dispersion_qgrid_convergence_sequencer.write()

    async def write_phonon_kgrid_convergence(self):
        """Writes the 'phonon_kgrid_convergence' part of the workflow only."""
        await self.phonon_kgrid_convergence_sequencer.write()

    async def write_phonon_smearing_convergence(self):
        """Writes the 'phonon_smearing_convergence' part of the workflow."""
        await self.phonon_smearing_convergence_sequencer.write()

    async def write_relaxation(self):
        """Writes the 'write_relaxation' part of the workflow only."""
        await self.relaxation_sequencer.write()

    @staticmethod
    def _add_calculation_parameters(prefix, parameters, sequencer):
        """Adds the calc parameters from a given prefix to the given sequencer.

        Parameters
        ----------
        prefix: str
            The calculation type prefix. Should end with underscore.
        parameters: dict
            The dictionary of parameters.
        sequencer: Sequencer object
            The Sequencer object on which we want to apply the given
            calculation parameters.
        """
        if not prefix.endswith("_"):
            prefix += "_"
        if parameters is None:
            raise ValueError(
                    f"Need to set '{prefix}calculation_parameters'.")
        for name, value in parameters.copy().items():
            if not name.startswith(prefix):
                name = prefix + name
            setattr(sequencer, name, value)

    @staticmethod
    def _add_input_variables(prefix, input_variables, sequencer):
        """Add input variables for a given prefix to a given sequencer."""
        if not prefix.endswith("_"):
            prefix += "_"
        if input_variables is None:
            raise ValueError(
                    f"Need to set {prefix}input_variables.")
        setattr(sequencer, f"{prefix}input_variables", input_variables)

    def _check_workflow(self):
        """Check that the workflow has all required funcs and attrs defined."""
        for workflow in self.workflow:
            compute_attr = f"_compute_{workflow}"
            if not hasattr(self, compute_attr):
                raise DevError(f"Need to set '{compute_attr}'.")
            seq_attr = f"{workflow}_sequencer"
            if seq_attr not in dir(self):
                raise DevError(f"Need to set '{seq_attr}'.")
            for functype in ("clean", "run", "set", "write"):
                func_attr = f"{functype}_{workflow}"
                if not hasattr(self, func_attr):
                    raise DevError(f"Need to set '{func_attr}'.")
                func = getattr(self, func_attr)
                if not callable(func):
                    raise DevError(f"'{func_attr}' should be callable.")

    async def _do_run_or_write(self, run_or_write):
        """Actually run or write the workflow.

        Paramaters
        ----------
        run_or_write: str
            The action to execute.
        """
        if run_or_write not in ("run", "write"):
            raise DevError(run_or_write)
        # self._set_global_sequencers_attributes()
        self._check_workflow()
        todomsg = f"Workflow parts to {run_or_write}:\n"
        for todo in self._to_compute_workflow_parts():
            todomsg += f"- {todo}\n"
        self._logger.info(todomsg)
        for workflow_part in self.workflow:
            # do we compute this workflow?
            if getattr(self, f"_compute_{workflow_part}"):
                if run_or_write == "run":
                    action = "Running"
                else:
                    action = "Writing"
                self._logger.info(
                    f"{action} '{workflow_part}' "
                    "part of the workflow.")
                # actually call the function to run or write workflow_part
                await getattr(self, f"{run_or_write}_{workflow_part}")()
            # do we stop at this workflow?
            if self.stop_at_workflow == workflow_part:
                self._logger.info(
                        f"{Colors.color_text('Stopping', 'bold', 'red')} "
                        "prematuraly because "
                        f"'{self.stop_at_workflow}' has/had to be completed. "
                        "Please rerun workflow script once everything is "
                        "done.")
                return
        if await self.workflow_completed:
            self._logger.info(
                    Colors.color_text("Workflow completed!", "green", "bold"))
        else:
            self._logger.info(
                    "Workflow not completed, please rerun workflow script if "
                    "it stopped or "
                    "check if something bad happened.")
            msg = ("The following "
                   "subworkflows did not complete:\n")
            for workflow_part in await self._incomplete_workflow_parts():
                msg += f"- {workflow_part}\n"
            self._logger.info(msg)

    def _to_compute_workflow_parts(self) -> Sequence[str]:
        """Return the workflow parts to run."""
        todos = []
        for workflow_part in self.workflow:
            if getattr(self, f"_compute_{workflow_part}"):
                todos.append(workflow_part)
        return todos

    async def _incomplete_workflow_parts(self) -> Sequence[str]:
        """Return the list of workflow parts that did not complete."""
        coros = []

        async def _get_sequence_completed(seq) -> bool:
            return await seq.sequence_completed

        workflow_parts = self._to_compute_workflow_parts()
        for workflow_part in workflow_parts:
            seq = self._get_sequencer_from_workflow_part(workflow_part)
            coros.append(_get_sequence_completed(seq))
        completed = await asyncio.gather(*coros)
        incomplete = []

        for workflow_part, complete in zip(workflow_parts, completed):
            if not complete:
                incomplete.append(workflow_part)
        return incomplete

    def _get_sequencer_from_workflow_part(
            self, workflow_part: str) -> BaseSequencer:
        """Return the sequencer attributed to this workflow part."""
        return getattr(self, f"{workflow_part}_sequencer")

    async def _use_converged_calculations(
            self, sequencer,
            use_gs=False,
            scf_input_variables=None,
            scf_calculation_parameters=None,
            scf_workdir=None,
            use_converged_gs_ecut=False,
            use_converged_gs_kgrid=False,
            use_converged_gs_smearing=False,
            use_converged_phonon_ecut=False,
            use_converged_phonon_smearing=False,
            use_relaxed_geometry=False,
            ):
        """Sets up or not a converged calculation to the given sequencer.

        Parameters
        ----------
        sequencer: Sequencer object
            The sequencer to set up in question.
        use_gs: bool, optional
            If True, the gs part of the workflow will be used as the scf part
            of the sequencer. This parameter will override the other 'use_*'
            kwargs.
        scf_input_variables: dict, optional
            If 'use_gs' is False, this dict is used for the input variables.
        scf_calculation_parameters: dict, optional
            If 'use_gs' is False, this dict is used for the scf calculation
            parameters.
        scf_workdir: str, optional
            If 'use_gs' is False, this path is used as the scf workdir.
        use_converged_gs_ecut: bool, optional
            If True, the gs converged ecut is used.
        use_converged_gs_kgrid: bool, optional
            If True, use the gs converged kgrid.
        use_converged_gs_smearing: bool, optional
            If True, use the gs converged kgrid and smearing.
        use_converged_phonon_ecut: bool, optional
            If True, the phonon converged ecut is used.
        use_converged_phonon_smearing: bool, optional
            If True, the phonon converged smearing is used.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be added to the input variables.
        """
        if use_gs:
            if any([use_converged_gs_ecut, use_converged_gs_kgrid,
                    use_converged_gs_smearing, use_relaxed_geometry,
                    use_converged_phonon_ecut, use_converged_phonon_smearing]):
                raise ValueError(
                        "'use_gs' cannot be used in combination with the other"
                        " 'use_* flags.")
            # if not self.workflow_part_completed("gs"):
            #     return
            if not self._compute_gs:
                raise RuntimeError(
                        "'use_gs' is True => need to set 'gs' to True upon "
                        "init.")
            sequencer.scf_workdir = self.gs_sequencer.scf_workdir
            sequencer.scf_input_variables = (
                    self.gs_sequencer.scf_input_variables.copy())
            if not await self.gs_sequencer.sequence_completed:
                self.stop_at_workflow = "gs"
            return
        if scf_workdir is None:
            raise DevError("Need to set 'scf_workdir'.")
        sequencer.scf_workdir = scf_workdir
        if scf_input_variables is None:
            raise ValueError("Need to set the 'scf_input_variables'.")
        await self._use_converged_quantities(
                scf_input_variables,
                use_converged_gs_ecut=use_converged_gs_ecut,
                use_converged_gs_kgrid=(
                    use_converged_gs_kgrid),
                use_converged_gs_smearing=use_converged_gs_smearing,
                use_converged_phonon_ecut=use_converged_phonon_ecut,
                use_converged_phonon_smearing=use_converged_phonon_smearing,
                use_relaxed_geometry=use_relaxed_geometry)
        sequencer.scf_input_variables = scf_input_variables
        self._add_calculation_parameters(
                "scf_", scf_calculation_parameters, sequencer)

    async def _use_any_converged_calculation(
            self,
            sequencer: BaseSequencer,
            sequence_part: str,
            add_input_variables: bool = True,
            default_workdir: str = None,
            default_input_variables: dict[str, Any] = None,
            default_calculation_parameters: dict[str, Any] = None,
            use_from: str = None,
            **kwargs,
            ) -> None:
        """Generalization of the previous 'use_converged_calculation' method.

        Can load a part of another sequencer (from another workflow part)
        or set default ones if we don't use this other part.

        Other kwargs are passed to the 'use_converged_quantities' method
        for fine tuning of input variables. Only in case the 'use_from'
        attribute is not None.

        Parameters
        ----------
        sequencer: The sequencer object we're working on.
        sequence_part: str
            The part of the sequence we're working on.
        add_input_variables: bool, optional
            If True (default), we add the given input variables
            if use_from is None.
        default_workdir: str, optional
            If not None, specifies the default workdir for this part.
        default_input_variables: dict, optional
            The default input variables for this part.
        default_calculation_parameters: dict, optional
            The default calculation parameters for this part.
        use_from: str, optional
            Can be a path towards a real calculation or a 'workflow_part' tag.
            This specifies where we get the calculation.
        """
        if not sequence_part.endswith("_"):
            sequence_part += "_"
        if use_from is None:
            # use default parameters
            if default_workdir is None:
                raise DevError(
                        f"Need to set '{sequence_part}workdir.")
            setattr(sequencer, f"{sequence_part}workdir", default_workdir)
            if add_input_variables:
                self._add_input_variables(
                    sequence_part, default_input_variables, sequencer)
            self._add_calculation_parameters(
                    sequence_part, default_calculation_parameters, sequencer)
            if kwargs:
                await self._use_converged_quantities(
                        getattr(sequencer, f"{sequence_part}input_variables"),
                        sequencer=sequencer, **kwargs)
            return
        if await is_calculation_directory(use_from):
            # we gave a calculation path directly
            # assume user knows what's he/she doin
            setattr(sequencer, f"{sequence_part}workdir", use_from)
            if default_input_variables is None:
                # parse the input file
                async with await CalculationDirectory.from_calculation(
                        use_from) as calc:
                    async with calc.input_file as inp:
                        default_input_variables = inp.input_variables
            if add_input_variables:
                self._add_input_variables(
                    sequence_part, default_input_variables, sequencer)
            self._add_calculation_parameters(
                    sequence_part, default_calculation_parameters,
                    sequencer)
            if kwargs:
                await self._use_converged_quantities(
                        getattr(
                            sequencer, f"{sequence_part}input_variables"),
                        sequencer=sequencer, **kwargs)
            return
        # if we're here, check if use_from is a workflow part
        # in that case, load the relevent information from the
        # other sequencer.
        if use_from not in self.workflow:
            raise DevError(f"Invalid workflow part: '{use_from}'.")
        # use the other calculation
        other_seq = getattr(self, f"{use_from}_sequencer")
        if not await other_seq.sequence_completed:
            self.stop_at_workflow = use_from
        # if not completed, stop there!
        # since it should be completed, we just need to set workdir
        # and input variables
        setattr(sequencer, f"{sequence_part}workdir",
                getattr(other_seq, f"{sequence_part}workdir"))
        setattr(sequencer, f"{sequence_part}input_variables",
                getattr(other_seq, f"{sequence_part}input_variables"))

    # FG: 2021/04/16
    # TODO: this method is wayyyy too bigggg... find a way to make it smaller
    # and (most importantly) DRYer!
    async def _use_converged_quantities(
            self, input_variables,
            sequencer=None,
            use_converged_gs_ecut=False,
            use_converged_gs_kgrid=False,
            use_converged_gs_smearing=False,
            use_converged_phonon_ecut=False,
            use_converged_phonon_smearing=False,
            use_relaxed_geometry=False,
            use_geometry_from=None,
            ):
        """Adds some converged quantities to the given input variables dict.

        Parameters
        ----------
        input_variables: dict
            The dict of input variables to manipulate.
        sequencer: Sequencer object, optional
            The Sequencer object that the input vars will be acted upon.
            Needed for some parameters.
        use_converged_gs_ecut: bool, optional
            If True, the gs converged ecut is used.
        use_converged_gs_kgrid: bool, optional
            If True, use the gs converged kgrid.
        use_converged_gs_smearing: bool, optional
            If True, use the gs converged smearing variables.
        use_relaxed_geometry: bool, optional
            If True, the relaxed geometry will be added to the input variables.
        use_converged_phonon_ecut: bool, optional
            If True, use the phonon converged ecut variables.
        use_converged_phonon_smearing: bool, optional
            If True, use the phonon converged smearing variables.
        use_geometry_from: str, optional
            If not None, specifies the workflow sequence (it's scf part)
            whose geometry we want to use (through the
            'scf_load_geometry_from' sequencer's property).
        """
        if not isinstance(input_variables, dict):
            raise TypeError(
                    "'input_variables' should be a dict but got "
                    f"'{input_variables}'.")
        if use_converged_gs_ecut and use_converged_phonon_ecut:
            raise ValueError(
                    "'use_converged_gs_ecut' cannot be used in conjunction "
                    "with 'use_converged_phonon_ecut'. Which ecut to take?")
        if use_converged_gs_ecut:
            if not self._compute_gs_ecut_convergence:
                if self.converged_gs_ecut is not None:
                    input_variables.update(self.converged_gs_ecut)
                else:
                    raise ValueError(
                        "Need to set 'gs_ecut_convergence' to True or 'converg"
                        "ed_gs_ecut' upon init.")
            else:
                if not await (
                        self.gs_ecut_convergence_sequencer.sequence_completed):
                    self._logger.info(
                            "Need to wait for 'gs_ecut_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_ecut_convergence"
                    return
                if isinstance(
                        self.gs_ecut_convergence_sequencer, SequencersList):
                    for ecut_seq in self.gs_ecut_convergence_sequencer:
                        varname = ecut_seq.ecuts_input_variable_name
                        varvalue = await ecut_seq.scf_converged_ecut
                        input_variables[varname] = varvalue
                else:
                    ecut_seq = self.gs_ecut_convergence_sequencer
                    varname = ecut_seq.ecuts_input_variable_name
                    varvalue = await ecut_seq.scf_converged_ecut
                    input_variables[varname] = varvalue
        if use_converged_gs_kgrid:
            if not self._compute_gs_kgrid_convergence:
                if self.converged_gs_kgrid is not None:
                    input_variables.update(self.converged_gs_kgrid)
                else:
                    raise ValueError(
                        "Need to set 'gs_kgrid_convergence' to True or "
                        "'gs_converged_kgrid' upon init."
                        )
            else:
                if not await (
                        self.gs_kgrid_convergence_sequencer.sequence_completed
                        ):
                    self._logger.info(
                            "Need to wait for 'gs_kgrid_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_kgrid_convergence"
                    return
                sequencer = self.gs_kgrid_convergence_sequencer
                varname = sequencer.kgrids_input_variable_name
                value = await sequencer.scf_converged_kgrid
                input_variables[varname] = value
        if use_converged_gs_kgrid and use_converged_gs_smearing:
            raise ValueError(
                    "'use_converged_gs_kgrid' is not compatible with 'use_"
                    "gs_converged_smearing'. Because we determine converged"
                    " kgrid when we compute the smearing convergence.")
        if use_converged_gs_smearing and use_converged_phonon_smearing:
            raise ValueError(
                    "'use_converged_gs_smearing' cannot be used in conjunction"
                    " with 'use_converged_phonon_smearing'. "
                    "Which smearing to take?")
        if use_converged_gs_smearing:
            if not self._compute_gs_smearing_convergence:
                if self.converged_gs_smearing is not None:
                    input_variables.update(self.converged_gs_smearing)
                else:
                    raise ValueError(
                        "Need to set 'gs_smearing_convergence' to True "
                        "or 'converged_gs_smearing' upon init.")
            else:
                seq = self.gs_smearing_convergence_sequencer
                if not await seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'gs_smearing_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "gs_smearing_convergence"
                    return
                seq = self.gs_smearing_convergence_sequencer
                input_variables.update(await seq.converged_smearing_variables)
        # relaxation comes before phonon convergences
        if use_relaxed_geometry or use_geometry_from is not None:
            if use_geometry_from is None:
                use_geometry_from = "relaxation"
            if use_relaxed_geometry and use_geometry_from != "relaxation":
                raise ValueError(
                        "'use_geometry_from' != 'relaxation' is incompatible "
                        "with 'use_relaxed_geometry' option.")
            await self._use_geometry_from(
                    use_geometry_from, input_variables, sequencer)
        if use_converged_phonon_ecut:
            if not self._compute_phonon_ecut_convergence:
                if self.converged_phonon_ecut is not None:
                    input_variables.update(self.converged_phonon_ecut)
                else:
                    raise ValueError(
                        "Need to set 'phonon_ecut_convergence' to True "
                        "or 'converged_phonon_ecut' upon init.")
            else:
                seq = self.phonon_ecut_convergence_sequencer
                if not seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'phonon_ecut_convergence' to "
                            "complete.")
                    self.stop_at_workflow = "phonon_ecut_convergence"
                    return
                if isinstance(
                        self.phonon_ecut_convergence_sequencer,
                        SequencersList):
                    for ecut_seq in self.phonon_ecut_convergence_sequencer:
                        varname = ecut_seq.ecuts_input_variable_name
                        varvalue = await ecut_seq.scf_converged_ecut
                        input_variables[varname] = varvalue[varname]
                else:
                    varname = seq.ecuts_input_variable_name
                    varvalue = await seq.scf_converged_ecut
                    input_variables[varname] = varvalue[varname]
        if use_converged_phonon_smearing:
            if not self._compute_phonon_smearing_convergence:
                if self.converged_phonon_smearing is not None:
                    input_variables.update(self.converged_phonon_smearing)
                else:
                    raise ValueError(
                        "Need to set 'phonon_smearing_convergence' to True "
                        "or 'converged_phonon_smearing' upon init.")
            else:
                seq = self.phonon_smearing_convergence_sequencer
                if not seq.sequence_completed:
                    self._logger.info(
                            "Need to wait for 'phonon_smearing_convergence' to"
                            " complete.")
                    self.stop_at_workflow = "phonon_smearing_convergence"
                    return
                input_variables.update(seq.converged_smearing_variables)

    async def _use_geometry_from(
            self, workflow_part, input_variables, sequencer):
        if workflow_part == "relaxation":
            if not self._compute_relaxation:
                if self.relaxed_geometry is not None:
                    input_variables.update(self.relaxed_geometry)
                    return
                else:
                    raise ValueError(
                            "Need to set 'relaxation' to True or "
                            "'relaxed_geometry' upon init.")
            if not await self.workflow_part_completed("relaxation"):
                return
            # if we're here, relaxation part of the workflow successfully done
            # pop out all geometry variables in case they are not updated
            for var in self._all_geometry_variables:
                if var in input_variables:
                    input_variables.pop(var)
            input_variables.update(
                    await self.relaxation_sequencer.relaxed_geometry_variables)
        else:
            if sequencer is None:
                raise DevError("Need sequencer.")
            # we use different workflow part for geometry
            # assume we load from scf part
            prev_sequencer = self.get_sequencer(workflow_part)
            if "scf_" not in (
                    prev_sequencer._all_sequencer_prefixes) or not hasattr(
                    prev_sequencer, "scf_load_geometry_from"):
                raise ValueError(
                        f"Cannot load geometry from '{workflow_part}'.")
            # TODO: generalize loading geometry from to any other calc
            if not hasattr(sequencer, "scf_load_geometry_from"):
                raise NotImplementedError(
                        f"{sequencer} does not support the 'scf_load_"
                        "geometry_from' property.")
            if prev_sequencer.scf_load_geometry_from is not None:
                # use this instead
                sequencer.scf_load_geometry_from = (
                        prev_sequencer.scf_load_geometry_from)
            else:
                await prev_sequencer.init_sequence()
                # take geometry from first scf calc in case there are many
                scf_calcs = prev_sequencer.get_sequence_calculation(
                        "scf_", always_return_list=True)
                sequencer.scf_load_geometry_from = scf_calcs[0]

    async def _post_process_dos(self) -> None:
        if not self._compute_band_structure:
            return
        await self._create_merged_band_structure_and_dos_plot()

    async def _create_merged_band_structure_and_dos_plot(self) -> None:
        if not await self.dos_sequencer.sequence_completed:
            return
        if not await self.band_structure_sequencer.sequence_completed:
            return
        if not self.band_structure_sequencer.has_been_run:
            # generate bs if needed
            await self.run_band_structure()
        self._logger.info("Merging Band Structure + DOS plots.")
        # load plots
        bs_plot = self.band_structure_sequencer.band_structure_plot
        dos_plot = self.dos_sequencer.dos_plot
        dos_plot.ylabel = ""
        multi_plot = MultiPlot(loglevel=self._loglevel)
        multi_plot.add_plot(bs_plot, row=0)
        multi_plot.add_plot(dos_plot, row=0)
        multi_plot.align_yaxes(0)
        # apply other plot parameters
        self.dos_sequencer.apply_plot_calculation_parameters(multi_plot)
        # now show/save if needed (based on dos sequencer)
        show = self.dos_sequencer.plot_calculation_parameters.get(
                "show", True)
        save = self.dos_sequencer.plot_calculation_parameters.get(
                "save", None)
        multi_plot.plot(show=show)
        if save is not None:
            save_pdf = save.rstrip(".pdf") + "_with_band_structure.pdf"
            save_pickle = (
                    save.rstrip(".pickle") + "_with_band_structure.pickle")
            await multi_plot.save(save_pdf, overwrite=True)
            await multi_plot.save_pickle(save_pickle, overwrite=True)


class SequencersList(BaseUtility):
    """Container for sequencers.

    Can launch all sequencers in it and access
    and set a bunch of global properties to all sequencers it contains.
    """

    _loggername = "SequencersList"

    def __init__(
            self,
            sequencers: Sequence = None,
            bypass_sequence_comparison: bool = False,
            **kwargs):
        super().__init__(**kwargs)
        if sequencers is None:
            sequencers = []
        if not is_list_like(sequencers):
            raise TypeError(
                    f"Expected list-like but got: '{sequencers}'.")
        self.sequencers = sequencers
        self._root_workdir = None
        self.bypass_sequence_comparison = bypass_sequence_comparison

    @property
    def root_workdir(self) -> None:
        """The root workdir for all sequencers in the list if relevent."""
        if self._root_workdir is not None:
            return self._root_workdir
        raise ValueError("No root_workdir set for all sequencers.")

    @root_workdir.setter
    def root_workdir(self, root: str) -> None:
        self._root_workdir = root

    def __bool__(self):
        if self.sequencers:
            return True
        return False

    def __iter__(self):
        for sequencer in self.sequencers:
            yield sequencer

    def __add__(self, sequencers_list):
        # adding a list of sequencers to this one
        if isinstance(sequencers_list, SequencersList):
            sequencers_list = sequencers_list.sequencers
        return SequencersList(
                self.sequencers + sequencers_list,
                bypass_sequence_comparison=self.bypass_sequence_comparison,
                )

    def __len__(self):
        return len(self.sequencers)

    def __getitem__(self, item):
        return self.sequencers[item]

    def __radd__(self, sequencers_list):
        # adding this object to another list
        if isinstance(sequencers_list, SequencersList):
            sequencers_list = sequencers_list.sequencers
        return SequencersList(sequencers_list + self.sequencers)

    def __repr__(self):
        return ("< SequencersList: [\n" + "\n".join(
            [repr(x) for x in self]) +
            "\n] >")

    def __setitem__(self, item, value):
        self.sequencers[item] = value

    @property
    def bypass_convergence_check(self):
        """Set to True to bypass convergence checks."""
        return self[0].bypass_convergence_check

    @bypass_convergence_check.setter
    def bypass_convergence_check(self, bypass):
        for sequencer in self:
            sequencer.bypass_convergence_check = bypass

    @async_property
    async def sequence_completed(self):
        """Return True if all sequencers have completed successfully."""
        return all(
                await asyncio.gather(*(seq.sequence_completed for seq in self))
                )

    def append(self, sequencer):
        """Append a sequencer to the list."""
        if self.bypass_sequence_comparison:
            sequencer.bypass_sequence_comparison = (
                    self.bypass_sequence_comparison)
        self.sequencers.append(sequencer)

    async def clean(self):
        """Cleans all sequencers in the list."""
        await asyncio.gather(*(seq.clean() for seq in self))

    async def clear_sequence(self):
        """Clears sequence for all Sequencers in the list."""
        await asyncio.gather(*(seq.clear_sequence() for seq in self))

    async def init_sequence(self):
        """Initialize the sequence of all sequencers in the list."""
        await asyncio.gather(*(seq.init_sequence() for seq in self))

    async def write(self):
        """Write all sequencers."""
        for sequencer in self:
            await sequencer.write()

    async def run(self):
        """Run all sequencers."""
        # run 1 at a time to make sure not too many are submitted at same time
        for sequencer in self:
            try:
                await sequencer.run()
            except TooManyCalculationsInQueueError:
                return

    def reset(self):
        """Resets SequencersList to an empty one."""
        self.sequencers = []
