import asyncio
import datetime
import os

from tabulate import tabulate

import tqdm

from .job import QueuedJob
from .queue_parsers import PBSQueueParser, SlurmQueueParser
from .. import __PWD__, __USER_CONFIG__
from ..bases import BaseUtility
from ..colors import Colors
from ..handlers import CalculationDirectory
from ..status_checkers.bases import __ERROR_STRING__


__QUEUING_SYSTEM_TO_QUEUE_PARSER__ = {
                "local": None,
                None: None,  # weird but it works...
                "slurm": SlurmQueueParser,
                "pbs_professional": PBSQueueParser,
                "torque": PBSQueueParser,
                }
__TIME_BETWEEN_CHECKS__ = 120  # in seconds
__MAX_WORKDIR_LEN__ = 85


class Queue(BaseUtility):
    """Object that represents the queue of a queuing system."""

    _loggername = "Queue"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.jobs = []
        self.njobs = 0
        self.has_been_obtained = False
        self.has_been_read_at = None
        self._username = None
        self._queue_command = None
        self.queuing_system = __USER_CONFIG__.SYSTEM.queuing_system
        self.lock = False

    def __iter__(self):
        for job in self.jobs:
            yield job

    def __contains__(self, item):
        if isinstance(item, QueuedJob):
            return item in self.jobs
        elif isinstance(item, str):
            # probably a path
            return item in [j.workdir for j in self.jobs]
        elif isinstance(item, CalculationDirectory):
            return item.run_directory.path in [j.workdir for j in self.jobs]
        return False

    async def __aenter__(self):
        while self.lock:
            # the queue is being read already
            # wait periodically until it is being released
            await asyncio.sleep(0.25)
        if not self.has_been_obtained:
            await self.read_queue()
        delta = (datetime.datetime.now() - self.has_been_read_at).seconds
        if delta > __TIME_BETWEEN_CHECKS__:
            await self.read_queue()
        return self

    async def __aexit__(self, exc_type, exc_value, traceback):
        pass

    def __getitem__(self, item):
        if item not in self:
            raise KeyError(item)
        if isinstance(item, CalculationDirectory):
            for job in self.jobs:
                if job.workdir == item.run_directory.path:
                    return job
        elif isinstance(item, str):
            for job in self.jobs:
                if job.workdir == item:
                    return job
        raise KeyError(item)

    @property
    def queue_parser(self):
        """Return the queue parser object associated with this system."""
        qp = __QUEUING_SYSTEM_TO_QUEUE_PARSER__[self.queuing_system]
        if qp is None:
            raise NotImplementedError(self.queuing_system)
        return qp

    @property
    def username(self):
        """Return the username."""
        if self._username is not None:
            return self._username
        # set default username
        username = __USER_CONFIG__.SYSTEM.username
        if username is None:
            raise ValueError(
                    "username in config file is None. Set it or set a username"
                    " to the Queue object.")
        self.username = username
        return self.username

    @username.setter
    def username(self, username):
        self._username = username

    def cancel_job(self, job):
        """Cancels a running job.

        Parameters
        ----------
        job: QueuedJob instance, str or CalculationDirectory instance
            The job to cancel.
        """
        if not isinstance(job, QueuedJob):
            job = self[job]
        job.cancel()

    async def print(self, **kwargs):  # noqa: T002
        """Print the queue to the terminal in a fancy table.

        Uses the tabulate module.

        Parameters
        ----------
        All kwargs are passed to the tabulate object.
        """
        tbl = await self._get_table(**kwargs)
        print(tbl)  # noqa: T001

    async def read_queue(self):
        """Read the queue and store queuing data."""
        self._logger.info("Reading system queue.")
        self.lock = True
        parser = self.queue_parser(loglevel=self._loglevel)
        parser.username = self.username
        await parser.read_queue()
        self.jobs = parser.jobs
        self.njobs = len(self.jobs)
        self.has_been_read_at = datetime.datetime.now()
        self.has_been_obtained = True
        self.lock = False  # release lock

    async def _get_table(
            self, workdir_shortening=True, show_progress=False, **kwargs):
        """Construct the queue table.

        Parameters
        ----------
        show_progress: bool, optional
            If True, a progress bar is added when computing the table.
        workdir_shortening: bool, optional
            If True and the length of a workdir path is longer than
            40 characters, we shorten it with a '[...]' symbol.
        """
        # actually create the table
        async with self:
            table = []
            iterable = enumerate(self.jobs)
            if show_progress:
                iterable = tqdm.tqdm(
                        iterable, desc="computing queue status",
                        total=len(self.jobs), unit="calculation")
            for ijob, job in iterable:
                workdir = job.workdir
                true_workdir = workdir
                if os.path.basename(workdir) == "run":
                    workdir = os.path.dirname(workdir)
                    true_workdir = workdir
                if len(true_workdir) > __MAX_WORKDIR_LEN__:
                    # try shortening it with relative path
                    true_workdir = os.path.relpath(true_workdir, __PWD__)
                if len(true_workdir) > __MAX_WORKDIR_LEN__ and (
                        workdir_shortening):
                    true_workdir = (
                            workdir[:__MAX_WORKDIR_LEN__ // 2] + "[...]" +
                            workdir[-__MAX_WORKDIR_LEN__ // 2:])
                status = job.status
                if status == "RUNNING":
                    # check actual calc status in case it has errored
                    calc = await job.calculation_directory
                    calc_stat = await calc.status
                    status = Colors.color_text(status, "yellow", "bold")
                    if job.jobname != "interactive":
                        # check if calc has errored
                        async with calc:
                            calc_stat = await calc.status
                            if calc_stat["calculation_finished"] == (
                                    __ERROR_STRING__):
                                status += "/" + Colors.color_text(
                                        "ERRORED", "red", "bold")
                elif status == "PENDING":
                    status = Colors.color_text("NOT STARTED", "cyan", "bold")
                table.append(
                        [ijob, job.id, job.jobname, status, job.time_left,
                         job.nodes, job.proc, true_workdir])
        return tabulate(
                table,
                headers=["#", "id", "jobname", "status", "time_left",
                         "nodes", "cpus", "workdir"],
                **kwargs)
