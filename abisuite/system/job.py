import os

from async_property import async_property

from ..bases import BaseUtility
from ..handlers import CalculationDirectory


__ALL_JOB_ATTRIBUTES__ = [
        "id", "username", "account", "jobname", "nodes", "proc", "status",
        "time_left", "workdir",
        ]


class QueuedJob(BaseUtility):
    """Object that represents a queued (or running) job.

    For now it is just a data structure.
    """

    _loggername = "QueuedJob"

    def __init__(self, queue_parser, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # store some properties
        self._calculation_directory = None
        self.queue_parser = queue_parser
        for attr in __ALL_JOB_ATTRIBUTES__:
            setattr(self, attr, None)

    @async_property
    async def calculation_directory(self):
        """The job's calculation directory object."""
        if self._calculation_directory is not None:
            return self._calculation_directory
        self._calculation_directory = await (
                CalculationDirectory.from_calculation(
                    os.path.dirname(self.workdir), loglevel=self._loglevel)
                )
        return self._calculation_directory

    # alias
    @property
    def tasks(self):
        """Alias for proc."""
        return self.proc

    @tasks.setter
    def tasks(self, tasks):
        self.proc = tasks

    def cancel(self):
        """Cancel job."""
        self.queue_parser.cancel_job(self.id)
