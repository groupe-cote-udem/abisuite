import asyncio
import time
from typing import Any, Sequence

from .bases import BaseUtility
from .routines import is_dict_like, is_list_like


class ReTryer(BaseUtility):
    """Utility functions to retry calling a function in case it fails."""

    _loggername = "ReTryer"

    def __init__(
            self, func: callable,
            func_args: Sequence[Any] = None,
            func_kwargs: dict[str, Any] = None,
            max_retries: int = 5,
            allowed_exceptions: Sequence[Exception] = None,
            delay: float = 0,
            **kwargs,
            ):
        """Init method.

        Parameters
        ----------
        func: callable
            The function to call multiple times.
        func_args: list-like, optional
            The list of args to pass to the function when calling it.
        func_kwargs: dict-like, optional
            The dict of kwargs to pass to the function when calling it.
        max_retries: int, optional
            The maximal number of retries.
        allowed_exceptions: list-like, optional
            The allowed exception types to happen. If None, all exceptions
            are allowed.
            Basically this would set this parameter to [Exception].
        delay: float, optional
            If given, this gives the time delay in seconds between each tries.

        Raises
        ------
        TypeError:
            If func is not callable or if allowed_exceptions it not list-like
            of if func_args is not list-like or
            if func_kwargs is not dict-like.
            If delay is <= 0 then no delay is applied.
        """
        super().__init__(**kwargs)
        if allowed_exceptions is None:
            allowed_exceptions = (Exception, )
        if not is_list_like(allowed_exceptions):
            raise TypeError("'allowed_exceptions' must be list-like.")
        allowed_exceptions = tuple(allowed_exceptions)
        if func_args is None:
            func_args = []
        if func_kwargs is None:
            func_kwargs = {}
        if not is_list_like(func_args):
            raise TypeError("'func_args' should be list-like.")
        if not is_dict_like(func_kwargs):
            raise TypeError("'func_kwargs' should be dict-like.")
        if not callable(func):
            raise TypeError("'func' should be callable.")
        self.func = func
        self.func_args = func_args
        self.func_kwargs = func_kwargs
        self.max_retries = max_retries
        self.allowed_exceptions = allowed_exceptions
        self.delay = delay

    def execute_sync(self) -> None:
        """Executes the function with retries synchronously.

        Returns
        -------
        Whatever the func returns if it returns anything.
        """
        if asyncio.iscoroutinefunction(self.func):
            raise TypeError(
                    "Func is not a coroutine: use awaitable execute() method.")
        itry = 0
        while itry < self.max_retries:
            try:
                return self.func(*self.func_args, **self.func_kwargs)
            except self.allowed_exceptions:
                itry += 1
                if itry == self.max_retries:
                    raise
                if self.delay > 0:
                    time.sleep(self.delay)

    async def execute(self) -> None:
        """Executes the function with retries.

        Returns
        -------
        Whatever the func returns if it returns anything.
        """
        if not asyncio.iscoroutinefunction(self.func):
            return self.execute_sync()
        itry = 0
        while itry < self.max_retries:
            try:
                return await self.func(*self.func_args, **self.func_kwargs)
            except self.allowed_exceptions:
                itry += 1
                if itry == self.max_retries:
                    raise
                if self.delay > 0:
                    await asyncio.sleep(self.delay)

    @classmethod
    async def try_with_retries(cls, *args, **kwargs) -> Any:
        """Try with retries an async function.

        See the :meth:`__init__<.__init__>`'s method docstring for details
        on arguments of this function.
        """
        retryer = cls(*args, **kwargs)
        return await retryer.execute()

    @classmethod
    def try_with_retries_sync(cls, *args, **kwargs) -> Any:
        """Synchronous version of :meth:`<.try_with_retries>`."""
        retryer = cls(*args, **kwargs)
        return retryer.execute_sync()


# Some aliases
try_with_retries = ReTryer.try_with_retries
try_with_retries_sync = ReTryer.try_with_retries_sync
