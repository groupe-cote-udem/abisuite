import abc
import os
from typing import Any, Sequence

import matplotlib

from mpl_toolkits.axes_grid1.inset_locator import inset_axes

import numpy as np

import seaborn as sns

from ..bases import BaseUtility
from ..routines import (
        full_abspath, is_list_like,
        is_scalar,
        is_scalar_or_str,
        sort_data as fsort, suppress_warnings,
        )


def flatten_out_single_element_axes(
        array: Sequence | np.ndarray) -> np.ndarray:
    """Flatten out single element axes of a numpy array."""
    # if array is already 1D dont reshape
    array = np.asarray(array)
    if len(array.shape) == 1:
        return array
    newshape = []
    for dim in array.shape:
        if dim <= 1:
            continue
        newshape.append(dim)
    return array.reshape(newshape)


class _BasePlotObject(BaseUtility, abc.ABC):

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):
        """Create the plot on the axis object."""
        # must be overidden
        pass

    def update(self):
        """Update axis data with current stored data."""
        raise NotImplementedError()


class BaseCurve(_BasePlotObject, abc.ABC):
    """Base class for curves.

    Note
    ----
    This class is not meant to be used directly by user. Use it's child
    instead.
    """

    equal_xdata_and_ydata = True

    def __init__(
            self,
            xdata: Sequence[float | int],
            ydata: Sequence[float | int],
            zdata: Sequence[float | int] = None,
            semilogx: bool = False,
            semilogy: bool = False,
            loglog: bool = False,
            **kwargs
            ):
        """Initialize base curve.

        Parameters
        ----------
        marker : str, optional
            The marker for the ydata. If None, no marker is applied.
        markersize : float, optional
            The marker size.
        markerfacecolor : str, optional
            The marker face color.
        zorder: float, optional
            Gives the zorder of the Curve type.
        semilogy : bool, optional
            If True, the ydata is plot on a semilog axis instead of a
            normal axis.
        semilogx : bool, optional
            Same as semilogy but for xaxis.
        loglog : bool, optional
            Same as semilogy but for both axis. If semilogx and semilogy
            are both True at the same time: loglog will be set to True.
        """
        loglevel = kwargs.pop("loglevel", None)
        self._set_defaults(**kwargs)
        super().__init__(loglevel=loglevel)
        self._xdata, self._ydata, self._zdata = None, None, None
        self.xdata, self.ydata = xdata, ydata
        if self.equal_xdata_and_ydata:
            if len(self.xdata) != len(self.ydata):
                raise ValueError(
                        f"xdata {self.xdata.shape} and ydata "
                        f"{self.ydata.shape} must be same length!")
        if zdata is not None:
            self.zdata = zdata
        self.lines = None  # data returned by matplotlib
        self.semilogy = semilogy
        self.semilogx = semilogx
        self.loglog = loglog
        if semilogy and semilogx:
            # use loglog instead
            self.loglog = True

    @property
    def xdata(self):
        """Return the xdata array."""
        return self._xdata

    @xdata.setter
    def xdata(self, xdata):
        self._xdata = self._check_list(xdata)

    @property
    def ydata(self):
        """Return the ydata array."""
        return self._ydata

    @ydata.setter
    def ydata(self, ydata):
        self._ydata = self._check_list(ydata)

    @property
    def zdata(self):
        """Return the zdata array."""
        return self._zdata

    @zdata.setter
    def zdata(self, zdata):
        # NOTE: don't do check as data might be more than 2D (color img)
        zdata = self._check_list(zdata)
        if len(zdata) != len(self.xdata):
            raise ValueError("zdata must be same len as x and y data!")
        self._zdata = zdata

    def _set_defaults(
            self, **kwargs,
            ):
        pairs = (
                 ("offset", None),
                 ("zdir", None),
                 ("alpha", 1.0),
                 ("label", None),
                 ("markersize", 1),
                 ("marker", None),
                 ("markerfacecolor", "C0"),
                 ("zorder", None),
                 ("markeredgewidth", 1),
                 )
        defaults = {
                attr: kwargs.pop(attr, default) for attr, default in pairs}
        for attr, val in defaults.items():
            if not hasattr(self, attr):
                setattr(self, attr, val)
        # exception for color as it was a dunder attribute before
        if not hasattr(self, "_color"):
            if not hasattr(self, "color"):
                self.color = kwargs.pop("color", "C0")
        else:
            if self._color is not None:
                if "color" in kwargs:
                    # disregard old value
                    self.color = kwargs.pop("color")
                else:
                    # take old value
                    self.color = self._color

    def copy(self):
        """Copy the object."""
        zdata = None
        if self.zdata is not None:
            zdata = self.zdata.copy()
        new = self.__class__(
                self.xdata.copy(), self.ydata.copy(), zdata,
                loglevel=self._loglevel)
        new.color = self.color
        new.alpha = self.alpha
        new.label = self.label
        new.offset = self.offset
        new.zdir = self.zdir
        new.marker = self.marker
        new.markerfacecolor = self.markerfacecolor
        new.markersize = self.markersize
        new.semilogx = self.semilogx
        new.semilogy = self.semilogy
        new.loglog = self.loglog
        return new

    def clear_label(self):
        """Sets the curve label to None."""
        self.label = None

    def reset(self):
        """Reset lines objects that were created when plotting."""
        del self.lines
        self.lines = None

    def _check_list(self, data: Sequence) -> np.ndarray:
        # check if arguments are array-like and return numpy arrays
        if not is_list_like(data):
            raise TypeError("Argument should be array-like.")
        data = flatten_out_single_element_axes(data)
        if len(data.shape) >= 2:
            raise TypeError("Argument is not equivalent to a 1D array!")
        return data


class Curve(BaseCurve):
    """Class that represents a curve."""

    _loggername = "Curve"

    def __init__(
            self, *args, linestyle="-",
            linewidth=1.0, normalize=False,
            xerr=None, yerr=None,
            sort_data=False, pickradius=5, fillstyle="full", capsize=0.0,
            use_transaxes_transform: bool = False, clip_on: bool = None,
            **kwargs):
        """Curve init method.

        Parameters
        ----------
        xdata : array-like
            x-data points.
        ydata : array-like
            y-data points.
        xerr: array-like, optional
            The error bars for x data.
        yerr: array-like, optional
            The error bars for y data.
        linestyle : str, optional
            Curve linestyle.
        alpha : float, optional
            The alpha value of the curve.
        color : str, optional
            Curve color.
        label : str, optional
            Curve label. None = no label.
        linewidth : float, optional
            Sets the width of the curves.
        normalize : bool, optional
            If True, the ydata is normalized by its maximum value.
        sort_data : bool, optional
            If True, xdata will be sorted and ydata too according to
            the sorting of xdata.
        pickradius : None or float,
            If A float, it tells pyplot how large the 'pickable' area is
            for a curve.
        capsize: float, optional
            If error bars are given, defines the size of the error bar caps
            in pts.
        """
        super().__init__(*args, **kwargs)
        if sort_data:
            self.sort_data()
        if linestyle is None:
            # matplotlib only accepts strings...
            linestyle = "None"
        self.linestyle = linestyle
        self.linewidth = linewidth
        self.xerr, self.yerr = xerr, yerr
        self.capsize = capsize
        self.use_transaxes_transform = use_transaxes_transform
        self.clip_on = clip_on
        if xerr is not None or yerr is not None:
            if any((self.semilogx, self.semilogy, self.loglog)):
                raise ValueError(
                        "Cannot have error bars with loglog or "
                        "semilog axes.")
            if self.zdata:
                raise ValueError("Cannot have 3D error bars.")
        self.pickradius = pickradius
        self.fillstyle = fillstyle
        if normalize:
            self.ydata = self.ydata / self.max

    def __truediv__(self, value):
        self._check_scalar(value)
        self.ydata = np.array(self.ydata, dtype=float) / value
        return self

    def __add__(self, value):
        self._check_scalar(value)
        self.ydata += value
        return self

    def __mul__(self, value):
        self._check_scalar(value)
        self.ydata = np.array(self.ydata) * value
        return self

    def __sub__(self, value):
        self._check_scalar(value)
        self.ydata -= value
        return self

    @property
    def max(self):
        """Return the max of the curve."""
        return np.max(self.ydata)

    @property
    def min(self):
        """Return the min of the curve."""
        return np.min(self.ydata)

    @property
    def wheremax(self):
        """Return what xdata corresponds to the max of the curve."""
        return self.xdata[np.where(self.ydata == self.max)[0]]

    @property
    def wheremin(self):
        """Return what xdata corresponds to the min of the curve."""
        return self.xdata[np.where(self.ydata == self.min)[0]]

    def copy(self):
        """Copy the curve object."""
        new = super().copy()
        new.linestyle = self.linestyle
        new.linewidth = self.linewidth
        new.pickradius = self.pickradius
        return new

    @suppress_warnings
    def plot_on_axis(self, axis):
        """Plot the curve on an axis."""
        args = [self.xdata,
                self.ydata]
        if self.zdata is not None:
            args.append(self.zdata)
        # make sure all attributes exists beforehand in case we load a pickle
        self._set_defaults()
        kwargs = {"linestyle": self.linestyle,
                  "color": self.color,
                  "label": self.label,
                  "linewidth": self.linewidth,
                  "marker": self.marker,
                  "alpha": self.alpha,
                  "zorder": self.zorder,
                  # "pickradius": self.pickradius,
                  "fillstyle": self.fillstyle,
                  "markersize": self.markersize,
                  "markeredgewidth": self.markeredgewidth,
                  "markerfacecolor": self.markerfacecolor}
        if hasattr(self, "use_transaxes_transform"):
            if self.use_transaxes_transform:
                kwargs["transform"] = axis.transAxes
        if hasattr(self, "clip_on"):
            if self.clip_on is not None:
                kwargs["clip_on"] = self.clip_on
        if not hasattr(self, "xerr"):
            self.xerr, self.yerr = None, None
        if self.xerr is not None:
            kwargs.update({"xerr": self.xerr,
                           "capsize": self.capsize})
        if self.yerr is not None:
            kwargs.update({"yerr": self.yerr,
                           "capsize": self.capsize})
        if "yerr" in kwargs or "xerr" in kwargs:
            self.lines = axis.errorbar(*args, **kwargs)
        elif self.loglog:
            # important to call this first because if semilogx-y are both True,
            # loglog wil be set to True
            self.lines = axis.loglog(*args, **kwargs)
        elif self.semilogy and not self.loglog:
            self.lines = axis.semilogy(*args, **kwargs)
        elif self.semilogx and not self.loglog:
            self.lines = axis.semilogx(*args, **kwargs)
        else:
            self.lines = axis.plot(*args, **kwargs)

    def _do_plot_on_axis(self, method, *args, **kwargs):
        return method(*args, **kwargs)

    def sort_data(self):
        """Sort x and y data."""
        self.xdata, self.ydata = fsort(self.xdata, self.ydata)

    def update(self):
        """Update the curve properties on the axis object."""
        line = self.lines[0]
        for prop in ("xdata", "ydata", "color", "label", "linewidth",
                     "marker", "alpha",  # "pickradius",
                     "fillstyle",
                     "markersize", "markerfacecolor"):
            getattr(line, f"set_{prop}")(getattr(self, prop))
        if self.zdata is not None:
            line.set_3d_properties(self.zdata, zdir="z")

    def _check_scalar(self, value):
        if not is_scalar(value):
            raise TypeError(
                    "Arithmetic operations only with int or floats but "
                    f"tried with: '{value}' which has type '{type(value)}'.")


class _SingledValueCoordinates(BaseCurve):
    """Base class for plot objects with single value coordinates."""

    def _sanitize_single_value_coordinate(
            self, val: float, label: str, allow_vector: bool = False,
            ) -> float:
        if is_scalar_or_str(val):
            return [val]
        if len(val) > 1 and not allow_vector:
            raise ValueError(
                    f"{label} must be an array of length 1 or scalar.")
        return val


class Arrow(_SingledValueCoordinates):
    """Class that represents an arrow object."""

    _loggername = "Arrow"

    def __init__(
            self, xdata: float, ydata: float, dx: float, dy: float,
            *args,
            text: str = "", size: float = 10,
            width: float = None, head_width: float = None,
            head_length: float = None,
            facecolor: str = "C0", edgecolor: str = "C0",
            **kwargs,
            ) -> None:
        """Arrow init method.

        Parameters
        ----------
        xdata: float
            The x value of the arrow origin.
        ydata: float
            The y value of the arrow origin.
        dx, dy: float
            The length and height of the arrow (includes head).
        text: str, optional
            The text at the bottom of the arrow.
        size: float, optional
            Text size.
        width: float, optional
            The arrow tail width.
        head_width: float, optional
            The arrow head width.
        head_length: float, optional
            The arrow head length.
        facecolor: str, optional
            The arrow facecolor.
        edgecolor: str, optional
            The arrow edgecolor.
        """
        xdata = self._sanitize_single_value_coordinate(xdata, "xdata")
        ydata = self._sanitize_single_value_coordinate(ydata, "ydata")
        super().__init__(xdata, ydata, *args, **kwargs)
        self.dx = dx
        self.dy = dy
        self.width = width
        self.head_width = head_width
        self.head_length = head_length
        self.text = text
        self.facecolor = facecolor
        self.edgecolor = edgecolor
        self.size = size

    def plot_on_axis(self, axis) -> None:
        """Plot arrow on axis.

        Uses the `annotate` pyplot method in order to have a square
        arrowhead.
        """
        xytext = [self.xdata[0], self.ydata[0]]  # start of arrow
        xy = np.array(xytext) + np.array([self.dx, self.dy])  # end of arrow
        arrowprops = {
                "fc": self.facecolor, "ec": self.edgecolor,
                }
        if self.width is not None:
            arrowprops["width"] = self.width
        if self.head_width is not None:
            arrowprops["headwidth"] = self.head_width
        if self.head_length is not None:
            arrowprops["headlength"] = self.head_length
        axis.annotate(
                self.text, xy=xy,  # start of arrow
                xytext=xytext, size=self.size,
                arrowprops=arrowprops,
                )


class Text(_SingledValueCoordinates):
    """Class that represents a text object."""

    _loggername = "Text"

    def __init__(
            self, text: str, xdata: float, ydata: float,
            zdata: float = None, fontsize: float | str = None,
            rotation: float | str = None,
            fontweight: float | str = None,
            **kwargs):
        """Text init method.

        Parameters
        ----------
        text: str
            The text to display.
        xdata: float
            The x-position of the text.
        ydata: float
            The y-position of the text.
        zdata: float, optional
            The z-position of the text for 3D plots.
        rotation: float, str ('vertical', 'horizontal'), optional
            Rotation of the text. Can be 'vertical' or 'horizontal'.
            Otherwise float values are interpreted as an angle from
            0 to 360 anticlockwise. Negative values turn clockwise.
        fontsize: float, str {'xx-small', 'x-small', 'small', ...}
            The fontsize for the text.
        fontweight: float, str {'bold', 'extra-bold', ...}
            The font weight. See pyplot docs for available options.
        """
        xdata = self._sanitize_single_value_coordinate(xdata, "xdata")
        ydata = self._sanitize_single_value_coordinate(ydata, "ydata")
        if zdata is not None:
            zdata = self._sanitize_single_value_coordinate(zdata, "zdata")
        super().__init__(xdata, ydata, zdata=zdata, **kwargs)
        self.text = text
        self.rotation = rotation
        self.fontsize = fontsize
        self.fontweight = fontweight

    def plot_on_axis(self, axis):
        """Plot the text on a matplotlib axis."""
        args = [self.xdata[0], self.ydata[0]]
        if self.zdata is not None:
            args.append(self.zdata[0])
        args.append(self.text)
        args.append(self.zdir)
        kwargs = {
                "color": self.color, "rotation": self.rotation,
                }
        for prop in ["fontsize", "fontweight"]:
            if getattr(self, prop) is not None:
                kwargs[prop] = getattr(self, prop)
        self.lines = axis.text(*args, **kwargs)
        return self.lines

    def update(self):
        """Update text data on the axis."""
        if self.zdata is None:
            self.lines.set_position((self.xdata[0], self.ydata[0]))
        else:
            self.lines.set_position(
                    (self.xdata[0], self.ydata[0], self.zdata[0]))
        self.lines.set_color(self.color)
        self.lines.set_text(self.text)


class FillBetween(BaseCurve):
    """Class that represents a filled area between two curves."""

    _loggername = "FillBetween"

    def __init__(
            self, xdata, ydata1, ydata2, color="r",
            where: Sequence[bool | int] = None,
            vertical_filling: bool = False,
            **kwargs):
        """Init method.

        Parameters
        ----------
        xdata : array-like
                x-data points.
        ydata1, ydata2 : array-like
                         The y coordinates of the two curves to fill between.
        where: array-like, optional
            Index array where x values are not colored.
        color : str, optional
                The color of the filled area.
        vertical_filling: bool, optional
            If True, we fill the area between vertical curves instead
            of horizontal ones. In this case, the meaning of
            xdata, ydata and ydata2 are reversed.
        """
        super().__init__(xdata, ydata1, **kwargs)
        self._ydata2 = None
        self.where = where
        self.ydata2 = ydata2
        self.color = color
        self.vertical_filling = vertical_filling

    @property
    def ydata2(self):
        """Return 2nd ydata array."""
        return self._ydata2

    @ydata2.setter
    def ydata2(self, ydata2):
        self._ydata2 = self._check_list(ydata2)

    def plot_on_axis(self, axis):
        """Plot fill between on the axis."""
        func = axis.fill_between
        if self.vertical_filling:
            func = axis.fill_betweenx
        self.lines = func(
                self.xdata, self.ydata, self.ydata2, color=self.color,
                label=self.label, alpha=self.alpha,
                where=self.where,
                )
        return self.lines

    def update(self):
        """Update the fill between on the axis."""
        # FIXME: THIS IS NOT WORKING PROPERLY
        return
        # delete the collection directly from the figure since
        # deleting the reference is not sufficient
        # loop over all axes in case we have a multiplot
        to_del = None
        axis_idx = None
        for iax, axis in enumerate(self.lines.get_figure().axes):
            for ic, collection in enumerate(axis.collections):
                if collection.get_label() == self.lines.get_label():
                    # delete this one
                    to_del = ic
                    axis_idx = iax
                    break
            if to_del is not None:
                break
        else:
            raise LookupError()
        axis = self.lines.get_figure().axes[axis_idx]
        del axis.collections[to_del]
        # redraw
        self.plot_on_axis(axis)
        # swap collection to previous position in order to keep ordering
        (axis.collections[ic],
         axis.collections[-1]) = (axis.collections[-1], axis.collections[ic])


class Histogram(_SingledValueCoordinates):
    """Class that represents a Histogram.

    xdata is the bins and ydata is the data to put in a histogram.
    If xdata is an array, the bins are that array. Can be a single integer
    where it would correspond to the number of bins.
    """

    equal_xdata_and_ydata = False
    _loggername = "Historam"

    def __init__(
            self, xdata: Sequence[float] | float, *args, **kwargs,
            ) -> None:
        """Histogram init method.

        Parameters
        ----------
        xdata: int or Sequence[floats]
            The number of bins or the bin edges.
        """
        xdata = self._sanitize_single_value_coordinate(
                xdata, "xdata", allow_vector=True)
        super().__init__(xdata, *args, **kwargs)

    def plot_on_axis(self, axis):
        """Plot the histogram on the axis."""
        bins = self.xdata
        if len(self.xdata) == 1:
            # given a number of bins
            # make sure it is an integer
            bins = int(self.xdata[0])
        self.lines = axis.hist(
                self.ydata, bins=bins, label=self.label,
                color=self.color,
                )

    def update(self):
        """Update the histogram data on the axis."""
        raise NotImplementedError()


DEFAULT_IMAGE_CMAP = sns.color_palette("light:b", as_cmap=True)


class Image(BaseCurve):
    """Class that represents an image in order to plot it on an axis.

    xdata and ydata are only the boundaries of the actual x and y data.
    """

    _loggername = "Image"
    equal_xdata_and_ydata = False

    def __init__(
            self, xdata: Sequence[float | int], ydata: Sequence[float | int],
            zdata: Sequence[Sequence[float | int]],
            cmap: Any = DEFAULT_IMAGE_CMAP,
            interpolation="none",
            origin="lower", **kwargs):
        super().__init__(xdata, ydata, zdata, **kwargs)
        if len(self.xdata) != len(self.ydata):
            # take only boundaries
            self.xdata = [self.xdata[0], self.xdata[-1]]
            self.ydata = [self.ydata[0], self.ydata[-1]]
            self._logger.info(
                    "Different arrays have been passed to Image. "
                    "Taking only boundaries.")
        # else:
        #     super().__init__(xdata, ydata, zdata, **kwargs)
        self.cmap = cmap
        self.interpolation = interpolation
        self.origin = origin

    @property
    def ax_image(self):
        """Return the axis image."""
        # alias
        return self.lines

    @ax_image.setter
    def ax_image(self, image):
        self.lines = image

    @BaseCurve.zdata.setter
    def zdata(self, zdata):
        """Return the image data."""
        # NOTE: don't do check as data might be more than 2D (color img)
        if not isinstance(zdata, np.ndarray):
            zdata = np.array(zdata, dtype=float)
        self._zdata = zdata

    def plot_on_axis(self, axis):
        """Plot the image on an axis."""
        self.ax_image = axis.imshow(
                self.zdata, cmap=self.cmap, origin=self.origin,
                extent=[self.xdata[0], self.xdata[-1],
                        self.ydata[0], self.ydata[-1]],
                interpolation=self.interpolation)
        return self.ax_image

    def update(self):
        """Irrelevent."""
        raise RuntimeError("Cannot update an image.")


class PNGImage(Image):
    """Class that represents a png image."""

    _loggername = "PNGImage"

    def __init__(
            self, path: str, *args,
            xmin: float = 0, xmax: float = 1, ymin: float = 0, ymax: float = 1,
            origin: str = "lower", reverse: bool = True,
            asinset: bool = False,
            inset_x: float = None, inset_y: float = None,
            inset_w: float = None, inset_h: float = None,
            inset_loc: str | int = 1,
            **kwargs) -> None:
        """Init method.

        Parameters
        ----------
        path : str
            the path to the png image.
        xmin, xmax, ymin, ymax : float, optional
            The boundaries of the data. Each pixel coordinates will be rescaled
            according to these values.
        reverse : bool, optional
            if True, the image is reversed after being read.
        """
        self.path = full_abspath(path)
        self.asinset = asinset
        self.inset_x, self.inset_y = inset_x, inset_y
        self.inset_w, self.inset_h = inset_w, inset_h
        self.inset_loc = inset_loc
        if not os.path.isfile(self.path):
            raise FileNotFoundError(self.path)
        img = matplotlib.image.imread(self.path)
        shape = img.shape
        n_xs = shape[0]
        n_ys = shape[1]
        if reverse:
            img = np.flip(img, axis=0)
        super().__init__(
                np.linspace(xmin, xmax, n_xs),
                np.linspace(ymin, ymax, n_ys),
                img, *args, origin=origin, **kwargs)

    def plot_on_axis(self, axis, *args, **kwargs):
        """Plot image on axis."""
        if not self.asinset:
            super().plot_on_axis(axis, *args, **kwargs)
            return
        axin = inset_axes(
                axis, width=self.inset_w, height=self.inset_h,
                loc=self.inset_loc,
                )
        # axin = axis.inset_axes(
        #         [self.inset_x, self.inset_y, self.inset_w, self.inset_h],
        #         # transform=axis.transData,
        #         )
        super().plot_on_axis(axin, *args, **kwargs)
        axin.axis("off")
        # axin.imshow(arr_image)

    def update(self):
        """Update the image on the axis."""
        raise NotImplementedError()


class Scatter(BaseCurve):
    """Class that represents a scatter plot to plot on an axis."""

    _loggername = "Scatter"

    def __init__(self, *args, edgecolors="face", **kwargs):
        super().__init__(*args, **kwargs)
        self.edgecolors = edgecolors

    def copy(self):
        """Copy the scatter object."""
        new = super().copy()
        new.edgecolors = self.edgecolors
        return new

    def plot_on_axis(self, axis):
        """Plot the scatter array on an axis."""
        args = [self.xdata, self.ydata]
        if self.zdata is not None:
            args.append(self.zdata)
        extra = {}
        if self.zorder is not None:
            extra["zorder"] = self.zorder
        self.lines = axis.scatter(
                *args, color=self.color, label=self.label,
                edgecolors=self.edgecolors, alpha=self.alpha,
                s=self.markersize,
                marker=self.marker,
                **extra,
                )
        if self.semilogy or self.loglog:
            axis.set_yscale("log")
        if self.semilogx or self.loglog:
            axis.set_xscale("log")
        return self.lines

    def update(self):
        """Update the scatter plot."""
        line = self.lines
        line.set_lw(self.markersize)
        line.set_ec(self.edgecolors)
        line.set_fc(self.color)
        line.set_label(self.label)
        if self.zdata is not None:
            line.set_offsets(np.array([self.xdata, self.ydata, self.zdata]).T)
        else:
            line.set_offsets(np.array([self.xdata, self.ydata]).T)


class Surface(Image):
    """Class that represents a 3D surface."""

    # ALL DATA MUST BE NxM (xdata, ydata, zdata)
    _loggername = "Surface"

    def __init__(self, *args, vmax=None, vmin=None, **kwargs):
        super().__init__(*args, **kwargs)
        if self.xdata.shape != self.ydata.shape:
            raise ValueError("xdata and ydata should agree in shapes.")
        if len(self.xdata.shape) == 1:
            # 1D data => create meshgrid
            self.xdata, self.ydata = np.meshgrid(self.xdata, self.ydata)
        # zdata must be same shape as this meshgrid
        # print(self.xdata.shape, self.zdata.shape)
        if self.xdata.shape != self.zdata.shape:
            raise ValueError("xdata and zdata should agree in shapes.")
        self.vmin = vmin
        self.vmax = vmax

    def plot_on_axis(self, axis):
        """Plot the surface on an axis."""
        self.ax_image = axis.plot_surface(
                self.xdata, self.ydata, self.zdata, cmap=self.cmap,
                color=self.color, alpha=self.alpha,
                vmax=self.vmax, vmin=self.vmin)
        self.axes = axis  # store for later use in case of update
        return self.ax_image

    def update(self):
        """Update the surface data on the axis."""
        # the best and only way to do this is to remove surface object
        # and redraw it
        try:
            self.lines.remove()
        except ValueError:
            # this error might happen if lines were already removed
            # for some reason.
            pass
        self.plot_on_axis(self.axes)

    def _check_list(self, data):
        # check if arguments are array-like and return numpy arrays
        for type_ in (np.ndarray, list, tuple):
            if isinstance(data, type_):
                break
        else:
            raise TypeError("Argument should be array-like.")
        if isinstance(data, np.ndarray):
            if len(data.shape) >= 3:
                raise TypeError("Argument is > 2D data!")
            return data
        return np.array(data, dtype=float)  # transform all to floats


class Trisurf(Image):
    """Class that represents a 3D trisurf."""

    _loggername = "Trisurf"

    def __init__(
            self, *args, triangles=None, zsort=None, mask=None,
            edgecolors=None, shade=None,
            **kwargs):
        super().__init__(*args, **kwargs)
        self.edgecolors = edgecolors
        self.shade = shade
        self.triangles = triangles
        self.zsort = zsort
        self.mask = mask
        if self.xdata.shape != self.ydata.shape:
            raise ValueError("xdata and ydata should agree in shapes.")
        if self.xdata.shape != self.zdata.shape:
            raise ValueError("xdata and zdata should agree in shapes.")

    def plot_on_axis(self, axis):
        """Plot the surface on an axis."""
        args = [self.xdata, self.ydata, self.zdata]
        kwargs = {}
        for attr in ("cmap", "alpha", "triangles", "zsort",
                     "color", "mask", "edgecolors", "shade"):
            if getattr(self, attr) is not None:
                kwargs[attr] = getattr(self, attr)
        self.ax_image = axis.plot_trisurf(*args, **kwargs)
        self.axes = axis  # store for later use in case of update
        return self.ax_image

    def update(self):
        """Update the surface data on the axis."""
        raise NotImplementedError

    def _check_list(self, data):
        # check if arguments are array-like and return numpy arrays
        if type(data) not in (np.ndarray, list, tuple):
            raise TypeError("Argument should be array-like.")
        if isinstance(data, np.ndarray):
            if len(data.shape) >= 3:
                raise TypeError("Argument is > 2D data!")
        return np.array(data, dtype=float)  # transform all to floats


class Contour(Surface):
    """Class that represents a contour plot on a 3D surface."""

    def __init__(
            self, xdata, ydata, zdata, levels, *args, linewidths=1.0,
            linestyles="-", corner_mask=None, **kwargs):
        """Contour init method.

        Parameters
        ----------
        xdata, ydata, zdata : arrays
                              Must be 2D shaped.
        linewidths : float, optional
            The coutour plot linewidths.
        levels : int or array-like
                 If one int, draws n+1 contour lines with automatic height
                 (by pyplot). If array-like, specifies the levels heights
                 manually for each contour line.
        """
        super().__init__(xdata, ydata, zdata, *args, **kwargs)
        self.corner_mask = corner_mask
        self._levels = None
        self.levels = levels
        self.linewidths = linewidths
        self.linestyles = linestyles

    @property
    def levels(self):
        """Return the contour levels."""
        return self._levels

    @levels.setter
    def levels(self, levels):
        if isinstance(levels, int):
            self._levels = levels
            return
        if is_list_like(levels):
            self._levels = levels
            return
        raise TypeError(f"Expected int or array-like but got: {levels}")

    @property
    def contour(self):
        """Return the contour lines."""
        # alias
        return self.lines

    @contour.setter
    def contour(self, contour):
        self.lines = contour

    def plot_on_axis(self, axis):
        """Plot the contour on an axis."""
        args = [self.xdata, self.ydata, self.zdata, self.levels]
        kwargs = {"linewidths": self.linewidths,
                  "linestyles": self.linestyles,
                  "label": self.label,
                  "corner_mask": self.corner_mask,
                  }
        if self.color is not None:
            self.contour = axis.contour(*args, colors=self.color, **kwargs)
            return self.contour
        self.contour = axis.contour(*args, cmap=self.cmap, **kwargs)
        return self.contour

    def update(self):
        """Update the contour data on the axis."""
        raise NotImplementedError()


class StraightLine(_BasePlotObject, abc.ABC):
    """Base class for straight lines."""

    def __init__(self, pos, linestyle="-", color="k", linewidth=1.0,
                 label=None, **kwargs):
        super().__init__(**kwargs)
        self.position = pos
        self.linestyle = linestyle
        self.color = color
        self.linewidth = linewidth
        self.label = label
        self.lines = None

    @abc.abstractmethod
    def plot_on_axis(self, *args, **kwargs):  # noqa: D102
        pass

    def reset(self):
        """Reset straight lines data."""
        del self.lines
        self.lines = None

    def update(self):
        """Update straight line data on the axis."""
        self.lines.set_linestyle(self.linestyle)
        self.lines.set_linewidth(self.linewidth)
        self.lines.set_color(self.color)
        self.lines.set_label(self.label)


class VLine(StraightLine):
    """Vertical line class."""

    _loggername = "VLine"

    def plot_on_axis(self, axis):
        """Plot the vertical line on the axis."""
        self.lines = axis.axvline(
                self.position, linestyle=self.linestyle, color=self.color,
                linewidth=self.linewidth, label=self.label)
        return self.lines

    def update(self):
        """Update the vertical line data on the axis."""
        self.lines.set_xdata(self.position)
        super().update()


class HLine(StraightLine):
    """Horizontal line class."""

    _loggername = "HLine"

    def plot_on_axis(self, axis):
        """Plot the horizontal line on the axis."""
        self.lines = axis.axhline(
                self.position, linestyle=self.linestyle, color=self.color,
                linewidth=self.linewidth, label=self.label)
        return self.lines

    def update(self):
        """Update the horizontal line data on the axis."""
        self.lines.set_ydata(self.position)
        super().update()


class NormalizedColorbar(_BasePlotObject):
    """Normalized colorbar."""

    _loggername = "NormalizedColorbar"

    def __init__(
            self, cmap: str, vmin: float, vmax: float,
            orientation: str = "vertical", label: str = "",
            loc: str | int = "upper right", height: float | str = 10,
            width: float | str = 10, ticks_direction: str = "right",
            tick_positions: Sequence[float] = None,
            tick_labels: Sequence[str | float] = None,
            bbox_to_anchor: Sequence[float] = None,
            tick_labels_rotation: float = None,
            pad: float | int = None,
            **kwargs):
        super().__init__(**kwargs)
        self.width = width
        self.pad = pad
        self.ticks_direction = ticks_direction
        self.height = height
        self.loc = loc
        self.cmap = cmap
        self.vmin = vmin
        self.vmax = vmax
        self.orientation = orientation
        self.label = label
        self.tick_positions = tick_positions
        self.tick_labels = tick_labels
        self.bbox_to_anchor = bbox_to_anchor
        self.tick_labels_rotation = tick_labels_rotation

    def plot_on_axis(self, axis):
        """Plot colorbar on axis."""
        # uses inset axes to place colorbar inside axis
        # https://stackoverflow.com/questions/18211967/position-colorbar-inside-figure
        cmap = matplotlib.cm.get_cmap(self.cmap)
        norm = matplotlib.colors.Normalize(vmin=self.vmin, vmax=self.vmax)
        cbaxes = inset_axes(
                axis, width=self.width, height=self.height, loc=self.loc,
                bbox_to_anchor=self.bbox_to_anchor,
                )
        kwargs = {}
        if self.tick_positions is not None:
            kwargs["ticks"] = self.tick_positions
        colorbar = matplotlib.colorbar.ColorbarBase(
                ax=cbaxes, cmap=cmap, norm=norm, orientation=self.orientation,
                **kwargs)
        if self.orientation == "vertical":
            cbaxes.yaxis.set_ticks_position(self.ticks_direction)
        else:
            cbaxes.xaxis.set_ticks_position(self.ticks_direction)
        if self.tick_labels is not None:
            if self.orientation == "vertical":
                colorbar.ax.set_yticklabels(self.tick_labels)
            else:
                colorbar.ax.set_xticklabels(self.tick_labels)
        self._set_tick_params(colorbar)
        colorbar.set_label(self.label)

    def _set_tick_params(self, colorbar):
        kwargs = {}
        if self.tick_labels_rotation:
            kwargs["rotation"] = self.tick_labels_rotation
        if self.pad is not None:
            kwargs["pad"] = self.pad
        if kwargs:
            colorbar.ax.tick_params(**kwargs)


class Inset(_BasePlotObject):
    """Inset curve object."""

    _loggername = "Inset"

    def __init__(
            self, inset, x, y, width: float, height: float,
            bbox_to_anchor: Sequence[float] = None,
            *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.x, self.y = x, y
        self.inset = inset  # should be a plot object
        self.width = width
        self.height = height

    def plot_on_axis(self, axis) -> None:
        """Plot inset on axis."""
        bounds = [self.x, self.y, self.width, self.height]
        inset_axis = axis.get_figure().add_axes(bounds)
        # axis.inset_axes(bounds)
        self.inset.plot_on_axis(None, inset_axis, handle_figure=False)
        # if self.inset._need_twinx:
        #     inset_twinx = inset_axes(self.inset._twinx, **kwargs)
        #     self.inset.plot_on_axis(
        #             None, inset_twinx, twinx=inset_twinx,
        #             plot_axis_items=False,
        #             handle_figure=False,
        #             )
