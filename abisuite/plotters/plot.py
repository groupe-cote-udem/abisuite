import colorsys
import logging
import os
import pickle

import aiofiles
import aiofiles.os

import matplotlib
# from matplotlib.collections import LineCollection
from matplotlib.colors import (
        # BoundaryNorm,
        LinearSegmentedColormap,
        # ListedColormap,
        )

# THIS BLOCK ONLY TO SWITCH DEFAULT MATPLOTLIB PARAMETERS
# WITHOUT ALWAYS TRIGGERING A WARNING
from ..routines import is_list_like, suppress_warnings  # noqa: E402


@suppress_warnings
def switch_toolbar_rcparam():
    """Switch parameters in matplotlib."""
    matplotlib.rcParams["toolbar"] = "toolmanager"  # noqa: E412


switch_toolbar_rcparam()

import matplotlib.pyplot as plt  # noqa: E402,I100

import numpy as np  # noqa: E402

from .curves import (  # noqa: E402
        Arrow, Contour, Curve,
        FillBetween,
        HLine,
        Histogram,
        Image, Inset, NormalizedColorbar, PNGImage,
        Scatter, Surface, Text, Trisurf,
        VLine,
        )
from .plot_tools import (  # noqa: E402
        CurveColor,
        LabelFontSize,
        TickFontSize,
        ToggleGrid,
        ToggleLegend,
        )
from ..bases import BaseUtility  # noqa: E402
from ..linux_tools import mkdir, mkdir_sync  # noqa: E402
from ..utils import TaggedList  # noqa: E402


# purely arbitrary but reasonable value here. Thanks Yoda for this number!
__DEFAULT_LABEL_FONTSIZE__ = 10
__DEFAULT_TICK_FONTSIZE__ = 10


# the following method is to use random colors displayed right
# taken from:
# https://stackoverflow.com/a/32520273/6362595
def rand_cmap(nlabels, type='bright', first_color_black=True,
              last_color_black=False):
    """
    Creates a random colormap to be used together with matplotlib.

    Useful for segmentation tasks

    Parameters
    ----------
    nlabels : int
              Number of labels (size of colormap)
    type : str, optional
           'bright' for strong colors, 'soft' for pastel colors
    first_color_black : bool, optional
                        Option to use first color as black.
    last_color_black : bool, optional
                       Option to use last color as black.

    Returns
    -------
    colormap for matplotlib
    """
    if type not in ('bright', 'soft'):
        raise ValueError('Please choose "bright" or "soft" for type')

    # Generate color map for bright colors, based on hsv
    if type == 'bright':
        randhsvcolors = [(np.random.uniform(low=0.0, high=1),
                          np.random.uniform(low=0.2, high=1),
                          np.random.uniform(low=0.9, high=1))
                         for i in range(nlabels)]

        # Convert HSV list to RGB
        randrgbcolors = []
        for hsvcolor in randhsvcolors:
            randrgbcolors.append(colorsys.hsv_to_rgb(hsvcolor[0], hsvcolor[1],
                                                     hsvcolor[2]))

        if first_color_black:
            randrgbcolors[0] = [0, 0, 0]

        if last_color_black:
            randrgbcolors[-1] = [0, 0, 0]

        random_colormap = LinearSegmentedColormap.from_list('new_map',
                                                            randrgbcolors,
                                                            N=nlabels)

    # Generate soft pastel colors, by limiting the RGB spectrum
    if type == 'soft':
        low = 0.6
        high = 0.95
        randrgbcolors = [(np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high),
                          np.random.uniform(low=low, high=high))
                         for i in range(nlabels)]

        if first_color_black:
            randrgbcolors[0] = [0, 0, 0]

        if last_color_black:
            randrgbcolors[-1] = [0, 0, 0]
        random_colormap = LinearSegmentedColormap.from_list('new_map',
                                                            randrgbcolors,
                                                            N=nlabels)
    return random_colormap


class Plot(BaseUtility):
    """Plotting class for singular plots."""

    _loggername = "Plot"
    _registered_plot_objects = {
            "arrows": Arrow,
            "contours": Contour,
            "curves": Curve,
            "curves_twinx": Curve,
            "curves_twiny": Curve,
            "histograms": Histogram,
            "images": Image, "pngimages": PNGImage,
            "normalized_colorbars": NormalizedColorbar,
            "vlines": VLine, "vlines_twiny": VLine,
            "hlines": HLine, "hlines_twinx": HLine,
            "fill_betweens": FillBetween,
            "fill_betweens_twinx": FillBetween,
            "fill_betweens_twiny": FillBetween,
            "insets": Inset,
            "scatters": Scatter,
            "surfaces": Surface,
            "trisurfs": Trisurf,
            "texts": Text,
            }

    def __init__(self, **kwargs):
        """Plot's init method."""
        # first mute the matplotlib logger because
        # it is unnecesseraily annoying
        logger = logging.getLogger("matplotlib.backend_managers")
        logger.setLevel(logging.ERROR)
        super().__init__(**kwargs)
        self._set_default()
        # init the attributes of the figure and the curves
        self._fig = None
        self._ax = None
        self._twinx = None
        self._twiny = None
        self._bar = None
        self.colorbar = True  # true => shows colorbar
        self.plot_objects = {}
        self._init_plot_objects_lists()

        # tick properties
        self._xtick_labels = None
        self._ytick_labels = None
        self._ytick_labels_twinx = None
        self._xtick_rotation = None
        self._xtick_labels_twiny = None
        self._xtick_rotation_twiny = None
        self.xtick_direction = "out"
        self.ytick_direction = "out"
        self.xtick_width = None
        self.ytick_width = None
        self.xtick_length = None
        self.ytick_length = None
        self.xtick_labeltop = False
        self.ytick_labelright = False
        self.xtick_top = False
        self.ytick_right = False
        self.tick_labelsize = None
        self.hide_ytick_labels = False
        self.hide_xtick_labels = False
        self.hide_xtick_bottom = False
        # axis properties
        self.bottom_axis_color = "k"
        self.top_axis_color = "k"
        self.left_axis_color = "k"
        self.right_axis_color = "k"
        self.bottom_axis_visible = True
        self.top_axis_visible = True
        # grid properties
        self.grid = False
        self.ygrid = False
        self.xgrid = False
        self.grid_color = None
        self.aspect = "auto"
        # legend properties
        self.legend_transparency = 0.9  # default is a bit transparent
        self.legend_loc = "best"
        self._legend = True
        self._legend_outside = False
        self._set_default_fontsizes()
        self._title = ""
        self._colorbarlabel = ""
        # axis label properties
        self.xlabel_coordinates = None
        self.ylabel_coordinates = None
        self.xlabel_rotation = None
        self.ylabel_rotation = None
        self._xlabel = ""
        self._xlabel_twiny = ""
        self._xlims = None
        self._xlims_twiny = None
        self._ylabel = ""
        self._ylabel_twinx = ""
        self._ylims = None
        self._ylims_twinx = None
        self._zlabel = ""
        self._zlims = None
        # global figure aspects
        self.figsize = None
        self.border_linewidth = None

    def __add__(self, plot):
        """Method that allows the addition of two plots.

        The addition is defined as another plot with the same characteritics
        but the superposition of all curves.
        """
        xx = self.xlabel != plot.xlabel
        yy = self.ylabel != plot.ylabel
        yx = self.ylabel_twinx != plot.ylabel_twinx
        xy = self.xlabel_twiny != plot.xlabel_twiny
        zz = self.zlabel != plot.zlabel
        if any((xx, yy, yx, xy, zz)):
            raise ValueError("Axis labels does not match :(")
        newplot = Plot()
        # set labels
        newplot.xlabel = self.xlabel
        newplot.ylabel = self.ylabel
        newplot.zlabel = self.zlabel
        newplot.ylabel_twinx = self.ylabel_twinx
        newplot.xlabel_twiny = self.xlabel_twiny
        for attr in self._registered_plot_objects:
            try:
                newplot.plot_objects[attr] = (
                        self.plot_objects[attr] + plot.plot_objects[attr])
            except AttributeError:
                # if plot was old (e.g.: loaded from an old pickle) it might
                # not have a given more recent attribute
                # add at least what exists
                for obj in (self, plot):
                    try:
                        setattr(newplot, attr, getattr(obj, attr))
                    except AttributeError:
                        # this object is not updated
                        pass
                pass
        for tick in ("xtick_labels", "xtick_labels_twiny"):
            selftick = getattr(self, tick)
            plottick = getattr(plot, tick)
            if selftick is not None and plottick is not None:
                # both plots have custom xticks. Merge them
                alltick = list(selftick)
                for xtick in plottick:
                    if xtick not in alltick:
                        alltick.append(xtick)
                setattr(newplot, tick, alltick)
            elif selftick is None and plottick is not None:
                setattr(newplot, tick, plottick)
            elif selftick is not None and plottick is None:
                setattr(newplot, tick, selftick)
            # else do nothing
        # compute new title
        if self.title == "" and plot.title != "":
            title = plot.title
        elif self.title != "" and plot.title == "":
            title = self.title
        elif self.title == "" and plot.title == "":
            title = ""
        else:
            title = self.title + " + " + plot.title
        newplot.title = title
        # grid, show if one of the plots is set to shot it
        newplot.grid = self.grid or plot.grid
        return newplot

    def _init_plot_objects_lists(self):
        for plot_object in self._registered_plot_objects:
            self.plot_objects[plot_object] = TaggedList(
                    loglevel=self._loglevel)

    @property
    def all_items(self):
        """The list of all items that will be on main axis."""
        if not hasattr(self, "plot_objects"):
            self._logger.warning(
                    "Old plot object -> might not have all curve types.")
            return self.get_all_items_from_names(
                    "curves", "surfaces", "hlines", "vlines",
                    "fill_between", "texts",
                    )
        return self.get_all_items_from_names(
                *(obj_type
                  for obj_type in self.plot_objects if "twin" not in obj_type)
                )

    @property
    def all_items_twinx(self):
        """The list of all items that will be on twinx axis."""
        return self.get_all_items_from_names(
                "curves_twinx", "fill_between_twinx", "hlines_twinx",
                "collections_twinx",
                )

    @property
    def all_items_twiny(self):
        """The list of all items that will be on twiny axis."""
        return self.get_all_items_from_names(
                "curves_twiny", "fill_between_twinx", "vlines_twiny",
                "collections_twiny",
                )

    def get_all_items_from_names(self, *args):
        """Returns the list of items using the item names to get the attr."""
        items = []
        for item_name in args:
            if hasattr(self, item_name):
                # an old version of plot pickle
                for item in getattr(self, item_name):
                    items.append(item)
                continue
            if item_name in self.plot_objects:
                # maybe old version of plot doesn't have this attr
                for item in self.plot_objects[item_name]:
                    items.append(item)
                continue
        return items

    @property
    def colorbarlabel(self):
        """Return the colorbar label."""
        return self._colorbarlabel

    @colorbarlabel.setter
    def colorbarlabel(self, label):
        self._colorbarlabel = label

    @property
    def curves_color(self):
        """Return the list of curves colors."""
        return [c.color for c in self.plot_objects["curves"]]

    @curves_color.setter
    def curves_color(self, colors):
        self._set_curves_color(self.plot_objects["curves"], colors)

    @property
    def curves_color_twinx(self):
        """Return the list of curve colors on twinx."""
        return [c.color for c in self.plot_objects["curves_twinx"]]

    @curves_color_twinx.setter
    def curves_color_twinx(self, colors):
        self._set_curves_color(self.plot_objects["curves_twinx"], colors)

    @property
    def curves_color_twiny(self):
        """Return the list of curve colors on twiny."""
        return [c.color for c in self.plot_objects["curves_twiny"]]

    @curves_color_twiny.setter
    def curves_color_twiny(self, colors):
        self._set_curves_color(self.plot_objects["curves_twiny"], colors)

    @property
    def legend(self):
        """Set to True to show legend."""
        return self._legend

    @legend.setter
    def legend(self, legend):
        if legend is not False and legend is not True:
            raise TypeError(f"Expected True or False but got: {legend}")
        self._legend = legend

    @property
    def legend_outside(self):
        """Set to True to put legend outside of plot."""
        return self._legend_outside

    @legend_outside.setter
    def legend_outside(self, legend):
        if legend is not False and legend is not True:
            raise TypeError(f"Expected True or False but got: {legend}")
        self._legend_outside = legend

    @property
    def title(self):
        """Set the title of the plot."""
        return self._title

    @title.setter
    def title(self, title):
        if not isinstance(title, str) and title is not None:
            raise TypeError("Title must be str.")
        self._title = title

    @property
    def xlabel(self):
        """Set the xlabel."""
        return self._xlabel

    @xlabel.setter
    def xlabel(self, label):
        if not isinstance(label, str) and label is not None:
            raise TypeError("Label must be str.")
        self._xlabel = label

    @property
    def xlabel_twiny(self):
        """Set the twiny label."""
        return self._xlabel_twiny

    @xlabel_twiny.setter
    def xlabel_twiny(self, xlabel):
        self._xlabel_twiny = xlabel

    @property
    def xlims(self):
        """Set the x limits."""
        return self._xlims

    @xlims.setter
    def xlims(self, xlims):
        if xlims is None:
            # reset
            self._xlims = None
            return
        if not is_list_like(xlims):
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        if len(xlims) != 2:
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        self._xlims = xlims

    @property
    def xlims_twiny(self):
        """Set the x limits of thw twiny axis."""
        return self._xlims_twiny

    @xlims_twiny.setter
    def xlims_twiny(self, xlims):
        if not is_list_like(xlims):
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        if len(xlims) != 2:
            raise TypeError(f"xlims should be a list of len 2 but got {xlims}")
        self._xlims_twiny = xlims

    @property
    def xtick_labels(self):
        """The xtick labels.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        return self._xtick_labels

    @xtick_labels.setter
    def xtick_labels(self, labels):
        # must be a list of two component lists.
        if labels is None:
            # reset
            self._xtick_labels = labels
            return
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if not is_list_like(labels[0]) or len(labels[0]) != 2:
                raise TypeError("Each xtick_labels componant is a list of"
                                " (pos, label).")
        self._xtick_labels = labels

    @property
    def xtick_labels_twiny(self):
        """The xtick labels on twiny axis.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        return self._xtick_labels_twiny

    def set_xtick_labels_twiny(self, labels):
        """Set the xtick labels on twiny axis.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        self.xtick_labels_twiny = labels

    @xtick_labels_twiny.setter
    def xtick_labels_twiny(self, labels):
        # must be a list of two component lists.
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if type(labels[0]) not in (list, tuple) or len(labels[0]) != 2:
                raise TypeError("Each xtick_labels componant is a list of"
                                " (pos, label).")
        self._xtick_labels_twiny = labels

    @property
    def ytick_labels(self):
        """The ytick labels.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        if not hasattr(self, "_ytick_labels"):
            self._ytick_labels = None
        return self._ytick_labels

    @ytick_labels.setter
    def ytick_labels(self, labels):
        # must be a list of two component lists.
        if labels is None:
            # reset
            self._ytick_labels = labels
            return
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if not is_list_like(labels[0]) or len(labels[0]) != 2:
                raise TypeError("Each ytick_labels componant is a list of"
                                " (pos, label).")
        self._ytick_labels = labels

    @property
    def ytick_labels_twinx(self):
        """The ytick labels on twinx axis.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        if not hasattr(self, "_ytick_labels_twinx"):
            self._ytick_labels_twinx = None
        return self._ytick_labels_twinx

    def set_ytick_labels_twinx(self, labels):
        """Set the ytick labels on twinx axis.

        Must be of the form
        [(pos, label), (pos2, label2), ...].
        """
        self.ytick_labels_twinx = labels

    @ytick_labels_twinx.setter
    def ytick_labels_twinx(self, labels):
        # must be a list of two component lists.
        if type(labels) not in (list, tuple):
            raise TypeError("Labels should be a list.")
        # if not an empty list, check that each labels are two components
        if len(labels):
            if type(labels[0]) not in (list, tuple) or len(labels[0]) != 2:
                raise TypeError("Each ytick_labels componant is a list of"
                                " (pos, label).")
        self._ytick_labels_twinx = labels

    @property
    def xtick_rotation(self):
        """Return the xtick rotation angle."""
        return self._xtick_rotation

    @xtick_rotation.setter
    def xtick_rotation(self, rotation):
        self._xtick_rotation = rotation

    @property
    def xtick_rotation_twiny(self):
        """Define the xtick rotation angle on the twiny axis."""
        return self._xtick_rotation_twiny

    @xtick_rotation_twiny.setter
    def xtick_rotation_twiny(self, rotation):
        self._xtick_rotation_twiny = rotation

    @property
    def ylabel(self):
        """Set the ylabel."""
        return self._ylabel

    @ylabel.setter
    def ylabel(self, label):
        if not isinstance(label, str) and label is not None:
            raise TypeError("Label must be str.")
        self._ylabel = label

    @property
    def ylabel_twinx(self):
        """Set the ylabel on twinx axis."""
        return self._ylabel_twinx

    @ylabel_twinx.setter
    def ylabel_twinx(self, ylabel):
        self._ylabel_twinx = ylabel

    @property
    def ylims(self):
        """Set the y limits."""
        return self._ylims

    @ylims.setter
    def ylims(self, ylims):
        if ylims is None:
            # reset
            self._ylims = None
            return
        if not is_list_like(ylims):
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        if len(ylims) != 2:
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        self._ylims = ylims

    @property
    def ylims_twinx(self):
        """Set the y limits on twinx axis."""
        return self._ylims_twinx

    @ylims_twinx.setter
    def ylims_twinx(self, ylims):
        if not is_list_like(ylims):
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        if len(ylims) != 2:
            raise TypeError(f"ylims should be a list of len 2 but got {ylims}")
        self._ylims_twinx = ylims

    @property
    def zlabel(self):
        """Set the zlabel."""
        return self._zlabel

    @zlabel.setter
    def zlabel(self, zlabel):
        if not isinstance(zlabel, str):
            raise TypeError("zlabel must be a string.")
        self._zlabel = zlabel

    @property
    def zlims(self):
        """Set the z limits."""
        return self._zlims

    @zlims.setter
    def zlims(self, zlims):
        if not is_list_like(zlims):
            raise TypeError(f"zlims should be a list of len 2 but got {zlims}")
        if len(zlims) != 2:
            raise TypeError(f"zlims should be a list of len 2 but got {zlims}")
        self._zlims = zlims

    @property
    def _need_twinx(self):
        if not hasattr(self, "plot_objects"):
            self._logger.warning(
                    "Old plot object -> no twinx.")
            return False
        for obj_type, obj_list in self.plot_objects.items():
            if "twinx" in obj_type and len(obj_list):
                return True
        return False

    @property
    def _need_twiny(self):
        if not hasattr(self, "plot_objects"):
            self._logger.warning(
                    "Old plot object -> no twiny.")
            return False
        for obj_type, obj_list in self.plot_objects.items():
            if "twiny" in obj_type and len(obj_list):
                return True
        return False

    @property
    def _need_3d_projection(self):
        if not hasattr(self, "plot_objects"):
            # old plot pickle
            self._logger.warning(
                    "Old plot object -> 3D projection might be wrong.")
            if len(self.surfaces):
                return True
            return False
        if len(self.plot_objects["surfaces"]):
            return True
        for obj_type, obj_list in self.plot_objects.items():
            if obj_type.endswith("images"):
                continue
            if obj_type == "surfaces" and len(obj_list) >= 1:
                return True
            for obj in obj_list:
                if not hasattr(obj, "zdata"):
                    continue
                if obj.zdata is not None:
                    return True
        return False

    def _add_plot_object(
            self, registered_plot_object_type: str,
            *args, tag: str = None, **kwargs) -> None:
        cls = self._registered_plot_objects[
                registered_plot_object_type]
        if isinstance(args[0], cls):
            inst = args[0]
        else:
            inst = cls(*args, loglevel=self._loglevel, **kwargs)
        self.plot_objects[registered_plot_object_type].append(inst, tag=tag)

    def add_normalized_colorbar(self, *args, **kwargs) -> None:
        """Add a normalized colorbar."""
        self._add_plot_object("normalized_colorbars", *args, **kwargs)

    def add_contour(self, *args, **kwargs) -> None:
        """Add a contour curve to the plot.

        All parameters except the tag are passed to the Contour class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the image in order to find it more quickly.
              If None, the tag is the index of the contour curve.
        """
        self._add_plot_object("contours", *args, **kwargs)

    def add_arrow(self, *args, **kwargs) -> None:
        """Add an Arrow to the plot."""
        self._add_plot_object("arrows", *args, **kwargs)

    def add_curve(
            self, *args, twinx: bool = False,
            twiny: bool = False, **kwargs) -> None:
        """Add a curve to the plot.

        All parameters except tag are passed to the
        :class:`Curve <abisuite.plotters.curves.Curve>` class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the curve in order to find it more quickly.
              If None, the tag is the index of the curve in the list of curves.
        twinx : bool, optional
                If True, the curves is added to the x twin axis curves instead
                of the main x axis.
        twiny : bool, optional
                If True, the curves is added to the y twin axis curves instead
                of the main y axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self._add_plot_object("curves", *args, **kwargs)
        elif twinx:
            self._add_plot_object("curves_twinx", *args, **kwargs)
        elif twiny:
            self._add_plot_object("curves_twiny", *args, **kwargs)

    def add_fill_between(
            self, *args, twinx: bool = False,
            twiny: bool = False,
            **kwargs):
        """Add a fill between curve to the plot.

        All parameters except tag are passed to the FillBetween class.

        Parameters
        ----------
        tag : optional
              To specify a tag for the fill_between curve to find it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the fill_between curve is added to the x twin axis.
        twiny : bool, optional
                If True, the fill_between curve is added to the y twin axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self._add_plot_object("fill_betweens", *args, **kwargs)
        elif twinx:
            self._add_plot_object("fill_betweens_twinx", *args, **kwargs)
        elif twiny:
            self._add_plot_object("fill_betweens_twiny", *args, **kwargs)

    def add_histogram(self, *args, **kwargs) -> None:
        """Add a histogram to the plot.

        All parameters except tag are passed to the Histogram class.

        Parameters
        ----------
        tag : optional
              To specify a tag for the histogram to find it quickly.
              If None, the tag is simply the normal index.
        """
        self._add_plot_object("histograms", *args, **kwargs)

    def add_image(self, *args, **kwargs) -> None:
        """Add a pcolormesh to the plot.

        All parameters except the tag are passed to the Image class.

        Parameters
        ----------
        tag : str, int, optional
              To specify a tag for the image in order to find it more quickly.
              If None, the tag is the index of the image in the list of images.
        """
        if len(args) == 1:
            # arg probably a path to a PNG image
            self._logger.debug(
                    "Only a path given to add_image => add PNGImage")
            self._add_plot_object("pngimages", *args, **kwargs)
        else:
            self._add_plot_object("images", *args, **kwargs)

    def add_inset(self, *args, **kwargs) -> None:
        """Add an inset plot to the plot."""
        self._add_plot_object("insets", *args, **kwargs)

    def add_scatter(self, *args, **kwargs) -> None:
        """Add a scatter plot to the plot.

        All parameters except tag are passed to the Scatter class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the scatter plot in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        self._add_plot_object("scatters", *args, **kwargs)

    def add_surface(self, *args, **kwargs) -> None:
        """Add a 3D surface to the plot.

        All parameters except tag are passed to the Surface class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the surface plot in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        self._add_plot_object("surfaces", *args, **kwargs)

    def add_trisurf(self, *args, **kwargs) -> None:
        """Add a 3D trisurf to the plot.

        All parameters except tag are passed to the Trisurf class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the trisurf plot in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        self._add_plot_object("trisurfs", *args, **kwargs)

    def add_text(self, *args, **kwargs) -> None:
        """Adds a text label to the plot.

        All parameters except 'tag' are passed to the Text class.

        Parameters
        ----------
        tag: optional
             To specify a tag for the text label in order to find it quickly
             than using integers. If None, no tag is applied.
        """
        self._add_plot_object("texts", *args, **kwargs)

    def add_vline(self, *args, twiny=False, **kwargs) -> None:
        """Add a vertical line.

        All parameters except tag are passed to the VLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the vline to find it quickly.
              If None, the tag is simply the normal index.
        twiny : bool, optional
                If True, the vline is added on the twiny axis.
        """
        if not twiny:
            self._add_plot_object("vlines", *args, **kwargs)
        else:
            self._add_plot_object("vlines_twiny", *args, **kwargs)

    def add_hline(self, *args, twinx=False, **kwargs) -> None:
        """Add a horizontal line.

        All parameters except tag are passed to the HLine class.

        Parameters
        ----------
        tag : optional
              Specifies a tag for the hline to find it quickly.
              If None, the tag is simply the normal index.
        twinx : bool, optional
                If True, the horizontal line is added on the twin axis
                instead of the main axis.
        """
        if not twinx:
            self._add_plot_object("hlines", *args, **kwargs)
        else:
            self._add_plot_object("hlines_twinx", *args, **kwargs)

    def _clear_plot_objects(self, plot_object_type: str) -> None:
        del self.plot_objects[plot_object_type]
        self.plot_objects[plot_object_type] = TaggedList(
                loglevel=self._loglevel)

    def clear_insets(self) -> None:
        """Remove all insets."""
        self._clear_plot_objects("insets")

    def clear_arrows(self) -> None:
        """Remove all arrows."""
        self._clear_plot_objects("arrows")

    def clear_contours(self) -> None:
        """Remove all contour plots from the plot."""
        self._clear_plot_objects("contours")

    def clear_pngimages(self) -> None:
        """Remove pngimages from plots."""
        self._clear_plot_objects("pngimages")

    def clear_images(self) -> None:
        """Remove images from plots."""
        self._clear_plot_objects("images")

    def clear_curves(self, twinx: bool = False, twiny: bool = False):
        """Removes all the curves of the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx curves are removed.
        twiny : bool, optional
                If True, all the twiny curves are removed.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self._clear_plot_objects("curves")
        elif twinx:
            self._clear_plot_objects("curves_twinx")
        elif twiny:
            self._clear_plot_objects("curves_twiny")

    def clear_curve_labels(
            self, twinx: bool = False, twiny: bool = False) -> None:
        """Sets the labels of all curves to None.

        Parameters
        ----------
        twinx : bool, optional
                If True, the label clearing is done on the x twin axis curves.
        twiny : bool, optional
                If True, the label clearing is done on the y twin axis curves.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if twinx:
            curves = self.plot_objects["curves_twinx"]
        elif twiny:
            curves = self.plot_objects["curves_twiny"]
        else:
            curves = self.plot_objects["curves"]
        for curve in curves:
            curve.clear_label()

    def clear_fill_between(
            self, twinx: bool = False, twiny: bool = False) -> None:
        """Removes all fill_betweens from the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx fill_between are removed.
        twiny : bool, optional
                If True, all the twiny fill_between are removed.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self._clear_plot_objects("fill_betweens")
        elif twinx:
            self._clear_plot_objects("fill_betweens_twinx")
        elif twiny:
            self._clear_plot_objects("fill_betweens_twiny")

    def clear_histograms(self) -> None:
        """Removes all histograms from the plot object."""
        self._clear_plot_objects("histograms")

    def clear_hlines(self, twinx: bool = False) -> None:
        """Removes all the hlines of the plot.

        Parameters
        ----------
        twinx : bool, optional
                If True, all the twinx hlines are removed.
        """
        if not twinx:
            self._clear_plot_objects("hlines")
        elif twinx:
            self._clear_plot_objects("hlines_twinx")

    def clear_normalized_colorbars(self) -> None:
        """Removes all normalized colorbars."""
        self._clear_plot_objects("normalized_colorbars")

    def clear_vlines(self, twiny: bool = False) -> None:
        """Removes all the hlines of the plot.

        Parameters
        ----------
        twiny : bool, optional
                If True, all the twinx hlines are removed.
        """
        if not twiny:
            self._clear_plot_objects("vlines")
        elif twiny:
            self._clear_plot_objects("vlines_twiny")

    def clear_scatters(self) -> None:
        """Remove all scatters of the plot."""
        self._clear_plot_objects("scatters")

    def clear_surfaces(self) -> None:
        """Removes all surfaces from 3D plot."""
        self._clear_plot_objects("surfaces")

    def clear_trisurfs(self) -> None:
        """Removes all trisurfs from 3D plot."""
        self._clear_plot_objects("trisurfs")

    def clear_texts(self) -> None:
        """Removes all texts labels of the plot."""
        self._clear_plot_objects("texts")

    @suppress_warnings
    def final_adjusting(self):
        """Make final adjustments."""
        if self.tick_labelsize is not None:
            # adjust xtick label size
            # https://stackoverflow.com/a/11386056/6362595
            self._ax.tick_params(
                    axis="both", which="both", labelsize=self.tick_labelsize,
                    )
            if self._need_twinx:
                self._twinx.tick_params(
                    axis="both", which="both", labelsize=self.tick_labelsize)
        self._ax.set_aspect(self.aspect)
        if self._need_3d_projection:
            if self.zlims is not None:
                self._ax.set_zlim(self.zlims[0], self.zlims[1])
            else:
                mins, maxs = [], []
                for obj_type, plot_objects in self.plot_objects.items():
                    if "twin" in obj_type:
                        continue  # will be dealt later
                    for plot_object in plot_objects:
                        if not hasattr(plot_object, "zdata"):
                            continue
                        if plot_object.zdata is None:
                            continue
                        mins.append(plot_object.zdata.min())
                        maxs.append(plot_object.zdata.max())
                self._automatic_adjust_axis(self._ax.set_zlim, mins, maxs)
        if self.ylims is not None:
            self._ax.set_ylim(self.ylims[0], self.ylims[1])
        else:
            # make sure we don't count empty curves
            mins, maxs = [], []
            if not hasattr(self, "plot_objects"):
                self._logger.warning(
                        "Old plot pickle -> Min/Max of axis might be off")
                all_plot_objects = {"curves": self.curves}
            else:
                all_plot_objects = self.plot_objects
            for plot_objects in all_plot_objects.values():
                for plot_object in plot_objects:
                    if hasattr(plot_object, "ydata"):
                        mins.append(plot_object.ydata.min())
                        maxs.append(plot_object.ydata.max())
                    if hasattr(plot_object, "ydata2"):
                        mins.append(plot_object.ydata2.min())
                        maxs.append(plot_object.ydata2.max())
                    if hasattr(plot_object, "position"):
                        mins.append(plot_object.position)
                        maxs.append(plot_object.position)
            self._automatic_adjust_axis(self._ax.set_ylim, mins, maxs)
        if self._need_twinx:
            if self.ylims_twinx:
                self._twinx.set_ylim(self.ylims_twinx[0], self.ylims_twinx[1])
            else:
                mins, maxs = [], []
                for obj_type, plot_objects in self.plot_objects.items():
                    if "twinx" not in obj_type:
                        continue
                    for plot_object in plot_objects:
                        if not hasattr(plot_object, "ydata"):
                            continue
                        if plot_object.ydata is None:
                            continue
                        mins.append(plot_object.ydata.min())
                        maxs.append(plot_object.ydata.max())
                mins += [line.position
                         for line in self.plot_objects["hlines_twinx"]]
                maxs += [line.position
                         for line in self.plot_objects["hlines_twinx"]]
                self._automatic_adjust_axis(self._twinx.set_ylim, mins, maxs)
        if self._need_twiny:
            if self.xlims_twiny:
                self._twiny.set_xlim(self.xlims_twiny[0], self.xlims_twiny[1])
            else:
                mins, maxs = [], []
                for obj_type, plot_objects in self.plot_objects.items():
                    if "twiny" not in obj_type:
                        continue
                    for plot_object in plot_objects:
                        if not hasattr(plot_object, "ydata"):
                            continue
                        if not plot_object.ydata:
                            continue
                        mins.append(plot_object.ydata.min())
                        maxs.append(plot_object.ydata.max())
                mins += [line.position
                         for line in self.plot_objects["vlines_twiny"]]
                maxs += [line.position
                         for line in self.plot_objects["vlines_twiny"]]
                self._automatic_adjust_axis(self._twiny.set_xlim, mins, maxs)
        if self.xlims is not None:
            self._ax.set_xlim(self.xlims[0], self.xlims[1])
        else:
            mins, maxs = [], []
            if hasattr(self, "plot_objects"):
                all_plot_objects = self.plot_objects
            else:
                all_plot_objects = {
                        "curves": self.curves,
                        }
            for obj_type, plot_objects in all_plot_objects.items():
                if "twin" in obj_type:
                    continue
                for plot_object in plot_objects:
                    if not hasattr(plot_object, "xdata"):
                        continue
                    if not len(plot_object.xdata):
                        continue
                    mins.append(plot_object.xdata.min())
                    maxs.append(plot_object.xdata.max())
            self._automatic_adjust_axis(self._ax.set_xlim, mins, maxs)

    @suppress_warnings
    def plot(self,
             show: bool = True,
             ax=None,
             random_colors: bool = False,
             handle_figure: bool = True,
             ) -> None:
        """Method to plot the figure.

        It must be called prior to saving
        the figure.

        Parameters
        ----------
        show : bool, optional
            If False, the plot is not shown but it is stored in an internal
            attribute.
        random_colors : bool, optional
            If True, a random color generator is used to generate
            curve colors.
        """
        if handle_figure:
            self._create_figure()
        # plot curves, lines and collections
        if random_colors:
            self.randomize_curve_colors(twinx=True, twiny=True)
        if ax is None:
            ax = self._ax
        for item in self.all_items:
            item.plot_on_axis(ax)
        if self._need_twinx:
            if self._twinx is None:
                self._create_twin_axes()
            for item in self.all_items_twinx:
                item.plot_on_axis(self._twinx)
        if self._need_twiny:
            for item in self.all_items_twiny:
                item.plot_on_axis(self._twiny)
        self._set_labels()
        self._set_ticks()
        self._set_legend()
        self._set_axis_colors()
        self._set_colorbars()
        # set figure border linewidth
        if hasattr(self, "border_linewidth"):
            if self.border_linewidth is not None:
                for value in self._ax.spines.values():
                    value.set_linewidth(self.border_linewidth)
        xgrid, ygrid = False, False
        if hasattr(self, "xgrid"):
            # palliate old plot pickles
            xgrid, ygrid = self.xgrid, self.ygrid
        if self.grid or xgrid or ygrid:
            if self.grid and (xgrid or ygrid):
                raise ValueError("Cannot have both grid and xgrid/ygrid True")
            kwargs = {}
            if hasattr(self, "grid_color"):
                if self.grid_color is not None:
                    kwargs["color"] = self.grid_color
            if xgrid:
                ax.xaxis.grid(xgrid, **kwargs)
            if ygrid:
                self._ax.yaxis.grid(ygrid, **kwargs)
            if self.grid:
                self._ax.grid(self.grid, **kwargs)
        self.final_adjusting()
        if handle_figure:
            # before showing, add tools
            self._add_tools_to_fig(self._fig)
            try:
                self._fig.tight_layout()
            except AttributeError:
                pass  # nevermind den
        try:
            self._ax.tight_layout()
        except AttributeError:
            pass  # nevermind den
        if show:
            plt.show()

    def plot_on_axis(self, fig, axis, **kwargs):
        """Same as plot, but give an axis and a figure Matplotlib object.

        All other kwargs go to the plot method.
        """
        self._fig = fig
        self._ax = axis
        self.plot(**kwargs)

    def randomize_curve_colors(
            self, twinx: bool = False,
            twiny: bool = False,
            colormap: str = "gist_rainbow",
            ) -> None:
        """Randomizes the color of each curve.

        Parameters
        ----------
        colormap : str, optional
                   States the matplotlib colormap to choose color from.
                   If set to 'random', a set of random color is created.
        twinx : bool, optional
                If True, the color randomization procedure is done on the
                curves of the twinx axis.
        twiny : bool, optional
                If True, the color randomization procedure is done on the
                curves of the twinx axis.
        """
        curves = self.plot_object["curves"].tolist(copy=False)
        if twinx:
            curves += self.plot_objects["curves_twinx"].tolist(copy=False)
        if twiny:
            curves += self.plot_objects["curves_twiny"].tolist(copy=False)
        nlines = len(curves)
        if colormap == "random":
            cmap = rand_cmap(
                    len(curves),
                    type='bright',
                    first_color_black=True,
                    last_color_black=False,
                    )
        else:
            cmap = plt.get_cmap(colormap)
        color_idx = np.linspace(0, 1, nlines)
        for curve, idx in zip(curves, color_idx):
            curve.color = cmap(idx)

    def _remove_plot_object(self, plot_obj_type: str, *args) -> None:
        self.plot_objects[plot_obj_type].remove(*args)

    def remove_contour(self, tag: str) -> None:
        """Removes a contour curve from the plot from its tag (or index).

        Parameters
        ----------
        tag : str or int
              The contour tag or index.
        """
        self._remove_plot_object("contours", tag)

    def remove_curve(
            self, tag: str, twinx: bool = False,
            twiny: bool = False) -> None:
        """Removes a curve according to its tag (or index).

        Parameters
        ----------
        tag : str
              The curve tag or index to remove.
        twinx : bool, optional
                If True, the curve is removed from the x twin axis instead
                of the x main axis.
        twiny : bool, optional
                If True, the curve is removed from the y twin axis instead
                of the y main axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self._remove_plot_object("curves", tag)
        elif twinx:
            self._remove_plot_object("curves_twinx", tag)
        elif twiny:
            self._remove_plot_object("curves_twiny", tag)

    def remove_fill_between(
            self, tag: str, twinx: bool = False, twiny: bool = False) -> None:
        """Remove a fill_between curve according to its tag.

        Parameters
        ----------
        tag : the tag/index of the fill_between to remove.
        twinx : bool, optional
                If True, the removal is done on the x twin axis fill_betweens.
        twiny : bool, optional
                If True, the removal is done on the y twin axis fill_betweens.
        """
        if not (twinx or twiny):
            self._remove_plot_object("fill_betweens", tag)
        elif twinx:
            self._remove_plot_object("fill_betweens_twinx", tag)
        elif twiny:
            self._remove_plot_object("fill_betweens_twiny", tag)

    def remove_hline(self, tag: str, twinx: bool = False) -> None:
        """Remove a hline according to its tag or index.

        Parameters
        ----------
        tag : tag/index of the hline to remove.
        twinx : bool, optional
                If True, the removal is done on the twin axis instead of
                the main one.
        """
        if not twinx:
            self._remove_plot_object("hlines", tag)
        else:
            self._remove_plot_object("hlines_twinx", tag)

    def remove_vline(self, tag: str, twiny: bool = False) -> None:
        """Remove vline according to its tag or index.

        Parameters
        ----------
        tag : The tag of the vline.
        twiny : bool, optional
                If True, the removal procedure is done on the twin y axis.
        """
        if not twiny:
            self._remove_plot_object("vlines", tag)
        else:
            self._remove_plot_object("vlines_twiny", tag)

    def remove_scatter(self, tag: str) -> None:
        """Remove a scatter plot according to its tag or its index."""
        self._remove_plot_object("scatters", tag)

    def remove_surface(self, tag: str) -> None:
        """Remove a surface plot according to its tag or index."""
        self._remove_plot_object("surfaces", tag)

    def remove_trisurf(self, tag: str) -> None:
        """Remove a trisurf plot according to its tag or index."""
        self._remove_plot_object("trisurfs", tag)

    def remove_text(self, tag: str) -> None:
        """Removes a text label according to its tag or index."""
        self._remove_plot_object("texts", tag)

    def reset(self):
        """Resets the matplotlib attributes."""
        if self._fig is not None:
            plt.close(self._fig)
        for attr in ("fig", "ax", "twinx", "twiny", "bar"):
            attr = "_" + attr
            val = getattr(self, attr)
            del val
            setattr(self, attr, None)
        # delete items 'lines' properties which are matplotlib objects
        for item in (self.all_items + self.all_items_twinx +
                     self.all_items_twiny):
            item.reset()

    def save_sync(self, path, overwrite=False, **kwargs):
        """Same as :meth:`<.save>` but sync.

        kwargs are passed to the 'savefig' function.
        """
        if path.endswith(".plot") or path.endswith(".pickle"):
            self.save_plot_sync(path, **kwargs)
            return
        # make directory if it doest not exist
        if not os.path.exists(os.path.dirname(path)):
            mkdir_sync(os.path.dirname(path))
        if os.path.exists(path):
            if overwrite:
                os.remove(path)
            else:
                raise FileExistsError(path)
        self._do_save(path, **kwargs)

    @suppress_warnings
    def _do_save(self, path, **kwargs):
        if self._fig is None:
            raise RuntimeError("Use the plot method to generate figure.")
        self._logger.info(f"Saving plot at: '{path}'.")
        if not self.legend_outside:
            self._fig.tight_layout()
            self._fig.savefig(path, **kwargs)
            return
        # if legend outside, add it to savefig to make sure it is saved too
        legend = self._ax.get_legend()
        self._fig.tight_layout()
        self._fig.savefig(path,
                          bbox_extra_artists=[legend],
                          bbox_inches="tight",
                          **kwargs)

    async def save(self, path, **kwargs):
        """Save plot as a png or pdf image using matplotlib savefig method.

        The Plot object is reset afterwards.

        Parameters
        ----------
        path : str
           The path where the file will be saved. If path has a '.plot'
            or '.pickle' extension, then the 'save_plot' method is used
            instead to pickle the object.
        overwrite: bool, optional
            If True and the png/pdf file already exists, it will be
            overwritten. Otherwise a FileExistsError will be thrown.

        Raises
        ------
        FileExistsError:
            If file already exists and overwrite is False.
        RuntimeError:
            If 'plot' method was not called before saving the figure.

        Warning
        -------
        This method cannot be awaited at the same time as the
        'save_plot' method as the latter deletes the figure to do so.
        """
        if path.endswith(".plot") or path.endswith(".pickle"):
            await self.save_plot(path)
            return
        await self._check_path_exists(path, **kwargs)
        self._do_save(path)

    def _check_path_exists_sync(self, path, overwrite=False):
        if os.path.exists(path):
            if not overwrite:
                raise FileExistsError(path)
            else:
                # delete file
                os.remove(path)
        if not os.path.isdir(os.path.dirname(path)):
            mkdir_sync(os.path.dirname(path))

    async def _check_path_exists(self, path, overwrite=False):
        if await aiofiles.os.path.exists(path):
            if not overwrite:
                raise FileExistsError(path)
            else:
                # delete file
                try:
                    await aiofiles.os.remove(path)
                except FileNotFoundError:
                    # was already deleted somehow
                    pass
        if not await aiofiles.os.path.isdir(os.path.dirname(path)):
            await mkdir(os.path.dirname(path))

    def save_plot_sync(self, path, **kwargs):
        """Same as :meth:`<.save_plot>` but synced."""
        self._check_path_exists_sync(path, **kwargs)
        self._do_save_plot(path)

    def save_pickle_sync(self, *args, **kwargs):
        """Alias for :meth:`<.save_plot_sync>`."""
        return self.save_plot_sync(*args, **kwargs)

    async def save_pickle(self, *args, **kwargs):
        """Alias for :meth:`<~abisuite.plotters.plot.Plot.save_plot>`."""
        await self.save_plot(*args, **kwargs)

    async def save_plot(self, path, **kwargs):
        """Save the Plot object into a file for easy reloading afterwards.

        This is done using the
        `pickle <https://docs.python.org/3/library/pickle.html>`__
        standard module. To load the saved
        plot, one needs to call the staticmethod
        :meth:`load_plot <abisuite.plotters.plot.Plot.load_plot>`.

        Parameters
        ----------
        path : str
            The path where the pickle object will be saved.
        overwrite: bool, optional
            If any file exists at the given path, it will be overwritten.
        """
        await self._check_path_exists(path, **kwargs)
        self._do_save_plot(path)

    def _do_save_plot(self, path):
        # call reset to not have to pickle matplotlib figures
        self.reset()
        self._logger.info(f"Saving plot pickle at: '{path}'.")
        with open(path, "wb") as f:
            pickle.dump(self, f)

    # def separate_positive_from_negative(self, color_pos, color_neg,
    #                                     twinx=False, twiny=False, zero=0.0):
    #     """Separates the curves according to their value.

    #     If curve is > 0, its color is set to color_pos, samething for < 0.

    #     Parameters
    #     ----------
    #     color_pos : str
    #                 The color for the positive part of the curve.
    #     color_neg : str
    #                 The color for the negative part of the curve.
    #     twinx : bool, optional
    #             If True, this operation is done on the x twin axis.
    #     twiny : bool, optional
    #             If True, this operation is done on the y twin axis.
    #     zero : float, optional
    #            Defines the zero of the y axis. (default=0)
    #     """
    #     # taken from
    # https://matplotlib.org/examples/pylab_examples/multicolored_line.html
    #     if twinx and twiny:
    #         raise ValueError(
    # "twinx and twiny cannot be true at the same time")
    #     curves = self.curves
    #     if twinx:
    #         curves = self.curves_twinx
    #     elif twiny:
    #         curves = self.curves_twiny
    #     for _tag, curve in curves.iter_tags_items():
    #         # for every curve introduce a point everytime the curve crosses 0
    #         xs = list(curve.xdata)
    #         ys = list(curve.ydata)
    #         newxs = list(curve.xdata.copy())
    #         newys = list(curve.ydata.copy())
    #         for i, (x, y) in enumerate(zip(xs[1:], ys[1:])):
    #             # check if there is a change of sign
    #             if (y - zero) * (ys[i - 1] - zero) < 0:
    #                 # there is a change of sign, introduce new point
    #                 # whose y value is mathematically exactly 0
    #                 newx = ((zero *
    #                         (x - xs[i - 1]) + y * xs[i - 1] - ys[i - 1] * x)/
    #                         (y - ys[i - 1]))
    #                 newxs.append(newx)
    #                 newys.append(zero)
    #         curve.xdata = newxs
    #         curve.ydata = newys
    #         # resort data just to make sure
    #         curve.sort_data()
    #         cmap = ListedColormap([color_neg, color_pos])
    #         norm = BoundaryNorm([curve.min, zero, curve.max], cmap.N)
    #         points = np.array([curve.xdata, curve.ydata]).T.reshape(-1, 1, 2)
    #         segments = np.concatenate([points[:-1], points[1:]], axis=1)
    #         lc = LineCollection(segments, cmap=cmap, norm=norm)
    #         lc.set_array(curve.ydata)
    #         self.add_collection(lc)
    #         # make line transparent
    #         curve.alpha = 0.0
    #     # remove all original curves
    #     # self.clear_curves(twinx=twinx)

    def set_curve_label(
            self, label: str, tag: str, twinx: bool = False,
            twiny: bool = False,
            ) -> None:
        """Set the label of a specific curve.

        Parameters
        ----------
        label : str
                The label to apply to the curve.
        tag : The curve tag/index.
        twinx : bool, optional
                If True, the label is applied on the x twin axis.
        twiny : bool, optional
                If True, the label is applied on the y twin axis.
        """
        if twinx and twiny:
            raise ValueError("twinx and twiny cannot be true at the same time")
        if not (twinx or twiny):
            self.plot_objects["curves"][tag].label = label
        elif twinx:
            self.plot_objects["curves_twinx"][tag].label = label
        elif twiny:
            self.plot_objects["curves_twiny"][tag].label = label

    def set_title(self, title):
        """Set the plot title."""
        self.title = title

    def set_xlabel(self, value):
        """Set the x label."""
        self.xlabel = value

    def set_xtick_labels(self, labels):
        """Set the xtick labels."""
        self.xtick_labels = labels

    def set_ylabel(self, label):
        """Set the y label."""
        self.ylabel = label

    def show(self, *args, **kwargs):
        """Shows the generated plot.

        It has to be drawn first by calling
        the plot() method.
        """
        if self._fig is None:
            raise RuntimeError("Generate plot first.")
        plt.show(*args, **kwargs)

    def synchronize_labels(self, plot):
        """Synchronize the axis labels.

        Such that they are the same as a
        given plot object.

        Parameters
        ----------
        plot: Plot object
            The plot object to copy the axis label from.

        Raises
        ------
        TypeError: if the given object is not a Plot instance.
        """
        if not isinstance(plot, Plot):
            raise TypeError(f"Need to give a Plot object but got '{plot}'.")
        self.xlabel = plot.xlabel
        self.ylabel = plot.ylabel
        if plot.zlabel is not None:
            self.zlabel = plot.zlabel
        if plot.ylabel_twinx is not None:
            self.ylabel_twinx = plot.ylabel_twinx
        if plot.xlabel_twiny is not None:
            self.xlabel_twiny = plot.xlabel_twiny

    def update(self):
        """Update the plot with stored data.

        Use this if data has changed since
        last drawing.
        """
        if self._fig is None:
            return RuntimeError("Generate plot first.")
        for item in (
                self.all_items + self.all_items_twinx + self.all_items_twiny):
            item.update()
        self._set_legend()
        self._set_labels()
        self._set_ticks()
        # the following seems to have a weird side effect
        # that reduce the plot size
        # (when 3D only?!?) FG: 2021/08/30
        # self._remove_colorbars()
        # self._set_colorbars()
        self._fig.canvas.draw()

    @suppress_warnings
    def _add_tools_to_fig(self, fig):
        # check the backend before adding tools
        backend = matplotlib.get_backend().lower()
        if backend == "agg" or backend == "nbagg" or "ipykernel" in backend:
            # agg backend or jupyter-notebook backend
            # don't add tools since they don't support it
            return
        toolmanager = fig.canvas.manager.toolmanager
        toolbar = fig.canvas.manager.toolbar
        # add the ToggleLegend tool
        if toolmanager is None or toolbar is None:
            # in case we use a backend which does not support tools
            return
        toolmanager.add_tool("togglelegend", ToggleLegend)
        toolbar.add_tool(toolmanager.get_tool("togglelegend"), "io")
        # add the ToggleGrid tool
        toolmanager.add_tool("togglegrid", ToggleGrid)
        toolbar.add_tool(toolmanager.get_tool("togglegrid"), "io")
        # add colorcurve to bar
        toolmanager.add_tool("curvecolor", CurveColor)
        toolbar.add_tool(toolmanager.get_tool("curvecolor"), "io")
        # axis font size
        toolmanager.add_tool("tick_font_size", TickFontSize)
        toolbar.add_tool(toolmanager.get_tool("tick_font_size"), "io")
        # axis font size
        toolmanager.add_tool("label_font_size", LabelFontSize)
        toolbar.add_tool(toolmanager.get_tool("label_font_size"), "io")

    def _automatic_adjust_axis(self, adjust_func, mins, maxs):
        if not mins or not maxs:
            return
        if not np.isfinite(max(maxs)) or not np.isfinite(min(mins)):
            return
        range_ = max(maxs) - min(mins)
        fivepercent = 0.05 * range_
        adjust_func(min(mins) - fivepercent, max(maxs) + fivepercent)

    @suppress_warnings
    def _create_figure(self):
        if self._fig is not None or self._ax is not None:
            return
        kwargs = {}
        if self.figsize is not None:
            kwargs["figsize"] = self.figsize
        self._fig = plt.figure(**kwargs)
        kwargs = {}
        if self._need_3d_projection:
            kwargs["projection"] = "3d"
        ax = self._fig.add_subplot(**kwargs)
        self._ax = ax
        self._create_twin_axes()

    def _create_twin_axes(self):
        if self._need_twinx:
            self._twinx = self._ax.twinx()
        if self._need_twiny:
            self._twiny = self._ax.twiny()

    # def _plot_collections(self):
    #     for collection in self.collections:
    #         self._ax.add_collection(collection)
    #     if self._need_twinx:
    #         for collection in self.collections_twinx:
    #             self._twinx.add_collection(collection)
    #     if self._need_twiny:
    #         for collection in self.collections_twiny:
    #             self._twiny.add_collection(collection)

    def _remove_colorbars(self):
        try:
            self._bar.remove()
        except (AttributeError, KeyError):
            pass
        del self._bar
        self._bar = None

    def _set_axis_colors(self):
        # set the color of the axes. useful when using twinx or twiny!
        self._logger.debug("Setting axis and ticks colors.")
        ax = self._ax
        if self._need_twinx:
            ax = self._twinx
        ax.spines["bottom"].set_color(self.bottom_axis_color)
        ax.spines["top"].set_color(self.top_axis_color)
        ax.spines["left"].set_color(self.left_axis_color)
        ax.spines["right"].set_color(self.right_axis_color)
        if hasattr(self, "bottom_axis_visible"):
            ax.spines["bottom"].set_visible(self.bottom_axis_visible)
        if hasattr(self, "top_axis_visible"):
            ax.spines["top"].set_visible(self.top_axis_visible)
        # also change the ticks and labels to the same color
        kwargs = {}
        if hasattr(self, "xtick_labeltop"):
            kwargs.update({
                "labeltop": self.xtick_labeltop,
                "top": self.xtick_top,
                })
        for prop, key in zip(
                ["bottom_axis_color", "xtick_direction", "xtick_length",
                 "xtick_width"],
                ["colors", "direction", "length", "width"]):
            if not hasattr(self, prop):
                continue
            if getattr(self, prop) is not None:
                kwargs[key] = getattr(self, prop)
        self._ax.tick_params(axis="x", **kwargs)
        kwargs = {}
        if hasattr(self, "ytick_labelright"):
            kwargs.update({
                "labelright": self.ytick_labelright,
                "right": self.ytick_right,
                })
        for prop, key in zip(
                ["left_axis_color", "ytick_direction", "ytick_length",
                 "ytick_width"],
                ["colors", "direction", "length", "width"]):
            if not hasattr(self, prop):
                continue
            if getattr(self, prop) is not None:
                kwargs[key] = getattr(self, prop)
        self._ax.tick_params(axis="y", **kwargs)
        self._ax.yaxis.label.set_color(self.left_axis_color)
        self._ax.xaxis.label.set_color(self.bottom_axis_color)
        if self._need_twinx:
            self._twinx.tick_params(axis="y", colors=self.right_axis_color)
            self._twinx.yaxis.label.set_color(self.right_axis_color)
        if self._need_twiny:
            self._twiny.tick_params(axis="x", colors=self.top_axis_color)
            self._twiny.xaxis.label.set_color(self.top_axis_color)

    def _set_colorbar(self, ax_images):
        if not self.colorbar:
            return
        if len(ax_images):
            if len(ax_images) > 1:
                self._logger.warning(
                        "Not sure more than 1 images/surface works...")
                self._logger.warning(
                        "Only plotting 1 colorbar on first image/surface...")
            # for ax_image in ax_images:
            self._bar = self._fig.colorbar(ax_images[0], ax=self._ax)
            self._bar.set_label(self.colorbarlabel)

    def _set_colorbars(self):
        if hasattr(self, "plot_objects"):
            surfaces = self.plot_objects["surfaces"]
            images = self.plot_objects["images"]
        else:
            surfaces, images = self.surfaces, self.images
        self._set_colorbar([image.ax_image for image in images])
        self._set_colorbar([surface.ax_image for surface in surfaces])

    def set_all_curves_same_color(self, color: str) -> None:
        """Set the same color to all curves.

        Parameters
        ----------
        color: str
            The desired curve color.
        """
        self.curves_color = color

    def _set_curves_color(self, curves, colors):
        if is_list_like(colors):
            if len(colors) != len(curves):
                raise ValueError(f"{colors} is not same length as curves"
                                 f" ({len(curves)}).")
        else:
            colors = [colors] * len(curves)
        self._logger.debug(f"Setting curves colors to {colors}")
        for color, curve in zip(colors, curves):
            # if not isinstance(color, str):
            #     raise TypeError(f"Color must be a string but got: {color}")
            curve.color = color

    def _set_default_fontsizes(self):
        self.legend_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self.xlabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self.xtick_fontsize = __DEFAULT_TICK_FONTSIZE__
        self.ylabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self.ytick_fontsize = __DEFAULT_TICK_FONTSIZE__
        self.zlabel_fontsize = __DEFAULT_LABEL_FONTSIZE__
        self.ztick_fontsize = __DEFAULT_TICK_FONTSIZE__

    def _set_labels(self):
        # set title and axis labels
        self._ax.set_title(self.title)
        self._ax.set_xlabel(self.xlabel, fontsize=self.xlabel_fontsize)
        self._ax.set_ylabel(self.ylabel, fontsize=self.ylabel_fontsize)
        if self._need_3d_projection:
            self._ax.set_zlabel(self.zlabel, fontsize=self.zlabel_fontsize)
            self._ax.tick_params(
                axis="z", which="both", labelsize=self.ztick_fontsize)
        if self._need_twinx:
            self._twinx.set_ylabel(
                    self.ylabel_twinx, fontsize=self.ylabel_fontsize)
            self._twinx.tick_params(
                axis="y", which="both", labelsize=self.ytick_fontsize)
        if self._need_twiny:
            self._twiny.set_ylabel(
                    self.xlabel_twiny, fontsize=self.xlabel_fontsize)
            self._twiny.tick_params(
                axis="x", which="both", labelsize=self.xtick_fontsize)
        # set rotation
        if hasattr(self, "xlabel_rotation"):
            if self.xlabel_rotation is not None:
                xlabel = self._ax.get_xlabel()
                self._ax.set_xlabel(xlabel, rotation=self.xlabel_rotation)
            if self.ylabel_rotation is not None:
                ylabel = self._ax.get_ylabel()
                self._ax.set_ylabel(ylabel, rotation=self.ylabel_rotation)
        # set coordinates
        if hasattr(self, "xlabel_coordinates"):
            if self.xlabel_coordinates is not None:
                self._ax.xaxis.set_label_coords(*self.xlabel_coordinates)
            if self.ylabel_coordinates is not None:
                self._ax.yaxis.set_label_coords(*self.ylabel_coordinates)

    def _set_legend(self):
        # show legend if needed and if at least one curve_labels is not None
        lines, labels = self._ax.get_legend_handles_labels()
        # set legend on this axis
        ax = self._ax
        if self._need_twinx:
            lines2, labels2 = self._twinx.get_legend_handles_labels()
            lines += lines2
            labels += labels2
            ax = self._twinx
        if self._need_twiny:
            lines3, labels3 = self._twiny.get_legend_handles_labels()
            lines += lines3
            labels += labels3
        if hasattr(self, "plot_objects"):
            contours = self.plot_objects["contours"]
        else:
            contours = self.contours
        if contours:
            # contours are handed differently
            # https://github.com/matplotlib/matplotlib/issues/11134#issuecomment-384966888
            all_contour_lines = [
                    c.contour.legend_elements()[0][0] for c in contours]
            contour_labels = [c.label for c in contours]
            # remove lines for None labels
            lines += [
                    line for line, label in zip(
                        all_contour_lines, contour_labels) if label is not None
                    ]
            labels += [label for label in contour_labels if label is not None]
        if self.legend and (labels or (
                self.custom_legend_args or self.custom_legend_kwargs)):
            args = [lines, labels]
            kwargs = {
                    "loc": "best",
                    "fontsize": self.legend_fontsize,
                    "framealpha": self.legend_transparency,
                    }
            if hasattr(self, "legend_loc"):
                kwargs["loc"] = self.legend_loc
            if self.legend_outside:
                kwargs["bbox_to_anchor"] = (1, 0.5)
                kwargs["loc"] = "center left"
            if self.custom_legend_args or self.custom_legend_kwargs:
                kwargs = self.custom_legend_kwargs or {}
                args = self.custom_legend_args or []
                legend = ax.legend(*args, **kwargs)
            else:
                legend = ax.legend(*args, **kwargs)
            # make legend draggable provided that backend allows it
            legend.set_draggable(True)

    def _set_ticks(self):
        # set ticks
        if not hasattr(self, "xtick_fontsize"):
            self._set_default_fontsizes()
        for attr, ax in zip(("xtick_labels", "xtick_labels_twiny"),
                            (self._ax, self._twiny)):
            selftick = getattr(self, attr)
            if selftick is not None:
                ticks_loc = [x[0] for x in selftick]
                ticks_labels = [x[1] for x in selftick]
                ax.set_xticks(ticks_loc)
                ax.set_xticklabels(ticks_labels)
        for attr, ax in zip(("ytick_labels", "ytick_labels_twinx"),
                            (self._ax, self._twinx)):
            selftick = getattr(self, attr)
            if selftick is not None:
                ticks_loc = [x[0] for x in selftick]
                ticks_labels = [x[1] for x in selftick]
                ax.set_yticks(ticks_loc)
                ax.set_yticklabels(ticks_labels)
        for attr, ax in zip(("xtick_rotation", "xtick_rotation_twiny"),
                            (self._ax, self._twiny)):
            selfrot = getattr(self, attr)
            if selfrot is not None:
                for tick in ax.get_xticklabels():
                    tick.set_rotation(selfrot)
        # other tick params
        kwargs = {}
        if hasattr(self, "hide_xtick_labels"):
            kwargs["labelbottom"] = not self.hide_xtick_labels
        if hasattr(self, "hide_xtick_bottom"):
            kwargs["bottom"] = not self.hide_xtick_bottom
        self._ax.tick_params(
                axis="x", which="both", labelsize=self.xtick_fontsize,
                **kwargs,
                )
        kwargs = {}
        if hasattr(self, "hide_ytick_labels"):
            kwargs["labelleft"] = not self.hide_ytick_labels
        self._ax.tick_params(
                axis="y", which="both", labelsize=self.ytick_fontsize,
                **kwargs,
                )

    @staticmethod
    def load_plot(path, loglevel=None):
        """Load a :class:`Plot <abisuite.plotters.plot.Plot>` from file.

        To save a plot, see the
        :meth:`save_plot <abisuite.plotters.plot.Plot.save_plot>` method.

        Parameters
        ----------
        path : str
            The path to the file where the object is contained.
        loglevel: int, optional
            If not None, sets the loglevel to this value. Otherwise,
            the value of the pickle will be taken since this value was
            pickled like everything else.
        """
        if not os.path.isfile(path):
            raise FileNotFoundError(path)
        with open(path, "rb") as f:
            plot = pickle.load(f)
        if loglevel is not None:
            plot._loglevel = loglevel
        if hasattr(plot, "_set_default"):
            plot._set_default()
        return plot

    def _set_default(self):
        # make sure all attributes are there if not set already
        defaults_none = ("custom_legend_args", "custom_legend_kwargs")
        for default_none in defaults_none:
            if not hasattr(self, default_none):
                setattr(self, default_none, None)
