class AbiDBRuntimeError(RuntimeError):
    """Custom error for abisuite DB errors."""

    pass


class CalculationDoesNotExistError(FileNotFoundError):
    """Error thrown when a calculation does not exists in a DB."""

    pass


class CachedAbiDBFileNotFoundError(FileNotFoundError):
    """Error thrown when a db file is not found in cache directory."""

    pass


class ColumnDoesNotExistsError(AbiDBRuntimeError):
    """Error thrown when a column does not exists."""

    pass
