import json

try:
    import peewee
    from peewee import (
            BooleanField, FloatField, Model, TextField,
            )
except ImportError:
    class DummyClass:
        """Dummy class that absorbs everything in order to prevent crashing.

        Since on some computers, sqlite might not be installed but some
        features of abisuite can still work without them.
        """

        def __init__(self, *args, **kwargs):
            pass
    TextField = DummyClass
    FloatField = DummyClass
    Model = DummyClass
    BooleanField = DummyClass

from .. import __USER_DATABASE__
from ..retryer import try_with_retries_sync
from ..routines import full_abspath


class DictField(TextField):
    """Custom database field for dictionaries.

    These are stored with as json strings and thus the dicts
    must be jsonable.
    """

    def db_value(self, value):
        """Convert the python value to DB value."""
        return json.dumps(value)

    def python_value(self, value):
        """Convert the databse value to python value."""
        if value is None:
            return value
        return json.loads(value)


class DBCalculation(Model):
    """Calculation DB model."""

    calculation = TextField(
            unique=True, help_text="The calculation's path.",
            verbose_name="Calculation path",
            )
    status = DictField(
            unique=False, default=None, null=True,
            help_text="The calculation's status dictionary.",
            )
    walltime = FloatField(
            unique=False, default=None, null=True,
            help_text="The calculation's walltime.",
            )
    monitored = BooleanField(
            unique=False, default=True,
            help_text="True if calculation is monitored.",
            )
    packaged = BooleanField(
            unique=False, default=False,
            help_text="True if the calculation was packaged.",
            )
    project = TextField(
            unique=False, default=None, null=True,
            help_text="The project the calculation is associated to.",
            )

    class Meta:  # noqa: D106
        # only_save_dirty = True
        database = __USER_DATABASE__

    @classmethod
    def create(cls, *args, **kwargs):
        """Insert new calculation into the database."""
        data = kwargs
        from ..handlers import CalculationDirectory
        if args:
            if isinstance(args[0], CalculationDirectory):
                # overwrite kwargs
                calc = args[0]
                data = {"calculation": calc.path}
        return super(DBCalculation, cls).create(**data)

    @classmethod
    def insert(cls, *args, **kwargs):
        """Insert new calculation into the database."""
        data = kwargs
        from ..handlers import CalculationDirectory
        if args:
            if isinstance(args[0], CalculationDirectory):
                # overwrite kwargs
                calc = args[0]
                data = {"calculation": calc.path}
        return super(DBCalculation, cls).insert(**data)

    @classmethod
    def clean_database(cls):
        """Cleans the database from non-existant calculations."""
        from ..handlers import is_calculation_directory
        for calc in cls.select(cls.calculation):
            if not is_calculation_directory(calc.calculation):
                cls.remove_instance()

    async def get_status(self, recompute=False):
        """Returns the status of a calculation and update it if needed.

        Parameters
        ----------
        recompute: bool, optional
            If True and the status loaded provides a 'calculation_finished' to
            False, the status will be recomputed and put back into the DB.
        """
        status = self.status.value
        if status is None:
            if recompute is False:
                return status
            await self.update_calculation_status(
                    new_status=None, recompute=True)
            return await self.get_status()
        if status["calculation_finished"] is False:
            if recompute:
                status = await self._compute_new_calculation_status()
                # update status but return this one
                await self.update_calculation_status(new_status=status)
        return status

    def get_walltime(self, force=False, refresh=False):
        """Return the walltime and recompute if needed.

        Parameters
        ----------
        refresh: bool, optional
            If True and walltime is None or force is True, the walltime
            entry is refreshed.

        Returns
        -------
        float: The walltime of the calculation. Can be None.
        """
        walltime = self.walltime.value
        if walltime is None:
            if not refresh:
                return walltime
            else:
                self.update_walltime(new_walltime=None, force=force)
                # set refresh to False in case of error to prevent
                # infinite recursion
                return self.get_walltime(force=force, refresh=False)
        else:
            # walltime is not None. only refresh if forced
            if not refresh or not force:
                return walltime
            else:
                # update walltime
                self.update_walltime(new_walltime=walltime, force=force)
                return walltime

    async def refresh(self):
        """Refreshes the calculation status and walltime."""
        await self.update_calculation_status(recompute=True)
        self.update_walltime()

    async def update_calculation(self, new_calculation, force=False):
        """Update the calculation path.

        Parameters
        ----------
        new_calculation : str
            The new path to the calculation. Must be a valid calculation path.
        force: bool, optional
            If True, the new_calculation is forced into the db even though
            it is not a valid calculation path.

        Raises
        ------
        ValueError: If new_calculation already in database.
        NotADirectoryError: If new_calculation is not a directory unless
            the 'force' option is used.
        """
        new_calculation = full_abspath(new_calculation)
        if self.get_or_none(
                self.calculation == new_calculation) is not None:
            raise ValueError(
                    f"Calculation already in database: '{new_calculation}'.")
        from ..handlers import is_calculation_directory
        if not await is_calculation_directory(
                new_calculation) and not force:
            raise NotADirectoryError(
                    f"Not a calculation directory: '{new_calculation}'.")
        query = DBCalculation.update(calculation=new_calculation).where(
                DBCalculation.calculation == self.calculation)
        self._execute_query(query)
        self.calculation = new_calculation

    def _execute_query(self, query):
        try_with_retries_sync(
                query.execute, delay=5,
                allowed_exceptions=(peewee.OperationalError, ))

    def update_packaged(self, packaged):
        """Update the 'packaged' attribute.

        Parameters
        ----------
        packaged: bool
            The new packaged attribute value.
        """
        if packaged is not True and packaged is not False:
            raise TypeError(
                    "'packaged' must be True or False.")
        query = DBCalculation.update(packaged=packaged).where(
                DBCalculation.calculation == self.calculation)
        self._execute_query(query)
        self.packaged = packaged

    async def update_walltime(self, new_walltime=None, force=False):
        """Updates the walltime of a calculation entry.

        Parameters
        ----------
        new_walltime: float, optional
            If not None, states the new walltime to use. If None, it will be
            computed.
        force: bool, optional
            If new_walltime is None and force is True, then the updated
            walltime will be forced to None.
        """
        if new_walltime is not None or force:
            query = DBCalculation.update(
                    walltime=new_walltime).where(
                            DBCalculation.calculation == self.calculation
                            )
            self.walltime = new_walltime
        else:
            from ..handlers import CalculationDirectory
            async with await CalculationDirectory.from_calculation(
                    self.calculation) as calc:
                status = await calc.status
                finished = status["calculation_finished"]
                if finished is not True:
                    query = DBCalculation.update(
                            walltime=new_walltime
                            ).where(
                                DBCalculation.calculation == self.calculation
                                )
                    self.walltime = new_walltime
                else:
                    # compute new walltime since calc is finished
                    async with await calc.log_file as log:
                        walltime = log.walltime
                        if isinstance(walltime, float):
                            dbcalc = DBCalculation.calculation
                            query = DBCalculation.update(
                                    {DBCalculation.walltime: walltime}
                                    ).where(
                                        dbcalc == self.calculation
                                        )
                            self.walltime = walltime
                    await self.update_calculation_status(
                            new_status=status, recompute=False)
        self._execute_query(query)

    async def update_calculation_status(
            self, new_status=None,
            update_status=None,
            recompute=False):
        """Update one calculation status based on its path or id.

        Parameters
        ----------
        new_status: dict, optional
            If dict, this should be the (complete) calculation status. If None,
            the computation status will be (re)computed if 'recompute' is True
            or nothing is stored inside the DB.
        update_status: dict, optional
            If not None, updates the previous entry with this dict. Useful
            when updating only one item of the status dict.
        """
        if new_status is None:
            if recompute:
                # (re)compute status
                query = DBCalculation.update(status=None).where(
                        DBCalculation.calculation == self.calculation)
                self._execute_query(query)
                new_status = await self._compute_new_calculation_status()
            else:
                new_status = await self.get_status()
        if update_status is not None:
            new_status.update(update_status)
        self.status = new_status
        query = DBCalculation.update(status=new_status).where(
                    DBCalculation.calculation == self.calculation)
        self._execute_query(query)

    async def _compute_new_calculation_status(self):
        # get calcdir and set new status
        from ..handlers import CalculationDirectory
        async with await CalculationDirectory.from_calculation(
                self.calculation) as calcdir:
            # compute status and update database
            return await calcdir.status
