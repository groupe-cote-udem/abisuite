import numpy as np

from .bases import BasePostProcClass
from ..handlers import (
        AbinitBSEMDFFile,
        AbinitOpticLincompFile,
        CalculationDirectory,
        )
from ..plotters import MultiPlot
from ..routines import is_list_like


class DielectricTensor(BasePostProcClass):
    """Object that represents a Dielectric tensor.

    It reads all the linear
    optic files from a calculation and can plot the tensor.
    """

    _loggername = "DielectricTensor"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._energies = None
        self._dielectric_tensor = None
        # default symbols
        symbol = r"$\epsilon$"
        self.dielectric_tensor_labels = [
                [symbol + r"$_{xx}$",
                 symbol + r"$_{xy}$",
                 symbol + r"$_{xz}$"],
                [symbol + r"$_{yx}$",
                 symbol + r"$_{yy}$",
                 symbol + r"$_{yz}$"],
                [symbol + "$_{zx}$",
                 symbol + "$_{zy}$",
                 symbol + r"$_{zz}$"],
                ]
        self.dielectric_tensor_symbol = r"$\epsilon_{ij}(\omega)$"

    @property
    def dielectric_tensor(self):
        """Return the dielectric tensor array."""
        if self._dielectric_tensor is not None:
            return self._dielectric_tensor
        raise ValueError(
                "Need to set 'dielectric_tensor'.")

    @dielectric_tensor.setter
    def dielectric_tensor(self, tensor):
        self._dielectric_tensor = tensor

    @property
    def energies(self):
        """Return the frequencies array."""
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set 'energies'.")

    @energies.setter
    def energies(self, energies):
        self._energies = energies

    # This is an alias
    @property
    def frequencies(self):
        """Return the frequencies array."""
        return self.energies

    @frequencies.setter
    def frequencies(self, freqs):
        self.energies = freqs

    def create_plot(
            self, elements=None, linestyle="-", label="", color=None,
            linewidth=2, multiplot=True, **kwargs):
        """Create the dielectric tensor plot.

        Either a MultiPlot is
        generated or two separated plot depending of the given
        parameters.

        Parameters
        ----------
        elements: list, optional
            Gives which elements of the tensor to plot. It should be a list
            of two-elements-lists which contains the indices to plot
            (0, 1 or 2). If None, all elements are plotted.
        linestyle: str, optional
            Gives the linestyles of the lines.
        linewidth: int, optional
            The linewidth of the curves.
        label: str, optional
            Extra label to add to the tensor labels.
        color: str or list, optional
            Forces a curve color. If a list is given, it is the color for
            each tensor elements plotted. Thus it must be the same length
            as the 'elements' kwarg.
        multiplot: bool, optional
            If True, a MultiPlot is returned instead of two separated plots.
            In that case, the plots are aligned vertically and the upper
            part is the real part of the dielectric tensor and the lower
            part is its imaginary part.
        """
        plot_re = super().create_plot(
                xlabel=kwargs.pop("xlabel", r"$\omega$"),
                xunits=kwargs.pop("xunits", "eV"),
                ylabel=kwargs.pop(
                    "ylabel",
                    r"$Re[$" + self.dielectric_tensor_symbol + r"$]$",
                    ),
                **kwargs
                )
        plot_im = super().create_plot(
                xlabel=kwargs.pop("xlabel", r"$\omega$"),
                xunits="eV",
                ylabel=kwargs.pop(
                    "ylabel",
                    r"$Im[$" + self.dielectric_tensor_symbol + r"$]$",
                    ),
                **kwargs
                )
        ydatas = []
        labels = []
        colors = [f"C{i}" for i in range(6)]
        if elements is None:
            indices = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]
        else:
            colors = colors[:len(elements)]
            if color is not None:
                if not is_list_like(color):
                    colors = [color] * len(colors)
                else:
                    colors = color
                if len(colors) != len(elements):
                    raise ValueError(
                            "List of colors must be same length as number of "
                            " tensor elements to plot.")
            indices = elements
        if label and not label.startswith(" "):
            # add beginning space if necessary
            label = " " + label
        for index in indices:
            i = index[0]
            j = index[1]
            ydatas.append(self.dielectric_tensor[i, j, :])
            labels.append(self.dielectric_tensor_labels[i][j] + label)
        for ydata, ylabel, color in zip(ydatas, labels, colors):
            plot_re.add_curve(
                    self.frequencies, ydata.real, label=ylabel,
                    linewidth=linewidth,
                    color=color, linestyle=linestyle,
                    )
            plot_re.grid = True
            plot_re.legend = True
            plot_im.add_curve(
                    self.frequencies, ydata.imag, label=ylabel,
                    linewidth=linewidth,
                    color=color, linestyle=linestyle,
                    )
            plot_im.grid = True
            plot_im.legend = True
        if multiplot:
            multiplot = MultiPlot(loglevel=self._loglevel)
            # remove the xlabel from the top one
            plot_re.xlabel = ""
            multiplot.add_plot(plot_re, 0)
            multiplot.add_plot(plot_im, 1)
            return multiplot
        return plot_re, plot_im

    @classmethod
    async def from_optic_lincomp_handlers(cls, handlers, *args, **kwargs):
        """Create a DielectricTensor obj from a list of optic lincomp handlers.

        Parameters
        ----------
        files: list-like
            The list of file handlers.
        """
        if isinstance(handlers, AbinitOpticLincompFile):
            # single item, create list
            handlers = (handlers, )
        if not is_list_like(handlers):
            raise TypeError(
                    f"Need a list of handlers but got '{handlers}'.")
        obj = cls(*args, **kwargs)
        tensor = None
        for handler in handlers:
            async with handler as lin:
                freqs = lin.frequencies
                if tensor is None:
                    tensor = np.zeros((3, 3, len(freqs)), dtype=np.complex128)
                # component is a string e.g.: '11', '12', ...
                # need to convert to actual indices
                component = lin.component
                idx = int(component[0]) - 1
                idy = int(component[1]) - 1
                # this is a complex tensor
                tensor[idx, idy] = lin.epsilon
        obj.dielectric_tensor = tensor
        obj.frequencies = freqs
        return obj

    @classmethod
    async def from_abinit_bse_handler(
            cls, handler, mdf_type=None, *args, **kwargs):
        """Create a DielectricTensor object from a _MDF file handler."""
        if not isinstance(handler, AbinitBSEMDFFile):
            raise TypeError(handler)
        diel = cls(*args, **kwargs)
        async with handler:
            diel.frequencies = handler.frequencies
            diel.dielectric_tensor = handler.epsilon
            for i, qpt_list in enumerate(handler.qpoints):
                for j, qpt in enumerate(qpt_list):
                    new_qpt = []
                    for qpt_component in qpt:
                        if qpt_component == 0.0:
                            new_qpt.append("0")
                        elif qpt_component > 0.0:
                            new_qpt.append(r"$0^+$")
                        else:
                            new_qpt.append(r"$0^-$")
                    # deal with spacing to make graph pretty
                    if "^" in new_qpt[0]:
                        if "^" not in new_qpt[1]:
                            new_qpt.insert(2, " ")
                    diel.dielectric_tensor_labels[i][j] = (
                            r"$\bf{q}$" + " = " + " ".join(new_qpt))
                    diel.dielectric_tensor_symbol = (
                        r"$\epsilon(\omega,\bf{q}$)"
                        )
        return diel

    @classmethod
    async def from_mdf_file(cls, path: str, *args, **kwargs):
        """Creates a DielectricTensor object from a MDF file.

        Parameters
        ----------
        path: str
            The path to the MDF file to load the DielectricTensor from.

        Returns
        -------
        DielectricTensor: The dielectric tensor object.
        """
        async with await AbinitBSEMDFFile.from_file(
                path, loglevel=kwargs.get("loglevel", None)) as mdf:
            return await cls.from_abinit_bse_handler(mdf, *args, **kwargs)

    @classmethod
    async def from_calculation(cls, path, *args, mdf_type=None, **kwargs):
        """Creates a DielectricTensor object from a calculation.

        This methods fills the attributes accordingly.
        Currently, only an 'abinit_optic' run or a BSE run with abinit
        is supported.

        Parameters
        ----------
        path: str
            The path to the calculation to load the DielectricTensor from.
        mdf_type: {'exc', 'gw_nlf', 'rpa_nlf'}, optional
            The mdf type to plot. When computing BSE, there are 3 MDF files
            that contains the dielectric tensor in different ways.
            This parameter lets the user to choose from the one needed.
            If None and a BSE calculation is detected, an error is raised.

        Returns
        -------
        DielectricTensor: The dielectric tensor object.

        Raises
        ------
        ValueError:
            - If an abinit run is given as input but it is not a BSE run.
            - If 'mdf_type' is not given while a BSE calculation is given.
        """
        async with await CalculationDirectory.from_calculation(path) as calc:
            # first check the calctype is supported
            if await calc.calctype == "abinit_optic":
                # there might be more than one files within a single calc
                # this, inititate from calculation and iterate through the list
                files_ = await AbinitOpticLincompFile.from_calculation(calc)
                return await cls.from_optic_lincomp_handlers(
                        files_, *args, **kwargs)
            elif await calc.calctype == "abinit":
                # check if it is a BSE calculation
                async with calc.input_file as inf:
                    optdriver = inf.input_variables.get("optdriver", 0)
                    if optdriver != 99:
                        raise ValueError("Not a BSE calculation!")
                if mdf_type not in ("exc", "gw_nlf", "rpa_nlf"):
                    raise ValueError("BSE detected: need to set 'mdf_type'.")
                diel = await cls.from_abinit_bse_handler(
                        (await AbinitBSEMDFFile.from_calculation(calc))[
                            mdf_type],
                        *args, **kwargs)
                return diel

    @classmethod
    async def from_file(cls, path, *args, **kwargs):
        """Creates a DielectricTensor object from a file.

        Currently, only 'linopt' files from an 'abinit_optic' calculation is
        supported for this method.

        Parameters
        ----------
        path: str
            The file path.
        """
        return await cls.from_optic_lincomp_handlers(
                (await AbinitOpticLincompFile.from_file(path), ),
                *args, **kwargs)
