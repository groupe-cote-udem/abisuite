from typing import Sequence

import numpy as np

from .bases import BasePostProcClass
from ..handlers import CalculationDirectory, QEEPWLogFile
from ..plotters import Plot
from ..routines import is_vector


class AdaptativeSmearingHistogram(BasePostProcClass):
    """Post processing class for an adaptative smearing histogram."""

    _loggername = "AdaptativeSmearingHistogram"

    def __init__(
            self, *args,
            adaptative_smearing_mins: Sequence[float] = None,
            adaptative_smearing_maxs: Sequence[float] = None,
            delta_bins: float = 1,
            **kwargs,
            ) -> None:
        """Adaptative smearing histogram init method.

        Parameters
        ----------
        adaptative_smearing_mins: sequence, optional
            The array of minimums of the adaptative smearing.
        adaptative_smearing_maxs: sequence, optional
            The array of maximums of the adaptative smearing.
        delta_bins: float, optional
            The histogram bin size (in meV).
        """
        super().__init__(*args, **kwargs)
        self._adaptative_smearing_mins = None
        self._adaptative_smearing_maxs = None
        self.delta_bins = delta_bins
        if adaptative_smearing_mins is not None:
            self.adaptative_smearing_mins = adaptative_smearing_mins
        if adaptative_smearing_maxs is not None:
            self.adaptative_smearing_maxs = adaptative_smearing_maxs

    @property
    def adaptative_smearing_mins(self) -> Sequence[float]:
        """The adaptative smearing minimums."""
        if self._adaptative_smearing_mins is not None:
            return self._adaptative_smearing_mins
        raise ValueError("Need to set 'adaptative_smearing_mins'.")

    @adaptative_smearing_mins.setter
    def adaptative_smearing_mins(self, mins: Sequence[float]) -> None:
        if not is_vector(mins):
            raise TypeError("'adaptative_smearing_mins' should be array-like.")
        self._adaptative_smearing_mins = mins

    @property
    def adaptative_smearing_maxs(self) -> Sequence[float]:
        """The adaptative smearing maximums."""
        if self._adaptative_smearing_maxs is not None:
            return self._adaptative_smearing_maxs
        raise ValueError("Need to set 'adaptative_smearing_maxs'.")

    @adaptative_smearing_maxs.setter
    def adaptative_smearing_maxs(self, maxs: Sequence[float]) -> None:
        if not is_vector(maxs):
            raise TypeError("'adaptative_smearing_maxs' should be array-like.")
        self._adaptative_smearing_maxs = maxs

    def create_plot(self, *args, **kwargs) -> Plot:
        """Create the adaptative smearing histogram."""
        plot = super().create_plot(
                *args, ylabel="# counts",
                xlabel="Adaptative smearings", xunits="meV",
                **kwargs)
        # start with minimums
        amax = np.max(self.adaptative_smearing_maxs)
        amin = np.min(self.adaptative_smearing_mins)
        bins = np.arange(
                np.floor(amin),
                # +1 to include endpoint
                np.ceil(amax) + 1,
                self.delta_bins,
                )
        plot.add_histogram(
                bins, self.adaptative_smearing_mins, color="C0",
                alpha=0.5, label=f"Min: {amin} meV",
                )
        plot.add_histogram(
                bins, self.adaptative_smearing_maxs, color="C1",
                alpha=0.5, label=f"Max: {amax} meV",
                )
        plot.xlims = [0, None]
        plot.ylims = [0, None]
        return plot

    @classmethod
    async def from_handlers(cls, *handlers, **kwargs):
        """Create an AdaptativeSmearingHistogram inst. from many handlers."""
        maxs = []
        mins = []
        for handler in handlers:
            if not isinstance(handler, QEEPWLogFile):
                raise NotImplementedError(handler)
            async with handler:
                if handler.adaptative_smearings is None:
                    continue
                maxs += [d["max"] for d in handler.adaptative_smearings]
                mins += [d["min"] for d in handler.adaptative_smearings]
        return cls(
                adaptative_smearing_mins=mins, adaptative_smearing_maxs=maxs,
                **kwargs,
                )

    @classmethod
    async def from_calculation(cls, calc, **kwargs):
        """Create an AdaptativeSmearingHistogram instance from a calc."""
        # there can be multiple log files in case calculation was restarted
        loglevel = kwargs.get("loglevel", None)
        handlers = []
        async with await CalculationDirectory.from_calculation(
                calc, loglevel=loglevel) as calcd:
            async for handler in calcd:
                if ".log" not in handler.basename:
                    continue
                if isinstance(handler, QEEPWLogFile):
                    handlers.append(handler)
                else:
                    # might be a GenericHandler
                    handlers.append(
                            await QEEPWLogFile.from_file(
                                handler.path, loglevel=loglevel)
                            )
        return await cls.from_handlers(*handlers, **kwargs)
