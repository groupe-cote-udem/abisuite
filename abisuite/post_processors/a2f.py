import numpy as np

from .bases import BasePostProcClass
from ..handlers import QEEPWa2FFile
from ..routines import is_list_like


class a2F(BasePostProcClass):  # noqa: N801
    """Class utility that can plot the a2F eliashberg function."""

    _loggername = "a2F"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._frequencies = None
        self._a2F = None

    @property
    def a2F(self):  # noqa: N802
        """Return the a2F function array."""
        return self._a2F

    @a2F.setter
    def a2F(self, a2f):  # noqa: N802
        if not is_list_like(a2f):
            raise TypeError(
                    f"Expected list-like but got '{a2f}'.")
        if self.frequencies is not None:
            if len(self.frequencies) != len(a2f):
                raise ValueError("Length of freqs must match len of a2F!")
        self._a2F = np.array(a2f)

    @property
    def frequencies(self):
        """Return the frequencies array."""
        return self._frequencies

    @frequencies.setter
    def frequencies(self, freqs):
        if not is_list_like(freqs):
            raise TypeError(
                    f"Expected list-like but got '{freqs}'.")
        if self.a2F is not None:
            if len(self.a2F) != len(freqs):
                raise ValueError("Length of freqs must match len of a2F!")
        self._frequencies = np.array(freqs)

    def get_plot(
            self, *args, xlabel=r"$\omega$", xunits="meV",
            ylabel=r"$\alpha^2F(\omega)$",
            index=0, **kwargs):
        """Get the a2F(w) plot in function of w.

        Parameters
        ----------
        xlabel, xunits, ylabel : str, optional
            Parameters passed to the super class method.
        index : int, optional
            Gives the index for which eliasberg function to use.
            EPW computes many a2F for different broadenings, this index
            the one desired.
        """
        if self.a2F is None:
            raise ValueError("Need to set 'a2F'.")
        if self.frequencies is None:
            raise ValueError("Need to set frequencies.")
        yunits = kwargs.pop("yunits", "")
        title = kwargs.pop("title", "")
        plot = super().get_plot(
                xlabel=xlabel, xunits=xunits, ylabel=ylabel, yunits=yunits,
                title=title)
        plot.add_curve(self.frequencies, self.a2F[:, index], *args, **kwargs)
        return plot

    @classmethod
    def from_file(cls, path, *args, **kwargs):
        """Create an a2F object from a file."""
        instance = cls(*args, **kwargs)
        with QEEPWa2FFile.from_file(path, loglevel=instance._loglevel) as a2f:
            instance.frequencies = a2f.frequencies
            instance.a2F = a2f.a2F
        return instance
