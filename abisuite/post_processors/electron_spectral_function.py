import numpy as np

from .bases import BasePostProcClassWithEnergyDispersions
from ..handlers import QEEPWSpecfunFile
from ..routines import is_2d_array


class ElectronSpectralFunction(BasePostProcClassWithEnergyDispersions):
    """Class that process and plot electron spectral functions."""

    _loggername = "ElectronSpectralFunction"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._spectral_function = None

    @property
    def nbands(self):
        """Number of bands not relevent here."""
        raise RuntimeError("No bands in a spectral function usually...")

    @property
    def spectral_function(self):
        """Return the spectral function array."""
        if self._spectral_function is not None:
            return self._spectral_function
        raise ValueError("Need to set 'spectral_function'.")

    @spectral_function.setter
    def spectral_function(self, spec):
        if not is_2d_array(spec):
            raise TypeError(
                    "The spectral function should be a 2D array "
                    "(nkpts x nfreqs).")
        if self._energies is not None:
            if self.energies.ndim > 1:
                self._energies = self.energies.flatten()
            if len(self.energies) != spec.shape[-1]:
                raise AssertionError(
                        f"Size of spectral function ({spec.shape[-1]}) does "
                        f"not match the length of energies ("
                        f"{len(self.energies)}).")
        self._spectral_function = spec
        self._nkpts = spec.shape[0]

    def _set_energies(self, *args):
        super()._set_energies(
                *args, set_nkpts=False, set_nbands=False)

    def create_plot(
            self, *args, plot_dispersion=False, dispersion_color="k",
            dispersion_linewidth=1,
            **kwargs):
        """Get the spectral function plot.

        Parameters
        ----------
        plot_dispersion : bool, optional
            If True and the eigenvalues are stored in the SpectalFunction
            object, then the dispersion is added on top.
        dispersion_color : str, optional
            The color of the phonon dispersion in case it is plotted.
        dispersion_linewidth : float, optional
            The linewidth of the dispersion curves in case they are plotted.
        """
        kw = {}
        kw["xlabel"] = kwargs.pop("xlabel", "")
        kw["xunits"] = kwargs.pop("xunits", None)
        kw["ylabel"] = kwargs.pop("ylabel", "")
        kw["yunits"] = kwargs.pop("yunits", None)
        kw["title"] = kwargs.pop("title", "")
        plot = super().create_plot(*args, **kw)
        # xdata is just range(0, nqpts)
        plot.add_image(
                np.arange(0, self.nkpts) / self.nkpts,
                self.energies,
                self.spectral_function.T, **kwargs)
        return plot

    @classmethod
    async def from_file(cls, path, *args, **kwargs):
        """Create an ElectronSpectralFunction object from a file."""
        async with await QEEPWSpecfunFile.from_file(
                path, loglevel=kwargs.get("loglevel", None)) as handler:
            return await cls.from_handler(handler, *args, **kwargs)

    @classmethod
    async def from_handler(cls, handler, *args, **kwargs):
        """Create an ElectronSpectralFunction object from a file handler."""
        instance = cls(*args, **kwargs)
        async with handler:
            instance.spectral_function = handler.spectral_function
            instance.energies = handler.energies
        return instance
