import numpy as np

from .bases import BasePostProcClassWithEnergies
from ..handlers import QEEPWDecayFile


class MLWFDecay(BasePostProcClassWithEnergies):
    """Post processing class to get MLWF decay plots."""

    _loggername = "MLWFDecay"

    def __init__(self, *args, decay_type: str = None, **kwargs):
        super().__init__(*args, **kwargs)
        self._wannier_centers = None
        self.decay_type = decay_type

    @property
    def wannier_centers(self):
        """The wannier centers."""
        if self._wannier_centers is not None:
            return self._wannier_centers
        raise ValueError("Need to set wannier_centers.")

    @wannier_centers.setter
    def wannier_centers(self, centers):
        self._wannier_centers = centers

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Initialize a MLWFHamiltonianDecay object from a calculation."""
        handlers = await QEEPWDecayFile.from_calculation(
                path, loglevel=kwargs.get("loglevel", None))
        instances = {}
        for type_, handler in handlers.items():
            inst = cls(*args, decay_type=type_, **kwargs)
            async with handler:
                inst.energies = handler.decay
                inst.wannier_centers = handler.wannier_centers
            instances[type_] = inst
        return instances

    def create_plot(self, *args, **kwargs):
        """Create the hamiltonian decay plot."""
        if self.decay_type == "H":
            ylabel = r"max$|H_{mn}(0,\mathbf{R}_e)|$"
            xlabel = r"$|\mathbf{R}_e|$"
        elif self.decay_type == "epmate":
            ylabel = r"max$|g_{mn\nu}(\mathbf{R}_e,0)|$"
            xlabel = r"$|\mathbf{R}_e|$"
        elif self.decay_type == "epmatp":
            ylabel = r"max$|g_{mn\nu}(0, \mathbf{R}_p)|$"
            xlabel = r"$|\mathbf{R}_p|$"
        elif self.decay_type == "v":
            ylabel = r"max$|v_{mn}(0,\mathbf{R}_e)$"
            xlabel = r"$|\mathbf{R}_e|$"
        else:
            raise ValueError(self.decay_type)
        plot = super().create_plot(
                *args, ylabel=ylabel, yunits="normalized",
                xunits=r"$\AA$", xlabel=xlabel,
                **kwargs)
        plot.add_scatter(
                self.wannier_centers,
                self.energies / np.max(self.energies),
                semilogy=True,
                markersize=20, color="C0", markerfacecolor="C0",
                zorder=2.5,
                )
        plot.xlims = [0, None]
        return plot
