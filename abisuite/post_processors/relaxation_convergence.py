from typing import Sequence

import numpy as np

from .bases import BasePostProcClass
from ..constants import (
        HARTREE_TO_EV,
        HARTREE_per_BOHR3_TO_GPA,
        HARTREE_per_BOHR_TO_EV_per_ANGSTROM,
        )
from ..handlers import CalculationDirectory
from ..plotters import MultiPlot, Plot


class RelaxationConvergence(BasePostProcClass):
    """Post processing class that can plot the relaxation convergence data."""

    _loggername = "RelaxationConvergence"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._relaxation_convergence_data = None

    @property
    def relaxation_convergence_data(self) -> Sequence[dict]:
        """The relaxation convergence data array."""
        if self._relaxation_convergence_data is not None:
            return self._relaxation_convergence_data
        raise ValueError("Need to set 'relaxation_convergence_data'.")

    @relaxation_convergence_data.setter
    def relaxation_convergence_data(self, data: Sequence[dict]) -> None:
        self._relaxation_convergence_data = data

    def create_plot(self) -> MultiPlot:
        """Creates a multiplot with relaxation convergence curves."""
        multi = MultiPlot(loglevel=self._loglevel)
        multi.add_plot(self.create_force_plot(), row=0)
        multi.add_plot(self.create_etot_plot(), row=0)
        multi.add_plot(self.create_pressure_plot(), row=1)
        return multi

    def create_pressure_plot(self) -> Plot:
        """Creates the pressure plot."""
        stress_tensors = (
                np.array([
                    d["stress_tensor"]
                    for d in self.relaxation_convergence_data]) *
                HARTREE_per_BOHR3_TO_GPA)
        pressures = [-np.trace(t) / 3 for t in stress_tensors]
        steps = np.arange(len(pressures), dtype=int)
        plot = Plot(loglevel=self._loglevel)
        plot.add_curve(
                steps, pressures, label="P",
                color="C0", linewidth=2, marker="o", markerfacecolor="C0",
                markersize=5)
        plot.xlabel = "Step"
        plot.ylabel = "Pressure [GPa]"
        plot.grid = True
        return plot

    def create_etot_plot(self) -> Plot:
        """Creates an energy vs step plot."""
        plot = Plot(loglevel=self._loglevel)
        etot_vs_step = [d["etotal"] for d in self.relaxation_convergence_data]
        steps = np.arange(len(etot_vs_step), dtype=int)
        plot.add_curve(
                steps, np.array(etot_vs_step) * HARTREE_TO_EV,
                marker="o", markerfacecolor="C0",
                color="C0", label="Energy", linewidth=2, markersize=5)
        plot.xlabel = "Step"
        plot.ylabel = "Energy [eV]"
        plot.grid = True
        return plot

    def create_force_plot(self) -> Plot:
        """Creates a force convergence plot."""
        plot = Plot(loglevel=self._loglevel)
        forces_vs_step = (
                np.array([
                    np.linalg.norm(d["forces"], axis=1)
                    for d in self.relaxation_convergence_data]) *
                HARTREE_per_BOHR_TO_EV_per_ANGSTROM)
        steps = np.arange(len(forces_vs_step), dtype=int)
        plot.add_curve(
            steps, np.max(forces_vs_step, axis=1),
            label="max |F|", color="C0", marker="o", markerfacecolor="C0",
            markersize=5, linewidth=2,
            )
        plot.add_curve(
            steps, np.min(forces_vs_step, axis=1),
            label="min |F|", color="C1", marker="o", markerfacecolor="C1",
            markersize=5, linewidth=2,
            )
        plot.add_curve(
            steps, np.mean(forces_vs_step, axis=1),
            label="mean |F|", color="C2", marker="o", markerfacecolor="C2",
            markersize=5, linewidth=2,
            )
        plot.add_curve(
            steps, np.std(forces_vs_step, axis=1),
            label="std |F|", color="C3", marker="o", markerfacecolor="C3",
            markersize=5, linewidth=2,
            )
        plot.xlabel = "Step"
        plot.ylabel = "F stats [eV/Angstrom]"
        plot.grid = True
        return plot

    @classmethod
    async def from_calculation(cls, path: str, *args, **kwargs):
        """Instanciate a RelaxationConvergence object from a calculation.

        Parameters
        ----------
        path: str
            The path of the calculation.
        Other args & kwargs are passed to the init method.
        """
        async with await CalculationDirectory.from_calculation(
                path, loglevel=kwargs.get("loglevel", None)) as calc:
            calctype = await calc.calctype
            if calctype != "abinit":
                raise NotImplementedError(calctype)
            log = await calc.log_file
        obj = cls(*args, **kwargs)
        async with log:
            obj.relaxation_convergence_data = (
                    log.dtsets[0]["relaxation_convergence"])
        return obj
