from typing import Sequence

import numpy as np

from scipy.integrate import trapezoid

from .bases import (
        BasePostProcClass,
        BasePostProcClassWithWeights,
        TemperatureDependentQuantity,
        TemperaturesDependentQuantity,
        )
from .chemical_potential import (
        ChemicalPotential, fermi_dirac,
        )
from ..constants import EV_TO_JOULES, JOULES_TO_EV, k_B
from ..plotters import Plot
from ..routines import is_2d_arr, is_none_numpy_safe, is_vector


class ElectronicThermodynamicProperties(ChemicalPotential):
    """Post processing class for many thermodynamical properties."""

    _loggername = "ElectronicThermodynamicProperties"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._u_electron = None
        self._f_electron = None
        self._s_electron = None
        self._u0_electron = None

    @property
    def f_electrons(self) -> float:
        """The electronic free energy at finite temperature."""
        if self._f_electron is not None:
            return self._f_electron
        self._logger.info(
                f"Computing electronic free energy at T={self.temperature}K")
        self._f_electron = self.compute_f_electron()
        return self._f_electron

    @property
    def u_electrons(self) -> float:
        """The electronic internal energy (thermal)."""
        if self._u_electron is not None:
            return self._u_electron
        self._logger.info(
                f"Computing U for electrons at T={self.temperature}K")
        self._u_electron = self.compute_u_electron()
        return self._u_electron

    @property
    def u0_electrons(self) -> float:
        """The electronic internal energy at T=0."""
        if self._u0_electron is not None:
            return self._u0_electron
        self._logger.info("Computing U for electrons at T=0K")
        self._u0_electron = self.compute_u_electron(null_temperature=True)
        return self._u0_electron

    @property
    def s_electrons(self) -> float:
        """The electronic entropy at finite temperature."""
        if self._s_electron is not None:
            return self._s_electron
        self._logger.info(
                f"Computing electronic entropy at T={self.temperature}K")
        self._s_electron = self.compute_s_electron()
        return self._s_electron

    def compute_f_electron(self) -> float:
        """Compute the electronic free energy at finite temperature."""
        if self.use_dos_formulation:
            # using F = U-TS
            return self.u_electrons - self.temperature * self.s_electrons
        kt = k_B * JOULES_TO_EV * self.temperature
        exp_arg = -(self.energies - self.mu) / (2 * kt)
        # use double precision to prevent overflows
        exp_arg = exp_arg.astype(np.float128)
        # use cosh formulation to halve exp argument to prevent furthermore
        # overflow
        log = exp_arg + np.log(2 * np.cosh(exp_arg))
        # instead of
        # log = np.log(1 + np.exp(exp_arg))
        return -kt * np.sum(log.T.dot(
            self.weights[:, None]))

    def compute_s_electron(self) -> float:
        """Compute the electronic entropy at finite temperature."""
        if self.use_dos_formulation:
            # description by Dal Corso and student
            where_good = np.logical_and(
                    self.occupations != 0, self.occupations != 1)
            one_minus_f = 1 - self.occupations[where_good]
            integrand = np.zeros_like(self.occupations)
            f = self.occupations
            integrand[where_good] = (
                    f[where_good] * np.log(f[where_good]) +
                    one_minus_f * np.log(one_minus_f)
                    )
            return -k_B * JOULES_TO_EV * trapezoid(
                    integrand * self.dos, x=self.energies[0])
        # using F = U-TS -> S = (U - F)/T
        return (self.u_electrons - self.f_electrons) / self.temperature

    def compute_u_electron(
            self, null_temperature: bool = False,
            ) -> float:
        """Compute the electronic internal thermic energy.

        Parameters
        ----------
        null_temperature: bool, optional
            If True, compute U at T=0.
        """
        if self.use_dos_formulation:
            return self._compute_u_electron_from_dos(
                    null_temperature=null_temperature,
                    )
        if null_temperature:
            occ_mask = fermi_dirac(
                    self.energies, self.fermi_energy, 0)
            tosum = ((self.energies - self.fermi_energy) * occ_mask).T.dot(
                    self.weights[:, None])
            return np.sum(tosum)
        occupations = fermi_dirac(
                self.energies,
                self.mu,
                self.temperature,
                )
        tosum = (self.energies - self.mu) * occupations
        tosum = tosum.dot(self.weights[:, None])
        return np.sum(tosum)

    def _compute_u_electron_from_dos(self, null_temperature: bool = False):
        if null_temperature:
            uptoefmask = self.energies[0] <= self.fermi_level
            return trapezoid(
                    ((self.energies[0][uptoefmask] - self.fermi_level) *
                     self.dos[uptoefmask]),
                    x=self.energies[0][uptoefmask]
                    )
        integrand1 = (self.energies[0] - self.mu) * self.dos * self.occupations
        return trapezoid(integrand1, x=self.energies[0])


class PhononThermodynamicProperties(
        BasePostProcClassWithWeights,
        TemperatureDependentQuantity,
        ):
    """Post processing class used to compute phonon thermodynamic props."""

    _loggername = "PhononThermodynamicProperties"
    _weights_sum_to = 1

    def __init__(
            self, *args, temperature: float = None, **kwargs):
        BasePostProcClassWithWeights.__init__(self, *args, **kwargs)
        TemperatureDependentQuantity.__init__(self, temperature=temperature)
        self._f_phonon = None
        self._bulk_modulus = None
        self.has_negative_phonons = False
        self.negative_phonons = None
        self._u0_phonon = None

    @property
    def u0_phonon(self):
        """The zero point motion energy of phonons."""
        if self._u0_phonon is not None:
            return self._u0_phonon
        self._u0_phonon = self.compute_u0_phonon()
        return self._u0_phonon

    @property
    def f_phonons(self) -> float:
        """The phononic free energy at finite temperature."""
        if self._f_phonon is not None:
            return self._f_phonon
        self._logger.info(
                f"Computing phononic free energy at T={self.temperature}K")
        self._f_phonon = self.compute_f_phonons()
        return self._f_phonon

    def compute_f_phonons(self) -> float:
        """Free energy computed from phonon frequencies."""
        if is_none_numpy_safe(self.weights):
            self.weights = np.full(self.nqpts, 1 / self.nqpts)
        kt = k_B * JOULES_TO_EV * self.temperature
        sinh = np.sinh(self.frequencies / (2 * kt))
        # we ignore gamma frequencies (equivalent formulation)
        where0 = np.where(self.frequencies == 0.0)
        sinh[where0] = 0.5  # so that log will be 0 afterwards
        if (self.frequencies < 0).any():
            # debug which indices and which branch is at fault.
            where = np.where(self.frequencies < 0)
            sinh[where] = 0.5
            self.has_negative_phonons = True
            self.negative_phonons = where
            # raise ValueError(
            self._logger.warning(
                "Negative frequencies at indices at "
                f"[qpt index, branch index]: {where}. The freqs are: "
                f"{self.frequencies[where]}.")
        logsinh = np.log(2 * sinh)  # nqpts x nbranches
        # weights is nqpts len array
        return kt * np.sum(logsinh.T.dot(self.weights[:, None]))

    def compute_u0_phonon(self) -> float:
        """Computes the zero temperature motion energy of phonons."""
        return np.sum(self.frequencies.T.dot(self.weights[:, None])) / 2


class HeatCapacityConstantVolume(
        BasePostProcClassWithWeights,
        TemperaturesDependentQuantity,
        ):
    """Post processing class for the heat capacity.

    Only supported for now = cubic lattices.
    """

    _loggername = "HeatCapacityConstantVolume"
    _weights_sum_to = 1

    def __init__(
            self, *args,
            lattice_vs_temperature: Sequence[float] = None,
            ibrav: int = None,
            volumes: Sequence[float] = None,
            equilibrium_volumes: Sequence[float] = None,
            phonon_frequencies: Sequence[Sequence[float]] = None,
            s_electrons: Sequence[Sequence[float]] = None,
            weights: Sequence = None,
            **kwargs,
            ):
        self.ibrav = ibrav
        BasePostProcClassWithWeights.__init__(self, *args, **kwargs)
        TemperaturesDependentQuantity.__init__(
                self, temperatures=kwargs.pop("temperatures", None))
        if self.ibrav == 2:
            self.weights = weights
            self.weights_per_volumes = None
        else:
            self.weights_per_volumes = np.asarray(weights)
            for w in weights:
                self._check_weights(w)
        self.phonon_frequencies = phonon_frequencies
        self.volumes = volumes
        self.equilibrium_volumes = equilibrium_volumes
        self.lattice = lattice_vs_temperature
        self._alpha = None
        self.s_electrons = s_electrons
        self._cv_electrons = None
        self._cv_phonons = None

    @property
    def cv_total(self) -> Sequence[float]:
        """The total heat capacity at constant volume."""
        return self.cv_electrons + self.cv_phonons

    @property
    def alpha(self) -> Sequence[float]:
        """The lattice expansion coefficient vs T.

        alpha = 1 / V(T) d/dT V(T)
        """
        if self._alpha is not None:
            return self._alpha
        if self.ibrav == 2:
            if self.lattice is None:
                raise ValueError("Need to set 'lattice'.")
            if not is_vector(self.lattice):
                raise TypeError("'lattice' is not an array.")
            self._alpha = 3 * np.divide(
                np.gradient(self.lattice, self.temperatures),
                self.lattice)
            if not is_vector(self.temperatures, length=len(self.lattice)):
                raise TypeError(
                        "'temperatures' is not an array with same length as "
                        "lattice.")
        elif self.ibrav == 4:
            # use equilibrium volumes directly
            self._alpha = np.divide(
                    np.gradient(self.equilibrium_volumes, self.temperatures),
                    self.equilibrium_volumes,
                    )
        else:
            raise NotImplementedError(self.ibrav)
        return self.alpha

    @property
    def cv_electrons(self) -> Sequence[float]:
        """Compute electronic contribution to heat capacity at cst volume.

        C_V = TdS/dT at equilibrium lattice volume. Makes use of linear
        interpolation to get dS/dT at equilibrium volume.
        """
        if self._cv_electrons is not None:
            return self._cv_electrons
        if self.s_electrons is None:
            raise ValueError("Need to set 's_electrons'.")
        # s electrons should be an array of shape (n_vols, n_temps)
        if not is_2d_arr(
                self.s_electrons,
                size=(len(self.volumes), len(self.temperatures))):
            raise TypeError("'s_electrons' should be (n_vols, n_temps)'.")
        # here s_electrons is the entropic energy (-TS) in eV
        dsdt = np.gradient(
                -np.divide(self.s_electrons, self.temperatures[None, :]),
                self.temperatures, axis=1)
        cv = np.multiply(self.temperatures[None, :], dsdt)
        # interpolate values to get at right volume
        self._cv_electrons = (
                self._interpolate_cv(cv) * EV_TO_JOULES / k_B)
        return self.cv_electrons

    @property
    def cv_phonons(self) -> Sequence[float]:
        """The heat capacity at cst volume for phonons.

        Interpolates the heat capacity at the V0(T) volumes.
        """
        if self._cv_phonons is not None:
            return self._cv_phonons
        if self.phonon_frequencies is None:
            raise ValueError("Need to set 'phonon_frequencies'.")
        k = k_B * JOULES_TO_EV
        kt = k * self.temperatures
        arg = self.phonon_frequencies / (2 * kt[:, None, None, None])
        sinh = np.sinh(arg)
        # ignore freqs at 0
        where0 = np.where(sinh == 0)
        sinh[where0] = 1  # set to 1, later these will be set to 0 in csech
        csch2 = np.square(1 / sinh)  # ntemps x nvols x nqs x nbranches
        csch2[where0] = 0  # do not contribute to sum
        tosum = np.multiply(np.square(arg), csch2)
        if self.ibrav == 2:
            weights = self.weights[None, None, :, None]
        else:
            # need to adjust multiplication
            weights = self.weights_per_volumes[None, :, :, None]
        cv = (tosum * weights).sum(
                axis=-1).sum(axis=-1).T
        # cv is now nvols x ntemps
        self._cv_phonons = self._interpolate_cv(cv)
        return self.cv_phonons

    def get_alpha_plot(self) -> Plot:
        """Create alpha(T) vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$\alpha_V(T)$", yunits=r"K$^{-1}$",
                )
        plot.add_curve(
                self.temperatures, self.alpha, linewidth=2,
                linestyle="-", marker="o", color="C0",
                markerfacecolor="C0", markersize=5,
                )
        return plot

    def get_electrons_heat_capacity_plot(self) -> Plot:
        """Create C_V^el vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_V^{el}$", yunits="$k_B$",
                )
        plot.add_curve(
                self.temperatures, self.cv_electrons, linewidth=2,
                color="C0", marker="o", markerfacecolor="C0", markersize=5,
                )
        return plot

    def get_phonon_heat_capacity_plot(self) -> Plot:
        """Create C_V^ph vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_V^{ph}$", yunits="$k_B$",
                )
        plot.add_curve(
                self.temperatures, self.cv_phonons,
                color="C0", marker="o", markerfacecolor="C0", markersize=5,
                linewidth=2,
                )
        return plot

    def _interpolate_cv(self, cv) -> Sequence[float]:
        """Interpolates the heat capacity at the equilibrium volume."""
        final_cvs = []
        for it, v0 in enumerate(self.equilibrium_volumes):
            diffs = np.abs(self.volumes - v0)
            low_to_high_order = np.argsort(diffs)
            idx1 = low_to_high_order[0]  # closest
            idx2 = low_to_high_order[1]
            if idx1 > idx2:
                # just swap
                idx1, idx2 = idx2, idx1
            slope = (cv[idx2, it] - cv[idx1, it]) / (
                    self.volumes[idx2] - self.volumes[idx1])
            offset = cv[idx2, it] - slope * self.volumes[idx2]
            final_cvs.append(slope * v0 + offset)
        return np.asarray(final_cvs)


class HeatCapacityConstantPressure(
        BasePostProcClass,
        TemperaturesDependentQuantity,
        ):
    """Post processing class for the heat capacity at constant pressure."""

    _loggername = "HeatCapacityConstantPressure"

    def __init__(
            self, *args,
            equilibrium_volumes: Sequence[float] = None,
            bulk_modulus: Sequence[float] = None,
            alpha: Sequence[float] = None,
            cv_phonons: Sequence[float] = None,
            cv_electrons: Sequence[float] = None,
            **kwargs,
            ):
        TemperaturesDependentQuantity.__init__(self, *args, **kwargs)
        self.alpha = alpha
        self.bulk_modulus = bulk_modulus
        self.equilibrium_volumes = equilibrium_volumes
        self._cp_offset = None
        self.cv_phonons = cv_phonons
        self.cv_electrons = cv_electrons

    @classmethod
    def from_heat_capacity_constant_volume(
            cls, cv, *args, electrons: bool = False, **kwargs):
        """Initialize a HeatCapacityConstantPressure instance from cst vol."""
        cv_electrons = None
        if electrons:
            cv_electrons = cv.cv_electrons
        return cls(
                *args,
                alpha=cv.alpha,
                equilibrium_volumes=cv.equilibrium_volumes,
                cv_phonons=cv.cv_phonons,
                cv_electrons=cv_electrons,
                temperatures=cv.temperatures,
                **kwargs,
                )

    @property
    def cp_offset(self) -> Sequence[float]:
        """The heat capacity at cst P offset.

        For cubic systems it reads:
        alpha^2 x B_0 x V x T.
        """
        if self._cp_offset is not None:
            return self._cp_offset
        if self.bulk_modulus is None:
            raise ValueError("Need to set 'bulk_modulus'.")
        self._cp_offset = np.multiply(
                np.multiply(
                    np.square(self.alpha),
                    self.bulk_modulus),
                np.multiply(
                    self.equilibrium_volumes, self.temperatures),
                ) * EV_TO_JOULES / k_B
        # bulk modulus here is in EV/bohr3 and volumes are in bohr3
        # divide by k_B to get unitless data [units = k_B]
        return self._cp_offset

    @property
    def cp(self) -> Sequence[float]:
        """The total heat capacity at constant pressure."""
        return self.cv_phonons + self.cv_electrons + self.cp_offset

    def get_cp_offset_plot(self) -> Plot:
        """Create the C_P - C_V plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_P-C_V$", yunits=r"$k_B$",
                )
        plot.add_curve(
                self.temperatures, self.cp_offset, linewidth=2,
                linestyle="-", marker="o", color="C0", markerfacecolor="C0",
                markersize=5,
                )
        return plot

    def get_electrons_heat_capacity_pressure_plot(self) -> Plot:
        """Create C_P^el vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_P^{el}$", yunits="$k_B$",
                )
        plot.add_curve(
                self.temperatures, self.cv_electrons + self.cp_offset,
                color="C0", marker="o", markerfacecolor="C0", markersize=5,
                linewidth=2,
                )
        return plot

    def get_phonon_heat_capacity_pressure_plot(self) -> Plot:
        """Create C_P^ph vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_P^{ph}$", yunits="$k_B$",
                )
        plot.add_curve(
                self.temperatures, self.cv_phonons + self.cp_offset,
                color="C0", marker="o", markerfacecolor="C0", markersize=5,
                linewidth=2,
                )
        return plot

    def get_total_heat_capacity_plot(self) -> Plot:
        """Create total C_P vs T plot."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=r"$C_P^{tot}$", yunits="$k_B$",
                )
        plot.add_curve(
                self.temperatures,
                self.cp,
                color="C0", marker="o", markerfacecolor="C0", markersize=5,
                linewidth=2,
                )
        return plot


class AdiabaticBulkModulus(
        BasePostProcClass, TemperaturesDependentQuantity):
    """Utility that can compute the adiabatic bulk modulus.

    It is computed from the isothermal bulk modulus using:

    B(T) = B_iso(T) + V(alpha^2 V T B_iso^2 / C_V)
    """

    _loggername = "AdiabaticBulkModulus"

    def __init__(
            self, *args, isothermal_bulk_modulus: Sequence[float] = None,
            alpha: Sequence[float] = None,
            cv: Sequence[float] = None,
            equilibrium_volumes: Sequence[float],
            **kwargs,
            ):
        TemperaturesDependentQuantity.__init__(self, *args, **kwargs)
        self.isothermal_bulk_modulus = isothermal_bulk_modulus
        self.alpha = alpha
        self.cv = cv
        self.equilibrium_volumes = equilibrium_volumes
        self.alpha = alpha
        self._adiabatic_bulk_modulus = None
        self._adiabatic_factor = None

    @property
    def adiabatic_factor(self) -> Sequence[float]:
        """The adiabatic factor to go from isothermal to adiabatic B."""
        if self._adiabatic_factor is not None:
            return self._adiabatic_factor
        a2 = np.square(self.alpha)  # [K^-2]  # here alpha is for volume
        vt = np.multiply(
                self.equilibrium_volumes, self.temperatures)
        a2bvt = np.multiply(np.multiply(a2, vt), self.isothermal_bulk_modulus)
        factor = 1 + np.divide(a2bvt, self.cv)  # [unitless]
        self._adiabatic_factor = factor
        return factor

    @property
    def adiabatic_bulk_modulus(self) -> Sequence[float]:
        """Computes the adiabatic bulk modulus."""
        if self._adiabatic_bulk_modulus is not None:
            return self._adiabatic_bulk_modulus
        self._adiabatic_bulk_modulus = np.multiply(
                self.isothermal_bulk_modulus, self.adiabatic_factor)
        return self._adiabatic_bulk_modulus

    @classmethod
    def from_heat_capacity_constant_volume(
            cls, cv_obj, electrons: bool = True, *args, **kwargs):
        """Initialize AdiabaticBulkModulus from C_V."""
        # cv is in units of k_B
        cv = cv_obj.cv_phonons.copy()
        if electrons:
            cv += cv_obj.cv_electrons.copy()
        cv *= k_B * JOULES_TO_EV
        return cls(
                *args,
                cv=cv,
                alpha=cv_obj.alpha,
                equilibrium_volumes=cv_obj.equilibrium_volumes,
                temperatures=cv_obj.temperatures,
                **kwargs,
                )

    def get_adiabatic_bulk_modulus_plot(
            self, label: str = None, **kwargs) -> Plot:
        """Plot the adiabatic bulk modulus."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=kwargs.get("ylabel", r"$B_0^{ad}(T)$"),
                yunits=kwargs.get("yunits", r"eV/bohr$^3$"),
                )
        plot.add_curve(
                self.temperatures, self.adiabatic_bulk_modulus,
                color="C0", linewidth=2, linestyle="-", marker="o",
                markerfacecolor="C0", markersize=5, label=label,
                )
        return plot

    def get_adiabatic_factor_plot(self, label: str = None) -> Plot:
        """Plot the adiabatic factor."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel="factor", yunits="unitless",
                )
        plot.add_curve(
                self.temperatures, self.adiabatic_factor,
                color="C0", linewidth=2, linestyle="-", marker="o",
                markerfacecolor="C0", markersize=5, label=label,
                )
        return plot


class AdiabaticBulkModulusPressureDerivative(
        BasePostProcClass, TemperaturesDependentQuantity):
    """Utility that compute the adiabatic bulk modulus press. derivative."""

    _loggername = "AdiabaticBulkModulusPressureDerivative"

    def __init__(
            self, *args,
            isothermal_bulk_modulus: Sequence[float] = None,
            isothermal_bulk_modulus_pressure_derivative: Sequence[float] = (
                None),
            adiabatic_bulk_modulus: Sequence[float] = None,
            alpha: Sequence[float] = None,
            cp: Sequence[float] = None,
            **kwargs,
            ):
        TemperaturesDependentQuantity.__init__(self, *args, **kwargs)
        self.isothermal_bulk_modulus = isothermal_bulk_modulus
        self.isothermal_bulk_modulus_pressure_derivative = (
                isothermal_bulk_modulus_pressure_derivative)
        self.adiabatic_bulk_modulus = adiabatic_bulk_modulus
        self.alpha = alpha
        self.cp = cp
        self.alpha = alpha
        self._adiabatic_bulk_modulus_pressure_derivative = None
        self._adiabatic_offset = None
        # see Thermodynamic of crystals by Duane Wallace p. 11 eq 1.61
        # first term is equal to (B_S/B_T)^2 B_T'
        self.adiabatic_scaling = np.square(
                np.divide(self.adiabatic_bulk_modulus,
                          self.isothermal_bulk_modulus))

    @property
    def adiabatic_offset(self) -> Sequence[float]:
        """The adiabatic offset to go from isothermal to adiabatic."""
        if self._adiabatic_offset is not None:
            return self._adiabatic_offset
        # second term
        # need pressure derivative of delta k
        # eq. 1.55 p. 10 of book above (beta = alpha in the book)
        k_t = np.divide(1, self.isothermal_bulk_modulus)
        k_s = np.divide(1, self.adiabatic_bulk_modulus)
        delta_k = k_t - k_s
        ddk1 = -k_t
        k_t_grad = np.gradient(k_t, self.temperatures)
        ddk2 = np.multiply(np.divide(-2, self.alpha), k_t_grad)
        dalpha_dt = np.gradient(self.alpha, self.temperatures)
        ddk3 = np.multiply(
                delta_k,
                1 + np.divide(dalpha_dt, np.square(self.alpha)))
        ddelta_k_dp = np.multiply(delta_k, ddk1 + ddk2 + ddk3)
        self._adiabatic_offset = np.multiply(
                np.square(self.adiabatic_bulk_modulus),
                ddelta_k_dp)
        return self._adiabatic_offset

    @property
    def adiabatic_bulk_modulus_pressure_derivative(self) -> Sequence[float]:
        """Computes the adiabatic bulk modulus."""
        if self._adiabatic_bulk_modulus_pressure_derivative is not None:
            return self._adiabatic_bulk_modulus_pressure_derivative
        self._adiabatic_bulk_modulus_pressure_derivative = (
                np.multiply(
                    self.isothermal_bulk_modulus_pressure_derivative,
                    self.adiabatic_scaling) +
                self.adiabatic_offset)
        return self._adiabatic_bulk_modulus_pressure_derivative

    def get_adiabatic_bulk_modulus_pressure_derivative_plot(
            self, label: str = None, **kwargs) -> Plot:
        """Plot the adiabatic bulk modulus pressure derivative."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel=kwargs.get("ylabel", r"$B_0^{ad}(T)$"),
                yunits=kwargs.get("yunits", r"eV/bohr$^3$"),
                )
        plot.add_curve(
                self.temperatures,
                self.adiabatic_bulk_modulus_pressure_derivative,
                color="C0", linewidth=2, linestyle="-", marker="o",
                markerfacecolor="C0", markersize=5, label=label,
                )
        return plot

    def get_adiabatic_scaling_plot(self, label: str = None) -> Plot:
        """Plot the adiabatic scaling."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel="$(B_0/B_T)^2$", yunits="unitless",
                )
        plot.add_curve(
                self.temperatures, self.adiabatic_scaling,
                color="C0", linewidth=2, linestyle="-", marker="o",
                markerfacecolor="C0", markersize=5, label=label,
                )
        return plot

    def get_adiabatic_offset_plot(self, label: str = None) -> Plot:
        """Plot the adiabatic offset."""
        plot = self.create_plot(
                xlabel="Temperature", xunits="K",
                ylabel="factor", yunits="unitless",
                )
        plot.add_curve(
                self.temperatures, self.adiabatic_offset,
                color="C0", linewidth=2, linestyle="-", marker="o",
                markerfacecolor="C0", markersize=5, label=label,
                )
        return plot
