import logging
import os
import traceback as tb
from typing import Sequence

import numpy as np

from .bases import BasePostProcClassWithFermi, Dispersion
from ..constants import HARTREE_TO_EV
from ..handlers import (
        AbinitEIGFile, AbinitGSRFile, AbinitLogFile, AbinitOutputFile,
        CalculationDirectory, MetaDataFile,
        QEEPWBandEigFile,
        QEPWLogFile,
        Wannier90BandDatFile, Wannier90BandkptFile, Wannier90InputFile,
        )


# TODO: should multi class this with a plot class
class BandStructure(BasePostProcClassWithFermi, Dispersion):
    """Class that represents a Band Structure.

    Basically, this is a Dispersion object with a fermi level associated.
    """

    _loggername = "BandStructure"

    def __init__(self, *args, **kwargs):
        BasePostProcClassWithFermi.__init__(self, *args, **kwargs)
        Dispersion.__init__(self, *args, **kwargs)
        self._fermiband = None
        self._software = None
        self._bandgap = None

    @property
    def bandgap(self):
        """Return the band gap."""
        if self._bandgap is not None:
            return self._bandgap
        raise ValueError("Plot data to compute band gap.")

    @property
    def fermi_band(self):
        """Return the fermi band index."""
        if self._fermiband is not None:
            return self._fermiband
        # if fermi energy is set and eigenvalues, we can compute the fermi band
        if self._energies is None or self._fermienergy is None:
            raise ValueError("Set fermi_band before using it.")
        for iband, band in enumerate(self.bands):
            # if fermi energy is within the band, this is it
            if self.fermi_energy <= max(band) and self.fermi_energy >= min(
                    band):
                self._fermiband = iband
                return self.fermi_band
        self._logger.error(
                f"Could not determine fermi band with '{self.fermi_energy}'.")
        raise RuntimeError("Could not compute fermi band.")

    @fermi_band.setter
    def fermi_band(self, fermi_band):
        if not isinstance(fermi_band, int):
            raise TypeError(f"Expected an integer but got: {fermi_band}")
        if self._fermienergy is not None:
            self._logger.warning("Resetting Fermi energy. Overwritting "
                                 "existing value using the fermi band index.")
        self._fermiband = fermi_band
        self.fermi_energy = max(self.bands[self.fermi_band])

    def compute_bandgap(self):
        """Computes the band gap."""
        try:
            return min(self.bands[self.fermi_band + 1]) - self.fermi_energy
        except Exception:
            raise RuntimeError

    def create_plot(
            self, *args,
            adjust_axis=True,
            color="k",
            show_bandgap=False,
            fermi_at_zero=True,
            **kwargs):
        """Plot the bandstructure.

        Parameters
        ----------
        show_bandgap : bool, optional
            If True, the band gap value will be shown in a label and dots
            will appear on the band structure where the min of the conduction
            band is located and where the max of the valence band is.
            Not compatible with 'adjust_axis'.
        fermi_at_zero: bool, optional
            If True (default), the fermi level is placed at the 0 energy.
            Otherwise, a line is just drawn at the fermi level.

        Other Parameters
        ----------------
        kwargs:
            All other parameters are passed to
            the :meth:`.Dispersion.create_plot`
            method and/or to the curves creation through the
            the :meth:`.Plot.add_curve` method.

        Returns
        -------
        Plot: The Plot object representing the Band Structure.
        """
        if show_bandgap and adjust_axis:
            raise NotImplementedError(
                    "Showing band gap not compatible yet with adjust axis.")
        plot = Dispersion.create_plot(
                self, *args, adjust_axis=adjust_axis,
                color=color,
                **kwargs,
                )
        if self._fermienergy is None:
            self._logger.warning("Fermi energy not set, assuming it is 0...")
            self.fermi_energy = 0
        plot.title += r"$\epsilon_F=$" + f"{self.fermi_energy} eV"
        # compute band gap
        try:
            self._bandgap = self.compute_bandgap()
            gap = self._bandgap
        except RuntimeError:
            self._logger.error("Could not compute bandgap...")
            self._bandgap = 0
            gap = 0
        plot.title += r" $E_{gap}=$" + f"{gap}"
        if fermi_at_zero:
            for curve in plot.plot_objects["curves"]:
                curve.ydata -= self.fermi_energy
        if not fermi_at_zero and self.fermi_energy != 0.0:
            # draw fermi energy line
            plot.add_hline(self.fermi_energy, linestyle="--")
        # add gap label if needed
        if show_bandgap:
            if fermi_at_zero:
                gap -= self.fermi_energy
            # add dots where the min of conduction band and max of valence band
            # are located.
            min_ = np.min(self.bands[self.fermi_band + 1])
            where_min_ = np.where(
                    self.bands[self.fermi_band + 1] == min_)[0] / self.nkpts
            mins_ = [min_] * len(where_min_)
            max_ = self.fermi_energy
            where_max_ = np.where(
                    self.bands[self.fermi_band] == max_)[0] / self.nkpts
            maxs_ = [max_] * len(where_max_)
            if fermi_at_zero:
                mins_ = (np.array(mins_) - self.fermi_energy).tolist()
                maxs_ = np.array([0] * len(maxs_)).tolist()
            plot.add_scatter(
                    list(where_min_) + list(where_max_), mins_ + maxs_,
                    color=color, s=50)
            plot.plot_objects["curves"][0].label = (
                    r"$E_{gap}=$" + str(round(self._bandgap, 3)) + " eV")
        return plot

    @classmethod
    async def from_abinit_calculation(cls, path, **kwargs):
        """Create band structure object from an abinit calculation.

        Sets the kpts and the energies but still need to set fermi_energy
        or fermi_band manually after that.

        Parameters
        ----------
        path : The abinit calculation path.
        """
        bandstructure = cls(**kwargs)
        async with await AbinitEIGFile.from_calculation(
                path, loglevel=bandstructure._logger.level) as eig:
            bandstructure.kpts = eig.k_points
            bandstructure.energies = np.array(eig.eigenvalues) * HARTREE_TO_EV
        await bandstructure.set_fermi_level_from_abinit_calculation(path)
        return bandstructure

    async def set_fermi_band_from_abinit_calculation(self, calc: str) -> None:
        """Sets fermi band from occupations read from a calculation.

        Parameters
        ----------
        calc: str
            The calculation to read occupations from.
        """
        async with await AbinitLogFile.from_calculation(
                calc, loglevel=self._loglevel) as log:
            self._logger.info(
                    "Trying to get occupations from log file to set fermi "
                    "band automatically.")
            try:
                self.set_fermi_band_from_occupations(log.occ)
                return
            except (TypeError, ValueError):
                pass
        # if cannot get occupations from log file, check GSR file
        self._logger.info(
                "Could not extract occupations from log file. "
                "Trying to get occupations from GSR file."
                )
        async with await AbinitGSRFile.from_calculation(
                calc, loglevel=self._loglevel) as gsr:
            # 1st 0 is for the first spin (assume same occ for all spins)
            # 2nd 0 is for the kpt index. assume same occ for all kpts
            self.set_fermi_band_from_occupations(gsr.occupations[0][0])

    def set_fermi_band_from_occupations(
            self, occupations: Sequence[float]) -> None:
        """Sets fermi band from occupations array.

        Parameters
        ----------
        occupations: list-like
            The sequence of occupations for each band at a given kpt
            for given spin. Assuming each spin and kpt has same occs.

        Raises
        ------
        ValueError: if cannot get fermi band from given occupations.
        """
        for band, occ in enumerate(occupations):
            # first occ to be 0 is the first conduction band.
            if not band:
                continue
            if not occ and occupations[band - 1] in (1.0, 2.0):
                self._logger.info(
                        "Automatically found fermi band to be: "
                        f"{band - 1}.")
                self.fermi_band = band - 1
                return
        raise ValueError(
                "Couldn't detect fermi band from occupations: '"
                f"{occupations}'.")

    async def set_fermi_level_from_abinit_calculation(self, calc: str) -> None:
        """Sets fermi level from a calculation.

        Parameters
        ----------
        calc: str
            Path towards the calculation to read fermi level from.
        """
        # check occupations first. If they are all perfect integers,
        # we can determine fermi band and therefore fermi level.
        try:
            await self.set_fermi_band_from_abinit_calculation(calc)
            return
        except ValueError:
            # failed
            pass
        # check the fermi level in the output file
        try:
            async with await AbinitOutputFile.from_calculation(
                    calc, loglevel=self._loglevel) as output:
                try:
                    fermi_energy = (
                            output.dtsets[0]["fermi_energy"] * HARTREE_TO_EV
                            )
                    return
                except IndexError as e:
                    self._logger.error(
                            f"Couldn't parse any dataset:\n{output}")
                    self._logger.exception(e)
                    raise e
                self._logger.info(
                    "Automatically got fermi energy from calculation: "
                    f"{calc} we found "
                    f"Ef={fermi_energy} eV")
        except FileNotFoundError:
            pass
        if fermi_energy is None:
            # try getting from log file
            try:
                async with await AbinitLogFile.from_calculation(
                        calc, loglevel=self._loglevel) as log:
                    fermi_energy = (
                            log.dtsets[0]["fermi_energy"] * HARTREE_TO_EV)
            except FileNotFoundError:
                pass
        if fermi_energy is None:
            # try getting fermi energy from GSR file instead
            async with await AbinitGSRFile.from_calculation(
                    calc, loglevel=self._loglevel) as gsr:
                if gsr.fermi_energy is not None:
                    fermi_energy = (
                            gsr.fermi_energy * HARTREE_TO_EV)
        if fermi_energy is not None:
            self.fermi_energy = fermi_energy

    @classmethod
    async def from_abinit_eig_file(cls, path, *args, **kwargs):
        """Initialize a BandStructure object from an abinit EIG file."""
        bandstructure = cls(*args, **kwargs)
        async with await AbinitEIGFile.from_file(
                path, loglevel=bandstructure._loglevel) as eig:
            bandstructure.kpts = eig.k_points
            bandstructure.energies = np.array(eig.eigenvalues)
            if eig.units != "eV":
                bandstructure.energies *= HARTREE_TO_EV
        return bandstructure

    @classmethod
    async def from_qe_pw_calculation(cls, path, *args, **kwargs):
        """Create a band structure object from a QE calculation.

        Parameters
        ----------
        path: str
            The path to the QEPW calculation.
        """
        loglevel = kwargs.get("loglevel", logging.INFO)
        # get log file
        async with await CalculationDirectory.from_calculation(path) as calc:
            if await calc.calctype != "qe_pw":
                raise TypeError("Not a qe_pw calculation.")
            meta = calc.meta_data_file
            async with await QEPWLogFile.from_meta_data_file(
                    meta, loglevel=loglevel) as log:
                bs = await cls.from_qelog(log, *args, **kwargs)
                # try to get fermi level from parent calculation
            parents = meta.parents
            if len(parents) != 1:
                bs._logger.warning("Could not get fermi level"
                                   " from parent "
                                   "calculation.")
                return bs
            try:
                async with await QEPWLogFile.from_calculation(
                        parents[0], loglevel=loglevel) as log:
                    await log.read()
                    bs.fermi_energy = log.fermi_energy
                    bs._logger.debug(f"Setting fermi level from parent "
                                     f"calculation: {log.fermi_energy}")
            except Exception as e:
                bs._logger.error("An error occured while trying to get"
                                 " fermi energy from parent "
                                 "calculation: "
                                 f"{tb.print_tb(e.__traceback__)}:"
                                 f" {e}")
            return bs

    @classmethod
    async def from_qelog(cls, path, *args, **kwargs):
        """Class method to read kpts/eigenvalues directly from a QE log file.

        Sets the kpts/qpts and the energies but still need to set fermi_energy
        or fermi_band manually if necessary after that.

        Parameters
        ----------
        path : The log file path or QEPWLogFile object.
        """
        bandstructure = cls(*args, **kwargs)
        if isinstance(path, str):
            log = await QEPWLogFile.from_file(
                    path, loglevel=bandstructure._loglevel)
        else:
            log = path
        if not isinstance(log, QEPWLogFile):
            raise TypeError("Was expecting either Log file object or path to "
                            f"log file but got: {log}")
        await log.read()
        bandstructure.kpts = np.array(log.eigenvalues["k_points"])
        bandstructure.energies = np.array(log.eigenvalues["eigenvalues"])
        return bandstructure

    # TODO: rebase this class method with the 'from_wannier90' one.
    @classmethod
    async def from_epw(cls, path, *args, **kwargs):
        """Get bandstructure from a wannier90 calculation."""
        bs = cls(*args, **kwargs)
        calc = await CalculationDirectory.from_calculation(
                path, loglevel=bs._loglevel)
        # check if bands_plot=True flag has been set in input file
        async with calc.meta_data_file as meta:
            wannier_input = os.path.join(meta.rundir, meta.jobname + ".win")
            async with await Wannier90InputFile.from_file(
                    wannier_input) as input_file:
                if not await input_file.exists:
                    raise FileNotFoundError(input_file.path)
                bands_plot = input_file.input_variables.get(
                        "bands_plot", False)
                if not bands_plot:
                    raise ValueError(
                            "EPW must be run with wdata(X)='bands_plot=True'.")
        # check if 'filkf' has been defined. If yes, use the kpts from this
        async with calc.input_file as inf:
            filkf = inf.input_variables.get("filkf", None).value
        if filkf is not None:
            # get kpts from the band.eig file
            # if not os.path.isabs(filkf):
            #     # filkf is relative to rundir.
            #     filkf = full_abspath(os.path.join(
            #         calc.run_directory.path, filkf))
            # # load kpts (skip first line which is irrelevent)
            # bs.kpts = np.loadtxt(filkf, skiprows=1, usecols=(0, 1, 2))
            # read eigs from the bands.eig file
            bs = cls.from_qeepw_bandeig(path, *args, **kwargs)
        else:
            # read kpts and eigs from the W90 files
            async with await Wannier90BandDatFile.from_calculation(
                    calc, loglevel=bs._loglevel) as banddat:
                bs.energies = np.array(banddat.bands).T
            async with await Wannier90BandkptFile.from_calculation(
                    calc, loglevel=bs._loglevel) as bandkpt:
                bs.kpts = bandkpt.kpts
        # get fermi level
        async with calc.meta_data_file as meta:
            # first get the nscf preparation run calculation
            for parent in meta.parents:
                async with await MetaDataFile.from_calculation(
                        parent) as meta_parent:
                    if meta_parent.calctype == "qe_pw":
                        # found it
                        parent_calc = parent
                        break
            else:
                raise FileNotFoundError("Could not find nscf parent "
                                        "calculation.")
        async with await QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        bs._logger.debug("Could not find fermi energy... trying parent calc")
        async with await MetaDataFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as meta:
            parent_calc = meta.parents[0]
        async with await QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        raise ValueError("Could not find fermi energy.")

    @classmethod
    def from_qeepw_bandeig(cls, filepath, *args, **kwargs):
        """Create a BandStructure object from an EPW band.eig file.

        Parameters
        ----------
        filepath : str
            Path to the band.eig file or calculation.

        Returns
        -------
        BandStructure object with kpts and eigenvalues read from the file.
        """
        bs = cls(*args, **kwargs)
        if CalculationDirectory.is_calculation_directory(filepath):
            classmeth = QEEPWBandEigFile.from_calculation
        else:
            classmeth = QEEPWBandEigFile.from_file
        with classmeth(
                filepath, loglevel=bs._loglevel) as eigs:
            bs.kpts = eigs.coordinates
            bs.energies = eigs.eigenvalues
        return bs

    @classmethod
    async def from_wannier90(cls, path, *args, **kwargs):
        """Get bandstructure from a wannier90 calculation."""
        bs = cls(*args, **kwargs)
        calc = await CalculationDirectory.from_calculation(
                path, loglevel=bs._loglevel)
        # check if bands_plot=True flag has been set in input file
        async with calc.input_file as input_file:
            bands_plot = input_file.input_variables.get("bands_plot", False)
            if not bands_plot:
                raise ValueError(
                        "Wannier90 must be run with 'bands_plot'=True.")
        # get bands and kpts
        async with await Wannier90BandDatFile.from_calculation(
                calc, loglevel=bs._loglevel) as banddat:
            bs.energies = np.array(banddat.bands).T
        async with await Wannier90BandkptFile.from_calculation(
                calc, loglevel=bs._loglevel) as bandkpt:
            bs.kpts = bandkpt.kpts
        # get fermi level
        async with calc.meta_data_file as meta:
            # first get the nscf preparation run calculation
            for parent in meta.parents:
                async with await MetaDataFile.from_calculation(
                        parent) as meta_parent:
                    if meta_parent.calctype == "qe_pw":
                        # found it
                        parent_calc = parent
                        break
                    if meta_parent.calctype == "qe_pw2wannier90":
                        # should find parent in this calc
                        async with await MetaDataFile.from_calculation(
                                meta_parent.calc_workdir) as meta_parent_p2w:
                            for parent_p2w in meta_parent_p2w.parents:
                                async with await MetaDataFile.from_calculation(
                                        parent_p2w,
                                        loglevel=bs._loglevel) as meta_parent2:
                                    if meta_parent2.calctype == "qe_pw":
                                        # found it
                                        parent_calc = parent_p2w
                                        break
                            else:
                                raise ValueError(
                                        "Could not find nscf parent.")
                        break
            else:
                raise FileNotFoundError("Could not find nscf parent "
                                        "calculation.")
        async with await QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        bs._logger.debug("Could not find fermi energy... trying parent calc")
        async with await MetaDataFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as meta:
            parent_calc = meta.parents[0]
        async with await QEPWLogFile.from_calculation(
                parent_calc, loglevel=bs._loglevel) as log:
            if log.fermi_energy is not None:
                bs.fermi_energy = log.fermi_energy
                return bs
        raise ValueError("Could not find fermi energy.")

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Returns a Bandstructure object from a calculation directory.

        This method reads the metadata file and deduces which output file
        to read.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        async with await MetaDataFile.from_calculation(
                path, loglevel=lvl) as meta:
            calctype = meta.calctype
            if calctype == "qe_pw":
                return await cls.from_qe_pw_calculation(path, *args, **kwargs)
            elif calctype == "abinit":
                return await cls.from_abinit_calculation(path, *args, **kwargs)
            elif calctype == "wannier90":
                return await cls.from_wannier90(path, *args, **kwargs)
            elif calctype == "qe_epw":
                return await cls.from_epw(path, *args, **kwargs)
            else:
                raise NotImplementedError(calctype)
