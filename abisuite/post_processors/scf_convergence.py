from typing import Sequence

from .bases import BasePostProcClass
from ..handlers import CalculationDirectory
from ..plotters import MultiPlot


class SCFConvergence(BasePostProcClass):
    """Scf convergence post processor. Creates convergence plots."""

    _loggername = "SCFConvergence"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._scf_convergence_data = None

    @property
    def scf_convergence_data(self) -> Sequence[dict]:
        """Scf convergence data."""
        if self._scf_convergence_data is not None:
            return self._scf_convergence_data
        raise ValueError("Need to set 'scf_convergence_data'.")

    @scf_convergence_data.setter
    def scf_convergence_data(self, scf: Sequence[dict]) -> None:
        self._scf_convergence_data = scf

    def _gather_data(self) -> dict:
        """Simply inverts the sequence of dicts into a dict of sequences."""
        curve_infos = {
                "etot": {
                    "label": "Etot [Ha]", "semilogy": False},
                "deltae(h)": {
                    "label": "deltaE(h) [Ha]", "semilogy": False},
                "residm": {
                    "label": "residm", "semilogy": True,
                    },
                "vres2": {
                    "label": "vres2",
                    "semilogy": True,
                    },
                }
        data = {}
        for scf_dat in self.scf_convergence_data:
            for key, info in curve_infos.items():
                label = info["label"]
                if key in scf_dat:
                    data.setdefault(label, {"data": []})
                    data[label].update(info)
                    data[label]["data"].append(scf_dat[key])
        return data

    def create_plot(self) -> MultiPlot:
        """Creates scf convergence multi plot."""
        multiplot = MultiPlot(loglevel=self._loglevel)
        for iplot, (label, data) in enumerate(self._gather_data().items()):
            plot = super().create_plot(
                    xlabel="Iteration Step",
                    ylabel=label,
                    )
            plot.add_curve(
                    list(range(len(data["data"]))), data["data"],
                    linewidth=2, marker="o",
                    markersize=5, markerfacecolor="C0", color="C0",
                    semilogy=data["semilogy"]
                    )
            plot.grid = True
            multiplot.add_plot(plot, row=iplot // 2)  # 2 plots per row
        multiplot.title = "SCF Convergence Data"
        return multiplot

    @classmethod
    async def from_calculation(cls, path: str, *args, **kwargs):
        """Initialize a SCFConvergence instance from a calculation path.

        Parameters
        ----------
        path: str
            The path of the calculation.

        Returns
        -------
        SCFConvergence instance.
        """
        async with await CalculationDirectory.from_calculation(
                path, loglevel=kwargs.get("loglevel", None)) as calc:
            calctype = await calc.calctype
            if calctype != "abinit":
                raise NotImplementedError(calctype)
            log = await calc.log_file
            inst = cls(*args, **kwargs)
            async with log:
                inst.scf_convergence_data = log.dtsets[0]["etot_convergence"]
        return inst
