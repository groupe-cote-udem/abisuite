import abc
from typing import Sequence

import numpy as np

from ..bases import BaseUtility
from ..exceptions import DevError
from ..plotters import Plot
from ..routines import (
        is_2d_arr, is_list_like,
        is_scalar, is_scalar_or_str, is_vector,
        )


class BasePostProcClass(BaseUtility, abc.ABC):
    """Base class for post-process utilities that usually plots something."""

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._plot = None

    def create_plot(self, *args, **kwargs):
        """Create a basic plot with titles and labels as given.

        Parameters
        ----------
        aspect: str, optional
            Aspect ratio for the plot.
        grid: bool, optional
            If True, a grid will be added to the plot.
        title : str, optional
            Title for the plot.
        xlabel : str, optional
            The xlabel for the plot.
        xlims: list-like, optional
            Specifies the x axis limits (min, max).
        xunits : str, optional
            If not None, the units for the xaxis.
        ylabel : str, optional
            The ylabel for the plot.
        ylims: list-like, optional
            Specifies the y axis limits (min, max).
        yunits : str, optional
            If not None, the units for the yaxis.
        zlabel : str, optional
            The ylabel for the plot.
        zlims: list-like, optional
            Specifies the z axis limits (min, max).
        zunits : str, optional
            If not None, the units for the yaxis.

        Returns
        -------
        Plot: Plot object with labels and titles.
        """
        plot = Plot(loglevel=self._logger.level)
        plot.xlabel = kwargs.pop("xlabel", "")
        plot.ylabel = kwargs.pop("ylabel", "")
        if "zlabel" in kwargs:
            plot.zlabel = kwargs.pop("zlabel", "")
        for dir_ in ("x", "y", "z"):
            if f"{dir_}units" in kwargs:
                if kwargs[f"{dir_}units"] is not None:
                    setattr(
                        plot, f"{dir_}label",
                        (getattr(plot, f"{dir_}label") +
                         " [" + kwargs.pop(f"{dir_}units") + "]"))
            if f"{dir_}lims" in kwargs:
                if kwargs[f"{dir_}lims"] is not None:
                    setattr(
                        plot, f"{dir_}lims",
                        kwargs.pop(f"{dir_}lims"))
        plot.title = kwargs.pop("title", "")
        plot.grid = kwargs.pop("grid", True)
        if "aspect" in kwargs:
            plot.aspect = kwargs.pop("aspect")
        self._plot = plot
        return plot

    # TODO: remove this function. for now it is only kept for legacy
    def get_plot(self, *args, **kwargs):
        """Create the plot."""
        return self.create_plot(*args, **kwargs)

    def show(self, *args, save_at=None, **kwargs):
        """Create the plot and show it. Also can save the plot.

        Parameters
        ----------
        save_at: str, optional
                 If not None, gives the path to where the figure will be saved.
        """
        if self._plot is None:
            self._plot = self.get_plot(*args, **kwargs)
        if save_at is not None:
            self._plot.save(save_at)
        self._plot.plot()

    def _extract_main_plot_params_from_kwargs(self, kwargs):
        return {"title": kwargs.pop("title", ""),
                "xlabel": kwargs.pop("xlabel", ""),
                "xunits": kwargs.pop("xunits", None),
                "ylabel": kwargs.pop("ylabel", ""),
                "yunits": kwargs.pop("yunits", None),
                }


class BasePostProcClassWithFermi(BasePostProcClass):
    """Base post proc class with fermi energy property."""

    def __init__(self, *args, fermi_energy: float = None, **kwargs):
        BasePostProcClass.__init__(self, *args, **kwargs)
        self._fermienergy = None
        if fermi_energy is not None:
            self.fermi_energy = fermi_energy

    @property
    def fermi_energy(self):
        """Return the fermi energy."""
        if self._fermienergy is not None:
            return self._fermienergy
        raise ValueError("Set fermi_energy before using it.")

    @fermi_energy.setter
    def fermi_energy(self, fermi_energy):
        if not is_scalar_or_str(fermi_energy):
            raise TypeError(f"Expected scalar but got: {fermi_energy}")
        self._fermienergy = float(fermi_energy)
        self._logger.debug(f"Setting fermi level to '{self._fermienergy}'.")

    @property
    def fermi_level(self):
        """The fermi energy (alias)."""
        return self.fermi_energy

    @fermi_level.setter
    def fermi_level(self, fermi):
        self.fermi_energy = fermi


class BasePostProcClassWithEnergies(BasePostProcClass):
    """base postprocessing class for quantities with energies."""

    def __init__(
            self, *args,
            energies: np.ndarray = None,
            **kwargs):
        BasePostProcClass.__init__(self, *args, **kwargs)
        self._energies = None
        if energies is not None:
            self.energies = energies

    @property
    def energies(self):
        """Return the energies array."""
        if self._energies is not None:
            return self._energies
        raise ValueError("Need to set energies before using them.")

    @energies.setter
    def energies(self, energies):
        energies = np.array(energies)
        self._energies = energies

    @property
    def frequencies(self):
        """Return the phonon frequencies."""
        return self.energies

    @frequencies.setter
    def frequencies(self, freqs):
        self.energies = freqs


class BasePostProcClassWithEnergyDispersions(BasePostProcClassWithEnergies):
    """Base postprocessing class for quantities with energy dispersions.

    This class puts constraints on the shape of the energies array
    and makes sure that the sizes of kpt array matches.
    """

    def __init__(
            self, *args,
            **kwargs):
        BasePostProcClassWithEnergies.__init__(self, *args, **kwargs)
        self._nkpts = None
        self._bands = None
        self._nbands = None

    @property
    def energies(self):
        """Return the energies array."""
        return BasePostProcClassWithEnergies.energies.fget(self)

    @energies.setter
    def energies(self, energies):
        self._set_energies(energies)

    def _set_energies(
            self, energies, set_nkpts: bool = True,
            set_nbands: bool = True,
            ) -> None:
        if is_vector(energies):
            # only one point
            energies = [energies]
        if not is_2d_arr(energies):
            raise ValueError(f"Expected 2D array but got: {energies}")
        energies = np.array(energies)
        # check size with number of kpts
        try:
            # if kpts aren't set, this will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"nb of energies ({self.energies.shape[0]}) "
                                f" not commensurable with number of points "
                                f"({self.nkpts}).")
        except ValueError:
            # bypass check
            pass
        self._energies = energies
        if set_nkpts:
            # set nkpts
            self._nkpts = energies.shape[0]
        if set_nbands:
            # set nbands
            self._nbands = energies.shape[1]

    @property
    def bands(self):
        """Return the bands arrays."""
        return self.energies.T

    @property
    def nbands(self):
        """Return the number of bands."""
        if self._nbands is not None:
            return self._nbands
        raise ValueError(
                "Set energies/freqs to get nbands/nbrances.")

    @property
    def nbranches(self):
        """Return the number of branches."""
        return self.nbands

    @property
    def nkpts(self):
        """Return the number of kpts."""
        if self._nkpts is not None:
            return self._nkpts
        raise ValueError(
                "Need to set kpts/qpts or energies before getting nb of pts.")

    # alias
    @property
    def qpoints(self):
        """Return the list of qpts."""
        return self.qpts

    @qpoints.setter
    def qpoints(self, qpoints):
        self.qpts = qpoints

    @property
    def qpts(self):
        """Return the list of qpts."""
        # samething as kpts
        return self.kpts

    @qpts.setter
    def qpts(self, qpts):
        # use kpts for qpts
        self.kpts = qpts

    @property
    def nqpoints(self):
        """Return the number of qpoints."""
        return self.nqpts

    @property
    def nqpts(self):
        """Return the number of qpoints."""
        return self.nkpts


class Dispersion(BasePostProcClassWithEnergyDispersions):
    """Base postprocessing class for quantities with dispersion.

    This includes all quantities that have a reciprocal space (k/q-space)
    dependency with possibly a band/branch index.
    """

    def __init__(self, *args, **kwargs):
        BasePostProcClassWithEnergies.__init__(self, *args, **kwargs)
        self._kpath = None
        self._kpts = None

    @property
    def kpath(self):
        """Return the list of tuples defining the k/q-pt path labels."""
        return self._kpath

    @kpath.setter
    def kpath(self, kpath):
        if not is_list_like(kpath):
            raise TypeError("'kpath' should be a list.")
        for item in kpath:
            if not is_list_like(item, length=2):
                raise ValueError(
                        "Each item in kpath should be a length-2 tuple whose "
                        "first element is kpt label and 2nd is location.")
        self._kpath = kpath

    @property
    def kpts(self):
        """Return the kpts list."""
        if self._kpts is not None:
            return self._kpts
        raise ValueError("Need to set kpts/qpts before using them.")

    @kpts.setter
    def kpts(self, kpts):
        # kpts must be a Nkpts x 3 array / list
        if is_vector(kpts):
            kpts = [kpts]
        if not is_2d_arr(kpts):
            raise TypeError(f"Expected 2d array but got: {kpts}")
        kpts = np.array(kpts)
        if kpts.shape[-1] != 3:
            raise TypeError(f"Expected Nx3 array but got: {kpts}")
        self._kpts = kpts
        self._nkpts = len(self.kpts)
        # check nkpts vs number of pts for energies
        try:
            # if energies not set it will raise an error
            if self.energies.shape[0] != self.nkpts:
                raise TypeError(f"npts ({self.nkpts}) not commensurable with "
                                f"energies ({self.energies.shape[0]})")
        except ValueError:
            # energies not set, bypass the check
            pass

    @property
    def qpath(self):
        """Return the phonon dispersion q-pt path."""
        return self.kpath

    @qpath.setter
    def qpath(self, qpath):
        self.kpath = qpath

    def create_plot(
            self, *args,
            adjust_axis=True,
            bands=None,
            color="k",
            high_sym_vlines=False,
            line_at_zero=True,
            other_k_labels=None,
            ylabel="Energy",
            **kwargs):
        """Create the dispersion plot.

        Parameters
        ----------
        adjust_axis: bool, optional
            If True, a new kpt/qpt axis is computed so that the distance
            between points is proportional to the length of the reciprocal
            vectors.
        bands: list, optional
            Selects the range of bands to plot (starting from 0).
            If set to None, all bands are shown.
            (tuple: (min, max))
        color: str, optional
            Bands' color.
        high_sym_vlines: bool, optional
            If True, plain vertical lines will be shown
            at the high symetry points.
        line_at_zero: bool, optional
            If True, a line is drawn at 0 energy.

        Other Parameters
        ----------------
        kwargs:
            Other kwargs are passed to
            the :meth:`.BasePostProcClass.create_plot` method.

        Returns
        -------
        The dispersion plot.
        """
        considered_bands = self._get_considered_bands(self.bands, bands)
        plot = BasePostProcClass.create_plot(
                self, *args, ylabel=ylabel,
                xlabel=kwargs.pop("xlabel", ""),
                xunits=kwargs.pop("xunits", None),
                yunits=kwargs.pop("yunits", None),
                )
        if self.kpath is not None:
            labels = [x[0] for x in self.kpath]
            labels_loc = [x[1] for x in self.kpath]
        else:
            # special value TODO: find a better way to do this
            labels = ["", ""]
            labels_loc = None
        if labels_loc is None:
            labels_loc = [0, self.nkpts - 1]
        if adjust_axis:
            # compute new kpts axis.
            if self.nkpts - 1 not in labels_loc:
                self._logger.warning("Last kpt don't have a label.")
                labels_loc.append(self.nkpts - 1)
                labels.append("")
            xs, labels_loc = self._get_kpts_axis(labels_loc)
        else:
            # just normalize by nb of points the axis
            xs = [i/self.nkpts for i in range(self.nkpts)]
            labels_loc = [i/self.nkpts for i in labels_loc]
        plot.xtick_labels = [(pos, label) for pos, label in
                             zip(labels_loc, labels)]
        if line_at_zero:
            plot.add_hline(0, linestyle="--")
        if high_sym_vlines:
            for pos in labels_loc:
                plot.add_vline(pos)
        for band in considered_bands:
            plot.add_curve(xs, band, color=color, **kwargs)
        curves = plot.plot_objects["curves"]
        if len(curves):
            # add limits to avoid the +/-5% automatic adjusting
            plot.xlims = [min(curves[0].xdata), max(curves[0].xdata)]
        return plot

    def set_kpath_from_uniform_density(self, labels, density):
        """Set the kpath from a uniform kpt density between divisions.

        Parameters
        ----------
        labels : list
            The list of kpoint labels.
        density : int
            The kpoint path density.
        """
        kpath = []
        for label, coord in zip(
                labels, range(0, len(labels) * (density + 1), density)):
            kpath.append((label, coord))
        self.kpath = kpath

    def set_kpath_from_coordinates(
            self, labels, label_coordinates, precision=1e-4):
        """Set the kpath from the list of labels and their coordinates.

        Ordering is not assumed, for every kpt label we loop over all kpts
        in the band structure.

        Parameters
        ----------
        labels: list
            The list of labels to detect.
        label_coordinates:
            The list of label coordinates matching the list of labels.
        precision: float, optional
            The precision in order to detect the kpt labels index.
            Abinit stores 4 digits in EIG file for kpt coordinates
            so having a precision lower than this is useless.
        """
        kpath = []
        if not is_list_like(labels):
            raise TypeError("'labels' should be list-like.")
        if not is_list_like(label_coordinates, length=len(labels)):
            raise ValueError(
                "'label_coordinates' should be a list with same length as "
                "the 'labels' list.")
        if not is_scalar(precision, strickly_positive=True):
            raise ValueError("'precision' should be a float > 0.")
        for ikpt, kpt in enumerate(self.kpts):
            for label, label_coordinate in zip(labels, label_coordinates):
                if (np.abs(np.array(label_coordinate) -
                           np.array(kpt)) < precision).all():
                    kpath.append((label, ikpt))
                    break
        self.kpath = kpath

    def _get_considered_bands(self, array, bands):
        """Returns the considered bands of an array if needed."""
        considered_bands = array
        if bands is not None:
            considered_bands = array[range(bands[0], bands[1] + 1), :]
        return considered_bands

    def _get_kpts_axis(self, labels_loc):
        # WARNING: this method is quite complicated and may only work
        # correctly for cubic systems...

        # return list(range(len(self.kpts)))
        # labels loc are the locations of the high symmetry points defining
        # sub paths in the kpts path
        # we want to get the xaxis pts such that the lengths of the subpaths in
        # reciprocal space are proportional to the lengths of corresponding
        # sections on band structure.
        # first get all subpaths
        if labels_loc is None:
            raise ValueError("labels_loc cannot be None.")
        if 0 not in labels_loc:
            raise LookupError(f"First kpt label not at 0: {labels_loc}")
        subpaths = []
        subpath = None
        for ikpt in range(len(self.kpts)):
            if ikpt in labels_loc:
                if subpath is None:
                    subpath = []
                    subpath.append(ikpt)
                    continue
                else:
                    subpath.append(ikpt)
                    subpaths.append(subpath)
                    subpath = [ikpt]
                    continue
            else:
                subpath.append(ikpt)
        # there should be n_labels - 1 kpts subpaths
        if len(subpaths) != len(labels_loc) - 1:
            raise LookupError(
                    "Number of subpaths don't match number of labels.")
        # get the lengths in reciprocal space of each subpaths
        lengths = []
        nkpts_per_subpath = []
        for subpath in subpaths:
            k_initial = self.kpts[subpath[0]]
            k_final = self.kpts[subpath[-1]]
            length = np.linalg.norm(k_final - k_initial)
            if length <= 1e-6:
                # very small length => probably same point
                raise RuntimeError(
                        "Missing labels in kpt path => subpaths have 0 length!"
                        )
            lengths.append(np.linalg.norm(k_final - k_initial))
            nkpts_per_subpath.append(len(subpath))
        total_length = sum(lengths)
        # check number of kpts in nkpts_per_subpath matches total nb of kpts
        if (sum(nkpts_per_subpath) - len(nkpts_per_subpath) + 1 !=
                len(self.kpts)):
            raise LookupError(
                    "Number of kpts per subpaths don't match tot number of "
                    "kpts.")
        # get the first and final positions of
        # the subpath on the band structure
        x_finals = []
        x_initials = []
        for j in range(len(nkpts_per_subpath)):
            x_finals.append(sum(lengths[:j+1]) / total_length)
            x_initials.append(sum(lengths[:j]) / total_length)
        xs = []
        for j, (x_initial, x_final, nkpts) in enumerate(
                zip(x_initials, x_finals, nkpts_per_subpath)):
            if j == len(nkpts_per_subpath) - 1:
                xs += np.linspace(
                        x_initial, x_final, num=nkpts, endpoint=True).tolist()
                continue
            xs += np.linspace(
                    x_initial, x_final, num=nkpts - 1, endpoint=False).tolist()
        # need to update the position of the labels location
        new_labels_loc = [labels_loc[0]]  # first label loc is 0 anyways
        new_labels_loc += x_finals
        # check that number of generated points matches number of kpts
        if len(xs) != len(self.kpts):
            raise LookupError("Number of xpts don't match number of kpts.")
        return xs, new_labels_loc


class BasePostProcClassWithWeights(BasePostProcClassWithEnergyDispersions):
    """Base processing class with kpts/qpts weights."""

    _weights_sum_to = None

    def __init__(
            self, *args,
            weights: np.ndarray = None,
            **kwargs):
        super().__init__(*args, **kwargs)
        if self._weights_sum_to is None:
            raise DevError(
                    "Need to set '_weights_sum_to'.")
        self._weights = None
        if weights is not None:
            self.weights = weights

    @property
    def weights(self) -> np.ndarray:
        """The kpt weights."""
        if self._weights is not None:
            return self._weights
        raise ValueError("Need to set kpt weights.")

    @weights.setter
    def weights(self, w: np.ndarray) -> None:
        if self._energies is not None:
            if len(w) != self._energies.shape[0]:
                raise ValueError(
                        "Shape mismatch of weights and nkpts.")
        self._check_weights(w)
        self._weights = w

    def _check_weights(self, w: np.ndarray) -> None:
        if not is_list_like(self._weights_sum_to):
            sumto = [self._weights_sum_to]
        else:
            sumto = self._weights_sum_to
        if np.round(np.sum(w), 5) not in sumto:
            raise ValueError(f"Weights must sum to {sumto}.")


class TemperatureDependentQuantity(BaseUtility):
    """Base class for temperature dependant quantities."""

    def __init__(self, *args, temperature: float = None, **kwargs):
        super().__init__(*args, **kwargs)
        self._temperature = temperature
        if temperature is not None:
            self.temperature = temperature

    @property
    def temperature(self) -> float:
        """The temperature in K."""
        if self._temperature is not None:
            return self._temperature
        raise ValueError("Need to set 'temperature'.")

    @temperature.setter
    def temperature(self, temp: float) -> None:
        if temp < 0:
            raise ValueError("Temperature must be >= 0.")
        self._temperature = temp


class TemperaturesDependentQuantity(BaseUtility):
    """Same as TemperatureDependantQuantity with multiple temperatures."""

    def __init__(self, *args, temperatures: Sequence[float] = None, **kwargs):
        BaseUtility.__init__(self, *args, **kwargs)
        self._temperatures = temperatures
        if temperatures is not None:
            self.temperatures = temperatures

    @property
    def temperatures(self) -> Sequence[float]:
        """The temperatures in K."""
        if self._temperatures is not None:
            return self._temperatures
        raise ValueError("Need to set 'temperatures'.")

    @temperatures.setter
    def temperatures(self, temps: Sequence[float]) -> None:
        if not is_vector(temps):
            raise TypeError("'temperatures' not a list.")
        temps = np.asarray(temps)
        if np.any(temps < 0):
            raise ValueError("Temperatures must be >= 0.")
        self._temperatures = temps
