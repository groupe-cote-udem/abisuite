import logging

import matplotlib.pyplot as plt

import numpy as np

from scipy.spatial import KDTree, Voronoi

try:
    from skimage.measure import marching_cubes
    __SKIMAGE_AVAILABLE__ = True
except ImportError:
    __SKIMAGE_AVAILABLE__ = False


import tqdm.notebook as tqdm

from .bases import BasePostProcClassWithFermi, Dispersion
from ..constants import HA_TO_EV
from ..handlers import (
        AbinitEIGFile, AbinitOutputFile,
        CalculationDirectory,
        )
from ..physics import (
        KptMesh,
        Lattice, PointGroup,
        )


class FermiSurface(BasePostProcClassWithFermi, Dispersion):
    """Object representing a fermi surface plot."""

    _loggername = "FermiSurface"

    def __init__(self, *args, **kwargs):
        BasePostProcClassWithFermi.__init__(self, *args, **kwargs)
        Dispersion.__init__(self, *args, **kwargs)
        self.kptmesh = KptMesh(loglevel=self._loglevel)
        self._coordinate_system = None

    @property
    def point_group(self):
        """Return the point group name associated with this system."""
        return self.kptmesh.point_group

    @point_group.setter
    def point_group(self, ptgroup):
        self.kptmesh.point_group = ptgroup

    @property
    def coordinate_system(self):
        """Return the kpts coordinate system."""
        if self._coordinate_system is not None:
            return self._coordinate_system
        raise ValueError("Need to set 'coordinate_system'.")

    @coordinate_system.setter
    def coordinate_system(self, system):
        if system not in ("reduced", "cartesian"):
            raise ValueError(system)
        self._coordinate_system = system

    @property
    def lattice(self):
        """Return the Lattice object associated with this Fermi Surface."""
        return self.kptmesh.lattice

    @lattice.setter
    def lattice(self, lattice):
        self.kptmesh.lattice = lattice

    def init_lattice_from_abinit_output_file(
            self, filepath, *args, **kwargs):
        """Initialize the lattice from an abinit output file path."""
        handler = self.kptmesh.init_lattice_from_abinit_output_file(
                filepath, *args, return_handler=True, **kwargs)
        with handler:
            self.fermi_energy = handler.dtsets[0]["fermi_energy"] * HA_TO_EV

    @classmethod
    def from_abinit_eig_file(
            cls, filepath, *args, fermi_energy=None,
            acell=None, rprim=None,
            **kwargs):
        """Create a FermiSurface object from an abinit EIG file.

        Parameters
        ----------
        acell: list-like
            If not None, rprim must not be None as well. Specifies the acell
            parameters for the material.
        rprim: 2d array-like
            If not None 'acell' must not be None as well. Speficies the rprim
            vectors that define the material structure.
        fermi_energy: float, optional
            This is actually mandatory for the object but it can be
            set after init. States what is the fermi level. Must be in eV!
        filepath: str
            The path towards the EIG file.

        Returns
        -------
        FermiSurface: The fermi surface object.
        """
        obj = cls(*args, **kwargs)
        with AbinitEIGFile.from_file(
                filepath, loglevel=kwargs.get(
                    "loglevel", logging.INFO)) as eig:
            obj.energies = np.array(eig.eigenvalues) * HA_TO_EV
            obj.kpts = eig.k_points
            obj.kptmesh.init_kpts_reduced_coordinates(obj.kpts)
            obj.coordinate_system = "reduced"
        if fermi_energy is not None:
            obj.fermi_energy = fermi_energy
        if acell is not None and rprim is None:
            raise ValueError("'acell' is not None but 'rprim' missing.")
        if acell is None and rprim is not None:
            raise ValueError("'rprim' is not None but 'acell' missing.")
        if acell:
            obj.set_lattice_from_abinit_input_variables(
                    acell=acell, rprim=rprim)
        return obj

    @classmethod
    async def from_calculation(cls, calculation, *args, **kwargs):
        """Create the FermiSurface object from a calculation.

        Parameters
        ----------
        calculation: str
            The path towards the calculation.

        Raises
        ------
        NotImplementedError:
            When dealing with calculations other than qe_pw calcs.

        Returns
        -------
        FermiSurface: The object representing the Fermi Surface
        """
        async with await CalculationDirectory.from_calculation(
                calculation,
                loglevel=kwargs.get("loglevel", logging.INFO)) as calc:
            if await calc.calctype == "qe_pw":
                return await cls.from_qe_pw_calculation(calc, *args, **kwargs)
            elif await calc.calctype == "abinit":
                return await cls.from_abinit_calculation(calc, *args, **kwargs)
            raise NotImplementedError(await calc.calctype)

    @classmethod
    async def from_abinit_calculation(cls, calc, *args, **kwargs):
        """Create the Fermi Surface object from an abinit calculation.

        Parameters
        ----------
        calc: str or CalculationDirectory instance
            The path towards the abinit calculation.

        Returns
        -------
        FermiSurface:
            The fermi surface object.
        """
        if not isinstance(calc, CalculationDirectory):
            calc = await CalculationDirectory.from_calculation(
                    calc, loglevel=kwargs.get("loglevel", logging.INFO))
        # eigenvalues are stored in the log file
        obj = cls(*args, **kwargs)
        obj.lattice = await Lattice.from_abinit_calculation(
                calc.path, loglevel=calc._loglevel)
        async with await AbinitEIGFile.from_calculation(
                calc.path, loglevel=calc._loglevel) as eig:
            obj.kpts = np.array(eig.k_points)
            obj.kptmesh.init_kpts_reduced_coordinates(obj.kpts)
            obj.coordinate_system = "reduced"
            obj.energies = np.array(eig.eigenvalues) * HA_TO_EV
        async with await AbinitOutputFile.from_calculation(
                (await calc.parents)[0], loglevel=calc._loglevel) as out:
            obj.fermi_energy = out.dtsets[0]["fermi_energy"] * HA_TO_EV
        return obj

    @classmethod
    async def from_qe_pw_calculation(cls, calc, *args, **kwargs):
        """Create the Fermi Surface object from a qe_pw calculation.

        Parameters
        ----------
        calc: str or CalculationDirectory instance
            The qe_pw calculation.

        Returns
        -------
        FermiSurface:
            The fermi surface object.
        """
        if not isinstance(calc, CalculationDirectory):
            calc = await CalculationDirectory.from_calculation(
                    calc, loglevel=kwargs.get("loglevel", logging.INFO))
        # eigenvalues are stored in the log file
        obj = cls(*args, **kwargs)
        async with await calc.log_file as log:
            obj.kpts = np.array(log.eigenvalues["k_points"])
            obj.energies = np.array(log.eigenvalues["eigenvalues"])
            obj.fermi_energy = log.fermi_energy
        async with calc.input_file as inf:
            param = inf.input_variables["k_points"]["parameter"]
            acell_a = inf.input_variables["celldm(1)"].value
        if param == "tpiba":
            # already in cartesian coordinates
            obj.kpts *= 2 * np.pi / acell_a
            obj.kptmesh.init_kpts_cartesian_coordinates(obj.kpts)
            obj.coordinate_system = "cartesian"
        else:
            raise NotImplementedError(param)
        obj.lattice = await Lattice.from_calculation(
                calc, loglevel=calc._loglevel)
        obj.point_group = await PointGroup.from_qe_pw_calculation(
                calc, loglevel=calc._loglevel)
        return obj

    def create_plot(self, *args, fermi_surface_k_z=None, **kwargs):
        """Create the Fermi Surface plot.

        Parameters
        ----------
        fermi_surface_k_z: float, optional
            If not None, specifies the k_z value to plot a 2D fermi surface.

        Other Parameters
        ----------------
        args/kwargs:
            Other parameters are passed to either the
            the :meth:`create_2d_fermi_surface` method if
            ``fermi_surface_k_z`` is not None or to
            the :meth:`create_3d_fermi_surface` method in the other
            case.

        Returns
        -------
        Plot: The plot object representing the Fermi surface.
        """
        # assume kpts are in cartesian coordinates already
        if fermi_surface_k_z is not None:
            return self.create_2d_fermi_surface(
                    fermi_surface_k_z, *args, **kwargs)
        else:
            return self.create_3d_fermi_surface(
                    *args, **kwargs)

    def create_2d_fermi_surface(
            self, fermi_surface_k_z, apply_symmetries=True, *args, **kwargs):
        """Create a 2D fermi surface plot at a given k_z.

        Parameters
        ----------
        apply_symmetries: bool, optional
            If True, we apply symmetries from the sgroup of the material
            to recover the FBZ.
        fermi_surface_k_z: float
            The k_z to plot the fermi surface at.

        Other Parameters
        ----------------
        args/kwargs:
            These are passed to
            the :meth:`.BasePostProcClassWithFermi.create_plot` method.

        Returns
        -------
        Plot: The plot object representing the 2D Fermi surface.
        """
        self._logger.info("Plotting 2D fermi surfaces.")
        self.kptmesh.init_kpts(self.kpts, self.coordinate_system)
        # round to 10 decimals since converting might get stuff rediculously
        # close to some numbers like 0 but not exactly (avoid rounding errors)
        kpts = self.kptmesh.get_kpts_cartesian_coordinates(rounded=10)
        fskz = fermi_surface_k_z
        if fskz not in kpts[:, 2]:
            raise ValueError(f"k_z={fskz} not in computed values!")
        # reinitialize kpt mesh to only extend kpts that we are interested in
        self.kptmesh.extend_full_bz()
        allkpts = self.kptmesh.get_kpts_cartesian_coordinates(rounded=10)
        allenergies = self.energies[self.kptmesh.extended_mapping]
        # keep only data at the level we are interested
        where = np.where(np.abs(allkpts[:, 2] - fskz) < 1e-10)[0]
        kpts = allkpts[where]
        energies = allenergies[where].T
        kxs = np.unique(kpts[:, 0])
        kys = np.unique(kpts[:, 1])
        plot = BasePostProcClassWithFermi.create_plot(
                self, *args,
                aspect="equal",
                xlabel=kwargs.get("xlabel", r"$k_x$"),
                ylabel=kwargs.get("ylabel", r"$k_y$"),
                grid=False,
                title=(
                    r"$\epsilon_F=$" +
                    f"{round(self.fermi_energy, 3)} eV " +
                    r"$k_z=$ " + f"{fskz}"
                    ),
                )
        # need to manually reshape bands array in case the grid is not
        # perfectly square since kpts might only be computed inside
        # first BZ but contour method requires a rectangular mesh
        nbands = energies.shape[0]
        bd = np.zeros((nbands, len(kxs), len(kys)))
        tree = KDTree(allkpts)
        masked = np.ones((len(kxs), len(kys)))
        for ikx, kx in enumerate(kxs):
            for iky, ky in enumerate(kys):
                if self.kptmesh.inside_first_bz(
                        [kx, ky, fskz], reduced_coordinates=False):
                    masked[ikx, iky] = 0
                ik = np.logical_and(kpts[:, 0] == kx, kpts[:, 1] == ky)
                if not np.any(ik):
                    # this kpt is not part of the initial mesh.
                    # check if it can be folded in the first BZ easily
                    # and get it's corresponding energy
                    newkpt = np.array([kx, ky, fskz])
                    if not self.kptmesh.inside_first_bz(
                            newkpt, reduced_coordinates=False):
                        # this kpt will be masked to just don't bother
                        continue
                    bd[:, ikx, iky] = allenergies.T[:, tree.query(newkpt)[1]]
                else:
                    bd[:, ikx, iky] = energies[:, ik][:, 0]
        # mask the band values outside the first BZ
        bd = np.ma.array(
                bd, mask=np.repeat(masked[np.newaxis, :, :], nbands, axis=0))
        # for each bands that crosses the fermi level,
        # plot a contour line
        colors = ["r", "b", "g", "orange"]  # if more than that -> cycle
        icolor = 0
        for iband, band in enumerate(bd):
            if np.min(band) > self.fermi_energy:
                continue
            if np.max(band) < self.fermi_energy:
                continue
            energy_surface = band - self.fermi_energy
            plot.add_contour(
                    kxs, kys,
                    energy_surface,
                    (0.0, ), linewidths=2,
                    color=colors[icolor % len(colors)],
                    corner_mask=True,
                    label=f"iband={iband}",
                    )
            icolor += 1
        plot.legend = True
        plot.legend_outside = True
        plot.add_hline(0)
        plot.add_vline(0)
        # plot BZ boundary
        rec_points = np.round(
                self.kptmesh.nearest_reciprocal_lattice_points, 12)
        vor = Voronoi(rec_points)
        # get list of faces. each element is a list of vertices
        faces = []
        for r in vor.ridge_dict:
            if r[0] == 13 or r[1] == 13:
                faces.append([vor.vertices[i] for i in vor.ridge_dict[r]])
        # get faces that intersects the k_z value
        foi = []
        for face in faces:
            if (np.array(face)[:, 2] - fskz > 1e-10).all():
                continue
            if (np.array(face)[:, 2] - fskz < -1e-10).all():
                continue
            foi.append(np.array(face))
        # now each face either lands on the boundary exactly or crosses it.
        # faces that lies on it will have 2 pts on it.
        # another case is a face that crosses the boundary have 2 vertices on
        # it as well. all of this will be considered by this
        for face in foi:
            where = np.where(np.abs(face[:, 2] - fskz) < 1e-8)[0]
            if len(where) == 2:
                limits = np.array([face[where[0]], face[where[1]]])
                plot.add_curve(
                    limits[:, 0], limits[:, 1], linewidth=1, color="k")
        # plot.curves[-1].label = "BZ border"
        curves = plot.plot_objects["curves"]
        plot.xlims = [1.05 * min([min(c.xdata) for c in curves]),
                      1.05 * max([max(c.xdata) for c in curves])]
        plot.ylims = [1.05 * min([min(c.ydata) for c in curves]),
                      1.05 * max([max(c.ydata) for c in curves])]
        # plot.plot(show=False)
        return plot

    def create_3d_fermi_surface(
            self, *args, apply_symmetries=True,
            allow_surfaces_outside_first_bz=False,
            **kwargs):
        """Create a 3D fermi surface plot using the marching cubes algorithm.

        Parameters
        ----------
        apply_symmetries: bool, optional
            If True, we apply symmetries from the sgroup of the material
            to recover the FBZ.

        Other Parameters
        ----------------
        args/kwargs:
            Other args and kwargs are passed to
            the :meth:`.BasePostProcClassWithFermi.create_plot` method.

        Returns
        -------
        Plot: The plot object representing the 3D Fermi surface.
        """
        if not __SKIMAGE_AVAILABLE__:
            self._logger.error(
                    "scikit-image is not available / does not work. I need it"
                    " for computing 3D fermi surface. Try (re)installing it.")
            return
        self._logger.info("Plotting 3D fermi surfaces.")
        # now plot whole volume
        # get all points that would be on a contour for each kz
        kpts = self.kpts
        if apply_symmetries:
            xlims = [-1.05 * min(kpts[:, 0]), 1.05 * max(kpts[:, 0])]
            ylims = [-1.05 * min(kpts[:, 1]), 1.05 * max(kpts[:, 1])]
            zlims = [-1.05 * min(kpts[:, 2]), 1.05 * max(kpts[:, 2])]
        else:
            xlims = [-0.5, 0.5]
            ylims = [-0.5, 0.5]
            zlims = [-0.5, 0.5]
        self.kptmesh.init_kpts(kpts, self.coordinate_system)
        self.kptmesh.extend_full_bz(
                drop_duplicates=True,
                )
        allkpts = self.kptmesh.get_kpts_cartesian_coordinates(rounded=10)
        allenergies = self.energies[self.kptmesh.extended_mapping].T
        kxs = np.unique(allkpts[:, 0])
        kys = np.unique(allkpts[:, 1])
        kzs = np.unique(allkpts[:, 2])
        nbands = allenergies.shape[0]
        energies = np.zeros((nbands, len(kxs), len(kys), len(kzs)))
        spacing = [kxs[1] - kxs[0], kys[1] - kys[0], kzs[1] - kzs[0]]
        tree = KDTree(allkpts)
        tot = len(kxs) * len(kys) * len(kzs)
        with tqdm.tqdm(
                total=tot, desc="Reshaping arrays into multidimentionnal ones"
                ) as pbar:
            for ikx, kx in enumerate(kxs):
                for iky, ky in enumerate(kys):
                    for ikz, kz in enumerate(kzs):
                        ik = np.logical_and(
                                np.logical_and(
                                    np.abs(allkpts[:, 0] - kx) < 1e-10,
                                    np.abs(allkpts[:, 1] - ky) < 1e-10),
                                np.abs(allkpts[:, 2] - kz) < 1e-10)
                        if np.any(ik):
                            energies[:, ikx, iky, ikz] = (
                                    allenergies[:, ik][:, 0])
                        else:
                            if not self.kptmesh.inside_first_bz(
                                    [kx, ky, kz], reduced_coordinates=False,
                                    progress_bar=False):
                                # this kpt will be masked anyway
                                pbar.update(1)
                                continue
                            # this kpt is not part of the initial kpt mesh.
                            # try to find its folded image
                            # find closest folded points
                            folded = self.kptmesh.fold_inside_first_bz(
                                    kpts=[[kx, ky, kz]],
                                    coordinate_system="cartesian",
                                    rounded=10, progress_bar=False,
                                    )[0]
                            ik = tree.query(folded)[1]
                            energies[:, ikx, iky, ikz] = allenergies[:, ik]
                        pbar.update(1)
        fermi_surfaces = []
        for band in tqdm.tqdm(
                energies, total=len(energies),
                desc="Computing 3D fermi surfaces"):
            if np.min(band) > self.fermi_energy:
                continue
            if np.max(band) < self.fermi_energy:
                continue
            verts, faces, normals, values = marching_cubes(
                    band, level=self.fermi_energy, spacing=spacing)
            # marching cubes interprets kpts with only positive values
            # it's like if the BZ is not shifted by origin.
            verts = np.array(verts)
            verts += np.array([min(kxs), min(kys), min(kzs)])
            mask = []
            for face in faces:
                for vertex in face:
                    kpt = verts[vertex]
                    if not self.kptmesh.inside_first_bz(
                            kpt, reduced_coordinates=False,
                            progress_bar=False):
                        mask.append(1)
                        break
                else:
                    mask.append(0)
            if not any(np.logical_not(mask)):
                # not sure what happened here but for some reason, all values
                # are outside the first BZ...
                continue
            fermi_surfaces.append((verts, faces, mask))
        if not fermi_surfaces:
            raise RuntimeError("No fermi surfaces detected...")
        # finished collecting all points on the fermi surfaces
        # now plot surface
        icolor = 0
        bz_plot = self.lattice.get_brillouin_zone_plot()
        plots = []
        for icolor, fermi_surface in enumerate(fermi_surfaces):
            plot = BasePostProcClassWithFermi.create_plot(
                    self, *args,
                    xlabel=kwargs.get("xlabel", r"$k_x$"),
                    ylabel=kwargs.get("ylabel", r"$k_y$"),
                    zlabel=kwargs.get("zlabel", r"$k_z$"),
                    xlims=kwargs.get("xlims", xlims),
                    ylims=kwargs.get("ylims", ylims),
                    zlims=kwargs.get("zlims", zlims),
                    aspect="auto", grid=False,
                    )
            verts = np.array(fermi_surface[0])
            faces = np.array(fermi_surface[1])
            facesmask = fermi_surface[2]
            if len(verts) < 3:
                raise RuntimeError(
                        "Fermi surface has less than 3 kpts on it."
                        " Perhaps mesh is too coarse...")
            plot.add_trisurf(
                    verts[:, 0], verts[:, 1], verts[:, 2], triangles=faces,
                    mask=facesmask,
                    color=f"C{icolor}",
                    alpha=1,
                    cmap=None,
                    )
            bz_plot.synchronize_labels(plot)
            icolor += 1
            plot += bz_plot
            plots.append(plot)
        return plots

    def set_lattice_from_abinit_input_variables(self, *args, **kwargs):
        """Set the lattice from the abinit rprim and acell variables.

        Other Parameters
        ----------------
        args/kwargs:
            All parameters are passed to
            the :meth:`.Lattice.from_abinit_input_variables` method.
        """
        self.lattice = Lattice.from_abinit_input_variables(*args, **kwargs)

    def _kpt_in_bz(self, kx, ky, kz, lattice):
        """Return True if a given kpt lies inside the BZ."""
        for rec in lattice.reciprocal_vectors:
            # k.rec <= 0.5 rec ** 2 for it to be inside BZ
            dot = abs(rec.dot([kx, ky, kz]))
            norm = rec.dot(rec)
            if dot > 0.5 * norm:
                return False
        return True

    def _get_contour_pts(self, kxs, kys, kz, energy_surface):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        lines = ax.contour(kxs, kys, energy_surface, (0.0, ))
        segments = lines.allsegs[0]
        pts = []
        for segment in segments:
            for pt in segment:
                pts.append([pt[0], pt[1], kz])
        plt.close(fig)
        del fig, ax, lines, segments
        return pts

    def _get_contour_pts_this_kz(self, kxs, kys, kz):
        kpts = self.kpts
        where = np.where(kpts[:, 2] == kz)[0]
        eigs_this_kz = self.energies[where].T
        # kpts_this_kz = kpts[where]
        # kxs = kpts_this_kz[:, 0]
        # kys = kpts_this_kz[:, 1]
        # gridx, gridy = np.meshgrid(
        #         np.linspace(min(kxs), max(kxs), len(np.unique(kxs))),
        #         np.linspace(min(kys), max(kys), len(np.unique(kys))))
        nbands = len(eigs_this_kz)
        fermi_surfaces = [[] for _ in range(nbands)]
        for iband, band in enumerate(eigs_this_kz):
            if min(band) > self.fermi_energy:
                continue
            if max(band) < self.fermi_energy:
                continue
            energy_surface = (
                    band.reshape(len(kxs), len(kys)) - self.fermi_energy)
            # use matplotlib to draw contour lines and extract data
            pts = self._get_contour_pts(kxs, kys, kz, energy_surface)
            # now that we have segments and points, extend this using
            # symetries
            self.kptmesh.init_kpts_cartesian_coordinates(np.array(pts))
            self.kptmesh.extend_full_bz(show_progress=False)
            self.kptmesh.fold_inside_first_bz()
            fermi_surfaces[iband] += self.kptmesh.kpts.tolist()
        return fermi_surfaces
