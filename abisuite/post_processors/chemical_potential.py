import numpy as np

from scipy.integrate import trapezoid
from scipy.optimize import minimize_scalar

from .bases import (
        BasePostProcClassWithFermi,
        BasePostProcClassWithWeights,
        TemperatureDependentQuantity,
        )
from ..constants import (
        JOULES_TO_EV,
        k_B,
        )


def fermi_dirac(
        energies: np.ndarray,
        mu: float,
        temperature: float,
        ) -> np.ndarray:
    """Computes the fermi-dirac occupation at finite temperature.

    Parameters
    ----------
    energies: np.ndarray
        The energies to evaluate the occupations in eV.
    mu: float
        The chemical potential in eV.
    temperature: float
        The temperature in K.
    """
    if temperature == 0:
        # heavyside
        where1_mask = energies <= mu
        occs = np.zeros_like(energies, dtype=int)
        occs[where1_mask] = 1
        return occs
    e_minus_mu = (energies - mu)
    exp_arg = e_minus_mu / (2 * k_B * JOULES_TO_EV * temperature)
    # use double-precision to prevent overflows
    # also use cosh formulation to halve exponent and prevent overflows
    exp_arg = exp_arg.astype(np.float128)
    # if (exp_arg > 11000).any():
    #     print(exp_arg.max())
    # return 1. / (np.exp(exp_arg) + 1)
    return np.exp(-exp_arg) / (2 * np.cosh(exp_arg))


def _sum_of_occupations_finite_temperature(
        mu: float,
        energies: np.ndarray,
        temperature: float,
        weights: np.ndarray,
        ) -> float:
    """Returns the sum of occupations at finite temperature.

    Parameters
    ----------
    energies: np.ndarray
        The energies where the occupations will be evaluated in eV.
    mu: float
        The chemical potential in eV.
    temperature: float
        The temperature in K.
    weights: np.ndarray
        The kpt weights.
    """
    occupations = fermi_dirac(
            energies, mu, temperature)
    return np.sum(occupations.T.dot(weights[:, None]))


def _integral_of_occupations_finite_temperature(
        mu: float,
        energies: np.ndarray,
        dos: np.ndarray,
        temperature: float,
        ) -> float:
    """Returns the integral of occupations at finite temperature.

    Parameters
    ----------
    energies: np.ndarray
        The energies where the occupations will be evaluated in eV.
    mu: float
        The chemical potential in eV.
    temperature: float
        The temperature in K.
    dos: np.ndarray
        The DOS..
    """
    occupations = fermi_dirac(
            energies, mu, temperature)
    integrand = dos * occupations
    return trapezoid(integrand, x=energies)


class ChemicalPotential(
        BasePostProcClassWithFermi, BasePostProcClassWithWeights,
        TemperatureDependentQuantity,
        ):
    """Class that computes the chemical potential at finite temperature."""

    _loggername = "ChemicalPotential"
    _weights_sum_to = (1, 2)

    def __init__(
            self, *args,
            energies: np.ndarray = None,
            nelectrons: int = None,
            fermi_energy: float = None,
            temperature: float = None,
            weights: np.ndarray = None,
            dos: np.ndarray = None,
            use_dos_formulation: bool = False,
            sommerfeld: bool = False,
            **kwargs):
        """Chemical potential init method.

        Parameters
        ----------
        energies: np.ndarray, optional
            The energies array. Size = (nspin x nkpt x nband) if not use_dos
            else (nspin x len(DOS)).
        nelectrons: int, optional
            Number of electrons.
        fermi_energy: float, optional
            The fermi level. Must have same units as energies array.
        temperature: float, optional
            Temperature in K.
        dos: np.ndarray, optional
            The dos. Must have same size as energies array.
        use_dos_formulation: bool, optional
            If True, use DOS formulation to compute chemical potential.
        sommerfeld: bool, optional
            If True, use sommerfeld expansion with dos formulation
            to compute chemical potential.
        """
        BasePostProcClassWithFermi.__init__(
                self, *args, fermi_energy=fermi_energy, **kwargs)
        BasePostProcClassWithWeights.__init__(
                self, *args, weights=weights,
                energies=energies, **kwargs)
        TemperatureDependentQuantity.__init__(
                self, temperature=temperature, **kwargs)
        self._nelectrons = None
        self._mu = None
        self._dos = None
        self._occupations = None
        self.use_dos_formulation = use_dos_formulation
        if nelectrons is not None:
            self.nelectrons = nelectrons
        if dos is not None:
            self.dos = dos
        self.sommerfeld = sommerfeld

    @property
    def dos(self) -> np.ndarray:
        """The DOS."""
        if self._dos is not None:
            return self._dos
        raise ValueError("Need to set 'dos'.")

    @dos.setter
    def dos(self, d: np.ndarray) -> None:
        self._dos = d
        if len(self._dos) != len(self.energies[0]):
            raise ValueError(
                    f"Shape mismatch between dos ({len(d)}) and "
                    f"energies ({len(self.energies[0])}).")

    @property
    def occupations(self) -> np.ndarray:
        """The occupations at the energies values."""
        if self._occupations is not None:
            return self._occupations
        if self.use_dos_formulation:
            energies = self.energies[0]
        else:
            energies = self.energies
        self._occupations = fermi_dirac(
                energies, self.mu, self.temperature)
        return self._occupations

    @property
    def nelectrons(self) -> int:
        """The number of electrons."""
        if self._nelectrons is None:
            raise ValueError("Need to set 'nelectrons'.")
        return self._nelectrons

    @nelectrons.setter
    def nelectrons(self, n: int) -> None:
        self._nelectrons = n

    @property
    def mu(self):
        """The chemical potential using sum-over-state approach."""
        if self._mu is not None:
            return self._mu
        self._logger.info(
                f"Computing chemical potential at T={self.temperature}K")
        self._mu = self.compute_mu()
        return self._mu

    def compute_mu(self) -> float:
        """Computes mu."""
        if self.sommerfeld:
            return self.compute_mu_sommerfeld()
        if self.use_dos_formulation:
            self._logger.info(
                    "Using DOS form to compute chemical potential")
            chem_fun = _integral_of_occupations_finite_temperature
            args = (self.energies[0], self.dos, self.temperature)
        else:
            chem_fun = _sum_of_occupations_finite_temperature
            args = (self.energies, self.temperature, self.weights)
        optimized_result = minimize_scalar(
                # expect results near fermi level
                # optimize absolute diff between number of electrons
                lambda mu, *args: abs(self.nelectrons - chem_fun(mu, *args)),
                bracket=(0.95 * self.fermi_energy, 1.05 * self.fermi_energy),
                args=args,
                )
        if not optimized_result.success:
            raise RuntimeError("Could not get chemical potential.")
        return optimized_result.x

    def compute_mu_sommerfeld(self):
        """Computes chemical potential from sommerfeld expansion."""
        self._logger.info(
                "Using sommerfeld expansion to compute chemical potential")
        grad = np.gradient(
                self.dos, self.energies[0], edge_order=2)
        # make a linear approx to get derivative and dos at fermi level
        diff = np.abs(self.energies[0] - self.fermi_level)
        closest = np.argmin(diff)
        if self.energies[0][closest] > self.fermi_level:
            closest2 = closest
            closest -= 1
        else:
            closest2 = closest + 1
        x1, x2 = self.energies[0][closest], self.energies[0][closest2]
        y1, y2 = self.dos[closest], self.dos[closest2]
        y1d, y2d = grad[closest], grad[closest2]
        dx = x2 - x1
        dos_at_fermi = (y2 - y1) * self.fermi_level / dx + y2
        grad_at_fermi = (y2d - y1d) * self.fermi_level / dx + y2d
        # sommerfeld expansion is
        # mu = e_F - pi^2(kT)^2D'(e_F)/(6D(e_F))
        # print(f"Fermi{self.fermi_level}closest={x1},2ndclosest= {x2}")
        # print(f"dosclose{y1},dos2ndcloses={y2},dos fermi={dos_at_fermi}")
        # print(f"gradclose{y1d}grad2ndclosest{y2d}gradfermi={grad_at_fermi}")
        # print(f"approxclosest{dos_at_fermi+grad_at_fermi*(x1-self.fermi_level)}")
        # print(f"approx2ndclose{dos_at_fermi+grad_at_fermi*(x2-self.fermi_level)}")
        return (self.fermi_level -
                (np.pi * k_B * JOULES_TO_EV * self.temperature) ** 2 *
                grad_at_fermi / (6 * dos_at_fermi))
