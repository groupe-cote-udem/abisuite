import logging
import os

import aiofiles.os

import numpy as np

from .bases import Dispersion
from ..handlers import (
        AbinitAnaddbInputFile, AbinitAnaddbPhfrqFile, MetaDataFile,
        QEEPWPHBandFreqFile, QEMatdynFreqFile,
        )
from ..handlers.file_parsers import AbinitAnaddbPhfrqParser


class PhononDispersion(Dispersion):
    """Post processing tool that makes a phonon dispersion."""

    _loggername = "PhononDispersion"

    def set_qpath_from_uniform_density(self, *args, **kwargs):
        """Sets the qpath from a uniform qpt density.

        All args and kwargs are passed to the
        :meth:`.Dispersion.set_kpath_from_uniform_density` method.
        """
        super().set_kpath_from_uniform_density(*args, **kwargs)

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Returns a Bandstructure object from a calculation directory.

        This method reads the metadata file and deduces which output file
        to read.
        """
        lvl = kwargs.get("loglevel", logging.INFO)
        async with await MetaDataFile.from_calculation(
                path, loglevel=lvl) as meta:
            calctype = meta.calctype
            if calctype == "abinit_anaddb":
                filepath = await AbinitAnaddbPhfrqParser._filepath_from_meta(
                        meta)
                freqs = await cls._load_freqs_from_abinit_anaddb_phfrq(
                        filepath, *args, **kwargs)
                qpts = await cls._load_qpts_from_abinit_anaddb_input(
                        meta.input_file_path, *args, **kwargs)
                phdisp = cls(*args, **kwargs)
                phdisp.frequencies = freqs
                phdisp.qpts = qpts
                return phdisp
            elif calctype == "qe_matdyn":
                filepath = os.path.join(meta.output_data_dir,
                                        meta.jobname + ".freq")
                return await cls.from_qe_matdyn_freq(filepath, *args, **kwargs)
            elif calctype == "qe_epw":
                # definite file name for EPW
                filepath = os.path.join(meta.rundir, "phband.freq")
                return await cls.from_qe_epw_phbandfreq(
                        filepath, *args, **kwargs)
            else:
                raise NotImplementedError(calctype)

    @classmethod
    def from_qe_matdyn_freq(cls, path, **kwargs):
        """Create phonon dispersion object from a matdyn freq file.

        Parameters
        ----------
        path : The .freq file path.
        """
        return cls._from_any_qe_freq_file(path, QEMatdynFreqFile, **kwargs)

    @classmethod
    def from_qe_epw_phbandfreq(cls, path, **kwargs):
        """Create phonon dispersion object from an epw phbandfreq file.

        Parameters
        ----------
        path : The .freq file path.
        """
        return cls._from_any_qe_freq_file(path, QEEPWPHBandFreqFile, **kwargs)

    @classmethod
    async def _from_any_qe_freq_file(cls, path, filehandler, **kwargs):
        """Return a PhononDispersion object from any QE freq file."""
        if not await aiofiles.os.path.exists(path):
            raise FileNotFoundError(path)
        phdisp = cls(**kwargs)
        async with await filehandler.from_file(
                    path, loglevel=phdisp._loglevel) as freq:
            qpts = np.array(freq.coordinates)
            eigs = np.array(freq.eigenvalues)
        phdisp.qpts = qpts
        phdisp.frequencies = eigs
        return phdisp

    @classmethod
    async def _load_freqs_from_abinit_anaddb_phfrq(cls, path, *args, **kwargs):
        """Returns the phonon frequencies from an anaddb phfrq file."""
        async with await AbinitAnaddbPhfrqFile.from_file(
                path, loglevel=kwargs.get("loglevel", logging.INFO)) as phfrq:
            return phfrq.frequencies

    @classmethod
    async def _load_qpts_from_abinit_anaddb_input(cls, path, *args, **kwargs):
        """Returns the list of qpts from the anaddb input file."""
        async with await AbinitAnaddbInputFile.from_file(
                path, loglevel=kwargs.get(
                    "loglevel", logging.INFO)) as inputfile:
            # discard the last column as it is a 'weight'
            return np.array(inputfile.input_variables["qph1l"].value)[:, :-1]
