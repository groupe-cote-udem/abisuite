import numpy as np

import seaborn as sns

from .bases import BasePostProcClassWithEnergies
from ..handlers import QEEPWInvTauFile
from ..plotters import Plot


class EPHRelaxationTimes(BasePostProcClassWithEnergies):
    """Makes a scatter plot of EPH relaxation times from an EPW calculation."""

    _loggername = "EPHRelaxationTimes"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._relaxation_times = None

    @property
    def relaxation_times(self):
        """The array of relaxation times."""
        if self._relaxation_times is not None:
            return self._relaxation_times
        raise ValueError("Need to set 'relaxation_times'.")

    @relaxation_times.setter
    def relaxation_times(self, relax):
        self._relaxation_times = relax

    def create_plot(self, *args, **kwargs):
        """Creates relaxation times scatter plot. 1 plot / temperature."""
        plots = {}
        plot_decomposed = False
        for itemp, (energies, inv_tau) in enumerate(
                zip(self.energies, self.relaxation_times)):
            if inv_tau.ndim == 3:
                # we have a mode decomposed inv_tau
                plot_decomposed = True
                inv_tau = np.sum(inv_tau, axis=-1)
                energies = energies[:, :, 0]  # same energy for all modes
            plot = self._get_inv_tau_plot_vs_temperature(
                    itemp, inv_tau, energies, *args,
                    ncolors=len(self.energies),
                    **kwargs)
            plots[f"itemp{itemp}"] = plot
        combined = Plot(loglevel=self._loglevel)
        combined.xlabel = "Energy [Ry]"
        combined.ylabel = r"$\tau^{-1}$ [Ry]"
        combined.title = "Combined"
        for label, plot in plots.items():
            scatter = plot.plot_objects["scatters"][0]
            scatter.label = label
            combined.add_scatter(scatter)
        plots["combined"] = combined
        if plot_decomposed:
            # create mode decomposed plots
            plots["mode_decomposed"] = self.create_mode_decomposed_plots(
                    *args, **kwargs)
        return plots

    def _get_inv_tau_plot_vs_temperature(
            self, itemp, inv_tau, energies, *args, ncolors=1, **kwargs,
            ) -> Plot:
        colors = sns.color_palette("hls", ncolors)
        plot = super().create_plot(*args, **kwargs)
        plot.add_scatter(
            energies.flatten(),
            inv_tau.flatten(),
            color=colors[itemp],
            markerfacecolor=colors[itemp],
            marker="o",
            markersize=1,
            )
        plot.xlabel = "Energy [Ry]"
        plot.ylabel = r"$\tau^{-1}$ [Ry]"
        plot.title = f"Relaxation times for itemp={itemp}"
        return plot

    def create_mode_decomposed_plots(self, *args, **kwargs):
        """Make mode decomposed relaxation times plots."""
        plots = {}
        for imode, inv_tau_mode in enumerate(
                np.moveaxis(self.relaxation_times, -1, 0)):
            key = f"imode{imode}"
            plots[key] = {}
            for itemp, (energies, inv_tau) in enumerate(
                    zip(self.energies[:, :, :, imode],
                        inv_tau_mode)):
                plot = self._get_inv_tau_plot_vs_temperature(
                            itemp, inv_tau, energies,
                            ncolors=len(self.energies))
                plots[key][f"itemp{itemp}"] = plot
            combined = Plot(loglevel=self._loglevel)
            combined.title = f"Combined imode={imode}"
            combined.xlabel = plot.xlabel
            combined.ylabel = plot.ylabel
            for templabel, plot in plots[key].items():
                scatter = plot.plot_objects["scatters"][0]
                scatter.label = templabel
                combined.add_scatter(scatter)
            plots[key]["combined"] = combined
        return plots

    @classmethod
    async def from_calculation(cls, path, *args, **kwargs):
        """Initialize EPHRelaxationTimes from a calculation path."""
        instances = {}
        relax_times_handlers = await QEEPWInvTauFile.from_calculation(
                path, loglevel=kwargs.get("loglevel", None))
        for particle, relaxation_times_handler in relax_times_handlers.items():
            try:
                async with relaxation_times_handler as relaxation_times:
                    inst = await cls.from_handler(
                            relaxation_times, *args, **kwargs)
                instances[particle] = inst
            except Exception:
                pass
        return instances

    @classmethod
    async def from_handler(cls, handler, *args, **kwargs):
        """Initialize a EPHRelaxationTimes object from a QEEPWInvTauFile."""
        inst = cls(*args, **kwargs)
        async with handler as relaxation_times:
            inst.relaxation_times = relaxation_times.relaxation_times
            inst.energies = relaxation_times.energies
            if relaxation_times.nmodes is not None:
                inst.nmodes = relaxation_times.nmodes
        return inst

    @classmethod
    async def from_file(cls, path, *args, **kwargs):
        """Initialize an EPHRelaxationTimes object from an inv_tau file."""
        async with await QEEPWInvTauFile.from_file(
                path, loglevel=kwargs.get("loglevel", None)) as handler:
            return await cls.from_handler(handler, *args, **kwargs)
