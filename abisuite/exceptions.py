class DevError(Exception):
    """Generic development error."""

    pass


class DirectoryExistsError(FileExistsError):
    """Error raised when a directory does not exists."""

    pass


class IsAFileError(FileExistsError):
    """Error raised when a file exists."""

    pass


class NotACalculationDirectoryError(FileNotFoundError):
    """Error raised when the calculation directory is not found.

    Raised when we try to access a directory which is thought to
    be a calculation directory but it is not.
    """

    pass


class CalculationNotFoundError(NotACalculationDirectoryError):
    """Alias of the base exception with a different name."""

    pass


class LengthError(ValueError):
    """Error raised when a list-like object doesn't have a good length."""

    pass
