from ..bases import BaseSCFCalculationStatusChecker, __NON_SCF_STATUS_STRING__


class AbinitCalculationStatusChecker(BaseSCFCalculationStatusChecker):
    """Status checker for an Abinit calculation."""

    _calculation_completed_keyword_trigger = (
            "Calculation completed.", "--- !STOP",
            )
    _error_keyword_trigger = "ERROR"
    _loggername = "AbinitCalculationStatusChecker"
    _scf_converged_keywords = "converged"
    _scf_non_converged_keywords = "not enough SCF cycles to converge"

    async def _is_calculation_converged(self):
        # check the calculation type before deciding which are the
        # triggering keywords
        # check the input variables
        async with self.calculation_directory.input_file as input_file:
            # based on the definitions of the variable in abinit,
            # we can deduce the default
            # by default it is always positive thus a SCF calc
            # put 1 for the sake of it
            iscf = input_file.input_variables.get("iscf", 1).value
            nstep = input_file.input_variables.get("nstep", 30).value
            paral_rf = input_file.input_variables.get("paral_rf", 0).value
            optdriver = input_file.input_variables.get("optdriver", 1).value
        if nstep == 0 or optdriver in (3, 4):
            return __NON_SCF_STATUS_STRING__
        if optdriver == 99:
            bs_algorithm = input_file.input_variables.get("bs_algorithm", 2)
            if bs_algorithm != 2:
                return __NON_SCF_STATUS_STRING__
        if paral_rf == -1:
            # special magical value which makes abinit stop after printing
            # irreps of a dfpt calculation
            return __NON_SCF_STATUS_STRING__
        if iscf < 0:
            # NON SCF CALCULATION => cannot tell if convergence is reached
            self._logger.debug(f"'iscf'={iscf}<0 cannot tell if convergence"
                               " is reached.")
            return __NON_SCF_STATUS_STRING__
        # check ionmov
        ionmov = input_file.input_variables.get("ionmov", 0)
        if ionmov > 0:
            # ionmov calculation, change the (non-)converged keywords
            converged = "gradients are converged"
            nonconverged = "not enough Broyd/MD steps to converge gradients"
            return await super()._is_calculation_converged(
                    scf_converged_keywords=converged,
                    scf_non_converged_keywords=nonconverged
                    )
        # else, just a normal GS calculation
        return await super()._is_calculation_converged()
