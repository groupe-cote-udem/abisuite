from .bases import BaseQECalculationStatusChecker


class QEKpointsCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for kpoints.x calculation."""

    _calculation_completed_keyword_trigger = "# of k-points"
    _loggername = "QEKpointsCalculationStatusChecker"
