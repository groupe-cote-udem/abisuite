import os

import aiofiles.os

from .bases import BaseSCFQECalculationStatusChecker
from ..bases import __ERROR_STRING__


class QEPHCalculationStatusChecker(BaseSCFQECalculationStatusChecker):
    """Status checker for a ph.x calculation from Quantum Espresso."""

    _loggername = "QEPHCalculationStatusChecker"
    _scf_converged_keywords = "convergence has been achieved"
    _scf_non_converged_keywords = "no convergence has been achieved"

    async def _dig_log_file_for_convergence(self, log_file):
        # need to check every representation.
        # in other words, count total number of times a representation
        # is computed vs total number of times 'convergence achieved'
        # appears
        lines = await log_file.read(extract_lines_only=True, force=True)
        n_repr = 0
        n_conv = 0
        for line in lines:
            if "Representation #" in line:
                n_repr += 1
                continue
            # put that first since both keywords are exactly the same (almost)
            if self._scf_non_converged_keywords in line.lower():
                return False
            if self._scf_converged_keywords in line.lower():
                n_conv += 1
                continue
        self._logger.debug(f"Found {n_repr} Repr calculations with {n_conv} "
                           "calculation converged keywords")
        return n_repr == n_conv

    async def _get_all_log_files(self):
        log_files = [await self.calculation_directory.log_file]
        # all other log files are listed in the 'run' directory
        # and the all start with 'out.1_0' or something like this
        # append them if they exist
        logcls = log_files[0].__class__
        for subfile in await aiofiles.os.listdir(
                self.calculation_directory.run_directory.path):
            if not subfile.startswith("out."):
                continue
            log = await logcls.from_file(
                    os.path.join(
                        self.calculation_directory.run_directory.path,
                        subfile))
            log_files.append(log)
        return log_files

    # special case since calculation can be split into many images
    # that need to be recovered afterwards if it's the case.
    # when it's split, there is > 1 log file and one needs to check em all
    # to see if calculation is actually finished since the split could be
    # not equal between images (depends on parallelization scheme also).
    async def _dig_log_file_for_status(self, *args, **kwargs):
        # there can be multiple log files. dig all of them
        log_files = await self._get_all_log_files()
        statuses = []
        for log_file in log_files:
            statuses.append(
                    await super()._dig_log_file_for_status(
                        *args, log_file=log_file, **kwargs))
        # if any logs shows an error. return error
        if __ERROR_STRING__ in statuses:
            return __ERROR_STRING__
        # if any log is not finished. return False
        return all(statuses)

    async def _is_calculation_converged(self):
        return all([x is True for x in [
            await self._dig_log_file_for_convergence(x)
            for x in await self._get_all_log_files()]])
