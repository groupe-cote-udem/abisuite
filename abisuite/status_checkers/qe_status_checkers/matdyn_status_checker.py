"""Module for status checkers of Quantum Espresso matdyn.x calculations."""
from .bases import BaseQECalculationStatusChecker


class QEMatdynCalculationStatusChecker(BaseQECalculationStatusChecker):
    """Status checker for a Quantum Espresso matdyn.x calculation."""

    _loggername = "QEMatdynCalculationStatusChecker"
