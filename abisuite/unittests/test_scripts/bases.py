import pytest

from ..routines_for_tests import TemporaryCalculation
from ..variables_for_tests import (
        QEPWCalc,
        abinit_gs_example,
        abinit_gs_pseudos,
        )
from ...routines import is_list_like
from ...status_checkers import __ERROR_STRING__, __NON_SCF_STATUS_STRING__


class BaseTestScript:
    """Base class to test scripts."""

    _script_cls = None

    @pytest.fixture(autouse=True)
    def setup_script(self):
        """Setup script instance fixture."""
        if self._script_cls is None:
            raise ValueError("'_script_cls' not set.")
        self.script = self._script_cls()

    @pytest.fixture
    async def setup_script_with_calculations(self, setup_script, tempdir):
        """Setup script with temporary calculations fixture."""
        self.temp_calc_abi = TemporaryCalculation(
                abinit_gs_example,
                new_pseudos=abinit_gs_pseudos,
                copy_tempdir=tempdir,
                )
        self.temp_calc_qe = TemporaryCalculation(
                QEPWCalc,
                copy_tempdir=tempdir,
                )
        self.temp_calcs_root = tempdir
        with self.temp_calc_abi, self.temp_calc_qe:
            await self.temp_calc_abi.copy_calculation()
            await self.temp_calc_qe.copy_calculation()
            self.calculations = [
                    self.temp_calc_abi.path, self.temp_calc_qe.path,
                    ]
            await self.script.load_calculations(self.calculations)
            yield

    @pytest.mark.parametrize(
            "method", ("error", "warning", "success", "successfull"))
    @pytest.mark.parametrize(
            "text", ("text", 0, None, False, True, -0.9, ValueError, ""))
    def test_print_method(self, method, text):
        """Test the error method."""
        assert getattr(self.script, method)(text) is None

    @pytest.mark.parametrize(
            "status, expected_contain, error",
            [
                ({"calculation_started": False}, ("UNSTARTED", ), None),
                ({"calculation_started": True, "calculation_finished": False},
                 "RUNNING", None),
                ({"calculation_started": True,
                  "calculation_finished": __ERROR_STRING__},
                 ("ERROR", ), None),
                ({"calculation_started": True,
                  "calculation_finished": "INVALID"},
                 None, ValueError),
                ({"calculation_started": True, "calculation_finished": True},
                 "FINISHED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": True,
                  },
                 "CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": False,
                  },
                 "NOT CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": __NON_SCF_STATUS_STRING__,
                  },
                 "FINISHED", None),
                ]
            )
    def test_get_status_flag(
            self, status,
            expected_contain, error):
        """Test the get_status_flag method for AbisuiteStatusScript.

        Override the base class' unittest as there are more parameters
        for this method.
        """
        self._do_test_get_status_flag(status, expected_contain, error)

    def _do_test_get_status_flag(self, status, expected_contain, error, *args):
        if error is not None:
            with pytest.raises(error):
                self.script.get_status_flag(status, *args)
        else:
            flag = self.script.get_status_flag(status, *args)
            if not is_list_like(expected_contain):
                expected_contain = (expected_contain, )
            assert all([expected in flag for expected in expected_contain])
            assert isinstance(flag, str)
