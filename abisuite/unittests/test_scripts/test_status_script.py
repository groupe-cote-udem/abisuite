import numpy as np

import pytest

from .bases import BaseTestScript
from ...scripts.status import AbisuiteStatusScript
from ...status_checkers import __ERROR_STRING__, __NON_SCF_STATUS_STRING__
from ...utils import TerminalTable


class TestStatusScript(BaseTestScript):
    """Test class for the status script."""
    _script_cls = AbisuiteStatusScript

    @pytest.mark.parametrize(
            "sort_by, sorted_column_idx", [
                ("walltime", 2),
                ("path", 0),
                ]
            )
    async def test_status(
            self, setup_script_with_calculations,
            sort_by, sorted_column_idx):
        """Test the status method of the AbisuiteStatusScript."""
        # this should not raise any error
        table = await self.script.status(sort_by=sort_by)
        assert isinstance(table, TerminalTable)
        assert len(table.rows) == len(self.calculations)
        assert len(self.script.calculations) == len(self.calculations)
        assert len(table.column_names) == 3
        # check sorting
        sorted_column = [row[sorted_column_idx] for row in table.rows]
        sorted_indices = np.argsort(sorted_column)
        assert (np.array(sorted_indices) == np.arange(len(sorted_column))
                ).all()

    @pytest.mark.parametrize(
            "status, inqueue, expected_contain, error",
            [
                ({"calculation_started": False}, True, ("UNSTARTED", ), None),
                ({"calculation_started": False}, False,
                 ("UNSTARTED", "NOT IN QUEUE"), None),
                ({"calculation_started": True, "calculation_finished": False},
                 True, "RUNNING", None),
                ({"calculation_started": True,
                  "calculation_finished": __ERROR_STRING__},
                 False, "ERROR", None),
                ({"calculation_started": True,
                  "calculation_finished": __ERROR_STRING__},
                 True, ("ERROR", "RUNNING"), None),
                ({"calculation_started": True,
                  "calculation_finished": __ERROR_STRING__},
                 False, ("ERROR", ), None),
                ({"calculation_started": True,
                  "calculation_finished": "INVALID"},
                 True, None, ValueError),
                ({"calculation_started": True,
                  "calculation_finished": "INVALID"},
                 False, None, ValueError),
                ({"calculation_started": True, "calculation_finished": True},
                 True, "FINISHED", None),
                ({"calculation_started": True, "calculation_finished": True},
                 False, "FINISHED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": True,
                  },
                 True, "CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": True,
                  },
                 False, "CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": False,
                  },
                 True, "NOT CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": False,
                  },
                 False, "NOT CONVERGED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": __NON_SCF_STATUS_STRING__,
                  },
                 True, "FINISHED", None),
                ({"calculation_started": True, "calculation_finished": True,
                  "calculation_converged": __NON_SCF_STATUS_STRING__,
                  },
                 False, "FINISHED", None),
                ]
            )
    def test_get_status_flag(
            self, status, inqueue,
            expected_contain, error):
        """Test the get_status_flag method for AbisuiteStatusScript.

        Override the base class' unittest as there are more parameters
        for this method.
        """
        self._do_test_get_status_flag(status, expected_contain, error, inqueue)
