import os
from unittest.mock import MagicMock, patch

import aiofiles.os

import pytest

from .bases import BaseTestScript
from ...scripts.package import AbisuitePackageScript


class TestPackageScript(BaseTestScript):
    """Tests for the package abisuite script."""

    _script_cls = AbisuitePackageScript

    @pytest.fixture
    def mock_get_packaging_roots(self, tempdir2):
        """Mock fixture for the get_packaging_roots public function."""
        get_packaging_root_mock = MagicMock()
        get_packaging_root_mock.return_value = (self.temp_calcs_root, tempdir2)
        with (patch(
                ("abisuite.handlers.directory_handlers.calculation_directory."
                 "get_packaging_roots"),
                new=get_packaging_root_mock),
              patch(
                "abisuite.packagers.routines.get_packaging_roots",
                new=get_packaging_root_mock,
                ),
              patch(
                "abisuite.packagers.bases.get_packaging_roots",
                new=get_packaging_root_mock,
                ),
              ):
            yield tempdir2

    async def test_package_calculations(
            self,
            setup_script_with_calculations,
            mock_get_packaging_roots,
            ):
        """Test packaging calculations."""
        package_dest = mock_get_packaging_roots
        assert not await aiofiles.os.listdir(package_dest)
        await self.script.package_calculations()  # not replaced by symlinks
        assert len(await aiofiles.os.listdir(package_dest)) == len(
                self.script.calculations)
        # check that if we replace with symlinks it works
        await self.script.package_calculations(
                replace_with_symlink=True,
                force=True, overwrite=True,
                )
        for calc in self.calculations:
            assert await aiofiles.os.path.islink(calc)
            assert await aiofiles.os.readlink(calc) == os.path.join(
                    package_dest, os.path.basename(calc))
