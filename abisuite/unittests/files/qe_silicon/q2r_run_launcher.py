from abisuite import QEQ2RLauncher as Launcher


launcher = Launcher("q2r")
launcher.workdir = "q2r_run"
launcher.link_calculation("ph_run")
launcher.input_variables = {}
launcher.command_arguments = "-npool 4"
launcher.mpi_command = "mpirun -np 4"
launcher.write()
launcher.run()
