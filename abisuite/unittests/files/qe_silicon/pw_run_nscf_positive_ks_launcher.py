from abisuite import QEPWLauncher as Launcher


# relaxed value
kpts = []
N = 4  # N^3 grid in positive part of BZ
for i in range(N):
    for j in range(N):
        for k in range(N):
            kpts.append([i/N, j/N, k/N])
w = 1 / (N ** 3)


variables = {
        "calculation": "nscf",
        "ecutwfc": 50.0,
        "nbnd": 12,
        "verbosity": "high",
        "conv_thr": 1e-12,
        "diago_full_acc": True,
        "diagonalization": "cg",
        "k_points": {
           "parameter": "crystal",
           "k_points": kpts,
           "weights": [w] * len(kpts),
           },
        }

launcher = Launcher("nscf")
launcher.command = "pw.x -npool 4"  # parallelize kpts
launcher.mpi_command = "mpirun -np 4"
launcher.workdir = "pw_run_nscf_positive_ks"
launcher.input_variables = variables
launcher.load_geometry_from("pw_run")
launcher.link_calculation("pw_run")
launcher.write()
launcher.run()
