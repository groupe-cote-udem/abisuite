#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/projwfc.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pdos_run/pdos.in
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pdos_run/pdos.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/pdos_run/pdos.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
