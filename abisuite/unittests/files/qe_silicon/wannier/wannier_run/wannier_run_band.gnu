set style data dots
set nokey
set xrange [0: 7.78597]
set yrange [ -6.75148 : 14.95823]
set arrow from  1.00027,  -6.75148 to  1.00027,  14.95823 nohead
set arrow from  1.81698,  -6.75148 to  1.81698,  14.95823 nohead
set arrow from  2.06705,  -6.75148 to  2.06705,  14.95823 nohead
set arrow from  3.31738,  -6.75148 to  3.31738,  14.95823 nohead
set arrow from  4.31765,  -6.75148 to  4.31765,  14.95823 nohead
set arrow from  4.94697,  -6.75148 to  4.94697,  14.95823 nohead
set arrow from  5.19704,  -6.75148 to  5.19704,  14.95823 nohead
set arrow from  6.01376,  -6.75148 to  6.01376,  14.95823 nohead
set arrow from  6.64308,  -6.75148 to  6.64308,  14.95823 nohead
set arrow from  6.93183,  -6.75148 to  6.93183,  14.95823 nohead
set xtics (" G "  0.00000," X "  1.00027," W "  1.81698," K "  2.06705," G "  3.31738," L "  4.31765," U "  4.94697," W "  5.19704," L "  6.01376," K "  6.64308," U "  6.93183," X "  7.78597)
 plot "../wannier_run_band.dat"
