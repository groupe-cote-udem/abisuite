from abisuite import Wannier90Launcher as Launcher

# PATH POINTS
Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]

# PATH
kpts = [Gamma, X, W, K, Gamma, L, U, W, L, K, U, X]
labels = ["G", "X", "W", "K", "G", "L", "U", "W", "L", "K", "U", "X"]
kpt_path = []
Npts = 4
for kpt1, kpt2, label1, label2 in zip(
        kpts[:-1], kpts[1:], labels[:-1], labels[1:]):
    kpt_path.append([{label1: kpt1}, {label2: kpt2}])


variables = {
        "num_bands": 16,
        "num_wann": 8,
        "num_iter": 200,
        "guiding_centres": True,
        "dis_num_iter": 1000,
        "dis_mix_ratio": 1.0,
        "conv_window": 3,
        "dis_win_max": 17,
        "dis_froz_max": 6.5,
        "projections": ["Si : sp3"],
        "mp_grid": [Npts] * 3,
        "kpoint_path": kpt_path,
        "bands_plot": True,
        }
launcher = Launcher("pp_wannier_run")
launcher.workdir = "pp_wannier_run"
launcher.command_arguments = "-pp"
launcher.input_variables = variables
launcher.link_calculation("../pw_run_nscf_positive_ks")
launcher.write()
launcher.run()
