#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/wannier90.x -pp"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pp_wannier_run/pp_wannier_run.win
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pp_wannier_run/pp_wannier_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/files/qe_silicon/wannier/pp_wannier_run/pp_wannier_run.stderr


$MPIRUN $EXECUTABLE ../pp_wannier_run.win > $LOG 2> $STDERR
