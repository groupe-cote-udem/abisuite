from abisuite import BandStructure


# PATH
k_path = [r"$\Gamma$", "X", "W", "K", r"$\Gamma$",
          "L", "U", "W", "L", "K", "W", "X"]
npts = 20
k_labels = [(lab, coord)
            for lab, coord in zip(k_path,
                                  range(0, len(k_path) * (npts + 1), npts))]

path = "bandstructure_run"
bs = BandStructure.from_calculation(path)
bs.fermi_energy = 0
bs.kpath = k_labels
plot = bs.get_plot(yunits="eV",
                   ylabel="Energy",
                   color="k",
                   symmetry="none")
plot.title = "Silicon Band Structure"
plot.plot()
