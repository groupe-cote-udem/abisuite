#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/abinit"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph7/run/ph7.files
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph7/ph7.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph7/ph7.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
