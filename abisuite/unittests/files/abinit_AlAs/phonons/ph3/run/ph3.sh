#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/Users/fgoudreault/Workspace/abinit/build/mpi/src/98_main/abinit"
INPUT=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph3/run/ph3.files
LOG=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph3/ph3.log
STDERR=/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/abinit_AlAs/phonons/ph3/ph3.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
