import os

from abisuite import AbinitLauncher as Launcher


launcher = Launcher("gamma")
launcher.workdir = os.path.join("phonons", "ph0")
launcher.input_variables = {
        "irdddk": 1,
        "kptopt": 2,
        "rfelfd": 3,
        "rfphon": 1,
        "rfatpol": [1, 2],
        "rfdir": [1, 1, 1],
        "tolvrs": 1.0e-8,
        "nstep": 25,
        "diemac": 9.0,
        "irdwfk": 1,
        }
launcher.link_wfk_from("gs")
launcher.load_geometry_from("ddk")
launcher.link_ddk_from("ddk")
launcher.write()
launcher.launch()
