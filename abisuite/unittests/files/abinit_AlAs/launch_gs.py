import os

from abisuite import AbinitLauncher as Launcher


here = "/Users/fgoudreault/Workspace/abisuite/abisuite/unittests/files/pseudos"

launcher = Launcher("gs")
launcher.workdir = "gs"
launcher.input_variables = {
        "tolvrs": 1.0e-18,
        "acell": "3*10.61",
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ecut": 3.0,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }
launcher.pseudos = [
        os.path.join(here, "13al.981214.fhi"),
        os.path.join(here, "33as.pspnc"),
        ]
launcher.mpi_command = "mpirun"
launcher.mpi_command_arguments = "-np 4"
launcher.write()
launcher.launch()
