import os
import tempfile

import aiofiles.os

import aioshutil

import pytest

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import (
        CalculationDirectory,
        )
from ...routines import is_list_like


class BaseSequencerTest:
    """Base class for all sequencers unittests."""

    _sequencer_class = None
    _sequencer_class_args = []
    _sequencer_class_kwargs = {}

    @pytest.fixture(autouse=True)
    def setup_sequencer(self):
        """Setup fixture for sequencer unittests."""
        if self._sequencer_class is None:
            raise DevError("Need to set '_sequencer_class'.")
        # create sequencer obj
        self._set_sequencer()

    async def _check(self, dir_, true, msg=""):
        isdir = await CalculationDirectory.is_calculation_directory(dir_)
        if not msg:
            msg = dir_
        if true:
            assert isdir, msg
        else:
            assert not isdir, msg

    def _set_sequencer(self):
        if hasattr(self, "sequencer"):
            return
        self.sequencer = self._sequencer_class(
                *self._sequencer_class_args,
                **self._sequencer_class_kwargs,
                )


class BaseSCFSequencerTest(BaseSequencerTest):
    """Base class for SCF sequencers test cases."""

    _scf_script_name = None
    _scf_example = None
    _scf_input_variables = None
    _pseudos = None

    @pytest.fixture(autouse=True)
    def setup_scf_sequencer(
            self, setup_sequencer, setup_scf_tempdir, setup_scf_command_file):
        """Setup fixture for scf sequencer unittests."""
        if self._scf_example is None:
            raise DevError("Need to set '_scf_example'.")
        if self._scf_script_name is None:
            raise DevError("Need to set '_scf_script_name'.")
        self.scf_workdir = os.path.join(self.scf_tempdir.name, "scf_run")
        self.scf_command = self.scf_command_file.name
        # setting SCF sequencer parameters
        self.sequencer.scf_workdir = self.scf_workdir
        self.sequencer.scf_queuing_system = "local"
        self.sequencer.scf_command = self.scf_command
        yield

    async def test_starting_from_scratch(self):
        """Test that starting from scratch works."""
        self._set_scf()
        await self.sequencer.write()
        await self._check_scf_written()
        assert not await self.sequencer.sequence_completed
        # cleanup and set done
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        assert await self.sequencer.sequence_completed

    async def _check_scf(self, true):
        await self._check(self.scf_workdir, true)

    async def _check_scf_written(self):
        await self._check_scf(True)

    async def _check_scf_not_written(self):
        await self._check_scf(False)

    def _set_scf(self):
        if self._scf_input_variables is None:
            raise DevError("Need to set '_scf_input_variables'.")
        self.sequencer.scf_input_variables = self._scf_input_variables

    async def _set_scf_done(self, **kwargs):
        await copy_calculation(
                self._scf_example, self.scf_workdir,
                new_pseudos=self._pseudos,
                **kwargs)

    @pytest.fixture
    def setup_scf_tempdir(self):
        """Setup an scf tempdir."""
        self.scf_tempdir = tempfile.TemporaryDirectory()
        yield
        self.scf_tempdir.cleanup()

    @pytest.fixture
    async def setup_scf_command_file(self):
        """Setup fixture for a scf command file."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._scf_script_name) as temp:
            self.scf_command_file = temp
            yield


class BaseRelaxationSequencerTest(BaseSCFSequencerTest):
    """Base test class for all RelaxationSequencers."""

    async def test_relax_atoms_only(self):
        """Test the atomic relaxation only."""
        self.sequencer.relax_atoms = True
        self.sequencer.relax_cell = False
        # start from scratch
        self._set_scf()
        await self.sequencer.write()
        assert len(await aiofiles.os.listdir(self.sequencer.scf_workdir)) == 1
        assert not await self.sequencer.sequence_completed
        await aioshutil.rmtree(self.sequencer.scf_workdir)
        await self._set_relax_atoms_done()
        await self.sequencer.run()
        assert self.sequencer.sequence_completed

    async def test_relax_cell_only(self):
        """Test cell relaxation only."""
        self.sequencer.relax_cell = True
        self.sequencer.relax_atoms = False
        self._set_scf()
        await self.sequencer.write()
        assert len(await aiofiles.os.listdir(self.sequencer.scf_workdir)) == 1
        assert not await self.sequencer.sequence_completed
        await aioshutil.rmtree(self.sequencer.scf_workdir)
        await self._set_relax_cell_done()
        await self.sequencer.run()
        assert await self.sequencer.sequence_completed

    async def test_relax_atoms_and_cell(self):
        """Test the cell and atomic relaxation only."""
        self.sequencer.relax_atoms = True
        self.sequencer.relax_cell = True
        self._set_scf()
        await self.sequencer.write()
        assert len(await aiofiles.os.listdir(self.sequencer.scf_workdir)) == 1
        assert not await self.sequencer.sequence_completed
        await aioshutil.rmtree(self.sequencer.scf_workdir)
        await self._set_relax_atoms_done()
        await self.sequencer.write()
        assert len(await aiofiles.os.listdir(self.sequencer.scf_workdir)) == 2
        assert not await self.sequencer.sequence_completed
        await aioshutil.rmtree(self.sequencer.scf_workdir)
        await self._set_relax_atoms_done()
        await self._set_relax_cell_done()
        await self.sequencer.run()
        assert await self.sequencer.sequence_completed

    def test_starting_from_scratch(self):
        """Test sequencer from scratch."""
        # this test is overridden by the ones above
        pass


class BaseConvergenceSequencerTest(BaseSCFSequencerTest):
    """Base test class for convergence sequencers."""

    _scf_convergence_criterion = 1

    @pytest.fixture(autouse=True)
    def setup_convergence_sequencer(self, setup_scf_sequencer):
        """Setup fixture for convergence sequencer."""
        self.sequencer.scf_convergence_criterion = (
                self._scf_convergence_criterion)
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.sequencer.scf_workdir, "plot.pdf")
        # don't save pickle as this is bugged somehow inside unittests...
        self.sequencer.plot_save_plot = False

    async def test_whole_sequence(self):
        """Test whole sequence."""
        self._set_scf()
        await self._set_scf_done()
        await self.sequencer.launch()
        await self._check_plot_written()

    async def _check_plot_written(self):
        assert await aiofiles.os.path.isfile(self.sequencer.plot_save), (
                self.sequencer.plot_save)

    async def _set_any_seq_prefix_done(
            self, prefix, root_examples=None,
            use_parameter_subdirs=True, **kwargs):
        name = self.sequencer._parameter_to_converge_input_variable_name
        prefix = prefix.strip("_")
        if root_examples is None:
            root_examples = getattr(self, f"_{prefix}_example")
        if isinstance(root_examples, str):
            root_examples = [
                    os.path.join(root_examples, x)
                    for x in await aiofiles.os.listdir(root_examples)]
        for calc in root_examples:
            basename = os.path.basename(calc)
            if basename == "results":
                continue
            if use_parameter_subdirs:
                dest = os.path.join(
                        getattr(self, f"{prefix}_workdir"), name, basename)
            else:
                dest = os.path.join(
                        getattr(self, f"{prefix}_workdir"), basename)
            await copy_calculation(
                    calc, dest, new_pseudos=self._pseudos, **kwargs)

    async def _set_scf_done(self, **kwargs):
        await self._set_any_seq_prefix_done("scf", **kwargs)

    async def _check_scf_written(self, use_parameter_subdirs=True):
        name = self.sequencer._parameter_to_converge_input_variable_name
        if use_parameter_subdirs:
            root = os.path.join(self.scf_workdir, name)
        else:
            root = self.scf_workdir
        assert len(await aiofiles.os.listdir(root)) == len(
                self.sequencer._parameters_to_converge)


class BaseEcutConvergenceSequencerTest(BaseConvergenceSequencerTest):
    """Base test class for ecut convergence sequencers."""

    _ecuts = None

    @pytest.fixture(autouse=True)
    def setup_ecut_convergence_sequencer(self, setup_convergence_sequencer):
        """Setup fixture for ecut convergence sequencers."""
        if self._ecuts is None:
            raise DevError("Need to set '_ecuts'.")
        self.sequencer.scf_ecuts = self._ecuts

    async def _check_scf_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        assert len(await aiofiles.os.listdir(
            os.path.join(self.scf_workdir, name))) == len(self._ecuts)


class BaseKgridConvergenceSequencerTest(BaseConvergenceSequencerTest):
    """Base test class for kgrid convergence sequencers."""

    _kgrids = None

    @pytest.fixture(autouse=True)
    def setup_kgrid_convergence_sequencer(
            self, setup_convergence_sequencer):
        """Setup fixture for kgrid convergence sequencers."""
        if self._kgrids is None:
            raise DevError("Need to set '_kgrids'.")
        self.sequencer.scf_kgrids = self._kgrids


class BaseSmearingConvergenceSequencerTest(BaseKgridConvergenceSequencerTest):
    """Base test class for smearing convergence sequencers."""

    _smearings = None

    @pytest.fixture(autouse=True)
    def setup_smearing_convergence_sequencer(
            self, setup_kgrid_convergence_sequencer):
        """Setup fixture for smearing convergence sequencer tests."""
        if self._smearings is None:
            raise DevError("Need to set '_smearings'.")
        self.sequencer.scf_smearings = self._smearings

    async def test_whole_sequence(self):
        """Test whole sequencer."""
        await BaseKgridConvergenceSequencerTest.test_whole_sequence(self)
        assert (await self.sequencer.scf_converged_parameter)[
                    self.sequencer.smearings_input_variable_name] in (
                self.sequencer.scf_smearings)
        assert (await self.sequencer.scf_converged_parameter)[
                    self.sequencer.kgrids_input_variable_name] in (
                self.sequencer.scf_kgrids)

    async def _check_plot_written(self):
        assert len([x for x in await aiofiles.os.listdir(self.scf_workdir)
                    if x.endswith(".pdf")]) == 3, (
                self.sequencer.plot_save)

    async def _check_scf_written(self):
        assert len(await aiofiles.os.listdir(self.scf_workdir)) == (
                len(self.sequencer.scf_smearings))
        for subdir in await aiofiles.os.listdir(self.scf_workdir):
            assert len(await aiofiles.os.listdir(
                os.path.join(self.scf_workdir, subdir))) == (
                    len(self._kgrids))

    async def _set_scf_done(self, **kwargs):
        # override this since there are calcs inside subdirs
        # rootdir/smearing_subdir/kgrid_subdir/calc
        for subdir in await aiofiles.os.listdir(self._scf_example):
            if subdir == "results":
                continue
            # this subdir contains the kgrid calcs
            for calc in await aiofiles.os.listdir(
                    os.path.join(self._scf_example, subdir)):
                await copy_calculation(
                        os.path.join(self._scf_example, subdir, calc),
                        os.path.join(self.scf_workdir, subdir, calc),
                        **kwargs)


class BasePhononSequencerTest(BaseSCFSequencerTest):
    """Base test class for sequencers with phonons."""

    _phonons_script_name = None
    _nphonons = None
    _phonons_example = None
    _phonons_input_variables = None

    @pytest.fixture(autouse=True)
    def setup_phonon_sequencer(
            self, setup_scf_sequencer, setup_phonons_tempdir,
            setup_phonons_command_file,
            ):
        """Setup fixture for phonon sequencer tests."""
        if self._phonons_example is None:
            raise DevError("Need to set '_phonons_example'.")
        if self._phonons_script_name is None:
            raise DevError("Need to set '_phonons_script_name'.")
        if is_list_like(self._phonons_example):
            # defaults to this... TODO: get rid of this (FG: 2022/02/08)
            self.phonons_workdir = os.path.join(
                    self.phonons_tempdir.name, "ph_runs", "ph_run")
        else:
            self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "ph_runs",
                os.path.basename(self._phonons_example))
        self.phonons_command = self.phonons_command_file.name
        # set sequencer object
        self.sequencer.phonons_workdir = self.phonons_workdir
        self.sequencer.phonons_command = self.phonons_command
        self.sequencer.phonons_queuing_system = "local"
        if self._nphonons is not None:
            self.sequencer.nphonons = self._nphonons
        yield

    async def test_whole_sequence(self):
        """Test whole sequencer."""
        self._set_scf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_phonons_not_written()
        assert not await self.sequencer.sequence_completed
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        # continue sequence
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await self.sequencer.sequence_completed
        # copy rest of sequence
        self.phonons_tempdir.cleanup()
        await self._set_phonons_done()
        await self.sequencer.write()
        assert await self.sequencer.sequence_completed

    async def test_starting_from_scf(self):
        """Test starting from scf part."""
        self._set_scf()
        await self._set_scf_done()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scratch(self):
        """Test starting sequencer from scratch."""
        self._set_scf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_phonons_not_written()
        assert not await self.sequencer.sequence_completed

    async def _check_phonons(self, true, exceptions=None):
        # check that there are as many phonon calcs written as there are
        # phonon calcs in the sequence.
        # exceptions: optional, list,
        #   list of eceptions strings. if a sequence.workdir contains these
        #   strings, it is ignored.
        if exceptions is not None:
            if not is_list_like(exceptions):
                exceptions = (exceptions, )
        basename = self.sequencer.phonons_workdir

        def _contain_exception(workdir):
            if exceptions is None:
                return False
            for exception in exceptions:
                if exception in workdir:
                    return True
            return False

        for calc in self.sequencer.sequence:
            if basename not in calc.workdir or _contain_exception(
                    calc.workdir):
                continue
            await self._check(calc.workdir, true)

    async def _check_phonons_written(self):
        await self._check_phonons(True)

    async def _check_phonons_not_written(self):
        await self._check_phonons(False)

    def _set_phonons(self):
        if self._phonons_input_variables is None:
            raise DevError("Need to set '_phonons_input_variables'.")
        self.sequencer.phonons_input_variables = self._phonons_input_variables

    async def _set_phonons_done(self):
        basename = os.path.basename(self._phonons_example)
        dirname = os.path.dirname(self._phonons_example)
        for calcdir in await aiofiles.os.listdir(dirname):
            if basename not in calcdir:
                continue
            ph_idx = calcdir.split(basename)[-1]
            phdir = self.sequencer.phonons_workdir + ph_idx
            await copy_calculation(
                    os.path.join(dirname, calcdir), phdir,
                    new_parents=[self.scf_workdir],
                    new_pseudos=self._pseudos)

    @pytest.fixture
    async def setup_phonons_command_file(self):
        """Phonons command file fixture."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._phonons_script_name) as temp:
            self.phonons_command_file = temp
            yield

    @pytest.fixture
    def setup_phonons_tempdir(self):
        """Setup fixture for a phonons tempdir."""
        self.phonons_tempdir = tempfile.TemporaryDirectory()
        yield
        self.phonons_tempdir.cleanup()


class BasePhononConvergenceSequencerTest(
        BaseConvergenceSequencerTest,
        BasePhononSequencerTest):
    """Base test case for phonon convergence sequencers."""

    _nphonons = 1
    _phonons_convergence_criterion = 1000000

    @pytest.fixture(autouse=True)
    def setup_phonon_convergence_sequencer(
            self,
            setup_convergence_sequencer,
            setup_phonon_sequencer):
        """Setup fixture for phonon convergence sequencer."""
        self.sequencer.phonons_qpt = [0.0, 0.0, 0.0]  # just do gamma
        self.sequencer.phonons_convergence_criterion = (
                self._phonons_convergence_criterion)

    async def test_whole_sequence(self):
        """Test whole sequencer."""
        self._set_scf()
        self._set_phonons()
        await self.sequencer.write()
        assert not await self.sequencer.sequence_completed
        await self._check_scf_written()
        await self._check_phonons_not_written()
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        assert not await self.sequencer.sequence_completed
        await self._check_phonons_written()
        self.phonons_tempdir.cleanup()
        await self._set_phonons_done()
        await self.sequencer.launch()
        await self._check_plot_written()

    async def test_starting_from_scf(self):
        """Test starting from scf part."""
        self._set_scf()
        self._set_phonons()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await self.sequencer.sequence_completed
        # set scf input variables, and restart. check its good
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await self.sequencer.sequence_completed

    async def _check_scf_written(self):
        await BaseConvergenceSequencerTest._check_scf_written(self)

    async def _check_phonons_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        assert len(await aiofiles.os.listdir(
            os.path.join(self.scf_workdir, name))) == (
                len(await aiofiles.os.listdir(
                    os.path.join(self.phonons_workdir, name))))

    async def _check_phonons_not_written(self):
        assert not await aiofiles.os.path.isdir(self.phonons_workdir)

    async def _set_phonons_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        if isinstance(self._phonons_example, str):
            ph_examples = [
                    os.path.join(self._phonons_example, x)
                    for x in os.listdir(self._phonons_example)]
        else:
            ph_examples = self._phonons_example
        for calc in ph_examples:
            basename = os.path.basename(calc)
            dest = os.path.join(self.phonons_workdir, name, basename)
            await copy_calculation(
                    calc, dest,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename)],
                    new_pseudos=self._pseudos,
                    )


class BaseEcutPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest,
        BaseEcutConvergenceSequencerTest):
    """Base test class for ecut phonons convergence sequencers."""

    _ecuts = None

    @pytest.fixture(autouse=True)
    def setup_ecut_phonon_convergence_sequencer(
            self, setup_phonon_convergence_sequencer):
        """Setup fixture for ecut phonon convergence sequencer tests."""
        if self._ecuts is None:
            raise DevError("Need to set '_ecuts'.")
        self.sequencer.scf_ecuts = self._ecuts

    async def _check_scf_written(self):
        await BaseEcutConvergenceSequencerTest._check_scf_written(self)


class BaseKgridPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest,
        BaseKgridConvergenceSequencerTest):
    """Base test class for kgrid phonon convergence sequencers."""

    _kgrids = None

    @pytest.fixture(autouse=True)
    def setup_kgrid_phonon_convergence_sequencer(
            self, setup_phonon_convergence_sequencer):
        """Setup fixture for kgrid phonon convergence sequencer tests."""
        if self._kgrids is None:
            raise DevError("Need to set '_kgrids'.")
        self.sequencer.scf_kgrids = self._kgrids

    async def _check_scf_written(self):
        await BaseKgridConvergenceSequencerTest._check_scf_written(self)


class BaseSmearingPhononConvergenceSequencerTest(
        BaseKgridPhononConvergenceSequencerTest,
        BaseSmearingConvergenceSequencerTest):
    """Base test class for smearing phonon convergence sequencers."""

    @pytest.fixture(autouse=True)
    def setup_smearing_phonon_convergence_sequencer(
            self, setup_kgrid_phonon_convergence_sequencer,
            setup_smearing_convergence_sequencer,
            ):
        """Setup fixture for smearing phonon convergence sequencer."""
        yield

    async def test_whole_sequence(self):
        """Test whole sequence."""
        await BaseKgridPhononConvergenceSequencerTest.test_whole_sequence(self)
        assert (await self.sequencer.scf_converged_parameter)[
                    self.sequencer.smearings_input_variable_name] in (
                self.sequencer.scf_smearings)
        assert (await self.sequencer.scf_converged_parameter)[
                    self.sequencer.kgrids_input_variable_name] in (
                self.sequencer.scf_kgrids)

    async def _check_phonons_written(self):
        nwritten = 0
        for subdir in await aiofiles.os.listdir(self.phonons_workdir):
            nwritten += len(
                    await aiofiles.os.listdir(
                        os.path.join(self.phonons_workdir, subdir)))
        assert nwritten == (
                len(self.sequencer.scf_smearings) *
                len(self.sequencer.scf_kgrids))

    async def _check_plot_written(self):
        # (at least) one graph for each smearings tests
        nfiles = await aiofiles.os.listdir(
                os.path.dirname(self.sequencer.plot_save))
        assert len([x for x in await aiofiles.os.listdir(self.scf_workdir)
                    if x.endswith(".pdf")]) >= (
                len(self.sequencer.scf_smearings)), (
                f"{self.sequencer.plot_save}: "
                f"{nfiles}"
                )

    async def _check_scf_written(self):
        assert len(await aiofiles.os.listdir(self.scf_workdir)) == (
                len(self.sequencer.scf_smearings))
        for subdir in await aiofiles.os.listdir(self.scf_workdir):
            assert len(await aiofiles.os.listdir(
                os.path.join(self.scf_workdir, subdir))) == (
                    len(self._kgrids))

    async def _set_phonons_done(self, **kwargs):
        for subdir in await aiofiles.os.listdir(self._phonons_example):
            for calc in await aiofiles.os.listdir(
                    os.path.join(self._phonons_example, subdir)):
                dest = os.path.join(
                        self.phonons_workdir, subdir, calc)
                await copy_calculation(
                    os.path.join(self._phonons_example, subdir, calc),
                    dest,
                    new_parents=[
                        os.path.join(self.scf_workdir, subdir, calc)],
                    **kwargs
                    )

    async def _set_scf_done(self, **kwargs):
        await BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, **kwargs)


class BaseBandStructureSequencerTest(BaseSCFSequencerTest):
    """Base class for a sequencer that creates a band structure plot."""

    _band_structure_example = None
    _band_structure_input_variables = None
    _band_structure_kpoint_path = None
    _band_structure_kpoint_path_density = None

    @pytest.fixture(autouse=True)
    def setup_band_structure_sequencer(
            self, setup_scf_sequencer, setup_band_structure_tempdir):
        """Setup fixture for band structure sequencer tests."""
        if self._band_structure_example is None:
            raise DevError("Need to set '_band_structure_example'.")
        if self._band_structure_input_variables is None:
            raise DevError("Need to set '_band_structure_input_variables'.")
        if self._band_structure_kpoint_path is None:
            raise DevError("Need to set '_band_structure_kpoint_path'.")
        if self._band_structure_kpoint_path_density is None:
            raise DevError("Need to set _band_structure_kpoint_path_density'.")
        self.band_structure_workdir = os.path.join(
                self.band_structure_tempdir.name, "band_structure_run")
        self.sequencer.band_structure_workdir = self.band_structure_workdir
        self.sequencer.band_structure_queuing_system = "local"
        self.sequencer.band_structure_command = self.scf_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = None

    async def test_starting_from_scratch(self):
        """Test that starting from scratch works."""
        self._set_scf()
        self._set_band_structure()
        await self.sequencer.write()
        await self._check_scf_written()
        assert not await self.sequencer.sequence_completed
        # cleanup and set done
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_band_structure_written()
        assert self.sequencer.sequence_completed
        # check everything done
        self.band_structure_tempdir.cleanup()
        await self._set_band_structure_done()
        await self.sequencer.launch()
        assert await self.sequencer.sequence_completed

    async def _check_band_structure(self, true):
        check = await CalculationDirectory.is_calculation_directory(
            self.band_structure_workdir)
        if true:
            assert check
        else:
            assert not check

    async def _check_band_structure_written(self):
        await self._check_band_structure(True)

    async def _check_band_structure_not_written(self):
        await self._check_band_structure(False)

    def _set_band_structure(self):
        self.sequencer.band_structure_kpoint_path = (
                self._band_structure_kpoint_path
                )
        self.sequencer.band_structure_kpoint_path_density = (
                self._band_structure_kpoint_path_density)
        self.sequencer.band_structure_input_variables = (
                self._band_structure_input_variables)

    async def _set_band_structure_done(self):
        await copy_calculation(
                self._band_structure_example,
                self.band_structure_workdir,
                new_parents=[self.scf_workdir],
                new_pseudos=self._pseudos)

    @pytest.fixture
    def setup_band_structure_tempdir(self):
        """Setup fixture for a band structure tempdir."""
        self.band_structure_tempdir = tempfile.TemporaryDirectory()
        yield
        self.band_structure_tempdir.cleanup()


class BaseNSCFSequencerTest(BaseSCFSequencerTest):
    """Base class for calculations requiring a NSCF part."""

    _nscf_example = None
    _nscf_input_variables = None

    @pytest.fixture(autouse=True)
    def setup_nscf_sequencer(self, setup_scf_sequencer, setup_nscf_tempdir):
        """Setup fixture for nscf sequencer tests."""
        if self._nscf_example is None:
            raise DevError("Need to set '_nscf_example'.")
        if self._nscf_input_variables is None:
            raise DevError("Need to set '_nscf_input_variables'.")
        self.nscf_workdir = os.path.join(self.nscf_tempdir.name, "nscf_run")
        self.sequencer.nscf_workdir = self.nscf_workdir
        self.sequencer.nscf_queuing_system = "local"
        self.sequencer.nscf_command = self.scf_command

    async def _check_nscf(self, true):
        await self._check(self.nscf_workdir, true)

    async def _check_nscf_written(self):
        await self._check_nscf(True)

    async def _check_nscf_not_written(self):
        await self._check_nscf(False)

    def _set_nscf(self):
        self.sequencer.nscf_input_variables = self._nscf_input_variables

    async def _set_nscf_done(self, **kwargs):
        await copy_calculation(
                self._nscf_example, self.nscf_workdir,
                new_parents=[self.scf_workdir],
                new_pseudos=self._pseudos,
                **kwargs)

    @pytest.fixture
    def setup_nscf_tempdir(self):
        """Setup fixture for a nscf tempdir."""
        self.nscf_tempdir = tempfile.TemporaryDirectory()
        yield
        self.nscf_tempdir.cleanup()


class BasePositiveKpointsNSCFSequencerTest(BaseNSCFSequencerTest):
    """Base class for a calcs that requires a scf and a pos. kpts nscf run."""

    _nscf_kgrid = None

    @pytest.fixture(autouse=True)
    def setup_positive_kpoints_nscf_sequencer(
            self, setup_nscf_sequencer):
        """Setup fixture for positive kpts nscf sequencers tests."""
        if self._nscf_kgrid is None:
            raise DevError("Need to set '_nscf_kgrid'.")

    def _set_nscf(self):
        self.sequencer.nscf_kgrid = self._nscf_kgrid
        BaseNSCFSequencerTest._set_nscf(self)


class BaseFermiSurfaceSequencerTest(BasePositiveKpointsNSCFSequencerTest):
    """Base class for fermi surface sequencers."""

    @pytest.fixture(autouse=True)
    def setup_fermi_surface_sequencer(
            self, setup_positive_kpoints_nscf_sequencer):
        """Setup fixture for fermi surface sequencer tests."""
        self.sequencer.plot_show = False

    async def test_starting_from_scratch(self):
        """Test that starting from scratch works."""
        self._set_scf()
        self._set_nscf()
        await self.sequencer.write()
        await self._check_scf_written()
        assert not await self.sequencer.sequence_completed
        # cleanup and set done
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        self.nscf_tempdir.cleanup()
        await self._set_nscf_done()
        await self.sequencer.clear_sequence()
        assert await self.sequencer.sequence_completed


class BaseDOSSequencerTest(BaseSCFSequencerTest):
    """Base class for dos sequencers tests."""

    _dos_example = None
    _dos_fine_kpoint_grid_variables = None
    _dos_input_variables = None

    @pytest.fixture(autouse=True)
    def setup_dos_sequencer(
            self, setup_scf_sequencer, setup_dos_tempdir):
        """Setup fixtures for dos sequencer unittests."""
        if self._dos_example is None:
            raise DevError("Need to set '_dos_example'.")
        if self._dos_fine_kpoint_grid_variables is None:
            raise DevError(
                    "Need to set '_dos_fine_kpoint_grid_variables'.")
        if self._dos_input_variables is None:
            raise DevError("Need to set '_dos_input_variables'.")
        self.dos_workdir = os.path.join(self.dos_tempdir.name, "dos_run")
        self.sequencer.dos_workdir = self.dos_workdir
        self.sequencer.dos_queuing_system = "local"
        self.sequencer.plot_show = False

    @pytest.fixture
    def setup_dos_tempdir(self):
        """Setup fixture for dos tempdir."""
        self.dos_tempdir = tempfile.TemporaryDirectory()
        yield
        self.dos_tempdir.cleanup()

    async def _set_dos_done(self, **kwargs):
        await copy_calculation(
                self._dos_example,
                self.dos_workdir,
                **kwargs)

    def _set_dos(self):
        self.sequencer.dos_fine_kpoint_grid_variables = (
                self._dos_fine_kpoint_grid_variables)
        self.sequencer.dos_input_variables = self._dos_input_variables

    async def _check_dos(self, true):
        await self._check(self.dos_workdir, true)

    async def _check_dos_written(self):
        return await self._check_dos(True)

    async def _check_dos_not_written(self):
        return await self._check_dos(False)
