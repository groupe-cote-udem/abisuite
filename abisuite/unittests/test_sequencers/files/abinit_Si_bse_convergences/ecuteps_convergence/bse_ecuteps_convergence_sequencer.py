from abisuite import AbinitBSEecutepsConvergenceSequencer, EV_TO_HA


sequencer = AbinitBSEecutepsConvergenceSequencer()
sequencer.scf_workdir = "gs_run"
sequencer.scf_input_variables = {
        "nband": 6,
        "tolvrs": 1e-8,
        "prtden": 1,
        "acell": [10.217] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 1,
        "shiftk": [[0.0, 0.0, 0.0]],
        "istwfk": "*1",
        "ecut": 12.0,
        "diemac": 12.0,
        "nstep": 50,
        }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_pseudos = "Si.scalar-lda-nc.psp8"

sequencer.nscf_workdir = "nscf_run"
sequencer.nscf_input_variables = {
        "nband": 105,
        "nbdbuf": 5,
        "tolwfr": 1e-8,
        "iscf": -2,
        "ngkpt": [4, 4, 4],
        "nshiftk": 1,
        "shiftk": [[0.0, 0.0, 0.0]],
        "istwfk": "*1",
        "diemac": 12.0,
        }
sequencer.nscf_mpi_command = "mpirun -np 4"

sequencer.nscfnosym_workdir = "nscf_nosym_run"
sequencer.nscfnosym_input_variables = {
        "iscf": -2,
        "nband": 15,
        "nbdbuf": 5,
        "tolwfr": 1e-8,
        "ngkpt": [4, 4, 4],
        "chksymbreak": 0,
        "nshiftk": 1,
        "shiftk": [[0.11, 0.21, 0.31]],
        "istwfk": "*1",
        "ecut": 8.0,
        "diemac": 12.0,
        }
sequencer.nscfnosym_mpi_command = "mpirun -np 4"

sequencer.screening_workdir = "screening_run"
sequencer.screening_input_variables = {
        "gwpara": 2,
        "inclvkb": 2,
        "awtr": 1,
        "symchi": 1,
        "nband": 100,
        "ecuteps": 6.0,
        "ecutwfn": 12.0,
        "nfreqre": 1,
        "nfreqim": 90,
        "istwfk": "*1",
        "diemac": 12.0,
        }
sequencer.screening_mpi_command = "mpirun -np 4"

sequencer.bse_workdir = "bse_run"
sequencer.bse_ecutepss = [1, 2, 3, 4]
sequencer.bse_input_variables = {
        "bs_loband": 2, "nband": 7,
        "bs_calctype": 1,
        "mbpt_sciss": 0.8 * EV_TO_HA,
        "bs_exchange_term": 1,
        "bs_coulomb_term": 11,
        "bs_coupling": 0,
        "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
        "bs_algorithm": 2,
        "bs_haydock_niter": 200,
        "bs_haydock_tol": [0.05, 0],
        "zcut": 0.15 * EV_TO_HA,
        "kptopt": 1,
        "chksymbreak": 0,
        "ecutwfn": 12.0,
        "inclvkb": 2,
        "gw_icutcoul": 3,
        }
sequencer.bse_mpi_command = "mpirun -np 4"
sequencer.run()
