from abisuite import AbinitGWSequencer, EV_TO_HA


sequencer = AbinitGWSequencer()
sequencer.scf_workdir = "gs_run"
sequencer.scf_input_variables = {
        "nband": 6,
        "tolvrs": 1e-10,
        "acell": [10.26] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                   [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ecut": 8.0,
        "diemac": 12.0,
        }
sequencer.scf_mpi_command = "mpirun -np 4"
sequencer.scf_pseudos = "Si.scalar-lda-nc.psp8"

sequencer.nscf_workdir = "nscf_run"
sequencer.nscf_input_variables = {
        "nband": 120,
        "tolwfr": 1e-12,
        "nbdbuf": 20,
        # "iscf": -2,
        # "acell": [10.26] * 3,
        # "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        # "ntypat": 1,
        # "znucl": [14],
        # "natom": 2,
        # "typat": [1, 1],
        # "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                   [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        # "istwfk": "*1",
        "ecut": 8.0,
        "diemac": 12.0,
        }
sequencer.nscf_mpi_command = "mpirun -np 4"

sequencer.screening_workdir = "screening_run"
sequencer.screening_input_variables = {
        "nband": 50,
        "ecuteps": 6.0,
        "ppmfrq": 16.7 * EV_TO_HA,
        }
sequencer.screening_mpi_command = "mpirun -np 4"

sequencer.selfenergy_workdir = "self_energy_run"
sequencer.selfenergy_input_variables = {
        # "optdriver": 4,
        "nband": 100,
        "ecutsigx": 8.0,
        "nkptgw": 1,
        "kptgw": [0.0, 0.0, 0.0],
        "bdgw": [4, 5],
        }
sequencer.selfenergy_mpi_command = "mpirun -np 4"
sequencer.run()
