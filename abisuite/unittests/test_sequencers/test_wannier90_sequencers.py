import os
import sys
import tempfile

import aiofiles.os

import aioshutil

import pytest

from .test_qe_sequencers import (
        BaseQEBandStructureSequencerTest,
        BaseQEPositiveKpointsNSCFSequencerTest,
        )
from ..routines_for_tests import copy_calculation
from ...handlers import (
    MetaDataFile,
    )
from ...sequencers import Wannier90InterpolationSequencer as Sequencer


here = os.path.dirname(os.path.abspath(__file__))
SCF_PSEUDO_DIR = os.path.join(here, "files", "qe_silicon")

SCF_EXAMPLE = os.path.join(here, "files", "qe_silicon", "scf_run")
SCF_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 35.0,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085,
                        }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
NSCF_EXAMPLE = os.path.join(here, "files", "qe_silicon", "nscf_run")
NSCF_INPUT_VARIABLES = {
    "ecutwfc": 35.0,
    "conv_thr": 1e-12,
    "nbnd": 12,
    "diagonalization": "cg",
    "pseudo_dir": SCF_PSEUDO_DIR,
    "diago_full_acc": True,
    "verbosity": "high",
    }
NSCF_KGRID = [4] * 3
BAND_STRUCTURE_KPOINT_PATH = [
    {"L": [0.0, 0.5, 0.0]},
    {r"$\Gamma$": [0.0, 0.0, 0.0]},
    {"X": [0.0, 0.5, 0.5]},
    {"U": [0.0, 0.625, 0.375]},
    {"K": [-0.375, 0.375, 0.0]},
    {r"$\Gamma$": [0.0, 0.0, 0.0]},
    ]
BAND_STRUCTURE_KPOINT_PATH_DENSITY = 20
BAND_STRUCTURE_INPUT_VARIABLES = NSCF_INPUT_VARIABLES.copy()
BAND_STRUCTURE_EXAMPLE = os.path.join(
        here, "files", "qe_silicon", "bandstructure_run")
WANNIER90_PP_EXAMPLE = os.path.join(
        here, "files", "qe_silicon", "wannier_run_pp")
WANNIER90_INPUT_VARIABLES = {
    "num_wann": 8,
    "bands_plot": True,
    "dis_win_max": 17.5,
    "dis_froz_max": 6.9,
    "dis_num_iter": 1000,
    "num_iter": 2000,
    "num_print_cycles": 20,
    "conv_window": 3,
    "projections": ["Si:sp3"],
    }

WANNIER90_EXAMPLE = os.path.join(here, "files", "qe_silicon", "wannier_run")
PW2WANNIER90_EXAMPLE = os.path.join(
        here, "files", "qe_silicon", "pw2wannier90_run")
PW2WANNIER90_INPUT_VARIABLES = {
    "spin_component": "none",
    }


@pytest.mark.order("first")
class TestWannier90InterpolationSequencer(
        BaseQEPositiveKpointsNSCFSequencerTest,
        BaseQEBandStructureSequencerTest,
        ):
    """Test case for the Wannier90 Interpolation sequencer.
    """
    _band_structure_example = BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = BAND_STRUCTURE_INPUT_VARIABLES
    _band_structure_kpoint_path = BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = BAND_STRUCTURE_KPOINT_PATH_DENSITY
    _nscf_example = NSCF_EXAMPLE
    _nscf_kgrid = NSCF_KGRID
    _nscf_input_variables = NSCF_INPUT_VARIABLES
    _scf_example = SCF_EXAMPLE
    _scf_input_variables = SCF_INPUT_VARIABLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = Sequencer
    _sequencer_class_args = ["qe"]

    @pytest.fixture(autouse=True)
    def setup_wannier90_interpolation_sequencer(
            self, setup_positive_kpoints_nscf_sequencer,
            setup_band_structure_sequencer,
            setup_wannier90_tempdir,
            setup_pw2wannier90_tempdir,
            temp_pw2wannier90x_script,
            temp_wannier90x_script,
            ):
        # setting up temporary directories
        self.wannier90_workdir = os.path.join(
                self.wannier90_tempdir.name, "wannier90_run")
        self.wannier90_pp_workdir = os.path.join(
                self.wannier90_tempdir.name, "wannier90_run_pp")
        self.pw2wannier90_workdir = os.path.join(
                self.pw2wannier90_tempdir.name, "pw2wannier90_run")
        self.pw2wannier90x_command_file = temp_pw2wannier90x_script
        self.pw2wannier90x_command = self.pw2wannier90x_command_file.name
        self.wannier90x_command_file = temp_wannier90x_script
        self.wannier90x_command = self.wannier90x_command_file.name
        # setting up sequencer object
        self.sequencer.wannier90_workdir = self.wannier90_workdir
        self.sequencer.wannier90_jobname = "wannier_run"
        self.sequencer.wannier90_queuing_system = "local"
        self.sequencer.pw2wannier90_workdir = self.pw2wannier90_workdir
        self.sequencer.wannier90_queuing_system = "local"
        self.sequencer.pw2wannier90_queuing_system = "local"
        self.sequencer.band_structure_command = self.scf_command
        self.sequencer.wannier90_command = self.wannier90x_command
        self.sequencer.pw2wannier90_command = self.pw2wannier90x_command

    @pytest.fixture
    def setup_pw2wannier90_tempdir(self):
        self.pw2wannier90_tempdir = tempfile.TemporaryDirectory()
        with self.pw2wannier90_tempdir:
            yield

    @pytest.fixture
    def setup_wannier90_tempdir(self):
        self.wannier90_tempdir = tempfile.TemporaryDirectory()
        with self.wannier90_tempdir:
            yield

    @pytest.mark.skipif(
        sys.platform == "win32",
        reason="Fails on windows not sure why...")
    async def test_whole_sequence(self):
        # create sequencer in full and run everything
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        self._set_pw2wannier90()
        # start sequence
        await self.sequencer.write()
        # check scf calculation is written but nothing else
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_band_structure_not_written()
        await self._check_wannier90_pp_not_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # erase directory and replace it with real one and continue
        await aioshutil.rmtree(self.sequencer.scf_workdir)
        await self._set_scf_done()
        # continue sequence
        await self.sequencer.write()
        # check that nscf and bs calculations are written but nothing else
        await self._check_nscf_written()
        await self._check_band_structure_written()
        await self._check_wannier90_pp_not_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # erase and replace with actual calculations
        await aioshutil.rmtree(self.sequencer.nscf_workdir)
        await aioshutil.rmtree(self.sequencer.band_structure_workdir)
        await self._set_nscf_done()
        await self._set_band_structure_done()
        # continue sequence
        await self.sequencer.write()
        # check that wannier90_pp has been written
        await self._check_wannier90_pp_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # replace and continue
        await aioshutil.rmtree(self.sequencer.wannier90_pp_workdir)
        await self._set_wannier90_pp_done()
        await self.sequencer.write()
        # check pw2wannier90 has been writen
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_written()
        assert not await self.sequencer.sequence_completed
        # replace and continue
        await aioshutil.rmtree(self.sequencer.pw2wannier90_workdir)
        await self._set_pw2wannier90_done()
        await self.sequencer.write()
        # check wannier90 has been writen
        await self._check_wannier90_written()
        assert not await self.sequencer.sequence_completed
        # replace and continue
        await aioshutil.rmtree(self.sequencer.wannier90_workdir)
        await self._set_wannier90_done()
        # nothing else to run, check that sequence is completed
        await self.sequencer.write()
        assert await self.sequencer.sequence_completed

    async def test_starting_from_pw2wannier90_with_bs(self):
        # start after pw2wannier90 run and band structure
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_wannier90_pp_done()
        await self._set_band_structure_done()
        await self._set_pw2wannier90_done()
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        # check that everything works
        # check wannier90 calculation have been written
        await self.sequencer.write()
        await self._check_wannier90_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding all input variables
        await aioshutil.rmtree(self.sequencer.wannier90_workdir)
        self._set_scf()
        self._set_pw2wannier90()
        await self.sequencer.write()
        await self._check_wannier90_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_pw2wannier90_no_bs(self):
        # start after pw2wannier90 run and no band structure
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_wannier90_pp_done()
        await self._set_pw2wannier90_done()
        # check that everything works
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        await self.sequencer.write()
        # check wannier90 calculation have been written
        await self._check_wannier90_written()
        await self._check_band_structure_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding all input variables
        await aioshutil.rmtree(self.sequencer.wannier90_workdir)
        self.band_structure_tempdir.cleanup()
        self._set_scf()
        self._set_pw2wannier90()
        await self.sequencer.write()
        await self._check_wannier90_written()
        await self._check_band_structure_written()
        assert not await self.sequencer.sequence_completed

    @pytest.mark.skipif(
        sys.platform == "win32",
        reason="Fails on windows not sure why...")
    async def test_starting_from_wannier90_pp_with_bs(self):
        # start after wannier90 pp run and band structure
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_band_structure_done()
        await self._set_wannier90_pp_done()
        # check that everything works
        self._set_pw2wannier90()
        await self.sequencer.write()
        # check pw2wannier90 calculation have been written
        await self._check_pw2wannier90_written()
        await self._check_wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding nscf input variables
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        await self.sequencer.write()
        await self._check_pw2wannier90_written()
        await self._check_wannier90_not_written()
        assert not await self.sequencer.sequence_completed

    @pytest.mark.skipif(
        sys.platform == "win32",
        reason="Fails on windows not sure why...")
    async def test_starting_from_wannier90_pp_run_no_bs(self):
        # start after wannier90 pp run but band structure not run yet
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_wannier90_pp_done()
        # check that everything works
        self._set_pw2wannier90()
        self._set_band_structure()
        await self.sequencer.write()
        await self._check_pw2wannier90_written()
        await self._check_wannier90_not_written()
        await self._check_band_structure_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding nscf input variables
        self.band_structure_tempdir.cleanup()
        self.pw2wannier90_tempdir.cleanup()
        self._set_scf()
        self._set_nscf()
        self._set_wannier90()
        await self.sequencer.write()
        await self._check_pw2wannier90_written()
        await self._check_wannier90_not_written()
        await self._check_band_structure_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_nscf_plus_bandstructure(self):
        # restart the sequencer after running the NSCF calculations
        # and the bandstructure calculation
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_band_structure_done()
        # check that everything works
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        await self.sequencer.write()
        # check wannier90 pp calculation have been written
        await self._check_wannier90_pp_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding nscf input variables
        await aioshutil.rmtree(self.sequencer.wannier90_pp_workdir)
        self._set_scf()
        await self.sequencer.write()
        await self._check_wannier90_pp_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_nscf_calculation(self):
        # restart the sequencer after running the NSCF calculations
        await self._set_scf_done()
        await self._set_nscf_done()
        # check that everything works
        self._set_nscf()
        self._set_band_structure()
        self._set_wannier90()
        await self.sequencer.write()
        # check bs and wannier90 pp calculation have been written
        await self._check_band_structure_written()
        await self._check_wannier90_pp_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding nscf input variables
        self.band_structure_tempdir.cleanup()
        await aioshutil.rmtree(self.sequencer.wannier90_pp_workdir)
        self._set_scf()
        await self.sequencer.write()
        await self._check_band_structure_written()
        await self._check_wannier90_pp_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_calculation(self):
        # restart the sequencer after running the SCF calculation
        await self._set_scf_done()
        self._set_nscf()
        self._set_band_structure()
        await self.sequencer.write()
        # check nscf calculation has been written and nothing else
        await self._check_nscf_written()
        await self._check_band_structure_written()
        await self._check_wannier90_pp_not_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed
        # redo everything after adding scf input variables
        self.nscf_tempdir.cleanup()
        self.band_structure_tempdir.cleanup()
        self._set_scf()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_band_structure_written()
        await self._check_wannier90_pp_not_written()
        await self._check_wannier90_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_nscf()
        await self.sequencer.write()
        # check that there is only one calculation that is written
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_band_structure_not_written()
        await self._check_wannier90_not_written()
        await self._check_wannier90_pp_not_written()
        await self._check_pw2wannier90_not_written()
        assert not await self.sequencer.sequence_completed

    async def _check_wannier90(self, true):
        await self._check(self.wannier90_workdir, true)

    async def _check_wannier90_written(self):
        await self._check_wannier90(True)

    async def _check_wannier90_not_written(self):
        await self._check_wannier90(False)

    async def _check_wannier90_pp(self, true):
        await self._check(self.wannier90_pp_workdir, true)

    async def _check_wannier90_pp_written(self):
        await self._check_wannier90_pp(True)

    async def _check_wannier90_pp_not_written(self):
        await self._check_wannier90_pp(False)

    async def _check_pw2wannier90(self, true):
        await self._check(self.pw2wannier90_workdir, true)

    async def _check_pw2wannier90_written(self):
        await self._check_pw2wannier90(True)

    async def _check_pw2wannier90_not_written(self):
        await self._check_pw2wannier90(False)

    def _set_pw2wannier90(self):
        self.sequencer.pw2wannier90_input_variables = (
                PW2WANNIER90_INPUT_VARIABLES)

    async def _set_pw2wannier90_done(self):
        await copy_calculation(PW2WANNIER90_EXAMPLE, self.pw2wannier90_workdir)
        async with await MetaDataFile.from_calculation(
                self.pw2wannier90_workdir) as meta:
            meta.parents = [self.nscf_workdir, self.wannier90_pp_workdir]

    def _set_wannier90(self):
        self.sequencer.wannier90_input_variables = WANNIER90_INPUT_VARIABLES

    async def _set_wannier90_done(self):
        await copy_calculation(
                WANNIER90_EXAMPLE, self.sequencer.wannier90_workdir)
        self.sequencer._logger.debug(
                await aiofiles.os.listdir(self.sequencer.wannier90_workdir))
        async with await MetaDataFile.from_calculation(
                self.sequencer.wannier90_workdir) as meta:
            meta.parents = [self.pw2wannier90_workdir,
                            self.wannier90_pp_workdir]
        self.sequencer._logger.debug(await meta.str())
        # need to create empty directory 'input_data' for some reason it is not
        # copied...
        idd = os.path.join(self.sequencer.wannier90_workdir, "input_data")
        if not await aiofiles.os.path.exists(idd):
            await aiofiles.os.mkdir(idd)

    async def _set_wannier90_pp_done(self):
        # need to modify the meta file of wannier90 pp
        await copy_calculation(WANNIER90_PP_EXAMPLE, self.wannier90_pp_workdir)
        async with await MetaDataFile.from_calculation(
                self.wannier90_pp_workdir) as meta:
            meta.parents = [self.nscf_workdir]
