import asyncio
import os
import tempfile
import unittest

import aiofiles.os

import aioshutil

import pytest

from .bases import (
        BaseBandStructureSequencerTest,
        BaseConvergenceSequencerTest,
        BaseDOSSequencerTest,
        BaseEcutConvergenceSequencerTest,
        BaseEcutPhononConvergenceSequencerTest,
        BaseFermiSurfaceSequencerTest,
        BaseKgridConvergenceSequencerTest,
        BaseKgridPhononConvergenceSequencerTest,
        BaseNSCFSequencerTest,
        BasePhononConvergenceSequencerTest,
        BasePhononSequencerTest,
        BaseRelaxationSequencerTest,
        BaseSCFSequencerTest,
        BaseSmearingConvergenceSequencerTest,
        BaseSmearingPhononConvergenceSequencerTest,
        )
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        abinit_band_structure_example,
        abinit_band_structure_input_variables,
        abinit_band_structure_kpoint_path,
        abinit_band_structure_kpoint_path_density,
        abinit_bse_ecuteps_convergence_bse_input_variables,
        abinit_bse_ecuteps_convergence_example,
        abinit_bse_example,
        abinit_bse_kgrid_convergence_example,
        abinit_bse_kgrid_convergence_input_variable_name,
        abinit_bse_nband_convergence_example,
        abinit_bse_nscf_input_variables,
        abinit_bse_nscfnosym_input_variables,
        abinit_bse_scf_input_variables,
        abinit_bse_screening_input_variables,
        abinit_dos_example,
        abinit_dos_fine_kpoint_grid_variables,
        abinit_dos_input_variables,
        abinit_fermi_surface_nscf_example,
        abinit_fermi_surface_nscf_input_variables,
        abinit_fermi_surface_nscf_kgrid,
        abinit_fermi_surface_pseudos,
        abinit_fermi_surface_scf_example,
        abinit_fermi_surface_scf_input_variables,
        abinit_gs_ecut_convergence_ecuts,
        abinit_gs_ecut_convergence_example,
        abinit_gs_ecut_convergence_input_variables,
        abinit_gs_example, abinit_gs_input_variables,
        abinit_gs_kgrid_convergence_example,
        abinit_gs_kgrid_convergence_scf_input_variables,
        abinit_gs_kgrid_convergence_scf_kgrids,
        abinit_gs_pseudos,
        abinit_gs_smearing_convergence_criterion,
        abinit_gs_smearing_convergence_example,
        abinit_gs_smearing_convergence_kgrids,
        abinit_gs_smearing_convergence_pseudos,
        abinit_gs_smearing_convergence_scf_input_variables,
        abinit_gs_smearing_convergence_smearings,
        abinit_gs_smearing_convergence_varname,
        abinit_gw_ecuteps_convergence_example,
        abinit_gw_example,
        abinit_gw_kgrid_convergence_example,
        abinit_gw_kgrid_convergence_nscf_input_variables,
        abinit_gw_kgrid_convergence_scf_input_variables,
        abinit_gw_kgrid_convergence_screening_input_variables,
        abinit_gw_kgrid_convergence_selfenergy_input_variables,
        abinit_gw_nscf_input_variables,
        abinit_gw_scf_input_variables,
        abinit_gw_screening_input_variables,
        abinit_gw_screening_nband_convergence_example,
        abinit_gw_screening_nband_convergence_screening_input_variables,
        abinit_gw_screening_nband_convergence_selfenergy_input_variables,
        abinit_gw_self_energy_nband_convergence_example,
        abinit_gw_self_energy_nband_convergence_screening_input_variables,
        abinit_gw_self_energy_nband_convergence_selfenergy_input_variables,
        abinit_gw_selfenergy_input_variables,
        abinit_optic_ddk_input_variables,
        abinit_optic_example,
        abinit_optic_input_variables,
        abinit_optic_linear_only_example,
        abinit_optic_linear_only_optic_input_variables,
        abinit_optic_nscf_input_variables,
        abinit_optic_pseudos,
        abinit_optic_scf_input_variables,
        abinit_phonon_dispersion_anaddb_input_variables,
        abinit_phonon_dispersion_compute_electric_field_response,
        abinit_phonon_dispersion_ddk_input_variables,
        abinit_phonon_dispersion_example,
        abinit_phonon_dispersion_noddk_example,
        abinit_phonon_dispersion_phonons_input_variables,
        abinit_phonon_dispersion_pseudos,
        abinit_phonon_dispersion_qgrid_generation_input_variable_name,
        abinit_phonon_dispersion_qpoint_grid,
        abinit_phonon_dispersion_qpoint_grid_list,
        abinit_phonon_dispersion_qpoint_path,
        abinit_phonon_dispersion_qpoint_path_density,
        abinit_phonon_dispersion_qpoints_split_by_irreps,
        abinit_phonon_dispersion_scf_input_variables,
        abinit_phonon_ecut_convergence_ddk_input_variables,
        abinit_phonon_ecut_convergence_ecuts,
        abinit_phonon_ecut_convergence_ecuts_ddk,
        abinit_phonon_ecut_convergence_example,
        abinit_phonon_ecut_convergence_example_ddk,
        abinit_phonon_ecut_convergence_phonons_input_variables,
        abinit_phonon_ecut_convergence_phonons_input_variables_ddk,
        abinit_phonon_ecut_convergence_pseudos,
        abinit_phonon_ecut_convergence_pseudos_ddk,
        abinit_phonon_ecut_convergence_scf_input_variables,
        abinit_phonon_ecut_convergence_scf_input_variables_ddk,
        abinit_phonon_kgrid_convergence_ddk_input_variables,
        abinit_phonon_kgrid_convergence_example,
        abinit_phonon_kgrid_convergence_example_no_ddk,
        abinit_phonon_kgrid_convergence_kgrid_input_variable_name,
        abinit_phonon_kgrid_convergence_phonons_input_variables,
        abinit_phonon_kgrid_convergence_phonons_qpt,
        abinit_phonon_kgrid_convergence_phonons_qpt_no_ddk,
        abinit_phonon_kgrid_convergence_pseudos,
        abinit_phonon_kgrid_convergence_scf_input_variables,
        abinit_phonon_kgrid_convergence_scf_kgrids,
        abinit_phonons_smearing_convergence_example,
        abinit_phonons_smearing_convergence_phonons_input_variables,
        abinit_phonons_smearing_convergence_scf_input_variables,
        abinit_relaxation_example,
        abinit_relaxation_scf_input_variables,
        )
from ...constants import EV_TO_HA
from ...exceptions import DevError
from ...sequencers import (
        AbinitBSEKgridConvergenceSequencer,
        AbinitBSESequencer,
        AbinitBSEecutepsConvergenceSequencer,
        AbinitBSEnbandConvergenceSequencer,
        AbinitBandStructureComparatorSequencer,
        AbinitBandStructureSequencer,
        AbinitDOSSequencer,
        AbinitEcutConvergenceSequencer, AbinitEcutPhononConvergenceSequencer,
        AbinitFermiSurfaceSequencer,
        AbinitGWKgridConvergenceSequencer,
        AbinitGWScreeningnbandConvergenceSequencer,
        AbinitGWSelfEnergynbandConvergenceSequencer,
        AbinitGWSequencer,
        AbinitGWecutepsConvergenceSequencer,
        AbinitIndividualPhononSequencer,
        AbinitKgridConvergenceSequencer, AbinitKgridPhononConvergenceSequencer,
        AbinitOpticSequencer,
        AbinitPhononDispersionSequencer,
        AbinitRelaxationSequencer,
        AbinitSmearingConvergenceSequencer,
        AbinitSmearingPhononConvergenceSequencer,
        )


here = os.path.dirname(os.path.abspath(__file__))


# #############################################################################
# ###################### BASE CLASSES #########################################
# #############################################################################
class BaseAbinitSCFSequencerTest(BaseSCFSequencerTest):
    """Base class for SCF sequencer test cases with abinit.
    """
    _scf_script_name = "abinit"
    _pseudos = None

    @pytest.fixture(autouse=True)
    def setup_abinit_scf_sequencer(
            self, setup_scf_sequencer):
        if self._pseudos is None:
            raise DevError(
                    f"Need to set '_pseudos' in '{self.__class__}'.")
        self.sequencer.scf_pseudos = self._pseudos


class BaseAbinitNSCFSequencerTest(
        BaseAbinitSCFSequencerTest, BaseNSCFSequencerTest):
    """Base class for abinit calculations that require a nscf run."""

    @pytest.fixture(autouse=True)
    def setup_abinit_nscf_sequencer(
            self, setup_abinit_scf_sequencer,
            setup_nscf_sequencer,
            ):
        yield


# #############################################################################
# ################# Relaxation Sequencer Test #################################
# #############################################################################


@pytest.mark.order("first")
class TestAbinitRelaxationSequencer(
        BaseAbinitSCFSequencerTest, BaseRelaxationSequencerTest):
    """Tests the AbinitRelaxationSequencer class.
    """
    _scf_example = abinit_relaxation_example
    _scf_input_variables = abinit_relaxation_scf_input_variables.copy()
    _sequencer_class = AbinitRelaxationSequencer
    _pseudos = abinit_gs_pseudos

    async def _set_relax_atoms_done(self):
        await copy_calculation(
                os.path.join(self._scf_example, "relax_atoms_only"),
                os.path.join(self.sequencer.scf_workdir, "relax_atoms_only"),
                new_pseudos=self._pseudos)

    async def _set_relax_cell_done(self):
        if self.sequencer.relax_atoms:
            new_parents = [os.path.join(
                self.sequencer.scf_workdir, "relax_atoms_only")]
        else:
            new_parents = []
        await copy_calculation(
                os.path.join(self._scf_example, "relax_cell_geometry"),
                os.path.join(
                    self.sequencer.scf_workdir, "relax_cell_geometry"),
                new_pseudos=self._pseudos,
                new_parents=new_parents)


class BaseAbinitConvergenceSequencerTest(
        BaseConvergenceSequencerTest, BaseAbinitSCFSequencerTest):
    """Base class for convergence sequencer unittests using abinit.
    """
    @pytest.fixture(autouse=True)
    def setup_abinit_convergence_sequencer(
            self, setup_convergence_sequencer,
            setup_abinit_scf_sequencer,
            ):
        yield

    async def _set_scf_done(self, *args, **kwargs):
        await BaseConvergenceSequencerTest._set_scf_done(self, *args, **kwargs)


class BaseAbinitPhononSequencerTest(
        BasePhononSequencerTest, BaseAbinitSCFSequencerTest):
    """Base class for phonons sequencer test cases with abinit.
    """
    _phonons_qpoint_grid = None
    _phonons_script_name = "abinit"
    _set_qpts = True

    @pytest.fixture(autouse=True)
    def setup_abinit_phonon_sequencer(
            self, setup_phonon_sequencer,
            setup_abinit_scf_sequencer,
            ):
        if self._phonons_qpoint_grid is None:
            raise DevError(
                    f"Need to set '_phonons_qpoint_grid' in "
                    f"'{self.__class__}'.")
        if self._set_qpts:
            self.sequencer.phonons_qpoint_grid = self._phonons_qpoint_grid

    async def _set_phonons_except_gamma_done(self):
        await self._set_phonons_done(skip_gamma=True)

    async def _set_phonons_done(self, skip_gamma=False):
        ex_dirname = os.path.dirname(self._phonons_example)
        new_dirname = os.path.dirname(self.sequencer.phonons_workdir)
        for calc in await aiofiles.os.listdir(ex_dirname):
            new_parents = [self.scf_workdir]
            source = os.path.join(ex_dirname, calc)
            if any(["atom_pol" in x
                    for x in await aiofiles.os.listdir(source)]):
                # qpt was split by irreps
                for subdir in await aiofiles.os.listdir(source):
                    source = os.path.join(ex_dirname, calc, subdir)
                    await copy_calculation(
                            source,
                            os.path.join(new_dirname, calc, subdir),
                            new_parents=new_parents,
                            new_pseudos=self._pseudos)
            else:
                target = os.path.join(new_dirname, calc)
                if calc.endswith(
                        os.path.basename(self.sequencer.phonons_workdir) +
                        "1"):
                    # gamma
                    if skip_gamma:
                        continue
                    if self.sequencer.compute_electric_field_response:
                        new_parents.append(self.ddk_workdir)
                if await aiofiles.os.path.isdir(target):
                    continue
                await copy_calculation(
                        source, target,
                        new_pseudos=self._pseudos,
                        new_parents=new_parents)


class BaseAbinitPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest, BaseAbinitPhononSequencerTest):
    """Base test case class for phonons convergence sequencers with abinit.
    """
    _phonons_qpoint_grid = [[0.0, 0.0, 0.0]]
    _set_qpts = False

    @pytest.fixture(autouse=True)
    def setup_abinit_phonon_convergence_sequencer(
            self, setup_phonon_convergence_sequencer,
            setup_abinit_phonon_sequencer,
            ):
        yield


class BaseAbinitSequencerWithDDKTest:
    @pytest.fixture
    def setup_ddk_tempdir(self):
        self.ddk_tempdir = tempfile.TemporaryDirectory()
        yield
        self.ddk_tempdir.cleanup()


class BaseAbinitPhononConvergenceWithDDKSequencerTest(
        BaseAbinitPhononConvergenceSequencerTest,
        BaseAbinitSequencerWithDDKTest,
        ):
    """Base test case class for phonons convergence sequencers with abinit.
    """

    async def _check_ddk_written(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        assert len(await aiofiles.os.listdir(
            os.path.join(self.ddk_workdir, name))) == (
                len(self.sequencer._parameters_to_converge))

    async def _check_ddk_not_written(self):
        assert not await (aiofiles.os.path.isdir(self.ddk_workdir))

    def _set_ddk(self):
        if self._ddk_input_variables is None:
            raise DevError(
                    f"Need to set '_ddk_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    async def _set_ddk_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        ddk_ex = self._ddk_example
        if isinstance(ddk_ex, str):
            ddk_ex = [os.path.join(ddk_ex, x) for x in os.listdir(ddk_ex)]
        for example in ddk_ex:
            basename = os.path.basename(example)
            dest = os.path.join(self.ddk_workdir, name, basename)
            await copy_calculation(
                    example, dest, new_pseudos=self._pseudos,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename)])

    async def _set_phonons_done(self):
        name = self.sequencer._parameter_to_converge_input_variable_name
        ph_ex = self._phonons_example
        if isinstance(ph_ex, str):
            ph_ex = [os.path.join(ph_ex, x) for x in os.listdir(ph_ex)]
        for example in ph_ex:
            basename = os.path.basename(example)
            dest = os.path.join(self.phonons_workdir, name, basename)
            await copy_calculation(
                    example, dest, new_pseudos=self._pseudos,
                    new_parents=[
                        os.path.join(self.scf_workdir, name, basename),
                        os.path.join(self.ddk_workdir, name, basename)],
                        )


QPTS = [
        [0.0, 0.0, 0.0],
        [0.25, 0.0, 0.0],
        [0.5, 0.0, 0.0],
        [0.25, 0.25, 0.0],
        [0.5, 0.25, 0.0],
        [-0.25, 0.25, 0.0],
        [0.5, 0.5, 0.0],
        [-0.25, 0.5, 0.25],
        ]
NPHONONS = len(QPTS)
SCF_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "scf_run")
PHONONS_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ph_runs", "ph_run")
DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons", "ddk")
SCF_INPUT_VARIABLES = {
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 2,
        "znucl": [13, 33],
        "natom": 2,
        "typat": [1, 2],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "nband": 4,
        "ixc": 1,
        "ecut": 3.0,
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 9.0
        }
DDK_INPUT_VARIABLES = {
        "tolwfr": 1.0e-22,
        "nstep": 25,
        "diemac": 9.0,
        "rfdir": [1, 1, 1],
        }
PHONONS_INPUT_VARIABLES = {
            "rfatpol": [1, 2],
            "tolvrs": 1.0e-8,
            "diemac": 9.0,
            "nstep": 25,
            }


# #############################################################################
# ############### abinit convergence sequencer test cases #####################
# #############################################################################

@pytest.mark.order("first")
class TestAbinitEcutConvergenceSequencer(
        BaseEcutConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest,
        ):
    """Test case for the abinit ecut convergence sequencer.
    """
    _ecuts = abinit_gs_ecut_convergence_ecuts
    _scf_input_variables = abinit_gs_ecut_convergence_input_variables.copy()
    _scf_example = os.path.join(abinit_gs_ecut_convergence_example, "ecut")
    _pseudos = abinit_gs_pseudos
    _sequencer_class = AbinitEcutConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_ecut_convergence_sequencer(
            self, setup_ecut_convergence_sequencer,
            setup_abinit_convergence_sequencer,
            ):
        self.sequencer.ecuts_input_variable_name = "ecut"

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)


@pytest.mark.order("first")
class TestAbinitEcutPhononConvergenceSequencerWithoutDDK(
        BaseEcutPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceSequencerTest,
        ):
    """Test case for the Abinit ecut phonons convergence sequencer without
    electric field response.
    """
    _ecuts = abinit_phonon_ecut_convergence_ecuts
    _phonons_example = os.path.join(
            abinit_phonon_ecut_convergence_example,
            "phonons_runs", "ecut")
    _phonons_input_variables = (
            abinit_phonon_ecut_convergence_phonons_input_variables)
    _scf_input_variables = abinit_phonon_ecut_convergence_scf_input_variables
    _scf_example = os.path.join(
            abinit_phonon_ecut_convergence_example, "scf_runs", "ecut")
    _pseudos = abinit_phonon_ecut_convergence_pseudos
    _sequencer_class = AbinitEcutPhononConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_ecut_phonon_convergence_without_ddk_sequencer(
            self, setup_ecut_phonon_convergence_sequencer,
            setup_abinit_phonon_convergence_sequencer,
            ):
        self.sequencer.ecuts_input_variable_name = "ecut"
        self.sequencer.compute_electric_field_response = False

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)


@pytest.mark.order("first")
class TestAbinitEcutPhononConvergenceSequencerWithDDK(
        BaseEcutPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceWithDDKSequencerTest,
        BaseAbinitSequencerWithDDKTest,
        ):
    """Test case for the Abinit ecut phonons convergence sequencer with
    electric field response.
    """
    _ecuts = abinit_phonon_ecut_convergence_ecuts_ddk
    _phonons_example = os.path.join(
            abinit_phonon_ecut_convergence_example_ddk, "phonons_runs", "ecut")
    _phonons_input_variables = (
            abinit_phonon_ecut_convergence_phonons_input_variables_ddk)
    _scf_input_variables = (
            abinit_phonon_ecut_convergence_scf_input_variables_ddk)
    _scf_example = os.path.join(
            abinit_phonon_ecut_convergence_example_ddk, "scf_runs", "ecut")
    _pseudos = abinit_phonon_ecut_convergence_pseudos_ddk
    _ddk_example = os.path.join(
            abinit_phonon_ecut_convergence_example_ddk, "ddk_runs", "ecut")
    _ddk_input_variables = (
            abinit_phonon_ecut_convergence_ddk_input_variables)
    _sequencer_class = AbinitEcutPhononConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_ecut_phonon_convergence_with_ddk_sequencer(
            self, setup_ecut_phonon_convergence_sequencer,
            setup_abinit_phonon_convergence_sequencer,
            setup_ddk_tempdir,
            ):
        self.sequencer.compute_electric_field_response = True
        self.sequencer.ecuts_input_variable_name = "ecut"
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_ddk_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        await self._set_ddk_done()
        await self._set_phonons_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        await self._check_plot_written()

    async def test_starting_from_ddk(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        await self._check_scf_written()
        await self._check_phonons_not_written()
        await self._check_ddk_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)

    async def _set_phonons(self):
        super()._set_phonons()


class BaseAbinitKgridConvergenceSequencerTest(
        BaseKgridConvergenceSequencerTest):
    """Base class for abinit kgrid convergence sequencers."""

    @pytest.fixture(autouse=True)
    def setup_abinit_kgrid_convergence_sequencer(
            self, setup_kgrid_convergence_sequencer,
            setup_abinit_convergence_sequencer,
            ):
        self.sequencer.kgrids_input_variable_name = "ngkpt"


@pytest.mark.order("first")
class TestAbinitKgridConvergenceSequencer(
        BaseAbinitKgridConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest,
        ):
    """Test case for the abinit kgrid convergence sequencer.
    """
    _kgrids = abinit_gs_kgrid_convergence_scf_kgrids
    _scf_input_variables = abinit_gs_kgrid_convergence_scf_input_variables
    _scf_example = os.path.join(
            abinit_gs_kgrid_convergence_example, "scf_runs", "ngkpt")
    _sequencer_class = AbinitKgridConvergenceSequencer
    _pseudos = abinit_gs_pseudos

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)


class BaseAbinitKgridPhononConvergenceSeqencerTest(
        BaseKgridPhononConvergenceSequencerTest,
        ):
    """Base class for both abinit phonon kgrid convergence sequencer tests."""
    _compute_electric_field_response = None
    _phonons_qpt = None
    _kgrids = abinit_phonon_kgrid_convergence_scf_kgrids
    _phonons_input_variables = (
            abinit_phonon_kgrid_convergence_phonons_input_variables.copy())
    _scf_input_variables = (
            abinit_phonon_kgrid_convergence_scf_input_variables.copy())
    _sequencer_class = AbinitKgridPhononConvergenceSequencer
    _pseudos = abinit_phonon_kgrid_convergence_pseudos
    _ddk_input_variables = (
            abinit_phonon_kgrid_convergence_ddk_input_variables.copy())

    @pytest.fixture(autouse=True)
    def setup_abinit_kgrid_phonon_convergence_sequencer(
            self, setup_kgrid_convergence_sequencer,
            ):
        self.sequencer.compute_electric_field_response = (
                self._compute_electric_field_response)
        self.sequencer.kgrids_input_variable_name = (
                abinit_phonon_kgrid_convergence_kgrid_input_variable_name)
        self.sequencer.phonons_qpt = self._phonons_qpt


# TODO: rebase this with TestAbinitEcutPhononConvergenceSequencerWithDDK
@pytest.mark.order("first")
class TestAbinitKgridPhononConvergenceSequencerWithDDK(
        BaseAbinitKgridPhononConvergenceSeqencerTest,
        BaseAbinitPhononConvergenceWithDDKSequencerTest,
        BaseAbinitSequencerWithDDKTest,
        ):
    """Test case for the abinit kgrids phonon convergence sequencer."""
    _compute_electric_field_response = True
    _phonons_qpt = abinit_phonon_kgrid_convergence_phonons_qpt
    _phonons_example = [
            os.path.join(
                abinit_phonon_kgrid_convergence_example,
                "phonons_runs", "ngkpt", x)
            for x in os.listdir(
                os.path.join(
                    abinit_phonon_kgrid_convergence_example,
                    "phonons_runs", "ngkpt"))
            ]
    _scf_example = [
            os.path.join(
                abinit_phonon_kgrid_convergence_example,
                "scf_runs", "ngkpt", x)
            for x in os.listdir(
                os.path.join(
                    abinit_phonon_kgrid_convergence_example,
                    "scf_runs", "ngkpt"))
            ]
    _ddk_example = [
            os.path.join(
                abinit_phonon_kgrid_convergence_example,
                "ddk_runs", "ngkpt", x)
            for x in os.listdir(
                os.path.join(
                    abinit_phonon_kgrid_convergence_example,
                    "ddk_runs", "ngkpt"))
            ]

    @pytest.fixture(autouse=True)
    def setup_abinit_kgrid_phonon_convergence_with_ddk_sequencer(
            self,
            setup_abinit_kgrid_phonon_convergence_sequencer,
            setup_abinit_phonon_convergence_sequencer,
            setup_ddk_tempdir,
            ):
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_ddk_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        await self._set_ddk_done()
        await self._set_phonons_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        await self._check_plot_written()

    async def test_starting_from_ddk(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        await self._check_scf_written()
        await self._check_phonons_not_written()
        await self._check_ddk_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        await self._set_phonons()
        self._set_ddk()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)

    async def _set_phonons(self):
        super()._set_phonons()


@pytest.mark.order("first")
class TestAbinitKgridPhononConvergenceSequencerWithoutDDK(
        BaseAbinitKgridPhononConvergenceSeqencerTest,
        BaseAbinitPhononConvergenceSequencerTest,
        ):
    """Test case for the abinit kgrids phonon convergence sequencer."""
    _phonons_qpt = abinit_phonon_kgrid_convergence_phonons_qpt_no_ddk
    _phonons_example = [
            os.path.join(
                abinit_phonon_kgrid_convergence_example_no_ddk,
                "phonons_runs", "ngkpt", x)
            for x in os.listdir(
                os.path.join(
                    abinit_phonon_kgrid_convergence_example_no_ddk,
                    "phonons_runs", "ngkpt"))
            ]
    _scf_example = [
            os.path.join(
                abinit_phonon_kgrid_convergence_example_no_ddk,
                "scf_runs", "ngkpt", x)
            for x in os.listdir(
                os.path.join(
                    abinit_phonon_kgrid_convergence_example_no_ddk,
                    "scf_runs", "ngkpt"))
            ]
    _compute_electric_field_response = False

    @pytest.fixture(autouse=True)
    def presetup_abinit_kgrid_phonon_convergence_without_ddk_sequencer(
            self,
            setup_abinit_phonon_convergence_sequencer,
            ):
        # this is to ensure fixture order
        yield

    @pytest.fixture(autouse=True)
    def setup_abinit_kgrid_phonon_convergence_without_ddk_sequencer(
            self,
            presetup_abinit_kgrid_phonon_convergence_without_ddk_sequencer,
            setup_abinit_kgrid_phonon_convergence_sequencer,
            ):
        yield

    async def _set_scf_done(self):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(self)


# ################# ABINIT smearing convergence sequencer tests ##############
@pytest.mark.order("first")
class TestAbinitSmearingConvergenceSequencer(
        BaseSmearingConvergenceSequencerTest,
        BaseAbinitConvergenceSequencerTest):
    """Test case for the AbinitSmearingConvergenceSequencer class.
    """
    _kgrids = abinit_gs_smearing_convergence_kgrids
    _pseudos = abinit_gs_smearing_convergence_pseudos
    _scf_example = abinit_gs_smearing_convergence_example
    _scf_input_variables = (
            abinit_gs_smearing_convergence_scf_input_variables)
    _sequencer_class = AbinitSmearingConvergenceSequencer
    _smearings = abinit_gs_smearing_convergence_smearings
    _scf_convergence_criterion = abinit_gs_smearing_convergence_criterion

    @pytest.fixture(autouse=True)
    def setup_abinit_smearing_convergence_sequencer(
            self, setup_smearing_convergence_sequencer,
            setup_abinit_convergence_sequencer,
            ):
        self.sequencer.kgrids_input_variable_name = (
                abinit_gs_smearing_convergence_varname)

    async def _set_scf_done(self):
        await BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, new_pseudos=self._pseudos)


ABINIT_SMEARING_CONVERGENCE_PHONONS_EXAMPLE = os.path.join(
            here, "files", "abinit_Al_smearing_convergence",
            "phonons_runs", "tsmear")


@pytest.mark.order("first")
class TestAbinitSmearingPhononConvergenceSequencer(
        BaseSmearingPhononConvergenceSequencerTest,
        BaseAbinitPhononConvergenceSequencerTest):
    """Test case for the Abinit smearing convergence sequencer.
    """
    _kgrids = abinit_gs_smearing_convergence_kgrids
    _smearings = abinit_gs_smearing_convergence_smearings
    _nphonons = 1
    _phonons_example = os.path.join(
            abinit_phonons_smearing_convergence_example,
            "phonons_runs")
    _phonons_input_variables = (
            abinit_phonons_smearing_convergence_phonons_input_variables)
    _scf_input_variables = (
            abinit_phonons_smearing_convergence_scf_input_variables)
    _scf_example = os.path.join(
            abinit_phonons_smearing_convergence_example,
            "scf_runs",
            )
    _pseudos = abinit_gs_smearing_convergence_pseudos
    _sequencer_class = AbinitSmearingPhononConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_smearing_phonon_convergence_sequencer(
            self, setup_smearing_phonon_convergence_sequencer,
            setup_abinit_phonon_convergence_sequencer,
            ):
        self.sequencer.kgrids_input_variable_name = "ngkpt"

    async def _set_scf_done(self):
        await BaseSmearingPhononConvergenceSequencerTest._set_scf_done(
                self, new_pseudos=self._pseudos)

    async def _set_phonons_done(self):
        await BaseSmearingPhononConvergenceSequencerTest._set_phonons_done(
                self, new_pseudos=self._pseudos)


# #############################################################################
# ############### abinit individual phonon sequencer test case ################
# #############################################################################
SCF_INPUT_VARIABLES = {
        "ecut": 5.0,
        "tolvrs": 1.0e-18,
        "acell": [10.61] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 4,
        "shiftk": [[0.0, 0.0, 0.5], [0.0, 0.5, 0.0],
                   [0.5, 0.0, 0.0], [0.5, 0.5, 0.5]],
        "nstep": 25,
        "diemac": 12.0
        }
DDK_INPUT_VARIABLES = {
        "tolwfr": 1.0e-22,
        "nstep": 25,
        "diemac": 12.0,
        "rfdir": [1, 1, 1],
        }
PHONONS_INPUT_VARIABLES = {
        "rfatpol": [1, 2],
        "tolvrs": 1.0e-8,
        "diemac": 12.0,
        "nstep": 25,
        }
PSEUDOS = [os.path.join(here, "files", "pseudos", "Si.psp8")]


@pytest.mark.order("first")
class TestAbinitIndividualPhononSequencerWithDDK(
        BaseAbinitPhononSequencerTest,
        BaseAbinitSequencerWithDDKTest,
        ):
    """Test case for the AbinitIndividualPhononSequencer class.
    """
    _nphonons = (
            len(os.listdir(os.path.join(
                abinit_phonon_dispersion_example, "phonons_runs"))) - 1)
    _phonons_example = os.path.join(
            abinit_phonon_dispersion_example,
            "phonons_runs", "ph_q")
    _phonons_input_variables = (
            abinit_phonon_dispersion_phonons_input_variables.copy())
    _phonons_qpoint_grid = abinit_phonon_dispersion_qpoint_grid_list
    _pseudos = abinit_phonon_dispersion_pseudos
    _ddk_example = os.path.join(
            abinit_phonon_dispersion_example, "ddk_runs")
    _ddk_input_variables = (
            abinit_phonon_dispersion_ddk_input_variables.copy())
    _scf_example = os.path.join(
            abinit_phonon_dispersion_example, "..", "gs_run")
    _scf_input_variables = (
            abinit_phonon_dispersion_scf_input_variables.copy())
    _sequencer_class = AbinitIndividualPhononSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_individual_phonon_with_ddk_sequencer(
            self, setup_abinit_phonon_sequencer, setup_ddk_tempdir):
        self.sequencer.compute_electric_field_response = True
        self.ddk_workdir = os.path.join(self.ddk_tempdir.name, "ddk_run")
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.phonons_command
        self.sequencer.ddk_queuing_system = "local"
        self.sequencer.phonons_qpoints_split_by_irreps = (
                abinit_phonon_dispersion_qpoints_split_by_irreps)

    async def _set_phonons(self):
        if asyncio.iscoroutinefunction(super()._set_phonons):
            await super()._set_phonons()
        else:
            super()._set_phonons()

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after scf calc
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_ddk_written()
        await self._check_phonons_except_gamma_written()
        assert not await (self.sequencer.sequence_completed)
        # continue after ddk calc and non gamma phonons
        self.ddk_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        await self._set_ddk_done()
        await self._set_phonons_except_gamma_done()
        await self.sequencer.write()
        await self._check_phonons_gamma_written()
        assert not await (self.sequencer.sequence_completed)
        # continue for gamma phonon
        # I use rmtree here cause cleanup does not seem to remove subdir
        # I don't know why...
        await aioshutil.rmtree(self.phonons_tempdir.name)
        # self.phonons_tempdir.cleanup()
        await self._set_phonons_done()
        await self.sequencer.write()
        assert await (self.sequencer.sequence_completed)

    async def test_starting_from_ddk_and_phonons_except_gamma(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons_except_gamma_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_written()
        await self._check_phonons_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_ddk_no_other_phonons(self):
        # check that scf and ddk are done without any phonons
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_written()
        await self._check_phonons_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        await self._set_scf_done()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        await self._check_scf_written()
        await self._check_phonons_except_gamma_written()
        await self._check_phonons_gamma_not_written()
        await self._check_ddk_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        await self._set_phonons()
        self._set_ddk()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def _check_ddk(self, true):
        await self._check(self.ddk_workdir, true)

    async def _check_ddk_written(self):
        await self._check_ddk(True)

    async def _check_ddk_not_written(self):
        await self._check_ddk(False)

    async def _check_phonons_except_gamma(self, true):
        for i in range(2, self.sequencer.nphonons + 1):
            # gamma is first phonon just skip it
            phdir = self.sequencer.phonons_workdir + str(i)
            if "irreps_determination" in await aiofiles.os.listdir(phdir):
                continue
            await self._check(phdir, true)

    async def _check_phonons_except_gamma_written(self):
        await self._check_phonons_except_gamma(True)

    async def _check_phonons_gamma(self, true):
        # gamma is first phonon
        phdir = self.sequencer.phonons_workdir + str(1)
        await self._check(phdir, true)

    async def _check_phonons_gamma_not_written(self):
        await self._check_phonons_gamma(False)

    async def _check_phonons_gamma_written(self):
        await self._check_phonons_gamma(True)

    def _set_ddk(self):
        if self._ddk_input_variables is None:
            raise DevError(
                    f"Need to set '_ddk_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    async def _set_ddk_done(self):
        await copy_calculation(
                self._ddk_example, self.ddk_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.scf_workdir],
                )


PHONONS_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "ph_runs", "ph_run")
SCF_NO_DDK_EXAMPLE = os.path.join(
        here, "files", "abinit_Si_phonons_noddk", "scf_run")


@pytest.mark.order("first")
class TestAbinitIndividualPhononSequencerWithoutDDK(
        BaseAbinitPhononSequencerTest):
    """Test case for the AbinitIndividualPhononSequencer class without the
    computation of the electric field response.
    """
    _nphonons = (
            len(os.listdir(os.path.join(
                abinit_phonon_dispersion_example, "phonons_runs"))) - 1)
    _phonons_example = os.path.join(
            abinit_phonon_dispersion_noddk_example,
            "phonons_runs", "ph_q")
    _phonons_input_variables = (
            abinit_phonon_dispersion_phonons_input_variables.copy())
    _phonons_qpoint_grid = abinit_phonon_dispersion_qpoint_grid_list
    _pseudos = abinit_phonon_dispersion_pseudos
    _scf_example = os.path.join(
            abinit_phonon_dispersion_example, "..", "gs_run")
    _scf_input_variables = (
            abinit_phonon_dispersion_scf_input_variables.copy())
    _sequencer_class = AbinitIndividualPhononSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_individual_phonon_sequencer_without_ddk(
            self, setup_abinit_phonon_sequencer):
        self.sequencer.compute_electric_field_response = False


# #############################################################################
# ######### abinit phonon dispersion sequencer test cases #####################
# #############################################################################


class BaseAbinitPhononDispersionSequencerTest(
        BaseAbinitPhononSequencerTest):
    """Base class for abinit phonon dispersion sequencer test cases.
    """
    _anaddb_input_variables = (
            abinit_phonon_dispersion_anaddb_input_variables.copy())
    _phonons_qpoint_grid = abinit_phonon_dispersion_qpoint_grid
    _phonons_input_variables = (
            abinit_phonon_dispersion_phonons_input_variables.copy())
    _pseudos = abinit_phonon_dispersion_pseudos

    @pytest.fixture(autouse=True)
    def setup_abinit_phonon_dispersion_sequencer(
            self, setup_mrgddb_tempdir, temp_mrgddb_script,
            setup_anaddb_tempdir, temp_anaddb_script,
            ):
        if self._anaddb_input_variables is None:
            raise DevError(
                    "Need to set '_anaddb_input_variables'.")
        self.sequencer.phonons_qgrid_generation_input_variable_name = (
                abinit_phonon_dispersion_qgrid_generation_input_variable_name)
        # need to set mrgddb and anaddb calculations afterwards
        self.mrgddb_workdir = os.path.join(
                self.mrgddb_tempdir.name, "mrgddb_run")
        self.mrgddb_command_file = temp_mrgddb_script
        self.sequencer.mrgddb_workdir = self.mrgddb_workdir
        self.sequencer.mrgddb_command = self.mrgddb_command_file.name
        self.sequencer.mrgddb_queuing_system = "local"
        self.anaddb_workdir = os.path.join(
                self.anaddb_tempdir.name, "anaddb_run")
        self.anaddb_command_file = temp_anaddb_script
        self.sequencer.anaddb_workdir = self.anaddb_workdir
        self.sequencer.anaddb_command = self.anaddb_command_file.name
        self.sequencer.anaddb_queuing_system = "local"
        self.sequencer.qpoint_path_density = (
                abinit_phonon_dispersion_qpoint_path_density)
        self.sequencer.qpoint_path = (
                abinit_phonon_dispersion_qpoint_path)
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.anaddb_workdir, "plot.pdf")

    async def _check_phonons(self, *args, **kwargs):
        # override this to take into account the 'qgrid_gen'
        await BaseAbinitPhononSequencerTest._check_phonons(
                self, *args, exceptions="qgrid_gen", **kwargs)

    async def _check_mrgddb(self, true):
        await self._check(self.mrgddb_workdir, true)

    async def _check_mrgddb_written(self):
        await self._check_mrgddb(True)

    async def _check_mrgddb_not_written(self):
        await self._check_mrgddb(False)

    async def _check_anaddb(self, true):
        await self._check(self.anaddb_workdir, true)

    async def _check_anaddb_written(self):
        await self._check_anaddb(True)

    async def _check_anaddb_not_written(self):
        await self._check_anaddb(False)

    @pytest.fixture
    def setup_mrgddb_tempdir(self):
        self.mrgddb_tempdir = tempfile.TemporaryDirectory()
        yield
        self.mrgddb_tempdir.cleanup()

    def _set_mrgddb(self):
        # nothing to do
        return

    async def _set_mrgddb_done(self):
        dirname = os.path.dirname(self.sequencer.phonons_workdir)
        new_parents = []
        for calc in await aiofiles.os.listdir(dirname):
            if any(["atom_pol" in subdir
                    for subdir in await aiofiles.os.listdir(
                        os.path.join(dirname, calc))]):
                # split by irreps
                for subdir in await aiofiles.os.listdir(
                        os.path.join(dirname, calc)):
                    if "determination" in subdir:
                        continue
                    new_parents.append(os.path.join(dirname, calc, subdir))
            elif "qgrid_gen" in calc:
                continue
            else:
                new_parents.append(os.path.join(dirname, calc))
        await copy_calculation(
                self._mrgddb_example, self.mrgddb_workdir,
                new_parents=new_parents,
                )

    async def _set_phonons(self):
        BaseAbinitPhononSequencerTest._set_phonons(self)
        # also copy the qpt generation run
        if await aiofiles.os.path.isdir(
                self.sequencer.phonons_workdir + "_qgrid_generation"):
            return
        await copy_calculation(
                self._phonons_example + "_qgrid_generation",
                self.sequencer.phonons_workdir + "_qgrid_generation",
                new_pseudos=self._pseudos)

    @pytest.fixture
    def setup_anaddb_tempdir(self):
        self.anaddb_tempdir = tempfile.TemporaryDirectory()
        yield
        self.anaddb_tempdir.cleanup()

    def _set_anaddb(self):
        if self._anaddb_input_variables is None:
            raise DevError(
                    f"Need to set '_anaddb_input_variables' in "
                    f"'{self.__class__}'.")
        self.sequencer.anaddb_input_variables = self._anaddb_input_variables

    async def _set_anaddb_done(self):
        await copy_calculation(
                self._anaddb_example, self.anaddb_workdir,
                new_parents=[self.mrgddb_workdir])

    async def _check_plot(self, true):
        isfile = await aiofiles.os.path.isfile(self.sequencer.plot_save)
        if true:
            assert isfile
        else:
            assert not isfile

    async def _check_plot_written(self):
        await self._check_plot(True)

    async def _check_plot_not_written(self):
        await self._check_plot(False)

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self._set_phonons_done()
        self._set_mrgddb()
        self._set_anaddb()
        await self.sequencer.write()
        await self._check_mrgddb_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)
        # continue for mrgddb
        self.mrgddb_tempdir.cleanup()
        await self._set_mrgddb_done()
        await self.sequencer.write()
        await self._check_anaddb_written()
        assert not await (self.sequencer.sequence_completed)
        # set anaddb done and check plot is created
        self.anaddb_tempdir.cleanup()
        await self._set_anaddb_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        await self._check_plot_written()

    async def test_starting_from_mrgddb(self):
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self._set_phonons_done()
        await self._set_mrgddb_done()
        self._set_anaddb()
        await self.sequencer.write()
        await self._check_anaddb_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_phonons(self):
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self._set_phonons_done()
        self._set_mrgddb()
        self._set_anaddb()
        await self.sequencer.write()
        await self._check_mrgddb_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_ddk_no_other_phonons(self):
        # check that scf and ddk are done without any phonons
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        await self._check_mrgddb_not_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_ddk()
        await self._set_phonons()
        await self.sequencer.write()
        # when computing electric field response, all phonons except gamma
        # should be written because gamma relies on the ddks
        await self._check_scf_written()
        await self._check_phonons_except_gamma_written()
        await self._check_phonons_gamma_not_written()
        await self._check_ddk_written()
        await self._check_mrgddb_not_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_not_written()
        await self._check_phonons_not_written()
        await self._check_mrgddb_not_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)


@pytest.mark.order("first")
class TestAbinitPhononDispersionWithDDKSequencer(
        TestAbinitIndividualPhononSequencerWithDDK,
        BaseAbinitPhononDispersionSequencerTest):
    """Test case for the abinit phonon dispersion sequencer with electric
    field response calculation.
    """
    _nphonons = None
    _phonons_qpoint_grid = abinit_phonon_dispersion_qpoint_grid
    _phonons_example = os.path.join(
            abinit_phonon_dispersion_example, "phonons_runs", "ph_q")
    _ddk_example = os.path.join(
            abinit_phonon_dispersion_example, "ddk_runs")
    _ddk_input_variables = abinit_phonon_dispersion_ddk_input_variables.copy()
    _scf_example = os.path.join(
            abinit_phonon_dispersion_example, "..", "gs_run")
    _scf_input_variables = abinit_phonon_dispersion_scf_input_variables.copy()
    _mrgddb_example = os.path.join(
            abinit_phonon_dispersion_example, "mrgddb_run")
    _anaddb_example = os.path.join(
            abinit_phonon_dispersion_example, "anaddb_run")
    _sequencer_class = AbinitPhononDispersionSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_phonon_dispersion_with_ddk_sequencer(
            self, setup_abinit_individual_phonon_with_ddk_sequencer,
            setup_abinit_phonon_dispersion_sequencer,
            ):
        self.sequencer.compute_electric_field_response = (
                abinit_phonon_dispersion_compute_electric_field_response)
        self.sequencer.phonons_qpoints_split_by_irreps = (
                abinit_phonon_dispersion_qpoints_split_by_irreps)
        self.sequencer.mrgddb_queuing_system = "local"
        self.sequencer.anaddb_queuing_system = "local"
        self.sequencer.qpoint_path_density = (
                abinit_phonon_dispersion_qpoint_path_density)
        self.sequencer.qpoint_path = (
                abinit_phonon_dispersion_qpoint_path)

    async def test_starting_from_scf(self):
        cls = BaseAbinitPhononDispersionSequencerTest
        await cls.test_starting_from_scf(self)

    async def test_starting_from_ddk_no_other_phonons(self):
        cls = BaseAbinitPhononDispersionSequencerTest
        await cls.test_starting_from_ddk_no_other_phonons(self)

    async def test_starting_from_ddk_and_phonons_except_gamma(self):
        # Test that starting will all phonons and ddk but no gamma works
        # this is tested since gamma phonons with electric response function
        # calculation relies on the ddk calculation
        self._set_scf()
        self._set_ddk()
        await self._set_scf_done()
        await self._set_ddk_done()
        await self._set_phonons_except_gamma_done()
        await self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_ddk_written()
        await self._check_phonons_written()
        await self._check_mrgddb_not_written()
        await self._check_anaddb_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_whole_sequence(self):
        await BaseAbinitPhononDispersionSequencerTest.test_whole_sequence(self)


@pytest.mark.order("first")
class TestAbinitPhononDispersionWithoutDDKSequencer(
        TestAbinitIndividualPhononSequencerWithoutDDK,
        BaseAbinitPhononDispersionSequencerTest):
    """Test case for the abinit phonon dispersion sequencer without electric
    field response calculation.
    """
    _nphonons = None
    _phonons_example = os.path.join(
            abinit_phonon_dispersion_noddk_example, "phonons_runs", "ph_q")
    _scf_example = os.path.join(
            abinit_phonon_dispersion_example, "..", "gs_run")
    _mrgddb_example = os.path.join(
            abinit_phonon_dispersion_noddk_example, "mrgddb_run")
    _anaddb_example = os.path.join(
            abinit_phonon_dispersion_noddk_example, "anaddb_run")
    _sequencer_class = AbinitPhononDispersionSequencer
    _phonons_qpoint_grid = abinit_phonon_dispersion_qpoint_grid

    @pytest.fixture(autouse=True)
    def setup_abinit_phonon_dispersion_without_ddk_sequencer(
            self,
            setup_abinit_individual_phonon_sequencer_without_ddk,
            setup_abinit_phonon_dispersion_sequencer,
            ):
        yield

    def _set_ddk(self):
        # do nothing
        pass

    async def _set_ddk_done(self):
        # just do nothing
        pass

    async def _check_ddk_written(self):
        # do nothing
        pass

    async def _check_ddk_not_written(self):
        # do nothing
        pass

    async def _check_phonons_except_gamma_written(self):
        # irrelevent here
        pass

    async def _check_phonons_gamma_not_written(self):
        # irrelevent here
        pass

    async def test_whole_sequence(self):
        await BaseAbinitPhononDispersionSequencerTest.test_whole_sequence(self)


# ########################################################################### #
# ########################## Optic sequencers ############################### #
# ########################################################################### #


class BaseAbinitOpticSequencerTest(
        BaseAbinitNSCFSequencerTest, BaseAbinitSequencerWithDDKTest,
        ):
    """Base class for optic sequencer tests.
    """
    _ddk_input_variables = abinit_optic_ddk_input_variables.copy()
    _nscf_input_variables = abinit_optic_nscf_input_variables.copy()
    _pseudos = abinit_optic_pseudos
    _scf_input_variables = abinit_optic_scf_input_variables.copy()
    _sequencer_class = AbinitOpticSequencer

    @pytest.fixture(autouse=True)
    def setup_base_abinit_optic_sequencer(
            self, setup_abinit_nscf_sequencer, setup_ddk_tempdir,
            setup_optic_tempdir, temp_optic_script,
            ):
        if self._optic_input_variables is None:
            raise DevError(
                    "Need to set '_optic_input_variables'.")
        self.ddk_workdir = os.path.join(
                self.ddk_tempdir.name, "ddk_run")
        self.ddk_command_file = self.scf_command_file
        self.sequencer.ddk_workdir = self.ddk_workdir
        self.sequencer.ddk_command = self.ddk_command_file.name
        self.sequencer.ddk_queuing_system = "local"
        self.optic_workdir = os.path.join(
                self.optic_tempdir.name, "optic_run")
        self.optic_command_file = temp_optic_script
        self.sequencer.optic_workdir = self.optic_workdir
        self.sequencer.optic_command = self.optic_command_file.name
        self.sequencer.optic_queuing_system = "local"
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.optic_workdir, "plot.pdf")

    async def _check_ddk(self, true):
        await self._check(self.ddk_workdir, true)

    async def _check_ddk_written(self):
        await self._check_ddk(True)

    async def _check_ddk_not_written(self):
        await self._check_ddk(False)

    def _set_ddk(self):
        # also need to set nscf input vars
        self._set_nscf()
        self.sequencer.ddk_input_variables = self._ddk_input_variables

    async def _check_nscf(self, true):
        await self._check(self.nscf_workdir, true)

    async def _check_nscf_written(self):
        await self._check_nscf(True)

    async def _check_nscf_not_written(self):
        await self._check_nscf(False)

    @pytest.fixture
    def setup_nscf_tempdir(self):
        self.nscf_tempdir = tempfile.TemporaryDirectory()
        yield
        self.nscf_tempdir.cleanup()

    def _set_nscf(self):
        self.sequencer.nscf_input_variables = self._nscf_input_variables

    async def _set_nscf_done(self):
        await copy_calculation(
                self._nscf_example, self.nscf_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.scf_workdir])

    async def _check_optic(self, true):
        await self._check(self.optic_workdir, true)

    async def _check_optic_written(self):
        await self._check_optic(True)

    async def _check_optic_not_written(self):
        await self._check_optic(False)

    @pytest.fixture
    def setup_optic_tempdir(self):
        self.optic_tempdir = tempfile.TemporaryDirectory()
        yield
        self.optic_tempdir.cleanup()

    def _set_optic(self):
        self.sequencer.optic_input_variables = self._optic_input_variables

    async def _check_plot(self, true):
        isfile = os.path.isfile(self.sequencer.plot_save)
        if true:
            assert isfile
        else:
            assert not isfile

    async def _check_plot_written(self):
        assert len(await aiofiles.os.listdir(
            os.path.dirname(self.sequencer.plot_save))) >= 1

    async def _check_plot_not_written(self):
        await self._check_plot(False)


@pytest.mark.order("first")
class TestAbinitOpticSequencer(
        BaseAbinitOpticSequencerTest,
        ):
    """class for the AbinitOpticSequencer test cases.
    """
    _ddk_example = os.path.join(
            abinit_optic_example, "ddk_run")
    _nscf_example = os.path.join(
            abinit_optic_example, "nscf_run")
    _nscffbz_example = os.path.join(
            abinit_optic_example, "nscf_fbz_run")
    _optic_example = os.path.join(
            abinit_optic_example, "optic_run")
    _optic_input_variables = abinit_optic_input_variables.copy()
    _scf_example = abinit_gs_example

    @pytest.fixture(autouse=True)
    def setup_abinit_optic_sequencer(
            self, setup_base_abinit_optic_sequencer,
            setup_nscffbz_tempdir,
            ):
        self.sequencer.compute_non_linear_optical_response = True
        self.nscffbz_workdir = os.path.join(
                self.nscffbz_tempdir.name, "nscf_fbz_run")
        self.nscffbz_command_file = self.scf_command_file
        self.sequencer.nscffbz_workdir = self.nscffbz_workdir
        self.sequencer.nscffbz_command = self.nscffbz_command_file.name
        self.sequencer.nscffbz_queuing_system = "local"

    async def _set_ddk_done(self):
        await copy_calculation(
                self._ddk_example, self.ddk_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.nscffbz_workdir])

    async def _check_nscffbz(self, true):
        await self._check(self.nscffbz_workdir, true)

    async def _check_nscffbz_written(self):
        await self._check_nscffbz(True)

    async def _check_nscffbz_not_written(self):
        await self._check_nscffbz(False)

    @pytest.fixture
    def setup_nscffbz_tempdir(self):
        self.nscffbz_tempdir = tempfile.TemporaryDirectory()
        yield
        self.nscffbz_tempdir.cleanup()

    def _set_nscffbz(self):
        # just set the nscf input vars
        self._set_nscf()

    async def _set_nscffbz_done(self):
        await copy_calculation(
                self._nscffbz_example, self.nscffbz_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.nscf_workdir,
                             self.sequencer.scf_workdir])

    async def _set_optic_done(self):
        await copy_calculation(
                self._optic_example, self.optic_workdir,
                new_parents=[self.sequencer.nscffbz_workdir,
                             self.sequencer.ddk_workdir])

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_nscffbz_done()
        await self._set_ddk_done()
        self._set_optic()
        await self.sequencer.write()
        await self._check_optic_written()
        assert not await self.sequencer.sequence_completed
        # continue for optic
        self.optic_tempdir.cleanup()
        await self._set_optic_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        await self._check_plot_written()

    async def test_starting_from_nscffbz(self):
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_nscffbz_done()
        self._set_ddk()
        await self.sequencer.write()
        await self._check_ddk_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_nscf(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        self._set_nscffbz()
        await self.sequencer.write()
        await self._check_nscffbz_written()
        await self._check_ddk_not_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        await self._set_scf_done()
        self._set_nscf()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_nscffbz_not_written()
        await self._check_ddk_not_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_nscf()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_nscffbz_not_written()
        await self._check_ddk_not_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)


@pytest.mark.order("first")
class TestAbinitOpticSequencerLinearOnly(
        BaseAbinitOpticSequencerTest,
        ):
    """class for the AbinitOpticSequencer test cases for linear optics only
    using EVK files.
    """
    _ddk_example = os.path.join(
            abinit_optic_linear_only_example, "ddk_run")
    _nscf_example = os.path.join(
            abinit_optic_linear_only_example, "nscf_run")
    _optic_example = os.path.join(
            abinit_optic_linear_only_example, "optic_run")
    _optic_input_variables = abinit_optic_linear_only_optic_input_variables
    _scf_example = abinit_gs_example

    @pytest.fixture(autouse=True)
    def setup_abinit_optic_linear_only_sequencer(
            self, setup_base_abinit_optic_sequencer,
            ):
        self.sequencer.compute_non_linear_optical_response = False

    async def _set_ddk_done(self):
        await copy_calculation(
                self._ddk_example, self.ddk_workdir,
                new_pseudos=self._pseudos,
                new_parents=[self.sequencer.nscf_workdir])

    async def _set_optic_done(self):
        await copy_calculation(
                self._optic_example, self.optic_workdir,
                new_parents=[self.sequencer.nscf_workdir,
                             self.sequencer.ddk_workdir])

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        await self._set_ddk_done()
        self._set_optic()
        await self.sequencer.write()
        await self._check_optic_written()
        assert not await (self.sequencer.sequence_completed)
        # continue for optic
        self.optic_tempdir.cleanup()
        await self._set_optic_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        await self._check_plot_written()

    async def test_starting_from_nscffbz(self):
        await self._set_scf_done()
        await self._set_nscf_done()
        self._set_ddk()
        await self.sequencer.write()
        await self._check_ddk_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scf(self):
        await self._set_scf_done()
        self._set_nscf()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_ddk_not_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_nscf()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_ddk_not_written()
        await self._check_optic_not_written()
        assert not await (self.sequencer.sequence_completed)


# #############################################################################
# ######################## TEST CASE FOR BAND STRUCTURE SEQUENCER #############
# #############################################################################
@pytest.mark.order("first")
class TestAbinitBandStructureSequencer(
        BaseAbinitSCFSequencerTest,
        BaseBandStructureSequencerTest,
        ):
    """Test case for the AbinitBandStructureSequencer class.
    """
    _band_structure_example = abinit_band_structure_example
    _band_structure_input_variables = abinit_band_structure_input_variables
    _band_structure_kpoint_path = abinit_band_structure_kpoint_path
    _band_structure_kpoint_path_density = (
            abinit_band_structure_kpoint_path_density)
    _scf_example = abinit_gs_example
    _scf_input_variables = abinit_gs_input_variables
    _sequencer_class = AbinitBandStructureSequencer
    _pseudos = abinit_gs_pseudos

    @pytest.fixture(autouse=True)
    def setup_abinit_band_structure_sequencer(
            self, setup_abinit_scf_sequencer,
            setup_band_structure_sequencer,
            ):
        yield


@pytest.mark.order("first")
class TestAbinitBandStructureComparatorSequencer(
        BaseAbinitSCFSequencerTest,
        BaseBandStructureSequencerTest):
    """Test case for the AbinitBandStructureSequencer class.
    """
    _band_structure_example = os.path.join(
            here, "..", "test_workflows", "test_files",
            "abinit_si", "nc", "band_structure_ecut_convergence",
            "band_structure_runs",
            )
    _band_structure_input_variables = abinit_band_structure_input_variables
    _band_structure_kpoint_path = abinit_band_structure_kpoint_path
    _band_structure_kpoint_path_density = (
            abinit_band_structure_kpoint_path_density)
    _scf_example = os.path.join(
            here, "..", "test_workflows", "test_files",
            "abinit_si", "nc", "band_structure_ecut_convergence",
            "scf_runs",
            )
    _scf_input_variables = {
        "ecut": 10, "istwfk": "*1", "kptopt": 1, "ngkpt": [6] * 3,
        "nshiftk": 4,
        "shiftk": [[.5, .5, .5], [.5, 0, 0], [0, .5, 0], [0, 0, .5]],
        "diemac": 12, "nstep": 10, "tolvrs": 1e-10,
        "acell": [7.2078778121] * 3,
        "natom": 2, "ntypat": 1,
        "rprim": [
            [0, 0.70710678119, 0.70710678119],
            [0.70710678119, 0, 0.70710678119],
            [0.70710678119, 0.70710678119, 0],
            ],
        "typat": [1] * 2, "xred": [[0] * 3, [.25] * 3],
        "znucl": 14,
        "pp_dirpath": "",
        "pseudos": abinit_gs_pseudos,
        }
    _sequencer_class = AbinitBandStructureComparatorSequencer
    _pseudos = abinit_gs_pseudos

    @pytest.fixture(autouse=True)
    def setup_abinit_band_structure_comparator_sequencer(
            self, setup_abinit_scf_sequencer, setup_band_structure_sequencer,
            ):
        self.sequencer.scf_specific_input_variables = [
                {"ecut": 10}, {"ecut": 30}]
        self.sequencer.scf_workdir = os.path.join(
                self.sequencer.scf_workdir, "scf")
        self.sequencer.band_structure_workdir = os.path.join(
                self.sequencer.band_structure_workdir,
                "band_structure")

    async def _check_band_structure(self, true):
        dirname = os.path.dirname(self.sequencer.band_structure_workdir)
        calcs = [os.path.join(dirname, calc)
                 for calc in await aiofiles.os.listdir(dirname)]
        for calc in calcs:
            await self._check(calc, true)

    async def _check_scf(self, true):
        dirname = os.path.dirname(self.sequencer.scf_workdir)
        calcs = [os.path.join(dirname, calc)
                 for calc in await aiofiles.os.listdir(dirname)]
        for calc in calcs:
            await self._check(calc, true, msg=calc)

    async def _set_band_structure_done(self):
        dirname = os.path.dirname(self.sequencer.band_structure_workdir)
        for calc in await aiofiles.os.listdir(self._band_structure_example):
            ext = calc.split("_")[-1]
            await copy_calculation(
                    os.path.join(self._band_structure_example, calc),
                    os.path.join(dirname, calc),
                    new_parents=[self.sequencer.scf_workdir + "_" + ext],
                    new_pseudos=self._pseudos)

    async def _set_scf_done(self):
        dirname = os.path.dirname(self.sequencer.scf_workdir)
        for calc in await aiofiles.os.listdir(self._scf_example):
            await copy_calculation(
                    os.path.join(self._scf_example, calc),
                    os.path.join(dirname, calc),
                    new_pseudos=self._pseudos)


# #############################################################################
# ########################## FERMI SURFACE SEQUENCER TESTS ####################
# #############################################################################
@pytest.mark.order("first")
class TestAbinitFermiSurfaceSequencer(
        BaseAbinitNSCFSequencerTest,
        BaseFermiSurfaceSequencerTest,
        ):
    """Test case for the abinit Fermi surface sequencer."""
    _nscf_example = abinit_fermi_surface_nscf_example
    _nscf_kgrid = abinit_fermi_surface_nscf_kgrid
    _nscf_input_variables = abinit_fermi_surface_nscf_input_variables.copy()
    _scf_example = abinit_fermi_surface_scf_example
    _scf_input_variables = abinit_fermi_surface_scf_input_variables.copy()
    _pseudos = abinit_fermi_surface_pseudos
    _sequencer_class = AbinitFermiSurfaceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_dermi_surface_sequencer(
            self, setup_fermi_surface_sequencer,
            setup_abinit_scf_sequencer,
            ):
        yield


# ######### TEST case for DOS sequencer
@pytest.mark.order("first")
class TestAbinitDOSSequencer(
        BaseAbinitSCFSequencerTest,
        BaseDOSSequencerTest,
        ):
    """Test case for the AbinitDOSSequencer class."""
    _pseudos = abinit_gs_pseudos
    _scf_example = abinit_gs_example
    _scf_input_variables = abinit_gs_input_variables
    _dos_example = abinit_dos_example
    _dos_fine_kpoint_grid_variables = abinit_dos_fine_kpoint_grid_variables
    _dos_input_variables = abinit_dos_input_variables
    _sequencer_class = AbinitDOSSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_dos_sequencer(
            self, setup_abinit_scf_sequencer,
            setup_dos_sequencer,
            ):
        self.sequencer.dos_command = self.sequencer.scf_command
        yield

    async def test_starting_from_scratch(self):
        # SCF part
        self._set_scf()
        self._set_dos()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_dos_not_written()
        assert not await (self.sequencer.sequence_completed)
        # DOS part
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_dos_written()
        # cleanup and set everything done
        self.dos_tempdir.cleanup()
        await self._set_dos_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)

    async def _set_dos_done(self):
        await super()._set_dos_done(
                new_parents=[self.scf_workdir],
                new_pseudos=self._pseudos)


# #############################################################################
# ##################### GW Sequencers #########################################
# #############################################################################
class BaseAbinitWithGWScreeningSequencerTest(BaseAbinitNSCFSequencerTest):
    """Base class for all abinit claculations with GW screenings."""
    _pseudos = abinit_gs_pseudos
    _nscf_example = None
    _screening_example = None
    _screening_input_variables = None
    _scf_input_variables = {
            "acell": [7.2078778121] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "znucl": [14],
            "natom": 2,
            "typat": [1, 1],
            "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
            "ngkpt": [6, 6, 6],
            "toldfe": 1e-6,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
            "ecut": 10,
            "kptopt": 1,
            "diemac": 12.0,
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }

    @pytest.fixture
    def setup_abinit_with_gw_screening_sequencer(
            self, setup_abinit_nscf_sequencer,
            setup_screening_tempdir,
            ):
        if self._screening_example is None:
            raise DevError("Need to set '_screening_example'.")
        if self._screening_input_variables is None:
            raise DevError("Need to set '_screening_input_variables'.")
        self.screening_workdir = os.path.join(
                self.screening_tempdir.name, "screening_run")
        self.sequencer.screening_workdir = self.screening_workdir
        self.sequencer.screening_command = self.sequencer.scf_command
        self.sequencer.screening_queuing_system = "local"

    async def _check_screening(self, *args):
        await self._check(self.screening_workdir, *args)

    async def _check_screening_written(self):
        return await self._check_screening(True)

    async def _check_screening_not_written(self):
        return await self._check_screening(False)

    def _set_screening(self):
        self.sequencer.screening_input_variables = (
                self._screening_input_variables.copy())

    async def _set_screening_done(self):
        await copy_calculation(
                self._screening_example,
                self.screening_workdir,
                new_parents=[self.sequencer.nscf_workdir],
                new_pseudos=self._pseudos)

    @pytest.fixture
    def setup_screening_tempdir(self):
        self.screening_tempdir = tempfile.TemporaryDirectory()
        yield
        self.screening_tempdir.cleanup()


class BaseAbinitGWSequencerTest(BaseAbinitWithGWScreeningSequencerTest):
    """Base class for all Abinit GW sequencers."""
    _selfenergy_example = None
    _selfenergy_input_variables = None
    _nscf_input_variables = {
            "nband": 180,
            "tolwfr": 1e-12,
            "nbdbuf": 10,
            "ngkpt": [4, 4, 4],
            "nshiftk": 4,
            "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                       [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
            "ecut": 8.0,
            "diemac": 12.0,
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }

    @pytest.fixture(autouse=True)
    def setup_abinit_gw_sequencer(
            self, setup_abinit_with_gw_screening_sequencer,
            setup_selfenergy_tempdir,
            ):
        if self._selfenergy_example is None:
            raise DevError("Need to set '_selfenergy_example'.")
        if self._selfenergy_input_variables is None:
            raise DevError("Need to set '_selfenergy_input_variables'.")
        self.selfenergy_workdir = os.path.join(
                self.selfenergy_tempdir.name, "selfenergy_run")
        self.sequencer.selfenergy_workdir = self.selfenergy_workdir
        self.sequencer.selfenergy_command = self.sequencer.scf_command
        self.sequencer.selfenergy_queuing_system = "local"

    async def test_starting_from_scratch(self):
        # SCF part
        self._set_scf()
        self._set_nscf()
        self._set_screening()
        self._set_selfenergy()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_screening_not_written()
        await self._check_selfenergy_not_written()
        assert not await (self.sequencer.sequence_completed)
        # NSCF part
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_screening_not_written()
        await self._check_selfenergy_not_written()
        assert not await (self.sequencer.sequence_completed)
        # Screening part
        self.nscf_tempdir.cleanup()
        await self._set_nscf_done()
        await self.sequencer.write()
        await self._check_screening_written()
        await self._check_selfenergy_not_written()
        assert not await (self.sequencer.sequence_completed)
        # Self energy part
        self.screening_tempdir.cleanup()
        await self._set_screening_done()
        await self.sequencer.write()
        await self._check_selfenergy_written()
        assert not await (self.sequencer.sequence_completed)
        # cleanup and set everything done
        self.selfenergy_tempdir.cleanup()
        await self._set_selfenergy_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)

    async def _check_selfenergy(self, *args):
        await self._check(self.selfenergy_workdir, *args)

    async def _check_selfenergy_written(self):
        return await self._check_selfenergy(True)

    async def _check_selfenergy_not_written(self):
        return await self._check_selfenergy(False)

    def _set_selfenergy(self):
        self.sequencer.selfenergy_input_variables = (
                self._selfenergy_input_variables.copy())

    async def _set_selfenergy_done(self):
        await copy_calculation(
                self._selfenergy_example,
                self.selfenergy_workdir,
                new_parents=[
                    self.sequencer.nscf_workdir,
                    self.sequencer.screening_workdir],
                new_pseudos=self._pseudos)

    @pytest.fixture
    def setup_selfenergy_tempdir(self):
        self.selfenergy_tempdir = tempfile.TemporaryDirectory()
        yield
        self.selfenergy_tempdir.cleanup()


@pytest.mark.order("first")
class TestAbinitGWSequencer(BaseAbinitGWSequencerTest):
    """Test case for an abinit GW sequencer."""
    _scf_example = os.path.join(
            abinit_gw_example, "scf_run")
    _scf_input_variables = abinit_gw_scf_input_variables
    _nscf_example = os.path.join(
            abinit_gw_example, "nscf_run")
    _nscf_input_variables = abinit_gw_nscf_input_variables.copy()
    _screening_example = os.path.join(
            abinit_gw_example, "screening_run")
    _selfenergy_example = os.path.join(
            abinit_gw_example, "selfenergy_run")
    _selfenergy_input_variables = abinit_gw_selfenergy_input_variables
    _sequencer_class = AbinitGWSequencer
    _screening_input_variables = abinit_gw_screening_input_variables


class BaseAbinitGWConvergenceSequencerTest(BaseAbinitGWSequencerTest):
    """Base class for all GW convergence sequencers."""

    @pytest.fixture(autouse=True)
    def setup_abinit_gw_convergence_sequencer(
            self, setup_abinit_gw_sequencer,
            ):
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.selfenergy_workdir, "plot.pdf")

    async def test_starting_from_scratch(self):
        await BaseAbinitGWSequencerTest.test_starting_from_scratch(self)
        assert await aiofiles.os.path.isfile(self.sequencer.plot_save)

    async def _check_selfenergy(self, true):
        isdir = await aiofiles.os.path.isdir(self.selfenergy_workdir)
        if isdir:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.selfenergy_workdir):
            await self._check(
                    os.path.join(self.selfenergy_workdir, subdir), true)


@pytest.mark.order("first")
class TestAbinitGWSelfEnergynbandConvergenceSequencer(
        BaseAbinitGWConvergenceSequencerTest,
        ):
    """Test case for the nband covergence sequencer for GW self energy calc."""

    _scf_example = os.path.join(
            os.path.dirname(abinit_gw_self_energy_nband_convergence_example),
            "gs_run")
    _screening_example = os.path.join(
            abinit_gw_self_energy_nband_convergence_example, "screening_run")
    _selfenergy_example = os.path.join(
            abinit_gw_self_energy_nband_convergence_example, "selfenergy_run")
    _nscf_example = os.path.join(
            abinit_gw_self_energy_nband_convergence_example, "nscf_run")
    _screening_input_variables = (
            abinit_gw_self_energy_nband_convergence_screening_input_variables)
    _selfenergy_input_variables = (
            abinit_gw_self_energy_nband_convergence_selfenergy_input_variables)
    _sequencer_class = AbinitGWSelfEnergynbandConvergenceSequencer

    def _set_selfenergy(self):
        BaseAbinitGWConvergenceSequencerTest._set_selfenergy(self)
        self.sequencer.selfenergy_nbands = [25, 50, 100, 150, 180]
        self.sequencer.selfenergy_nband_convergence_criterion = 3

    async def _set_selfenergy_done(self):
        root = self._selfenergy_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                os.path.join(root, subdir),
                os.path.join(self.selfenergy_workdir, subdir),
                new_parents=[
                    self.sequencer.nscf_workdir,
                    self.sequencer.screening_workdir],
                new_pseudos=self._pseudos)


class BaseAbinitGWScreeningConvergenceSequencerTest(
        BaseAbinitGWConvergenceSequencerTest):
    """Base class for screening calculations convergence sequencers tests."""

    async def _check_screening(self, true):
        isdir = await aiofiles.os.path.isdir(self.screening_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.screening_workdir):
            await self._check(
                    os.path.join(self.screening_workdir, subdir), true)

    async def _set_screening_done(self, *args, **kwargs):
        root = self._screening_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                os.path.join(root, subdir),
                os.path.join(self.screening_workdir, subdir),
                new_parents=[
                    self.sequencer.nscf_workdir,
                    ],
                new_pseudos=self._pseudos)

    async def _set_selfenergy_done(self, *args, **kwargs):
        root = self._selfenergy_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                os.path.join(root, subdir),
                os.path.join(self.selfenergy_workdir, subdir),
                new_parents=[
                    self.sequencer.nscf_workdir,
                    os.path.join(self.sequencer.screening_workdir, subdir)],
                new_pseudos=self._pseudos)


@pytest.mark.order("first")
class TestAbinitGWScreeningnbandConvergenceSequencer(
        BaseAbinitGWScreeningConvergenceSequencerTest,
        ):
    """Test case for the nband covergence sequencer for GW screening calc."""

    _scf_example = os.path.join(
            os.path.dirname(abinit_gw_screening_nband_convergence_example),
            "gs_run")
    _screening_example = os.path.join(
            abinit_gw_screening_nband_convergence_example, "screening_run")
    _selfenergy_example = os.path.join(
            abinit_gw_screening_nband_convergence_example, "selfenergy_run")
    _nscf_example = os.path.join(
            abinit_gw_self_energy_nband_convergence_example, "nscf_run")
    _screening_input_variables = (
            abinit_gw_screening_nband_convergence_screening_input_variables)
    _selfenergy_input_variables = (
            abinit_gw_screening_nband_convergence_selfenergy_input_variables)
    _sequencer_class = AbinitGWScreeningnbandConvergenceSequencer

    async def _check_screening(self, *args, **kwargs):
        await BaseAbinitGWScreeningConvergenceSequencerTest._check_screening(
                self, *args, **kwargs)

    def _set_screening(self):
        BaseAbinitGWScreeningConvergenceSequencerTest._set_screening(self)
        self.sequencer.screening_nbands = [25, 50, 100, 150, 180]
        self.sequencer.screening_nband_convergence_criterion = 3

    async def _set_screening_done(self, *args, **kwargs):
        cls = BaseAbinitGWScreeningConvergenceSequencerTest
        await cls._set_screening_done(
                self, *args, **kwargs)

    async def _set_selfenergy_done(self, *args, **kwargs):
        cls = BaseAbinitGWScreeningConvergenceSequencerTest
        await cls._set_selfenergy_done(
                self, *args, **kwargs)


@pytest.mark.order("first")
class TestAbinitGWecutepsConvergenceSequencer(
        BaseAbinitGWScreeningConvergenceSequencerTest,
        ):
    """Test case for the ecuteps GW convergence sequencer."""

    _scf_example = abinit_gs_example
    _nscf_example = os.path.join(
            abinit_gw_self_energy_nband_convergence_example, "nscf_run")
    _screening_example = os.path.join(
            abinit_gw_ecuteps_convergence_example, "screening_run")
    _screening_input_variables = {
            "nband": 100,
            "ppmfrq": 16.7 * EV_TO_HA,
            }
    _selfenergy_input_variables = {
            "nband": 100,
            "ecutsigx": 8.0,
            "nkptgw": 1,
            "kptgw": [0.0, 0.0, 0.0],
            "bdgw": [4, 5],
            }
    _selfenergy_example = os.path.join(
            abinit_gw_ecuteps_convergence_example, "selfenergy_run")
    _sequencer_class = AbinitGWecutepsConvergenceSequencer

    def _set_screening(self):
        BaseAbinitGWScreeningConvergenceSequencerTest._set_screening(self)
        self.sequencer.screening_ecutepss = [2, 4, 6, 8]
        self.sequencer.screening_ecuteps_convergence_criterion = 30


@pytest.mark.order("first")
class TestAbinitGWKgridConvergenceSequencer(
        BaseAbinitConvergenceSequencerTest,
        BaseAbinitKgridConvergenceSequencerTest,
        BaseAbinitGWScreeningConvergenceSequencerTest,
        ):
    """Test case for the kgrid GW convergence sequencer."""

    _kgrids = [[x, x, x] for x in (2, 4, 8)]
    _scf_example = os.path.join(
            abinit_gw_kgrid_convergence_example, "scf_run")
    _scf_convergence_criterion = 5
    _scf_input_variables = abinit_gw_kgrid_convergence_scf_input_variables
    _nscf_example = os.path.join(
            abinit_gw_kgrid_convergence_example, "nscf_run")
    _nscf_input_variables = abinit_gw_kgrid_convergence_nscf_input_variables
    _screening_example = os.path.join(
            abinit_gw_kgrid_convergence_example, "screening_run")
    _selfenergy_example = os.path.join(
            abinit_gw_kgrid_convergence_example, "selfenergy_run")
    _screening_input_variables = (
            abinit_gw_kgrid_convergence_screening_input_variables)
    _selfenergy_input_variables = (
            abinit_gw_kgrid_convergence_selfenergy_input_variables)
    _sequencer_class = AbinitGWKgridConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_abinit_gw_kgrid_convergence_sequencer(
            self, setup_abinit_kgrid_convergence_sequencer,
            setup_abinit_convergence_sequencer,
            setup_abinit_gw_convergence_sequencer,
            ):
        yield

    @unittest.skip
    def test_whole_sequence(self):
        # same as the test_starting_from_scratch test
        pass

    async def _check_nscf(self, true):
        isdir = await aiofiles.os.path.isdir(self.nscf_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.nscf_workdir):
            await self._check(os.path.join(self.nscf_workdir, subdir), true)

    async def _check_scf_written(self, *args, **kwargs):
        await BaseKgridConvergenceSequencerTest._check_scf_written(
                self, *args, use_parameter_subdirs=False, **kwargs)

    async def _set_scf_done(self, *args, **kwargs):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(
                self, *args, use_parameter_subdirs=False, **kwargs)

    def _set_screening(self):
        BaseAbinitGWScreeningConvergenceSequencerTest._set_screening(self)

    async def _set_nscf_done(self, *args, **kwargs):
        root = self._nscf_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.nscf_workdir, subdir),
                    new_parents=[os.path.join(self.scf_workdir, subdir)],
                    new_pseudos=self._pseudos,
                    )

    async def _set_screening_done(self, *args, **kwargs):
        root = self._screening_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.screening_workdir, subdir),
                    new_pseudos=self._pseudos,
                    new_parents=[os.path.join(self.nscf_workdir, subdir)])

    async def _set_selfenergy_done(self, *args, **kwargs):
        root = self._selfenergy_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.selfenergy_workdir, subdir),
                    new_pseudos=self._pseudos,
                    new_parents=[os.path.join(self.screening_workdir, subdir),
                                 os.path.join(self.nscf_workdir, subdir)])


# #############################################################################
# ###################### BSE Sequencers #######################################
# #############################################################################

class BaseAbinitBSESequencerTest(BaseAbinitWithGWScreeningSequencerTest):
    """Base class for abinit BSE sequencers test cases."""
    _scf_input_variables = abinit_bse_scf_input_variables
    _nscf_input_variables = abinit_bse_nscf_input_variables
    _nscfnosym_input_variables = abinit_bse_nscfnosym_input_variables
    _screening_input_variables = abinit_bse_screening_input_variables
    _nscfnosym_example = None
    _bse_example = None
    _bse_input_variables = None

    @pytest.fixture(autouse=True)
    def setup_abinit_bse_sequencer(
            self, setup_abinit_with_gw_screening_sequencer,
            setup_nscfnosym_tempdir, setup_bse_tempdir,
            ):
        if self._bse_example is None:
            raise DevError("Need to set '_bse_example'.")
        if self._bse_input_variables is None:
            raise DevError("Need to set '_bse_input_variables'.")
        if self._nscfnosym_input_variables is None:
            raise DevError("Need to set '_nscfnosym_input_variables'.")
        if self._nscfnosym_example is None:
            raise DevError("Need to set '_nscfnosym_example'.")
        self.nscfnosym_workdir = os.path.join(
                self.nscfnosym_tempdir.name, "nscfnosym_run")
        self.bse_workdir = os.path.join(
                self.bse_tempdir.name, "bse_run")
        self.sequencer.nscfnosym_workdir = self.nscfnosym_workdir
        self.sequencer.nscfnosym_command = self.sequencer.scf_command
        self.sequencer.nscfnosym_queuing_system = "local"
        self.sequencer.bse_workdir = self.bse_workdir
        self.sequencer.bse_command = self.sequencer.scf_command
        self.sequencer.bse_queuing_system = "local"
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.bse_workdir, "plot.pdf")

    async def test_starting_from_scratch(self):
        # SCF part
        self._set_scf()
        self._set_nscf()
        self._set_screening()
        self._set_nscfnosym()
        self._set_bse()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_screening_not_written()
        await self._check_nscfnosym_not_written()
        await self._check_bse_not_written()
        assert not await (self.sequencer.sequence_completed)
        # NSCF part
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_nscfnosym_written()
        await self._check_screening_not_written()
        await self._check_bse_not_written()
        assert not await (self.sequencer.sequence_completed)
        # Screening part
        self.nscf_tempdir.cleanup()
        await self._set_nscf_done()
        await self.sequencer.write()
        await self._check_screening_written()
        await self._check_bse_not_written()
        assert not await (self.sequencer.sequence_completed)
        # BSE part
        self.screening_tempdir.cleanup()
        await self._set_screening_done()
        await self.sequencer.write()
        self.nscfnosym_tempdir.cleanup()
        await self._set_nscfnosym_done()
        await self.sequencer.write()
        await self._check_bse_written()
        assert not await (self.sequencer.sequence_completed)
        # cleanup and set everything done
        self.bse_tempdir.cleanup()
        await self._set_bse_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)
        assert await (aiofiles.os.path.isfile(self.sequencer.plot_save))

    async def _check_nscfnosym(self, *args):
        await self._check(self.nscfnosym_workdir, *args)

    async def _check_bse(self, *args):
        await self._check(self.bse_workdir, *args)

    async def _check_nscfnosym_written(self):
        return await self._check_nscfnosym(True)

    async def _check_nscfnosym_not_written(self):
        return await self._check_nscfnosym(False)

    def _check_bse_written(self):
        return self._check_bse(True)

    async def _check_bse_not_written(self):
        return await self._check_bse(False)

    def _set_nscfnosym(self):
        self.sequencer.nscfnosym_input_variables = (
                self._nscfnosym_input_variables.copy())

    def _set_bse(self):
        self.sequencer.bse_input_variables = (
                self._bse_input_variables.copy())

    async def _set_nscfnosym_done(self):
        await copy_calculation(
                self._nscfnosym_example,
                self.nscfnosym_workdir,
                new_parents=[
                    self.sequencer.scf_workdir,
                    ],
                new_pseudos=self._pseudos)

    async def _set_bse_done(self):
        await copy_calculation(
                self._bse_example,
                self.bse_workdir,
                new_parents=[
                    self.sequencer.nscfnosym_workdir,
                    self.sequencer.screening_workdir,
                    ],
                new_pseudos=self._pseudos)

    @pytest.fixture
    def setup_nscfnosym_tempdir(self):
        self.nscfnosym_tempdir = tempfile.TemporaryDirectory()
        yield
        self.nscfnosym_tempdir.cleanup()

    @pytest.fixture
    def setup_bse_tempdir(self):
        self.bse_tempdir = tempfile.TemporaryDirectory()
        yield
        self.bse_tempdir.cleanup()


@pytest.mark.order("first")
class TestAbinitBSESequencer(BaseAbinitBSESequencerTest):
    """Test case for the abinit BSE sequencer."""
    _scf_example = os.path.join(
            abinit_bse_example, "scf_run")
    _nscf_example = os.path.join(
            abinit_bse_example, "nscf_run")
    _nscfnosym_example = os.path.join(
            abinit_bse_example, "nscfnosym_run")
    _screening_example = os.path.join(
            abinit_bse_example, "screening_run")
    _bse_example = os.path.join(
            abinit_bse_example, "bse_run")
    _sequencer_class = AbinitBSESequencer
    _bse_input_variables = {
            "bs_calctype": 1,
            "mbpt_sciss": 0.8 * EV_TO_HA,
            "bs_exchange_term": 1,
            "bs_coulomb_term": 11,
            "bs_coupling": 0,
            "bs_loband": 2,
            "nband": 7,
            "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
            "bs_algorithm": 2,
            "bs_haydock_niter": 200,
            "bs_haydock_tol": [0.05, 0],
            "zcut": 0.15 * EV_TO_HA,
            "kptopt": 1,
            "chksymbreak": 0,
            "ecutwfn": 12.0,
            "ecuteps": 3.0,
            "inclvkb": 2,
            "gw_icutcoul": 3,
            }


class BaseAbinitBSEConvergenceSequencerTest(BaseAbinitBSESequencerTest):
    """Base class for abinit bse convergence sequencers tests."""

    @pytest.fixture(autouse=True)
    def setup_abinit_bse_convergence_sequencer(
            self, setup_abinit_bse_sequencer):
        yield

    async def _check_bse(self, true):
        isdir = await aiofiles.os.path.isdir(self.bse_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.bse_workdir):
            await self._check(os.path.join(self.bse_workdir, subdir), true)

    async def _set_bse_done(self):
        root = self._bse_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                os.path.join(root, subdir),
                os.path.join(self.bse_workdir, subdir),
                new_parents=[
                    self.sequencer.nscfnosym_workdir,
                    self.sequencer.screening_workdir,
                    ],
                new_pseudos=self._pseudos)


@pytest.mark.order("first")
class TestAbinitBSEnbandConvergenceSequencer(
        BaseAbinitBSEConvergenceSequencerTest):
    """Test case for the abinit bse nband convergence sequencer."""

    _sequencer_class = AbinitBSEnbandConvergenceSequencer
    _scf_example = os.path.join(
            abinit_bse_nband_convergence_example, "scf_run")
    _nscf_example = os.path.join(
            abinit_bse_nband_convergence_example, "nscf_run")
    _nscfnosym_example = os.path.join(
            abinit_bse_nband_convergence_example, "nscfnosym_run")
    _screening_example = os.path.join(
            abinit_bse_nband_convergence_example, "screening_run")
    _bse_example = os.path.join(
            abinit_bse_nband_convergence_example, "bse_run")
    _bse_input_variables = (
            abinit_bse_ecuteps_convergence_bse_input_variables)

    def _set_bse(self):
        BaseAbinitBSEConvergenceSequencerTest._set_bse(self)
        self.sequencer.bs_lobands = [2, 3, 2]
        self.sequencer.bs_nbands = [8, 6, 7]


@pytest.mark.order("first")
class TestAbinitBSEecutepsConvergenceSequencer(
        BaseAbinitBSEConvergenceSequencerTest):
    """Test case for the abinit bse ecuteps convergence sequencer."""

    _sequencer_class = AbinitBSEecutepsConvergenceSequencer
    _scf_example = os.path.join(
            abinit_bse_nband_convergence_example, "scf_run")
    _nscf_example = os.path.join(
            abinit_bse_nband_convergence_example, "nscf_run")
    _nscfnosym_example = os.path.join(
            abinit_bse_nband_convergence_example, "nscfnosym_run")
    _screening_example = os.path.join(
            abinit_bse_nband_convergence_example, "screening_run")
    _bse_example = os.path.join(
            abinit_bse_ecuteps_convergence_example, "bse_run")
    _bse_input_variables = {
            "bs_calctype": 1,
            "mbpt_sciss": 0.8 * EV_TO_HA,
            "bs_exchange_term": 1,
            "bs_coulomb_term": 11,
            "bs_coupling": 0,
            "bs_loband": 2,
            "nband": 7,
            "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
            "bs_algorithm": 2,
            "bs_haydock_niter": 200,
            "bs_haydock_tol": [0.05, 0],
            "zcut": 0.15 * EV_TO_HA,
            "kptopt": 1,
            "chksymbreak": 0,
            "ecutwfn": 12.0,
            "inclvkb": 2,
            "gw_icutcoul": 3,
            }

    def _set_bse(self):
        BaseAbinitBSEConvergenceSequencerTest._set_bse(self)
        self.sequencer.bs_ecutepss = [1, 2, 3, 4]


@pytest.mark.order("first")
class TestAbinitBSEngkptConvergenceSequencer(
        BaseAbinitConvergenceSequencerTest,
        BaseKgridConvergenceSequencerTest,
        BaseAbinitBSEConvergenceSequencerTest,
        ):
    """Test case for the abinit bse ngkpt convergence sequencer."""
    _sequencer_class = AbinitBSEKgridConvergenceSequencer
    _scf_example = os.path.join(
            abinit_bse_kgrid_convergence_example,
            "scf_run")
    _kgrids = [[x, x, x] for x in (4, 5, 6)]
    _nscf_example = os.path.join(
            abinit_bse_kgrid_convergence_example, "nscf_run")
    _nscfnosym_example = os.path.join(
            abinit_bse_kgrid_convergence_example, "nscfnosym_run")
    _screening_example = os.path.join(
            abinit_bse_kgrid_convergence_example, "screening_run")
    _bse_example = os.path.join(
            abinit_bse_kgrid_convergence_example, "bse_run")
    _bse_input_variables = {
            "bs_calctype": 1,
            "mbpt_sciss": 0.8 * EV_TO_HA,
            "bs_exchange_term": 1,
            "bs_coulomb_term": 11,
            "bs_coupling": 0,
            "bs_loband": 2,
            "ecuteps": 3.0,
            "nband": 7,
            "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
            "bs_algorithm": 2,
            "bs_haydock_niter": 200,
            "bs_haydock_tol": [0.05, 0],
            "zcut": 0.15 * EV_TO_HA,
            "kptopt": 1,
            "chksymbreak": 0,
            "ecutwfn": 12.0,
            "inclvkb": 2,
            "gw_icutcoul": 3,
            }

    @pytest.fixture(autouse=True)
    def setup_abinit_bse_kgrid_convergence_sequencer(
            self, setup_kgrid_convergence_sequencer,
            setup_abinit_convergence_sequencer,
            setup_abinit_bse_convergence_sequencer,
            ):
        self.sequencer.kgrids_input_variable_name = (
                abinit_bse_kgrid_convergence_input_variable_name)
        yield

    async def test_starting_from_scratch(self):
        await BaseAbinitBSEConvergenceSequencerTest.test_starting_from_scratch(
                self)

    @unittest.skip
    def test_whole_sequence(self):
        # same as the test_starting_from_scratch test
        pass

    async def _check_nscf(self, true):
        isdir = await aiofiles.os.path.isdir(self.nscf_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.nscf_workdir):
            await self._check(os.path.join(self.nscf_workdir, subdir), true)

    async def _check_nscfnosym(self, true):
        isdir = await aiofiles.os.path.isdir(self.nscfnosym_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.nscfnosym_workdir):
            await self._check(
                    os.path.join(self.nscfnosym_workdir, subdir), true)

    async def _check_screening(self, true):
        isdir = await aiofiles.os.path.isdir(self.screening_workdir)
        if true:
            assert isdir
        else:
            assert not isdir
            return
        for subdir in await aiofiles.os.listdir(self.screening_workdir):
            await self._check(
                    os.path.join(self.screening_workdir, subdir), true)

    async def _check_scf_written(self, *args, **kwargs):
        await BaseKgridConvergenceSequencerTest._check_scf_written(
                self, *args, use_parameter_subdirs=False, **kwargs)

    async def _set_scf_done(self, *args, **kwargs):
        await BaseAbinitConvergenceSequencerTest._set_scf_done(
                self, *args, use_parameter_subdirs=False, **kwargs)

    async def _set_nscf_done(self, *args, **kwargs):
        root = self._nscf_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.nscf_workdir, subdir),
                    new_parents=[os.path.join(self.scf_workdir, subdir)],
                    new_pseudos=self._pseudos,
                    )

    async def _set_nscfnosym_done(self, *args, **kwargs):
        root = self._nscfnosym_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.nscfnosym_workdir, subdir),
                    new_parents=[os.path.join(self.scf_workdir, subdir)],
                    new_pseudos=self._pseudos,
                    )

    async def _set_screening_done(self, *args, **kwargs):
        root = self._screening_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.screening_workdir, subdir),
                    new_pseudos=self._pseudos,
                    new_parents=[os.path.join(self.nscf_workdir, subdir)])

    async def _set_bse_done(self, *args, **kwargs):
        root = self._bse_example
        for subdir in await aiofiles.os.listdir(root):
            await copy_calculation(
                    os.path.join(root, subdir),
                    os.path.join(self.bse_workdir, subdir),
                    new_pseudos=self._pseudos,
                    new_parents=[os.path.join(self.screening_workdir, subdir),
                                 os.path.join(self.nscfnosym_workdir, subdir)])
