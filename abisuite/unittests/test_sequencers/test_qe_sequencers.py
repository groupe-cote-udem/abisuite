import asyncio
import os
import tempfile

import aiofiles.os

import pytest

from .bases import (
        BaseBandStructureSequencerTest,
        BaseConvergenceSequencerTest, BaseDOSSequencerTest,
        BaseEcutConvergenceSequencerTest,
        BaseEcutPhononConvergenceSequencerTest,
        BaseFermiSurfaceSequencerTest,
        BaseKgridConvergenceSequencerTest,
        BaseKgridPhononConvergenceSequencerTest,
        BaseNSCFSequencerTest,
        BasePhononConvergenceSequencerTest,
        BasePhononSequencerTest,
        BasePositiveKpointsNSCFSequencerTest,
        BaseRelaxationSequencerTest,
        BaseSCFSequencerTest,
        BaseSequencerTest,
        BaseSmearingConvergenceSequencerTest,
        BaseSmearingPhononConvergenceSequencerTest,
        )
from ..routines_for_tests import (
        TemporaryCalculation, copy_calculation,
        )
from ..variables_for_tests import (
        qe_dos_example,
        qe_dos_fine_kpoint_grid_variables,
        qe_dos_input_variables, qe_dos_nscf_example,
        qe_dos_nscf_input_variables,
        qe_epw_wannierization_band_structure_example,
        qe_epw_wannierization_band_structure_input_variables,
        qe_epw_wannierization_band_structure_kpoint_path,
        qe_epw_wannierization_band_structure_kpoint_path_density,
        qe_epw_wannierization_epw_input_variables,
        qe_epw_wannierization_example,
        qe_epw_wannierization_matdyn_example,
        qe_epw_wannierization_nscf_example,
        qe_epw_wannierization_nscf_input_variables,
        qe_epw_wannierization_nscf_kgrid,
        qe_epw_wannierization_phonon_dispersion_example,
        qe_epw_wannierization_phonon_dispersion_input_variables,
        # qe_epw_wannierization_phonon_dispersion_qpath,
        # qe_epw_wannierization_phonon_dispersion_qpath_density,
        # qe_epw_wannierization_phonon_dispersion_qpoint_grid,
        qe_epw_wannierization_pseudo_dir,
        qe_epw_wannierization_q2r_example,
        qe_epw_wannierization_scf_example,
        qe_epw_wannierization_scf_input_variables,
        qe_fermi_surface_example,
        qe_fermi_surface_nscf_input_variables,
        qe_fermi_surface_nscf_kgrid,
        qe_gs_smearing_convergence_example,
        qe_gs_smearing_convergence_kgrids,
        qe_gs_smearing_convergence_scf_input_variables,
        qe_gs_smearing_convergence_smearings,
        qe_ibte_example, qe_ibte_input_variables,
        qe_phonon_dispersion_asr,
        qe_phonon_dispersion_matdyn_input_variables,
        qe_phonon_dispersion_phonons_example,
        qe_phonon_dispersion_phonons_input_variables,
        qe_phonon_dispersion_phonons_qpoint_grid,
        qe_phonon_dispersion_q2r_input_variables,
        qe_phonon_dispersion_qpath,
        qe_phonon_dispersion_qpath_density,
        qe_phonon_kgrid_convergence_phonons_examples,
        qe_phonon_kgrid_convergence_phonons_input_variables,
        qe_phonon_kgrid_convergence_pseudo_dir,
        qe_phonon_kgrid_convergence_scf_examples,
        qe_phonon_kgrid_convergence_scf_input_variables,
        qe_phonon_kgrid_convergence_scf_kgrids,
        qe_phonon_smearing_convergence_example,
        qe_phonon_smearing_convergence_phonons_input_variables,
        qe_phonon_smearing_convergence_scf_input_variables,
        qe_relaxation_example,
        qe_relaxation_input_variables,
        qe_scf_example, qe_scf_input_variables, qe_scf_pseudo_dir,
        qe_thermal_lattice_expansion_deltas,
        qe_thermal_lattice_expansion_example,
        qe_thermal_lattice_expansion_fine_qpoint_grid,
        qe_thermal_lattice_expansion_phdisp_nqpts,
        qe_thermal_lattice_expansion_phdisp_qpath,
        qe_thermal_lattice_expansion_phonons_input_variables,
        qe_thermal_lattice_expansion_phonons_qgrid,
        qe_thermal_lattice_expansion_scf_input_variables,
        qe_ziman_example, qe_ziman_input_variables,
        )
from ...exceptions import DevError
from ...handlers import (
    CALCTYPES_TO_INPUT_FILE_CLS, is_calculation_directory,
    )
from ...sequencers import (
        QEDOSSequencer,
        QEEPWIBTESequencer,
        QEEPWSequencer,
        QEEPWZimanSequencer,
        QEEcutConvergenceSequencer,
        QEEcutPhononConvergenceSequencer,
        QEFermiSurfaceSequencer, QEIndividualPhononSequencer,
        QEKgridConvergenceSequencer, QEKgridPhononConvergenceSequencer,
        QEPhononDispersionSequencer,
        QERelaxationSequencer,
        QESCFSequencer,
        QESmearingConvergenceSequencer,
        QESmearingPhononConvergenceSequencer,
        QEThermalExpansionSequencer,
        )


here = os.path.dirname(os.path.abspath(__file__))
SCF_PSEUDO_DIR = os.path.join(here, "files", "pseudos")


class BaseQESCFSequencerTest(BaseSCFSequencerTest):
    """Base class for scf sequencer test cases with Quantum Espresso.
    """
    _scf_pseudo_dir = None
    _scf_script_name = "pw.x"

    @pytest.fixture(autouse=True)
    async def setup_qe_scf_sequencer(self, setup_scf_sequencer):
        if self._scf_pseudo_dir is None:
            raise DevError(
                    f"Need to set '_scf_pseudo_dir' in '{self.__class__}'.")

    async def _set_scf_done(self):
        await BaseSCFSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


# #############################################################################
# ####################### QERelaxationSequencer ###############################
# #############################################################################


@pytest.mark.order("first")
class TestQERelaxationSequencer(
        BaseQESCFSequencerTest, BaseRelaxationSequencerTest):
    """Test case for the QERelaxationSequencer.
    """
    _scf_example = qe_relaxation_example
    _scf_input_variables = qe_relaxation_input_variables.copy()
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QERelaxationSequencer

    @pytest.fixture(autouse=True)
    async def setup_qe_relaxation_sequencer(self, setup_qe_scf_sequencer):
        self.sequencer.maximum_relaxations = 3

    async def test_relax_atoms_only(self) -> None:
        # no atoms to relax for Pb -> skip this
        return

    async def test_relax_atoms_and_cell(self):
        # no atoms to relax for Pb -> skip this
        pass

    async def _set_relax_atoms_done(self):
        # nothing to be done for lead
        pass
        # await copy_calculation(
        #         os.path.join(self._scf_example, "relax_atoms_only"),
        #         os.path.join(self.sequencer.scf_workdir, "relax_atoms_only"),
        #         pseudo_dir=self._scf_pseudo_dir)

    async def _set_relax_cell_done(self):
        for basename in await aiofiles.os.listdir(self._scf_example):
            if "atoms_only" in basename:
                continue
            ending = int(basename.split("_")[-1])
            if ending > 1:
                depends_on = os.path.join(
                    basename.replace("_" + str(ending), "_" + str(ending - 1)))
            else:
                if self.sequencer.relax_atoms:
                    depends_on = "relax_atoms_only"
                else:
                    depends_on = None
            if depends_on is not None:
                depends_on = [
                        os.path.join(self.sequencer.scf_workdir, depends_on)]
            else:
                depends_on = []
            await copy_calculation(
                    os.path.join(self._scf_example, basename),
                    os.path.join(self.sequencer.scf_workdir, basename),
                    pseudo_dir=self._scf_pseudo_dir,
                    new_parents=depends_on)


class BaseQEConvergenceSequencerTest(
        BaseConvergenceSequencerTest, BaseQESCFSequencerTest):
    """Base class for ecut convergence sequencer with QE.
    """

    @pytest.fixture(autouse=True)
    def setup_qe_convergence_sequencer(
            self, setup_convergence_sequencer):
        yield

    async def _set_scf_done(self):
        await BaseEcutConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


class BaseQEPhononSequencerTest(
        BasePhononSequencerTest, BaseQESCFSequencerTest):
    """Base class for phonons sequencer test cases with Quantum Espresso.
    """
    _phonons_script_name = "ph.x"
    _phonons_qpoint_grid = None

    @pytest.fixture(autouse=True)
    def setup_qe_phonon_sequencer(
            self, setup_phonon_sequencer):
        yield

    def _set_phonons(self):
        if self._phonons_qpoint_grid is not None:
            self.sequencer.phonons_qpoint_grid = self._phonons_qpoint_grid
        BasePhononSequencerTest._set_phonons(self)


class BaseQEPhononConvergenceSequencerTest(
        BasePhononConvergenceSequencerTest, BaseQEPhononSequencerTest):
    """Base test case class for phonons convergence sequencers with QE.
    """

    @pytest.fixture(autouse=True)
    def setup_qe_phonon_convergence_sequencer(
            self, setup_phonon_convergence_sequencer):
        yield


class BaseQEBandStructureSequencerTest(
        BaseBandStructureSequencerTest, BaseQESCFSequencerTest):
    """Base class for Band Sructures sequencers test cases with Quantum
    Espresso.
    """
    @pytest.fixture(autouse=True)
    def setup_qe_band_structure_sequencer(
            self, setup_band_structure_sequencer):
        yield

    async def _set_band_structure_done(self):
        await super()._set_band_structure_done()
        # need to reset pseudo dir when copying calcdir
        async with await CALCTYPES_TO_INPUT_FILE_CLS["qe_pw"].from_calculation(
                        self.sequencer.band_structure_workdir) as input_file:
            input_file.input_variables["pseudo_dir"] = self._scf_pseudo_dir


# ############################################################################
# ################ SCF sequencer with Quantum Espresso #######################
# ############################################################################


@pytest.mark.order("first")
class TestQESCFSequencer(BaseQESCFSequencerTest):
    """Test case for the SCF sequencer with Quantum Espresso.
    """
    _scf_example = qe_scf_example
    _scf_input_variables = qe_scf_input_variables.copy()
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QESCFSequencer


SCF_NO_ECUT_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    "k_points": {
        "parameter": "automatic",
        "k_points": [4, 4, 4, 0, 0, 0],
        },
    }
ECUTS = [10, 20]
root = os.path.join(
        here, "files", "qe_silicon_ecut_convergence", "scf_run", "ecutwfc")
SCF_ECUTS_EXAMPLES = [os.path.join(root, x) for x in os.listdir(root)]


# ####################### QE Ecut convergence sequencer #######################
@pytest.mark.order("first")
class TestQEEcutConvergenceSequencer(
        BaseEcutConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest):
    """Test case for the QE Ecut convergence sequencer.
    """
    _ecuts = ECUTS
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEEcutConvergenceSequencer

    async def _set_scf_done(self):
        await BaseQEConvergenceSequencerTest._set_scf_done(self)


PHONON_CONVERGENCE_INPUT_VARIABLES = {
        "amass(1)": 28.085,
        }
root = os.path.join(
        here, "files", "qe_silicon_ecut_convergence", "phonon_run", "ecutwfc")
PHONONS_ECUT_CONVERGENCE_EXAMPLES = [
        os.path.join(root, x) for x in os.listdir(root)]


# ################### QE Ecut phonon convergence sequencer ####################
@pytest.mark.order("first")
class TestQEEcutPhononConvergenceSequencer(
        BaseEcutPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest):
    """Test case for the QE ecut phonons convergence sequencer."""
    _ecuts = ECUTS
    _phonons_input_variables = PHONON_CONVERGENCE_INPUT_VARIABLES.copy()
    _phonons_example = PHONONS_ECUT_CONVERGENCE_EXAMPLES
    _scf_input_variables = SCF_NO_ECUT_INPUT_VARIABLES.copy()
    _scf_example = SCF_ECUTS_EXAMPLES
    _scf_pseudo_dir = SCF_PSEUDO_DIR
    _sequencer_class = QEEcutPhononConvergenceSequencer

    async def _set_scf_done(self):
        await BaseQEConvergenceSequencerTest._set_scf_done(self)


SCF_NO_KGRID_INPUT_VARIABLES = {
    "ibrav": 2,
    "celldm(1)": 10.3472,
    "ntyp": 1,
    "nat": 2,
    "ecutwfc": 10.0,
    "conv_thr": 1e-8,
    "pseudo_dir": SCF_PSEUDO_DIR,
    "atomic_species": [{"atom": "Si",
                        "pseudo": "Si.pbe-mt_fhi.UPF",
                        "atomic_mass": 28.085, }],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {"Si": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]]}},
    }
KGRIDS = [[4, 4, 4, 0, 0, 0], [5, 5, 5, 0, 0, 0]]
SCF_KGRIDS_EXAMPLES = [
        os.path.join(
            here, "files", "qe_silicon_kgrid_convergence/scf_run/",
            f"k_points_{x[0]}_{x[1]}_{x[2]}")
        for x in KGRIDS]
PHONONS_KGRID_CONVERGENCE_EXAMPLES = [
        os.path.join(
            here, "files", "qe_silicon_kgrid_convergence/ph_runs/",
            f"k_points_{x[0]}_{x[1]}_{x[2]}")
        for x in KGRIDS]


# ################ QE Kgrid Convergence Sequencer #############################
@pytest.mark.order("first")
class TestQEKgridConvergenceSequencer(
        BaseKgridConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest):
    """Test case for the QE Kgrid convergence sequencer.
    """
    _kgrids = qe_phonon_kgrid_convergence_scf_kgrids
    _scf_input_variables = (
            qe_phonon_kgrid_convergence_scf_input_variables.copy())
    _scf_example = qe_phonon_kgrid_convergence_scf_examples
    _scf_pseudo_dir = qe_phonon_kgrid_convergence_pseudo_dir
    _sequencer_class = QEKgridConvergenceSequencer

    async def _set_scf_done(self):
        await BaseQEConvergenceSequencerTest._set_scf_done(self)


# ########################## QE Kgrid Phonon Convergence Sequencer ############
@pytest.mark.order("first")
class TestQEKgridPhononConvergenceSequencer(
        BaseKgridPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest,
        ):
    """Test case for the QE kgrids phonon convergence sequencer."""
    _kgrids = qe_phonon_kgrid_convergence_scf_kgrids
    _phonons_input_variables = (
            qe_phonon_kgrid_convergence_phonons_input_variables.copy())
    _phonons_example = qe_phonon_kgrid_convergence_phonons_examples
    _scf_input_variables = (
            qe_phonon_kgrid_convergence_scf_input_variables.copy())
    _scf_example = qe_phonon_kgrid_convergence_scf_examples
    _scf_pseudo_dir = qe_phonon_kgrid_convergence_pseudo_dir
    _sequencer_class = QEKgridPhononConvergenceSequencer

    async def _set_scf_done(self):
        await BaseQEConvergenceSequencerTest._set_scf_done(self)


# ############### QE smearing convergence sequencer ###########################


@pytest.mark.order("first")
class TestQESmearingConvergenceSequencer(
        BaseSmearingConvergenceSequencerTest,
        BaseQEConvergenceSequencerTest):
    """Test case for the QE smearing convergence sequencer.
    """
    _kgrids = qe_gs_smearing_convergence_kgrids
    _scf_input_variables = (
            qe_gs_smearing_convergence_scf_input_variables.copy())
    _scf_example = qe_gs_smearing_convergence_example
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QESmearingConvergenceSequencer
    _smearings = qe_gs_smearing_convergence_smearings
    _scf_convergence_criterion = 10

    @pytest.fixture(autouse=True)
    def setup_qe_smearing_convergence_sequencer(
            self, setup_smearing_convergence_sequencer,
            setup_qe_convergence_sequencer,
            ):
        yield

    async def _set_scf_done(self):
        await BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


@pytest.mark.order("first")
class TestQESmearingPhononConvergenceSequencer(
        BaseSmearingPhononConvergenceSequencerTest,
        BaseQEPhononConvergenceSequencerTest):
    """Test case for the QE smearing convergence sequencer.
    """
    _kgrids = qe_gs_smearing_convergence_kgrids
    _smearings = qe_gs_smearing_convergence_smearings
    _nphonons = 1
    _phonons_example = os.path.join(
            qe_phonon_smearing_convergence_example, "phonons_runs")
    _phonons_input_variables = (
            qe_phonon_smearing_convergence_phonons_input_variables)
    _scf_input_variables = qe_phonon_smearing_convergence_scf_input_variables
    _scf_example = os.path.join(
            qe_phonon_smearing_convergence_example, "scf_runs")
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QESmearingPhononConvergenceSequencer

    @pytest.fixture(autouse=True)
    def setup_qe_smearing_phonon_convergence_sequencer(
            self, setup_smearing_phonon_convergence_sequencer,
            setup_qe_phonon_convergence_sequencer,
            ):
        yield

    async def _set_scf_done(self):
        await BaseSmearingConvergenceSequencerTest._set_scf_done(
                self, pseudo_dir=self._scf_pseudo_dir)

    async def _set_phonons_done(self):
        await BaseSmearingPhononConvergenceSequencerTest._set_phonons_done(
                self, new_pseudos=self._pseudos)


# #############################################################################
# ##################### QE NSCF Sequencers test cases #########################
# #############################################################################


class BaseQENSCFSequencerTest(BaseQESCFSequencerTest, BaseNSCFSequencerTest):
    """Base class for nscf sequencers tests with QE."""

    @pytest.fixture
    def setup_qe_nscf_sequencer(
            self, setup_qe_scf_sequencer, setup_nscf_sequencer):
        yield


class BaseQEPositiveKpointsNSCFSequencerTest(
        BasePositiveKpointsNSCFSequencerTest, BaseQENSCFSequencerTest):
    """Base class for positive kpoints sequencer test cases with QE.
    """

    @pytest.fixture(autouse=True)
    def setup_qe_positive_kpoints_nscf_sequencer(
            self, setup_positive_kpoints_nscf_sequencer):
        yield

    async def _set_nscf_done(self):
        await BasePositiveKpointsNSCFSequencerTest._set_nscf_done(
                self, pseudo_dir=self._scf_pseudo_dir)


# ################ QE Fermi Surface Sequencer Test ############################
@pytest.mark.order("first")
class TestQEFermiSurfaceSequencer(
        BaseFermiSurfaceSequencerTest,
        BaseQEPositiveKpointsNSCFSequencerTest):
    """Test case for the fermi surface sequencer with Quantum Espresso.
    """
    _nscf_example = os.path.join(
            qe_fermi_surface_example, "nscf_run")
    _nscf_kgrid = qe_fermi_surface_nscf_kgrid
    _nscf_input_variables = qe_fermi_surface_nscf_input_variables
    _scf_example = qe_scf_example
    _scf_input_variables = qe_scf_input_variables
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QEFermiSurfaceSequencer

    def setUp(self):
        BaseQEPositiveKpointsNSCFSequencerTest.setUp(self)
        BaseFermiSurfaceSequencerTest.setUp(self)


@pytest.mark.order("first")
class TestQEDOSSequencer(
        BaseDOSSequencerTest,
        BaseQENSCFSequencerTest,
        ):
    _sequencer_class = QEDOSSequencer
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _scf_example = qe_scf_example
    _scf_input_variables = qe_scf_input_variables
    _dos_example = qe_dos_example
    _dos_input_variables = qe_dos_input_variables
    _dos_fine_kpoint_grid_variables = qe_dos_fine_kpoint_grid_variables
    _nscf_example = qe_dos_nscf_example
    _nscf_input_variables = qe_dos_nscf_input_variables

    @pytest.fixture(autouse=True)
    def setup_qe_dos_sequencer(
            self, setup_qe_nscf_sequencer,
            setup_dos_sequencer,
            temp_dosx_script,
            ):
        self.sequencer.dos_command = temp_dosx_script.name
        self.sequencer.dos_mpi_command = ""
        yield

    async def test_starting_from_scratch(self):
        # SCF PART
        self._set_scf()
        self._set_nscf()
        self._set_dos()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_dos_not_written()
        assert not await (self.sequencer.sequence_completed)
        # NSCF PART
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_dos_not_written()
        assert not await (self.sequencer.sequence_completed)
        # DOS part
        self.nscf_tempdir.cleanup()
        await self._set_nscf_done()
        await self.sequencer.write()
        await self._check_dos_written()
        # cleanup and set everything done
        self.dos_tempdir.cleanup()
        await self._set_dos_done()
        await self.sequencer.launch()
        assert await (self.sequencer.sequence_completed)

    async def _set_dos_done(self):
        await super()._set_dos_done(
                new_parents=[self.nscf_workdir],
                )

    def _set_nscf(self):
        # nscf input vars not setteable for this sequencer
        pass


# #############################################################################
# ########## QE EPW Sequencer Test Case #######################################
# #############################################################################


@pytest.mark.order("first")
class TestQEEPWSequencer(
        BaseQEPhononSequencerTest, BaseQEPositiveKpointsNSCFSequencerTest,
        BaseQEBandStructureSequencerTest):
    """Test case for the QEEPW sequencer.
    """
    _band_structure_example = qe_epw_wannierization_band_structure_example
    _band_structure_input_variables = (
            qe_epw_wannierization_band_structure_input_variables.copy())
    _band_structure_kpoint_path = (
            qe_epw_wannierization_band_structure_kpoint_path)
    _band_structure_kpoint_path_density = (
            qe_epw_wannierization_band_structure_kpoint_path_density)
    _nscf_example = qe_epw_wannierization_nscf_example
    _nscf_kgrid = qe_epw_wannierization_nscf_kgrid
    _nscf_input_variables = qe_epw_wannierization_nscf_input_variables.copy()
    _nphonons = len(
            os.listdir(
                os.path.dirname(
                    qe_epw_wannierization_phonon_dispersion_example)))
    _phonons_example = qe_epw_wannierization_phonon_dispersion_example
    _phonons_input_variables = (
            qe_epw_wannierization_phonon_dispersion_input_variables.copy())
    _phonons_script_name = "ph.x"
    _scf_example = qe_epw_wannierization_scf_example
    _scf_input_variables = qe_epw_wannierization_scf_input_variables.copy()
    _scf_pseudo_dir = qe_epw_wannierization_pseudo_dir
    _sequencer_class = QEEPWSequencer

    @pytest.fixture(autouse=True)
    def setup_qe_epw_sequencer(
            self, setup_qe_phonon_sequencer,
            setup_qe_positive_kpoints_nscf_sequencer,
            setup_qe_band_structure_sequencer,
            setup_epw_tempdir, temp_epwx_script,
            ):
        # setting up temporary epw command
        self.epw_workdir = os.path.join(self.epw_tempdir.name, "epw_run")
        self.epwx_command_file = temp_epwx_script
        self.epwx_command = self.epwx_command_file.name
        # setting up sequencer object
        self.sequencer.epw_workdir = self.epw_workdir
        self.sequencer.epw_command = self.epwx_command
        self.sequencer.epw_queuing_system = "local"
        # just a fix for the files in place.
        self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "ph_q")
        self.sequencer.phonons_workdir = self.phonons_workdir

    @pytest.fixture
    def setup_epw_tempdir(self):
        self.epw_tempdir = tempfile.TemporaryDirectory()
        yield
        self.epw_tempdir.cleanup()

    async def test_whole_sequence(self):
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self._set_phonons()
        self._set_epw()
        await self.sequencer.write()
        # check scf has been written and nothing else
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_phonons_not_written()
        await self._check_band_structure_not_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # copy SCF and relaunch. check that all post scf calcs are written
        self.scf_tempdir.cleanup()
        await self._set_scf_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # copy all post scf calc and relaunch. check epw written
        self.nscf_tempdir.cleanup()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        await self._set_nscf_done()
        await self._set_band_structure_done()
        await self._set_phonons_done()
        await self.sequencer.write()
        await self._check_epw_written()
        assert not await self.sequencer.sequence_completed
        # copy epw calc and relaunch. check calculation is completed
        self.epw_tempdir.cleanup()
        await self._set_epw_done()
        await self.sequencer.write()
        assert await self.sequencer.sequence_completed

    async def test_starting_before_epw(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_band_structure()
        await self._set_band_structure_done()
        self._set_nscf()
        await self._set_nscf_done()
        self._set_phonons()
        await self._set_phonons_done()
        self._set_epw()
        await self.sequencer.write()
        await self._check_epw_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+PH vars
        self._set_scf()
        self._set_phonons()
        self.epw_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_epw_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_phonons_and_band_structure(self):
        await self._set_scf_done()
        await self._set_band_structure_done()
        self._set_nscf()
        self._set_phonons()
        await self._set_phonons_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+PH+BS vars
        self._set_scf()
        self._set_band_structure()
        self._set_phonons()
        self.nscf_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_nscf_and_band_structure(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_band_structure_done()
        await self._set_nscf_done()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+NSCF+BS vars
        self._set_scf()
        self._set_nscf()
        self._set_band_structure()
        self.phonons_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_nscf_and_phonons(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        self._set_band_structure()
        await self._set_nscf_done()
        self._set_nscf()
        self._set_band_structure()
        self._set_epw()
        self._set_phonons()
        await self._set_phonons_done()
        await self.sequencer.write()
        await self._check_band_structure_written()
        # epw should be written
        await self._check_epw_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+PH+NSCF vars
        self._set_scf()
        self._set_phonons()
        self._set_nscf()
        self.band_structure_tempdir.cleanup()
        self.epw_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_band_structure_written()
        await self._check_epw_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_phonons(self):
        await self._set_scf_done()
        self._set_band_structure()
        self._set_nscf()
        self._set_phonons()
        await self._set_phonons_done()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+phonons vars
        self._set_scf()
        self._set_phonons()
        self.band_structure_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_band_structure(self):
        await self._set_scf_done()
        await self._set_band_structure_done()
        self._set_nscf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+BS vars
        self._set_scf()
        self._set_band_structure()
        self.phonons_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf_with_nscf(self):
        self._set_scf()
        self._set_nscf()
        await self._set_scf_done()
        await self._set_nscf_done()
        self._set_band_structure()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF+NSCF vars
        self._set_scf()
        self._set_nscf()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf(self):
        await self._set_scf_done()
        self._set_band_structure()
        self._set_nscf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed
        # restart with SCF vars
        self._set_scf()
        self.band_structure_tempdir.cleanup()
        self.phonons_tempdir.cleanup()
        self.nscf_tempdir.cleanup()
        await self.sequencer.write()
        await self._check_nscf_written()
        await self._check_phonons_written()
        await self._check_band_structure_written()
        await self._check_epw_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_nscf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_nscf_not_written()
        await self._check_phonons_not_written()
        await self._check_epw_not_written()
        await self._check_band_structure_not_written()
        assert not await self.sequencer.sequence_completed

    async def _check_epw(self, true):
        await self._check(self.epw_workdir, true)

    async def _check_epw_written(self):
        await self._check_epw(True)

    async def _check_epw_not_written(self):
        await self._check_epw(False)

    def _set_epw(self):
        self.sequencer.epw_input_variables = (
                qe_epw_wannierization_epw_input_variables.copy())

    async def _set_epw_done(self):
        new_parents = [self.nscf_workdir]
        for i in range(1, self._nphonons + 1):
            new_parents.append(self.phonons_workdir + str(i))
        await copy_calculation(
            os.path.join(
                qe_epw_wannierization_example, "epw_wannierization_run"),
            self.epw_workdir,
            new_parents=new_parents)


# ############################ POST EPW SEQUENCERS ############################
class BasePostQEEPWSequencerTest(BaseSequencerTest):
    """Base class for tests involving post epw sequencers.
    """
    @pytest.fixture
    def setup_post_epw_tempdir(self):
        self.post_epw_tempdir = tempfile.TemporaryDirectory()
        yield
        self.post_epw_tempdir.cleanup()

    @pytest.fixture(autouse=True)
    async def setup_post_qeepw_sequencer(
            self, setup_sequencer, setup_post_epw_tempdir,
            temp_epwx_script, tempdir):
        self.post_epw_workdir = os.path.join(
                self.post_epw_tempdir.name, "post_epw_run")
        self.epwx_command_file = temp_epwx_script
        self.epwx_command = self.epwx_command_file.name
        prefix = self.sequencer._post_qe_epw_prefix
        setattr(self.sequencer, f"{prefix}workdir", self.post_epw_workdir)
        setattr(self.sequencer, f"{prefix}command", self.epwx_command)
        setattr(self.sequencer, f"{prefix}queuing_system", "local")
        # copy whole calculation tree leading to EPW calc into temp location
        self.temp_parents = tempdir
        # scf
        self.temp_scf = TemporaryCalculation(
                qe_epw_wannierization_scf_example,
                copy_tempdir=self.temp_parents,
                )
        await self.temp_scf.copy_calculation()
        # band structure
        self.temp_band_structure = TemporaryCalculation(
                qe_epw_wannierization_band_structure_example,
                copy_tempdir=self.temp_parents,
                )
        await self.temp_band_structure.copy_calculation()
        # phonons
        self.temp_phonons = []
        for ph in await aiofiles.os.listdir(os.path.dirname(
                qe_epw_wannierization_phonon_dispersion_example)):
            tempcalc = TemporaryCalculation(
                        os.path.join(
                            os.path.dirname(
                                qe_epw_wannierization_phonon_dispersion_example
                                ),
                            ph),
                        copy_tempdir=self.temp_parents,
                        new_parents=[self.temp_scf.path],
                        )
            await tempcalc.copy_calculation()
            self.temp_phonons.append(tempcalc)
        # Q2R
        self.temp_q2r = TemporaryCalculation(
                qe_epw_wannierization_q2r_example,
                copy_tempdir=self.temp_parents,
                new_parents=[ph.path for ph in self.temp_phonons],
                )
        await self.temp_q2r.copy_calculation()
        # matdyn
        self.temp_matdyn = TemporaryCalculation(
                qe_epw_wannierization_matdyn_example,
                copy_tempdir=self.temp_parents,
                new_parents=[self.temp_q2r.path],
                )
        await self.temp_matdyn.copy_calculation()
        # nscf
        self.temp_nscf = TemporaryCalculation(
                qe_epw_wannierization_nscf_example,
                copy_tempdir=self.temp_parents,
                new_parents=[self.temp_scf.path],
                )
        await self.temp_nscf.copy_calculation()
        # epw wannierization
        self.temp_epw_wannierization = TemporaryCalculation(
                os.path.join(
                    qe_epw_wannierization_example, "epw_wannierization_run"),
                new_parents=[self.temp_nscf.path, self.temp_q2r.path] + [
                    x.path for x in self.temp_phonons],
                copy_tempdir=self.temp_parents,
                )
        await self.temp_epw_wannierization.copy_calculation()
        # epw dispersion interpolation
        self.temp_epw_disp_interpolation = TemporaryCalculation(
                os.path.join(
                    qe_epw_wannierization_example,
                    "epw_dispersion_interpolation_run"),
                new_parents=[
                    self.temp_q2r.path,
                    self.temp_band_structure.path,
                    self.temp_matdyn.path,
                    self.temp_epw_wannierization.path],
                copy_tempdir=self.temp_parents,
                )
        await self.temp_epw_disp_interpolation.copy_calculation()

    async def test_starting_from_scratch(self):
        await self._set_post_qe_epw()
        await self.sequencer.write()
        await self._check_post_qe_epw_written()
        assert not await self.sequencer.sequence_completed

    async def test_whole_sequence(self):
        await self._set_post_qe_epw()
        await self._set_post_qe_epw_done()
        await self.sequencer.run()
        assert await self.sequencer.sequence_completed

    async def _check_post_qe_epw_written(self):
        await self._check(self.post_epw_workdir, True)

    async def _set_post_qe_epw(self):
        await self.sequencer.set_epw_wannierization_calculation(
                self.temp_epw_wannierization.path)
        prefix = self.sequencer._post_qe_epw_prefix
        setattr(self.sequencer, f"{prefix}input_variables",
                self._post_qe_epw_input_variables.copy())
        self.sequencer.plot_show = False

    async def _set_post_qe_epw_done(self):
        await copy_calculation(
                self._post_qe_epw_example,
                self.post_epw_workdir,
                new_parents=[
                    self.post_epw_workdir, self.temp_q2r.path],
                )


# QE IBTE Sequencer Test Case
@pytest.mark.order("first")
class TestQEEPWIBTESequencer(BasePostQEEPWSequencerTest):
    """Test case for the QEEPWIBTESequencer class.
    """
    _sequencer_class = QEEPWIBTESequencer
    _post_qe_epw_example = qe_ibte_example
    _post_qe_epw_input_variables = qe_ibte_input_variables.copy()


# QE EPW Ziman Sequencer
@pytest.mark.order("first")
class TestQEEPWZimanSequencer(BasePostQEEPWSequencerTest):
    """Test case for the QEEPWZimanSequencer class.
    """
    _sequencer_class = QEEPWZimanSequencer
    _post_qe_epw_example = qe_ziman_example
    _post_qe_epw_input_variables = qe_ziman_input_variables.copy()


# #############################################################################
# ########## QE Individual Phonon Sequencer Test Case #########################
# #############################################################################


@pytest.mark.order("first")
class TestQEIndividualPhononSequencer(
        BaseQEPhononSequencerTest):
    """TestCase for the QEIndividualPhononSequencer class.
    """
    _nphonons = len(os.listdir(os.path.join(
        qe_phonon_dispersion_phonons_example, "phonons_runs")))
    _phonons_example = os.path.join(
            qe_phonon_dispersion_phonons_example, "phonons_runs", "ph_q")
    _phonons_input_variables = qe_phonon_dispersion_phonons_input_variables
    _phonons_qpoint_grid = None
    _scf_example = qe_scf_example
    _scf_input_variables = qe_scf_input_variables.copy()
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QEIndividualPhononSequencer


# #############################################################################
# ########## QE Phonon dispersion sequencer test case #########################
# #############################################################################

class BasePostQEPhononSequencerTest(BaseQEPhononSequencerTest):
    @pytest.fixture
    def setup_q2r_tempdir(self):
        self.q2r_tempdir = tempfile.TemporaryDirectory()
        yield
        self.q2r_tempdir.cleanup()

    @pytest.fixture
    def setup_matdyn_tempdir(self):
        self.matdyn_tempdir = tempfile.TemporaryDirectory()
        yield
        self.matdyn_tempdir.cleanup()


@pytest.mark.order("first")
class TestQEPhononDispersionSequencer(
        BasePostQEPhononSequencerTest):
    """TestCase for the QE Phonon Dispersion Sequencer class.
    """
    _phonons_example = os.path.join(
            qe_phonon_dispersion_phonons_example,
            "phonons_runs", "ph_q",
            )
    _phonons_input_variables = (
            qe_phonon_dispersion_phonons_input_variables.copy())
    _phonons_qpoint_grid = qe_phonon_dispersion_phonons_qpoint_grid
    _scf_example = qe_scf_example
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _scf_input_variables = qe_scf_input_variables.copy()
    _sequencer_class = QEPhononDispersionSequencer

    @pytest.fixture(autouse=True)
    def setup_qe_phonon_dispersion_sequencer(
            self, setup_qe_phonon_sequencer, setup_q2r_tempdir,
            setup_matdyn_tempdir, temp_q2rx_script, temp_matdynx_script):
        self.q2r_workdir = os.path.join(self.q2r_tempdir.name, "q2r_run")
        self.matdyn_workdir = os.path.join(
                self.matdyn_tempdir.name, "matdyn_run")
        self.q2rx_command_file = temp_q2rx_script
        self.q2rx_command = self.q2rx_command_file.name
        self.matdynx_command_file = temp_matdynx_script
        self.matdynx_command = self.matdynx_command_file.name
        # set sequencer
        self.sequencer.q2r_queuing_system = "local"
        self.sequencer.matdyn_queuing_system = "local"
        self.sequencer.q2r_workdir = self.q2r_workdir
        self.sequencer.matdyn_workdir = self.matdyn_workdir
        self.sequencer.q2r_command = self.q2rx_command
        self.sequencer.matdyn_command = self.matdynx_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                self.matdyn_workdir, "plot.pdf")

    async def test_whole_sequence(self):
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        self._set_q2r()
        self._set_matdyn()
        await self.sequencer.write()
        await self._check_q2r_written()
        await self._check_matdyn_not_written()
        assert not await self.sequencer.sequence_completed
        self.q2r_tempdir.cleanup()
        await self._set_q2r_done()
        await self.sequencer.write()
        await self._check_matdyn_written()
        assert not await self.sequencer.sequence_completed
        self.matdyn_tempdir.cleanup()
        await self._set_matdyn_done()
        await self.sequencer.launch()
        assert await self.sequencer.sequence_completed
        await self._check_plot_written()

    async def test_start_from_q2r(self):
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        await self._set_q2r_done()
        self._set_matdyn()
        await self.sequencer.write()
        await self._check_matdyn_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed
        # copy matdyn and check plot is run afterwards
        # this is done in test whole sequence no need to rerun

    async def test_start_from_phonons(self):
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        self._set_q2r()
        await self.sequencer.write()
        await self._check_q2r_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf(self):
        await self._set_scf_done()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_phonons_written()
        await self._check_q2r_not_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed
        # set scf input variables, and restart. check its good
        self.phonons_tempdir.cleanup()
        self._set_scf()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_phonons_written()
        await self._check_q2r_not_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_phonons_not_written()
        await self._check_q2r_not_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def _check_matdyn(self, true):
        await self._check(self.sequencer.matdyn_workdir, true)

    async def _check_matdyn_written(self):
        await self._check_matdyn(True)

    async def _check_matdyn_not_written(self):
        await self._check_matdyn(False)

    async def _check_q2r(self, true):
        await self._check(self.sequencer.q2r_workdir, true)

    async def _check_q2r_written(self):
        await self._check_q2r(True)

    async def _check_q2r_not_written(self):
        await self._check_q2r(False)

    async def _check_plot(self, true):
        isfile = await aiofiles.os.path.isfile(self.sequencer.plot_save)
        if true:
            assert isfile
        else:
            assert not isfile

    async def _check_plot_written(self):
        await self._check_plot(True)

    async def _check_plot_not_written(self):
        await self._check_plot(False)

    def _set_matdyn(self):
        self.sequencer.qpoint_path = qe_phonon_dispersion_qpath
        self.sequencer.qpoint_path_density = (
                qe_phonon_dispersion_qpath_density)
        self.sequencer.asr = qe_phonon_dispersion_asr
        self.sequencer.matdyn_input_variables = (
                qe_phonon_dispersion_matdyn_input_variables.copy())

    async def _set_matdyn_done(self):
        await copy_calculation(
                os.path.join(
                    qe_phonon_dispersion_phonons_example,
                    "matdyn_run"),
                self.matdyn_workdir,
                new_parents=[self.q2r_workdir],
                )

    def _set_q2r(self):
        self.sequencer.q2r_input_variables = (
                qe_phonon_dispersion_q2r_input_variables.copy())

    async def _set_q2r_done(self):
        newparents = []
        for calc in self.sequencer.sequence:
            if calc.calctype != "qe_ph":
                continue
            newparents.append(calc.workdir)
        await copy_calculation(
                os.path.join(qe_phonon_dispersion_phonons_example, "q2r_run"),
                self.q2r_workdir, new_parents=newparents,
                )


# #############################################################################
# ################## Thermal Expansion Sequencer ##############################
# #############################################################################


@pytest.mark.order("first")
class TestQEThermalExpansionSequencer(BasePostQEPhononSequencerTest):
    """Test case for the thermal expansion sequencer of Quantum Espresso.
    """
    _phonons_example = os.path.join(
            qe_thermal_lattice_expansion_example, "phonons_runs")
    _phonons_input_variables = (
            qe_thermal_lattice_expansion_phonons_input_variables)
    _phonons_qpoint_grid = (
            qe_thermal_lattice_expansion_phonons_qgrid)
    _scf_example = os.path.join(
            qe_thermal_lattice_expansion_example, "scf_runs")
    _scf_input_variables = qe_thermal_lattice_expansion_scf_input_variables
    _scf_pseudo_dir = qe_scf_pseudo_dir
    _sequencer_class = QEThermalExpansionSequencer

    @pytest.fixture
    def setup_matdyngrid_tempdir(self):
        self.matdyngrid_tempdir = tempfile.TemporaryDirectory()
        yield
        self.matdyngrid_tempdir.cleanup()

    @pytest.fixture
    def setup_kpoints_tempdir(self):
        self.kpoints_tempdir = tempfile.TemporaryDirectory()
        yield
        self.kpoints_tempdir.cleanup()

    @pytest.fixture(autouse=True)
    def setup_qe_thermal_expansion_sequencer(
            self, setup_qe_phonon_sequencer,
            setup_q2r_tempdir, setup_matdyn_tempdir, setup_matdyngrid_tempdir,
            setup_kpoints_tempdir,
            temp_q2rx_script, temp_matdynx_script, testdb,
            temp_kpointsx_script,
            ):
        # just a fix for the files in place.
        self.scf_workdir = os.path.join(
                self.scf_tempdir.name, "scf_runs")
        self.sequencer.scf_workdir = os.path.join(
                self.scf_workdir, "scf")
        self.phonons_workdir = os.path.join(
                self.phonons_tempdir.name, "phonons_runs", "phonons")
        self.sequencer.phonons_workdir = self.phonons_workdir
        # use normpath because for windows, you can't mix forward and
        # backwards slashes
        self.q2r_workdir = os.path.normpath(
            os.path.join(self.q2r_tempdir.name, "q2r_runs/q2r"))
        self.matdyn_workdir = os.path.normpath(os.path.join(
                self.matdyn_tempdir.name, "matdyn_runs/matdyn"))
        self.matdyngrid_workdir = os.path.normpath(os.path.join(
                self.matdyngrid_tempdir.name, "matdyngrid_runs/matdyngrid"))
        self.q2rx_command_file = temp_q2rx_script
        self.q2rx_command = self.q2rx_command_file.name
        self.kpointsx_command_file = temp_kpointsx_script
        self.kpointsx_command = self.kpointsx_command_file.name
        self.matdynx_command_file = temp_matdynx_script
        self.matdynx_command = self.matdynx_command_file.name
        # set sequencer
        self.sequencer.q2r_queuing_system = "local"
        self.sequencer.matdyn_queuing_system = "local"
        self.sequencer.q2r_workdir = self.q2r_workdir
        self.sequencer.matdyn_workdir = self.matdyn_workdir
        self.sequencer.matdyngrid_workdir = self.matdyngrid_workdir
        self.sequencer.q2r_command = self.q2rx_command
        self.sequencer.matdyn_command = self.matdynx_command
        self.sequencer.kpoints_workdir = os.path.join(
                self.kpoints_tempdir.name, "fine_qpoint_grid")
        self.sequencer.kpoints_command = self.kpointsx_command
        self.sequencer.plot_show = False
        self.sequencer.plot_save = os.path.join(
                os.path.dirname(self.matdyn_workdir), "plot.pdf")
        self.sequencer.temperatures = [100, 200]
        self.sequencer.bulk_modulus_initial_guess = 37
        self.sequencer.relaxed_lattice_parameters = {"celldm(1)": 9.210587579}
        self.sequencer.asr = "crystal"
        self.sequencer.deltas_volumes = qe_thermal_lattice_expansion_deltas
        self.sequencer.q2r_input_variables = {}
        self.sequencer.phonons_fine_qpoint_grid = (
                qe_thermal_lattice_expansion_fine_qpoint_grid)

    async def test_starting_from_scratch(self):
        self._set_scf()
        self._set_phonons()
        await self.sequencer.write()
        await self._check_scf_written()
        await self._check_kpoints_not_written()
        await self._check_phonons_not_written()
        await self._check_q2r_not_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_scf(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_phonons()
        await self.sequencer.write()
        # check 1 phonon have been written for each temperature
        await self._check_phonons_written(nphonons=1)
        await self._check_q2r_not_written()
        await self._check_matdyn_not_written()
        await self._check_kpoints_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_phonons(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        await self._set_kpoints_done()
        self._set_q2r()
        self._set_matdyn()
        await self.sequencer.write()
        await self._check_q2r_written()
        await self._check_kpoints_written()
        await self._check_matdyn_not_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_starting_from_q2r(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        self._set_q2r()
        await self._set_q2r_done()
        await self._set_kpoints_done()
        self._set_matdyn()
        await self.sequencer.write()
        await self._check_matdyn_written()
        await self._check_plot_not_written()
        assert not await self.sequencer.sequence_completed

    async def test_whole_sequence(self):
        self._set_scf()
        await self._set_scf_done()
        self._set_phonons()
        await self._set_phonons_done()
        self._set_q2r()
        await self._set_q2r_done()
        self._set_matdyn()
        await self._set_matdyn_done()
        await self._set_kpoints_done()
        await self.sequencer.launch()
        await self._check_plot_written()
        assert await self.sequencer.sequence_completed
        # TODO: reimplement this assert
        # assert len(self.sequencer.extended_lattice_volumes[
        #     "free_energy_total_vs_volume"]) == (
        #         len(self.sequencer.temperatures))

    async def _check_scf_written(self):
        # check that there is one scf calc for each delta
        assert len(await aiofiles.os.listdir(self.scf_workdir)) == (
                         len(self.sequencer.deltas_volumes))

    async def _check_phonons_written(self, nphonons=None):
        # check that there are NPHONONS calculations written for each delta
        root = os.path.dirname(self.phonons_workdir)
        assert len(await aiofiles.os.listdir(root)) == (
                         len(self.sequencer.deltas_volumes))
        for voldirname in await aiofiles.os.listdir(root):
            this_vol_root = os.path.join(root, voldirname)
            assert len(await aiofiles.os.listdir(this_vol_root)) == (
                    nphonons if nphonons is not None else self._nphonons)

    async def _set_scf_done(self):
        # many scf calculations to copy
        coros = []
        for basename in await aiofiles.os.listdir(self._scf_example):
            dest = os.path.join(self.scf_workdir, basename)
            coros.append(copy_calculation(
                    os.path.join(self._scf_example, basename),
                    dest, pseudo_dir=self._scf_pseudo_dir)
                    )
        await asyncio.gather(*coros)

    async def _set_phonons_done(self):
        # list all calcs to copy
        rel_paths = []
        for subdir in await aiofiles.os.listdir(self._phonons_example):
            for phdir in await aiofiles.os.listdir(
                    os.path.join(self._phonons_example, subdir)):
                rel_paths.append(os.path.join(subdir, phdir))
        coros = []
        for rel_path in rel_paths:
            dest = os.path.join(os.path.dirname(self.phonons_workdir),
                                rel_path)
            scf_ext = os.path.dirname(rel_path).split("_")[-1]
            coros.append(copy_calculation(
                    os.path.join(self._phonons_example, rel_path), dest,
                    new_parents=[self.sequencer.scf_workdir + "_" + scf_ext]))
        await asyncio.gather(*coros)

    async def _check_kpoints(self, true):
        check = await is_calculation_directory(
                    self.sequencer.kpoints_workdir)
        if true:
            assert check
        else:
            assert not check

    async def _check_kpoints_written(self):
        await self._check_kpoints(True)

    async def _check_kpoints_not_written(self):
        await self._check_kpoints(False)

    async def _set_kpoints_done(self):
        await copy_calculation(
                os.path.join(
                    qe_thermal_lattice_expansion_example, "fine_qpoint_grid"),
                self.sequencer.kpoints_workdir,
                )

    async def _check_matdyn_written(self):
        assert len(await aiofiles.os.listdir(
            os.path.dirname(self.matdyn_workdir))) == (
                len(self.sequencer.deltas_volumes))
        assert len(await aiofiles.os.listdir(
            os.path.dirname(self.matdyngrid_workdir))) == (
                len(self.sequencer.deltas_volumes))

    async def _check_matdyn_not_written(self):
        assert not await aiofiles.os.path.exists(
                os.path.dirname(self.matdyn_workdir))
        assert not await aiofiles.os.path.exists(
                os.path.dirname(self.matdyngrid_workdir))

    async def _check_q2r_written(self):
        assert len(await aiofiles.os.listdir(
            os.path.dirname(self.q2r_workdir))) == (
                len(self.sequencer.deltas_volumes))

    async def _check_q2r_not_written(self):
        assert not await aiofiles.os.path.exists(
                os.path.dirname(self.q2r_workdir))

    async def _check_plot_written(self):
        # check that there are 2 plots created
        root = os.path.dirname(self.sequencer.plot_save)
        all_files = await aiofiles.os.listdir(root)
        assert len([x for x in all_files if x.endswith(".pdf")]) == 23

    async def _check_plot_not_written(self):
        # there is a few plots so check that there are no files ending with pdf
        root = os.path.dirname(self.sequencer.plot_save)
        if not await aiofiles.os.path.exists(root):
            return
        all_files = await aiofiles.os.listdir(root)
        assert not any([x.endswith(".pdf") for x in all_files])

    def _set_matdyn(self):
        self.sequencer.qpoint_path = qe_thermal_lattice_expansion_phdisp_qpath
        self.sequencer.qpoint_path_density = (
                qe_thermal_lattice_expansion_phdisp_nqpts)
        self.sequencer.matdyn_input_variables = {}

    async def _set_matdyn_done(self) -> None:
        # many matdyn calculations to copy
        coros = []
        for basename in await aiofiles.os.listdir(
                os.path.join(
                    qe_thermal_lattice_expansion_example, "matdyn_runs")):
            dest = os.path.join(os.path.dirname(self.matdyn_workdir), basename)
            ext = basename.split("_")[-1]
            coros.append(copy_calculation(
                    os.path.join(
                        qe_thermal_lattice_expansion_example,
                        "matdyn_runs", basename),
                    dest,
                    new_parents=[self.q2r_workdir + "_" + ext]))
        for basename in await aiofiles.os.listdir(
                os.path.join(
                    qe_thermal_lattice_expansion_example,
                    "matdyngrid_runs")):
            ext = basename.split("_")[-1]
            dest = os.path.join(
                    os.path.dirname(self.matdyngrid_workdir),
                    basename)
            coros.append(copy_calculation(
                    os.path.join(
                        qe_thermal_lattice_expansion_example,
                        "matdyngrid_runs",
                        basename),
                    dest,
                    new_parents=[self.q2r_workdir + "_" + ext]))
        await asyncio.gather(*coros)

    def _set_q2r(self) -> None:
        self.sequencer.q2r_input_variables = {}

    async def _set_q2r_done(self) -> None:
        # many q2r calculations to copy
        coros = []
        for basename in await aiofiles.os.listdir(
                os.path.join(
                    qe_thermal_lattice_expansion_example,
                    "q2r_runs",
                    )):
            dest = os.path.join(os.path.dirname(self.q2r_workdir), basename)
            # need to relink phonons dependencies
            ext = "_" + basename.split("_")[-1]
            ph_parent_root = self.phonons_workdir + ext
            new_parents = [os.path.join(ph_parent_root, x)
                           for x in await aiofiles.os.listdir(ph_parent_root)]
            coros.append(
                    copy_calculation(
                        os.path.join(
                            qe_thermal_lattice_expansion_example,
                            "q2r_runs", basename),
                        dest, new_parents=new_parents))
        await asyncio.gather(*coros)
