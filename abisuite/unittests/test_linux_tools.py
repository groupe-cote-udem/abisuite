import os

import aiofiles
import aiofiles.os

import pytest

from ..exceptions import DirectoryExistsError
from ..linux_tools import (
        ln_s,
        mkdir,
        touch,
        which,
        )


class TestLinuxTools:
    """Test case for the linux tools.
    """

    async def test_mkdir(self, tempdir, named_tempfile):
        # check that directory creation works recursively too
        newdir = os.path.join(tempdir, "newdir", "newdir_2")
        await mkdir(newdir)
        assert await aiofiles.os.path.isdir(newdir)
        assert await aiofiles.os.path.isdir(os.path.dirname(newdir))
        # check that creating a directory when it already exists does
        # nothing (like raising an error)
        await mkdir(newdir)
        # check that an error is raised when attempting to create dir on a file
        with pytest.raises(FileExistsError):
            await mkdir(named_tempfile.name)

    async def test_touch(self, tempdir):
        # test that creating a file in a temporary directory works
        filename = os.path.join(tempdir, "file")
        await touch(filename)
        assert await aiofiles.os.path.exists(filename)
        assert await aiofiles.os.path.isfile(filename)
        # test that touching an already existing file is ok
        await touch(filename)
        # test that creating a file a the directory's place will raise
        with pytest.raises(DirectoryExistsError):
            await touch(tempdir)

    async def test_which(self):
        # python should be accessible everywhere!
        which_result = await which("python")
        assert "python" in which_result, which_result
        # check for a non-existent script
        assert await which("non_existent_script") is None

    async def test_ln_s(self, tempdir, tempdir2):
        # create 2 temporary directory. inside the first create a file
        filename = os.path.join(tempdir, "file")
        await touch(filename)
        target = os.path.join(tempdir2, "file_link")
        await ln_s(filename, target)
        assert await aiofiles.os.path.exists(target)
        assert await aiofiles.os.path.islink(target)
        assert await aiofiles.os.path.samefile(
                await aiofiles.os.readlink(target), filename)
        # test the symlink to a directory
        target = os.path.join(tempdir2, "dir_link")
        await ln_s(tempdir, target)
        assert await aiofiles.os.path.exists(target)
        assert await aiofiles.os.path.islink(target)
        assert await aiofiles.os.path.samefile(
                await aiofiles.os.readlink(target), tempdir)
        # check that if directory needs to be created, it is
        target = os.path.join(tempdir2, "new_dir", "file_link")
        await ln_s(filename, target)
        assert await aiofiles.os.path.exists(target)
        assert await aiofiles.os.path.islink(target)
        assert await aiofiles.os.path.samefile(
                await aiofiles.os.readlink(target), filename)
