import pytest

from ..maths import find_divisors


class TestMaths:
    """Test the math tools."""

    def test_find_divisors(self):
        """Test the the 'find_divisors' function works as intended."""
        for n, divisors in zip(
                [1, 2, 100], [[1], [1, 2], [1, 2, 4, 5, 10, 20, 25, 50, 100]]):
            assert find_divisors(n) == divisors
        with pytest.raises(TypeError):
            find_divisors(-1)
        with pytest.raises(TypeError):
            find_divisors(None)
