import asyncio
import os

import aiofiles.os

import aioshutil

import pytest

from .bases import BaseWorkflowTest, __TEST_WORKFLOW_ROOT__
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        qe_dos_example,
        qe_dos_fine_kpoint_grid_variables,
        qe_dos_input_variables,
        qe_dos_kgrid_convergence_example,
        qe_dos_kgrid_convergence_input_variables,
        qe_dos_kgrid_convergence_kgrids,
        qe_dos_nscf_example,
        qe_epw_wannierization_epw_input_variables,
        qe_epw_wannierization_example,
        qe_epw_wannierization_nscf_input_variables,
        qe_epw_wannierization_nscf_kgrid,
        qe_gs_smearing_convergence_criterion,
        qe_gs_smearing_convergence_example,
        qe_gs_smearing_convergence_kgrids,
        qe_gs_smearing_convergence_scf_input_variables,
        qe_gs_smearing_convergence_smearings,
        qe_phonon_dispersion_phonons_example,
        qe_phonon_dispersion_phonons_input_variables,
        qe_phonon_dispersion_phonons_qpoint_grid,
        qe_phonon_dispersion_qpath,
        qe_phonon_dispersion_qpath_density,
        qe_phonon_kgrid_convergence_phonons_examples,
        qe_phonon_kgrid_convergence_phonons_input_variables,
        qe_phonon_kgrid_convergence_scf_examples,
        qe_phonon_kgrid_convergence_scf_input_variables,
        qe_phonon_kgrid_convergence_scf_kgrids,
        qe_phonon_smearing_convergence_example,
        qe_phonon_smearing_convergence_phonons_input_variables,
        qe_phonon_smearing_convergence_qpt,
        qe_phonon_smearing_convergence_scf_input_variables,
        qe_relaxation_example,
        qe_relaxation_input_variables,
        qe_scf_example,
        qe_scf_input_variables,
        qe_thermal_lattice_expansion_asr,
        qe_thermal_lattice_expansion_bulk_modulus_initial_guess,
        qe_thermal_lattice_expansion_deltas,
        qe_thermal_lattice_expansion_example,
        qe_thermal_lattice_expansion_phdisp_nqpts,
        qe_thermal_lattice_expansion_phdisp_qpath,
        qe_thermal_lattice_expansion_phonons_fine_qpoint_grid,
        qe_thermal_lattice_expansion_phonons_input_variables,
        qe_thermal_lattice_expansion_phonons_qgrid,
        qe_thermal_lattice_expansion_scf_input_variables,
        qe_thermal_lattice_expansion_temperatures,
        )
from ...exceptions import DevError
from ...handlers import is_calculation_directory
from ...sequencers import QEEPWIBTESequencer
from ...workflows import QEWorkflow


# QEWorkflow unittest with Pb
# GENERAL
__PSEUDO_DIR__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "pseudos")
# GS ECUT CONVERGENCE #########################################################
QE_GS_ECUT_CONVERGENCE_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_GS_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
QE_GS_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "gs_ecut_convergence",
        "scf_runs",
        )
QE_GS_ECUT_CONVERGENCE_CRITERION = 100

# ######################### GS KGRID CONVERGENCE PART #########################

QE_GS_KGRID_CONVERGENCE_INPUT_VARS = {
         "calculation": "scf",
         "ibrav": 2,
         "ntyp": 1,
         "nat": 1,
         "celldm(1)": 9.2225583816,
         "pseudo_dir": __PSEUDO_DIR__,
         "occupations": "smearing",
         "degauss": 0.1,
         "conv_thr": 1.0e-8,
         "ecutwfc": 50,
         "atomic_species": [
             {"atom": "Pb",
              "atomic_mass": 207.2,
              "pseudo": "pb_s.UPF",
              }],
         "k_points": {"parameter": "automatic",
                      "k_points": [8, 8, 8, 1, 1, 1],
                      },
         "atomic_positions": {"parameter": "crystal",
                              "positions": {"Pb": [0.0, 0.0, 0.0]}},
         }
QE_GS_KGRID_CONVERGENCE_KGRIDS = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
QE_GS_KGRID_CONVERGENCE_CRITERION = 10
QE_GS_KGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "gs_kgrid_convergence")

# ######################### PHONON ECUT CONVERGENCE PART ######################
QE_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.281641264965383,
        "pseudo_dir": __PSEUDO_DIR__,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [4, 4, 4, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
QE_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS = {}
QE_PHONON_ECUT_CONVERGENCE_ECUTS = [10, 20, 30]
QE_PHONON_ECUT_CONVERGENCE_PHONON_QPT = [0.0, 0.0, 0.0]
QE_PHONON_ECUT_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_ecut_convergence")


# ########################### BAND STRUCTURE PART #############################
QE_BAND_STRUCTURE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "band_structure",
        "band_structure")
QE_BAND_STRUCTURE_INPUT_VARIABLES = None
GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
X = {"X": [0.0, 0.5, 0.5]}   # X
K = {"K": [3/8, 3/8, 3/4]}
L = {"L": [1/2, 1/2, 1/2]}
W = {"W": [1/4, 1/2, 3/4]}

QE_BAND_STRUCTURE_KPOINT_PATH = [GAMMA, X, W, L, K, GAMMA, W]
QE_BAND_STRUCTURE_KPOINT_PATH_DENSITY = 100


# ######################## FERMI SURFACE PART #################################
QE_FERMI_SURFACE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "fermi_surface")
QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES = {}
QE_FERMI_SURFACE_NSCF_KGRID = [15] * 3


# ####################### IBTE FSTHICK CONVERGENCE PART #######################
QE_IBTE_FSTHICK_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "ibte_fsthick_convergence")
QE_IBTE_FSTHICK_CONVERGENCE_INPUT_VARIABLES = {
        "assume_metal": True,
        "degaussw": 0.0,
        "restart_step": 100,
        "etf_mem": 0,
        "ngaussw": -99,
        "vme": "wannier",
        "temps": [50, 100, 300, 600],
        "nstemp": 4,
        "nkf1": 10, "nkf2": 10, "nkf3": 10, "nqf1": 10, "nqf2": 10, "nqf3": 10,
        }
QE_IBTE_FSTHICK_CONVERGENCE_FSTHICKS = [0.1, 0.2, 0.3]

# ####################### IBTE FINE GRID CONVERGENCE PART #####################
QE_IBTE_FINE_GRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "ibte_fine_grid_convergence")
QE_IBTE_FINE_GRID_CONVERGENCE_INPUT_VARIABLES = {
        "assume_metal": True,
        "degaussw": 0.0,
        "restart_step": 100,
        "etf_mem": 0,
        "ngaussw": -99,
        "vme": "wannier",
        "temps": [50, 100, 300, 600],
        "nstemp": 4,
        "fsthick": 0.1,
        }
QE_IBTE_FINE_GRID_CONVERGENCE_FINE_GRIDS = [
        {"nqf1": x, "nqf2": x, "nqf3": x, "nkf1": x, "nkf2": x, "nkf3": x}
        for x in [10, 20]]

# ############################## IBTE PART ####################################
QE_IBTE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "ibte", "ibte_run")
QE_IBTE_INPUT_VARIABLES = {
         "assume_metal": True,
         "restart_step": 100,
         "degaussw": 0.0,
         "etf_mem": 0,
         "fsthick": 0.1,
         "ngaussw": -99,
         "vme": "wannier",
         "temps": [50, 100, 300, 600],
         "nstemp": 4,
         "nkf1": 10, "nkf2": 10, "nkf3": 10,
         "nqf1": 10, "nqf2": 10, "nqf3": 10}


# #################### PHONON DISPERSION QGRID CONVERGENCE PART ###############
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS = [
        [2, 2, 2], [4, 4, 4]]
QE_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "phonon_dispersion_qgrid_convergence")

# ###############  EPW WANNIERIZATION WITH THERMAL EXPANSION PART #############
__QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "epw_wannierization_with_lattice_expansion")
__QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "scf_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "nscf_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "phonons_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "q2r_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "band_structure_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__, "matdyn_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__,
        "epw_wannierization_runs")
__QE_EPW_WITH_THERMAL_EXPANSION_EPWDISPINTERPOLATION_EXAMPLES__ = os.path.join(
        __QE_EPW_WITH_THERMAL_EXPANSION_EXAMPLE_ROOT__,
        "epw_dispersion_interpolation_runs",
        )

__QE_IBTE_WITH_THERMAL_EXPANSION_EXAMPLES__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
        "ibte_with_thermal_expansion", "ibte_runs")

# #################### ZIMAN PART #############################################
__QE_ZIMAN_EXAMPLE__ = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb", "ziman", "ziman_run")
__QE_ZIMAN_INPUT_VARIABLES__ = {
            "a2f": True, "phonselfen": True, "degaussw": 0.005, "ngaussw": -99,
            "fsthick": 0.2, "rand_nk": 10 ** 3, "rand_nq": 10 ** 3,
            "rand_q": True, "rand_k": True, "vme": "wannier",
            }


@pytest.mark.order("first")
class TestQEWorkflow(BaseWorkflowTest):
    """Test case for the QEWorkflow."""

    _band_structure_example = QE_BAND_STRUCTURE_EXAMPLE
    _band_structure_input_variables = QE_BAND_STRUCTURE_INPUT_VARIABLES
    _band_structure_kpoint_path = QE_BAND_STRUCTURE_KPOINT_PATH
    _band_structure_kpoint_path_density = QE_BAND_STRUCTURE_KPOINT_PATH_DENSITY
    _dos_example = qe_dos_example
    _dos_fine_kpoint_grid_variables = qe_dos_fine_kpoint_grid_variables
    _dos_input_variables = qe_dos_input_variables
    _dos_script_name = "dos.x"
    _fermi_surface_nscf_kgrid = QE_FERMI_SURFACE_NSCF_KGRID
    _fermi_surface_nscf_input_variables = (
            QE_FERMI_SURFACE_NSCF_INPUT_VARIABLES.copy())
    _fermi_surface_nscf_example = (
            os.path.join(QE_FERMI_SURFACE_EXAMPLE, "nscf_run"))
    _gs_ecut_convergence_criterion = QE_GS_ECUT_CONVERGENCE_CRITERION
    _gs_ecut_convergence_input_vars = QE_GS_ECUT_CONVERGENCE_INPUT_VARS
    _gs_ecut_convergence_ecuts = QE_GS_ECUT_CONVERGENCE_ECUTS
    _gs_ecut_convergence_example = QE_GS_ECUT_CONVERGENCE_EXAMPLE
    _gs_kgrid_convergence_criterion = QE_GS_KGRID_CONVERGENCE_CRITERION
    _gs_kgrid_convergence_input_vars = QE_GS_KGRID_CONVERGENCE_INPUT_VARS
    _gs_kgrid_convergence_kgrids = QE_GS_KGRID_CONVERGENCE_KGRIDS
    _gs_kgrid_convergence_example = QE_GS_KGRID_CONVERGENCE_EXAMPLE
    _gs_smearing_convergence_criterion = qe_gs_smearing_convergence_criterion
    _gs_smearing_convergence_input_vars = (
            qe_gs_smearing_convergence_scf_input_variables)
    _gs_smearing_convergence_kgrids = qe_gs_smearing_convergence_kgrids
    _gs_smearing_convergence_smearings = qe_gs_smearing_convergence_smearings
    _gs_smearing_convergence_example = qe_gs_smearing_convergence_example
    _gs_input_vars = qe_scf_input_variables
    _gs_example = qe_scf_example
    _init_kwargs = {"gs": True, "lattice_expansion": True}
    _phonon_ecut_convergence_ecuts = QE_PHONON_ECUT_CONVERGENCE_ECUTS
    _phonon_ecut_convergence_example = QE_PHONON_ECUT_CONVERGENCE_EXAMPLE
    _phonon_ecut_convergence_scf_input_vars = (
            QE_PHONON_ECUT_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_ecut_convergence_phonons_input_vars = (
            QE_PHONON_ECUT_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_ecut_convergence_phonon_qpt = QE_PHONON_ECUT_CONVERGENCE_PHONON_QPT
    _phonon_ecut_convergence_criterion = 10
    _phonon_dispersion_example = qe_phonon_dispersion_phonons_example
    _phonon_dispersion_phonons_input_vars = (
            qe_phonon_dispersion_phonons_input_variables)
    _phonon_dispersion_qgrid = qe_phonon_dispersion_phonons_qpoint_grid
    _phonon_dispersion_qgrid_convergence_qgrids = (
                QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS
                )
    _phonon_dispersion_qgrid_convergence_example = (
            QE_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE
            )
    _phonon_dispersion_qpoint_path = qe_phonon_dispersion_qpath
    _phonon_dispersion_qpoint_path_density = qe_phonon_dispersion_qpath_density
    _phonon_kgrid_convergence_scf_kgrids = (
            qe_phonon_kgrid_convergence_scf_kgrids)
    _phonon_kgrid_convergence_phonons_examples = (
            qe_phonon_kgrid_convergence_phonons_examples)
    _phonon_kgrid_convergence_phonons_input_variables = (
            qe_phonon_kgrid_convergence_phonons_input_variables)
    _phonon_kgrid_convergence_scf_examples = (
            qe_phonon_kgrid_convergence_scf_examples)
    _phonon_kgrid_convergence_scf_input_variables = (
            qe_phonon_kgrid_convergence_scf_input_variables)
    _phonon_smearing_convergence_example = (
            qe_phonon_smearing_convergence_example)
    _phonon_smearing_convergence_scf_input_vars = (
            qe_phonon_smearing_convergence_scf_input_variables)
    _phonon_smearing_convergence_phonons_input_vars = (
            qe_phonon_smearing_convergence_phonons_input_variables)
    _phonon_smearing_convergence_kgrids = qe_gs_smearing_convergence_kgrids
    _phonon_smearing_convergence_phonon_qpt = (
            qe_phonon_smearing_convergence_qpt)
    _phonon_smearing_convergence_smearings = (
            qe_gs_smearing_convergence_smearings)
    _phonon_smearing_convergence_criterion = 10
    _relaxation_input_vars = qe_relaxation_input_variables
    _scf_script_name = "pw.x"
    _phonons_script_name = "ph.x"
    _relaxation_other_kwargs = {"maximum_relaxations": 10}
    _workflow_cls = QEWorkflow

    @pytest.fixture(autouse=True)
    def setup_qe_workflow(
            self, setup_workflow, temp_q2rx_script,
            temp_matdynx_script, temp_epwx_script):
        """Set up test."""
        self.q2r_command_file = temp_q2rx_script
        self.matdyn_command_file = temp_matdynx_script
        self.epw_command_file = temp_epwx_script

    async def test_dos_kgrid_convergence(self):
        workflow = self._workflow_cls(
                dos_kgrid_convergence=True,
                gs=True,
                )
        await self._set_dos_kgrid_convergence(workflow)
        await workflow.write()
        # make sure nscf calcs were written
        assert len(workflow.dos_kgrid_convergence_sequencer) == len(
                qe_dos_kgrid_convergence_kgrids)
        for seq in workflow.dos_kgrid_convergence_sequencer:
            assert await is_calculation_directory(seq.nscf_workdir)
            assert not await is_calculation_directory(seq.dos_workdir)
            await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_dos_kgrid_convergence_done(workflow, nscf=True)
        await workflow.write()
        for seq in workflow.dos_kgrid_convergence_sequencer:
            assert await is_calculation_directory(seq.dos_workdir)
            await aioshutil.rmtree(seq.dos_workdir)
        await self._set_dos_kgrid_convergence_done(workflow, dos=True)
        await workflow.run()
        assert await workflow.workflow_completed
        resdir = os.path.join(
                workflow.dos_kgrid_convergence_sequencer.root_workdir,
                "results")
        assert await aiofiles.os.path.isdir(resdir)
        assert len(await aiofiles.os.listdir(resdir)) == 2

    async def test_epw_wannierization(self):
        """Test the epw wannierization part of the workflow."""
        workflow = self._workflow_cls(
                band_structure=True,
                gs=True, epw_wannierization=True,
                phonon_dispersion=True,
                )
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_epw_wannierization(workflow)
        await workflow.write()
        # make sure nscf calc was written
        nscf = workflow.epw_wannierization_sequencer.nscf_workdir
        assert await aiofiles.os.path.isdir(nscf)
        await aioshutil.rmtree(nscf)
        await self._set_epw_wannierization_done(workflow, nscf=True)
        await workflow.write()
        # make sure epw part has been written
        epw = workflow.epw_wannierization_sequencer.epw_workdir
        assert await aiofiles.os.path.isdir(epw)
        await aioshutil.rmtree(epw)
        await self._set_epw_wannierization_done(workflow, epw=True)
        await workflow.write()
        # check epw disp interpolation part is written
        seq = workflow.epw_wannierization_sequencer
        assert await aiofiles.os.path.isdir(seq.epwdispinterpolation_workdir)
        await aioshutil.rmtree(seq.epwdispinterpolation_workdir)
        await self._set_epw_wannierization_done(
                workflow, epw_disp_interpolation=True)
        await workflow.run()
        # make sure results have been printed
        results = os.path.join(
                self.workflow_dir.name, "epw_wannierization", "results")
        assert len(await aiofiles.os.listdir(results)) >= 2

    async def test_epw_wannierization_with_lattice_expansion(self):
        """Test the epw interpolation w lattice exp part of the workflow."""
        workflow = self._workflow_cls(
                gs=True, lattice_expansion=True,
                epw_wannierization_with_thermal_expansion=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_lattice_expansion(workflow)
        await self._set_epw_wannierization_with_lattice_expansion(workflow)
        await workflow.write()
        # make sure no calculations for the epw run are written
        # since lattice expansion was not copied yet
        root_workdir = os.path.join(
                self.workflow_dir.name,
                "epw_wannierization_with_lattice_expansion")
        assert not await aiofiles.os.path.exists(root_workdir)
        # copy calculation for lattice expansion
        lat_seq = workflow.lattice_expansion_sequencer
        # copy scf calculations
        # rm all calcs that are not a symlink
        dirname = os.path.dirname(lat_seq.scf_workdir)
        coro = []
        for path in await aiofiles.os.listdir(dirname):
            join = os.path.join(dirname, path)
            if await aiofiles.os.path.islink(join):
                continue
            coro.append(aioshutil.rmtree(join))
        await asyncio.gather(*coro)
        await aioshutil.rmtree(
                os.path.dirname(
                    os.path.dirname(
                        workflow.lattice_expansion_sequencer.phonons_workdir)))

        await self._set_lattice_expansion_done(workflow)
        # reset this workflow part
        sequencer = workflow.lattice_expansion_sequencer
        await sequencer.clear_sequence()
        await workflow.run_lattice_expansion()
        # reset epw setup
        workflow.stop_at_workflow = None
        await self._set_epw_wannierization_with_lattice_expansion(workflow)
        await workflow.write()
        assert await aiofiles.os.path.exists(root_workdir), root_workdir
        # set all calculations done and check plots are made
        await aioshutil.rmtree(root_workdir)
        await self._set_epw_wannierization_with_lattice_expansion_done(
                workflow)
        await workflow.run()
        # check that a results directory has been created with results in it
        results = os.path.join(root_workdir, "results")
        assert await aiofiles.os.path.exists(results)
        assert len(await aiofiles.os.listdir(results)) > 2
        assert await workflow.workflow_completed

    async def test_epw_wannierization_with_manual_lattice_expansion(self):
        """Test the epw w lattice expansion part of the Workflow.

        Test the manual insertion of lattice exp.
        """
        # same as epw_with_lattice_expansion but we don't compute lattice exp
        workflow = self._workflow_cls(
                epw_wannierization_with_thermal_expansion=True)
        await self._set_epw_wannierization_with_manual_lattice_expansion(
                workflow)
        root_workdir = os.path.join(
                self.workflow_dir.name,
                "epw_with_lattice_expansion")
        await self._set_epw_wannierization_with_lattice_expansion_done(
                workflow)
        await workflow.run()
        # check that a results directory has been created with results in it
        results = os.path.join(root_workdir, "results")
        assert await aiofiles.os.path.exists(results)
        assert len(await aiofiles.os.listdir(results)) > 2
        assert await workflow.workflow_completed

    async def test_ibte(self):
        """Test the ibte part of the workflow."""
        workflow = self._workflow_cls(
                gs=True, band_structure=True, phonon_dispersion=True,
                epw_wannierization=True, ibte=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_epw_wannierization(workflow)
        await self._set_epw_wannierization_done(
                workflow, nscf=True, epw=True, epw_disp_interpolation=True)
        await self._set_ibte(workflow)
        await workflow.write()
        assert await aiofiles.os.path.isdir(
                    workflow.ibte_sequencer.ibte_workdir)
        await aioshutil.rmtree(workflow.ibte_sequencer.ibte_workdir)
        await self._set_ibte_done(workflow)
        await workflow.run()
        assert len(await aiofiles.os.listdir(
                    os.path.dirname(workflow.ibte_sequencer.plot_save))) == 28

    async def test_ibte_fsthick_convergence(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True, phonon_dispersion=True,
                epw_wannierization=True, ibte_fsthick_convergence=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_epw_wannierization(workflow)
        await self._set_epw_wannierization_done(
                workflow, nscf=True, epw=True, epw_disp_interpolation=True)
        await self._set_ibte_fsthick_convergence(workflow)
        await workflow.write()
        for sequencer in workflow.ibte_fsthick_convergence_sequencer:
            assert await aiofiles.os.path.isdir(sequencer.ibte_workdir)
            await aioshutil.rmtree(sequencer.ibte_workdir)
        await self._set_ibte_fsthick_convergence_done(workflow)
        await workflow.run()
        coro = []
        for sequencer in workflow.ibte_fsthick_convergence_sequencer:
            # make sure each fsthick calc has their plots
            coro.append(aiofiles.os.listdir(
                    os.path.dirname(sequencer.plot_save)))
        dirlengths = [len(d) for d in await asyncio.gather(*coro)]
        assert all(x == 28 for x in dirlengths), dirlengths
        # make sure a global results dir has plots in it
        path = os.path.join(
                os.path.dirname(
                    os.path.dirname(os.path.dirname(sequencer.plot_save))),
                "results")
        assert len(await aiofiles.os.listdir(path)) == 12, path

    async def test_ibte_fine_grid_convergence(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True, phonon_dispersion=True,
                epw_wannierization=True, ibte_fine_grid_convergence=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_epw_wannierization(workflow)
        await self._set_epw_wannierization_done(
                workflow, nscf=True, epw=True, epw_disp_interpolation=True)
        await self._set_ibte_fine_grid_convergence(workflow)
        await workflow.write()
        coro = []
        coro_del = []
        for sequencer in workflow.ibte_fine_grid_convergence_sequencer:
            coro.append(aiofiles.os.path.isdir(sequencer.ibte_workdir))
            coro_del.append(aioshutil.rmtree(sequencer.ibte_workdir))
        assert all(await asyncio.gather(*coro))
        await asyncio.gather(*coro_del)
        await self._set_ibte_fine_grid_convergence_done(workflow)
        await workflow.run()
        coro = []
        for sequencer in workflow.ibte_fine_grid_convergence_sequencer:
            # make sure each fsthick calc has their plots
            coro.append(aiofiles.os.listdir(
                    os.path.dirname(sequencer.plot_save)))
        lens = [len(x) for x in await asyncio.gather(*coro)]
        assert all([le == 28 for le in lens]), lens
        # make sure a global results dir has plots in it
        path = os.path.join(
                os.path.dirname(
                    os.path.dirname(os.path.dirname(sequencer.plot_save))),
                "results")
        assert len(await aiofiles.os.listdir(path)) == 144, path

    async def test_ibte_with_lattice_expansion(self):
        workflow = self._workflow_cls(
                gs=True, lattice_expansion=True,
                epw_wannierization_with_thermal_expansion=True,
                ibte_with_thermal_expansion=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_lattice_expansion(workflow)
        await self._set_lattice_expansion_done(workflow)
        await self._set_epw_wannierization_with_lattice_expansion(workflow)
        await self._set_epw_wannierization_with_lattice_expansion_done(
                workflow,
                )
        await self._set_ibte_with_lattice_expansion(workflow)
        await self._set_ibte_with_lattice_expansion_done(workflow)
        # check that results have been written
        await workflow.run()
        results = os.path.dirname(
                workflow.ibte_with_thermal_expansion_sequencer[0].plot_save)
        assert await aiofiles.os.path.exists(results)
        list_ = await aiofiles.os.listdir(results)
        assert len(list_) >= 4, list_

    async def test_ibte_with_manual_lattice_expansion(self):
        # samething as with lattice expansion but with manual settings
        workflow = self._workflow_cls(
                epw_wannierization_with_thermal_expansion=True,
                ibte_with_thermal_expansion=True,
                )
        await self._set_epw_wannierization_with_manual_lattice_expansion(
                workflow)
        await self._set_epw_wannierization_with_lattice_expansion_done(
                workflow)
        await self._set_ibte_with_lattice_expansion(workflow)
        await self._set_ibte_with_lattice_expansion_done(workflow)
        # check that results have been written
        await workflow.run()
        results = os.path.dirname(
                workflow.ibte_with_thermal_expansion_sequencer[0].plot_save)
        assert await aiofiles.os.path.exists(results)
        list_ = await aiofiles.os.listdir(results)
        assert len(list_) >= 4, list_

    async def test_ibte_coarse_kgrid_convergence(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True,
                phonon_dispersion=True,
                ibte_coarse_kgrid_convergence=True,
                )
        root_workdir = os.path.join(
                self.workflow_dir.name, "ibte_coarse_kgrid_convergence")
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_ibte_coarse_kgrid_convergence(workflow)
        await workflow.write()
        # check epw wannierization has been written
        root_content = await aiofiles.os.listdir(root_workdir)
        assert len(root_content) == 2
        assert not await workflow.workflow_completed
        for subdirname in root_content:
            subdir = os.path.join(root_workdir, subdirname)
            nscf_dir = os.path.join(subdir, "nscf_run")
            assert await is_calculation_directory(nscf_dir), nscf_dir
            await aioshutil.rmtree(nscf_dir)
        await self._set_ibte_coarse_kgrid_convergence_done(workflow, nscf=True)
        await workflow.write()
        assert not await workflow.workflow_completed
        for subdirname in root_content:
            subdir = os.path.join(root_workdir, subdirname)
            epw_wann_dir = os.path.join(subdir, "epw_wannierization_run")
            assert await is_calculation_directory(epw_wann_dir), epw_wann_dir
            await aioshutil.rmtree(epw_wann_dir)
        await self._set_ibte_coarse_kgrid_convergence_done(
                workflow, epw_wannierization=True,
                )
        await workflow.write()
        assert not await workflow.workflow_completed
        for subdirname in root_content:
            subdir = os.path.join(root_workdir, subdirname)
            epw_dir = os.path.join(
                    subdir, "epw_dispersion_interpolation_run")
            assert await is_calculation_directory(epw_dir), epw_dir
            await aioshutil.rmtree(epw_dir)
        await self._set_ibte_coarse_kgrid_convergence_done(
                workflow, epw_dispersion_interpolation=True,
                )
        # need to reset workflow as ibte sequencers need to set epw part
        workflow.ibte_coarse_kgrid_convergence_sequencer.reset()
        await self._set_ibte_coarse_kgrid_convergence(workflow)
        await workflow.write()
        assert not await workflow.workflow_completed
        for subdirname in root_content:
            subdir = os.path.join(root_workdir, subdirname)
            ibte_dir = os.path.join(
                    subdir, "ibte_run")
            assert await is_calculation_directory(ibte_dir), ibte_dir
            await aioshutil.rmtree(ibte_dir)
        await self._set_ibte_coarse_kgrid_convergence_done(
                workflow, ibte=True)
        await workflow.run()
        assert await workflow.workflow_completed
        resdir = os.path.join(root_workdir, "results")
        assert await aiofiles.os.path.isdir(resdir)
        assert len(await aiofiles.os.listdir(resdir)) == 80

    async def _set_ibte_coarse_kgrid_convergence(
            self, workflow: QEWorkflow) -> None:
        root_workdir = os.path.join(
                self.workflow_dir.name, "ibte_coarse_kgrid_convergence")
        await workflow.set_ibte_coarse_kgrid_convergence(
                root_workdir=root_workdir,
                use_gs=True, use_band_structure=True,
                use_phonon_dispersion=True,
                nscf_kgrids=[[4] * 3, [6] * 3],
                nscf_input_variables={
                    "tprnfor": True,
                    "tstress": True,
                    "conv_thr": 1e-10,
                    "diagonalization": "cg",
                    "diago_full_acc": True,
                    "noncolin": False,
                    "lspinorb": False,
                    "nbnd": 15,
                    },
                nscf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                epw_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                epw_input_variables={
                    "amass(1)": 207.2,
                    "elph": True,
                    "etf_mem": 0,
                    "epbwrite": True,
                    "epbread": False,
                    "epwwrite": True,
                    "epwread": False,
                    "nbndsub": 4,   # number of wannier functions to use
                    # "nbndskip": 10,  # nbands under the disentanglement windo
                    "bands_skipped": "exclude_bands = 1-5",
                    "wannierize": True,
                    "num_iter": 1000,
                    "wdata(1)": "dis_num_iter = 1000",
                    "wdata(2)": "guiding_centres = True",
                    "wdata(3)": "num_print_cycles = 100",
                    "wdata(4)": "conv_window = 10",
                    "dis_win_max": 21,
                    "dis_win_min": -3,
                    "dis_froz_min": -3,
                    "dis_froz_max": 13.5,
                    "proj(1)": "Pb:sp3",
                    "iverbosity": 0,
                    "elecselfen": False,
                    "phonselfen": False,
                    "fsthick": 6,  # fermi surface thickness
                    "degaussw": 0.0,
                    "ngaussw": -99,  # force FD distribution from the start
                    "a2f": False,
                    "vme": "wannier",
                    },
                ibte_input_variables={
                    "assume_metal": True,
                    "degaussw": 0.0,
                    "restart_step": 100,
                    "fsthick": 0.1,
                    "etf_mem": 0,
                    "ngaussw": -99,
                    "vme": "wannier",
                    "temps": [50, 100, 300, 600],
                    "nstemp": 4,
                    "nkf1": 10, "nkf2": 10, "nkf3": 10,
                    "nqf1": 10, "nqf2": 10, "nqf3": 10,
                    },
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False},
                )

    async def _set_ibte_coarse_kgrid_convergence_done(
            self, workflow: QEWorkflow,
            nscf: bool = False, epw_wannierization: bool = False,
            epw_dispersion_interpolation: bool = False,
            ibte: bool = False,
            ) -> None:
        src_root = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "qe_Pb",
                "ibte_coarse_kgrid_convergence",
                )
        epw_seqs = [
                seq for seq in workflow.ibte_coarse_kgrid_convergence_sequencer
                if not isinstance(seq, QEEPWIBTESequencer)
                ]
        ibte_seqs = [
                seq for seq in workflow.ibte_coarse_kgrid_convergence_sequencer
                if isinstance(seq, QEEPWIBTESequencer)]
        for seq in epw_seqs:
            nscf_dest = seq.nscf_workdir
            base = os.path.basename(os.path.dirname(nscf_dest))
            if nscf:
                nscf_src = os.path.join(src_root, base, "nscf_run")
                await copy_calculation(
                        nscf_src, nscf_dest,
                        new_parents=[seq.scf_workdir],
                        pseudo_dir=__PSEUDO_DIR__,
                        )
            if epw_wannierization:
                new_parents = [
                    seq.nscf_workdir,
                    seq.q2r_workdir,
                    ]
                for path in await aiofiles.os.listdir(
                        os.path.dirname(
                            seq.phonons_workdir)):
                    new_parents.append(
                            os.path.join(
                                os.path.dirname(seq.phonons_workdir),
                                path))
                epw_wannierization_src = os.path.join(
                        src_root, base, "epw_wannierization_run")
                await copy_calculation(
                        epw_wannierization_src,
                        seq.epw_workdir,
                        new_parents=new_parents,
                        )
            if epw_dispersion_interpolation:
                src = os.path.join(
                        src_root, base, "epw_dispersion_interpolation_run")
                await copy_calculation(
                        src, seq.epwdispinterpolation_workdir,
                        new_parents=[
                            seq.epw_workdir, seq.matdyn_workdir,
                            seq.q2r_workdir, seq.band_structure_workdir,
                            ],
                        )
        if ibte:
            for ibte_seq in ibte_seqs:
                ibte_dest = ibte_seq.ibte_workdir
                base = os.path.basename(os.path.dirname(ibte_dest))
                src = os.path.join(src_root, base, "ibte_run")
                await copy_calculation(
                        src, ibte_dest,
                        new_parents=[
                            ibte_seq.epw_wannierization_calculation],
                        )

    async def test_lattice_expansion(self):
        workflow = self._workflow_cls(gs=True, lattice_expansion=True)
        await self._set_gs(workflow)
        # copy gs calculation
        await self._set_gs_done(workflow)
        await self._set_lattice_expansion(workflow)
        # write workflow
        await workflow.write()
        # this should have written all the scf calcs.
        scf_rootdir = os.path.dirname(
                workflow.lattice_expansion_sequencer.scf_workdir)
        list_ = await aiofiles.os.listdir(scf_rootdir)
        assert len(list_) == (
                len(qe_thermal_lattice_expansion_deltas)), (
                list_)
        assert not await (workflow.workflow_completed)
        lat_seq = workflow.lattice_expansion_sequencer
        # copy scf calculations
        # rm all calcs that are not a symlink
        dirname = os.path.dirname(lat_seq.scf_workdir)
        coro = []
        for path in await aiofiles.os.listdir(dirname):
            join = os.path.join(dirname, path)
            if await aiofiles.os.path.islink(join):
                continue
            coro.append(aioshutil.rmtree(join))
        await asyncio.gather(*coro)
        await self._set_lattice_expansion_scf_calculations_done(workflow)
        await workflow.write()
        assert len(
                await aiofiles.os.listdir(os.path.dirname(
                    workflow.lattice_expansion_sequencer.phonons_workdir
                    ))) == (
                len(qe_thermal_lattice_expansion_deltas))
        assert not await (workflow.workflow_completed)
        # copy phonon calculations + kpoints
        # kpoints calc is ran at same time as non-gamma phonons
        # just copy it now but don't check it is written
        await aioshutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.phonons_workdir))
        await self._set_lattice_expansion_phonons_calculations_done(workflow)
        await self._set_lattice_expansion_kpoints_calculation_done(workflow)
        await workflow.write()
        assert len(await aiofiles.os.listdir(os.path.dirname(
                    workflow.lattice_expansion_sequencer.q2r_workdir))) == (
                len(qe_thermal_lattice_expansion_deltas))
        assert not await workflow.workflow_completed
        # copy the q2r calculations + kpoints calc
        await aioshutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.q2r_workdir))
        await self._set_lattice_expansion_q2r_calculations_done(workflow)
        await workflow.write()
        assert len(await aiofiles.os.listdir(
            os.path.dirname(lat_seq.matdyn_workdir))) == (
                len(qe_thermal_lattice_expansion_deltas))
        assert not await (workflow.workflow_completed)
        # copy matdyn calculations
        await aioshutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.matdyn_workdir))
        await aioshutil.rmtree(
                os.path.dirname(
                    workflow.lattice_expansion_sequencer.matdyngrid_workdir))
        await self._set_lattice_expansion_matdyn_calculations_done(workflow)
        await self._set_lattice_expansion_matdyngrid_calculations_done(
                workflow)
        await workflow.run()
        assert len(await aiofiles.os.listdir(
            os.path.dirname(lat_seq.plot_save))) == 46
        assert await (workflow.workflow_completed)

    async def test_phonon_dispersion_qgrid_convergence(self):
        workflow = self._workflow_cls(
                gs=True,
                phonon_dispersion_qgrid_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_phonon_dispersion_qgrid_convergence(workflow)
        await workflow.write()
        # make sure the first phonon calc was written for each qgrids
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        assert await aiofiles.os.path.exists(dir_)
        assert len(await aiofiles.os.listdir(dir_)) == (
                len(QE_PHONON_DISPERSION_QGRID_CONVERGENCE_PHONONS_QGRIDS)
                )
        for subdir in await aiofiles.os.listdir(dir_):
            assert len(await aiofiles.os.listdir(
                        os.path.join(dir_, subdir, "phonons_runs"))) == 1
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, first_phonon=True)
        await workflow.write()
        # check all phonon calcs have been written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            assert len(await aiofiles.os.listdir(
                        os.path.dirname(seq.phonons_workdir))) == seq.nphonons
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, all_phonons=True)
        await workflow.write()
        # make sure q2r calculation is written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            assert "q2r_run" in await aiofiles.os.listdir(dir_)
            await aioshutil.rmtree(os.path.join(dir_, "q2r_run"))
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, q2r=True)
        await workflow.write()
        # make sure the matdyn calc was written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            assert "matdyn_run" in await aiofiles.os.listdir(dir_)
            await aioshutil.rmtree(os.path.join(dir_, "matdyn_run"))
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, matdyn=True)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        results_dir = os.path.join(dir_, "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await (workflow.workflow_completed)

    async def test_ziman(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True, phonon_dispersion=True,
                epw_wannierization=True, ziman=True)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True, q2r=True, matdyn=True)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_epw_wannierization(workflow)
        await self._set_epw_wannierization_done(
                workflow, nscf=True, epw=True, epw_disp_interpolation=True)
        await self._set_ziman(workflow)
        await workflow.write()
        assert await aiofiles.os.path.isdir(
                    workflow.ziman_sequencer.ziman_workdir)
        await aioshutil.rmtree(workflow.ziman_sequencer.ziman_workdir)
        await self._set_ziman_done(workflow)
        await workflow.run()
        assert len(await aiofiles.os.listdir(
                    os.path.dirname(workflow.ziman_sequencer.plot_save))) == 2

    async def _check_phonon_dispersion_post_phonons(self, workflow):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        # make sure q2r calculation is written
        assert "q2r_run" in await aiofiles.os.listdir(dir_)
        await aioshutil.rmtree(os.path.join(dir_, "q2r_run"))
        await self._set_phonon_dispersion_done(workflow, q2r=True)
        await workflow.write()
        # make sure the matdyn calc was written
        assert "matdyn_run" in await aiofiles.os.listdir(dir_)
        await aioshutil.rmtree(os.path.join(dir_, "matdyn_run"))
        await self._set_phonon_dispersion_done(workflow, matdyn=True)

    async def _check_phonon_dispersion_qgrid_convergence_post_phonons(
            self, workflow):
        # make sure q2r calculation is written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            assert "q2r_run" in await aiofiles.os.listdir(dir_)
            await aioshutil.rmtree(os.path.join(dir_, "q2r_run"))
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, q2r=True)
        await workflow.write()
        # make sure the matdyn calc was written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.dirname(seq.q2r_workdir)
            assert "matdyn_run" in await aiofiles.os.listdir(dir_)
            await aioshutil.rmtree(os.path.join(dir_, "matdyn_run"))
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, matdyn=True)
        await workflow.run()

    async def _set_band_structure_done(self, *args, **kwargs):
        await super()._set_band_structure_done(
                *args, **kwargs, pseudo_dir=__PSEUDO_DIR__)

    async def _set_dos(self, workflow, *args, **kwargs):
        await super()._set_dos(
                workflow,
                nscf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                dos_calculation_parameters={
                    "command": self.dos_command_file.name,
                    "queuing_system": "local", "mpi_command": "",
                    },
                )
        # copy nscf calculation cirst
        await copy_calculation(
                qe_dos_nscf_example,
                workflow.dos_sequencer.nscf_workdir,
                new_parents=[workflow.dos_sequencer.scf_workdir],
                )

    async def _set_dos_done(self, workflow, *args, **kwargs):
        await super()._set_dos_done(
                workflow, *args,
                new_parents=[workflow.dos_sequencer.nscf_workdir],
                )

    async def _set_dos_kgrid_convergence(self, workflow):
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await workflow.set_dos_kgrid_convergence(
            root_workdir=os.path.join(
                self.workflow_dir.name, "dos_kgrid_convergence"),
            use_gs=True,
            dos_input_variables=qe_dos_kgrid_convergence_input_variables,
            dos_calculation_parameters={
                "command": self.dos_command_file.name,
                "queuing_system": "local", "mpi_command": "",
                },
            dos_fine_kpoint_grids_variables=qe_dos_kgrid_convergence_kgrids,
            nscf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            plot_calculation_parameters={"show": False},
            )

    async def _set_dos_kgrid_convergence_done(
            self, workflow, nscf=False, dos=False) -> None:
        if dos and nscf:
            raise DevError("Cannot copy both dos and nscf calcs at same time")
        coros = []
        for seq in workflow.dos_kgrid_convergence_sequencer:
            subdir = os.path.basename(os.path.dirname(seq.dos_workdir))
            if nscf:
                nscf_subdir = os.path.basename(seq.nscf_workdir)
                coros.append(copy_calculation(
                    os.path.join(
                        qe_dos_kgrid_convergence_example, subdir, nscf_subdir),
                    seq.nscf_workdir,
                    new_parents=[seq.scf_workdir],
                    ))
            if dos:
                dos_subdir = os.path.basename(seq.dos_workdir)
                coros.append(copy_calculation(
                    os.path.join(
                        qe_dos_kgrid_convergence_example, subdir, dos_subdir),
                    seq.dos_workdir,
                    new_parents=[seq.nscf_workdir],
                    ))
        await asyncio.gather(*coros)

    async def _set_epw_wannierization(self, workflow):
        await workflow.set_epw_wannierization(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "epw_wannierization"),
                use_gs=True,
                nscf_kgrid=qe_epw_wannierization_nscf_kgrid,
                nscf_input_variables=(
                    qe_epw_wannierization_nscf_input_variables.copy()),
                nscf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                use_band_structure=True,
                use_phonon_dispersion=True,
                epw_input_variables=(
                    qe_epw_wannierization_epw_input_variables.copy()),
                epw_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False},
                )

    async def _set_epw_wannierization_done(
            self, workflow, nscf=False, epw=False,
            epw_disp_interpolation=False, all_calculations=False):
        coro = []
        if nscf or all_calculations:
            coro.append(copy_calculation(
                    os.path.join(
                        qe_epw_wannierization_example, "nscf_run"),
                    workflow.epw_wannierization_sequencer.nscf_workdir,
                    pseudo_dir=__PSEUDO_DIR__,
                    new_parents=[
                        workflow.epw_wannierization_sequencer.scf_workdir]))
        if epw or all_calculations:
            new_parents = [
                workflow.epw_wannierization_sequencer.nscf_workdir,
                workflow.epw_wannierization_sequencer.q2r_workdir,
                # workflow.epw_wannierization_sequencer.matdyn_workdir,
                # workflow.epw_wannierization_sequencer.band_structure_workdir,
                ]
            for path in await aiofiles.os.listdir(
                    os.path.dirname(
                        workflow.phonon_dispersion_sequencer.phonons_workdir)):
                seq = workflow.phonon_dispersion_sequencer
                new_parents.append(
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir),
                            path))
            coro.append(copy_calculation(
                    os.path.join(
                        qe_epw_wannierization_example,
                        "epw_wannierization_run"),
                    workflow.epw_wannierization_sequencer.epw_workdir,
                    new_parents=new_parents,
                    )
                    )
        if epw_disp_interpolation or all_calculations:
            seq = workflow.epw_wannierization_sequencer
            new_parents = [
                    seq.epw_workdir,
                    seq.matdyn_workdir,
                    seq.q2r_workdir,
                    seq.band_structure_workdir,
                    ]
            coro.append(copy_calculation(
                    os.path.join(
                        qe_epw_wannierization_example,
                        "epw_dispersion_interpolation_run"),
                    seq.epwdispinterpolation_workdir,
                    new_parents=new_parents,
                    )
                    )
        await asyncio.gather(*coro)

    async def _set_gs_done(self, *args, **kwargs):
        await super()._set_gs_done(*args, **kwargs, pseudo_dir=__PSEUDO_DIR__)

    async def _set_gs_ecut_convergence_done(self, *args, **kwargs):
        await super()._set_gs_ecut_convergence_done(
                *args, pseudo_dir=__PSEUDO_DIR__, **kwargs)

    async def _set_gs_kgrid_convergence_done(self, *args, **kwargs):
        await super()._set_gs_kgrid_convergence_done(
                *args, pseudo_dir=__PSEUDO_DIR__, **kwargs)

    async def _set_gs_smearing_convergence_done(self, workflow):
        await super()._set_gs_smearing_convergence_done(
                workflow,
                pseudo_dir=__PSEUDO_DIR__,
                )

    async def _set_ibte(self, workflow):
        await workflow.set_ibte(
                root_workdir=os.path.join(self.workflow_dir.name, "ibte"),
                ibte_input_variables=QE_IBTE_INPUT_VARIABLES,
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False})

    async def _set_ibte_done(self, workflow):
        await copy_calculation(
                QE_IBTE_EXAMPLE,
                workflow.ibte_sequencer.ibte_workdir,
                new_parents=[
                    workflow.epw_wannierization_sequencer.epw_workdir],
                )

    async def _set_ibte_fsthick_convergence(self, workflow):
        await workflow.set_ibte_fsthick_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "ibte_fsthick_convergence"),
                ibte_input_variables=(
                    QE_IBTE_FSTHICK_CONVERGENCE_INPUT_VARIABLES),
                ibte_fsthicks=(
                    QE_IBTE_FSTHICK_CONVERGENCE_FSTHICKS),
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False})

    async def _set_ibte_fsthick_convergence_done(self, workflow):
        await asyncio.gather(
                *(copy_calculation(
                    os.path.join(
                        QE_IBTE_FSTHICK_CONVERGENCE_EXAMPLE,
                        os.path.basename(
                            os.path.dirname(sequencer.ibte_workdir)),
                        "ibte_run"),
                    sequencer.ibte_workdir,
                    new_parents=[
                        workflow.epw_wannierization_sequencer.epw_workdir],
                    )
                    for sequencer in (
                        workflow.ibte_fsthick_convergence_sequencer))
                )

    async def _set_ibte_fine_grid_convergence(self, workflow):
        await workflow.set_ibte_fine_grid_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "ibte_fine_grid_convergence"),
                ibte_input_variables=(
                    QE_IBTE_FINE_GRID_CONVERGENCE_INPUT_VARIABLES),
                ibte_fine_grids=(
                    QE_IBTE_FINE_GRID_CONVERGENCE_FINE_GRIDS),
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False})

    async def _set_ibte_fine_grid_convergence_done(self, workflow):
        coro = []
        for sequencer in workflow.ibte_fine_grid_convergence_sequencer:
            coro.append(copy_calculation(
                    os.path.join(
                        QE_IBTE_FINE_GRID_CONVERGENCE_EXAMPLE,
                        os.path.basename(
                            os.path.dirname(sequencer.ibte_workdir)),
                        "ibte_run"),
                    sequencer.ibte_workdir,
                    new_parents=[
                        workflow.epw_wannierization_sequencer.epw_workdir],
                    ))
        await asyncio.gather(*coro)

    async def _set_ibte_with_lattice_expansion_done(self, workflow):
        coro = []
        for seq in workflow.ibte_with_thermal_expansion_sequencer:
            ibte_workdir = os.path.dirname(seq.ibte_workdir)
            calc = os.path.basename(seq.ibte_workdir)
            # fix here for legacy, source has its dirname in integer form
            calc_int = str(int(float(calc.split("K")[0]))) + "K"
            source = os.path.join(
                        __QE_IBTE_WITH_THERMAL_EXPANSION_EXAMPLES__, calc_int)
            target = os.path.join(ibte_workdir, calc)
            coro.append(copy_calculation(
                    source, target,
                    new_parents=[seq.epw_wannierization_calculation],
                    title=calc, prefix=calc,
                    ))
        await asyncio.gather(*coro)

    async def _set_epw_wannierization_with_lattice_expansion_done(
            self, workflow):
        # set scf calculations
        seq = workflow.epw_wannierization_with_thermal_expansion_sequencer[0]
        scf_workdir = os.path.dirname(seq.scf_workdir)
        coro = []
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__):
            coro.append(copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_SCF_EXAMPLES__, calc),
                    os.path.join(scf_workdir, calc),
                    pseudo_dir=__PSEUDO_DIR__,
                    ))
        # set phonons calculations
        ph_workdir = os.path.dirname(os.path.dirname(seq.phonons_workdir))
        for temp_dir in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__):
            for calc in await aiofiles.os.listdir(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__,
                        temp_dir)):
                source = os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_PHONONS_EXAMPLES__,
                        temp_dir, calc)
                new_parent = os.path.join(scf_workdir, temp_dir)
                coro.append(copy_calculation(
                        source,
                        os.path.join(ph_workdir, temp_dir, calc),
                        new_parents=[new_parent]))
        # set band structures
        ex = __QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__
        band_structure_workdir = os.path.dirname(seq.band_structure_workdir)
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_BAND_STRUCTURE_EXAMPLES__):
            coro.append(copy_calculation(
                    os.path.join(ex, calc),
                    os.path.join(band_structure_workdir, calc),
                    new_parents=[os.path.join(scf_workdir, calc)],
                    pseudo_dir=__PSEUDO_DIR__,
                    ))
        # set nscf calcs
        nscf_workdir = os.path.dirname(seq.nscf_workdir)
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__):
            coro.append(copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_NSCF_EXAMPLES__, calc),
                    os.path.join(nscf_workdir, calc),
                    new_parents=[os.path.join(scf_workdir, calc)],
                    pseudo_dir=__PSEUDO_DIR__,
                    ))
        await asyncio.gather(*coro)
        coro = []
        # set q2r calcs
        q2r_workdir = os.path.dirname(seq.q2r_workdir)
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__):
            phdir = os.path.join(ph_workdir, calc)
            coro.append(copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_Q2R_EXAMPLES__, calc),
                    os.path.join(q2r_workdir, calc),
                    new_parents=[
                        os.path.join(phdir, ph)
                        for ph in await aiofiles.os.listdir(phdir)],
                    ))
        # set matdyn calcs
        matdyn_workdir = os.path.dirname(seq.matdyn_workdir)
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__):
            coro.append(copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_MATDYN_EXAMPLES__,
                        calc),
                    os.path.join(matdyn_workdir, calc),
                    new_parents=[os.path.join(q2r_workdir, calc)],
                    ))
        # set epw wannierization calcs
        epw_workdir = os.path.dirname(seq.epw_workdir)
        for calc in await aiofiles.os.listdir(
                __QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__):
            phdir = os.path.join(ph_workdir, calc)
            coro.append(copy_calculation(
                    os.path.join(
                        __QE_EPW_WITH_THERMAL_EXPANSION_EPW_EXAMPLES__, calc),
                    os.path.join(epw_workdir, calc),
                    new_parents=[
                        os.path.join(nscf_workdir, calc),
                        # os.path.join(band_structure_workdir, calc),
                        # os.path.join(matdyn_workdir, calc),
                        os.path.join(q2r_workdir, calc),
                        ] + [os.path.join(phdir, ph)
                             for ph in await aiofiles.os.listdir(phdir)],
                    ))
        # set epw disp interpolation calcs
        epwdisp_workdir = os.path.dirname(seq.epwdispinterpolation_workdir)
        src = __QE_EPW_WITH_THERMAL_EXPANSION_EPWDISPINTERPOLATION_EXAMPLES__
        for calc in await aiofiles.os.listdir(src):
            coro.append(copy_calculation(
                    os.path.join(src, calc),
                    os.path.join(epwdisp_workdir, calc),
                    new_parents=[
                        os.path.join(epw_workdir, calc),
                        os.path.join(band_structure_workdir, calc),
                        os.path.join(matdyn_workdir, calc),
                        os.path.join(q2r_workdir, calc),
                        ],
                    ))
        await asyncio.gather(*coro)

    async def _set_epw_wannierization_with_manual_lattice_expansion(
            self, workflow):
        # set epw calculations
        gamma = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
        x = {"X": [0.0, 0.5, 0.5]}   # X
        k = {"K": [3/8, 3/8, 3/4]}
        lpt = {"L": [1/2, 1/2, 1/2]}
        w = {"W": [1/4, 1/2, 3/4]}
        nqpts = 100  # number of points between each symmetry points
        nkpts = 100
        kpath = [gamma, x, w, lpt, k, gamma, w]
        qpath = kpath
        root_workdir = os.path.join(
                self.workflow_dir.name, "epw_with_lattice_expansion")
        await workflow.set_epw_wannierization_with_thermal_expansion(
                root_workdir=root_workdir,
                lattice_parameters=[{"celldm(1)": a} for a in [
                    9.114516294987888,
                    9.127129240832433,
                    9.148714520867596,
                    9.207252333572992,
                    ]],
                temperatures=[50.0, 150.0, 300.0, 600.0],
                asr="crystal",
                scf_input_variables={
                    "calculation": "scf",
                    "pseudo_dir": __PSEUDO_DIR__,
                    "conv_thr": 1.0e-8,
                    "occupations": "smearing",
                    "ecutwfc": 40,
                    "degauss": 0.1,
                    "k_points": {"parameter": "automatic",
                                 "k_points": [6, 6, 6, 1, 1, 1],
                                 },
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    },
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                nscf_kgrid=[6, 6, 6],
                nscf_input_variables={
                    "tprnfor": True,
                    "tstress": True,
                    "ecutwfc": 40,
                    "occupations": "smearing",
                    "degauss": 0.1,
                    "conv_thr": 1e-10,
                    "diagonalization": "cg",
                    "diago_full_acc": True,
                    "pseudo_dir": __PSEUDO_DIR__,
                    "noncolin": False,
                    "lspinorb": False,
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    "nbnd": 15,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                band_structure_input_variables={
                    "calculation": "scf",
                    "pseudo_dir": __PSEUDO_DIR__,
                    "conv_thr": 1.0e-8,
                    "occupations": "smearing",
                    "ecutwfc": 40,
                    "degauss": 0.1,
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    },
                band_structure_kpoint_path=kpath,
                band_structure_kpoint_path_density=nkpts,
                band_structure_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                phonons_input_variables={
                    "tr2_ph": 1e-12,
                    },
                phonons_qpoint_grid=[2, 2, 2],
                phonons_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.phonons_command_file.name,
                    },
                q2r_calculation_parameters={
                    "command": self.q2r_command_file.name,
                    "queuing_system": "local",
                    },
                qpoint_path=qpath,
                qpoint_path_density=nqpts,
                matdyn_calculation_parameters={
                    "command": self.matdyn_command_file.name,
                    "queuing_system": "local",
                    },
                epw_input_variables={
                    "amass(1)": 207.2,
                    "elph": True,
                    "band_plot": True,
                    "etf_mem": 0,
                    "epbwrite": True,
                    "epbread": False,
                    "epwwrite": True,
                    "epwread": False,
                    "nbndsub": 4,   # number of wannier functions to use
                    "bands_skipped": "exclude_bands = 1-5",
                    "wannierize": True,
                    "num_iter": 1000,
                    "wdata(1)": "dis_num_iter = 1000",
                    "wdata(2)": "guiding_centres = True",
                    "wdata(3)": "num_print_cycles = 100",
                    "wdata(4)": "conv_window = 10",
                    "dis_win_max": 21,
                    "dis_win_min": -3,
                    "dis_froz_min": -3,
                    "dis_froz_max": 13.5,
                    "proj(1)": "Pb:sp3",
                    "iverbosity": 0,
                    "elecselfen": False,
                    "phonselfen": False,
                    "fsthick": 6,  # fermi surface thickness
                    "degaussw": 0.0,
                    "ngaussw": -99,  # force FD distribution from the start
                    "a2f": False,
                    "vme": "wannier",
                    },
                epw_calculation_parameters={
                        "queuing_system": "local",
                        "command": self.epw_command_file.name,
                        },
                q2r_input_variables={},
                plot_calculation_parameters={"show": False},
                )

    async def _set_epw_wannierization_with_lattice_expansion(self, workflow):
        # set epw calculations
        gamma = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
        x = {"X": [0.0, 0.5, 0.5]}   # X
        k = {"K": [3/8, 3/8, 3/4]}
        lpt = {"L": [1/2, 1/2, 1/2]}
        w = {"W": [1/4, 1/2, 3/4]}
        nqpts = 100  # number of points between each symmetry points
        nkpts = 100
        kpath = [gamma, x, w, lpt, k, gamma, w]
        qpath = kpath
        root_workdir = os.path.join(
                self.workflow_dir.name,
                "epw_wannierization_with_lattice_expansion")
        await workflow.set_epw_wannierization_with_thermal_expansion(
                root_workdir=root_workdir,
                asr="crystal",
                scf_input_variables={
                    "calculation": "scf",
                    "pseudo_dir": __PSEUDO_DIR__,
                    "conv_thr": 1.0e-8,
                    "occupations": "smearing",
                    "ecutwfc": 40,
                    "degauss": 0.1,
                    "k_points": {"parameter": "automatic",
                                 "k_points": [6, 6, 6, 1, 1, 1],
                                 },
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    },
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                nscf_kgrid=[6, 6, 6],
                nscf_input_variables={
                    "tprnfor": True,
                    "tstress": True,
                    "ecutwfc": 40,
                    "occupations": "smearing",
                    "degauss": 0.1,
                    "conv_thr": 1e-10,
                    "diagonalization": "cg",
                    "diago_full_acc": True,
                    "pseudo_dir": __PSEUDO_DIR__,
                    "noncolin": False,
                    "lspinorb": False,
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    "nbnd": 15,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                band_structure_input_variables={
                    "calculation": "scf",
                    "pseudo_dir": __PSEUDO_DIR__,
                    "conv_thr": 1.0e-8,
                    "occupations": "smearing",
                    "ecutwfc": 40,
                    "degauss": 0.1,
                    "ntyp": 1, "nat": 1, "ibrav": 2,
                    "atomic_species": [
                        {"atom": "Pb",
                         "atomic_mass": 207.2,
                         "pseudo": "pb_s.UPF",
                         }],
                    "atomic_positions": {"parameter": "crystal",
                                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
                    },
                band_structure_kpoint_path=kpath,
                band_structure_kpoint_path_density=nkpts,
                band_structure_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local"},
                qpoint_path=qpath,
                qpoint_path_density=nqpts,
                epw_input_variables={
                    "amass(1)": 207.2,
                    "elph": True,
                    "band_plot": True,
                    "etf_mem": 0,
                    "epbwrite": True,
                    "epbread": False,
                    "epwwrite": True,
                    "epwread": False,
                    "nbndsub": 4,   # number of wannier functions to use
                    "bands_skipped": "exclude_bands = 1-5",
                    "wannierize": True,
                    "num_iter": 1000,
                    "wdata(1)": "dis_num_iter = 1000",
                    "wdata(2)": "guiding_centres = True",
                    "wdata(3)": "num_print_cycles = 100",
                    "wdata(4)": "conv_window = 10",
                    "dis_win_max": 21,
                    "dis_win_min": -3,
                    "dis_froz_min": -3,
                    "dis_froz_max": 13.5,
                    "proj(1)": "Pb:sp3",
                    "iverbosity": 0,
                    "elecselfen": False,
                    "phonselfen": False,
                    "fsthick": 6,  # fermi surface thickness
                    "degaussw": 0.0,
                    "ngaussw": -99,  # force FD distribution from the start
                    "a2f": False,
                    "vme": "wannier",
                    },
                epw_calculation_parameters={
                        "queuing_system": "local",
                        "command": self.epw_command_file.name,
                        },
                phonons_qpoint_grid=[2, 2, 2],
                phonons_input_variables={
                        "ldisp": True,
                        "tr2_ph": 1e-12,
                        },
                phonons_calculation_parameters={
                        "command": self.phonons_command_file.name,
                        "queuing_system": "local",
                        },
                q2r_input_variables={},
                q2r_calculation_parameters={
                        "command": self.q2r_command_file.name,
                        "queuing_system": "local",
                        },
                matdyn_calculation_parameters={
                        "command": self.matdyn_command_file.name,
                        "queuing_system": "local",
                        },
                plot_calculation_parameters={"show": False},
                )

    async def _set_fermi_surface_done(self, *args, **kwargs):
        await super()._set_fermi_surface_done(
                *args,
                pseudo_dir=__PSEUDO_DIR__, **kwargs,
                )

    async def _set_ibte_with_lattice_expansion(self, workflow):
        root_workdir = os.path.join(
                self.workflow_dir.name, "ibte_with_lattice_expansion")
        workflow.relaxed_geometry = {"celldm(1)": 9.134699723318054}
        await workflow.set_ibte_with_thermal_expansion(
                root_workdir=root_workdir,
                ibte_input_variables={
                    "assume_metal": True,
                    "restart_step": 100,
                    "degaussw": 0.0,
                    "etf_mem": 0,
                    "fsthick": 0.1,
                    "ngaussw": -99,
                    "vme": "wannier",
                    "nkf1": 10, "nkf2": 10, "nkf3": 10,
                    "nqf1": 10, "nqf2": 10, "nqf3": 10},
                ibte_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={
                    "show": False, "elements": [[0, 0]], },
                )

    async def _set_lattice_expansion(self, workflow):
        # setup lattice_expansion calculations
        workflow.relaxed_geometry = {"celldm(1)": 9.134699723318054}
        await workflow.set_lattice_expansion(
                deltas_volumes=qe_thermal_lattice_expansion_deltas,
                asr=qe_thermal_lattice_expansion_asr,
                temperatures=qe_thermal_lattice_expansion_temperatures,
                bulk_modulus_initial_guess=(
                    qe_thermal_lattice_expansion_bulk_modulus_initial_guess),
                root_workdir=os.path.join(
                    self.workflow_dir.name, "lattice_expansion"),
                scf_input_variables=(
                    qe_thermal_lattice_expansion_scf_input_variables),
                scf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                phonons_input_variables=(
                    qe_thermal_lattice_expansion_phonons_input_variables),
                phonons_qpoint_grid=(
                    qe_thermal_lattice_expansion_phonons_qgrid),
                phonons_fine_qpoint_grid=(
                    qe_thermal_lattice_expansion_phonons_fine_qpoint_grid),
                phonons_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.phonons_command_file.name,
                    },
                qpoint_path=qe_thermal_lattice_expansion_phdisp_qpath,
                qpoint_path_density=qe_thermal_lattice_expansion_phdisp_nqpts,
                q2r_input_variables={},
                q2r_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.q2r_command_file.name},
                matdyn_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.matdyn_command_file.name},
                plot_calculation_parameters={"plot_show": False},
                )

    async def _set_lattice_expansion_scf_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        coro = []
        root = os.path.join(qe_thermal_lattice_expansion_example, "scf_runs")
        for calc in await aiofiles.os.listdir(root):
            # if "+0percent" in calc:
            #     # will be symlinked
            #     continue
            coro.append(copy_calculation(
                    os.path.join(root, calc),
                    os.path.join(
                        os.path.dirname(lat_seq.scf_workdir), calc),
                    pseudo_dir=__PSEUDO_DIR__,
                    ))
        await asyncio.gather(*coro)

    async def _set_lattice_expansion_phonons_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        coro = []
        root = os.path.join(
                qe_thermal_lattice_expansion_example, "phonons_runs")
        for vol_dir in await aiofiles.os.listdir(root):
            for calc in await aiofiles.os.listdir(
                    os.path.join(
                        root, vol_dir)):
                source = os.path.join(
                        root, vol_dir, calc)
                suffix = "_" + os.path.dirname(source).split("_")[-1]
                new_parent = lat_seq.scf_workdir + suffix
                coro.append(copy_calculation(
                        source,
                        os.path.join(
                            os.path.dirname(
                                lat_seq.phonons_workdir), vol_dir, calc),
                        new_parents=[new_parent]))
        await asyncio.gather(*coro)

    async def _set_lattice_expansion_q2r_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        coro = []
        root = os.path.join(qe_thermal_lattice_expansion_example, "q2r_runs")
        for calc in await aiofiles.os.listdir(root):
            suffix = "_" + calc.split("_")[-1]
            parent_root = lat_seq.phonons_workdir + suffix
            coro.append(copy_calculation(
                    os.path.join(root, calc),
                    os.path.join(os.path.dirname(lat_seq.q2r_workdir), calc),
                    new_parents=[
                        os.path.join(parent_root, x)
                        for x in await aiofiles.os.listdir(parent_root)]))
        await asyncio.gather(*coro)

    async def _set_lattice_expansion_matdyngrid_calculations_done(
            self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        coro = []
        root = os.path.join(
                qe_thermal_lattice_expansion_example, "matdyngrid_runs")
        for calc in await aiofiles.os.listdir(root):
            suffix = "_" + calc.split("_")[-1]
            parent = workflow.lattice_expansion_sequencer.q2r_workdir + suffix
            coro.append(copy_calculation(
                    os.path.join(root, calc),
                    os.path.join(
                        os.path.dirname(lat_seq.matdyngrid_workdir), calc),
                    new_parents=[parent]))
        await asyncio.gather(*coro)

    async def _set_lattice_expansion_matdyn_calculations_done(self, workflow):
        lat_seq = workflow.lattice_expansion_sequencer
        coro = []
        root = os.path.join(
                qe_thermal_lattice_expansion_example, "matdyn_runs")
        for calc in await aiofiles.os.listdir(root):
            suffix = "_" + calc.split("_")[-1]
            parent = workflow.lattice_expansion_sequencer.q2r_workdir + suffix
            coro.append(copy_calculation(
                    os.path.join(root, calc),
                    os.path.join(
                        os.path.dirname(lat_seq.matdyn_workdir), calc),
                    new_parents=[parent]))
        await asyncio.gather(*coro)

    async def _set_lattice_expansion_kpoints_calculation_done(
            self, workflow: QEWorkflow) -> None:
        lat_seq = workflow.lattice_expansion_sequencer
        await copy_calculation(
                os.path.join(
                    qe_thermal_lattice_expansion_example, "fine_qpoint_grid"),
                lat_seq.kpoints_workdir,
                )

    async def _set_lattice_expansion_done(self, workflow: QEWorkflow) -> None:
        await self._set_lattice_expansion_scf_calculations_done(
            workflow)
        await self._set_lattice_expansion_phonons_calculations_done(
            workflow)
        await self._set_lattice_expansion_kpoints_calculation_done(
                workflow)
        await self._set_lattice_expansion_q2r_calculations_done(
            workflow)
        await self._set_lattice_expansion_matdyngrid_calculations_done(
            workflow)
        await self._set_lattice_expansion_matdyn_calculations_done(
            workflow)

    async def _set_phonon_dispersion(self, workflow):
        await super()._set_phonon_dispersion(
                workflow,
                q2r_input_variables={},
                q2r_calculation_parameters={
                    "command": self.q2r_command_file.name,
                    "queuing_system": "local"},
                asr="crystal",
                matdyn_calculation_parameters={
                    "command": self.matdyn_command_file.name,
                    "queuing_system": "local"},
                )

    async def _set_phonon_dispersion_done(
            self, workflow, first_phonon=False, q2r=False, matdyn=False,
            **kwargs):
        coro = []
        await super()._set_phonon_dispersion_done(workflow, **kwargs)
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if first_phonon:
            coro.append(copy_calculation(
                os.path.join(root, "phonons_runs", "ph_q1"),
                os.path.join(
                    os.path.dirname(seq.phonons_workdir), "ph_q1"),
                new_parents=[seq.scf_workdir],
                **kwargs))
        if q2r:
            phroot = os.path.dirname(seq.phonons_workdir)
            coro.append(copy_calculation(
                    os.path.join(root, "q2r_run"),
                    seq.q2r_workdir,
                    new_parents=[
                        os.path.join(phroot, calcdir)
                        for calcdir in await aiofiles.os.listdir(phroot)],
                    ))
        if matdyn:
            coro.append(copy_calculation(
                    os.path.join(root, "matdyn_run"),
                    seq.matdyn_workdir,
                    new_parents=[seq.q2r_workdir],
                    ))
        await asyncio.gather(*coro)

    async def _set_phonon_dispersion_qgrid_convergence(self, *args):
        await super()._set_phonon_dispersion_qgrid_convergence(
                *args, q2r_input_variables={},
                q2r_calculation_parameters={
                    "command": self.q2r_command_file.name,
                    "queuing_system": "local"},
                asr="crystal",
                matdyn_calculation_parameters={
                    "command": self.matdyn_command_file.name,
                    "queuing_system": "local"},
                )

    async def _set_phonon_dispersion_qgrid_convergence_done(
            self, workflow, first_phonon=False, q2r=False, matdyn=False,
            **kwargs):
        coro = []
        coro.append(super()._set_phonon_dispersion_qgrid_convergence_done(
                workflow, **kwargs))
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            root = os.path.join(
                    self._phonon_dispersion_qgrid_convergence_example,
                    os.path.basename(os.path.dirname(
                        os.path.dirname(seq.phonons_workdir))))
            if first_phonon:
                coro.append(copy_calculation(
                        os.path.join(
                            root, "phonons_runs", "ph_q1"),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir),
                            "ph_q1"),
                        new_parents=[seq.scf_workdir],
                        ))
            if q2r:
                phroot = os.path.dirname(seq.phonons_workdir)
                coro.append(copy_calculation(
                        os.path.join(root, "q2r_run"),
                        seq.q2r_workdir,
                        new_parents=[
                            os.path.join(phroot, calcdir)
                            for calcdir in await aiofiles.os.listdir(phroot)],
                        ))
            if matdyn:
                coro.append(copy_calculation(
                        os.path.join(root, "matdyn_run"),
                        seq.matdyn_workdir,
                        new_parents=[seq.q2r_workdir],
                        ))
        await asyncio.gather(*coro)

    async def _set_phonon_ecut_convergence_done(self, *args, **kwargs):
        await super()._set_phonon_ecut_convergence_done(
                *args, _copy_scf_extra_kwargs={"pseudo_dir": __PSEUDO_DIR__},
                **kwargs)

    async def _set_phonon_kgrid_convergence_done(self, *args, **kwargs):
        await super()._set_phonon_kgrid_convergence_done(
                *args, copy_scf_extra_kwargs={"pseudo_dir": __PSEUDO_DIR__},
                **kwargs)

    async def _set_phonon_smearing_convergence_done(self, *args, **kwargs):
        await super()._set_phonon_smearing_convergence_done(
                *args,
                copy_scf_extra_kwargs={"pseudo_dir": __PSEUDO_DIR__}, **kwargs)

    async def _set_relaxation(self, *args, **kwargs):
        await super()._set_relaxation(
                *args, relax_atoms=False, **kwargs)

    async def _set_relaxation_done(self, workflow):
        dirlist = await aiofiles.os.listdir(qe_relaxation_example)
        coro = []
        for calcdir in dirlist:
            new_parents = []
            if not calcdir.endswith("1"):
                # add dependencies
                end = calcdir.split("_")[-1]
                new = str(int(end) - 1)
                new_parents = [
                        os.path.join(
                            workflow.relaxation_sequencer.scf_workdir,
                            calcdir.replace("_" + end, "_" + new))]
            coro.append(copy_calculation(
                    os.path.join(qe_relaxation_example, calcdir),
                    os.path.join(
                        workflow.relaxation_sequencer.scf_workdir, calcdir),
                    pseudo_dir=__PSEUDO_DIR__,
                    new_parents=new_parents))
        await asyncio.gather(*coro)

    async def _set_ziman(self, workflow):
        await workflow.set_ziman(
                root_workdir=os.path.join(self.workflow_dir.name, "ziman"),
                ziman_input_variables=__QE_ZIMAN_INPUT_VARIABLES__,
                ziman_calculation_parameters={
                    "command": self.epw_command_file.name,
                    "queuing_system": "local",
                    },
                plot_calculation_parameters={"show": False})

    async def _set_ziman_done(self, workflow):
        await copy_calculation(
                __QE_ZIMAN_EXAMPLE__,
                workflow.ziman_sequencer.ziman_workdir,
                new_parents=[
                    workflow.epw_wannierization_sequencer.epw_workdir],
                )
