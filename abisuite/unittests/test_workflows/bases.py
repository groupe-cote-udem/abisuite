import abc
import asyncio
import os
import tempfile
from typing import Any, Mapping

import aiofiles
import aiofiles.os

import aioshutil

import pytest

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import is_calculation_directory
from ...workflows.bases import BaseWorkflow


__TEST_WORKFLOW_ROOT__ = os.path.dirname(os.path.abspath(__file__))


class BaseWorkflowTest(abc.ABC):
    """Base class for workflow unittests.

    Defines common unittests and helper functions.
    """

    _band_structure_example = None
    _band_structure_input_variables = None
    _band_structure_kpoint_path = None
    _band_structure_kpoint_path_density = None
    _dos_example = None
    _dos_input_variables = None
    _dos_fine_kpoint_grid_variables = None
    _fermi_surface_example = None
    _gs_ecut_convergence_criterion = None
    _gs_ecut_convergence_input_vars = None
    _gs_ecut_convergence_ecuts = None
    _gs_ecut_convergence_example = None
    _gs_ecut_convergence_other_kwargs = None
    _gs_kgrid_convergence_criterion = None
    _gs_kgrid_convergence_input_vars = None
    _gs_kgrid_convergence_kgrids = None
    _gs_kgrid_convergence_example = None
    _gs_kgrid_convergence_other_kwargs = None
    _gs_smearing_convergence_criterion = None
    _gs_smearing_convergence_input_vars = None
    _gs_smearing_convergence_kgrids = None
    _gs_smearing_convergence_smearings = None
    _gs_smearing_convergence_example = None
    _gs_smearing_convergence_other_kwargs = None
    _gs_example = None
    _gs_input_vars = None
    _phonon_ecut_convergence_criterion = None
    _phonon_ecut_convergence_scf_input_vars = None
    _phonon_ecut_convergence_phonons_input_vars = None
    _phonon_ecut_convergence_ecuts = None
    _phonon_ecut_convergence_example = None
    _phonon_ecut_convergence_phonon_qpt = None
    _phonon_ecut_convergence_other_kwargs = None
    _phonon_dispersion_example = None
    _phonon_dispersion_phonons_input_vars = None
    _phonon_dispersion_qgrid = None
    _phonon_dispersion_qpoint_path = None
    _phonon_dispersion_qpoint_path_density = None
    _phonon_dispersion_qgrid_convergence_qgrids = None
    _phonon_dispersion_qgrid_convergence_example = None
    _phonon_kgrid_convergence_phonons_input_variables = None
    _phonon_kgrid_convergence_phonons_examples = None
    _phonon_kgrid_convergence_scf_examples = None
    _phonon_kgrid_convergence_scf_kgrids = None
    _phonon_kgrid_convergence_scf_input_variables = None
    _phonon_smearing_convergence_example = None
    _phonon_smearing_convergence_scf_input_vars = None
    _phonon_smearing_convergence_phonons_input_vars = None
    _phonon_smearing_convergence_kgrids = None
    _phonon_smearing_convergence_phonon_qpt = None
    _phonon_smearing_convergence_smearings = None
    _phonon_smearing_convergence_criterion = None
    _phonon_smearing_convergence_other_kwargs = {}
    _relaxation_input_vars = None
    _relaxation_other_kwargs = None
    _scf_script_name = None
    _phonons_script_name = None
    _set_gs_calculation_kwargs = None
    _workflow_cls = None
    _workflow_init_kwargs = None

    @pytest.fixture(autouse=True)
    def setup_workflow(
            self, setup_workflow_tempdir, setup_scf_script,
            setup_dos_script,
            setup_phonons_script):
        """Setup fixture for workflow unittests."""
        # GENERAL PART
        if self._workflow_cls is None:
            raise DevError("Need to set '_workflow_cls'.")
        if self._workflow_init_kwargs is None:
            self._workflow_init_kwargs = {}
        if self._scf_script_name is None:
            raise DevError("Need to set '_scf_script_name'.")
        if self._phonons_script_name is None:
            raise DevError("Need to set '_phonons_script_name'.")
        # GS PART
        if self._gs_example is None:
            raise DevError("Need to set '_gs_example'.")
        if self._gs_input_vars is None:
            raise DevError("Need to set '_gs_input_vars'.")
        if self._set_gs_calculation_kwargs is None:
            self._set_gs_calculation_kwargs = {}
        # GS ECUT CONVERGENCE PART
        if self._gs_ecut_convergence_criterion is None:
            raise DevError("Need to set '_gs_ecut_convergence_criterion'.")
        if self._gs_ecut_convergence_input_vars is None:
            raise DevError("Need to set '_gs_ecut_convergence_input_vars'.")
        if self._gs_ecut_convergence_ecuts is None:
            raise DevError("Need to set '_gs_ecut_convergence_ecuts'.")
        if self._gs_ecut_convergence_example is None:
            raise DevError("Need to set '_gs_ecut_convergence_example'.")
        if self._gs_ecut_convergence_other_kwargs is None:
            self._gs_ecut_convergence_other_kwargs = {}
        # GS KGRID CONVERGENCE PART
        if self._gs_kgrid_convergence_criterion is None:
            raise DevError("Need to set '_gs_kgrid_convergence_criterion'.")
        if self._gs_kgrid_convergence_input_vars is None:
            raise DevError("Need to set '_gs_kgrid_convergence_input_vars'.")
        if self._gs_kgrid_convergence_kgrids is None:
            raise DevError("Need to set '_gs_kgrid_convergence_kgrids'.")
        if self._gs_kgrid_convergence_example is None:
            raise DevError("Need to set '_gs_kgrid_convergence_example'.")
        if self._gs_kgrid_convergence_other_kwargs is None:
            self._gs_kgrid_convergence_other_kwargs = {}
        # GS SMEARING CONVERGENCE PART
        if self._gs_smearing_convergence_criterion is None:
            raise DevError("Need to set '_gs_smearing_convergence_criterion'.")
        if self._gs_smearing_convergence_input_vars is None:
            raise DevError(
                    "Need to set '_gs_smearing_convergence_input_vars'.")
        if self._gs_smearing_convergence_kgrids is None:
            raise DevError("Need to set '_gs_smearing_convergence_kgrids'.")
        if self._gs_smearing_convergence_example is None:
            raise DevError("Need to set '_gs_smearing_convergence_example'.")
        if self._gs_smearing_convergence_smearings is None:
            raise DevError("Need to set '_gs_smearing_convergence_smearings'.")
        if self._gs_smearing_convergence_other_kwargs is None:
            self._gs_smearing_convergence_other_kwargs = {}
        # RELAXATION PART
        if self._relaxation_other_kwargs is None:
            self._relaxation_other_kwargs = {}
        if self._relaxation_input_vars is None:
            raise DevError("Need to set '_relaxation_input_vars'.")
        # PHONON ECUT CONVERGENCE PART
        if self._phonon_ecut_convergence_criterion is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_criterion'.")
        if self._phonon_ecut_convergence_scf_input_vars is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_scf_input_vars'.")
        if self._phonon_ecut_convergence_phonons_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_ecut_convergence_phonons_input_vars'.")
        if self._phonon_ecut_convergence_ecuts is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_ecuts'.")
        if self._phonon_ecut_convergence_example is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_example'.")
        if self._phonon_ecut_convergence_phonon_qpt is None:
            raise DevError(
                    "Need to set '_phonon_ecut_convergence_phonon_qpt'.")
        if self._phonon_ecut_convergence_other_kwargs is None:
            self._phonon_ecut_convergence_other_kwargs = {}
        # PHONON DISPERSION QGRID CONVERGENCE PART
        if self._phonon_dispersion_qgrid_convergence_qgrids is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_dispersion_qgrid_convergence_qgrids'.")
        if self._phonon_dispersion_qgrid_convergence_example is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_dispersion_qgrid_convergence_example'.")
        # PHONON DISPERSION PART
        if self._phonon_dispersion_example is None:
            raise DevError("Need to set ')phonon_dispersion_example'.")
        if self._phonon_dispersion_phonons_input_vars is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_phonons_input_vars'.")
        if self._phonon_dispersion_qgrid is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_qgrid'.")
        if self._phonon_dispersion_qpoint_path is None:
            raise DevError("Need to set '_phonon_dispersion_qpoint_path'.")
        if self._phonon_dispersion_qpoint_path_density is None:
            raise DevError(
                    "Need to set '_phonon_dispersion_qpoint_path_density'.")
        # PHONON KGRID CONVERGENCE PART
        if self._phonon_kgrid_convergence_scf_kgrids is None:
            raise DevError(
                    "Need to set '_phonon_kgrid_convergence_scf_kgrids'.")
        if self._phonon_kgrid_convergence_phonons_input_variables is None:
            raise DevError(
                    "Need to set '_phonon_kgrid_convergence_phonons_input_"
                    "variables'.")
        if self._phonon_kgrid_convergence_scf_input_variables is None:
            raise DevError(
                    "Need to set '_phonon_kgrid_convergence_scf_input_"
                    "variables'.")
        if self._phonon_kgrid_convergence_scf_examples is None:
            raise DevError(
                    "Need to set '_phonon_kgrid_convergence_scf_examples'.")
        if self._phonon_kgrid_convergence_phonons_examples is None:
            raise DevError(
                    "Need to set '_phonon_kgrid_convergence_phonons_examples'."
                    )
        # PHONON SMEARING CONVERGENCE PART
        if self._phonon_smearing_convergence_criterion is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_criterion'.")
        if self._phonon_smearing_convergence_scf_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_smearing_convergence_scf_input_vars'.")
        if self._phonon_smearing_convergence_phonons_input_vars is None:
            raise DevError(
                    "Need to set "
                    "'_phonon_smearing_convergence_phonons_input_vars'.")
        if self._phonon_smearing_convergence_smearings is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_smearings'.")
        if self._phonon_smearing_convergence_kgrids is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_kgrids'.")
        if self._phonon_smearing_convergence_example is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_example'.")
        if self._phonon_smearing_convergence_phonon_qpt is None:
            raise DevError(
                    "Need to set '_phonon_smearing_convergence_phonon_qpt'.")
        if self._phonon_smearing_convergence_other_kwargs is None:
            self._phonon_smearing_convergence_other_kwargs = {}
        # BAND STRUCTURE PART
        if self._band_structure_example is None:
            raise DevError("Need to set '_band_structure_example'.")
        if self._band_structure_kpoint_path is None:
            raise DevError("Need to set '_band_structure_kpoint_path'.")
        if self._band_structure_kpoint_path_density is None:
            raise DevError(
                    "Need to set '_band_structure_kpoint_path_density'.")
        # FERMI SURFACE PART
        if self._fermi_surface_nscf_example is None:
            raise DevError("Need to set '_fermi_surface_nscf_example'.")
        if self._fermi_surface_nscf_input_variables is None:
            raise DevError(
                    "Need to set '_fermi_surface_nscf_input_variables'.")
        if self._fermi_surface_nscf_kgrid is None:
            raise DevError(
                    "Need to set '_fermi_surface_nscf_kgrid'.")
        # DOS PART
        if self._dos_example is None:
            raise DevError("Need to set '_dos_example'.")
        if self._dos_input_variables is None:
            raise DevError(
                    "Need to set '_dos_input_variables'.")
        if self._dos_fine_kpoint_grid_variables is None:
            raise DevError(
                    "Need to set '_dos_fine_kpoint_grid_variables'.")

    @pytest.fixture
    def setup_workflow_tempdir(self):
        """Setup fixture for the workflow temp dir."""
        self.workflow_dir = tempfile.TemporaryDirectory()
        yield
        self.workflow_dir.cleanup()

    @pytest.fixture
    async def setup_dos_script(self):
        """Setup fixture for the scf script."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._dos_script_name) as temp:
            self.dos_command_file = temp
            yield

    @pytest.fixture
    async def setup_scf_script(self):
        """Setup fixture for the scf script."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._scf_script_name) as temp:
            self.scf_command_file = temp
            yield

    @pytest.fixture
    async def setup_phonons_script(self):
        """Setup fixture for the phonons script."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._phonons_script_name) as temp:
            self.phonons_command_file = temp
            yield

    async def test_band_structure(self):
        """Unitest for the band structure part of the workflow."""
        workflow = self._workflow_cls(
                gs=True,
                band_structure=True,
                **self._workflow_init_kwargs)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_band_structure(workflow)
        await workflow.write()
        assert await aiofiles.os.path.isdir(
                    workflow.band_structure_sequencer.band_structure_workdir)
        await aioshutil.rmtree(
                workflow.band_structure_sequencer.band_structure_workdir)
        await self._set_band_structure_done(workflow)
        await workflow.run()
        # make sure band structure plots were written
        res_dir = os.path.join(
                os.path.dirname(
                    workflow.band_structure_sequencer.band_structure_workdir),
                "results")
        # many files created since this is a fatband run
        assert len(await aiofiles.os.listdir(res_dir)) >= 2

    async def test_dos(self) -> None:
        """Tests the dos part of the workflow."""
        workflow = self._workflow_cls(
                dos=True,
                gs=True,
                band_structure=True,
                **self._workflow_init_kwargs)
        await self._set_dos(workflow)
        await workflow.write()
        # make sure dos calc was written
        assert await is_calculation_directory(
                    workflow.dos_sequencer.dos_workdir)
        # set done and make sure plots were written
        await aioshutil.rmtree(workflow.dos_sequencer.dos_workdir)
        await self._set_dos_done(workflow)
        await workflow.run()
        results_dir = os.path.join(
                os.path.dirname(workflow.dos_sequencer.dos_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 4

    async def test_fermi_surface(self, *args, **kwargs):
        """Test the fermi surface part of the workflow."""
        workflow = self._workflow_cls(
                gs=True, fermi_surface=True,
                **self._workflow_init_kwargs)
        await self._set_gs(workflow, *args, **kwargs)
        await self._set_gs_done(workflow, *args, **kwargs)
        await self._set_fermi_surface(workflow)
        await workflow.write()
        # check nscf calc has been written
        nscf = workflow.fermi_surface_sequencer.nscf_workdir
        assert await is_calculation_directory(nscf)
        await aioshutil.rmtree(nscf)
        await self._set_fermi_surface_done(workflow)
        await workflow.run()
        assert len(await aiofiles.os.listdir(
                    os.path.join(os.path.dirname(nscf), "results"))) >= 4

    async def test_gs_ecut_convergence(self):
        """Test the gs_ecut_convergence part of the workflow."""
        workflow = self._workflow_cls(
                gs_ecut_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_gs_ecut_convergence(workflow)
        await workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._gs_ecut_convergence_ecuts)
        # find the varname
        seq = workflow.gs_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_ecut_convergence", "scf_runs", varname)
        assert len(await aiofiles.os.listdir(dir_)) == ncalcs
        # set done and check workflow completed
        await aioshutil.rmtree(dir_)
        await self._set_gs_ecut_convergence_done(workflow, varname)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(dir_)), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2
        assert await workflow.workflow_completed

    async def test_gs_kgrid_convergence(self):
        """Test the gs_kgrid_convergence part of the workflow."""
        workflow = self._workflow_cls(
                gs_kgrid_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_gs_kgrid_convergence(workflow)
        await workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._gs_kgrid_convergence_kgrids)
        seq = workflow.gs_kgrid_convergence_sequencer
        varname = seq._parameter_to_converge_input_variable_name
        dirs = [x for x in await aiofiles.os.listdir(
            os.path.join(seq.scf_workdir, varname)) if "results" not in x]
        assert len(dirs) == ncalcs
        await aioshutil.rmtree(
                workflow.gs_kgrid_convergence_sequencer.scf_workdir)
        await self._set_gs_kgrid_convergence_done(workflow)
        await workflow.run()
        results_dir = os.path.join(
                os.path.dirname(
                    workflow.gs_kgrid_convergence_sequencer.scf_workdir),
                "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await workflow.workflow_completed

    async def test_gs_smearing_convergence(self):
        """Test for the gs_smearing_convergence part of the workflow."""
        workflow = self._workflow_cls(
                gs_smearing_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_gs_smearing_convergence(workflow)
        await workflow.write()
        ncalcs = (len(self._gs_smearing_convergence_kgrids) *
                  len(self._gs_smearing_convergence_smearings))
        dirs = await aiofiles.os.listdir(
                workflow.gs_smearing_convergence_sequencer.scf_workdir)
        nwritten = 0
        for dir_ in dirs:
            nwritten += len(await aiofiles.os.listdir(
                os.path.join(
                    workflow.gs_smearing_convergence_sequencer.scf_workdir,
                    dir_)))
        assert ncalcs == nwritten
        await aioshutil.rmtree(
                workflow.gs_smearing_convergence_sequencer.scf_workdir)
        await self._set_gs_smearing_convergence_done(workflow)
        await workflow.run()
        results_dir = os.path.join(
                workflow.gs_smearing_convergence_sequencer.scf_workdir,
                "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 6
        assert await workflow.workflow_completed

    async def test_gs(self):
        """Test the gs part of the workflow."""
        workflow = self._workflow_cls(gs=True, **self._workflow_init_kwargs)
        await self._set_gs(workflow)
        await workflow.write()
        # make sure gs run is written
        assert await aiofiles.os.path.isdir(workflow.gs_sequencer.scf_workdir)
        # check that workflow is not completed
        assert not await workflow.workflow_completed
        # delete what was writte and copy example calculation
        await aioshutil.rmtree(workflow.gs_sequencer.scf_workdir)
        await self._set_gs_done(workflow)
        # check that the workflow is now completed
        await workflow.gs_sequencer.clear_sequence()
        assert await workflow.workflow_completed

    async def test_phonon_ecut_convergence(self):
        """Test the phonon_ecut_convergence part of the workflow."""
        workflow = self._workflow_cls(
                phonon_ecut_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_phonon_ecut_convergence(workflow)
        await workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._phonon_ecut_convergence_ecuts)
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_ecut_convergence")
        seq = workflow.phonon_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        scf_root = os.path.join(seq.scf_workdir, varname)
        assert len(await aiofiles.os.listdir(scf_root)) == ncalcs
        # set scf_runs done
        await aioshutil.rmtree(scf_root)
        await self._set_phonon_ecut_convergence_done(workflow, scf=True)
        await workflow.write()
        # check phonon calcs have been written
        ph_root = os.path.join(seq.phonons_workdir, varname)
        assert len(await aiofiles.os.listdir(ph_root)) == ncalcs
        await aioshutil.rmtree(ph_root)
        await self._set_phonon_ecut_convergence_done(
                workflow, phonons=True)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(dir_, "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await workflow.workflow_completed

    async def test_phonon_dispersion(self):
        """Test the phonon_dispersion part of the workflow."""
        workflow = self._workflow_cls(
                gs=True,
                phonon_dispersion=True,
                **self._workflow_init_kwargs)
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_phonon_dispersion(workflow)
        await workflow.write()
        # make sure the first phonon calc was written
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        assert len(await aiofiles.os.listdir(
                    os.path.join(dir_, "phonons_runs"))) == 1
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_done(
                workflow, first_phonon=True)
        await workflow.write()
        # check all phonon calcs have been written
        seq = workflow.phonon_dispersion_sequencer
        all_phs = await aiofiles.os.listdir(os.path.dirname(
            workflow.phonon_dispersion_sequencer.phonons_workdir))
        all_phs = [x for x in all_phs if "qgrid_gen" not in x]
        assert len(all_phs) == seq.nphonons
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_done(
                workflow, all_phonons=True)
        await workflow.write()
        # make sure post phonons calcs have been written
        await self._check_phonon_dispersion_post_phonons(workflow)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(dir_, "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await workflow.workflow_completed

    async def test_phonon_dispersion_qgrid_convergence(self):
        """Test the phonon_dispersion_qgrid_convergence part."""
        workflow = self._workflow_cls(
                gs=True,
                phonon_dispersion_qgrid_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_phonon_dispersion_qgrid_convergence(workflow)
        await workflow.write()
        # make sure the first phonon calc was written for each qgrids
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        assert len(await aiofiles.os.listdir(dir_)) == (
                len(self._phonon_dispersion_qgrid_convergence_qgrids)
                )
        for subdir in await aiofiles.os.listdir(dir_):
            assert len(await aiofiles.os.listdir(
                        os.path.join(dir_, subdir, "phonons_runs"))) == 1
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, first_phonon=True)
        await workflow.write()
        # check all phonon calcs have been written
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            assert len([x for x in await aiofiles.os.listdir(
                     os.path.dirname(seq.phonons_workdir))
                     if "generation" not in x]) == (
                seq.nphonons)
        await aioshutil.rmtree(dir_)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, all_phonons=True)
        await workflow.write()
        await self._check_phonon_dispersion_qgrid_convergence_post_phonons(
                workflow)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        # make sure there are >= 2 files in results dir
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence")
        results_dir = os.path.join(dir_, "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await workflow.workflow_completed

    async def test_phonon_kgrid_convergence(self, ddk=False):
        """Test the phonon_kgrid_convergence part of the workflow."""
        # NB: ddk is only used for abinit runs
        workflow = self._workflow_cls(
                phonon_kgrid_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_phonon_kgrid_convergence(workflow, ddk=ddk)
        await workflow.write()
        # make sure scf calcs were written
        ncalcs = len(self._phonon_kgrid_convergence_scf_kgrids)
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_kgrid_convergence")
        seq = workflow.phonon_kgrid_convergence_sequencer
        varname = (
                await aiofiles.os.listdir(os.path.join(dir_, "scf_runs")))[0]
        scf_root = os.path.join(seq.scf_workdir, varname)
        assert len(await aiofiles.os.listdir(scf_root)) == ncalcs
        # set scf_runs done
        await aioshutil.rmtree(scf_root)
        await self._set_phonon_kgrid_convergence_done(workflow, scf=True)
        await workflow.write()
        if ddk:
            # check DDK calcs have been written
            ddk_root = os.path.join(seq.ddk_workdir, varname)
            assert len(await aiofiles.os.listdir(ddk_root)), ncalcs
            # set ddk runs done
            await aioshutil.rmtree(ddk_root)
            await self._set_phonon_kgrid_convergence_done(workflow, ddk=ddk)
            await workflow.write()
        # check phonon calcs have been written
        ph_root = os.path.join(seq.phonons_workdir, varname)
        assert len(await aiofiles.os.listdir(ph_root)) == ncalcs
        await aioshutil.rmtree(ph_root)
        if ddk:
            await self._set_phonon_kgrid_convergence_done(
                    workflow, phonons_with_ddk=True)
        else:
            await self._set_phonon_kgrid_convergence_done(
                    workflow, phonons=True)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(dir_, "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2
        assert await workflow.workflow_completed

    async def test_phonon_smearing_convergence(self):
        """Test the phonon_smearing_convergence part of the workflow."""
        workflow = self._workflow_cls(
                phonon_smearing_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_phonon_smearing_convergence(workflow)
        await workflow.write()
        # make sure scf calcs were written
        ncalcs = (len(self._phonon_smearing_convergence_kgrids) *
                  len(self._phonon_smearing_convergence_smearings))
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence", "scf_runs")
        assert len(await aiofiles.os.listdir(dir_)) == (
                len(self._phonon_smearing_convergence_smearings))
        coro = []
        for subdir in await aiofiles.os.listdir(dir_):
            coro.append(aiofiles.os.listdir(os.path.join(dir_, subdir)))
        assert ncalcs == sum(len(x) for x in await asyncio.gather(*coro))
        # set scf_runs done
        await aioshutil.rmtree(dir_)
        await self._set_phonon_smearing_convergence_done(workflow, scf=True)
        await workflow.write()
        # check phonon calcs have been written
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence", "phonons_runs")
        assert len(await aiofiles.os.listdir(dir_)) == (
                len(self._phonon_smearing_convergence_smearings))
        dirlengths = await asyncio.gather(
                *(aiofiles.os.listdir(os.path.join(dir_, subdir))
                    for subdir in await aiofiles.os.listdir(dir_))
                )
        assert ncalcs == sum([len(x) for x in dirlengths])
        await aioshutil.rmtree(dir_)
        await self._set_phonon_smearing_convergence_done(
                workflow, phonons=True)
        await workflow.run()
        # make sure there are >= 2 files in results dir
        results_dir = os.path.join(
                self.workflow_dir.name, "phonon_smearing_convergence",
                "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2
        assert await workflow.workflow_completed

    async def test_relaxation(self):
        """Test the relaxation part of the workflow."""
        workflow = self._workflow_cls(
                relaxation=True,
                **self._workflow_init_kwargs)
        await self._set_relaxation(workflow)
        await workflow.write()
        # check that first calc has been written
        assert len(
                await aiofiles.os.listdir(
                    workflow.relaxation_sequencer.scf_workdir)) == 1
        await aioshutil.rmtree(workflow.relaxation_sequencer.scf_workdir)
        await self._set_relaxation_done(workflow)
        await workflow.run()
        assert await workflow.workflow_completed

    @abc.abstractmethod
    async def _check_phonon_dispersion_post_phonons(self):
        pass

    @abc.abstractmethod
    async def _check_phonon_dispersion_qgrid_convergence_post_phonons(self):
        pass

    async def _set_band_structure(self, workflow, **kwargs):
        calculation_parameters = {
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                }
        await workflow.set_band_structure(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "band_structure"),
                band_structure_input_variables=(
                    self._band_structure_input_variables),
                band_structure_calculation_parameters=calculation_parameters,
                band_structure_kpoint_path=self._band_structure_kpoint_path,
                band_structure_kpoint_path_density=(
                    self._band_structure_kpoint_path_density),
                plot_calculation_parameters={"show": False},
                use_gs=True,
                **kwargs
                )

    async def _set_band_structure_done(self, workflow, **kwargs):
        await copy_calculation(
                self._band_structure_example,
                workflow.band_structure_sequencer.band_structure_workdir,
                new_parents=[workflow.band_structure_sequencer.scf_workdir],
                **kwargs)

    async def _set_dos(
           self, workflow: BaseWorkflow,
           dos_calculation_parameters: Mapping[str, Any] = None,
           **kwargs) -> None:
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        if dos_calculation_parameters is None:
            dos_calculation_parameters = {
                "command": self.dos_command_file.name,
                "queuing_system": "local",
                }
        await workflow.set_dos(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "dos"),
                use_gs=True,
                dos_fine_kpoint_grid_variables=(
                    self._dos_fine_kpoint_grid_variables),
                dos_input_variables=self._dos_input_variables,
                dos_calculation_parameters=dos_calculation_parameters,
                plot_calculation_parameters={"show": False},
                **kwargs,
                )

    async def _set_dos_done(self, workflow, **kwargs):
        await copy_calculation(
                self._dos_example,
                workflow.dos_sequencer.dos_workdir,
                **kwargs,
                )

    async def _set_fermi_surface(self, workflow):
        await workflow.set_fermi_surface(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "fermi_surface"),
                use_gs=True,
                fermi_surface_k_z=[0.0],
                nscf_kgrid=self._fermi_surface_nscf_kgrid,
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name},
                plot_calculation_parameters={"show": False},
                nscf_input_variables=self._fermi_surface_nscf_input_variables,
                )

    async def _set_fermi_surface_done(self, workflow, *args, **kwargs):
        await copy_calculation(
                self._fermi_surface_nscf_example,
                workflow.fermi_surface_sequencer.nscf_workdir,
                *args,
                new_parents=[workflow.gs_sequencer.scf_workdir],
                **kwargs,
                )

    async def _set_gs_ecut_convergence(self, workflow):
        await workflow.set_gs_ecut_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_ecut_convergence"),
            scf_input_variables=self._gs_ecut_convergence_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_ecuts=self._gs_ecut_convergence_ecuts,
            scf_convergence_criterion=self._gs_ecut_convergence_criterion,
            plot_calculation_parameters={"show": False},
            **self._gs_ecut_convergence_other_kwargs)

    async def _set_gs_ecut_convergence_done(self, workflow, varname, **kwargs):
        seq = workflow.gs_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        coro = []
        for calcdir in await aiofiles.os.listdir(
                os.path.join(
                    self._gs_ecut_convergence_example, varname)):
            coro.append(copy_calculation(
                    os.path.join(
                        self._gs_ecut_convergence_example, varname, calcdir),
                    os.path.join(
                        seq.scf_workdir, varname, calcdir), **kwargs))
        await asyncio.gather(*coro)

    async def _set_gs_kgrid_convergence(self, workflow):
        await workflow.set_gs_kgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_kgrid_convergence"),
            scf_input_variables=self._gs_kgrid_convergence_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_kgrids=self._gs_kgrid_convergence_kgrids,
            scf_convergence_criterion=self._gs_kgrid_convergence_criterion,
            plot_calculation_parameters={"show": False},
            **self._gs_kgrid_convergence_other_kwargs)

    async def _set_gs_kgrid_convergence_done(self, workflow, **kwargs):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_kgrid_convergence", "scf_runs")
        seq = workflow.gs_kgrid_convergence_sequencer
        varname = seq.kgrids_input_variable_name
        coro = []
        for calcdir in await aiofiles.os.listdir(
                os.path.join(
                    self._gs_kgrid_convergence_example, "scf_runs",
                    varname)):
            if "results" in calcdir:
                continue
            coro.append(copy_calculation(
                    os.path.join(
                        self._gs_kgrid_convergence_example, "scf_runs",
                        varname, calcdir),
                    os.path.join(dir_, varname, calcdir), **kwargs))
        await asyncio.gather(*coro)

    async def _set_gs_smearing_convergence(self, workflow):
        await workflow.set_gs_smearing_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gs_smearing_convergence"),
                scf_input_variables=self._gs_smearing_convergence_input_vars,
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                scf_kgrids=self._gs_smearing_convergence_kgrids,
                scf_smearings=self._gs_smearing_convergence_smearings,
                scf_convergence_criterion=(
                    self._gs_smearing_convergence_criterion),
                plot_calculation_parameters={"show": False},
                **self._gs_smearing_convergence_other_kwargs,
                )

    async def _set_gs_smearing_convergence_done(self, workflow, **kwargs):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "gs_smearing_convergence")
        coro = []
        for subdir in await aiofiles.os.listdir(
                self._gs_smearing_convergence_example):
            if "results" in subdir:
                continue
            for calcdir in await aiofiles.os.listdir(
                    os.path.join(self._gs_smearing_convergence_example,
                                 subdir)):
                coro.append(copy_calculation(
                    os.path.join(
                        self._gs_smearing_convergence_example,
                        subdir, calcdir),
                    os.path.join(dir_, subdir, calcdir),
                    **kwargs))
        await asyncio.gather(*coro)

    async def _set_gs(self, workflow):
        calculation_parameters = {"command": self.scf_command_file.name,
                                  "queuing_system": "local"}
        calculation_parameters.update(self._set_gs_calculation_kwargs)
        await workflow.set_gs(
                root_workdir=os.path.join(self.workflow_dir.name, "gs_run"),
                scf_input_variables=self._gs_input_vars.copy(),
                scf_calculation_parameters=calculation_parameters)

    async def _set_gs_done(self, workflow, **kwargs):
        await copy_calculation(
                self._gs_example, workflow.gs_sequencer.scf_workdir,
                **kwargs)

    async def _set_phonon_ecut_convergence(self, workflow, **kwargs):
        await workflow.set_phonon_ecut_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_ecut_convergence"),
            scf_input_variables=self._phonon_ecut_convergence_scf_input_vars,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_ecuts=self._phonon_ecut_convergence_ecuts,
            phonons_qpt=self._phonon_ecut_convergence_phonon_qpt,
            phonons_input_variables=(
                self._phonon_ecut_convergence_phonons_input_vars),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=(
                self._phonon_ecut_convergence_criterion),
            plot_calculation_parameters={"show": False},
            **self._phonon_ecut_convergence_other_kwargs)

    async def _set_phonon_ecut_convergence_done(
            self, workflow, scf=False, phonons=False,
            _copy_phonons_extra_kwargs=None,
            _copy_scf_extra_kwargs=None):
        if _copy_phonons_extra_kwargs is None:
            _copy_phonons_extra_kwargs = {}
        if _copy_scf_extra_kwargs is None:
            _copy_scf_extra_kwargs = {}
        seq = workflow.phonon_ecut_convergence_sequencer
        varname = seq.ecuts_input_variable_name
        coro = []
        if scf:
            root = os.path.join(
                    self._phonon_ecut_convergence_example,
                    "scf_runs", varname)
            for calcdir in await aiofiles.os.listdir(root):
                coro.append(copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(seq.scf_workdir, varname, calcdir),
                    **_copy_scf_extra_kwargs
                    ))
        if phonons:
            root = os.path.join(
                    self._phonon_ecut_convergence_example,
                    "phonons_runs", varname)
            for calcdir in await aiofiles.os.listdir(root):
                coro.append(copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(seq.phonons_workdir, varname, calcdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, varname, calcdir)],
                    **_copy_phonons_extra_kwargs
                    ))
        await asyncio.gather(*coro)

    async def _set_phonon_dispersion(self, workflow, **kwargs):
        await workflow.set_phonon_dispersion(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion"),
            phonons_input_variables=self._phonon_dispersion_phonons_input_vars,
            phonons_qpoint_grid=self._phonon_dispersion_qgrid,
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            plot_calculation_parameters={"show": False},
            qpoint_path=self._phonon_dispersion_qpoint_path,
            qpoint_path_density=self._phonon_dispersion_qpoint_path_density,
            use_gs=True,
            **kwargs
            )

    async def _set_phonon_dispersion_done(
            self, workflow, all_phonons=False, **kwargs):
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if all_phonons:
            coro = []
            for calcdir in await aiofiles.os.listdir(
                    os.path.join(root, "phonons_runs")):
                coro.append(copy_calculation(
                    os.path.join(root, "phonons_runs", calcdir),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir), calcdir),
                    new_parents=[seq.scf_workdir],
                    **kwargs))
            await asyncio.gather(*coro)

    async def _set_phonon_dispersion_qgrid_convergence(
            self, workflow, **kwargs):
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await workflow.set_phonon_dispersion_qgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion_qgrid_convergence"),
            phonons_input_variables=(
                self._phonon_dispersion_phonons_input_vars
                ),
            phonons_qpoint_grids=(
                self._phonon_dispersion_qgrid_convergence_qgrids),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            plot_calculation_parameters={"show": False},
            qpoint_path=self._phonon_dispersion_qpoint_path,
            qpoint_path_density=(
                self._phonon_dispersion_qpoint_path_density),
            use_gs=True,
            **kwargs)

    async def _set_phonon_dispersion_qgrid_convergence_done(
            self, workflow, all_phonons=False, **kwargs):
        if not all_phonons:
            return
        coro = []
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            root = os.path.join(
                    self._phonon_dispersion_qgrid_convergence_example,
                    os.path.basename(os.path.dirname(
                        os.path.dirname(seq.phonons_workdir))))
            for calcdir in await aiofiles.os.listdir(
                    os.path.join(root, "phonons_runs")):
                coro.append(copy_calculation(
                    os.path.join(root, "phonons_runs", calcdir),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir), calcdir),
                    new_parents=[seq.scf_workdir], **kwargs
                    ))
        await asyncio.gather(*coro)

    async def _set_phonon_kgrid_convergence(self, workflow, **kwargs):
        await workflow.set_phonon_kgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_kgrid_convergence"),
            scf_input_variables=(
                self._phonon_kgrid_convergence_scf_input_variables.copy()),
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_kgrids=self._phonon_kgrid_convergence_scf_kgrids,
            phonons_qpt=[0.0, 0.0, 0.0],
            phonons_input_variables=(
                self._phonon_kgrid_convergence_phonons_input_variables.copy()),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=10,
            plot_calculation_parameters={"show": False},
            )

    async def _set_phonon_kgrid_convergence_done(
            self, workflow, scf=False,
            phonons=False, copy_scf_extra_kwargs=None,
            **kwargs,
            ):
        coro = []
        if copy_scf_extra_kwargs is None:
            copy_scf_extra_kwargs = {}
        seq = workflow.phonon_kgrid_convergence_sequencer
        varname = os.path.basename(
                os.path.dirname(
                    self._phonon_kgrid_convergence_scf_examples[0]))
        if scf:
            for calcdir in self._phonon_kgrid_convergence_scf_examples:
                coro.append(copy_calculation(
                    calcdir,
                    os.path.join(
                        seq.scf_workdir, varname, os.path.basename(calcdir)),
                    **kwargs, **copy_scf_extra_kwargs,
                    ))
        if phonons:
            for calc in self._phonon_kgrid_convergence_phonons_examples:
                calcdir = os.path.basename(calc)
                coro.append(copy_calculation(
                    calc,
                    os.path.join(seq.phonons_workdir, varname, calcdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, varname, calcdir)],
                    **kwargs,
                    ))
        await asyncio.gather(*coro)

    async def _set_phonon_smearing_convergence(self, workflow):
        await workflow.set_phonon_smearing_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_smearing_convergence"),
            scf_input_variables=(
                self._phonon_smearing_convergence_scf_input_vars),
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_smearings=self._phonon_smearing_convergence_smearings,
            scf_kgrids=self._phonon_smearing_convergence_kgrids,
            phonons_input_variables=(
                self._phonon_smearing_convergence_phonons_input_vars),
            phonons_qpt=self._phonon_smearing_convergence_phonon_qpt,
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=(
                self._phonon_smearing_convergence_criterion),
            plot_calculation_parameters={"show": False},
            **self._phonon_smearing_convergence_other_kwargs)

    async def _set_phonon_smearing_convergence_done(
            self, workflow, scf=False, phonons=False,
            copy_phonons_extra_kwargs=None,
            copy_scf_extra_kwargs=None):
        if copy_phonons_extra_kwargs is None:
            copy_phonons_extra_kwargs = {}
        if copy_scf_extra_kwargs is None:
            copy_scf_extra_kwargs = {}
        seq = workflow.phonon_smearing_convergence_sequencer
        coro = []
        if scf:
            root = os.path.join(
                    self._phonon_smearing_convergence_example,
                    "scf_runs")
            for subdir in await aiofiles.os.listdir(root):
                for calcdir in await aiofiles.os.listdir(
                        os.path.join(root, subdir)):
                    coro.append(copy_calculation(
                        os.path.join(root, subdir, calcdir),
                        os.path.join(seq.scf_workdir, subdir, calcdir),
                        **copy_scf_extra_kwargs))
        if phonons:
            root = os.path.join(
                    self._phonon_smearing_convergence_example,
                    "phonons_runs")
            for subdir in await aiofiles.os.listdir(root):
                for calcdir in await aiofiles.os.listdir(
                        os.path.join(root, subdir)):
                    coro.append(copy_calculation(
                        os.path.join(root, subdir, calcdir),
                        os.path.join(seq.phonons_workdir, subdir, calcdir),
                        new_parents=[
                            os.path.join(seq.scf_workdir, subdir, calcdir)],
                        **copy_phonons_extra_kwargs
                        ))
        await asyncio.gather(*coro)

    async def _set_relaxation(
            self, workflow, relax_cell=True, relax_atoms=True):
        await workflow.set_relaxation(
            root_workdir=os.path.join(
                    self.workflow_dir.name, "relax_run"),
            scf_input_variables=self._relaxation_input_vars,
            relax_atoms=relax_atoms,
            relax_cell=relax_cell,
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            **self._relaxation_other_kwargs,
            )

    @abc.abstractmethod
    async def _set_relaxation_done(self, *args, **kwargs):
        pass
