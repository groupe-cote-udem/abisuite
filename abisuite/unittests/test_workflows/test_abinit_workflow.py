import asyncio
import os

import aiofiles.os

import aioshutil

import pytest


from .bases import BaseWorkflowTest, __TEST_WORKFLOW_ROOT__
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        abinit_band_structure_example,
        abinit_band_structure_input_variables,
        abinit_band_structure_kpoint_path,
        abinit_band_structure_kpoint_path_density,
        abinit_bse_ecuteps_convergence_example,
        abinit_bse_example,
        abinit_bse_kgrid_convergence_example,
        abinit_bse_kgrid_convergence_input_variable_name,
        abinit_bse_nband_convergence_example,
        abinit_dos_example,
        abinit_dos_fine_kpoint_grid_variables,
        abinit_dos_input_variables,
        abinit_fermi_surface_nscf_example,
        abinit_fermi_surface_nscf_input_variables,
        abinit_fermi_surface_nscf_kgrid,
        abinit_gs_ecut_convergence_criterion,
        abinit_gs_ecut_convergence_ecuts,
        abinit_gs_ecut_convergence_example,
        abinit_gs_ecut_convergence_input_variables,
        abinit_gs_ecut_convergence_pawecuts,
        abinit_gs_example,
        abinit_gs_input_variables,
        abinit_gs_kgrid_convergence_criterion,
        abinit_gs_kgrid_convergence_example,
        abinit_gs_kgrid_convergence_scf_input_variables,
        abinit_gs_kgrid_convergence_scf_kgrids,
        abinit_gs_kgrid_convergence_scf_kgrids_varname,
        abinit_gs_pawecut_convergence_criterion,
        abinit_gs_pawecut_convergence_example,
        abinit_gs_pseudos,
        abinit_gs_smearing_convergence_criterion,
        abinit_gs_smearing_convergence_example,
        abinit_gs_smearing_convergence_kgrids,
        abinit_gs_smearing_convergence_pseudos,
        abinit_gs_smearing_convergence_scf_input_variables,
        abinit_gs_smearing_convergence_smearings,
        abinit_gs_smearing_convergence_varname,
        abinit_gw_ecuteps_convergence_example,
        abinit_gw_example,
        abinit_gw_kgrid_convergence_example,
        abinit_gw_screening_nband_convergence_example,
        abinit_gw_self_energy_nband_convergence_example,
        abinit_optic_nscf_convergence_ddk_input_variables,
        abinit_optic_nscf_convergence_example,
        abinit_optic_nscf_convergence_experimental_bandgap,
        abinit_optic_nscf_convergence_nscf_input_variables,
        abinit_optic_nscf_convergence_nscf_kpoint_grids,
        abinit_optic_nscf_convergence_nscf_kpoint_grids_input_varname,
        abinit_optic_nscf_convergence_optic_input_variables,
        abinit_optic_nscf_convergence_pseudos,
        abinit_phonon_dispersion_anaddb_input_variables,
        abinit_phonon_dispersion_compute_electric_field_response,
        abinit_phonon_dispersion_ddk_input_variables,
        abinit_phonon_dispersion_example,
        abinit_phonon_dispersion_phonons_input_variables,
        abinit_phonon_dispersion_pseudos,
        abinit_phonon_dispersion_qgrid_generation_input_variable_name,
        abinit_phonon_dispersion_qpoint_grid,
        abinit_phonon_dispersion_qpoint_path,
        abinit_phonon_dispersion_qpoint_path_density,
        abinit_phonon_dispersion_qpoints_split_by_irreps,
        abinit_phonon_ecut_convergence_criterion,
        abinit_phonon_ecut_convergence_ecuts,
        abinit_phonon_ecut_convergence_example,
        abinit_phonon_ecut_convergence_phonons_input_variables,
        abinit_phonon_ecut_convergence_pseudos,
        abinit_phonon_ecut_convergence_qpt,
        abinit_phonon_ecut_convergence_scf_input_variables,
        abinit_phonon_kgrid_convergence_ddk_input_variables,
        abinit_phonon_kgrid_convergence_example,
        abinit_phonon_kgrid_convergence_kgrid_input_variable_name,
        abinit_phonon_kgrid_convergence_phonons_examples,
        abinit_phonon_kgrid_convergence_phonons_input_variables,
        abinit_phonon_kgrid_convergence_phonons_qpt,
        abinit_phonon_kgrid_convergence_phonons_qpt_no_ddk,
        abinit_phonon_kgrid_convergence_pseudos,
        abinit_phonon_kgrid_convergence_scf_examples,
        abinit_phonon_kgrid_convergence_scf_input_variables,
        abinit_phonon_kgrid_convergence_scf_kgrids,
        abinit_relaxation_example,
        abinit_relaxation_scf_input_variables,
        )
from ...constants import EV_TO_HA
from ...exceptions import DevError
from ...handlers import is_calculation_directory
from ...workflows import AbinitWorkflow


# ############################  GS ############################################
ABINIT_Al_GS_PSEUDOS = [
        os.path.join(
            __TEST_WORKFLOW_ROOT__, "test_files", "pseudos", "Al.psp8"),
        ]
ABINIT_Al_GS_INPUT_VARIABLES = {
        "occopt": 4, "tsmear": 0.001, "ecut": 30, "ngkpt": [2, 2, 2],
        "nshiftk": 4, "shiftk": [
            [0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10, "toldfe": 1e-06,
        "acell": [5.326968652, 5.326968652, 5.326968652],
        "natom": 1, "ntypat": 1, "rprim": [
            [0.0, 0.70710678119, 0.70710678119],
            [0.70710678119, 0.0, 0.70710678119],
            [0.70710678119, 0.70710678119, 0.0]],
        "typat": 1, "xred": [0, 0, 0], "znucl": [13.0],
        "pseudos": ABINIT_Al_GS_PSEUDOS,
        "pp_dirpath": "",
        }
ABINIT_Al_GS_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al", "gs_run")


# #################### PHONON DISPERSION QGRID CONVERGENCE ####################
ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
        "phonon_dispersion_qgrid_convergence")
ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_QGRIDS = [[x] * 3 for x in [2, 6]]


# ################### PHONON SMEARING CONVERGENCE #############################
ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS = {
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0, 0.5, 0], [0, 0, 0.5]],
            "nstep": 10,
            "toldfe": 1e-6,
            "occopt": 4,
            "acell": [5.326968652] * 3,
            "natom": 1,
            "ntypat": 1,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "typat": 1,
            "ecut": 30,
            "xred": [0, 0, 0],
            "znucl": [13.0],
            "pseudos": abinit_phonon_ecut_convergence_pseudos,
            "pp_dirpath": "",
            }
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25}
ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE = os.path.join(
        __TEST_WORKFLOW_ROOT__, "test_files", "abinit_Al",
        "phonon_smearing_convergence")
ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS = [0.1, 0.01, 0.001]
ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS = [[x, x, x] for x in [2, 4, 8]]
ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS = {
        "scf_kgrids_input_variable_name": "ngkpt"}
ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION = 10
ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT = [0.0, 0.0, 0.0]


@pytest.mark.order("first")
class TestAbinitWorkflow(BaseWorkflowTest):
    """Test case for AbinitWorkflow class.
    """
    _band_structure_example = abinit_band_structure_example
    _band_structure_input_variables = abinit_band_structure_input_variables
    _band_structure_kpoint_path = abinit_band_structure_kpoint_path
    _band_structure_kpoint_path_density = (
            abinit_band_structure_kpoint_path_density)
    _dos_example = abinit_dos_example
    _dos_input_variables = abinit_dos_input_variables
    _dos_fine_kpoint_grid_variables = abinit_dos_fine_kpoint_grid_variables
    _dos_script_name = "abinit"
    _fermi_surface_nscf_input_variables = (
            abinit_fermi_surface_nscf_input_variables)
    _fermi_surface_nscf_kgrid = abinit_fermi_surface_nscf_kgrid
    _fermi_surface_nscf_example = abinit_fermi_surface_nscf_example
    _gs_ecut_convergence_criterion = abinit_gs_ecut_convergence_criterion
    _gs_ecut_convergence_input_vars = (
            abinit_gs_ecut_convergence_input_variables)
    _gs_ecut_convergence_ecuts = abinit_gs_ecut_convergence_ecuts
    _gs_ecut_convergence_example = abinit_gs_ecut_convergence_example
    _gs_ecut_convergence_other_kwargs = {
            "scf_pawecutdg": abinit_gs_ecut_convergence_pawecuts,
            "scf_pawecutdg_convergence_criterion": (
                abinit_gs_pawecut_convergence_criterion),
            }
    _gs_kgrid_convergence_criterion = (
            abinit_gs_kgrid_convergence_criterion)
    _gs_kgrid_convergence_example = (
            abinit_gs_kgrid_convergence_example)
    _gs_kgrid_convergence_input_vars = (
            abinit_gs_kgrid_convergence_scf_input_variables)
    _gs_kgrid_convergence_kgrids = (
            abinit_gs_kgrid_convergence_scf_kgrids)
    _gs_kgrid_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                abinit_gs_kgrid_convergence_scf_kgrids_varname),
            }
    _gs_smearing_convergence_criterion = (
            abinit_gs_smearing_convergence_criterion)
    _gs_smearing_convergence_example = abinit_gs_smearing_convergence_example
    _gs_smearing_convergence_input_vars = (
            abinit_gs_smearing_convergence_scf_input_variables.copy())
    _gs_smearing_convergence_kgrids = abinit_gs_smearing_convergence_kgrids
    _gs_smearing_convergence_smearings = (
            abinit_gs_smearing_convergence_smearings
            )
    _gs_smearing_convergence_other_kwargs = {
            "scf_kgrids_input_variable_name": (
                abinit_gs_smearing_convergence_varname
                )
            }
    _gs_example = abinit_gs_example
    _gs_input_vars = abinit_gs_input_variables
    _phonon_ecut_convergence_criterion = (
            abinit_phonon_ecut_convergence_criterion)
    _phonon_ecut_convergence_scf_input_vars = (
            abinit_phonon_ecut_convergence_scf_input_variables)
    _phonon_ecut_convergence_phonons_input_vars = (
            abinit_phonon_ecut_convergence_phonons_input_variables)
    _phonon_ecut_convergence_ecuts = (
            abinit_phonon_ecut_convergence_ecuts)
    _phonon_ecut_convergence_example = (
            abinit_phonon_ecut_convergence_example)
    _phonon_ecut_convergence_phonon_qpt = (
            abinit_phonon_ecut_convergence_qpt)
    _phonon_ecut_convergence_other_kwargs = (
            {"compute_electric_field_response": False})
    _phonon_dispersion_example = abinit_phonon_dispersion_example
    _phonon_dispersion_phonons_input_vars = (
            abinit_phonon_dispersion_phonons_input_variables)
    _phonon_dispersion_qgrid = abinit_phonon_dispersion_qpoint_grid
    _phonon_dispersion_qpoint_path = abinit_phonon_dispersion_qpoint_path
    _phonon_dispersion_qpoint_path_density = (
            abinit_phonon_dispersion_qpoint_path_density)
    _phonon_dispersion_qgrid_convergence_example = (
            ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_EXAMPLE)
    _phonon_dispersion_qgrid_convergence_qgrids = (
            ABINIT_PHONON_DISPERSION_QGRID_CONVERGENCE_QGRIDS)
    _phonon_kgrid_convergence_phonons_examples = (
            abinit_phonon_kgrid_convergence_phonons_examples)
    _phonon_kgrid_convergence_phonons_input_variables = (
        abinit_phonon_kgrid_convergence_phonons_input_variables)
    _phonon_kgrid_convergence_scf_kgrids = (
            abinit_phonon_kgrid_convergence_scf_kgrids)
    _phonon_kgrid_convergence_scf_input_variables = (
            abinit_phonon_kgrid_convergence_scf_input_variables)
    _phonon_kgrid_convergence_scf_examples = (
            abinit_phonon_kgrid_convergence_scf_examples)
    _phonon_smearing_convergence_criterion = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_CRITERION)
    _phonon_smearing_convergence_scf_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SCF_INPUT_VARS)
    _phonon_smearing_convergence_phonons_input_vars = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_INPUT_VARS)
    _phonon_smearing_convergence_example = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_EXAMPLE)
    _phonon_smearing_convergence_other_kwargs = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_OTHER_KWARGS)
    _phonon_smearing_convergence_phonon_qpt = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_PHONONS_QPT)
    _phonon_smearing_convergence_smearings = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_SMEARINGS)
    _phonon_smearing_convergence_kgrids = (
            ABINIT_PHONON_SMEARING_CONVERGENCE_KGRIDS)
    _phonons_script_name = "abinit"
    _relaxation_input_vars = abinit_relaxation_scf_input_variables
    _set_gs_calculation_kwargs = None
    _scf_script_name = "abinit"
    _workflow_cls = AbinitWorkflow

    @pytest.fixture(autouse=True)
    def setup_abinit_workflow(
            self, setup_workflow, temp_mrgddb_script, temp_anaddb_script,
            temp_optic_script,
            ):
        self.mrgddb_command_file = temp_mrgddb_script
        self.anaddb_command_file = temp_anaddb_script
        self.optic_command_file = temp_optic_script

    async def test_band_structure_convergence(self):
        workflow = self._workflow_cls(
                band_structure_convergence=True,
                **self._workflow_init_kwargs)
        await self._set_band_structure_convergence(workflow)
        await workflow.write()
        # make sure scf calcs were written
        seq = workflow.band_structure_convergence_sequencer
        ncalcs = len(seq.scf_specific_input_variables)
        dir_ = os.path.dirname(seq.scf_workdir)
        assert len(await aiofiles.os.listdir(dir_)) == ncalcs
        # set done and check workflow completed
        await aioshutil.rmtree(dir_)
        await self._set_band_structure_convergence_done(workflow)
        await workflow.run()
        # make sure there are 2ncalcs + 2 files in results dir
        results_dir = os.path.join(
                os.path.dirname(os.path.dirname(seq.scf_workdir)), "results")
        assert len(await aiofiles.os.listdir(results_dir)) == 2 * ncalcs + 2
        assert await workflow.workflow_completed

    async def test_bse_nband_convergence(self):
        workflow = self._workflow_cls(
                relaxation=True,
                bse_nband_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_bse_nband_convergence(workflow)
        seq = workflow.bse_nband_convergence_sequencer
        await workflow.write()
        # check scf part
        assert await is_calculation_directory(seq.scf_workdir)
        await aioshutil.rmtree(seq.scf_workdir)
        await self._set_bse_nband_convergence_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        assert await is_calculation_directory(seq.nscf_workdir)
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_bse_nband_convergence_done(workflow, nscf=True)
        # check nscf nosym part
        await workflow.write()
        assert await is_calculation_directory(seq.nscfnosym_workdir)
        await aioshutil.rmtree(seq.nscfnosym_workdir)
        await self._set_bse_nband_convergence_done(workflow, nscfnosym=True)
        # screening part
        await workflow.write()
        assert await is_calculation_directory(seq.screening_workdir)
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_bse_nband_convergence_done(workflow, screening=True)
        # BSE part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.bse_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.bse_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.bse_workdir)
        await self._set_bse_nband_convergence_done(workflow, bse=True)
        # check completed
        await workflow.run()
        assert await (workflow.workflow_completed)
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_bse_ecuteps_convergence(self):
        workflow = self._workflow_cls(
                relaxation=True,
                bse_ecuteps_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_bse_ecuteps_convergence(workflow)
        seq = workflow.bse_ecuteps_convergence_sequencer
        await workflow.write()
        # check scf part
        assert await is_calculation_directory(seq.scf_workdir)
        await aioshutil.rmtree(seq.scf_workdir)
        await self._set_bse_ecuteps_convergence_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        assert await is_calculation_directory(seq.nscf_workdir)
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_bse_ecuteps_convergence_done(workflow, nscf=True)
        # check nscf nosym part
        await workflow.write()
        assert await is_calculation_directory(seq.nscfnosym_workdir)
        await aioshutil.rmtree(seq.nscfnosym_workdir)
        await self._set_bse_ecuteps_convergence_done(workflow, nscfnosym=True)
        # screening part
        await workflow.write()
        assert await is_calculation_directory(seq.screening_workdir)
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_bse_ecuteps_convergence_done(workflow, screening=True)
        # BSE part
        await workflow.write()
        for subdir in await aiofiles.os.listdir(seq.bse_workdir):
            assert await is_calculation_directory(
                    os.path.join(seq.bse_workdir, subdir))
        await aioshutil.rmtree(seq.bse_workdir)
        await self._set_bse_ecuteps_convergence_done(workflow, bse=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_bse_kgrid_convergence(self):
        workflow = self._workflow_cls(
                relaxation=True,
                bse_kgrid_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_bse_kgrid_convergence(workflow)
        seq = workflow.bse_kgrid_convergence_sequencer
        await workflow.write()
        # check scf part
        coro = []
        for subdir in await aiofiles.os.listdir(seq.scf_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.scf_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.scf_workdir)
        await self._set_bse_kgrid_convergence_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.nscf_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.nscf_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_bse_kgrid_convergence_done(workflow, nscf=True)
        # check nscf nosym part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.nscfnosym_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.nscfnosym_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.nscfnosym_workdir)
        await self._set_bse_kgrid_convergence_done(workflow, nscfnosym=True)
        # screening part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.screening_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.screening_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_bse_kgrid_convergence_done(workflow, screening=True)
        # BSE part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.bse_workdir):
            coro.append(is_calculation_directory(
                    os.path.join(seq.bse_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.bse_workdir)
        await self._set_bse_kgrid_convergence_done(workflow, bse=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_bse(self):
        workflow = self._workflow_cls(
                relaxation=True,
                bse=True,
                **self._workflow_init_kwargs,
                )
        await self._set_bse(workflow)
        seq = workflow.bse_sequencer
        await workflow.write()
        # check scf part
        assert await is_calculation_directory(seq.scf_workdir)
        await aioshutil.rmtree(seq.scf_workdir)
        await self._set_bse_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        assert await is_calculation_directory(seq.nscf_workdir)
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_bse_done(workflow, nscf=True)
        # check nscf nosym part
        await workflow.write()
        assert await is_calculation_directory(seq.nscfnosym_workdir)
        await aioshutil.rmtree(seq.nscfnosym_workdir)
        await self._set_bse_done(workflow, nscfnosym=True)
        # screening part
        await workflow.write()
        assert await is_calculation_directory(seq.screening_workdir)
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_bse_done(workflow, screening=True)
        # BSE part
        await workflow.write()
        assert await is_calculation_directory(seq.bse_workdir)
        await aioshutil.rmtree(seq.bse_workdir)
        await self._set_bse_done(workflow, bse=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_fermi_surface(self):
        await super().test_fermi_surface(material="Al")

    async def test_gw(self):
        workflow = self._workflow_cls(
                relaxation=True,
                gw=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gw(workflow)
        await workflow.write()
        # check scf part
        assert await is_calculation_directory(
                    workflow.gw_sequencer.scf_workdir)
        await aioshutil.rmtree(workflow.gw_sequencer.scf_workdir)
        await self._set_gw_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        assert await is_calculation_directory(
                    workflow.gw_sequencer.nscf_workdir)
        await aioshutil.rmtree(workflow.gw_sequencer.nscf_workdir)
        await self._set_gw_done(workflow, nscf=True)
        # screening part
        await workflow.write()
        assert await is_calculation_directory(
                    workflow.gw_sequencer.screening_workdir)
        await aioshutil.rmtree(workflow.gw_sequencer.screening_workdir)
        await self._set_gw_done(workflow, screening=True)
        # self energy part
        await workflow.write()
        assert await is_calculation_directory(
                    workflow.gw_sequencer.selfenergy_workdir)
        await aioshutil.rmtree(workflow.gw_sequencer.selfenergy_workdir)
        await self._set_gw_done(workflow, selfenergy=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed

    async def test_gw_self_energy_nband_convergence(self):
        workflow = self._workflow_cls(
                gs=True,
                gw_self_energy_nband_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gw_self_energy_nband_convergence(workflow)
        await workflow.write()
        # check nscf part
        seq = workflow.gw_self_energy_nband_convergence_sequencer
        assert await is_calculation_directory(seq.nscf_workdir)
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_gw_self_energy_nband_convergence_done(
                workflow, nscf=True)
        # screening part
        await workflow.write()
        assert await is_calculation_directory(seq.screening_workdir)
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_gw_self_energy_nband_convergence_done(
                workflow, screening=True)
        # self energy part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.selfenergy_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.selfenergy_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.selfenergy_workdir)
        await self._set_gw_self_energy_nband_convergence_done(
                workflow, selfenergy=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_gw_screening_nband_convergence(self):
        workflow = self._workflow_cls(
                gs=True,
                gw_screening_nband_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gw_screening_nband_convergence(workflow)
        await workflow.write()
        # check nscf part
        seq = workflow.gw_screening_nband_convergence_sequencer
        assert await is_calculation_directory(seq.nscf_workdir)
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_gw_screening_nband_convergence_done(
                workflow, nscf=True)
        # screening part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.screening_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.screening_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_gw_screening_nband_convergence_done(
                workflow, screening=True)
        # self energy part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.selfenergy_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.selfenergy_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.selfenergy_workdir)
        await self._set_gw_screening_nband_convergence_done(
                workflow, selfenergy=True)
        # check completed
        await workflow.run()
        assert await workflow.workflow_completed
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_gw_ecuteps_convergence(self):
        workflow = self._workflow_cls(
                gs=True,
                gw_ecuteps_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gw_ecuteps_convergence(workflow)
        await workflow.write()
        # check nscf part
        seq = workflow.gw_ecuteps_convergence_sequencer
        assert await (is_calculation_directory(seq.nscf_workdir))
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_gw_ecuteps_convergence_done(workflow, nscf=True)
        # screening part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.screening_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.screening_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_gw_ecuteps_convergence_done(
                workflow, screening=True)
        # self energy part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.selfenergy_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.selfenergy_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.selfenergy_workdir)
        await self._set_gw_ecuteps_convergence_done(
                workflow, selfenergy=True)
        # check completed
        await workflow.run()
        assert await (workflow.workflow_completed)
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_gw_kgrid_convergence(self):
        workflow = self._workflow_cls(
                relaxation=True,
                gw_kgrid_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gw_kgrid_convergence(workflow)
        await workflow.write()
        seq = workflow.gw_kgrid_convergence_sequencer
        # check scf part
        coro = []
        for subdir in await aiofiles.os.listdir(seq.scf_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.scf_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.scf_workdir)
        await self._set_gw_kgrid_convergence_done(workflow, scf=True)
        # check nscf part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.nscf_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.nscf_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.nscf_workdir)
        await self._set_gw_kgrid_convergence_done(workflow, nscf=True)
        # screening part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.screening_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.screening_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.screening_workdir)
        await self._set_gw_kgrid_convergence_done(
                workflow, screening=True)
        # self energy part
        await workflow.write()
        coro = []
        for subdir in await aiofiles.os.listdir(seq.selfenergy_workdir):
            coro.append(is_calculation_directory(
                os.path.join(seq.selfenergy_workdir, subdir)))
        assert all(await asyncio.gather(*coro))
        await aioshutil.rmtree(seq.selfenergy_workdir)
        await self._set_gw_kgrid_convergence_done(
                workflow, selfenergy=True)
        # check completed
        await workflow.run()
        assert await (workflow.workflow_completed)
        results_dir = os.path.join(
                os.path.dirname(seq.screening_workdir), "results")
        assert len(await aiofiles.os.listdir(results_dir)) >= 2

    async def test_optic_nscf_convergence(self):
        workflow = self._workflow_cls(
                gs=True, band_structure=True,
                optic_nscf_convergence=True,
                **self._workflow_init_kwargs,
                )
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await self._set_band_structure(workflow)
        await self._set_band_structure_done(workflow)
        await self._set_optic_nscf_convergence(workflow)
        await workflow.write()
        sequencers = workflow.optic_nscf_convergence_sequencer

        async def get_and_check(seq_part):
            dirs = [getattr(seq, f"{seq_part}_workdir") for seq in sequencers]
            assert all(await asyncio.gather(
                    *(is_calculation_directory(d) for d in dirs)))
            await asyncio.gather(
                    *(aioshutil.rmtree(d) for d in dirs))

        await get_and_check("nscf")
        await self._set_optic_nscf_convergence_done(workflow, nscf=True)
        await workflow.write()
        # get_and_check("nscffbz")
        # self._set_optic_nscf_convergence_done(workflow, nscffbz=True)
        await workflow.write()
        await get_and_check("ddk")
        await self._set_optic_nscf_convergence_done(workflow, ddk=True)
        await workflow.write()
        await get_and_check("optic")
        await self._set_optic_nscf_convergence_done(workflow, optic=True)
        await workflow.optic_nscf_convergence_sequencer.clear_sequence()
        assert await (workflow.workflow_completed)
        await workflow.run()
        main_res_dir = os.path.join(
                os.path.dirname(os.path.dirname(
                    sequencers[0].nscf_workdir)),
                "results")
        assert len(await aiofiles.os.listdir(main_res_dir)) == 24

    async def _check_phonon_dispersion_post_phonons(self, workflow):
        dir_ = os.path.join(
                    self.workflow_dir.name,
                    "phonon_dispersion")
        # make sure q2r calculation is written
        assert "mrgddb_run" in await aiofiles.os.listdir(dir_)
        await aioshutil.rmtree(os.path.join(dir_, "mrgddb_run"))
        await self._set_phonon_dispersion_done(workflow, mrgddb=True)
        await workflow.write()
        # make sure the matdyn calc was written
        assert "anaddb_run" in await aiofiles.os.listdir(dir_)
        await aioshutil.rmtree(os.path.join(dir_, "anaddb_run"))
        await self._set_phonon_dispersion_done(workflow, anaddb=True)

    async def test_phonon_kgrid_convergence(self):
        await super().test_phonon_kgrid_convergence(ddk=True)

    async def test_phonon_kgrid_convergence_no_ddk(self):
        await super().test_phonon_kgrid_convergence(ddk=False)

    async def _check_phonon_dispersion_qgrid_convergence_post_phonons(
            self, workflow):
        coro = []
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.join(
                        self.workflow_dir.name,
                        "phonon_dispersion_qgrid_convergence",
                        os.path.basename(
                            os.path.dirname(
                                os.path.dirname(seq.phonons_workdir))))
            # make sure q2r calculation is written
            assert "mrgddb_run" in await aiofiles.os.listdir(dir_)
            coro.append(aioshutil.rmtree(os.path.join(dir_, "mrgddb_run")))
        await asyncio.gather(*coro)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, mrgddb=True)
        await workflow.write()
        coro = []
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            dir_ = os.path.join(
                        self.workflow_dir.name,
                        "phonon_dispersion_qgrid_convergence",
                        os.path.basename(
                            os.path.dirname(
                                os.path.dirname(seq.phonons_workdir))))
            # make sure the matdyn calc was written
            assert "anaddb_run" in await aiofiles.os.listdir(dir_)
            coro.append(aioshutil.rmtree(os.path.join(dir_, "anaddb_run")))
        await asyncio.gather(*coro)
        await self._set_phonon_dispersion_qgrid_convergence_done(
                workflow, anaddb=True)
        await workflow.run()

    async def _set_band_structure_convergence(self, workflow):
        calculation_parameters = {
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                }
        await workflow.set_band_structure_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "band_structure_convergence"),
                scf_specific_input_variables=[
                    {"ecut": ecut} for ecut in [10, 30]],
                scf_input_variables={
                    "acell": [7.2078778121] * 3,
                    "rprim": [[0.0, 0.70710678119, 0.70710678119],
                              [0.70710678119, 0.0, 0.70710678119],
                              [0.70710678119, 0.70710678119, 0.0]],
                    "ntypat": 1,
                    "znucl": [14],
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [
                        [0.0, 0.0, 0.0],
                        [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "ngkpt": [6, 6, 6],
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 50,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    "pseudos": abinit_gs_pseudos,
                    "pp_dirpath": "",
                    },
                scf_calculation_parameters=calculation_parameters,
                band_structure_input_variables={
                    "iscf": -2,
                    "nband": 8,
                    "tolwfr": 1e-12,
                    "diemac": 12.0,
                    "autoparal": 1,
                    },
                band_structure_calculation_parameters=calculation_parameters,
                band_structure_kpoint_path=[
                    {"L": [0.5, 0.0, 0.0]},
                    {r"$\Gamma$": [0.0, 0.0, 0.0]},
                    {"X": [0.0, 0.5, 0.5]},
                    {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
                band_structure_kpoint_path_density=20,
                plot_calculation_parameters={"show": False},
                )

    async def _set_band_structure_convergence_done(self, workflow):
        # there are scf calculations and band structure calculations to copy
        seq = workflow.band_structure_convergence_sequencer
        scf_root = os.path.dirname(seq.scf_workdir)
        bs_root = os.path.dirname(seq.band_structure_workdir)
        scf_root_examples = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "scf_runs")
        bs_root_examples = os.path.join(
                __TEST_WORKFLOW_ROOT__, "test_files", "abinit_si", "nc",
                "band_structure_ecut_convergence", "band_structure_runs")
        coro = []
        for scf_calc, bs_calc in zip(
                sorted(await aiofiles.os.listdir(scf_root_examples)),
                sorted(await aiofiles.os.listdir(bs_root_examples))):
            target_scf = os.path.join(scf_root, scf_calc)
            coro.append(copy_calculation(
                    os.path.join(scf_root_examples, scf_calc),
                    target_scf,
                    new_pseudos=abinit_gs_pseudos))
            coro.append(copy_calculation(
                    os.path.join(bs_root_examples, bs_calc),
                    os.path.join(bs_root, bs_calc),
                    new_parents=[target_scf],
                    new_pseudos=abinit_gs_pseudos))
        await asyncio.gather(*coro)

    async def _set_band_structure(self, *args):
        await super()._set_band_structure(*args, fatbands=True)

    async def _set_band_structure_done(self, workflow):
        await super()._set_band_structure_done(
                workflow, new_pseudos=abinit_gs_pseudos)

    async def _set_bse(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_bse(
            root_workdir=os.path.join(
                self.workflow_dir.name, "bse"),
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "ngkpt": [4, 4, 4],
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            screening_input_variables={
                "ecuteps": 4,
                "nband": 100,
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            use_relaxed_geometry=True,
            bse_input_variables={
                "bs_loband": 2,
                "nband": 7,
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecuteps": 3.0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )

    async def _set_bse_done(
            self, workflow, scf=False, nscf=False,
            nscfnosym=False, screening=False,
            bse=False):
        seq = workflow.bse_sequencer
        root = abinit_bse_example
        coro = []
        if scf:
            coro.append(copy_calculation(
                    os.path.join(root, "scf_run"),
                    seq.scf_workdir,
                    new_parents=[],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscfnosym:
            coro.append(copy_calculation(
                    os.path.join(root, "nscfnosym_run"),
                    seq.nscfnosym_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            coro.append(copy_calculation(
                    os.path.join(root, "screening_run"),
                    seq.screening_workdir,
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if bse:
            coro.append(copy_calculation(
                    os.path.join(root, "bse_run"),
                    seq.bse_workdir,
                    new_parents=[
                        seq.nscfnosym_workdir,
                        seq.screening_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_bse_nband_convergence(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_bse_nband_convergence(
            root_workdir=os.path.join(
                self.workflow_dir.name, "bse"),
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "ngkpt": [4, 4, 4],
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            screening_input_variables={
                "ecuteps": 4,
                "nband": 100,
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            use_relaxed_geometry=True,
            bse_input_variables={
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecuteps": 2.0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            bse_lobands=[2, 3, 2],
            bse_nbands=[8, 6, 7],
            plot_calculation_parameters={
                "show": False,
                },
            )

    async def _set_bse_nband_convergence_done(
            self, workflow, scf=False, nscf=False,
            nscfnosym=False, screening=False,
            bse=False):
        seq = workflow.bse_nband_convergence_sequencer
        root = abinit_bse_nband_convergence_example
        coro = []
        if scf:
            coro.append(copy_calculation(
                    os.path.join(root, "scf_run"),
                    seq.scf_workdir,
                    new_parents=[],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscfnosym:
            coro.append(copy_calculation(
                    os.path.join(root, "nscfnosym_run"),
                    seq.nscfnosym_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            coro.append(copy_calculation(
                    os.path.join(root, "screening_run"),
                    seq.screening_workdir,
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if bse:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "bse_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "bse_run", subdir),
                    os.path.join(seq.bse_workdir, subdir),
                    new_parents=[
                        seq.nscfnosym_workdir,
                        seq.screening_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_bse_kgrid_convergence(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_bse_kgrid_convergence(
            root_workdir=os.path.join(
                self.workflow_dir.name, "bse_ngkpt_convergence"),
            scf_kgrids=[[x] * 3 for x in [4, 5, 6]],
            scf_kgrids_input_variable_name=(
                abinit_bse_kgrid_convergence_input_variable_name),
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            screening_input_variables={
                "ecuteps": 4,
                "nband": 100,
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            use_relaxed_geometry=True,
            bse_input_variables={
                "bs_loband": 2, "nband": 7,
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecuteps": 3.0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )

    async def _set_bse_kgrid_convergence_done(
            self, workflow, scf=False, nscf=False,
            nscfnosym=False, screening=False,
            bse=False):
        seq = workflow.bse_kgrid_convergence_sequencer
        root = abinit_bse_kgrid_convergence_example
        coro = []
        if scf:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "scf_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "scf_run", subdir),
                    os.path.join(seq.scf_workdir, subdir),
                    new_parents=[],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscf:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "nscf_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "nscf_run", subdir),
                    os.path.join(seq.nscf_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscfnosym:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "nscfnosym_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "nscfnosym_run", subdir),
                    os.path.join(seq.nscfnosym_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "screening_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "screening_run", subdir),
                    os.path.join(seq.screening_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.nscf_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if bse:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "bse_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "bse_run", subdir),
                    os.path.join(seq.bse_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.nscfnosym_workdir, subdir),
                        os.path.join(seq.screening_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_bse_ecuteps_convergence(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_bse_ecuteps_convergence(
            root_workdir=os.path.join(
                self.workflow_dir.name, "bse_ecuteps_convergence"),
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "ngkpt": [4, 4, 4],
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "ngkpt": [4, 4, 4],
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            screening_input_variables={
                "ecuteps": 4,
                "nband": 100,
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            use_relaxed_geometry=True,
            bse_input_variables={
                "bs_calctype": 1,
                "bs_loband": 2,
                "nband": 7,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            bse_ecutepss=[1, 2, 3, 4],
            plot_calculation_parameters={
                "show": False,
                },
            )

    async def _set_bse_ecuteps_convergence_done(
            self, workflow, scf=False, nscf=False,
            nscfnosym=False, screening=False,
            bse=False):
        seq = workflow.bse_ecuteps_convergence_sequencer
        nband_root = abinit_bse_nband_convergence_example
        root = abinit_bse_ecuteps_convergence_example
        coro = []
        if scf:
            coro.append(copy_calculation(
                    os.path.join(nband_root, "scf_run"),
                    seq.scf_workdir,
                    new_parents=[],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(nband_root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if nscfnosym:
            coro.append(copy_calculation(
                    os.path.join(nband_root, "nscfnosym_run"),
                    seq.nscfnosym_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            coro.append(copy_calculation(
                    os.path.join(nband_root, "screening_run"),
                    seq.screening_workdir,
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if bse:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "bse_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "bse_run", subdir),
                    os.path.join(seq.bse_workdir, subdir),
                    new_parents=[
                        seq.nscfnosym_workdir,
                        seq.screening_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_dos_done(self, workflow, *args, **kwargs):
        await super()._set_dos_done(
                workflow, *args,
                new_pseudos=abinit_gs_pseudos,
                new_parents=[workflow.dos_sequencer.scf_workdir],
                **kwargs,
                )

    async def _set_fermi_surface_done(self, *args, **kwargs):
        await super()._set_fermi_surface_done(
                *args,
                new_pseudos=ABINIT_Al_GS_PSEUDOS,
                **kwargs)

    async def _set_gs(self, *args, material="Si_nc", **kwargs):
        if material == "Si_nc":
            await super()._set_gs(*args, **kwargs)
        elif material == "Al":
            await self._set_gs_al(*args, **kwargs)
        else:
            raise DevError(material)

    async def _set_gs_al(self, workflow):
        await workflow.set_gs(
                root_workdir=os.path.join(self.workflow_dir.name, "gs_run"),
                scf_input_variables=ABINIT_Al_GS_INPUT_VARIABLES.copy(),
                scf_calculation_parameters={
                    "command": self.scf_command_file.name,
                    "queuing_system": "local",
                    },
                )

    async def _set_gs_done(self, workflow, *args, material="Si_nc", **kwargs):
        if material == "Si_nc":
            await super()._set_gs_done(
                    workflow, *args, new_pseudos=abinit_gs_pseudos, **kwargs)
        elif material == "Al":
            await self._set_gs_done_al(workflow, *args, **kwargs)

    async def _set_gs_done_al(self, workflow, *args, **kwargs):
        await copy_calculation(
                ABINIT_Al_GS_EXAMPLE, workflow.gs_sequencer.scf_workdir, *args,
                new_pseudos=ABINIT_Al_GS_PSEUDOS,
                **kwargs)

    async def _set_gs_ecut_convergence_done(self, workflow, varname):
        # 2 sets of calculations to copy: 'ecut' and 'pawecutdg'
        # start with ECUT example
        await super()._set_gs_ecut_convergence_done(
                workflow, varname,
                new_pseudos=abinit_gs_pseudos)
        # reset workflow
        # at this point there should be only one sequencer
        await workflow.gs_ecut_convergence_sequencer.clear_sequence()
        await self._set_gs_ecut_convergence(workflow)
        seq = workflow.gs_ecut_convergence_sequencer[1]
        varname = seq.ecuts_input_variable_name
        coro = []
        for calcdir in await aiofiles.os.listdir(
                abinit_gs_pawecut_convergence_example):
            coro.append(copy_calculation(
                    os.path.join(
                        abinit_gs_pawecut_convergence_example,
                        calcdir),
                    os.path.join(
                        workflow.gs_ecut_convergence_sequencer[1].scf_workdir,
                        varname, calcdir),
                    new_pseudos=abinit_gs_pseudos)
                    )
        await asyncio.gather(*coro)

    async def _set_gs_kgrid_convergence_done(self, workflow, *args, **kwargs):
        await super()._set_gs_kgrid_convergence_done(
                workflow, *args, new_pseudos=abinit_gs_pseudos)

    async def _set_gs_smearing_convergence(self, workflow, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        await super()._set_gs_smearing_convergence(workflow, *args, **kwargs)

    async def _set_gs_smearing_convergence_done(
            self, workflow, *args, **kwargs):
        # for this test we use a different material and thus different pseudo
        await super()._set_gs_smearing_convergence_done(
                workflow, *args,
                new_pseudos=abinit_gs_smearing_convergence_pseudos,
                **kwargs)

    async def _set_gw(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_gw(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "gw"),
                scf_input_variables={
                    "nband": 6,
                    "tolvrs": 1e-10,
                    "nshiftk": 4,
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ngkpt": [4, 4, 4],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                scf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscf_input_variables={
                    "nband": 120,
                    "tolwfr": 1e-12,
                    "nbdbuf": 20,
                    "nshiftk": 4,
                    "ngkpt": [4, 4, 4],
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_input_variables={
                    "ppmfrq": 16.7 * EV_TO_HA,
                    "nband": 100,
                    "ecuteps": 7.0,
                    },
                screening_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                use_relaxed_geometry=True,
                selfenergy_input_variables={
                    "nband": 100,
                    "ecutsigx": 8.0,
                    "nkptgw": 1,
                    "kptgw": [0.0, 0.0, 0.0],
                    "bdgw": [4, 5],
                    },
                selfenergy_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                )

    async def _set_gw_done(
            self, workflow, scf=False, nscf=False, screening=False,
            selfenergy=False):
        coro = []
        if scf:
            coro.append(copy_calculation(
                    os.path.join(
                        abinit_gw_example, "scf_run"),
                    workflow.gw_sequencer.scf_workdir,
                    new_parents=[],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(
                        abinit_gw_example, "nscf_run"),
                    workflow.gw_sequencer.nscf_workdir,
                    new_parents=[
                        workflow.gw_sequencer.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if screening:
            coro.append(copy_calculation(
                    os.path.join(
                        abinit_gw_example, "screening_run"),
                    workflow.gw_sequencer.screening_workdir,
                    new_parents=[
                        workflow.gw_sequencer.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if selfenergy:
            coro.append(copy_calculation(
                    os.path.join(
                        abinit_gw_example, "selfenergy_run"),
                    workflow.gw_sequencer.selfenergy_workdir,
                    new_parents=[
                        workflow.gw_sequencer.nscf_workdir,
                        workflow.gw_sequencer.screening_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        await asyncio.gather(*coro)

    async def _set_gw_self_energy_nband_convergence(self, workflow):
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await workflow.set_gw_self_energy_nband_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gw_self_energy_nband_convergence"),
                nscf_input_variables={
                    "nband": 180,
                    "tolwfr": 1e-12,
                    "nbdbuf": 10,
                    "nshiftk": 4,
                    "ngkpt": [4, 4, 4],
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_input_variables={
                    "ppmfrq": 16.7 * EV_TO_HA,
                    "nband": 50,
                    "ecuteps": 6.0,
                    },
                screening_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                selfenergy_input_variables={
                    "ecutsigx": 8.0,
                    "nkptgw": 1,
                    "kptgw": [0.0, 0.0, 0.0],
                    "bdgw": [4, 5],
                    },
                selfenergy_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                selfenergy_nbands=[25, 50, 100, 150, 180],
                plot_calculation_parameters={
                    "show": False,
                    },
                selfenergy_nband_convergence_criterion=3,
                use_gs=True,
                )

    async def _set_gw_self_energy_nband_convergence_done(
            self, workflow, nscf=False, screening=False, selfenergy=False):
        seq = workflow.gw_self_energy_nband_convergence_sequencer
        root = abinit_gw_self_energy_nband_convergence_example
        coro = []
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if screening:
            coro.append(copy_calculation(
                    os.path.join(root, "screening_run"),
                    seq.screening_workdir,
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    )
                    )
        if selfenergy:
            for subdir in os.listdir(os.path.join(root, "selfenergy_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "selfenergy_run", subdir),
                    os.path.join(seq.selfenergy_workdir, subdir),
                    new_parents=[
                        seq.nscf_workdir,
                        seq.screening_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_gw_screening_nband_convergence(self, workflow):
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await workflow.set_gw_screening_nband_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gw_screening_nband_convergence"),
                nscf_input_variables={
                    "nband": 180,
                    "tolwfr": 1e-12,
                    "nbdbuf": 10,
                    "nshiftk": 4,
                    "ngkpt": [4, 4, 4],
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_input_variables={
                    "ppmfrq": 16.7 * EV_TO_HA,
                    "ecuteps": 6.0,
                    },
                screening_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                selfenergy_input_variables={
                    "ecutsigx": 8.0,
                    "nkptgw": 1,
                    "kptgw": [0.0, 0.0, 0.0],
                    "bdgw": [4, 5],
                    "nband": 100,
                    },
                selfenergy_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_nbands=[25, 50, 100, 150, 180],
                plot_calculation_parameters={
                    "show": False,
                    },
                screening_nband_convergence_criterion=3,
                use_gs=True,
                )

    async def _set_gw_screening_nband_convergence_done(
            self, workflow, nscf=False, screening=False, selfenergy=False):
        seq = workflow.gw_screening_nband_convergence_sequencer
        root = abinit_gw_screening_nband_convergence_example
        self_root = abinit_gw_self_energy_nband_convergence_example
        coro = []
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(self_root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "screening_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "screening_run", subdir),
                    os.path.join(seq.screening_workdir, subdir),
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if selfenergy:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "selfenergy_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "selfenergy_run", subdir),
                    os.path.join(seq.selfenergy_workdir, subdir),
                    new_parents=[
                        seq.nscf_workdir,
                        os.path.join(seq.screening_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_gw_ecuteps_convergence(self, workflow):
        await self._set_gs(workflow)
        await self._set_gs_done(workflow)
        await workflow.set_gw_ecuteps_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gw_ecuteps_convergence"),
                nscf_input_variables={
                    "nband": 180,
                    "tolwfr": 1e-12,
                    "nbdbuf": 10,
                    "nshiftk": 4,
                    "ngkpt": [4, 4, 4],
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_input_variables={
                    "ppmfrq": 16.7 * EV_TO_HA,
                    "ecuteps": 6.0,
                    "nband": 100,
                    },
                screening_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                selfenergy_input_variables={
                    "ecutsigx": 8.0,
                    "nkptgw": 1,
                    "kptgw": [0.0, 0.0, 0.0],
                    "bdgw": [4, 5],
                    "nband": 100,
                    },
                selfenergy_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_ecutepss=[2, 4, 6, 8],
                plot_calculation_parameters={
                    "show": False,
                    },
                screening_ecuteps_convergence_criterion=30,
                use_gs=True,
                )

    async def _set_gw_ecuteps_convergence_done(
            self, workflow, nscf=False, screening=False, selfenergy=False):
        seq = workflow.gw_ecuteps_convergence_sequencer
        root = abinit_gw_ecuteps_convergence_example
        self_root = abinit_gw_self_energy_nband_convergence_example
        coro = []
        if nscf:
            coro.append(copy_calculation(
                    os.path.join(self_root, "nscf_run"),
                    seq.nscf_workdir,
                    new_parents=[
                        seq.scf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "screening_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "screening_run", subdir),
                    os.path.join(seq.screening_workdir, subdir),
                    new_parents=[
                        seq.nscf_workdir,
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if selfenergy:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "selfenergy_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "selfenergy_run", subdir),
                    os.path.join(seq.selfenergy_workdir, subdir),
                    new_parents=[
                        seq.nscf_workdir,
                        os.path.join(seq.screening_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_gw_kgrid_convergence(self, workflow):
        await self._set_relaxation(workflow)
        await self._set_relaxation_done(workflow)
        await workflow.set_gw_kgrid_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "gw_kgrid_convergence"),
                scf_kgrids_input_variable_name="ngkpt",
                scf_input_variables={
                    "nband": 6,
                    "tolvrs": 1e-10,
                    "nshiftk": 4,
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                scf_kgrid_convergence_criterion=5,
                scf_kgrids=[[x, x, x] for x in [2, 4, 8]],
                scf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscf_input_variables={
                    "nband": 100,
                    "tolwfr": 1e-12,
                    "nbdbuf": 10,
                    "nshiftk": 4,
                    "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                               [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ecut": 8.0,
                    "diemac": 12.0,
                    },
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                screening_input_variables={
                    "ppmfrq": 16.7 * EV_TO_HA,
                    "ecuteps": 4,
                    "nband": 100,
                    },
                screening_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                selfenergy_input_variables={
                    "ecutsigx": 8.0,
                    "nkptgw": 1,
                    "kptgw": [0.0, 0.0, 0.0],
                    "bdgw": [4, 5],
                    "nband": 100,
                    },
                selfenergy_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                plot_calculation_parameters={
                    "show": False,
                    },
                use_relaxed_geometry=True,
                )

    async def _set_gw_kgrid_convergence_done(
            self, workflow, scf=False,
            nscf=False, screening=False, selfenergy=False):
        seq = workflow.gw_kgrid_convergence_sequencer
        root = abinit_gw_kgrid_convergence_example
        coro = []
        if scf:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "scf_run")):
                coro.append(copy_calculation(
                        os.path.join(root, "scf_run", subdir),
                        os.path.join(seq.scf_workdir, subdir),
                        new_parents=[],
                        new_pseudos=abinit_gs_pseudos,
                        ))
        if nscf:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "nscf_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "nscf_run", subdir),
                    os.path.join(seq.nscf_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if screening:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "screening_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "screening_run", subdir),
                    os.path.join(seq.screening_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.nscf_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        if selfenergy:
            for subdir in await aiofiles.os.listdir(
                    os.path.join(root, "selfenergy_run")):
                coro.append(copy_calculation(
                    os.path.join(root, "selfenergy_run", subdir),
                    os.path.join(seq.selfenergy_workdir, subdir),
                    new_parents=[
                        os.path.join(seq.nscf_workdir, subdir),
                        os.path.join(seq.screening_workdir, subdir),
                        ],
                    new_pseudos=abinit_gs_pseudos,
                    ))
        await asyncio.gather(*coro)

    async def _set_optic_nscf_convergence(self, workflow):
        name = abinit_optic_nscf_convergence_nscf_kpoint_grids_input_varname
        await workflow.set_optic_nscf_convergence(
                root_workdir=os.path.join(
                    self.workflow_dir.name, "optic_nscf_convergence"),
                experimental_bandgap=(
                    abinit_optic_nscf_convergence_experimental_bandgap),
                compute_non_linear_optical_response=False,
                nscf_input_variables=(
                    abinit_optic_nscf_convergence_nscf_input_variables),
                nscf_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscffbz_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                nscf_kpoint_grids=(
                    abinit_optic_nscf_convergence_nscf_kpoint_grids),
                nscf_kpoint_grids_input_variable_name=name,
                ddk_input_variables=(
                    abinit_optic_nscf_convergence_ddk_input_variables),
                ddk_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.scf_command_file.name,
                    },
                optic_input_variables=(
                    abinit_optic_nscf_convergence_optic_input_variables),
                optic_calculation_parameters={
                    "queuing_system": "local",
                    "command": self.optic_command_file.name,
                    },
                use_gs=True,
                plot_calculation_parameters={"show": False},
                )

    async def _set_optic_nscf_convergence_done(
            self, workflow, nscf=False, nscffbz=False, ddk=False, optic=False):
        async def copy(sequencer, prefix, new_parents):
            target = getattr(sequencer, f"{prefix}_workdir")
            src = os.path.join(
                    abinit_optic_nscf_convergence_example,
                    os.path.basename(os.path.dirname(target)),
                    os.path.basename(target))
            await copy_calculation(
                    src, target,
                    new_parents=new_parents,
                    new_pseudos=abinit_optic_nscf_convergence_pseudos,
                    )
        coro = []
        for seq in workflow.optic_nscf_convergence_sequencer:
            if nscf:
                coro.append(
                        copy(seq, "nscf",
                             new_parents=[workflow.gs_sequencer.scf_workdir]
                             ))
            if nscffbz:
                coro.append(
                        copy(seq, "nscffbz",
                             new_parents=[
                                seq.nscf_workdir,
                                workflow.gs_sequencer.scf_workdir]))
            if ddk:
                coro.append(
                        copy(seq, "ddk",
                             new_parents=[
                                seq.nscf_workdir,
                                ]))
            if optic:
                coro.append(
                        copy(seq, "optic",
                             new_parents=[
                                seq.ddk_workdir,
                                seq.nscf_workdir,
                                ]))
        await asyncio.gather(*coro)

    async def _set_phonon_ecut_convergence_done(
            self, workflow, *args, **kwargs):
        await super()._set_phonon_ecut_convergence_done(
                workflow, *args,
                _copy_scf_extra_kwargs={
                    "new_pseudos": abinit_phonon_ecut_convergence_pseudos},
                _copy_phonons_extra_kwargs={
                    "new_pseudos": abinit_phonon_ecut_convergence_pseudos},
                **kwargs)

    async def _set_phonon_dispersion(self, *args, **kwargs):
        varname = abinit_phonon_dispersion_qgrid_generation_input_variable_name
        await super()._set_phonon_dispersion(
                *args,
                compute_electric_field_response=(
                    abinit_phonon_dispersion_compute_electric_field_response),
                ddk_input_variables=(
                    abinit_phonon_dispersion_ddk_input_variables),
                ddk_calculation_parameters={
                    "command": self.phonons_command_file.name,
                    "queuing_system": "local"},
                mrgddb_calculation_parameters={
                    "command": self.mrgddb_command_file.name,
                    "queuing_system": "local",
                    },
                anaddb_input_variables=(
                    abinit_phonon_dispersion_anaddb_input_variables),
                anaddb_calculation_parameters={
                    "command": self.anaddb_command_file.name,
                    "queuing_system": "local",
                    },
                phonons_qpoints_split_by_irreps=(
                    abinit_phonon_dispersion_qpoints_split_by_irreps),
                phonons_qgrid_generation_input_variable_name=varname,
                **kwargs)

    async def _set_phonon_dispersion_done(
            self, workflow,
            all_phonons=False, first_phonon=False, mrgddb=False,
            ddk=False,
            anaddb=False, **kwargs):
        coro = []
        coro.append(super()._set_phonon_dispersion_done(
                workflow,
                new_pseudos=abinit_phonon_dispersion_pseudos,
                **kwargs)
                )
        seq = workflow.phonon_dispersion_sequencer
        root = self._phonon_dispersion_example
        if first_phonon:
            coro.append(copy_calculation(
                    os.path.join(
                        root, "phonons_runs", "ph_q_qgrid_generation"),
                    os.path.join(
                        os.path.dirname(seq.phonons_workdir),
                        "ph_q_qgrid_generation"),
                    new_pseudos=abinit_phonon_dispersion_pseudos))
            # also copy ddk
            coro.append(self._set_phonon_dispersion_done(workflow, ddk=True))
        if ddk:
            coro.append(copy_calculation(
                    os.path.join(root, "ddk_runs"),
                    seq.ddk_workdir,
                    new_pseudos=abinit_phonon_dispersion_pseudos,
                    new_parents=[seq.scf_workdir])
                    )
        if all_phonons:
            # also copy again ddk
            coro.append(self._set_phonon_dispersion_done(workflow, ddk=True))
            for calcdir in await aiofiles.os.listdir(
                    os.path.join(root, "phonons_runs")):
                new_parents = [seq.scf_workdir]
                if calcdir == "ph_q1":
                    new_parents.append(seq.ddk_workdir)
                if any(["atom_pol" in x for x in await aiofiles.os.listdir(
                        os.path.join(root, "phonons_runs", calcdir))]):
                    # copy subdirs
                    for subcalcdir in await aiofiles.os.listdir(
                            os.path.join(root, "phonons_runs", calcdir)):
                        coro.append(copy_calculation(
                                os.path.join(
                                    root, "phonons_runs", calcdir, subcalcdir),
                                os.path.join(
                                    os.path.dirname(seq.phonons_workdir),
                                    calcdir, subcalcdir),
                                new_parents=new_parents,
                                new_pseudos=abinit_phonon_dispersion_pseudos,
                                **kwargs)
                                )
                else:
                    coro.append(copy_calculation(
                        os.path.join(root, "phonons_runs", calcdir),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir), calcdir),
                        new_parents=new_parents,
                        new_pseudos=abinit_phonon_dispersion_pseudos,
                        **kwargs)
                        )
        if mrgddb:
            phroot = os.path.dirname(seq.phonons_workdir)
            new_parents = []
            for calcdir in await aiofiles.os.listdir(phroot):
                if any(["atom_pol" in x
                        for x in await aiofiles.os.listdir(
                            os.path.join(phroot, calcdir))]):
                    # add all subcalcdir as parents
                    for subcalcdir in await aiofiles.os.listdir(
                            os.path.join(phroot, calcdir)):
                        if "irreps" in subcalcdir:
                            continue
                        new_parents.append(
                                os.path.join(phroot, calcdir, subcalcdir))
                elif "qgrid_gen" in calcdir:
                    continue
                else:
                    new_parents.append(os.path.join(phroot, calcdir))
            coro.append(copy_calculation(
                    os.path.join(root, "mrgddb_run"),
                    seq.mrgddb_workdir,
                    new_parents=new_parents,
                    ))
        if anaddb:
            coro.append(copy_calculation(
                    os.path.join(root, "anaddb_run"),
                    seq.anaddb_workdir,
                    new_parents=[seq.mrgddb_workdir])
                    )
        await asyncio.gather(*coro)

    async def _set_phonon_dispersion_qgrid_convergence(self, *args, **kwargs):
        await super()._set_phonon_dispersion_qgrid_convergence(
                *args,
                compute_electric_field_response=True,
                ddk_input_variables={
                    "rfdir": [1, 1, 1],
                    "tolwfr": 1e-16,
                    "nstep": 25,
                    "diemac": 12.0,
                    },
                ddk_calculation_parameters={
                    "command": self.phonons_command_file.name,
                    "queuing_system": "local"},
                mrgddb_calculation_parameters={
                    "command": self.mrgddb_command_file.name,
                    "queuing_system": "local",
                    },
                anaddb_input_variables={
                    "ifcflag": 1, "ifcout": 0, "brav": 2,
                    "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                    "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                    },
                anaddb_calculation_parameters={
                    "command": self.anaddb_command_file.name,
                    "queuing_system": "local",
                    },
                phonons_qgrid_generation_input_variable_name="ngkpt",
                **kwargs)

    async def _set_phonon_dispersion_qgrid_convergence_done(
            self, workflow,
            all_phonons=False, first_phonon=False, mrgddb=False,
            ddk=False,
            anaddb=False, **kwargs):
        await super()._set_phonon_dispersion_qgrid_convergence_done(
                workflow,
                new_pseudos=abinit_phonon_dispersion_pseudos,
                **kwargs)
        if all_phonons:
            # also copy again ddk
            await self._set_phonon_dispersion_qgrid_convergence_done(
                    workflow, ddk=True)
        coro = []
        for seq in workflow.phonon_dispersion_qgrid_convergence_sequencer:
            root = os.path.join(
                    self._phonon_dispersion_qgrid_convergence_example,
                    os.path.basename(
                        os.path.dirname(os.path.dirname(seq.phonons_workdir))))
            if first_phonon:
                coro.append(copy_calculation(
                        os.path.join(
                            root, "phonons_runs", "ph_q_qgrid_generation"),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir),
                            "ph_q_qgrid_generation"),
                        new_pseudos=abinit_phonon_dispersion_pseudos))
            if ddk:
                coro.append(copy_calculation(
                        os.path.join(root, "ddk_runs"),
                        seq.ddk_workdir,
                        new_pseudos=abinit_phonon_dispersion_pseudos,
                        new_parents=[seq.scf_workdir]))
            if all_phonons:
                for calcdir in await aiofiles.os.listdir(
                        os.path.join(root, "phonons_runs")):
                    new_parents = [seq.scf_workdir]
                    if calcdir == "ph_q1":
                        new_parents.append(seq.ddk_workdir)
                    coro.append(copy_calculation(
                        os.path.join(root, "phonons_runs", calcdir),
                        os.path.join(
                            os.path.dirname(seq.phonons_workdir), calcdir),
                        new_parents=new_parents,
                        new_pseudos=abinit_phonon_dispersion_pseudos,
                        **kwargs))
            if mrgddb:
                phroot = os.path.dirname(seq.phonons_workdir)
                coro.append(copy_calculation(
                        os.path.join(root, "mrgddb_run"),
                        seq.mrgddb_workdir,
                        new_parents=[
                            os.path.join(phroot, calc)
                            for calc in await aiofiles.os.listdir(phroot)
                            if "qgrid_gen" not in calc],
                        ))
            if anaddb:
                coro.append(copy_calculation(
                        os.path.join(root, "anaddb_run"),
                        seq.anaddb_workdir,
                        new_parents=[seq.mrgddb_workdir]))
        await asyncio.gather(*coro)
        if first_phonon:
            # also copy ddk
            await self._set_phonon_dispersion_qgrid_convergence_done(
                    workflow, ddk=True)

    async def _set_phonon_kgrid_convergence(self, workflow, ddk=False):
        await workflow.set_phonon_kgrid_convergence(
            root_workdir=os.path.join(
                    self.workflow_dir.name,
                    "phonon_kgrid_convergence"),
            scf_input_variables=(
                abinit_phonon_kgrid_convergence_scf_input_variables.copy()),
            scf_calculation_parameters={"command": self.scf_command_file.name,
                                        "queuing_system": "local"},
            scf_kgrids=abinit_phonon_kgrid_convergence_scf_kgrids,
            scf_kgrids_input_variable_name=(
                abinit_phonon_kgrid_convergence_kgrid_input_variable_name),
            ddk_input_variables=(
                abinit_phonon_kgrid_convergence_ddk_input_variables.copy()),
            ddk_calculation_parameters={
                "command": self.scf_command_file.name,
                "queuing_system": "local",
                },
            phonons_qpt=(
                abinit_phonon_kgrid_convergence_phonons_qpt if ddk else (
                    abinit_phonon_kgrid_convergence_phonons_qpt_no_ddk)),
            phonons_input_variables=(
                self._phonon_kgrid_convergence_phonons_input_variables.copy()),
            phonons_calculation_parameters={
                "command": self.phonons_command_file.name,
                "queuing_system": "local"},
            phonons_convergence_criterion=10,
            plot_calculation_parameters={"show": False},
            compute_electric_field_response=ddk,
            )

    async def _set_phonon_kgrid_convergence_done(
            self, workflow, *args, ddk=False, phonons_with_ddk=False,
            **kwargs):
        coro = []
        coro.append(super()._set_phonon_kgrid_convergence_done(
                workflow, *args,
                new_pseudos=abinit_phonon_kgrid_convergence_pseudos,
                **kwargs))
        seq = workflow.phonon_kgrid_convergence_sequencer
        varname = seq.kgrids_input_variable_name
        if ddk:
            root = os.path.join(
                    abinit_phonon_kgrid_convergence_example,
                    "ddk_runs", varname)
            for calcdir in await aiofiles.os.listdir(root):
                coro.append(copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(seq.ddk_workdir, varname, calcdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, varname, calcdir)],
                    new_pseudos=abinit_phonon_kgrid_convergence_pseudos,
                    ))
        if phonons_with_ddk:
            root = os.path.join(
                    abinit_phonon_kgrid_convergence_example,
                    "phonons_runs", varname)
            for calcdir in await aiofiles.os.listdir(root):
                coro.append(copy_calculation(
                    os.path.join(root, calcdir),
                    os.path.join(
                        seq.phonons_workdir, varname, calcdir),
                    new_parents=[
                        os.path.join(seq.scf_workdir, varname, calcdir),
                        os.path.join(seq.ddk_workdir, varname, calcdir)],
                    new_pseudos=abinit_phonon_kgrid_convergence_pseudos,
                    )
                    )
        await asyncio.gather(*coro)

    async def _set_phonon_smearing_convergence(
            self, workflow, *args, **kwargs):
        # use the Al example
        await super()._set_phonon_smearing_convergence(
                workflow, *args, **kwargs)

    async def _set_phonon_smearing_convergence_done(
            self, workflow, *args, **kwargs):
        await super()._set_phonon_smearing_convergence_done(
                workflow, *args,
                copy_scf_extra_kwargs={
                    "new_pseudos": abinit_phonon_ecut_convergence_pseudos},
                copy_phonons_extra_kwargs={
                    "new_pseudos": abinit_phonon_ecut_convergence_pseudos},
                **kwargs)

    async def _set_relaxation_done(self, workflow):
        dirlist = await aiofiles.os.listdir(abinit_relaxation_example)
        coro = []
        for icalc, calcdir in enumerate(dirlist):
            new_parents = []
            if calcdir == os.path.basename(
                    workflow.relaxation_sequencer._get_relax_cell_workdir()):
                # need to link the other calc
                basename = dirlist[0] if icalc == 1 else dirlist[1]
                new_parents = [
                        os.path.join(
                            workflow.relaxation_sequencer.scf_workdir,
                            basename)]
            coro.append(copy_calculation(
                    os.path.join(abinit_relaxation_example, calcdir),
                    os.path.join(
                        workflow.relaxation_sequencer.scf_workdir, calcdir),
                    new_pseudos=abinit_gs_pseudos,
                    new_parents=new_parents))
        await asyncio.gather(*coro)
