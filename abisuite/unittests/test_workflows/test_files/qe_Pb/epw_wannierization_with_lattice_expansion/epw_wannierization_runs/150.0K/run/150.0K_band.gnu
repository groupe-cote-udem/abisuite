set style data dots
set nokey
set xrange [0: 6.30117]
set yrange [ -1.94159 : 18.56191]
set arrow from  1.26070,  -1.94159 to  1.26070,  18.56191 nohead
set arrow from  1.89104,  -1.94159 to  1.89104,  18.56191 nohead
set arrow from  2.78249,  -1.94159 to  2.78249,  18.56191 nohead
set arrow from  3.55450,  -1.94159 to  3.55450,  18.56191 nohead
set arrow from  4.89167,  -1.94159 to  4.89167,  18.56191 nohead
set xtics ("G"  0.00000,"X"  1.26070,"W"  1.89104,"L"  2.78249,"K"  3.55450,"G"  4.89167,"W"  6.30117)
 plot "150.0K_band.dat"
