#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points8_8_8/k_points8_8_8.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points8_8_8/k_points8_8_8.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.1/k_points8_8_8/k_points8_8_8.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
