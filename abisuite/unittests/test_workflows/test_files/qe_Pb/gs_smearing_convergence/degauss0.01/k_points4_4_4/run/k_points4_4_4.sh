#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.01/k_points4_4_4/k_points4_4_4.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.01/k_points4_4_4/k_points4_4_4.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_smearing_convergence/degauss0.01/k_points4_4_4/k_points4_4_4.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
