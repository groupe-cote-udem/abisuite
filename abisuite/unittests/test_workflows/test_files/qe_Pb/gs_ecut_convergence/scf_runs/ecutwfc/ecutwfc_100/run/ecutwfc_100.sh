#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_100/ecutwfc_100.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_100/ecutwfc_100.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_100/ecutwfc_100.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
