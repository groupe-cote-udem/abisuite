#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_10/ecutwfc_10.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_10/ecutwfc_10.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/gs_ecut_convergence/scf_runs/ecutwfc/ecutwfc_10/ecutwfc_10.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
