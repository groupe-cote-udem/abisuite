#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/dos.x"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_1/dos_run/dos_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_1/dos_run/dos_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_1/dos_run/dos_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
