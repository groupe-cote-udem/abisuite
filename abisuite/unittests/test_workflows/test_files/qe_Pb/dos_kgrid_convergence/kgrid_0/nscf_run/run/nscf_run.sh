#!/bin/bash

MPIRUN="mpirun -np 6"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 6"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_0/nscf_run/nscf_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_0/nscf_run/nscf_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/dos_kgrid_convergence/kgrid_0/nscf_run/nscf_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
