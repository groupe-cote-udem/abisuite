#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/scf_runs/scf_vol+0percent/scf_vol+0percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
