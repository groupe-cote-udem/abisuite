#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/kpoints.x"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/fine_qpoint_grid/fine_qpoint_grid.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/fine_qpoint_grid/fine_qpoint_grid.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/fine_qpoint_grid/fine_qpoint_grid.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
