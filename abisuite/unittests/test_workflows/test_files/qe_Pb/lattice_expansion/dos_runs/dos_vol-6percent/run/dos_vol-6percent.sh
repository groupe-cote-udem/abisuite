#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/dos.x"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/dos_runs/dos_vol-6percent/dos_vol-6percent.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/dos_runs/dos_vol-6percent/dos_vol-6percent.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/dos_runs/dos_vol-6percent/dos_vol-6percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
