#!/bin/bash

MPIRUN="mpirun -np 6"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x -npool 6"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/nscf_runs/nscf_vol-4percent/nscf_vol-4percent.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/nscf_runs/nscf_vol-4percent/nscf_vol-4percent.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/nscf_runs/nscf_vol-4percent/nscf_vol-4percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
