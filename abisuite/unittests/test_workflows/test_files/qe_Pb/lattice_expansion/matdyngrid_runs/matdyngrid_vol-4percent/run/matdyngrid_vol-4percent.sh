#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/matdyn.x"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyngrid_runs/matdyngrid_vol-4percent/matdyngrid_vol-4percent.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyngrid_runs/matdyngrid_vol-4percent/matdyngrid_vol-4percent.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/lattice_expansion/matdyngrid_runs/matdyngrid_vol-4percent/matdyngrid_vol-4percent.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
