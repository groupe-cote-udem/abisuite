#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/epw.x -npool 4"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
