#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/q-e/build/develop/2023-03-07/bin/pw.x"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/nscf_run/nscf_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/nscf_run/nscf_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_4x4x4/nscf_run/nscf_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
