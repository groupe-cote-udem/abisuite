#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/q-e/bin/epw.x -npool 4"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_wannierization/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/ibte_coarse_kgrid_convergence/coarse_kgrid_6x6x6/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/qe_Pb/epw_wannierization/epw_dispersion_interpolation_run/epw_dispersion_interpolation_run.stderr


$MPIRUN $EXECUTABLE -input $INPUT > $LOG 2> $STDERR
