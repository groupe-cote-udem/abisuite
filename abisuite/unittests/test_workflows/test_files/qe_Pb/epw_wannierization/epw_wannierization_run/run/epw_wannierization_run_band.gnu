set style data dots
set nokey
set xrange [0: 6.39389]
set yrange [ -1.57369 : 19.57817]
set arrow from  1.27925,  -1.57369 to  1.27925,  19.57817 nohead
set arrow from  1.91887,  -1.57369 to  1.91887,  19.57817 nohead
set arrow from  2.82343,  -1.57369 to  2.82343,  19.57817 nohead
set arrow from  3.60681,  -1.57369 to  3.60681,  19.57817 nohead
set arrow from  4.96365,  -1.57369 to  4.96365,  19.57817 nohead
set xtics ("G"  0.00000,"X"  1.27925,"W"  1.91887,"L"  2.82343,"K"  3.60681,"G"  4.96365,"W"  6.39389)
 plot "epw_wannierization_run_band.dat"
