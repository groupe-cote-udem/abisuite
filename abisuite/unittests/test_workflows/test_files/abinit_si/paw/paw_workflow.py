from abisuite import AbinitWorkflow


workflow = AbinitWorkflow(
        gs=False,
        band_structure=False,
        band_structure_convergence=False,
        optic=False,
        relaxation=True,
        gs_ecut_convergence=True,
        gs_kgrid_convergence=True,
        phonon_ecut_convergence=True,
        pseudos=["../../pseudos/Si.xml"],  # PAW
        )
workflow.set_gs_ecut_convergence(
        root_workdir="gs_ecut_convergence",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": [14],
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "pawecutdg": 30.0,
                "ngkpt": [2, 2, 2],
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 50,
                "tolvrs": 1.0e-10,
                "diemac": 12.0,
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 20, 30],
        scf_pawecutdg=[10, 20, 30, 50],
        scf_pawecutdg_convergence_criterion=10,
        scf_convergence_criterion=10)
workflow.set_gs_kgrid_convergence(
        scf_workdir="gs_kgrid_convergence",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": [14],
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "toldfe": 1.0e-6,
                "diemac": 12.0,
                },
        scf_kgrids_input_variable_name="ngkpt",
        scf_kgrids=[[2, 2, 2], [4, 4, 4], [6, 6, 6], [8, 8, 8]],
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_convergence_criterion=10,
        use_gs_converged_ecut=True)

workflow.set_relaxation(
        root_workdir="relaxation",
        scf_input_variables={
                "acell": [10.18] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": [14],
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                "kptopt": 1,
                "nshiftk": 4,
                "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                           [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                "nstep": 10,
                "tolvrs": 1.0e-10,
                "diemac": 12.0,
                "optcell": 1,  # only modify acell
                "ionmov": 22,
                "ntime": 20,
                "tolmxf": 5e-04,
                "ecutsm": 0.5,
                "dilatmx": 1.1,
                },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4"},
        relax_atoms=False,
        relax_cell=True,
        use_gs_converged_ecut=True,
        use_gs_converged_kgrid=True,
        )

workflow.set_phonon_ecut_convergence(
        root_workdir="phonon_ecut_convergence",
        compute_electric_field_response=True,
        ddk_input_variables={
            "rfatpol": [1, 2],
            "tolwfr": 1e-22,
            "nstep": 25,
            "diemac": 9.0,
            "rfdir": [1, 1, 1],
            "pawxcdev": 0,  # needed for dfpt + GGA
            },
        ddk_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        phonons_input_variables={
            "rfatpol": [1, 2],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "diemac": 9.0,
            "nstep": 25,
            "pawxcdev": 0,  # needed for dfpt + GGA
            },
        scf_input_variables={
            "kptopt": 1,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
            "nstep": 10,
            "toldfe": 1.0e-6,
            "diemac": 12.0,
            "pawecutdg": 30,
            "pawxcdev": 0,  # needed for dfpt + GGA
            },
        scf_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        scf_ecuts=[10, 20, 30],
        scf_pawecutdg=[10, 20, 30, 50],
        phonons_convergence_criterion=10,  # cm^-1
        phonons_pawecutdg_convergence_criterion=10,  # cm^-1
        phonons_qpt=[0, 0, 0],  # test convergence at Gamma
        phonons_calculation_parameters={
            "mpi_command": "mpirun -np 4",
            },
        use_relaxed_geometry=True,
        use_gs_converged_kgrid=True,
        )
# workflow.set_gs(
#         scf_workdir="gs_run",
#         scf_input_variables={
#                 "kptopt": 1,
#                 "nshiftk": 4,
#                 "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
#                            [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
#                 "nstep": 10,
#                 "toldfe": 1.0e-6,
#                 "diemac": 12.0,
#                 },
#         scf_calculation_parameters={
#             "mpi_command": "mpirun -np 4"},
#         use_phonon_converged_ecut=True,
#         use_gs_converged_kgrid=True,
#         use_relaxed_geometry=True,
#         )
# workflow.set_band_structure(
#         root_workdir="band_structure",
#         band_structure_input_variables={
#             "iscf": -2,
#             "nband": 8,
#             "tolwfr": 1e-12,
#             "diemac": 12.0,
#             },
#         band_structure_calculation_parameters={
#             "mpi_command": "mpirun -np 4",
#             },
#         band_structure_kpoint_path=[
#             {"L": [0.5, 0.0, 0.0]},
#             {r"$\Gamma$": [0.0, 0.0, 0.0]},
#             {"X": [0.0, 0.5, 0.5]},
#             {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
#         band_structure_kpoint_path_density=20,
#         fatbands=True,
#         use_gs=True,
#         )
# workflow.set_band_structure_convergence(
#         root_workdir="band_structure_ecut_convergence",
#         scf_specific_input_variables=[
# {"ecut": ecut} for ecut in [10, 20, 30]],
#         scf_calculation_parameters={
#             "mpi_command": "mpirun -np 4"},
#         plot_calculation_parameters={
# "show_bandgap": True, "adjust_axis": False},
#         use_gs_converged_ecut=True,
#         use_gs_converged_kgrid=True,
#         use_relaxed_geometry=True,
#         )
# workflow.set_optic(
#         root_workdir="optic",
#         experimental_bandgap=1.14,
#         compute_non_linear_optical_response=False,
#         nscf_input_variables={
#             "nstep": 20,
#             "tolwfr": 1e-14, "ngkpt": [6, 6, 6], "nbdbuf": 2, "nband": 10},
#         nscf_calculation_parameters={
#             "mpi_command": "mpirun -np 4", "queuing_system": "local"},
#         nscffbz_calculation_parameters={
#             "mpi_command": "mpirun -np 4", "queuing_system": "local"},
#         ddk_input_variables={"rfdir": [1, 1, 1]},
#         ddk_calculation_parameters={
#             "mpi_command": "mpirun -np 4", "queuing_system": "local"},
#         optic_input_variables={
#             "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
#             "num_nonlin_comp": 0, "num_nonlin2_comp": 0, "num_linel_comp": 0,
#             "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
#             "tolerance": 1e-3},
#         optic_calculation_parameters={
#             "mpi_command": "mpirun -np 4", "queuing_system": "local"},
#         use_gs=True,
#         )
workflow.run()
