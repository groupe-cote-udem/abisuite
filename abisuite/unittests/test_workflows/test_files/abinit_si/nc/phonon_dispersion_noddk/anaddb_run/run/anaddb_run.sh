#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/abinit/build/develop/2023-02-08/src/98_main/anaddb"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/anaddb_run/run/anaddb_run.files
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/anaddb_run/anaddb_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/anaddb_run/anaddb_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
