#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/abinit/build/develop/2023-02-08/src/98_main/abinit"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q3/ph_q3.abi
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q3/ph_q3.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q3/ph_q3.stderr


$MPIRUN $EXECUTABLE $INPUT > $LOG 2> $STDERR
