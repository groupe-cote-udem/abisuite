#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/abinit/build/develop/2023-02-08/src/98_main/abinit"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q1/ph_q1.abi
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q1/ph_q1.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/phonons_runs/ph_q1/ph_q1.stderr


$MPIRUN $EXECUTABLE $INPUT > $LOG 2> $STDERR
