#!/bin/bash

MPIRUN=""
EXECUTABLE="/home/felix/Workspace/abinit/build/develop/2023-02-08/src/98_main/mrgddb"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/mrgddb_run/mrgddb_run.in
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/mrgddb_run/mrgddb_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_noddk/mrgddb_run/mrgddb_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
