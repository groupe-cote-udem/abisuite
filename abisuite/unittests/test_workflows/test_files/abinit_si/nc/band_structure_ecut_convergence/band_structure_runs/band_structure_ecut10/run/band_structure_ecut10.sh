#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-16/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/band_structure_ecut_convergence/band_structure_runs/band_structure_ecut10/run/band_structure_ecut10.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/band_structure_ecut_convergence/band_structure_runs/band_structure_ecut10/band_structure_ecut10.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/band_structure_ecut_convergence/band_structure_runs/band_structure_ecut10/band_structure_ecut10.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
