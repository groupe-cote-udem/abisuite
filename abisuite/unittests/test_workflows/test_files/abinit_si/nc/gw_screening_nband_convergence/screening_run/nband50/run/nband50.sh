#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband50/run/nband50.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband50/nband50.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband50/nband50.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
