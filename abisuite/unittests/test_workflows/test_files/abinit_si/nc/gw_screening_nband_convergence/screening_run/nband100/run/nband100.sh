#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband100/run/nband100.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband100/nband100.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_screening_nband_convergence/screening_run/nband100/nband100.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
