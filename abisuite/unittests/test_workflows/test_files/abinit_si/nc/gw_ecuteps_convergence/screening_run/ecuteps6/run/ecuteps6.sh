#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/screening_run/ecuteps6/run/ecuteps6.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/screening_run/ecuteps6/ecuteps6.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/screening_run/ecuteps6/ecuteps6.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
