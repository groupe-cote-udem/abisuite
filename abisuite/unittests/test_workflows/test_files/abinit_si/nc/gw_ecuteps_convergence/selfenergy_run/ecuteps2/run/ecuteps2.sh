#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/selfenergy_run/ecuteps2/run/ecuteps2.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/selfenergy_run/ecuteps2/ecuteps2.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gw_ecuteps_convergence/selfenergy_run/ecuteps2/ecuteps2.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
