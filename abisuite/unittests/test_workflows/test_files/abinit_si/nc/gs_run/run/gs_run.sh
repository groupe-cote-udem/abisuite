#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/felix/Workspace/abinit/build/develop/2023-02-08/src/98_main/abinit"
INPUT=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gs_run/gs_run.abi
LOG=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gs_run/gs_run.log
STDERR=/home/felix/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/gs_run/gs_run.stderr


$MPIRUN $EXECUTABLE $INPUT > $LOG 2> $STDERR
