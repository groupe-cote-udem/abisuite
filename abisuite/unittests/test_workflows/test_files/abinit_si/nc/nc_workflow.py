import asyncio

from abisuite.constants import EV_TO_HA
from abisuite.workflows import AbinitWorkflow


async def main():
    """Main script."""
    workflow = AbinitWorkflow(
            gs=True,
            band_structure=False,
            band_structure_convergence=False,
            converged_bse_nband={"bs_loband": 2, "nband": 7},
            converged_bse_ecuteps={"ecuteps": 3.0},
            converged_bse_kgrid={"ngkpt": [4, 4, 4]},
            dos=False,
            optic=False,
            optic_nscf_convergence=False,
            relaxation=True,
            gs_ecut_convergence=True,
            gs_kgrid_convergence=True,
            gw_self_energy_nband_convergence=False,
            gw_screening_nband_convergence=False,
            gw_ecuteps_convergence=False,
            gw_kgrid_convergence=False,
            gw=False,
            bse_nband_convergence=False,
            bse_ecuteps_convergence=False,
            bse_kgrid_convergence=False,
            bse=False,
            phonon_ecut_convergence=False,
            converged_phonon_ecut={"ecut": 10},
            phonon_kgrid_convergence=False,
            phonon_dispersion=True,
            phonon_dispersion_qgrid_convergence=False,
            )

    await workflow.set_gs_ecut_convergence(
            root_workdir="ecut_convergence",
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "rprim": [
                        [0.0, 0.5, 0.5], [0.5, 0.0, 0.5],
                        [0.5, 0.5, 0.0]],
                    "ntypat": 1,
                    "znucl": 14,
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "ngkpt": [2, 2, 2],
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 50,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    "pp_dirpath": "",
                    "pseudos": [
                        "/home/fgoudreault/Workspace/abisuite/abisuite/unittes"
                        "ts/test_workflows/test_files/pseudos/Si.psp8"],
                    },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            scf_ecuts=[10, 20, 30],
            scf_convergence_criterion=1,
            plot_calculation_parameters={"show": False},
            )
    await workflow.set_gs_kgrid_convergence(
            root_workdir="kgrid_convergence",
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "rprim": [
                        [0.0, 0.5, 0.5], [0.5, 0.0, 0.5],
                        [0.5, 0.5, 0.0]],
                    "ntypat": 1,
                    "znucl": 14,
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 10,
                    "toldfe": 1.0e-6,
                    "diemac": 12.0,
                    "pp_dirpath": "",
                    "pseudos": [
                        "/home/fgoudreault/Workspace/abisuite/abisuite/unittes"
                        "ts/test_workflows/test_files/pseudos/Si.psp8"],
                    },
            scf_kgrids_input_variable_name="ngkpt",
            scf_kgrids=[[2, 2, 2], [4, 4, 4], [6, 6, 6], [8, 8, 8]],
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            scf_convergence_criterion=1,
            use_gs_converged_ecut=True,
            plot_calculation_parameters={"show": False},
            )

    await workflow.set_relaxation(
            root_workdir="relax_run",
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "rprim": [
                        [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ntypat": 1,
                    "znucl": 14,
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 10,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    "optcell": 1,  # only modify acell
                    "ionmov": 22,
                    "ntime": 20,
                    "tolmxf": 5e-04,
                    "ecutsm": 0.5,
                    "dilatmx": 1.1,
                    "pp_dirpath": "",
                    "pseudos": [
                        "/home/felix/Workspace/abisuite/abisuite/unittests/"
                        "test_workflows/test_files/pseudos/Si.psp8"],
                    },
            relax_atoms=True,
            relax_cell=True,
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4"},
            use_gs_converged_ecut=True,
            use_gs_converged_kgrid=True,
            )
    await workflow.set_phonon_ecut_convergence(
            root_workdir="phonon_ecut_convergence",
            compute_electric_field_response=True,
            ddk_input_variables={
                "rfatpol": [1, 2],
                "tolwfr": 1e-22,
                "nstep": 25,
                "diemac": 9.0,
                "rfdir": [1, 1, 1],
                },
            ddk_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            phonons_input_variables={
                "rfatpol": [1, 2],
                "rfdir": [1, 1, 1],
                "tolvrs": 1e-8,
                "diemac": 9.0,
                "nstep": 25,
                },
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "rprim": [
                        [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ntypat": 1,
                    "znucl": 14,
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "nshiftk": 4,
                    "kptrlatt": [[3, 3, -3], [-3, 3, -3], [-3, 3, 3]],
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 10,
                    "toldfe": 1.0e-6,
                    "diemac": 12.0,
                    "pp_dirpath": "",
                    "pseudos": [
                        "/home/felix/Workspace/abisuite/abisuite/unittests/"
                        "test_workflows/test_files/pseudos/Si.psp8"],
                    },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            scf_ecuts=[10, 20, 30],
            phonons_convergence_criterion=10,  # cm^-1
            phonons_qpt=[0, 0, 0],  # test convergence at Gamma
            phonons_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={"show": False},
            )
    await workflow.set_phonon_kgrid_convergence(
            # root_workdir="phonon_kgrid_convergence",
            root_workdir="phonon_kgrid_convergence_no_ddk",
            compute_electric_field_response=False,
            # compute_electric_field_response=True,
            ddk_input_variables={
                "rfatpol": [1, 2],
                "tolwfr": 1e-22,
                "nstep": 25,
                "diemac": 9.0,
                "rfdir": [1, 1, 1],
                },
            ddk_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            phonons_input_variables={
                "rfatpol": [1, 2],
                "rfdir": [1, 1, 1],
                "tolvrs": 1e-8,
                "diemac": 9.0,
                "nstep": 25,
                },
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "rprim": [
                        [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                    "ntypat": 1,
                    "znucl": 14,
                    "natom": 2,
                    "typat": [1, 1],
                    "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
                    "kptopt": 1,
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 10,
                    "toldfe": 1.0e-6,
                    "diemac": 12.0,
                    },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            scf_kgrids=[[2, 2, 2], [4, 4, 4]],
            scf_kgrids_input_variable_name="ngkpt",
            phonons_convergence_criterion=10,  # cm^-1
            # phonons_qpt=[0, 0, 0],  # test convergence at Gamma
            phonons_qpt=[0, 0, 0.5],
            phonons_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={"show": True},
            use_phonon_converged_ecut=True,
            )

    await workflow.set_gs(
            root_workdir="gs_run",
            scf_input_variables={
                    "kptopt": 1,
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 10,
                    "toldfe": 1.0e-6,
                    "diemac": 12.0,
                    "pp_dirpath": "",
                    "pseudos": [
                        "/home/felix/Workspace/abisuite/abisuite/unittests"
                        "/test_workflows/test_files/pseudos/Si.psp8"],
                    },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4"},
            use_phonon_converged_ecut=True,
            use_gs_converged_kgrid=True,
            use_relaxed_geometry=True,
            )
    await workflow.set_band_structure(
            root_workdir="band_structure",
            band_structure_input_variables={
                "iscf": -2,
                "nband": 8,
                "diemac": 12.0,
                "tolwfr": 1e-12,
                "autoparal": 1,
                },
            band_structure_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            band_structure_kpoint_path=[
                {"L": [0.5, 0.0, 0.0]},
                {r"$\Gamma$": [0.0, 0.0, 0.0]},
                {"X": [0.0, 0.5, 0.5]},
                {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
            band_structure_kpoint_path_density=20,
            fatbands=True,
            use_gs=True,
            plot_calculation_parameters={"show": False},
            )
    await workflow.set_gw_self_energy_nband_convergence(
            root_workdir="gw_self_energy_nband_convergence",
            use_gs=True,
            nscf_input_variables={
                "nband": 180,
                "tolwfr": 1e-12,
                "nbdbuf": 10,
                "ngkpt": [4, 4, 4],
                "nshiftk": 4,
                "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                           [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "nband": 50,
                "ecuteps": 6.0,
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_nbands=[25, 50, 100, 150, 180],
            selfenergy_nband_convergence_criterion=3,
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_gw_screening_nband_convergence(
            root_workdir="gw_screening_nband_convergence",
            use_gs=True,
            use_nscf_from="gw_self_energy_nband_convergence",
            screening_input_variables={
                "nband": 50,
                "ecuteps": 6.0,
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_nbands=[25, 50, 100, 150, 180],
            use_converged_gw_self_energy_nband=True,
            screening_nband_convergence_criterion=5,
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_gw_ecuteps_convergence(
            root_workdir="gw_ecuteps_convergence",
            use_gs=True,
            use_nscf_from="gw_self_energy_nband_convergence",
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_ecuteps_convergence_criterion=30,
            screening_ecutepss=[2, 4, 6, 8],
            use_converged_gw_self_energy_nband=True,
            use_converged_gw_screening_nband=True,
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_gw_kgrid_convergence(
            root_workdir="gw_ngkpt_convergence",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-10,
                "nshiftk": 4,
                "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                           [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ecut": 8.0,
                "diemac": 12.0,
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 100,
                "tolwfr": 1e-12,
                "nbdbuf": 10,
                "nshiftk": 4,
                "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                           [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            scf_ngkpt_convergence_criterion=5,
            scf_kgrids=[[x, x, x] for x in [2, 4, 8]],
            use_converged_gw_self_energy_nband=True,
            use_converged_gw_screening_nband=True,
            use_converged_gw_ecuteps=True,
            use_relaxed_geometry=True,
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_gw(
            root_workdir="gw",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-10,
                "nshiftk": 4,
                "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                           [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ecut": 8.0,
                "diemac": 12.0,
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 120,
                "tolwfr": 1e-12,
                "nbdbuf": 20,
                "nshiftk": 4,
                "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                           [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            use_converged_gw_self_energy_nband=True,
            use_converged_gw_screening_nband=True,
            use_converged_gw_ecuteps=True,
            use_converged_gw_ngkpt=True,
            use_relaxed_geometry=True,
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
    await workflow.set_bse_nband_convergence(
            root_workdir="bse_nband_convergence",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "ngkpt": [4, 4, 4],
                "chksymbreak": 0,
                "nshiftk": 1,
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            use_converged_gw_screening_nband=True,
            use_converged_gw_ecuteps=True,
            use_converged_gw_ngkpt=True,
            use_relaxed_geometry=True,
            bse_lobands=[2, 3, 2],
            bse_nbands=[8, 6, 7],
            bse_input_variables={
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecutwfn": 12.0,
                "ecuteps": 2.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_bse_ecuteps_convergence(
            root_workdir="bse_ecuteps_convergence",
            use_scf_from="bse_nband_convergence",
            use_nscf_from="bse_nband_convergence",
            use_nscfnosym_from="bse_nband_convergence",
            use_screening_from="bse_nband_convergence",
            use_converged_bse_nband=True,
            bse_ecutepss=[1, 2, 3, 4],
            bse_input_variables={
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_bse_kgrid_convergence(
            root_workdir="bse_ngkpt_convergence",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            use_converged_gw_screening_nband=True,
            use_converged_gw_ecuteps=True,
            use_converged_bse_ecuteps=True,
            use_converged_bse_nband=True,
            use_relaxed_geometry=True,
            scf_kgrids=[[x] * 3 for x in [4, 5, 6]],
            bse_input_variables={
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_bse(
            root_workdir="bse",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-8,
                "prtden": 1,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "ecut": 12.0,
                "diemac": 12.0,
                "nstep": 50,
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 105,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "iscf": -2,
                "nshiftk": 1,
                "shiftk": [[0.0, 0.0, 0.0]],
                "istwfk": "*1",
                "diemac": 12.0,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscfnosym_input_variables={
                "nband": 15,
                "nbdbuf": 5,
                "tolwfr": 1e-8,
                "chksymbreak": 0,
                "nshiftk": 1,
                "shiftk": [[0.11, 0.21, 0.31]],
                "istwfk": "*1",
                "ecut": 8.0,
                "diemac": 12.0,
                },
            nscfnosym_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "gwpara": 2,
                "inclvkb": 2,
                "awtr": 1,
                "symchi": 1,
                "ecutwfn": 12.0,
                "nfreqre": 1,
                "nfreqim": 90,
                "istwfk": "*1",
                "diemac": 12.0,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            use_converged_gw_screening_nband=True,
            use_converged_gw_ecuteps=True,
            use_converged_bse_ecuteps=True,
            use_converged_bse_nband=True,
            use_relaxed_geometry=True,
            use_converged_bse_ngkpt=True,
            bse_input_variables={
                "bs_calctype": 1,
                "mbpt_sciss": 0.8 * EV_TO_HA,
                "bs_exchange_term": 1,
                "bs_coulomb_term": 11,
                "bs_coupling": 0,
                "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
                "bs_algorithm": 2,
                "bs_haydock_niter": 200,
                "bs_haydock_tol": [0.05, 0],
                "zcut": 0.15 * EV_TO_HA,
                "kptopt": 1,
                "chksymbreak": 0,
                "ecutwfn": 12.0,
                "inclvkb": 2,
                "gw_icutcoul": 3,
                },
            bse_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={
                "show": False,
                },
            )
    await workflow.set_dos(
            root_workdir="dos",
            use_gs=True,
            use_gs_converged_ecut_for_dos=True,
            dos_fine_kpoint_grid_variables={
                "ngkpt": [10, 10, 10],
                "nshiftk": 1,
                # don't shift grid (prefered according to abinit docs)
                "shiftk": [[0.0, 0.0, 0.0]],
                "nband": 8,
                },
            dos_input_variables={
                "iscf": -3,
                "prtdos": 2,
                "tolwfr": 1e-12,
                "autoparal": 1,
                },
            dos_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            plot_calculation_parameters={"show": False},
            )

    await workflow.set_band_structure_convergence(
            root_workdir="band_structure_ecut_convergence",
            scf_input_variables={
                    "acell": [10.18] * 3,
                    "kptopt": 1,
                    "nshiftk": 4,
                    "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                               [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
                    "nstep": 50,
                    "tolvrs": 1.0e-10,
                    "diemac": 12.0,
                    },
            scf_specific_input_variables=[{"ecut": ecut} for ecut in [10, 30]],
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4"},
            plot_calculation_parameters={
                "show_bandgap": True, "adjust_axis": False, "show": False},
            band_structure_kpoint_path=[
                {"L": [0.5, 0.0, 0.0]},
                {r"$\Gamma$": [0.0, 0.0, 0.0]},
                {"X": [0.0, 0.5, 0.5]},
                {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
            band_structure_kpoint_path_density=20,
            band_structure_input_variables={
                "iscf": -2,
                "nband": 8,
                "diemac": 12.0,
                "tolwfr": 1e-12,
                "autoparal": 1,
                },
            band_structure_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            use_gs_converged_ecut=True,
            use_gs_converged_kgrid=True,
            use_relaxed_geometry=True,
            )
    await workflow.set_optic(
            root_workdir="optic",
            experimental_bandgap=1.14,
            compute_non_linear_optical_response=False,
            nscf_input_variables={
                "nstep": 20,
                "tolwfr": 1e-14, "ngkpt": [6, 6, 6], "nbdbuf": 2, "nband": 10},
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            nscffbz_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            ddk_input_variables={"rfdir": [1, 1, 1]},
            ddk_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            optic_input_variables={
                "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
                "num_nonlin_comp": 0, "num_nonlin2_comp": 0,
                "num_linel_comp": 0,
                "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
                "tolerance": 1e-3},
            optic_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            use_gs=True,
            plot_calculation_parameters={"show": False},
            )
    await workflow.set_optic_nscf_convergence(
            root_workdir="optic_nscf_convergence",
            experimental_bandgap=1.14,
            compute_non_linear_optical_response=False,
            nscf_input_variables={
                "nstep": 20,
                "tolwfr": 1e-14, "nbdbuf": 2, "nband": 10},
            nscf_kpoint_grids=[[x, x, x] for x in [6, 8]],
            nscf_kpoint_grids_input_variable_name="ngkpt",
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            nscffbz_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            ddk_input_variables={"rfdir": [1, 1, 1]},
            ddk_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            optic_input_variables={
                "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
                "num_nonlin_comp": 0, "num_nonlin2_comp": 0,
                "num_linel_comp": 0,
                "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
                "tolerance": 1e-3},
            optic_calculation_parameters={
                "mpi_command": "mpirun -np 4", "queuing_system": "local"},
            use_gs=True,
            plot_calculation_parameters={"show": True},
            )
    await workflow.set_phonon_dispersion_qgrid_convergence(
            root_workdir="phonon_dispersion_qgrid_convergence",
            use_gs=True,
            compute_electric_field_response=True,
            phonons_input_variables={
                "rfatpol": [1, 2],
                "tolvrs": 1e-8,
                "diemac": 12.0,
                "nstep": 25,
                },
            phonons_qpoint_grids=[[x] * 3 for x in [2, 6]],
            phonons_calculation_parameters={"mpi_command": "mpirun -np 4"},
            ddk_input_variables={
                "rfdir": [1, 1, 1],
                "tolwfr": 1e-16,
                "nstep": 25,
                "diemac": 12.0,
                },
            ddk_calculation_parameters={"mpi_command": "mpirun -np 4"},
            qpoint_path=[
                {"L": [0.5, 0.0, 0.0]},
                {r"$\Gamma$": [0.0, 0.0, 0.0]},
                {"X": [0.0, 0.5, 0.5]},
                {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
            qpoint_path_density=20,
            anaddb_input_variables={
                "ifcflag": 1, "ifcout": 0, "brav": 2, "ngqpt": [6, 6, 6],
                "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                },
            anaddb_calculation_parameters={},
            mrgddb_calculation_parameters={},
            plot_calculation_parameters={"show": False},
            )
    await workflow.set_phonon_dispersion(
            root_workdir="phonon_dispersion_noddk",
            use_gs=True,
            compute_electric_field_response=False,
            phonons_qgrid_generation_input_variable_name="ngkpt",
            phonons_input_variables={
                "rfatpol": [1, 2],
                "tolvrs": 1e-8,
                "diemac": 12.0,
                "nstep": 25,
                },
            phonons_qpoint_grid=[6, 6, 6],
            # phonons_qpoints_split_by_irreps=[7],
            phonons_calculation_parameters={"mpi_command": "mpirun -np 4"},
            ddk_input_variables={
                "rfdir": [1, 1, 1],
                "tolwfr": 1e-16,
                "nstep": 25,
                "diemac": 12.0,
                },
            ddk_calculation_parameters={"mpi_command": "mpirun -np 4"},
            qpoint_path=[
                {"L": [0.5, 0.0, 0.0]},
                {r"$\Gamma$": [0.0, 0.0, 0.0]},
                {"X": [0.0, 0.5, 0.5]},
                {r"$\Gamma_2$": [1.0, 1.0, 1.0]}],
            qpoint_path_density=20,
            anaddb_input_variables={
                "ifcflag": 1, "ifcout": 0, "brav": 2, "ngqpt": [6, 6, 6],
                "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
                "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
                },
            anaddb_calculation_parameters={},
            mrgddb_calculation_parameters={},
            plot_calculation_parameters={"show": False},
            )
    await workflow.run()


if __name__ == "__main__":
    asyncio.run(main())
