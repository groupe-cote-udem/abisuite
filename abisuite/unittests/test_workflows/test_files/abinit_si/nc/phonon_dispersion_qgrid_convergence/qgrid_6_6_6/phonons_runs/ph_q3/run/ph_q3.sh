#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/phonons_runs/ph_q3/run/ph_q3.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/phonons_runs/ph_q3/ph_q3.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/phonon_dispersion_qgrid_convergence/qgrid_6_6_6/phonons_runs/ph_q3/ph_q3.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
