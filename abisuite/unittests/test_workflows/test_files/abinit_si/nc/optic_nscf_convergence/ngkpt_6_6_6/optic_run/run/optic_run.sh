#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/optic"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_6_6_6/optic_run/run/optic_run.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_6_6_6/optic_run/optic_run.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/optic_nscf_convergence/ngkpt_6_6_6/optic_run/optic_run.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
