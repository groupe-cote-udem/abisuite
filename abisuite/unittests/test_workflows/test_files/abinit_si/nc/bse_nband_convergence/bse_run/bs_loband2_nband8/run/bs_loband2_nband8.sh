#!/bin/bash

MPIRUN="mpirun -np 4"
EXECUTABLE="/home/fgoudreault/Workspace/abinit/abinit/build/develop/2021-04-20/src/98_main/abinit"
INPUT=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/bse_nband_convergence/bse_run/bs_loband2_nband8/run/bs_loband2_nband8.files
LOG=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/bse_nband_convergence/bse_run/bs_loband2_nband8/bs_loband2_nband8.log
STDERR=/home/fgoudreault/Workspace/abisuite/abisuite/unittests/test_workflows/test_files/abinit_si/nc/bse_nband_convergence/bse_run/bs_loband2_nband8/bs_loband2_nband8.stderr


$MPIRUN $EXECUTABLE < $INPUT > $LOG 2> $STDERR
