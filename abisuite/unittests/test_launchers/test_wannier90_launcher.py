from .bases import BaseLauncherWithParentsTest
from ..variables_for_tests import (
        QEPWNscfPositiveKptsCalc,
        wannier90_vars,
        )
from ...launchers import (
        Wannier90Launcher,
        )


class TestWannier90Launcher(BaseLauncherWithParentsTest):
    _calctype = "wannier90.x"
    _command_flags = "-pp"
    _input_vars = wannier90_vars.copy()
    _jobname = "test_wannier90_launcher"
    _launcher_class = Wannier90Launcher
    _link_calculation = QEPWNscfPositiveKptsCalc
