import os
import tempfile

import aiofiles.os

import pytest

from .bases import BaseLauncherTest, BaseLauncherTestNoInputVariables
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        AbinitMrgddbCalc,
        abinit_anaddb_vars,
        abinit_gs_input_variables,
        abinit_gs_pseudos,
        abinit_optic_input_variables,
        )
from ...handlers.file_approvers.exceptions import PseudosError
from ...handlers.file_parsers import AbinitOpticInputParser
from ...launchers import (
        AbinitAnaddbLauncher, AbinitLauncher, AbinitMrgddbLauncher,
        AbinitOpticLauncher,
        )
from ...routines import expand_symlink


class BaseAbinitLauncherTest(BaseLauncherTest):
    """Base test case for abinit launchers."""


class TestAbinitLauncher(BaseAbinitLauncherTest):
    _calctype = "abinit"
    _input_vars = abinit_gs_input_variables.copy()
    _jobname = "test_launcher"
    _launcher_class = AbinitLauncher
    _pseudos = abinit_gs_pseudos

    @pytest.fixture(autouse=True)
    async def setup_abinit_launcher(self, setup_launcher):
        await self.launcher.set_pseudos(self._pseudos)

    async def test_raise_pseudos_not_exists(self):
        with pytest.raises((PseudosError, FileNotFoundError)):
            # create new non existant pseudo
            pseudo2 = os.path.expanduser("~/pseudo")
            await self.launcher.set_pseudos([pseudo2])
            self.launcher.queuing_system = "local"
            await self.launcher.write()

    async def test_linking(self, tempdir2):
        # create file to link
        path = os.path.join(tempdir2, "odat_WFK")
        async with aiofiles.open(path, "a") as f:
            await f.write("test")
        await self.launcher.add_input_file(path)
        self.launcher.queuing_system = "local"
        await self.launcher.write()
        link = os.path.join(self.workdir, "input_data",
                            "idat_gs_run_WFK")
        assert await aiofiles.os.path.isfile(path)
        assert await aiofiles.os.path.exists(link)
        assert await aiofiles.os.path.islink(link)


class TestAbinitAnaddbLauncher(BaseAbinitLauncherTest):
    """Test case for the anaddb launcher.
    """
    _calctype = "abinit_anaddb"
    _input_vars = abinit_anaddb_vars.copy()
    _jobname = "test_abinid_anaddb_launcher"
    _launcher_class = AbinitAnaddbLauncher

    @pytest.fixture(autouse=True)
    async def setup_abinit_anaddb_launcher(self, setup_launcher):
        # copy calculation in a temp dir and link it
        new_calc = os.path.join(
                self.tempdir,
                os.path.basename(AbinitMrgddbCalc))
        await copy_calculation(AbinitMrgddbCalc, new_calc)
        await self.launcher.link_calculation(new_calc)


class TestAbinitMrgddbLauncher(BaseLauncherTestNoInputVariables):
    """Test case for the mrgddb launcher.
    """
    _calctype = "abinit_mrgddb"
    _jobname = "test_abinit_mrgddb_launcher"
    _launcher_class = AbinitMrgddbLauncher

    @pytest.fixture(autouse=True)
    async def setup_abinit_mrgddb_launcher(self, setup_launcher_no_input_vars):
        # create temporary DDB files and link them
        self._temp_ddbs = []
        self._ntemp_ddbs = 10
        for i in range(self._ntemp_ddbs):
            temp = tempfile.NamedTemporaryFile(suffix=f"{i}_DDB")
            await self.launcher.add_input_file(temp.name)
            self._temp_ddbs.append(temp)
        yield
        for temp in self._temp_ddbs:
            del temp

    async def test_ddb_links(self):
        assert self.launcher.input_file.nddb == self._ntemp_ddbs
        tempnames = [x.name for x in self._temp_ddbs]
        async for link in self.launcher.input_data_dir:
            assert link.source in tempnames
            assert link.path in self.launcher.input_file.ddb_paths


class TestAbinitOpticLauncher(BaseAbinitLauncherTest):
    _calctype = "optic"
    _input_vars = abinit_optic_input_variables.copy()
    _jobname = "test_abinit_optic_launcher"
    _launcher_class = AbinitOpticLauncher

    async def test_input_links(self):
        self.launcher.queuing_system = "local"
        # need to link the files manually since we link example files
        for varname in ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"):
            if varname != "wfkfile":
                ddk_idx = varname.split("_")[-1]
            else:
                ddk_idx = None
            await self.launcher.add_input_file(
                    self.launcher.input_variables[varname].value,
                    ddk_idx=ddk_idx)
        await self.launcher.write()
        links = await aiofiles.os.listdir(self.launcher.input_data_dir.path)
        if not links:
            raise FileNotFoundError()
        dests = []
        for link in links:
            # check link exists
            path = os.path.join(self.launcher.input_data_dir.path, link)
            dests.append(await expand_symlink(path))
            # os.path.abspath(os.readlink(path)))
            assert await aiofiles.os.path.islink(path)
        # check that all links are good in input file
        path = self.launcher.input_file.path
        parser = await AbinitOpticInputParser.from_file(path)
        await parser.read()
        for name in ("ddkfile_1", "ddkfile_2", "ddkfile_3", "wfkfile"):
            assert os.path.basename(
                    parser.input_variables[name].value) in links
            # FG 2023/05/25 FIXME
            # # check that linked is good
            # assert any([full_abspath(abinit_optic_input_variables[name]) in x
            #             for x in dests]), (
            #                     f"{abinit_optic_input_variables[name]}")
