import os
import unittest

import pytest

from .bases import BaseLauncherTest, BaseLauncherWithParentsTest
from ..variables_for_tests import (
        POSITIVE_KS_NSCF_CALC,
        QEPHCalc,
        QEPWCalc,
        QEQ2RCalc,
        qe_dos_input_variables,
        qe_dynmatx_vars,
        qe_epsilonx_vars, qe_epwx_vars, qe_fsx_vars, qe_ld1x_vars,
        qe_matdynx_vars,
        qe_phx_vars, qe_ppx_vars, qe_projwfcx_vars,
        qe_pwx_vars,
        qe_q2rx_vars,
        # pseudo_dir,
        )
from ...handlers import MetaDataFile
from ...handlers.file_approvers.exceptions import InputFileError
from ...launchers import (
        QEDOSLauncher,
        QEDynmatLauncher,
        QEEPWLauncher,
        QEEpsilonLauncher,
        QEFSLauncher, QELD1Launcher,
        QEMatdynLauncher,
        QEPHLauncher, QEPPLauncher,
        QEPWLauncher,
        QEProjwfcLauncher,
        QEQ2RLauncher
        )


class BaseQELauncherTest(BaseLauncherTest):
    """Class for QE Launchers unittests.
    """
    async def test_recover(self):
        # check that after writing launcher, one can recover it
        # test for input variables and batch file parameters
        await self.launcher.write()
        newlauncher = self._launcher_class(self._jobname)
        # use same workdir for simplicity with comparisions
        await newlauncher.set_workdir(self.workdir)
        await newlauncher.recover_from(self.launcher.workdir)
        assert self.launcher.input_file == newlauncher.input_file
        assert self.launcher.pbs_file == newlauncher.pbs_file, (
                f"{await self.launcher.pbs_file.str()} != "
                f"{await newlauncher.pbs_file.str()}")


class BaseRequiredInputQELauncherTest(
        BaseQELauncherTest, BaseLauncherWithParentsTest):
    """Class that adds tests for Launchers that require some input files.
    """
    async def test_raise_when_no_inputdata(self):
        self.launcher.clear_input_files()
        with pytest.raises(FileNotFoundError):
            await self.launcher.validate()


class TestQEDOSLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "dos.x"
    _input_vars = qe_dos_input_variables.copy()
    _jobname = "test_qedos_launcher"
    _launcher_class = QEDOSLauncher
    _link_calculation = QEPWCalc
    _mpi_command = ""
    _nodes = 1
    _ppn = 1


class TestQEDynmatLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "dynmat.x"
    _input_vars = qe_dynmatx_vars.copy()
    _jobname = "test_qedynmat_launcher"
    _launcher_class = QEDynmatLauncher
    _link_calculation = QEPHCalc


class TestQEEpsilonLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "epsilon.x"
    _input_vars = qe_epsilonx_vars.copy()
    _jobname = "test_qeepsilon_launcher"
    _launcher_class = QEEpsilonLauncher
    _link_calculation = QEPWCalc


class TestQEEPWLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "epw.x"
    _input_vars = qe_epwx_vars.copy()
    _jobname = "test_qeepw_launcher"
    _mpi_command = ""
    _launcher_class = QEEPWLauncher
    _link_calculation = [QEPHCalc, POSITIVE_KS_NSCF_CALC]
    _mpi_command = ""


class TestQEFSLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "fs.x"
    _input_vars = qe_fsx_vars.copy()
    _jobname = "test_qefs_launcher"
    _launcher_class = QEFSLauncher
    _link_calculation = QEPWCalc


class TestQELD1Launcher(BaseQELauncherTest):
    _calctype = "ld1.x"
    _input_vars = qe_ld1x_vars.copy()
    _jobname = "test_qeld1_launcher"
    _launcher_class = QELD1Launcher


class TestQEMatdynLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "matdyn.x"
    _input_vars = qe_matdynx_vars.copy()
    _jobname = "test_matdyn_launcher"
    _launcher_class = QEMatdynLauncher
    _link_calculation = QEQ2RCalc


class TestQEPHLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "ph.x"
    _input_vars = qe_phx_vars.copy()
    _jobname = "test_qeph_launcher"
    _launcher_class = QEPHLauncher
    _link_calculation = QEPWCalc

    def test_recover(self):
        # recover test is done using another class
        pass


# simpler to make a new test class to test recovery
class TestQEPHLauncherRecover(BaseLauncherWithParentsTest):
    _calctype = "ph.x"
    _jobname = "test_qephrecover_launcher"
    _launcher_class = QEPHLauncher
    _link_calculation = [QEPHCalc]  # not PWCalc
    _input_vars = {}
    _allow_linking_errors = True

    @pytest.fixture(autouse=True)
    async def setup_qe_ph_launcher_with_parents(
            self, setup_launcher_with_parents, tempdir3):
        self._recover_from = (await self._copy_calc_to_link(
                self._link_calculation, tempdir3))[0]
        self._linked_calc = self._recover_from
        await self.launcher.recover_from(self._recover_from)
        # hack needed to force executable path
        self.launcher.pbs_file.structure._command = self.command

    def test_linked_calculation(self):
        # not really linking a calculation, just recovering from it
        # i'm using the already existing machinery for this unittest
        pass

    def test_clear_calculations(self):
        # not really linking calculations either
        pass

    def test_linking_against_database(self):
        # same as above
        pass

    def test_recover(self):
        # check that recover has been inserted
        assert self.launcher.input_variables["recover"].value is True
        # check that input variables are the same
        assert self.launcher.input_variables["amass(1)"].value == (
                28.086)  # directly read from corresponding input
        # check prefix is new prefix
        assert self.launcher.input_variables["prefix"].value == (
                self.launcher.jobname)
        # check cmd args are the same
        assert self.launcher.mpi_command_arguments == {"-np": 4}
        assert self.launcher.command_arguments == {"-npool": 4}

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    async def test_clean_workdir(self):
        # skip if not enough proc (e.g. on gitlab)
        await super().test_clean_workdir()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    async def test_raise_when_changing_npool_or_nimage(self):
        # force one mpi process
        self.launcher.command_arguments["-npool"] += 1
        with pytest.raises((ValueError, InputFileError)):
            await self.launcher.validate()
        self.launcher.command_arguments["-npool"] -= 1
        self.launcher.command_arguments["-nimage"] = 2
        with pytest.raises((ValueError, InputFileError)):
            await self.launcher.validate()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    async def test_launcher_write(self):
        # skip if not enough proc (e.g. on gitlab)
        await super().test_launcher_write()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    async def test_link_raise(self):
        await super().test_link_raise()

    async def test_basic_properties(self):
        # just skip this test for now as it is not relevant
        # TODO: try to find a cleaner way to do this...
        #    pass
        self._modules = []
        self.launcher.ppn, self._ppn = None, None
        self.launcher.nodes, self._nodes = None, None
        self.command += " -npool 4"
        self._mpi_command += " -np 4"
        async with await MetaDataFile.from_calculation(
                self._recover_from) as meta:
            ifcls = self._launcher_class._input_file_handler_class
            async with await ifcls.from_meta_data_file(meta) as inputfile:
                await inputfile.read()
                self.input_variables = inputfile.input_variables.todict()
        await super().test_basic_properties()

    @unittest.skipIf(os.cpu_count() < 4, "Not enough cpu for this test.")
    async def test_raise_when_no_variables(self):
        # skip if not enough poc (e.g. on gitlab)
        await super().test_raise_when_no_variables()


class TestQEPPLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "pp.x"
    _input_vars = qe_ppx_vars.copy()
    _jobname = "test_qepp_launcher"
    _launcher_class = QEPPLauncher
    _link_calculation = QEPWCalc


class TestQEProjwfcLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "projwfc.x"
    _input_vars = qe_projwfcx_vars.copy()
    _jobname = "test_qeprojwfc_launcher"
    _launcher_class = QEProjwfcLauncher
    _link_calculation = QEPWCalc


class TestQEPWLauncher(BaseQELauncherTest):
    _calctype = "pw.x"
    _input_vars = qe_pwx_vars.copy()
    _jobname = "test_qepw_launcher"
    _launcher_class = QEPWLauncher


class TestQEQ2RLauncher(BaseRequiredInputQELauncherTest):
    _calctype = "q2r.x"
    _input_vars = qe_q2rx_vars.copy()
    _jobname = "test_q2r_launcher"
    _launcher_class = QEQ2RLauncher
    _link_calculation = QEPHCalc
