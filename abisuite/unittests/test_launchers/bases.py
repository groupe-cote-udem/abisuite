import os
import tempfile
import unittest

import aiofiles
import aiofiles.os

import pytest

from ..routines_for_tests import copy_calculation
from ... import __USER_DATABASE__
from ...databases import DBCalculation
from ...exceptions import DevError
from ...handlers import MetaDataFile, SymLinkFile
from ...routines import is_list_like


class BaseLauncherTestNoInputVariables:
    """Base test class for launchers with no input variables."""

    _calctype = None
    _command_flags = None
    _jobname = None
    _launcher_class = None
    _modules = ["one_module"]
    _mpi_command = "mpirun"
    _nodes = 1
    _ppn = 2

    @pytest.fixture(scope="class")
    async def command_file(self):
        """Fixture for a command file."""
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._calctype) as temp:
            yield temp

    @pytest.fixture(autouse=True)
    async def setup_launcher_no_input_vars(self, command_file, tempdir):
        """Setup fixture for launchers with no input variables."""
        # create temporary abinit script
        self.command_file = command_file
        self.command = self.command_file.name
        if self._command_flags is not None:
            if not self._command_flags.startswith(" "):
                self.command += " " + self._command_flags
            else:
                self.command += self._command_flags
        self.tempdir = tempdir
        self.workdir = os.path.join(self.tempdir, self._jobname)
        self.launcher = self._launcher_class(jobname=self._jobname)
        await self.launcher.set_workdir(self.workdir)
        self.launcher.allow_connection_to_database = False
        self.launcher.command = self.command
        self.launcher.modules = self._modules
        self.launcher.ppn = self._ppn
        self.launcher.nodes = self._nodes
        self.launcher.mpi_command = self._mpi_command
        self.launcher.queuing_system = "local"

    async def test_basic_properties(self):
        """Test the launchers basic properties."""
        # test assigning properties
        # jobname
        assert self.launcher.jobname == self._jobname
        # workdir
        assert self.launcher.workdir == self.workdir
        # modules
        mods = self._modules
        if not isinstance(self._modules, list):
            mods = [self._modules]
        assert self.launcher.modules == mods
        # ppn
        assert self.launcher.ppn == self._ppn, (
                f"{self.launcher.ppn} != {self._ppn}")
        # nodes
        assert self.launcher.nodes == self._nodes
        # command
        assert self.launcher.command == self.command
        # mpi_command
        assert self.launcher.mpi_command == self._mpi_command
        # input data dir
        idd = os.path.join(self.workdir, "input")
        await self.launcher.input_data_dir.set_path(idd)
        assert self.launcher.input_data_dir.path == idd
        # output data dir
        odd = os.path.join(self.workdir, "output")
        await self.launcher.output_data_dir.set_path(odd)
        assert self.launcher.output_data_dir.path == odd
        # run dir
        rd = os.path.join(self.workdir, "run")
        await self.launcher.rundir.set_path(rd)
        assert self.launcher.rundir.path == rd
        # quality of service and its alias
        qos = "qos"
        self.launcher.quality_of_service = qos
        assert self.launcher.quality_of_service == qos
        assert self.launcher.qos == qos
        qos += "new"
        self.launcher.qos = qos
        assert self.launcher.quality_of_service == qos
        assert self.launcher.qos == qos

    async def test_add_input_file(self, tempdir2):
        """Test adding an input file to a launcher."""
        # create input files
        files = ["file1", "file2", "file3"]
        suffixes = ["_EIG", "_WFK", "_DEN"]
        temps = []
        filepaths = []
        for fil, suffix in zip(files, suffixes):
            temps.append(tempfile.NamedTemporaryFile(suffix=suffix,
                                                     prefix=fil,
                                                     dir=tempdir2))
        for temp in temps:
            filepaths.append(temp.name)

        # check files are added one shot
        await self.launcher.add_input_file(filepaths)
        links = []
        for item in await self.launcher.input_data_dir.walk(paths_only=False):
            if isinstance(item, SymLinkFile):
                links.append(item.source)
        sources = self.launcher.meta_data_file.linked_files
        for fil in filepaths:
            # check input file has been added to input data dir and meta data
            assert fil in links
            assert fil in sources
        self.launcher.clear_input_files()
        # check star notation
        star = os.path.join(tempdir2, "file*.dat")
        await self.launcher.add_input_file(star)
        for fil in filepaths:
            assert fil in links
            assert fil in sources

    async def test_link_raise(self):
        """Test that linking a non-existing file raises error."""
        # test that an error is raised when we want to link a non existent file
        with pytest.raises(FileNotFoundError):
            path = os.path.join(self.workdir, "odat_random_file_EIG")
            await self.launcher.add_input_file(path)
            self.launcher.queuing_system = "local"
            await self.launcher.write()

    async def test_clean_workdir(self):
        """Test cleaning the workdir."""
        await self.launcher.write()
        await self.launcher.clean_workdir()
        assert not await aiofiles.os.path.exists(self.launcher.workdir)

    def test_raise_when_wrong_script(self):
        """Test that an error is raised with command file has wrong name."""
        with pytest.raises(ValueError):
            self.launcher.command = "Wrong script name"

    async def test_launcher_write(self):
        """Test writing a launcher."""
        self.launcher.queuing_system = "local"
        await self.launcher.write()
        # check that input file is created in good dir
        assert await aiofiles.os.path.isdir(self.launcher.workdir)
        assert await aiofiles.os.path.isfile(self.launcher.input_file.path)
        assert await aiofiles.os.path.isfile(self.launcher.pbs_file.path)
        assert await aiofiles.os.path.isdir(self.launcher.input_data_dir.path)
        assert await aiofiles.os.path.isdir(self.launcher.rundir.path)
        assert await aiofiles.os.path.isdir(self.launcher.output_data_dir.path)

    @unittest.skipIf(__USER_DATABASE__ is None, "'peewee' is not available.")
    async def test_automatic_database_entry(self, testdb):
        """Test that calculation is added to database."""
        # artificially connect calc dir to unittest database
        self.launcher.calculation_directory.connected_to_database = True
        # we don't really need to validate here
        await self.launcher.write(
                bypass_validation=True, bypass_pbs_validation=True)
        # artificially call add
        self.launcher.calculation_directory.add_to_database()
        # check that calculation has been appended to database
        assert DBCalculation.get_or_none(
                    DBCalculation.calculation == self.launcher.workdir
                    ) is not None


class BaseLauncherTest(BaseLauncherTestNoInputVariables):
    """Test case base class to test Launchers with input variables."""

    _input_vars = None

    @pytest.fixture(autouse=True)
    async def setup_launcher(self, setup_launcher_no_input_vars):
        """Setup fixture for launcher tests."""
        if self._input_vars is None:
            raise DevError(
                f"Need to set '_input_vars' in '{self.__class__}'.")
        self.input_variables = self._input_vars.copy()
        await self.launcher.set_input_variables(self.input_variables)

    async def test_basic_properties(self):
        """Test basic properties."""
        await super().test_basic_properties()
        # also test input variables
        assert self.launcher.input_variables is not None
        for k in self.input_variables:
            assert k in self.launcher.input_variables

    async def test_raise_when_no_variables(self):
        """Test that an error is raised when no input variables supplied."""
        self.launcher.clear_input_variables()
        with pytest.raises(ValueError):
            self.launcher.command = self.command
            await self.launcher.write(force_queuing_system="local")


class BaseLauncherWithParentsTest(BaseLauncherTest):
    """Tests for launchers with parent calculations."""

    _link_calculation = None
    _allow_linking_errors = False

    @pytest.fixture(autouse=True)
    async def setup_launcher_with_parents(self, setup_launcher, tempdir2):
        """Setup fixture for launchers with parent calculations."""
        if self._link_calculation is None:
            raise DevError("Need to set '_link_calculation'.")
        if not is_list_like(self._link_calculation):
            tolink = [self._link_calculation]
        else:
            tolink = self._link_calculation
        self._linked_calc = await self._copy_calc_to_link(tolink, tempdir2)
        for calc_to_link in self._linked_calc:
            try:
                await self.launcher.link_calculation(calc_to_link)
            except Exception:
                if not self._allow_linking_errors:
                    raise

    async def test_clear_calculations(self):
        """Test clearing calculations."""
        # test that clearing calculations doesn't affect workflow.
        # in the sense that everything has been removed and that
        # we can safely readd calculations afterward
        self.launcher.clear_linked_calculations()
        assert self.launcher.meta_data_file.parents == []
        assert self.launcher.meta_data_file.copied_files == []
        assert self.launcher.meta_data_file.linked_files == []
        for calc_to_link in self._linked_calc:
            await self.launcher.link_calculation(calc_to_link)
        await self.launcher.write()

    async def test_linked_calculation(self):
        """Test that children calcl have been linked."""
        # check children have been added in linked calculation
        await self.launcher.write()
        for calc in self._linked_calc:
            async with await MetaDataFile.from_calculation(calc) as meta:
                assert self.launcher.workdir in meta.children
            assert calc in self.launcher.meta_data_file.parents

    @unittest.skipIf(__USER_DATABASE__ is None, "'peewee' not available.")
    async def test_linking_against_database(self, testdb):
        """Test that we can link against a calc stored in the database."""
        # purge calculations
        self.launcher.clear_linked_calculations()
        # clear database just to be sure
        DBCalculation.delete().execute()
        for calc in self._linked_calc:
            obj = DBCalculation.create(calculation=calc)
            # link from database
            await self.launcher.link_calculation(obj.id)
        await self.launcher.write()
        for calc in self._linked_calc:
            async with await MetaDataFile.from_calculation(calc) as meta:
                assert self.launcher.workdir in meta.children
            assert calc in self.launcher.meta_data_file.parents

    async def _copy_calc_to_link(self, calcs, link_tempdir):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        self._link_tempdir = link_tempdir
        newcalcs = []
        for i, calc in enumerate(calcs):
            whereto = os.path.join(self._link_tempdir, "calc" + str(i))
            await copy_calculation(calc, whereto)
            newcalcs.append(whereto)
        return newcalcs
