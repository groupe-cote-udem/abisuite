import asyncio
import os

import pytest

from .variables_for_tests import (
        abinit_gs_input_variables,
        abinit_gs_pseudos,
        )
from ..launchers import AbinitLauncher
from ..status_checkers import StatusTree


here = os.path.dirname(os.path.abspath(__file__))
# remove ecut from input vars
abinit_vars = abinit_gs_input_variables.copy()
abinit_vars.pop("ecut")


class TestStatusTree:
    jobnames = ["ecut5", "ecut10"]

    @pytest.fixture(autouse=True)
    async def setup_status_tree(self, tempdir, temp_abinit_script):
        self.tempdir = tempdir
        self.path = self.tempdir
        # need to mock the abinit script
        self.abinit = temp_abinit_script
        self.launchers = [AbinitLauncher(j) for j in self.jobnames]
        await asyncio.gather(
                *(self._write_launcher(launcher, ecut)
                    for launcher, ecut in zip(self.launchers, [5.0, 10.0])))
        self.status_tree = StatusTree(self.path, loglevel=1)
        await self.status_tree.build_tree()

    async def _write_launcher(self, launcher, ecut):
        await launcher.set_workdir(os.path.join(self.path, f"ecut{ecut}"))
        await launcher.set_pseudos(abinit_gs_pseudos)
        ivars = abinit_vars.copy()
        ivars["ecut"] = ecut
        await launcher.set_input_variables(ivars)
        launcher.command = self.abinit.name
        launcher.queuing_system = "local"
        await launcher.write()

    async def test_status_check(self):
        statuses = self.status_tree.status
        for status in statuses:
            assert not (await status)["calculation_started"]
            assert not (await status)["calculation_finished"]

    async def test_print_status(self):
        # test that printing status does not raise an error
        await self.status_tree.print_status()

    async def test_print_ecut_convergence(self):
        # test that printing the ecut convergence does not raise an error
        assert await self.status_tree.print_attributes(
                "status", "convergence_reached",
                "ecut", "etotal",
                delta="etotal",
                sortby="ecut") is None
        # test that forgetting the sortby, it raises an error
        with pytest.raises(ValueError):
            await self.status_tree.print_attributes(
                    "status", "convergence_reached",
                    "ecut", "etotal",
                    delta="etotal")
