import os

import aioshutil

from .bases import BasePostProcTest
from ..variables_for_tests import (
        QEPWBandCalc, QEPWBandCalclog,
        QEPWCalc)
from ...handlers import MetaDataFile, PBSFile
from ...post_processors import BandStructure


class TestBandStructure(BasePostProcTest):
    """Test case for the Band Structure post processor object.
    """
    _post_proc_class = BandStructure
    _from_file = QEPWBandCalclog
    _from_calc = QEPWBandCalc

    async def test_from_calculation(self):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        # need to copy PWCalc (GS calculation)
        pwcalc = await aioshutil.copytree(
                QEPWCalc, os.path.join(self._link_tempdir, "PWCalc"),
                symlinks=True)
        # reset that meta data file
        async with await MetaDataFile.from_calculation(pwcalc) as meta:
            meta.calc_workdir = pwcalc
        # recast PBS file of that new dir
        async with await PBSFile.from_calculation(pwcalc, loglevel=1) as pbs:
            log = os.path.basename(pbs.log_path)
            pbs.log_path = os.path.join(self._link_tempdir,
                                        log)
        calc = await aioshutil.copytree(
                self._from_calc, self._link_tempdir_path, symlinks=True)
        # change meta file there
        async with await MetaDataFile.from_calculation(
                calc, loglevel=1) as meta:
            # rename all files from the temporary root
            meta.calc_workdir = calc
            # change path of parent to a the actual path
            meta.parents[0] = pwcalc
            # force write here to make sure all properties are changed
            # according to new calc workdir.
            await meta.write(overwrite=True)
            # recast pbs file of moved directory
            async with await PBSFile.from_meta_data_file(
                    meta, loglevel=1) as pbs:
                pbs.log_path = os.path.join(meta.calc_workdir,
                                            os.path.basename(pbs.log_path))
        await super().test_from_calculation()

    async def test_from_file(self):
        # just check that it works and doesn't raise an error
        # from a QE bandstructure calculation
        self.postproc = await BandStructure.from_qelog(QEPWBandCalclog)
        self.postproc.get_plot()
