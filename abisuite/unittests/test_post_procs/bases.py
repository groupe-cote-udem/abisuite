import os

import pytest

from ...exceptions import DevError


class BasePostProcTest:
    """Base class for all post processor objects unittests."""

    _post_proc_class = None
    _from_calc = None
    _from_file = None

    @pytest.fixture(autouse=True)
    def setup_post_proc(self, tempdir):
        """Post processor test fixture."""
        if self._post_proc_class is None:
            raise DevError("Need to set '_post_proc_class'.")
        if self._from_file is None:
            raise DevError("Need to set '_from_file'.")
        if self._from_calc is None:
            raise DevError("Need to set '_from_calc'.")
        self.postproc = None
        self._link_tempdir = tempdir
        self._link_tempdir_path = os.path.join(self._link_tempdir, "calc")
        yield
        del self.postproc

    async def test_from_calculation(self):
        """Test post processor from calculation method."""
        # need to copy all calculation into temporary directory.
        # this is because absolute path information is lost when executing
        # the tests on different computers using the same files
        # FIXME: find a way to uncomment this and make test works
        # copy tree
        # calc = shutil.copytree(self._from_calc,
        #                        os.path.join(self._link_tempdir.name,
        #                                     "calc"),
        #                        symlinks=True)
        # # change meta file there
        # with MetaDataFile.from_calculation(calc, loglevel=1) as meta:
        #     # rename all files from the temporary root
        #     meta.calc_workdir = calc
        #     # change path of parent to a the actual path
        #     # according to new calc workdir.
        #     meta.write(overwrite=True)
        #     # recast pbs file of moved directory
        #     with PBSFile.from_meta_data_file(meta, loglevel=1) as pbs:
        #         pbs.log_path = os.path.join(meta.calc_workdir,
        #                                     os.path.basename(pbs.log_path))
        # need to correct the log file too in pbs file
        # just check that it works and doesn't raise an error
        # from a calculation
        self.postproc = await self._post_proc_class.from_calculation(
                self._link_tempdir_path, loglevel=1)
        self.postproc.get_plot()
