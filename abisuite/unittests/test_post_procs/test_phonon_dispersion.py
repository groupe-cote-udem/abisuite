import os

import aioshutil

from .bases import BasePostProcTest
from ..variables_for_tests import AbinitAnaddbCalc, QEMatdynCalc, QEMatdynfreq
from ...handlers import MetaDataFile, PBSFile
from ...post_processors import PhononDispersion


class BasePhononDispersionTest(BasePostProcTest):
    """Base class for phonon dispersion test cases.
    """
    _post_proc_class = PhononDispersion

    async def test_from_calculation(self):
        calc = await aioshutil.copytree(
                self._from_calc, self._link_tempdir_path,
                symlinks=True)
        # change meta file there
        async with await MetaDataFile.from_calculation(
                calc, loglevel=1) as meta:
            # rename all files from the temporary root
            meta.calc_workdir = calc
            # force write here to make sure all properties are changed
            # according to new calc workdir.
            await meta.write(overwrite=True)
            # recast pbs file of moved directory
            async with await PBSFile.from_meta_data_file(
                    meta, loglevel=1) as pbs:
                pbs.log_path = os.path.join(meta.calc_workdir,
                                            os.path.basename(pbs.log_path))
        await super().test_from_calculation()


class TestPhononDispersionFromQECalc(BasePhononDispersionTest):
    """Test class for a Phonon Dispersion object created from a Quantum
    Espresso matdyn calculation.
    """
    _from_calc = QEMatdynCalc
    _from_file = QEMatdynfreq

    async def test_from_freq_file(self):
        # test from a ".freq" file
        self.postproc = await self._post_proc_class.from_qe_matdyn_freq(
                self._from_file)
        self.postproc.get_plot()


class TestPhononDispersionFromAbinitCalculation(
        BasePhononDispersionTest):
    """Test case for a phonon dispersion object created from an abinit anaddb
    calculation.
    """
    _from_calc = AbinitAnaddbCalc
    _from_file = ""
