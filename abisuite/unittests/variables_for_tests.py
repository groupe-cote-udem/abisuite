import os

from ..constants import EV_TO_HA


# #############################################################################
# ############################## COMMON(WEALTH) ###############################
# #############################################################################


BASIC_MPI_VARIABLES = {"ppn": 12,
                       "mpi_command": "mpirun -npernode 12",
                       "nodes": "3:m48G"}
here = os.path.dirname(os.path.abspath(__file__))
pseudo_dir = os.path.join(here, "files/pseudos")


# #############################################################################
# ################################# ABINIT ####################################
# #############################################################################

# ########### ABINIT GS EXAMPLE (Si) #####
abinit_gs_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc", "gs_run")
abinit_gs_pseudos = [
        os.path.join(
            here, "test_workflows", "test_files", "pseudos", "Si.psp8"),
        ]
abinit_gs_input_variables = {
        "ecut": 10, "istwfk": "*1", "kptopt": 1, "ngkpt": [6] * 3,
        "nshiftk": 4,
        "shiftk": [[.5, .5, .5], [.5, 0, 0], [0, .5, 0], [0, 0, .5]],
        "diemac": 12, "nstep": 10, "toldfe": 1e-6, "acell": [7.2078778121] * 3,
        "natom": 2, "ntypat": 1,
        "rprim": [
            [0, 0.70710678119, 0.70710678119],
            [0.70710678119, 0, 0.70710678119],
            [0.70710678119, 0.70710678119, 0],
            ],
        "typat": [1] * 2, "xred": [[0] * 3, [.25] * 3],
        "znucl": (14, ),
        "pp_dirpath": "",
        "pseudos": abinit_gs_pseudos,
        "indata_prefix": os.path.join(
            abinit_gs_example, "input_data", "idat_gs_run"),
        "outdata_prefix": os.path.join(abinit_gs_example, "odat_gs_run"),
        "tmpdata_prefix": os.path.join(abinit_gs_example, "tmp_gs_run"),
        }
abinit_gs_output_file = os.path.join(
        abinit_gs_example, "gs_run.abo")


# ######################### PHONON ECUT CONVERGENCE PART ######################
# Al example (no DDK)
abinit_phonon_ecut_convergence_pseudos = [os.path.join(
    here, "test_workflows", "test_files", "pseudos", "Al.psp8")
    ]
abinit_phonon_ecut_convergence_criterion = 10
abinit_phonon_ecut_convergence_scf_input_variables = {
            "acell": [5.326968652] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "occopt": 4,
            "znucl": [13],
            "natom": 1,
            "typat": [1],
            "xred": [[0.0, 0.0, 0.0]],
            "tsmear": 0.1,
            "nshiftk": 4,
            "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                       [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
            "nstep": 10,
            "toldfe": 1.0e-6,
            "ngkpt": [4, 4, 4], "pp_dirpath": "",
            "pseudos": abinit_phonon_ecut_convergence_pseudos,
            }
abinit_phonon_ecut_convergence_phonons_input_variables = {
            "rfatpol": [1, 1],
            "rfdir": [1, 1, 1],
            "tolvrs": 1e-8,
            "nstep": 25,
            }
abinit_phonon_ecut_convergence_ecuts = [10, 30, 50]
abinit_phonon_ecut_convergence_qpt = [0.0, 0.0, 0.0]
abinit_phonon_ecut_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_Al",
        "phonon_ecut_convergence")
# Si example (with DDK)
abinit_phonon_ecut_convergence_pseudos_ddk = [os.path.join(
    here, "test_workflows", "test_files", "pseudos", "Si.psp8")
    ]
abinit_phonon_ecut_convergence_criterion_ddk = 10
abinit_phonon_ecut_convergence_scf_input_variables_ddk = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": 14,
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "kptrlatt": [[3, 3, -3], [-3, 3, -3], [-3, 3, 3]],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 10,
        "toldfe": 1.0e-6,
        "diemac": 12.0,
        "pp_dirpath": "",
        "pseudos": abinit_phonon_ecut_convergence_pseudos_ddk,
        }
abinit_phonon_ecut_convergence_phonons_input_variables_ddk = {
        "rfatpol": [1, 2],
        "rfdir": [1, 1, 1],
        "tolvrs": 1e-8,
        "diemac": 9.0,
        "nstep": 25,
        }
abinit_phonon_ecut_convergence_ecuts_ddk = [10, 20, 30]
abinit_phonon_ecut_convergence_example_ddk = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "phonon_ecut_convergence")
abinit_phonon_ecut_convergence_ddk_input_variables = {
        "rfatpol": [1, 2],
        "tolwfr": 1e-22,
        "nstep": 25,
        "diemac": 9.0,
        "rfdir": [1, 1, 1],
        }


# ABINIT PHONON KGRID CONVERGENCE EXAMPLE
# With DDK
abinit_phonon_kgrid_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "phonon_kgrid_convergence",
        )
abinit_phonon_kgrid_convergence_example_no_ddk = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "phonon_kgrid_convergence_no_ddk",
        )
abinit_phonon_kgrid_convergence_phonons_examples = [
        os.path.join(
            abinit_phonon_kgrid_convergence_example_no_ddk,
            "phonons_runs", "ngkpt", x)
        for x in os.listdir(os.path.join(
            abinit_phonon_kgrid_convergence_example, "phonons_runs", "ngkpt"))
        ]
abinit_phonon_kgrid_convergence_scf_examples = [
        os.path.join(
            abinit_phonon_kgrid_convergence_example, "scf_runs", "ngkpt", x)
        for x in os.listdir(os.path.join(
            abinit_phonon_kgrid_convergence_example, "scf_runs", "ngkpt"))
        ]
abinit_phonon_kgrid_convergence_phonons_input_variables = {
        "rfatpol": [1, 2],
        "rfdir": [1, 1, 1],
        "tolvrs": 1e-8,
        "diemac": 9.0,
        "nstep": 25,
        }
abinit_phonon_kgrid_convergence_ddk_input_variables = {
        "rfatpol": [1, 2],
        "tolwfr": 1e-22,
        "nstep": 25,
        "diemac": 9.0,
        "rfdir": [1, 1, 1],
        }
abinit_phonon_kgrid_convergence_pseudos = [
        os.path.join(
            here, "test_workflows", "test_files", "pseudos", "Si.psp8"),
        ]
abinit_phonon_kgrid_convergence_scf_input_variables = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": 14,
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 10,
        "toldfe": 1.0e-6,
        "diemac": 12.0,
        "ecut": 10,
        "pp_dirpath": "",
        "pseudos": abinit_phonon_kgrid_convergence_pseudos,
        }
abinit_phonon_kgrid_convergence_kgrid_input_variable_name = "ngkpt"
abinit_phonon_kgrid_convergence_phonons_qpt = [0, 0, 0]
abinit_phonon_kgrid_convergence_scf_kgrids = [[2] * 3, [4] * 3]
abinit_phonon_kgrid_convergence_phonons_qpt_no_ddk = [0, 0, 0.5]

# abinit phonon dispersion example
abinit_phonon_dispersion_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "phonon_dispersion",
        )
abinit_phonon_dispersion_noddk_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "phonon_dispersion_noddk",
        )
abinit_phonon_dispersion_compute_electric_field_response = True
abinit_phonon_dispersion_phonons_input_variables = {
        "rfatpol": [1, 2],
        "tolvrs": 1e-8,
        "diemac": 12.0,
        "nstep": 25,
        }
abinit_phonon_dispersion_qpoint_grid = [6] * 3
abinit_phonon_dispersion_qpoint_grid_list = [
        [0.00000000E+00, 0.00000000E+00, 0.00000000E+00],
        [1.66666667E-01, 0.00000000E+00, 0.00000000E+00],
        [3.33333333E-01, 0.00000000E+00, 0.00000000E+00],
        [5.00000000E-01, 0.00000000E+00, 0.00000000E+00],
        [1.66666667E-01, 1.66666667E-01, 0.00000000E+00],
        [3.33333333E-01, 1.66666667E-01, 0.00000000E+00],
        [5.00000000E-01, 1.66666667E-01, 0.00000000E+00],
        [-3.33333333E-01, 1.66666667E-01, 0.00000000E+00],
        [-1.66666667E-01, 1.66666667E-01, 0.00000000E+00],
        [3.33333333E-01, 3.33333333E-01, 0.00000000E+00],
        [5.00000000E-01, 3.33333333E-01, 0.00000000E+00],
        [-3.33333333E-01, 3.33333333E-01, 0.00000000E+00],
        [5.00000000E-01, 5.00000000E-01, 0.00000000E+00],
        [5.00000000E-01, 3.33333333E-01, 1.66666667E-01],
        [-3.33333333E-01, 3.33333333E-01, 1.66666667E-01],
        [-3.33333333E-01, 5.00000000E-01, 1.66666667E-01],
        ]
abinit_phonon_dispersion_qpoints_split_by_irreps = [7]
abinit_phonon_dispersion_ddk_input_variables = {
        "rfdir": [1, 1, 1],
        "tolwfr": 1e-16,
        "nstep": 25,
        "diemac": 12.0,
        }
abinit_phonon_dispersion_qpoint_path = [
        {"L": [0.5, 0.0, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {r"$\Gamma_2$": [1.0, 1.0, 1.0]},
        ]
abinit_phonon_dispersion_qpoint_path_density = 20
abinit_phonon_dispersion_anaddb_input_variables = {
        "ifcflag": 1, "ifcout": 0, "brav": 2, "ngqpt": [6, 6, 6],
        "q1shft": [0.0] * 3, "nqshft": 1, "chneut": 1, "dipdip": 1,
        "eivec": 4, "nph2l": 1, "qph2l": [1.0, 0.0, 0.0, 0.0],
        }
abinit_phonon_dispersion_pseudos = [
        os.path.join(
            here, "test_workflows", "test_files", "pseudos", "Si.psp8"),
        ]
abinit_phonon_dispersion_qgrid_generation_input_variable_name = "ngkpt"
abinit_phonon_dispersion_scf_input_variables = {
        "ecut": 10,
        "ngkpt": [6, 6, 6],
        "acell": [7.2078778121] * 3,
        "natom": 2,
        "ntypat": 1,
        "rprim": [[0.0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0.0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0.0]],
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0],
                 [0.25, 0.25, 0.25]],
        "znucl": 14.0,
        "kptopt": 1,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 10,
        "toldfe": 1.0e-6,
        "diemac": 12.0,
        "pseudos": abinit_phonon_dispersion_pseudos,
        "pp_dirpath": "",
        }

# ################ OPTIC #########################
abinit_optic_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc", "optic")
abinit_optic_nscf_input_variables = {
        "nband": 10, "ngkpt": [6, 6, 6], "tolwfr": 1e-14,
        "nbdbuf": 2,
        }
abinit_optic_pseudos = abinit_gs_pseudos
abinit_optic_scf_input_variables = abinit_gs_input_variables
_root = os.path.join(
        here,
        "test_workflows/test_files/abinit_si/nc/optic/optic_run/input_data/"
        )
abinit_optic_input_variables = {
        "ddkfile_1": os.path.join(_root, "idat_optic_run_1WF7.nc"),
        "ddkfile_2": os.path.join(_root, "idat_optic_run_1WF8.nc"),
        "ddkfile_3": os.path.join(_root, "idat_optic_run_1WF9.nc"),
        "wfkfile": os.path.join(_root, "idat_optic_run_WFK.nc"),
        "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
        "num_nonlin_comp": 0, "num_nonlin2_comp": 0,
        "num_linel_comp": 0,
        "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
        "tolerance": 1e-3, "scissor": 0.02524746980237357,
        }


# ######################## OPTIC NSCF CONVERGENCE ####################
abinit_optic_nscf_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "optic_nscf_convergence",
        )
abinit_optic_nscf_convergence_nscf_kpoint_grids = [
        [x, x, x] for x in [6, 8]]
abinit_optic_nscf_convergence_experimental_bandgap = 1.14
abinit_optic_nscf_convergence_nscf_kpoint_grids_input_varname = "ngkpt"
abinit_optic_nscf_convergence_nscf_input_variables = {
        "nstep": 20, "tolwfr": 1e-14, "nbdbuf": 2, "nband": 10,
        }
abinit_optic_nscf_convergence_ddk_input_variables = {
        "rfdir": [1, 1, 1],
        }
abinit_optic_ddk_input_variables = (
        abinit_optic_nscf_convergence_ddk_input_variables.copy())
abinit_optic_nscf_convergence_optic_input_variables = {
        "num_lin_comp": 6, "lin_comp": [11, 12, 13, 22, 23, 33],
        "num_nonlin_comp": 0, "num_nonlin2_comp": 0, "num_linel_comp": 0,
        "broadening": 1e-3, "domega": 0.1, "maxomega": 1,
        # manually set scissor as it can cause problems when testing
        # with different version of pythons or on different machines
        # most likely due to rounding errors (FG: 2021/07/28)
        "scissor": 0.02524746980237357,
        "tolerance": 1e-3}
abinit_optic_nscf_convergence_pseudos = abinit_gs_pseudos


# ############### OPTIC (linear response only) ##############
abinit_optic_linear_only_example = os.path.join(
        abinit_optic_nscf_convergence_example,
        "ngkpt_6_6_6"
        )
abinit_optic_linear_only_optic_input_variables = (
        abinit_optic_nscf_convergence_optic_input_variables.copy())


# optic lincomp file example
abinit_optic_lincomp_example = os.path.join(here,
                                            ("../../examples/parsers/"
                                             "abinit_parsers/"
                                             "optic_lin_comp_parser/"
                                             "optic_lin_comp_"
                                             "example.out"))
# DMFT file examples
abinit_dmft_projectors_example = os.path.join(here,
                                              ("../../examples/parsers/"
                                               "abinit_parsers/"
                                               "dmft_parsers/projectors/"
                                               "projectors_example.ovlp"))
abinit_dmft_hamiltonian_example = os.path.join(
        here,
        ("../../examples/parsers/abinit_parsers/dmft_parsers/hamiltonian/"
         "forlb.eig")
        )

# anaddb input variables example (based on trf2)
abinit_anaddb_vars = {
        "ifcflag": 1,
        "brav": 2,
        "ngqpt": [4, 4, 4],
        "nqshft": 1,
        "q1shft": [0.0, 0.0, 0.0],
        "chneut": 1,
        "dipdip": 1,
        "ifcana": 1,
        "ifcout": 20,
        "natifc": 1,
        "atifc": 1,
        }
AbinitAnaddbCalc = os.path.join(here, "files/abinit_AlAs/anaddb")
abinit_anaddb_log_example = os.path.join(AbinitAnaddbCalc, "anaddb.log")
abinit_anaddb_phfrq_example = os.path.join(
        AbinitAnaddbCalc, "anaddb.out_PHFRQ")

# mrgddb calculation example
AbinitMrgddbCalc = os.path.join(here, "files/abinit_AlAs/mrgddb")
abinit_mrgddb_log_example = os.path.join(AbinitMrgddbCalc, "mrgddb.log")


test_workflows_files = os.path.join(here, "test_workflows", "test_files")
# ABINIT FERMI SURFACE EXAMPLE
abinit_fermi_surface_scf_example = os.path.join(
        test_workflows_files, "abinit_Al", "gs_run")
abinit_fermi_surface_nscf_example = os.path.join(
        test_workflows_files, "abinit_Al", "fermi_surface", "nscf_run")
abinit_fermi_surface_nscf_input_variables = {
        "tolwfr": 1e-18,
        }
abinit_fermi_surface_nscf_kgrid = [15] * 3
abinit_fermi_surface_pseudos = [
        os.path.join(test_workflows_files, "pseudos", "Al.psp8"),
        ]
abinit_fermi_surface_scf_input_variables = {
        "nshiftk": 4, "shiftk": [
            [0.5, 0.5, 0.5], [0.5, 0.0, 0.0], [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10, "toldfe": 1e-6, "occopt": 4, "tsmear": 0.001,
        "ecut": 30, "ngkpt": [2, 2, 2], "acell": [5.326968652] * 3,
        "natom": 1, "ntypat": 1,
        "rprim": [
            [0.0, 0.70710678119, 0.70710678119],
            [0.70710678119, 0.0, 0.70710678119],
            [0.70710678119, 0.70710678119, 0.0]],
        "typat": [1], "xred": [[0.0, 0.0, 0.0]], "znucl": [13.0],
        "pseudos": abinit_fermi_surface_pseudos,
        "pp_dirpath": "",
        }

# ABINIT DOS EXAMPLES
abinit_dos_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "dos", "dos_run",
        )
abinit_dos_input_variables = {
        "pseudos": abinit_gs_pseudos,
        "pp_dirpath": "",
        "iscf": -3,
        "prtdos": 2,
        "tolwfr": 1e-12,
        "autoparal": 1,
        }
abinit_dos_fine_kpoint_grid_variables = {
        "ngkpt": [10, 10, 10],
        "nshiftk": 1,
        # don't shift grid (prefered according to abinit docs)
        "shiftk": [[0.0, 0.0, 0.0]],
        "nband": 8,
        }

# ########################### BAND STRUCTURE ##################################
abinit_band_structure_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "band_structure", "band_structure")
abinit_band_structure_input_variables = {
        "ecut": 20,
        "iscf": -2,
        "nband": 8,
        "tolwfr": 1e-12,
        "autoparal": 1,
        "diemac": 12.0,
        }
abinit_band_structure_kpoint_path = [
        {"L": [0.5, 0.0, 0.0]},
        {r"$\Gamma$": [0.0, 0.0, 0.0]},
        {"X": [0.0, 0.5, 0.5]},
        {r"$\Gamma_2$": [1.0, 1.0, 1.0]}]
abinit_band_structure_kpoint_path_density = 20


# abinit gs smearing convergence
abinit_gs_smearing_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_Al",
        "gs_smearing_convergence",
        )
abinit_gs_smearing_convergence_pseudos = [
        os.path.join(
            here, "test_workflows", "test_files", "pseudos", "Al.psp8"),
        ]
abinit_gs_smearing_convergence_scf_input_variables = {
        "acell": [7.6] * 3,
        "rprim": [[0, 0.5, 0.5], [0.5, 0, 0.5], [0.5, 0.5, 0]],
        "ntypat": 1,
        "znucl": [13],
        "natom": 1,
        "typat": [1],
        "xred": [0, 0, 0],
        "ecut": 20,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        "pseudos": abinit_gs_smearing_convergence_pseudos,
        "pp_dirpath": "",
        }
abinit_gs_smearing_convergence_smearings = [0.001, 0.1, 0.01]
abinit_gs_smearing_convergence_kgrids = [[x] * 3 for x in [2, 4, 8]]
abinit_gs_smearing_convergence_criterion = 10
abinit_gs_smearing_convergence_varname = "ngkpt"

# ABINIT RELAXATION example
abinit_relaxation_scf_input_variables = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5],
                  [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "ngkpt": [6, 6, 6],
        "ecut": 20.0,
        "nstep": 10,
        "tolvrs": 1.0e-10,
        "diemac": 12.0,
        "optcell": 1,  # only modify acell
        "ionmov": 22,
        "ntime": 20,
        "tolmxf": 5e-04,
        "ecutsm": 0.5,
        "dilatmx": 1.1,
        "pseudos": abinit_gs_pseudos,
        "pp_dirpath": "",
        }
abinit_relaxation_example = os.path.join(
    here, "test_workflows", "test_files", "abinit_si", "nc", "relax_run")

# ABINIT GS ECUT CONVERGENCE
abinit_gs_ecut_convergence_input_variables = {
        "acell": [10.18] * 3,
        "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
        "kptopt": 1,
        "pawecutdg": 30.0,
        "ngkpt": [2, 2, 2],
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
        "nstep": 50,
        "tolvrs": 1.0e-10,
        "diemac": 12.0, "pp_dirpath": "",
        "pseudos": abinit_gs_pseudos,
        }
abinit_gs_ecut_convergence_ecuts = [10, 20, 30]
abinit_gs_ecut_convergence_pawecuts = [20, 30, 50]
abinit_gs_ecut_convergence_criterion = 10
abinit_gs_pawecut_convergence_criterion = 10
abinit_gs_ecut_convergence_example = os.path.join(
        here, "test_workflows",
        "test_files", "abinit_si", "paw", "gs_ecut_convergence")
abinit_gs_pawecut_convergence_example = os.path.join(
        here, "test_workflows",
        "test_files", "abinit_si", "paw", "gs_ecut_convergence", "pawecutdg")

# ABINIT GS KGRID CONVERGENCE
abinit_gs_kgrid_convergence_scf_input_variables = {
         "acell": [10.18] * 3,
         "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
         "ntypat": 1,
         "znucl": [14],
         "natom": 2,
         "typat": [1, 1],
         "xred": [[0.0, 0.0, 0.0], [1/4, 1/4, 1/4]],
         "kptopt": 1,
         "nshiftk": 4,
         "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                    [0.0, 0.5, 0.0], [0.0, 0.0, 0.5]],
         "nstep": 10,
         "ecut": 20,
         "toldfe": 1.0e-6,
         "diemac": 12.0, "pp_dirpath": "",
         "pseudos": abinit_gs_pseudos,
         }
abinit_gs_kgrid_convergence_criterion = 1
abinit_gs_kgrid_convergence_scf_kgrids = [
        [2, 2, 2], [4, 4, 4],
        [6, 6, 6], [8, 8, 8]]
abinit_gs_kgrid_convergence_scf_kgrids_varname = "ngkpt"
abinit_gs_kgrid_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_si", "nc",
        "kgrid_convergence")

# abinit phonons smearing convergence
abinit_phonons_smearing_convergence_scf_input_variables = {
        "acell": [5.326968652] * 3,
        "rprim": [[0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0]],
        "ntypat": 1,
        "znucl": [13],
        "natom": 1,
        "typat": [1],
        "xred": [0, 0, 0],
        "ecut": 30,
        "nshiftk": 4,
        "shiftk": [[0.5, 0.5, 0.5], [0.5, 0.0, 0.0],
                   [0, 0.5, 0], [0, 0, 0.5]],
        "nstep": 10,
        "toldfe": 1e-6,
        "occopt": 4,
        "pp_dirpath": "",
        "pseudos": abinit_phonon_ecut_convergence_pseudos,
        }
abinit_phonons_smearing_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "abinit_Al",
        "phonon_smearing_convergence",
        )
abinit_phonons_smearing_convergence_phonons_input_variables = {
        "rfatpol": [1, 1],
        "tolvrs": 1e-8,
        "nstep": 25,
        "pseudos": abinit_phonon_ecut_convergence_pseudos,
        }

# ABINIT GW EXAMPLES
abinit_gw_example = os.path.join(test_workflows_files, "abinit_si", "nc", "gw")
abinit_gw_scf_input_variables = {
            "acell": [7.2078778121] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "znucl": [14],
            "natom": 2,
            "typat": [1, 1],
            "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
            "ngkpt": [4, 4, 4],
            "nband": 6,
            "nshiftk": 4,
            "tolvrs": 1e-10,
            "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                       [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
            "ecut": 8.0,
            "diemac": 12.0,
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }
abinit_gw_nscf_input_variables = {
            "nband": 120,
            "tolwfr": 1e-12,
            "nbdbuf": 20,
            "ngkpt": [4, 4, 4],
            "nshiftk": 4,
            "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                       [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
            "ecut": 8.0,
            "diemac": 12.0,
            }
abinit_gw_selfenergy_input_variables = {
            "nband": 100,
            "ecutsigx": 8.0,
            "nkptgw": 1,
            "kptgw": [0.0, 0.0, 0.0],
            "bdgw": [4, 5],
            }
abinit_gw_screening_input_variables = {
            "nband": 100,
            "ecuteps": 7.0,
            "ppmfrq": 16.7 * EV_TO_HA,
            }

# GW self energy nband convergence
abinit_gw_self_energy_nband_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc",
        "gw_self_energy_nband_convergence")
abinit_gw_self_energy_nband_convergence_screening_input_variables = {
            "nband": 50,
            "ecuteps": 6.0,
            "ppmfrq": 16.7 * EV_TO_HA,
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }
abinit_gw_self_energy_nband_convergence_selfenergy_input_variables = {
            "ecutsigx": 8.0,
            "nkptgw": 1,
            "kptgw": [0.0, 0.0, 0.0],
            "bdgw": [4, 5],
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }
# GW screening nband convergence
abinit_gw_screening_nband_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc",
        "gw_screening_nband_convergence")
abinit_gw_screening_nband_convergence_screening_input_variables = {
            "ecuteps": 6.0,
            "ppmfrq": 16.7 * EV_TO_HA,
            }
abinit_gw_screening_nband_convergence_selfenergy_input_variables = {
            "nband": 100,
            "ecutsigx": 8.0,
            "nkptgw": 1,
            "kptgw": [0.0, 0.0, 0.0],
            "bdgw": [4, 5],
            }

# GW ecuteps convergence
abinit_gw_ecuteps_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "gw_ecuteps_convergence")

# abinit GW kgrid convergence
abinit_gw_kgrid_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "gw_ngkpt_convergence")
abinit_gw_kgrid_convergence_scf_input_variables = {
            "acell": [7.2078778121] * 3,
            "rprim": [[0.0, 0.70710678119, 0.70710678119],
                      [0.70710678119, 0.0, 0.70710678119],
                      [0.70710678119, 0.70710678119, 0.0]],
            "ntypat": 1,
            "znucl": [14],
            "natom": 2,
            "typat": [1, 1],
            "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
            "nband": 6,
            "nshiftk": 4,
            "tolvrs": 1e-10,
            "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                       [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
            "ecut": 8.0,
            "diemac": 12.0,
            "pseudos": abinit_gs_pseudos,
            "pp_dirpath": "",
            }
abinit_gw_kgrid_convergence_nscf_input_variables = {
            "nband": 100,
            "tolwfr": 1e-12,
            "nbdbuf": 10,
            "nshiftk": 4,
            "shiftk": [[0.0, 0.0, 0.0], [0.0, 0.5, 0.5],
                       [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
            "ecut": 8.0,
            "diemac": 12.0,
            }
abinit_gw_kgrid_convergence_screening_input_variables = {
            "ecuteps": 4,
            "nband": 100,
            "ppmfrq": 16.7 * EV_TO_HA,
            }
abinit_gw_kgrid_convergence_selfenergy_input_variables = {
            "nband": 100,
            "ecutsigx": 8.0,
            "nkptgw": 1,
            "kptgw": [0.0, 0.0, 0.0],
            "bdgw": [4, 5],
            }
# ABINIT BSE EXAMPLES
abinit_bse_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "bse")
abinit_bse_scf_input_variables = {
        "pp_dirpath": "",
        "pseudos": abinit_gs_pseudos,
        "nband": 6,
        "tolvrs": 1e-8,
        "prtden": 1,
        "acell": [7.2078778121] * 3,
        "rprim": [[0.0, 0.70710678119, 0.70710678119],
                  [0.70710678119, 0.0, 0.70710678119],
                  [0.70710678119, 0.70710678119, 0.0]],
        "ntypat": 1,
        "znucl": [14],
        "natom": 2,
        "typat": [1, 1],
        "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
        "ngkpt": [4, 4, 4],
        "nshiftk": 1,
        "shiftk": [[0.0, 0.0, 0.0]],
        "istwfk": "*1",
        "ecut": 12.0,
        "diemac": 12.0,
        "nstep": 50,
        }
abinit_bse_nscf_input_variables = {
        "nband": 105,
        "tolwfr": 1e-8,
        "nbdbuf": 5,
        "ngkpt": [4, 4, 4],
        "nshiftk": 1,
        "shiftk": [[0.0, 0.0, 0.0]],
        "istwfk": "*1",
        "diemac": 12.0,
        }
abinit_bse_nscfnosym_input_variables = {
        "nband": 15,
        "tolwfr": 1e-8,
        "nbdbuf": 5,
        "ngkpt": [4, 4, 4],
        "nshiftk": 1,
        "shiftk": [[0.11, 0.21, 0.31]],
        "istwfk": "*1",
        "diemac": 12.0,
        "chksymbreak": 0,
        }
abinit_bse_screening_input_variables = {
        "gwpara": 2,
        "inclvkb": 2,
        "awtr": 1,
        "symchi": 1,
        "nband": 100,
        "ecuteps": 4,
        "ecutwfn": 12.0,
        "nfreqre": 1,
        "nfreqim": 90,
        "istwfk": "*1",
        "diemac": 12.0,
        }

# abinit bse nband convergence
abinit_bse_nband_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc",
        "bse_nband_convergence")
# Abinit bse ECUTEPS convergence
abinit_bse_ecuteps_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "bse_ecuteps_convergence")
abinit_bse_ecuteps_convergence_bse_input_variables = {
            "bs_calctype": 1,
            "mbpt_sciss": 0.8 * EV_TO_HA,
            "bs_exchange_term": 1,
            "bs_coulomb_term": 11,
            "bs_coupling": 0,
            "ecuteps": 2.0,
            "bs_freq_mesh": [0, 6 * EV_TO_HA, 0.02 * EV_TO_HA],
            "bs_algorithm": 2,
            "bs_haydock_niter": 200,
            "bs_haydock_tol": [0.05, 0],
            "zcut": 0.15 * EV_TO_HA,
            "kptopt": 1,
            "chksymbreak": 0,
            "ecutwfn": 12.0,
            "inclvkb": 2,
            "gw_icutcoul": 3,
            "pp_dirpath": "",
            "pseudos": abinit_gs_pseudos,
            }

# abinit bse kgrid convergence example
abinit_bse_kgrid_convergence_example = os.path.join(
        test_workflows_files, "abinit_si", "nc", "bse_ngkpt_convergence")
abinit_bse_kgrid_convergence_input_variable_name = "ngkpt"


# #############################################################################
# ########################## QUANTUM ESPRESSO #################################
# #############################################################################

# QE Phonon kgrid convergence examples
qe_phonon_kgrid_convergence_root = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "phonon_kgrid_convergence")
qe_phonon_kgrid_convergence_scf_kgrids = [
        [x, x, x, 1, 1, 1] for x in [2, 4, 8]]
qe_phonon_kgrid_convergence_phonons_input_variables = {}
qe_phonon_kgrid_convergence_phonons_examples = [
        os.path.join(
            qe_phonon_kgrid_convergence_root, "phonons_runs", "k_points", x)
        for x in os.listdir(os.path.join(
            qe_phonon_kgrid_convergence_root, "phonons_runs", "k_points"))]
qe_phonon_kgrid_convergence_scf_examples = [
        os.path.join(
            qe_phonon_kgrid_convergence_root, "scf_runs", "k_points", x)
        for x in os.listdir(os.path.join(
            qe_phonon_kgrid_convergence_root, "scf_runs", "k_points"))]
qe_phonon_kgrid_convergence_pseudo_dir = os.path.join(
    here, "test_workflows", "test_files", "pseudos")
qe_phonon_kgrid_convergence_scf_input_variables = {
    "calculation": "scf", "pseudo_dir": qe_phonon_kgrid_convergence_pseudo_dir,
    "occupations": "smearing",
    "conv_thr": 1.0e-8, "ecutwfc": 10, "degauss": 0.1, "ibrav": 2, "nat": 1,
    "ntyp": 1, "celldm(1)": 9.281641264965383,
    "atomic_species": [
        {"atomic_mass": 207.2, "atom": "Pb", "pseudo": "pb_s.UPF"},
        ],
    "atomic_positions": {
        "parameter": "crystal",
        "positions": {
            "Pb": [0.0, 0.0, 0.0],
            },
        },
    }


# simple dynmat calc
qe_dynmatx_vars = {"asr": "simple",
                   "fildyn": "test_fildyn",
                   "lperm": True,
                   "q(1)": 1.0,
                   "q(2)": 0.0,
                   "q(3)": 0.0}


# QE epsilon.x example
qe_epsilonx_vars = {
        "calculation": "eps",
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "smeartype": "gauss",
        "intersmear": 0.02,
        "wmin": 0.0,
        "wmax": 30.0,
        "nw": 500,
        }

# QE epw.x example
qe_epwx_vars = {
        "amass(1)": 207.2,
        "elph": True,
        "epbwrite": True,
        "epbread": False,
        "epwwrite": True,
        "epwread": False,
        "nbndsub": 4,
        "bands_skipped": "exclude_bands = 1:5",
        "wannierize": True,
        "num_iter": 300,
        "dis_win_max": 21,
        "dis_win_min": -3,
        "dis_froz_min": -3,
        "dis_froz_max": 13.5,
        "proj(1)": "PB:sp3",
        "wdata(1)": "bands_plot = True",
        "wdata(2)": "begin kpoint_path",
        "wdata(3)": "G 0.0 0.0 0.0 X 0.0 0.5 0.5",
        "wdata(4)": "X 0.0 0.5 0.5 W 0.25 0.5 0.75",
        "wdata(5)": "W 0.25 0.5 0.75 L 0.5 0.5 0.5",
        "wdata(6)": "L 0.5 0.5 0.5 K 0.375 0.375 0.75",
        "wdata(7)": "K 0.375 0.375 0.75 G 0.0 0.0 0.0",
        "wdata(8)": "G 0.0 0.0 0.0 L 0.5 0.5 0.5",
        "wdata(9)": "end kpoint_path",
        "wdata(10)": "bands_plot_format = gnuplot",
        "iverbosity": 0,
        "elecselfen": False,
        "phonselfen": True,
        "fsthick": 6,
        "degaussw": 0.1,
        "a2f": True,
        "nkf1": 10,
        "nkf2": 10,
        "nkf3": 10,
        "nqf1": 10,
        "nqf2": 10,
        "nqf3": 10,
        "nk1": 8,
        "nk2": 8,
        "nk3": 8,
        "title": "epw",
        "q_points": {
            "parameter": "cartesian",
            "q_points": [[0.0, 0.0, 0.0], [0.5, -0.5, 0.5], [0.0, -1.0, 0.0]],
            "weights": [1, 1, 1],
            },
        "prefix": "epw_test",
        "outdir": "outdir_test",
        }

# QE fs.x example
qe_fsx_vars = {
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        }

# QE ld1.x example
qe_ld1x_vars = {
        "atom": "Ti",
        "dft": "PBE",
        "config": "[Ar] 3d2 4s2 4p0",
        "iswitch": 2,
        "file_pseudo": "Ti_pseudo_file.UPF",
        "nconf": 1,
        "configts(1)": "3d2 4s2 4p0",
        "ecutmin": 50,
        "ecutmax": 200,
        "decut": 50,
        }

# simple matdyn calc for silicon
qe_matdynx_vars = {
        "asr": "simple",
        "flfrc": "test_flfrc",
        "flfrq": "test_flfrq",
        "q_in_band_form": True,
        "q_points": {"qpts": [[0.5, 0.5, 0.5],
                              [0.0, 0.0, 0.0],
                              [1.0, 0.0, 0.0]],
                     "nqpts": [20, 20, 20]},
        }
QEMatdynCalc = os.path.join(here, "files/qe_silicon/matdyn_run")
QEMatdynfreq = os.path.join(QEMatdynCalc, "run/output_data/matdyn.freq")


# simple ph calc for silicon
qe_phx_vars = {
        "amass(1)": 28.086,
        "q_points": [0.0, 0.0, 0.0],
        "title": "silicon",
        "fildyn": "test_fildyn",
        "fildvscf": "dvscf",
        "outdir": "test_outfir",
        "prefix": "test_prefix",
        }
QEPHCalc = os.path.join(here, "files/qe_silicon/ph_run")
qe_phx_dyn0_example = os.path.join(
        QEPHCalc, "run", "output_data", "ph_run.dyn0")
qe_phx_output_data_dir_example = os.path.join(QEPHCalc, "run", "output_data")
qe_phxlog_example = os.path.join(here, "files/qe_silicon/ph_run/ph_run.log")


# simple pp.x example
qe_ppx_vars = {
        "plot_num": 7,
        "lsign": True,
        "kpoint": 21,
        "kband": 4,
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "filplot": "test_filplot",
        "iflag": 3,
        "output_format": 5,
        "fileout": "test_fileout",
        }

# simple projwfc.x example
qe_projwfcx_vars = {
        "ngauss": 0,
        "degauss": 0.005,
        "Emin": -10.0,
        "Emax": 30.0,
        "DeltaE": 0.001,
        "filpdos": "test_filpdos",
        "filproj": "test_filproj",
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        }

QEProjwfcCalc = os.path.join(here, "files", "qe_silicon", "pdos_run")
QEProjwfcPDOSExample = os.path.join(
        here, "files", "qe_silicon", "pdos_run", "run", "output_data",
        "pdos.pdos_atm#1(Si)_wfc#2(p)"
        )
QEProjwfcLogExample = os.path.join(
        here, "files", "qe_silicon", "pdos_run", "pdos.log")


# QE pw.x example
qe_pwx_vars = {
        "calculation": "scf",
        "ibrav": 2,
        "prefix": "test_prefix",
        "outdir": "test_outdir",
        "pseudo_dir": pseudo_dir,
        "celldm(1)": 10.28,
        "nat": 2,
        "ntyp": 1,
        "ecutwfc": 18.0,
        "atomic_species": [{"atom": "Si", "atomic_mass": 28.086,
                            "pseudo": "Si.upf"}],
        "atomic_positions": {"parameter": "alat",
                             "positions": {"Si": [[0.00, 0.00, 0.00],
                                                  [0.25, 0.25, 0.25]],
                                           }},
        "k_points": {"parameter": "automatic",
                     "k_points": [4, 4, 4, 1, 1, 1]
                     }
        }

QEPWCalc = os.path.join(here, "files/qe_silicon/pw_run")
qe_pwxlog_example = os.path.join(here, ("files/qe_silicon/pw_run/pw.log"))
QEPWNscfPositiveKptsCalc = os.path.join(
        here, "files", "qe_silicon", "pw_run_nscf_positive_ks")
qe_pwx_output_data_dir_example = os.path.join(QEPWCalc, "run", "output_data")
PWCalc = QEPWCalc  # legacy + lot of laziness haha

# QE pw.x bandstructure vars example
qe_pwx_bandstructure_vars = {
        "calculation": "bands",
        "nbnd": 10,
        "ecutwfc": 50.0,
        "k_points": {"parameter": "crystal_b",
                     "nks": 2,
                     "k_points": [[0.0, 0.0, 0.0], [-0.5, 0.0, 0.0]],
                     "weights": [10, 10],
                     },
        "verbosity": "high",
        }
QEPWBandCalc = os.path.join(here, "files/qe_silicon/bandstructure_run")
QEPWBandCalclog = os.path.join(QEPWBandCalc, "bandstructure.log")

# QE pw2wannier90.x example
qe_pw2wannier90x_vars = {
        "outdir": "test_outdir",
        "prefix": "test_prefix",
        "seedname": "test_seedname",
        "spin_component": "none",
        "write_mmn": True,
        "write_amn": True,
        "write_unk": True,
        "reduce_unk": True,
        }

# simple q2r calc for silicon
qe_q2rx_vars = {
        "fildyn": "test_fildyn",
        "flfrc": "test_flfrc",
        }
QEQ2RCalc = os.path.join(here, "files/qe_silicon/q2r_run")


# TODO: to remove (below)??
# Already made calculations
POSITIVE_KS_NSCF_CALC = os.path.join(
        here, "files", "qe_silicon",
        "pw_run_nscf_positive_ks")


# QE SCF
qe_scf_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "gs_run")
qe_scf_pseudo_dir = os.path.join(
        here, "test_workflows", "test_files", "pseudos")
qe_scf_input_variables = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.281626909967228,
        "ecutwfc": 50,
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [8, 8, 8, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }


# QE DOS
qe_dos_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "dos_kgrid_convergence",
        "kgrid_1", "dos_run",
        )
qe_dos_nscf_input_variables = qe_scf_input_variables.copy()
qe_dos_nscf_example = os.path.join(  # pre nscf calculation
        here, "test_workflows", "test_files", "qe_Pb", "dos_kgrid_convergence",
        "kgrid_1", "nscf_run",
        )
qe_dos_input_variables = {
        "DeltaE": 0.1, "degauss": 0.05, "fildos": "dummy",
        "outdir": "dummy", "prefix": "dummy",
        }
qe_dos_fine_kpoint_grid_variables = {
        "k_points": {"parameter": "automatic", "k_points": [5] * 3 + [1] * 3},
        }
qe_dos_nscf_input_variables.update(qe_dos_fine_kpoint_grid_variables)
qe_dos_nscf_input_variables["calculation"] = "nscf"

# QE DOS kgrid convergence
qe_dos_kgrid_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "dos_kgrid_convergence",
        )
qe_dos_kgrid_convergence_input_variables = qe_dos_input_variables.copy()
qe_dos_kgrid_convergence_kgrids = [
        {"k_points": {"parameter": "automatic", "k_points": [x] * 3 + [1] * 3}}
        for x in [3, 5]]


# qe fermi surface
qe_fermi_surface_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "fermi_surface",
        )
qe_fermi_surface_nscf_kgrid = [15] * 3
qe_fermi_surface_nscf_input_variables = {}


# QE gs smearing convergence
qe_gs_smearing_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "gs_smearing_convergence",
        )
qe_gs_smearing_convergence_scf_input_variables = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "ecutwfc": 50,
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
qe_gs_smearing_convergence_kgrids = [[x, x, x, 1, 1, 1] for x in [2, 4, 8]]
qe_gs_smearing_convergence_smearings = [0.1, 0.01]
qe_gs_smearing_convergence_criterion = 10

# QE Phonon smearing convergence
qe_phonon_smearing_convergence_scf_input_variables = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.281641264965383,
        "ecutwfc": 20,
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
qe_phonon_smearing_convergence_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "phonon_smearing_convergence",
        )
qe_phonon_smearing_convergence_phonons_input_variables = {}
qe_phonon_smearing_convergence_qpt = [0.0, 0.0, 0.0]

# ####################### PHONON DISPERSION ##############################

GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}
X = {"X": [0.0, 0.5, 0.5]}   # X
K = {"K": [3/8, 3/8, 3/4]}
L = {"L": [1/2, 1/2, 1/2]}
W = {"W": [1/4, 1/2, 3/4]}
qe_phonon_dispersion_qpath = [GAMMA, X, W, L, K, GAMMA, W]
qe_phonon_dispersion_qpath_density = 100
qe_phonon_dispersion_phonons_input_variables = {
        "tr2_ph": 1e-12, "nq1": 2, "nq2": 2, "nq3": 2,
        "ldisp": True,
        }
qe_phonon_dispersion_phonons_qpoint_grid = [2, 2, 2]
qe_phonon_dispersion_phonons_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "phonon_dispersion")
qe_phonon_dispersion_q2r_input_variables = {}
qe_phonon_dispersion_asr = "crystal"
qe_phonon_dispersion_matdyn_input_variables = {}


# ######################### RELAXATION PART ###################################
qe_relaxation_input_variables = {
        "calculation": "scf",
        "ibrav": 2,
        "ntyp": 1,
        "nat": 1,
        "celldm(1)": 9.2225583816,
        "ecutwfc": 100,
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "press_conv_thr": 0.0001,
        "forc_conv_thr": 1e-6,
        "etot_conv_thr": 1e-6,
        "degauss": 0.1,
        "conv_thr": 1.0e-8,
        "atomic_species": [
            {"atom": "Pb",
             "atomic_mass": 207.2,
             "pseudo": "pb_s.UPF",
             }],
        "k_points": {"parameter": "automatic",
                     "k_points": [4, 4, 4, 1, 1, 1],
                     },
        "atomic_positions": {"parameter": "crystal",
                             "positions": {"Pb": [0.0, 0.0, 0.0]}},
        }
qe_relaxation_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "relax_run")


# QE Thermal lattice expansion
qe_thermal_lattice_expansion_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb",
        "lattice_expansion")
qe_thermal_lattice_expansion_phonons_qgrid = [2, 2, 2]
qe_thermal_lattice_expansion_scf_input_variables = {
    "ibrav": 2,
    "ntyp": 1,
    "nat": 1,
    "celldm(1)": 9.2225583816,
    "ecutwfc": 40,
    "pseudo_dir": qe_scf_pseudo_dir,
    "occupations": "smearing",
    "degauss": 0.1,
    "conv_thr": 1.0e-8,
    "atomic_species": [
        {"atom": "Pb",
         "atomic_mass": 207.2,
         "pseudo": "pb_s.UPF",
         }],
    "k_points": {"parameter": "automatic",
                 "k_points": [6, 6, 6, 1, 1, 1],
                 },
    "atomic_positions": {"parameter": "crystal",
                         "positions": {"Pb": [0.0, 0.0, 0.0]}},
    }
qe_thermal_lattice_expansion_phonons_input_variables = {
    "ldisp": True,
    "tr2_ph": 1e-12,
    }
qe_thermal_lattice_expansion_deltas = [0, -2, 2, -4, 4, -6, 6]
qe_thermal_lattice_expansion_phonons_fine_qpoint_grid = [4] * 3
qe_thermal_lattice_expansion_temperatures = [50, 150, 300, 600]
qe_thermal_lattice_expansion_asr = "crystal"
qe_thermal_lattice_expansion_bulk_modulus_initial_guess = 37

FCC_GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
FCC_X = {"X": [0.0, 0.5, 0.5]}   # X
FCC_K = {"K": [3/8, 3/8, 3/4]}
FCC_L = {"L": [1/2, 1/2, 1/2]}
FCC_W = {"W": [1/4, 1/2, 3/4]}
qe_thermal_lattice_expansion_phdisp_nqpts = 100
qe_thermal_lattice_expansion_phdisp_qpath = [
        FCC_GAMMA, FCC_X, FCC_W, FCC_L, FCC_K, FCC_GAMMA, FCC_W]
qe_thermal_lattice_expansion_fine_qpoint_grid = [4, 4, 4]


# QE epw wannierization + dispersion interpolation
qe_epw_wannierization_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "epw_wannierization",
        )
qe_epw_wannierization_nscf_example = os.path.join(
        qe_epw_wannierization_example, "nscf_run")
qe_epw_wannierization_nscf_kgrid = [6] * 3
qe_epw_wannierization_nscf_input_variables = {
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "degauss": 0.1,
        "ecutwfc": 50,
        "tprnfor": True,
        "tstress": True,
        "conv_thr": 1e-10,
        "diagonalization": "cg",
        "diago_full_acc": True,
        "noncolin": False,
        "lspinorb": False,
        "nbnd": 15,
        }
qe_epw_wannierization_epw_input_variables = {
        "amass(1)": 207.2,
        "elph": True,
        "band_plot": False,
        "etf_mem": 0,
        "epbwrite": True,
        "epbread": False,
        "epwwrite": True,
        "epwread": False,
        "nbndsub": 4,   # number of wannier functions to use
        # "nbndskip": 10,  # nbands under the disentanglement window
        "bands_skipped": "exclude_bands = 1-5",
        "wannierize": True,
        "num_iter": 1000,
        "wdata(1)": "dis_num_iter = 1000",
        "wdata(2)": "guiding_centres = True",
        "wdata(3)": "num_print_cycles = 100",
        "wdata(4)": "conv_window = 10",
        "dis_win_max": 21,
        "dis_win_min": -3,
        "dis_froz_min": -3,
        "dis_froz_max": 13.5,
        "proj(1)": "Pb:sp3",
        "iverbosity": 0,
        "elecselfen": False,
        "phonselfen": False,
        "fsthick": 6,  # fermi surface thickness
        "degaussw": 0.0,
        "ngaussw": -99,  # force FD distribution from the start
        "a2f": False,
        "vme": "wannier",
        "lifc": True, "nqf1": 1, "nqf2": 1, "nqf3": 1, "nkf1": 1, "nkf2": 1,
        "nkf3": 1,
        }
qe_epw_wannierization_pseudo_dir = qe_scf_pseudo_dir
qe_epw_wannierization_scf_example = qe_scf_example
qe_epw_wannierization_scf_input_variables = qe_scf_input_variables
qe_epw_wannierization_phonon_dispersion_example = os.path.join(
        qe_epw_wannierization_example, "..", "phonon_dispersion",
        "phonons_runs", "ph_q")
qe_epw_wannierization_phonon_dispersion_input_variables = {
        "tr2_ph": 1e-12, "ldisp": True, "nq1": 2, "nq2": 2, "nq3": 2,
        }
qe_epw_wannierization_phonon_dispersion_qpoint_grid = [2] * 3
GAMMA = {r"$\Gamma$": [0.0, 0.0, 0.0]}   # Gamma
X = {"X": [0.0, 0.5, 0.5]}   # X
K = {"K": [3/8, 3/8, 3/4]}
L = {"L": [1/2, 1/2, 1/2]}
W = {"W": [1/4, 1/2, 3/4]}
qe_epw_wannierization_phonon_dispersion_qpath = [GAMMA, X, W, L, K, GAMMA, W]
qe_epw_wannierization_phonon_dispersion_qpath_density = 100
qe_epw_wannierization_matdyn_example = os.path.join(
        os.path.dirname(os.path.dirname(
            qe_epw_wannierization_phonon_dispersion_example)),
        "matdyn_run")
qe_epw_wannierization_q2r_example = os.path.join(
        os.path.dirname(os.path.dirname(
            qe_epw_wannierization_phonon_dispersion_example)), "q2r_run")
qe_epw_wannierization_band_structure_example = os.path.join(
        qe_epw_wannierization_example, "..", "band_structure",
        "band_structure")
qe_epw_wannierization_band_structure_input_variables = {
        "pseudo_dir": qe_scf_pseudo_dir,
        "occupations": "smearing",
        "degauss": 0.1,
        "ecutwfc": 50,
        "conv_thr": 1e-8,
        }
qe_epw_wannierization_band_structure_kpoint_path = [
        GAMMA, X, W, L, K, GAMMA, W]
qe_epw_wannierization_band_structure_kpoint_path_density = 100

# IBTE example
qe_ibte_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "ibte", "ibte_run",
        )
qe_ibte_input_variables = {
        "assume_metal": True,
        "degaussw": 0.0,  # automatic smearing
        "iverbosity": 0,
        "etf_mem": 0,
        "fsthick": 0.1,
        "ngaussw": -99,  # FD
        "vme": "wannier",
        "temps": [50, 100, 300, 600],
        "restart_step": 100,
        "nstemp": 4,
        "nkf1": 10, "nkf2": 10, "nkf3": 10,
        "nqf1": 10, "nqf2": 10, "nqf3": 10,
        }
# Ziman example
qe_ziman_example = os.path.join(
        here, "test_workflows", "test_files", "qe_Pb", "ziman", "ziman_run")
qe_ziman_input_variables = {
        "a2f": True, "phonselfen": True, "degaussw": 0.005, "ngaussw": -99,
        "fsthick": 0.2, "rand_nk": 10 ** 3, "rand_nq": 10 ** 3,
        "rand_q": True, "rand_k": True, "vme": "wannier",
        }


# #############################################################################
# ############################# Wannier 90 ####################################
# #############################################################################


wannier90_vars = {
        "num_bands": 16,
        "num_wann": 8,
        "num_iter": 200,
        "conv_window": 3,
        "dis_win_max": 17.0,
        "dis_froz_max": 6.5,
        "dis_mix_ratio": 1.0,
        "bands_plot": True,
        "guiding_centres": True,
        "dis_num_iter": 1000,
        "projections": ["Si:sp3"],
        "mp_grid": [4] * 3,
        "atomic_positions": {
            "parameter": "atoms_frac",
            "positions": {"Si": [[0.0, 0.0, 0.0],
                                 [0.25, 0.25, 0.25]]}},
        "unit_cell": {
            "parameter": "unit_cell_cart",
            "vectors": [[-5.14, 0.0, 5.14],
                        [0.0, 5.14, 5.14],
                        [-5.14, 5.14, 0.0]], },
        }

Gamma = [0.0, 0.0, 0.0]
X = [-0.5, 0.0, 0.0]  # [-0.5, 1.0, 0.0]
W = [-0.75, -0.5, -0.25]  # [-0.75, 1.5, -0.25]
K = [-0.75, -0.5, -3/8]  # [-0.75, 1.5, -3/8]
L = [-0.5, -0.5, -0.5]  # [-0.5, 1.5, -0.5]
U = [-5/8, -0.5, -0.25]  # [-5/8, 1.5, -0.25]

# PATH
kpts = [Gamma, X, W, K, Gamma, L, U, W, L, K, U, X]
labels = ["G", "X", "W", "K", "G", "L", "U", "W", "L", "K", "U", "X"]
kpt_path = []
Npts = 4
for kpt1, kpt2, label1, label2 in zip(
        kpts[:-1], kpts[1:], labels[:-1], labels[1:]):
    kpt_path.append([{label1: kpt1}, {label2: kpt2}])

wannier90_vars["kpoint_path"] = kpt_path
for i in range(Npts):
    for j in range(Npts):
        for k in range(Npts):
            wannier90_vars.setdefault("kpoints", [])
            wannier90_vars["kpoints"].append([i / Npts, j / Npts, k / Npts])
