from abisuite.physics import Lattice

import pytest

from ..routines_for_tests import TemporaryCalculation
from ..variables_for_tests import QEPWCalc


class TestLattice:
    """Test case for a Lattice object.
    """
    @pytest.fixture(autouse=True)
    async def setup_lattice(self):
        with TemporaryCalculation(QEPWCalc) as calc_gen:
            calc = next(calc_gen)
            await calc.copy_calculation()
            self._qepwcalc = calc
            yield

    async def test_with_qe(self):
        # test with the Silicon example
        self.lattice = await Lattice.from_calculation(self._qepwcalc.path)
        assert self.lattice.bravais_lattice == "fcc"
        assert self.lattice.primitive_vectors.shape == (3, 3)
        assert self.lattice.reciprocal_vectors.shape == (3, 3)
        assert "Si" in self.lattice.atomic_types
        assert len(self.lattice.atomic_types) == 1
        assert len(self.lattice.atomic_masses) == 1
        assert len(self.lattice.atomic_pseudos) == 1
        assert self.lattice.natoms == 2
        for attr in ("atomic_positions", "atomic_positions_cartesian"):
            assert len(getattr(self.lattice, attr)) == 1
            assert len(getattr(self.lattice, attr)[0]) == 2  # 2 Si
