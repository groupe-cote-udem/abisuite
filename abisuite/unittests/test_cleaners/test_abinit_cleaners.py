import os
import unittest

import pytest

from .bases import BaseCleanerTest
from ..variables_for_tests import (
        abinit_gs_example,
        abinit_gs_input_variables,
        abinit_gs_pseudos,
        abinit_optic_example,
        abinit_optic_input_variables,
        )
from ...cleaners import AbinitCleaner, AbinitOpticCleaner
from ...exceptions import DevError


class BaseAbinitCleanerTest(BaseCleanerTest):
    """Base class for abinit cleaners unittests.
    """
    _calculation_pseudos = None

    @pytest.fixture(autouse=True)
    def setup_abinit_cleaner(self, setup_cleaner):
        if self._calculation_pseudos is None:
            raise DevError("Need to set '_calculation_pseudos'.")
        self.calc.new_pseudos = self._calculation_pseudos

    async def _get_launcher(self):
        launcher = await super()._get_launcher()
        await launcher.set_pseudos(self._calculation_pseudos)
        return launcher


class TestAbinitCleaner(BaseAbinitCleanerTest):
    """Test case for the AbinitCleaner class.
    """
    _cleaner_cls = AbinitCleaner
    _calculation_example = abinit_gs_example
    _calculation_example_input_variables = abinit_gs_input_variables
    _calculation_pseudos = abinit_gs_pseudos
    _script_name = "abinit"


class TestAbinitOpticCleaner(BaseAbinitCleanerTest):
    """Test case for the AbinitOptic cleaner class.
    """
    _cleaner_cls = AbinitOpticCleaner
    _calculation_example = os.path.join(
            abinit_optic_example, "optic_run")
    _calculation_example_input_variables = abinit_optic_input_variables
    _calculation_example_parents = []  # TODO: IMPLEMENT ME
    _script_name = "optic"

    @pytest.fixture(autouse=True)
    def setup_abinit_cleaner(self, setup_cleaner):
        # override
        pass

    @unittest.skip("Not implemented...")
    def test_clean_abort_if_calculation_not_started(self):
        BaseCleanerTest.test_clean_abort_if_calculation_not_started(self)
