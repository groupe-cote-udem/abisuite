import os
import unittest

from .bases import BaseCleanerTest
from ..variables_for_tests import (
        QEMatdynCalc,
        QEPHCalc,
        QEPWCalc,
        QEQ2RCalc,
        qe_epw_wannierization_epw_input_variables,
        qe_epw_wannierization_example,
        qe_matdynx_vars,
        qe_phx_vars,
        qe_pwx_vars,
        qe_q2rx_vars,
        )
from ...cleaners import (
        QEEPWCleaner, QEMatdynCleaner, QEPHCleaner, QEPWCleaner, QEQ2RCleaner,
        )


class BaseQECleanerTest(BaseCleanerTest):
    """Base class for QE cleaners test cases.
    """
    pass


class TestQEEPWCleaner(BaseQECleanerTest):
    """Test case for the 'qe_epw' cleaner class.
    """
    _cleaner_cls = QEEPWCleaner
    _calculation_example = os.path.join(
            qe_epw_wannierization_example, "epw_wannierization_run")
    _calculation_example_input_variables = (
            qe_epw_wannierization_epw_input_variables.copy())
    _calculation_example_parents = []  # TODO: IMPLEMENT ME
    _script_name = "epw.x"

    @unittest.skip("Not implemented...")
    async def test_clean_abort_if_calculation_not_started(self):
        await BaseQECleanerTest.test_clean_abort_if_calculation_not_started(
                self)


class TestQEMatdynCleaner(BaseQECleanerTest):
    """Test case for the 'qe_matdyn' cleaner class.
    """
    _cleaner_cls = QEMatdynCleaner
    _calculation_example = QEMatdynCalc
    _calculation_example_input_variables = qe_matdynx_vars
    _calculation_example_parents = [QEQ2RCalc]
    _script_name = "matdyn.x"


class TestQEPHCleaner(BaseQECleanerTest):
    """Test case for the 'qe_ph' cleaner class.
    """
    _cleaner_cls = QEPHCleaner
    _calculation_example = QEPHCalc
    _calculation_example_input_variables = qe_phx_vars
    _calculation_example_parents = [QEPWCalc]
    _script_name = "ph.x"


class TestQEPWCleaner(BaseQECleanerTest):
    """Test case for the 'qe_pw' cleaner class.
    """
    _cleaner_cls = QEPWCleaner
    _calculation_example = QEPWCalc
    _calculation_example_input_variables = qe_pwx_vars
    _script_name = "pw.x"


class TestQRQ2RCleaner(BaseQECleanerTest):
    """Test case for the 'qe_q2r' cleaner class.
    """
    _cleaner_cls = QEQ2RCleaner
    _calculation_example = QEQ2RCalc
    _calculation_example_input_variables = qe_q2rx_vars
    _calculation_example_parents = [QEPHCalc]
    _script_name = "q2r.x"
