import asyncio

import aiofiles
import aiofiles.os

import pytest

from ..routines_for_tests import TemporaryCalculation
from ...exceptions import DevError
from ...handlers import CalculationDirectory
from ...launchers import __ALL_LAUNCHERS__
from ...routines import is_list_like
from ...status_checkers.exceptions import CalculationNotFinishedError


class BaseCleanerTest:
    """Base class for all Cleaners unittests."""

    _cleaner_cls = None
    _calculation_example = None
    _calculation_example_input_variables = None
    _calculation_example_parents = None
    _script_name = None

    @pytest.fixture
    def temp_calc(self):
        """Temporary calculation fixture."""
        with TemporaryCalculation(
                self._calculation_example,
                ) as temp:
            yield next(temp)

    @pytest.fixture(scope="class")
    async def temp_cmd_file(self):
        """Temporary command file fixture."""
        if self._script_name is None:
            raise DevError("Need to set '_script_name'.")
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._script_name) as temp:
            yield temp

    @pytest.fixture(autouse=True)
    def setup_cleaner(self, temp_calc, temp_cmd_file):
        """Setup fixture for cleaners unittests."""
        if self._cleaner_cls is None:
            raise DevError("Need to set '_cleaner_cls'.")
        if self._calculation_example is None:
            raise DevError("Need to set '_calculation_example'.")
        if self._calculation_example_input_variables is None:
            raise DevError(
                    "Need to set '_calculation_example_input_variables'.")
        if self._calculation_example_parents is not None:
            if not is_list_like(self._calculation_example_parents):
                raise DevError(
                        "'_calculation_example_parents' must be a list.")
        # copy example calculation into a temp dir
        self.calc = temp_calc
        self.cleaner = self._cleaner_cls()
        self.command_file = temp_cmd_file

    async def test_clean(self):
        """Test clean method from cleaner objects."""
        # check that cleaning actually works
        await self.calc.copy_calculation()
        await self.cleaner.set_calculation_directory(self.calc.path)
        to_delete = [x.path for x in await self.cleaner.get_files_to_delete()]
        await self.cleaner.clean()
        # check that all files have been deleted
        coro = []
        for path in await (await CalculationDirectory.from_calculation(
                self.calc.path)).walk(paths_only=True):
            if path in to_delete:
                coro.append(aiofiles.os.path.exists(path))
        assert (not x for x in await asyncio.gather(*coro))

    async def test_clean_abort_if_calculation_not_started(self):
        """Test that cleaning aborts if calculation is not started."""
        # write a qe_pw calc using launcher and check that
        # cleaning does not work
        launcher = await self._get_launcher()
        parents = []
        coro = []
        coro_link = []
        if self._calculation_example_parents is not None:
            for parent in self._calculation_example_parents:
                parent_calc = TemporaryCalculation(
                        parent)
                coro.append(parent_calc.copy_calculation())
                parents.append(parent_calc)
                coro_link.append(launcher.link_calculation(parent_calc.path))
        await asyncio.gather(*coro)
        await asyncio.gather(*coro_link)
        launcher.command = self.command_file.name
        await launcher.write()
        await self.cleaner.set_calculation_directory(self.calc.path)
        with pytest.raises(CalculationNotFinishedError):
            await self.cleaner.clean()
        # check that force cleaning does not complain
        assert await self.cleaner.clean(force=True) is None
        # clean parents
        for parent in parents:
            parent.cleanup()

    async def _get_launcher(self):
        """Creates a launcher object."""
        launcher_cls = __ALL_LAUNCHERS__[self.cleaner.calctype]
        launcher = launcher_cls("test")
        launcher.queuing_system = "local"
        await launcher.set_workdir(self.calc.path)
        await launcher.set_input_variables(
                    self._calculation_example_input_variables.copy())
        return launcher
