import os

import aiofiles.os

import numpy as np

import pytest

from ..plotters import (
        Curve,
        MultiPlot,
        Plot,
        )


class TestCurve:
    _xdata = [-1, 2, 8]
    _ydata = np.array([0.0, 1.0, 2.3])
    _label = "label"

    @pytest.fixture(autouse=True)
    def setup_curve(self):
        self.curve = Curve(self._xdata, self._ydata, label=self._label)
        yield
        del self.curve

    def test_clear_label(self):
        assert self.curve.label == self._label
        self.curve.clear_label()
        assert self.curve.label is None

    def test_max_min(self):
        assert self.curve.max == np.max(self._ydata)
        assert self.curve.min == np.min(self._ydata)
        assert self.curve.wheremin == -1
        assert self.curve.wheremax == 8

    def test_arithmetic_operations(self):
        self.curve *= 2.0
        assert self.curve.ydata.tolist() == (self._ydata * 2.0).tolist()
        self.curve /= 4.0
        assert self.curve.ydata.tolist() == (self._ydata * 2.0 / 4.0).tolist()
        self.curve += 3.4
        assert self.curve.ydata.tolist() == (
                self._ydata * 2.0 / 4.0 + 3.4).tolist()
        self.curve -= 8.9
        assert self.curve.ydata.tolist() == (
                self._ydata * 2.0 / 4.0 + 3.4 - 8.9).tolist()
        with pytest.raises(TypeError):
            self.curve += "string"
        with pytest.raises(TypeError):
            self.curve *= "string"
        with pytest.raises(TypeError):
            self.curve -= "string"
        with pytest.raises(TypeError):
            self.curve /= "string"


class TestMultiPlot:
    _xdata = [0, 1, 2]
    _ydata = [0, 1, 2]

    @pytest.fixture(autouse=True)
    def setup_multiplot(self, tempdir):
        self.plot1 = Plot()
        self.plot1.add_curve(self._xdata, self._ydata)
        self.plot2 = Plot()
        self.plot2.add_curve(self._ydata, self._xdata)
        self.multiplot = MultiPlot()
        self.multiplot.add_plot(self.plot1, 0)
        self.multiplot.add_plot(self.plot2, 0)
        self.tempdir = tempdir
        self.path = os.path.join(self.tempdir, "test_multiplot.png")

    def test_muliplot_creation(self):
        # check that it raises an error if we try to add anything else
        with pytest.raises(TypeError):
            self.multiplot.add_plot(1, 1)
        assert len(self.multiplot.plot_rows) == 1
        assert len(self.multiplot.plot_rows[0]) == 2
        assert len(self.multiplot.plots) == 2
        # test that plotting process does not raise an error
        self.multiplot.plot(show=False)

    async def test_multiplot_save(self):
        # should raise an error if plot not drawn before saving
        with pytest.raises(RuntimeError):
            await self.multiplot.save(self.path)
        self.multiplot.plot(show=False)
        await self.multiplot.save(self.path)
        assert await aiofiles.os.path.isfile(self.path)


class TestPlot:
    _xdata = np.array([0, 1, 2])
    _ydata = np.array([0, 1, 3])
    _color = "b"
    _linestyle = "--"
    _label = "label"
    _linewidth = 1.5
    _tag = "curve"

    @pytest.fixture(autouse=True)
    def setup_plot(self, tempdir):
        self.plot = Plot()
        self.plot.add_curve(self._xdata, self._ydata, color=self._color,
                            linestyle=self._linestyle, label=self._label,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_vline(1, linestyle=self._linestyle, color=self._color,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_hline(1, linestyle=self._linestyle, color=self._color,
                            linewidth=self._linewidth, tag=self._tag)
        self.plot.add_fill_between(self._xdata, self._ydata - 1,
                                   self._ydata + 1, color=self._color,
                                   tag=self._tag)
        self.temp = tempdir
        self.name = os.path.join(self.temp, "save.png")

    def test_remove_by_tag(self):
        self.plot.remove_curve(self._tag)
        self.plot.remove_vline(self._tag)
        self.plot.remove_hline(self._tag)
        self.plot.remove_fill_between(self._tag)

    def test_remove_by_index(self):
        ncurves = len(self.plot.plot_objects["curves"])
        curve = self.plot.plot_objects["curves"][0]
        self.plot.remove_curve(0)
        assert len(self.plot.plot_objects["curves"]) == ncurves - 1
        assert curve not in self.plot.plot_objects["curves"]

        nvlines = len(self.plot.plot_objects["vlines"])
        vline = self.plot.plot_objects["vlines"][0]
        self.plot.remove_vline(0)
        assert len(self.plot.plot_objects["vlines"]) == nvlines - 1
        assert vline not in self.plot.plot_objects["vlines"]

        nhlines = len(self.plot.plot_objects["hlines"])
        hline = self.plot.plot_objects["hlines"][0]
        self.plot.remove_hline(0)
        assert len(self.plot.plot_objects["hlines"]) == nhlines - 1
        assert hline not in self.plot.plot_objects["hlines"]

        fill_between = self.plot.plot_objects["fill_betweens"][0]
        nfills = len(self.plot.plot_objects["fill_betweens"])
        self.plot.remove_fill_between(0)
        assert len(self.plot.plot_objects["fill_betweens"]) == nfills - 1
        assert fill_between not in self.plot.plot_objects["fill_betweens"]

    def test_clear_labels(self):
        # test that the clear labels method works
        for curve in self.plot.plot_objects["curves"]:
            assert curve.label == self._label
        self.plot.clear_curve_labels()
        for curve in self.plot.plot_objects["curves"]:
            assert curve.label is None

    def test_create_plot(self):
        curves = self.plot.plot_objects["curves"]
        assert curves[0].color == self._color
        assert curves[0].linestyle == self._linestyle
        assert curves[0].label == self._label
        assert curves[0].linewidth == self._linewidth
        assert curves[0].xdata.tolist() == self._xdata.tolist()
        assert curves[0].ydata.tolist() == self._ydata.tolist()
        fill_betweens = self.plot.plot_objects["fill_betweens"]
        assert fill_betweens[0].xdata.tolist() == self._xdata.tolist()
        assert fill_betweens[0].ydata.tolist() == (
                self._ydata - 1).tolist()
        assert fill_betweens[0].ydata2.tolist() == (
                self._ydata + 1).tolist()
        assert fill_betweens[0].color == self._color

    async def test_save_plot(self):
        # check that an error is raised if saving file before plot is made
        with pytest.raises(RuntimeError):
            await self.plot.save(self.name)
        self.plot.plot(show=False)
        await self.plot.save(self.name)
        assert await aiofiles.os.path.isfile(self.name)

    def test_add_plot(self):
        plot2 = Plot()
        plot2.add_curve(self._ydata, self._xdata, color="k", label="label2",
                        linestyle=":", linewidth=2.0)
        plot2.add_fill_between(self._ydata, self._xdata - 1,
                               self._xdata + 1, color=self._color)
        plot3 = self.plot + plot2
        curves = plot3.plot_objects["curves"]
        assert len(curves) == 2
        assert len(plot3.plot_objects["hlines"]) == 1
        assert len(plot3.plot_objects["vlines"]) == 1
        assert curves[0].xdata.tolist() == self._xdata.tolist()
        assert curves[0].ydata.tolist() == self._ydata.tolist()
        assert curves[1].xdata.tolist() == self._ydata.tolist()
        assert curves[1].ydata.tolist() == self._xdata.tolist()
        fill_betweens = plot3.plot_objects["fill_betweens"]
        assert fill_betweens[0].xdata.tolist() == self._xdata.tolist()
        assert fill_betweens[0].ydata.tolist() == (
                self._ydata - 1).tolist()
        assert fill_betweens[0].ydata2.tolist() == (
                self._ydata + 1).tolist()
        assert fill_betweens[1].xdata.tolist() == self._ydata.tolist()
        assert fill_betweens[1].ydata.tolist() == (
                self._xdata - 1).tolist()
        assert fill_betweens[1].ydata2.tolist() == (
                self._xdata + 1).tolist()
        assert curves[0].color == self._color
        assert curves[0].linestyle == self._linestyle
        assert curves[0].label == self._label
        assert curves[1].color == "k"
        assert curves[1].linestyle == ":"
        assert curves[1].label == "label2"

    def test_set_curve_label(self):
        self.plot.set_curve_label("newlabel", 0)
        assert self.plot.plot_objects["curves"][0].label == "newlabel"

    def test_add_vlines(self):
        assert self.plot.plot_objects["vlines"][0].position == 1
        assert self.plot.plot_objects["vlines"][0].linestyle == self._linestyle
        assert self.plot.plot_objects["vlines"][0].color == self._color
        assert self.plot.plot_objects["vlines"][0].linewidth == self._linewidth

    def test_add_hlines(self):
        assert self.plot.plot_objects["hlines"][0].position == 1
        assert self.plot.plot_objects["hlines"][0].linestyle == self._linestyle
        assert self.plot.plot_objects["hlines"][0].color == self._color
        assert self.plot.plot_objects["hlines"][0].linewidth == self._linewidth
