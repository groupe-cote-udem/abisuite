# flake8: noqa D101,D102,D103,D200,D204,D205,D400

# Suite of tools to post process test files after generating new ones.
# Basically it replaces symlinks with relative symlinks (in order that tests
# can run on different machines). Also it reduces the size of some files to 0
# (like WFK files or DEN etc.)
import asyncio
import os

import aiofiles.os
from abisuite.linux_tools import touch


async def replace_file(path, **kwargs):
    if await aiofiles.os.path.islink(path):
        # compute relative path and replace link
        og_source = await aiofiles.os.readlink(path)
        dirname = os.path.dirname(path)
        relpath = os.path.relpath(og_source, start=dirname)
        if og_source != os.path.abspath(os.path.join(dirname, relpath)):
            raise LookupError(
                    "Recomputed symlink path don't match previous one.")
        await aiofiles.os.remove(path)
        await aiofiles.os.symlink(relpath, path, **kwargs)
    else:
        await aiofiles.os.remove(path)
        await touch(path, sync_open=True)


async def is_file_to_replace(path):
    if await aiofiles.os.path.islink(path):
        if os.path.isabs(await aiofiles.os.readlink(path)):
            return True
        return False
    if not await aiofiles.os.path.isfile(path):
        return False
    path = os.path.basename(path)
    if ".wfc" in path:
        return True
    if path.startswith("wfc") and path.endswith(".dat"):
        return True
    if "charge-density.dat" in path:
        return True
    if path.endswith("_WFK") or "_1WF" in path or path.endswith("_WFK.nc"):
        return True
    if path.endswith("_DEN") or path.endswith("_DEN.nc"):
        return True
    if "_SCR" in path:
        return True
    # Don't delete these files...
    # if "_GSR" in path:
    #     return True
    return False


async def is_broken_link(path):
    if not await aiofiles.os.path.islink(path):
        return False
    src = await aiofiles.os.readlink(path)
    if not os.path.isabs(src):
        src = os.path.join(os.path.dirname(path), src)
    if not await aiofiles.os.path.isfile(src):
        return True
    return False


async def repair_broken_link(path):
    src = await aiofiles.os.readlink(path)
    if not os.path.isabs(src):
        src = os.path.join(os.path.dirname(path), src)
    if await aiofiles.os.path.islink(src):
        if await is_broken_link(src):
            await repair_broken_link(src)
            return
    try:
        await touch(src, sync_open=True)
    except FileNotFoundError:
        print(f"{path} -> {src} is non-existent")


async def main():
    coros = []
    for dirname, _dirpaths, filepaths in os.walk("."):
        # check if there are symlinks of directories
        for _dirpath in _dirpaths:
            fullpath = os.path.abspath(os.path.join(dirname, _dirpath))
            if await aiofiles.os.path.islink(fullpath):
                og_source = await aiofiles.os.readlink(fullpath)
                if not os.path.isabs(og_source):
                    continue
                coros.append(replace_file(fullpath, target_is_directory=True))
        for filepath in filepaths:
            fullpath = os.path.abspath(os.path.join(dirname, filepath))
            if await is_file_to_replace(fullpath):
                coros.append(replace_file(fullpath))
                continue
            if await is_broken_link(fullpath):
                coros.append(repair_broken_link(fullpath))
    await asyncio.gather(*coros)


if __name__ == "__main__":
    asyncio.run(main())
