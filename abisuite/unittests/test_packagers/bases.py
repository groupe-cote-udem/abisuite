import os

import aiofiles.os

import pytest

from ..routines_for_tests import TemporaryCalculation
from ... import __USER_DATABASE__
from ...databases import DBCalculation
from ...exceptions import DevError


class BasePackagerTest:
    """Base class for Packager objects."""

    _example = None
    _packager_cls = None

    @pytest.fixture
    def temp_calc(self):
        """Fixture for a temporary calculation."""
        with TemporaryCalculation(
                self._example, new_parents=[]) as temp:
            yield next(temp)

    @pytest.fixture(autouse=True)
    def setup_packager(self, tempdir, temp_calc):
        """Setup fixture for packagers tests."""
        if self._example is None:
            raise DevError("Need to set '_example'.")
        self.calc = temp_calc
        if self._packager_cls is None:
            raise DevError("Need to set '_packager_cls'.")
        self.packager = self._packager_cls(loglevel=1)
        self.dest = tempdir

    async def test_package(self, testdb, calc_example=None):
        """Test the packaging feature of the Packaging object.

        Parameters
        ----------
        calc_example: str, optional
            If None, the example calculation used is the one given as
            the class attribute `_example`. Otherwise we use the one
            given.
        """
        if calc_example is not None:
            calc = TemporaryCalculation(
                    calc_example,
                    new_parents=[])
        else:
            calc = self.calc
        await calc.copy_calculation()
        await self.packager.set_calculation_directory(calc.path)
        prev_walk = await self.packager.calculation_directory.walk(
                paths_only=False)
        prev_walk_paths = [x.path for x in prev_walk]
        if __USER_DATABASE__ is not None:
            self.packager.calculation_directory.connected_to_database = True
            assert not await self.packager.calculation_directory.is_packaged
        await self.packager.package(
                src_root=os.path.dirname(calc.path),
                dest_root=self.dest,
                replace_with_symlink=True)
        basename = os.path.basename(calc.path)
        # check that all files needed to copy were copied
        # and that all files copied needed to be copy (1:1 correspondance)
        for handler in prev_walk:
            if self.packager.keep_file(handler):
                # make sure this file appears in the dest
                relpath = os.path.relpath(handler.path, calc.path)
                newpath = os.path.join(self.dest, basename, relpath)
                assert await aiofiles.os.path.isfile(newpath)
        for root, _, files in os.walk(self.dest):
            for filename in files:
                relpath = os.path.relpath(
                        os.path.join(root, filename),
                        os.path.join(self.dest, basename))
                oldpath = os.path.join(calc.path, relpath)
                assert oldpath in prev_walk_paths
        # check prev directory has been symlinked
        assert await aiofiles.os.path.islink(calc.path), (
                f"{os.path.dirname(calc.path)}: "
                f"{await aiofiles.os.listdir(os.path.dirname(calc.path))}"
                )
        final = os.path.join(self.dest, basename)
        assert await aiofiles.os.readlink(calc.path) == final
        # check that the db entry is now set
        if __USER_DATABASE__ is not None:
            model = DBCalculation.get(
                        DBCalculation.calculation == final)
            assert model.packaged
            # also check the same entry in the calculation
            assert await self.packager.calculation_directory.is_packaged
        if calc_example is not None:
            calc.cleanup()
