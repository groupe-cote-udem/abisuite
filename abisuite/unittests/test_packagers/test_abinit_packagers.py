import os

import pytest

from .bases import BasePackagerTest
from ..variables_for_tests import (
        abinit_gs_example,
        abinit_gs_pseudos,
        abinit_optic_example,
        )
from ...packagers import (
        AbinitOpticPackager,
        AbinitPackager,
        )


class TestAbinitPackager(BasePackagerTest):
    """Test case for the AbinitPackager.
    """
    _example = abinit_gs_example
    _packager_cls = AbinitPackager

    @pytest.fixture(autouse=True)
    def setup_abinit_packager(self, setup_packager):
        self.calc.new_pseudos = abinit_gs_pseudos


class TestAbinitOpticPackager(BasePackagerTest):
    """Test case for the AbinitOpticPackager.
    """
    _example = os.path.join(abinit_optic_example, "optic_run")
    _packager_cls = AbinitOpticPackager
