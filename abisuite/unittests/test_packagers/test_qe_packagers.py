import os

import pytest

from .bases import BasePackagerTest
from ..variables_for_tests import (
        QEMatdynCalc, QEPHCalc, QEPWCalc, QEQ2RCalc,
        qe_epw_wannierization_example,
        qe_ibte_example, qe_ziman_example,
        )
from ...packagers import (
        QEEPWPackager, QEMatdynPackager, QEPHPackager, QEPWPackager,
        QEQ2RPackager,
        )


class TestQEEPWPackager(BasePackagerTest):
    """Test case for the QEEPWPackager object.
    """
    _example = os.path.join(
            qe_epw_wannierization_example, "epw_wannierization_run")
    _packager_cls = QEEPWPackager

    @pytest.mark.parametrize(
            "calc_example", [None, qe_ibte_example, qe_ziman_example])
    async def test_package(self, calc_example, testdb):
        """Extra test to package an IBTE calculation to make sure it works.
        """
        await super().test_package(testdb, calc_example=calc_example)


class TestQEMatdynPackager(BasePackagerTest):
    """Test case for the QEMatdynPackager object.
    """
    _example = QEMatdynCalc
    _packager_cls = QEMatdynPackager


class TestQEPHPackager(BasePackagerTest):
    """Test case for the QEPHPackager object.
    """
    _example = QEPHCalc
    _packager_cls = QEPHPackager


class TestQEPWPackager(BasePackagerTest):
    """Test case for the QEPWPackager object.
    """
    _example = QEPWCalc
    _packager_cls = QEPWPackager


class TestQEQ2RPackager(BasePackagerTest):
    _example = QEQ2RCalc
    _packager_cls = QEQ2RPackager
