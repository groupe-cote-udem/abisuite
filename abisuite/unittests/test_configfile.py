import os

import pytest

from ..config import ConfigFileParser


class TestConfigFile:

    @pytest.fixture(autouse=True)
    def setup_configfile(self, tempdir):
        self.tempdir = tempdir
        # use a subclass of the config file parser to change it's path
        self.configdir = os.path.join(self.tempdir, "config")
        self.configpath = os.path.join(self.configdir, "abiproject")

        class ConfigFileParserTestClass(ConfigFileParser):
            config_path = self.configpath

        self.config = ConfigFileParserTestClass()

    def test_config_parser_works(self):
        # check that a 'none' value returns None
        # by default, abinit_default_build is "abinit_default_build
        assert self.config.ABINIT.default_build == "abinit_default_build"
        # check that a 'false' value returns False
        # by default, the abinit_default_build don't have mpi
        assert self.config.abinit_default_build.has_mpi is False
        # samething for true but set it in the default config first
        # use force for tests to prevent the raise of an error
        # cause WE SHOULD NOT BE ABLE TO MODIFY THE CONFIG FILE USING THE API
        # EXCEPT WHEN SETTING DEFAULTS
        self.config.write("has_mpi", True, "abinit_default_build", force=True)
        assert self.config.abinit_default_build.has_mpi is True
        # check that a list config entry is well read
        for prop in ("modules_to_load", "modules_to_unload", "modules_to_use"):
            assert getattr(self.config.abinit_default_build, prop) == []
            modules = ['test_module', 'test_module_2']
            self.config.write(
                    prop, modules, "abinit_default_build", force=True)
            assert getattr(self.config.abinit_default_build, prop) == modules

    def test_config_entries(self):
        # test that a config entry can be represented (printed)
        # should not raise an error
        assert isinstance(repr(self.config.ABINIT), str)
        # test that we can get a build entry from the special method
        entry = self.config.get_build("abinit_default_build")
        assert "build_path" in entry
        # if accessing a non-existent entry, it should raise an error
        with pytest.raises(ValueError):
            self.config.get_build("non-existent-build")
