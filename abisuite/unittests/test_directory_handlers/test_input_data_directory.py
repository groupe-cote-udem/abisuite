import os

import aiofiles.os

from .bases import BaseDirectoryHandlerTest
from ...handlers import InputDataDirectory


class TestInputDataDirectory(BaseDirectoryHandlerTest):
    _directory_handler_class = InputDataDirectory

    async def test_write(self, tempdir2):
        # take the file already existing from the directory handler in setUp
        # and copy/link them in a new directory
        newdir = InputDataDirectory()
        await newdir.set_path(tempdir2)
        link1 = os.path.join(newdir.path, "link1")
        copy1 = os.path.join(newdir.path, "copy1")
        link2 = os.path.join(newdir.path, "subdir/link2")
        copy2 = os.path.join(newdir.path, "subdir/copy2")
        for link in (link1, link2):
            await newdir.add_symlink(link, self._file)
        for copy in (copy1, copy2):
            await newdir.add_copied_file(copy, self._subfile)
        await newdir.write()
        for link in (link1, link2):
            assert await aiofiles.os.path.islink(link)
            assert await aiofiles.os.path.samefile(
                    await aiofiles.os.readlink(link), self._file)
        for copy in (copy1, copy2):
            assert await aiofiles.os.path.isfile(copy)
