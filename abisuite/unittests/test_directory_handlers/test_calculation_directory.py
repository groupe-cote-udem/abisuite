import asyncio
import os
import unittest

import aiofiles
import aiofiles.os

import aioshutil

import pytest

from .bases import BaseDirectoryHandlerWithPBSTest
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        QEPWCalc,
        abinit_gs_input_variables,
        abinit_gs_pseudos,
        qe_phx_output_data_dir_example,
        qe_phx_vars,
        qe_phxlog_example,
        qe_pwx_output_data_dir_example,
        qe_pwx_vars, qe_pwxlog_example,
        )
from ... import __USER_DATABASE__
from ...databases import DBCalculation
from ...handlers import (
        CalculationDirectory, CalculationTree,
        GenericInputFile,  # test with generic input file
        OutputDataDirectory,
        )
from ...launchers import (
        AbinitLauncher,
        QEPHLauncher,
        QEPWLauncher,
        )
from ...linux_tools import (
        mkdir,
        touch,
        )


class TestCalculationDirectory(BaseDirectoryHandlerWithPBSTest):
    _directory_handler_class = CalculationDirectory
    _directory_handler_init_args = [GenericInputFile]

    @pytest.fixture(autouse=True)
    async def setup_calculation_dir_handler(self, setup_dir_with_pbs_handler):
        await asyncio.gather(
                self.handler.meta_data_file.set_path(
                    os.path.join(self.handler.path, "meta.meta")),
                self.handler.input_file.set_path(
                    os.path.join(self.handler.path, "inputfile.in")),
                )
        # add a random input var
        self.handler.input_file.input_variables = {"generic_variable": 1}
        self.input_symlink_path = os.path.join(
                self.handler.input_data_directory.path, "input_symlink")
        self.input_copiedfile_path = os.path.join(
                self.handler.input_data_directory.path, "input_copied_file")
        await asyncio.gather(
                self.handler.add_symlink(
                    self.input_symlink_path,
                    self._file
                    ),
                self.handler.add_copied_file(
                    self.input_copiedfile_path,
                    self._subfile),
                self.handler.pbs_file.set_path(
                    os.path.join(
                        self.handler.run_directory.path,
                        os.path.basename(self.handler.pbs_file.path))),
                    )
        self.handler.input_file.input_variables = {
                "generic_variable": "generic_value"}
        self.handler.meta_data_file.calctype = "calctype"
        self.handler.meta_data_file.jobname = "jobname"
        self.handler.meta_data_file.submit_time = "time"

    async def test_write(self):
        await super().test_write()
        assert all(await asyncio.gather(
            aiofiles.os.path.isfile(self.handler.input_file.path),
            aiofiles.os.path.isdir(
                self.handler.input_data_directory.path),
            aiofiles.os.path.isdir(self.handler.run_directory.path),
            aiofiles.os.path.isdir(
                self.handler.run_directory.output_data_directory.path),
            aiofiles.os.path.islink(self.input_symlink_path),
            aiofiles.os.path.isfile(self.input_copiedfile_path),
            aiofiles.os.path.samefile(
                await aiofiles.os.readlink(
                    self.input_symlink_path), self._file),
                )
                )


class BaseCalculationManagementTest:
    _scriptname = None
    _jobname = None
    _launcher_class = None
    _input_vars = None
    _log_example = None
    _output_data_dir_example = None

    @pytest.fixture
    async def temp_script(self):
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=self._scriptname) as temp:
            yield temp

    @pytest.fixture(autouse=True)
    async def setup_calc_management(self, tempdir, tempdir2, temp_script):
        # prepare a finished calculation in a temporary directory
        self.tempdir = tempdir
        self.workdir = self.tempdir
        self.script_file = temp_script
        # create calc
        self.launcher = await self._set_launcher(
                tempdir2,
                self._launcher_class,
                self._input_vars)
        await self.launcher.write()
        # create artificial log file and stderr file
        await self._copy_log_stderr(self._log_example)
        # copy artificial output
        await self._copy_output_dir(self._output_data_dir_example)
        # file are written, delete launcher and use calc directory now
        del self.launcher
        yield

    @unittest.skipIf(__USER_DATABASE__ is None, "'peewee' not available.")
    async def test_move_one_time_with_db(self, tempdir3, testdb):
        # create db
        obj = DBCalculation.create(calculation=self.workdir)
        oldid = obj.id
        # move calculation and check it is still inside the db
        # with the new path instead of the previous one
        calc = await CalculationDirectory.from_calculation(self.workdir)
        calc.connected_to_database = True
        await calc.move(tempdir3)
        # query new path
        query = DBCalculation.get_or_none(
                    DBCalculation.calculation == tempdir3)
        assert query is not None
        query2 = DBCalculation.get_or_none(
                DBCalculation.id == oldid)
        assert query2.calculation == tempdir3
        assert DBCalculation.get_or_none(
            DBCalculation.calculation == self.workdir) is None, (
                    f"workdir = {self.workdir}")

    async def test_move_one_time(self, tempdir3):
        calc = await CalculationDirectory.from_calculation(
                self.workdir)
        # move to a new temporary dir
        await calc.move(tempdir3)
        # create new calc dir and check everything is kept intact
        self.calc = await CalculationDirectory.from_calculation(
                tempdir3)
        await self.calc.read()
        # check meta data file
        async with self.calc.meta_data_file as meta:
            assert meta.calc_workdir == tempdir3
            for path in [meta.pbs_file_path, meta.input_file_path,
                         meta.script_input_file_path]:
                assert await aiofiles.os.path.isfile(path)
            for path in [meta.calc_workdir, meta.rundir, meta.output_data_dir,
                         meta.input_data_dir]:
                assert await aiofiles.os.path.isdir(path), f"{path}"
                assert path.startswith(tempdir3)
        # check pbs file
        async with self.calc.pbs_file as pbs:
            for path in [pbs.log_path, pbs.stderr_path]:
                assert await aiofiles.os.path.isfile(path), (
                        f"Calc dir {tempdir3} content: "
                        f"{await aiofiles.os.listdir(tempdir3)}, "
                        f"file checked: {path}, "
                        f"Old workdir: {self.workdir}.")
        assert await self.calc.equals(calc), (
                f"\n{await self.calc.content.str()}\n!="
                f"\n{await calc.content.str()}")
        await self._check_input_variables()
        await self._check_parents_and_children()

    async def test_move_back_and_forth(self, tempdir3):
        calc = await CalculationDirectory.from_calculation(self.workdir)
        calc_moved = await CalculationDirectory.from_calculation(self.workdir)
        async with calc_moved:
            await calc_moved.move(tempdir3)
            await calc_moved.move(self.workdir)
        assert await calc.equals(calc_moved), (
                f"{calc}\n!=\n{calc_moved}"
                )

    async def _copy_log_stderr(self, example_log):
        await aioshutil.copy2(example_log, self.launcher.pbs_file.log_path)
        await touch(self.launcher.pbs_file.stderr_path)

    async def _copy_output_dir(self, output_example):
        handler = OutputDataDirectory()
        await handler.set_path(output_example)
        await handler.read()
        for path in await handler.walk():
            relpath = os.path.relpath(path, start=output_example)
            newpath = os.path.join(self.launcher.output_data_dir.path, relpath)
            if not await aiofiles.os.path.isdir(os.path.dirname(newpath)):
                await mkdir(os.path.dirname(newpath))
            if await aiofiles.os.path.islink(path):
                # create new link instead of copying
                source = await aiofiles.os.readlink(path)
                # if source is inside input data dir, relink everything
                if "input_data" in source:
                    path_inside_idd = source.split("input_data/")[-1]
                    source = os.path.join(self.launcher.input_data_dir.path,
                                          path_inside_idd)
                    # linked data in the input, it is supposed to be linked
                    # already so skip it
                    continue
                await aiofiles.os.symlink(
                        source, newpath,
                        target_is_directory=await aiofiles.os.path.isdir(
                            source))
            else:
                await aioshutil.copy2(path, newpath)

    async def _check_input_variables(self):
        pass  # by default nothing

    async def _check_parents_and_children(self):
        pass

    async def _set_launcher(self, tempdir2, launcher_cls, input_vars):
        launcher = launcher_cls(self._jobname)
        await launcher.set_workdir(self.workdir)
        await launcher.set_input_variables(input_vars.copy())
        launcher.queuing_system = "local"
        launcher.command = self.script_file.name
        launcher.mpi_command = "mpirun -np 1"
        return launcher


# test that moving a calculation directory will update the file content
class TestQEPWCalculationManagement(BaseCalculationManagementTest):
    _scriptname = "pw.x"
    _jobname = "pw_run"
    _launcher_class = QEPWLauncher
    _input_vars = qe_pwx_vars
    _log_example = qe_pwxlog_example
    _output_data_dir_example = qe_pwx_output_data_dir_example

    async def _check_input_variables(self):
        infile = self.calc.input_file
        assert infile.input_variables["outdir"] == (
                self.calc.output_data_directory.path)


class TestQEPHCalculationManagement(BaseCalculationManagementTest):
    _scriptname = "ph.x"
    _jobname = "ph_run"
    _launcher_class = QEPHLauncher
    _input_vars = qe_phx_vars
    _log_example = qe_phxlog_example
    _output_data_dir_example = qe_phx_output_data_dir_example

    async def test_moving_parent(self, tempdir3):
        parent_calc = await CalculationDirectory.from_calculation(
                self.linked_calculation)
        # move to a new temporary dir
        # move PARENT calculation
        await parent_calc.move(tempdir3)
        # create calc dir and check everything is kept intact
        self.calc = await CalculationDirectory.from_calculation(
                self.tempdir)
        # check that all linked files and copied files correctly points to
        # something
        async with self.calc.meta_data_file as meta:
            for copied in meta.copied_files:
                assert await aiofiles.os.path.isfile(copied)
            for linked in meta.linked_files:
                assert await aiofiles.os.path.isfile(linked)
        # now look in input dir for this link and make sure it points
        # to the same file
        for path in await self.calc.input_data_directory.walk(paths_only=True):
            if await aiofiles.os.path.islink(path):
                # check that all links are unbroken
                readlink = await aiofiles.os.readlink(path)
                exists = await aiofiles.os.path.exists(readlink)
                assert exists, f"'{readlink}' doesn't exists..."

    async def _check_input_variables(self):
        infile = self.calc.input_file
        assert infile.input_variables["outdir"] == (
                self.calc.output_data_directory.path)
        for i in range(4):
            # there is 4 dynfiles
            dyn = infile.input_variables["fildyn"].value + str(i)
            assert await aiofiles.os.path.isfile(dyn), (
                    f"{dyn} not found in {self.calc.output_data_directory}"
                    )

    async def _check_parents_and_children(self):
        # check the PWCalc has changed its child
        parent_calc = await CalculationDirectory.from_calculation(
                self.linked_calculation)
        parent_meta = parent_calc.meta_data_file
        childpath = parent_meta.children[-1]  # supposed to be the last one...
        assert childpath == self.calc.path
        # check the calc parent points to the same place
        parentpath = self.calc.meta_data_file.parents[0]
        assert parentpath == parent_meta.calc_workdir

    async def _set_launcher(self, tempdir2, *args, **kwargs):
        launcher = await super()._set_launcher(tempdir2, *args, **kwargs)
        self.linked_calculation = await self._copy_calc_to_link(
                QEPWCalc, tempdir2)
        await launcher.link_calculation(self.linked_calculation)
        return launcher

    async def _copy_calc_to_link(self, calc_to_link, tempdir2):
        # need to modify meta files because meta file are LOCAL
        # do this in a temporary directory to not modify file inplace
        whereto = os.path.join(tempdir2, "calc")
        await copy_calculation(calc_to_link, whereto)
        return whereto


class TestCalculationTree:

    @pytest.fixture(autouse=True)
    async def setup_calc_tree(
            self, tempdir, temp_abinit_script, temp_pwx_script):
        # create two different series of random calculations
        # tempdir and temp command file
        self.top_dir = tempdir
        self.abinit_command_file = temp_abinit_script
        self.qepw_command_file = temp_pwx_script
        # first one
        self.njobs1 = 5
        self.launchers1 = [
                AbinitLauncher(f"abinit{i}") for i in range(self.njobs1)]
        coro = []
        coro_ivars = []
        coro_write = []
        for launcher1 in self.launchers1:
            await launcher1.set_workdir(os.path.join(
                    self.top_dir, "abinit", launcher1.jobname))
            launcher1.command = self.abinit_command_file.name
            coro_ivars.append(
                    launcher1.set_input_variables(
                        abinit_gs_input_variables.copy()))
            launcher1.queuing_system = "local"
            await launcher1.set_pseudos(abinit_gs_pseudos)
            coro_write.append(launcher1.write())
        # second job
        self.njobs2 = 3
        self.launchers2 = [
                QEPWLauncher(f"qepw{i}") for i in range(self.njobs2)]
        for launcher2 in self.launchers2:
            coro.append(launcher2.set_workdir(os.path.join(
                    self.top_dir, "qepw", launcher2.jobname)))
            launcher2.command = self.qepw_command_file.name
            coro_ivars.append(
                    launcher2.set_input_variables(qe_pwx_vars.copy()))
            launcher2.queuing_system = "local"
            coro_write.append(launcher2.write())
        await asyncio.gather(*coro)
        await asyncio.gather(*coro_ivars)
        await asyncio.gather(*coro_write)
        # create tree
        self.tree = CalculationTree(loglevel=1)
        await self.tree.set_path(self.top_dir)

    async def test_read(self):
        await self.tree.build_tree()
        assert len(self.tree) == self.njobs1 + self.njobs2
        assert await self.tree.len() == 2, await self.tree.content.str()
        assert await self.tree.contains("abinit")
        assert await self.tree.contains("qepw")

    async def test_status(self):
        await self.tree.read()
        for calcstatus in self.tree.status:
            assert not calcstatus["calculation_started"]
            assert not calcstatus["calculation_finished"]
            assert not calcstatus["calculation_converged"]
