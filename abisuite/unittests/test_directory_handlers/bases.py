import os

import aiofiles
import aiofiles.os

import pytest

from ...exceptions import DevError
from ...linux_tools import touch


class BaseDirectoryHandlerTest:
    """Base directory handler tests."""

    _directory_handler_class = None
    _directory_handler_init_args = []

    @pytest.fixture(autouse=True)
    async def setup_dir_handler(self, tempdir):
        """Setup fixture for directory handler tests."""
        if self._directory_handler_class is None:
            raise DevError("Test requires a directory handler class.")
        # create a directory with some content in it
        self._dir = tempdir
        # create file inside
        self._file = os.path.join(self._dir, "tempfile")
        await touch(self._file)
        # create sub directory
        async with aiofiles.tempfile.TemporaryDirectory(
                dir=self._dir) as subdir:
            self._subdir = subdir
            # create subfile in subdirectory
            self._subfile = os.path.join(self._subdir, "tempsubfile")
            await touch(self._subfile)
            self.handler = self._directory_handler_class(
                    *self._directory_handler_init_args, loglevel=1
                    )
            await self.handler.set_path(self._dir)
            yield

    async def test_read(self):
        """Test the read method of directory handlers."""
        # test that reading gets all the content and subcontent
        await self.handler.read()
        # check all paths and subpaths
        allpaths = [x.path async for x in self.handler]
        assert self._file in allpaths
        assert self._subdir in allpaths
        allsubpaths = await self.handler.walk(paths_only=True)
        assert self._subfile in allsubpaths, f"{str(self.handler)}"

    async def test_copy(self, tempdir2):
        """Test the copy method of directory handlers."""
        # find other place
        await self.handler.copy(tempdir2)
        newhandler = self._directory_handler_class(
                *self._directory_handler_init_args, loglevel=1)
        await newhandler.set_path(tempdir2)
        await newhandler.read()
        # direct comparision is hard to do since not same path for each objects
        # self.assertEqual(newhandler.content, self.handler.content)
        assert await newhandler.len() == await self.handler.len(), (
                f"\n{os.listdir(newhandler.path)}\n!=\n"
                f"{os.listdir(self.handler.path)}")

    async def test_copy_handler(self):
        """Test the copy_handler method of directory handlers."""
        copy = await self.handler.copy_handler()
        assert await self.handler.equals(copy), (
                f"{await self.handler.content.str()} != "
                f"{await copy.content.str()}")

    async def test_delete(self):
        """Test the delete method of directory handlers."""
        await self.handler.delete()
        assert not await aiofiles.os.path.exists(self.handler.path)

    async def test_move(self, tempdir2):
        """Test the move method of directory handlers."""
        # read now to make sure everything is included
        await self.handler.read()
        olddir = await self.handler.copy_handler()
        await self.handler.move(tempdir2)
        assert not await aiofiles.os.path.exists(olddir.path)
        assert await aiofiles.os.path.isdir(tempdir2)
        assert len(await self.handler.content) == len(await olddir.content), (
                f"\n{[x.path for x in self.handler.content]}\n!="
                f"\n{[x.path for x in olddir.content]}")
        # direct comparision is hard to do since not same path for each objects
        # self.assertTrue(all([x == y for x, y in zip(self.handler.content,
        #                                             olddir.content)]),
        #                 msg=f"{self.handler.content} != {olddir.content}")
        assert self.handler.path == tempdir2


class BaseDirectoryHandlerWithPBSTest(BaseDirectoryHandlerTest):
    """Base class for unittests of directory handlers which include a pbs."""

    @pytest.fixture(autouse=True)
    async def setup_dir_with_pbs_handler(self, setup_dir_handler):
        """Setup fixture for dir handler tests with pbs file."""
        # create random pbs file
        await self.handler.pbs_file.set_path(
                os.path.join(self.handler.path, "pbs.sh"))
        self.handler.pbs_file.queuing_system = "local"
        self.handler.pbs_file.input_file_path = "input"
        self.handler.pbs_file.stderr_path = "stderr"
        self.handler.pbs_file.log_path = "log"
        self.handler.pbs_file.command = "command"

    async def test_copy_handler(self):
        """Test copy_handler method of dir handler with pbs file."""
        await self.handler.write()
        await super().test_copy_handler()

    async def test_copy(self, tempdir2):
        """Test copy method of dir handler with pbs file."""
        await self.handler.write()
        await super().test_copy(tempdir2)

    async def test_move(self, tempdir2):
        """Test the move method of dir handler with pbs file."""
        await self.handler.write()
        await super().test_move(tempdir2)

    async def test_write(self):
        """Test the write method of dir handler with pbs file."""
        await self.handler.write()
        assert await aiofiles.os.path.isfile(self.handler.pbs_file.path)
        assert await aiofiles.os.path.isdir(self.handler.path)
