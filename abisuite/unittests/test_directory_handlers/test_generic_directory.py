from .bases import BaseDirectoryHandlerTest
from ...handlers import GenericDirectory


class TestGenericDirectory(BaseDirectoryHandlerTest):
    _directory_handler_class = GenericDirectory
