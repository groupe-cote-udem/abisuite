from .bases import BaseDirectoryHandlerWithPBSTest
from ...handlers import RunDirectory


class TestRunDirectory(BaseDirectoryHandlerWithPBSTest):
    _directory_handler_class = RunDirectory
