from .bases import (
        BaseSCFStatusCheckerTest,
        BaseStatusCheckerTest,
        )
from ..variables_for_tests import (
        AbinitAnaddbCalc,
        AbinitMrgddbCalc,
        abinit_gs_example,
        )


class TestAbinitStatusChecker(BaseSCFStatusCheckerTest):
    """Test case for an abinit calculation status checker.
    """
    _calculation = abinit_gs_example


class TestAbinitAnaddbStatusChecker(BaseStatusCheckerTest):
    """Test case for an anaddb calculation status checker.
    """
    _calculation = AbinitAnaddbCalc


class TestAbinitMrgddbStatusChecker(BaseStatusCheckerTest):
    """Test case for a mrgddb calculation status checker.
    """
    _calculation = AbinitMrgddbCalc
