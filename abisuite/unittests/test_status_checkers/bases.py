import os

import pytest

from ..routines_for_tests import copy_calculation
from ...exceptions import DevError
from ...handlers import CalculationDirectory


class BaseStatusCheckerTest:
    """Base test class for status checkers."""

    _calculation = None

    @pytest.fixture(autouse=True)
    async def setup_status_checker(self, tempdir):
        """Setup fixture for status checkers."""
        if self._calculation is None:
            raise DevError("Need to set '_calculation'.")
        # create tempdir
        self.calculation_path = os.path.join(tempdir, "test")
        # copy calculaion into tempdir
        await copy_calculation(
                self._calculation, self.calculation_path, new_pseudos=[])
        self.calc_dir = await CalculationDirectory.from_calculation(
                            self.calculation_path)

    async def test_status_computation(self):
        """Test computation status property."""
        # just check the status by calling the status property of the calcdir
        status = await self.calc_dir.status
        # calculation should be finished
        for prop in ("calculation_finished", "calculation_started", ):
            assert prop in status
            assert status[prop]


class BaseSCFStatusCheckerTest(BaseStatusCheckerTest):
    """A base class for unittest for status checkers of SCF calculations."""

    async def test_status_computation(self):
        """Test the status computation."""
        await super().test_status_computation()
        status = await self.calc_dir.status
        assert "calculation_converged" in status
        assert status["calculation_converged"]
