from .bases import (
        BaseSCFStatusCheckerTest,
        BaseStatusCheckerTest,
        )
from ..variables_for_tests import (
        QEMatdynCalc, QEPHCalc,
        QEPWBandCalc,
        QEPWCalc,
        QEPWNscfPositiveKptsCalc,
        QEProjwfcCalc,
        QEQ2RCalc,
        qe_dos_example,
        )


class TestQEDOSStatusChecker(BaseStatusCheckerTest):
    _calculation = qe_dos_example


class TestQEMatdynStatusChecker(BaseStatusCheckerTest):
    _calculation = QEMatdynCalc


class TestQEPHStatusChecker(BaseSCFStatusCheckerTest):
    _calculation = QEPHCalc


class TestQEProjwfcStatusChecker(BaseStatusCheckerTest):
    _calculation = QEProjwfcCalc


class TestQEPWStatusChecker(BaseSCFStatusCheckerTest):
    _calculation = QEPWCalc


class TestQEPWBandStructureStatusChecker(
        BaseStatusCheckerTest):
    _calculation = QEPWBandCalc


class TestQEPWNSCFStatusChecker(BaseStatusCheckerTest):
    _calculation = QEPWNscfPositiveKptsCalc


class TestQEQ2RStatusChecker(BaseStatusCheckerTest):
    _calculation = QEQ2RCalc
