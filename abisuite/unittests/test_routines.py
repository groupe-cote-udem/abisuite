import os

import numpy as np

import pytest

from ..routines import (
        full_abspath,
        is_2d_arr,
        is_list_like, is_scalar_or_str,
        is_vector,
        return_list,
        )


class TestSimpleRoutines:
    """Test cases for the routines."""

    @pytest.mark.parametrize(
            "lst, is_lst_like",
            [([], True),
             ((0, 1), True),
             (np.array((0, 1)), True),
             (set((0, 1)), True),
             ("string", False),
             (0, False),
             (-1.0, False),
             (None, False),
             (True, False)
             ],
            )
    def test_is_list_like(self, lst, is_lst_like):
        if is_lst_like:
            assert is_list_like(lst)
        else:
            assert not is_list_like(lst)

    @pytest.mark.parametrize(
            "obj, ok",
            [("string", True),
             (1, True),
             (-10.0, True),
             ([], False),
             ((0, 1), False),
             (set((0, 1)), False),
             (False, False),
             (None, False),
             (np.array((0, 1)), False),
             ],
            )
    def test_is_scalar_or_str(self, obj, ok):
        if ok:
            assert is_scalar_or_str(obj)
        else:
            assert not is_scalar_or_str(obj)

    def test_full_abspath(self):
        assert os.path.isabs(full_abspath("~"))

    @pytest.mark.parametrize(
            "obj, res",
            [(None, []), ("string", ["string"]),
             (["already_list"], ["already_list"]),
             ],
            )
    def test_return_list(self, obj, res):
        assert return_list(obj) == res

    @pytest.mark.parametrize(
            "obj, ok, length",
            [
                ([], True, None),
                ((0, 1), True, None),
                (np.array((0, 1)), True, None),
                (set((0, 1)), True, None),
                ("string", False, None),
                (0, False, None),
                (-1.0, False, None),
                (None, False, None),
                (True, False, None),
                ([0, 0], True, 2),
                ([0, 0], False, 1),
                ([0, 0], False, 3),
                ],
            )
    def test_is_vector(self, obj, ok, length):
        res = is_vector(obj, length=length)
        if ok:
            assert res
        else:
            assert not res

    @pytest.mark.parametrize(
            "obj, res, size",
            [([[]], True, None),
             (((0, 1), (1, 0)), True, None),
             (np.array(((0, 1), (0, 1))), True, None),
             (set(((0, 1), (0, 2))), True, None),
             ("string", False, None),
             (0, False, None),
             (-1.0, False, None),
             (None, False, None),
             (True, False, None),
             ([0, 1], False, None),
             ((0, 1), False, None),
             (set((0, 1)), False, None),
             ([[0, 0]], True, [1, 2]),
             ([[0]], True, [1, 1]),
             ([[0], [0]], True, [2, 1]),
             ([[0, 0]], False, [2, 2]),
             ([[0]], False, [1, 2]),
             ([[0], [0]], False, [1, 1]),
             ],
            )
    def test_is_2d_arr(self, obj, res, size):
        result = is_2d_arr(obj, size=size)
        if res:
            assert result
        else:
            assert not result
