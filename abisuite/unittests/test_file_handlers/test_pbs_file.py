import os
import random
import string

import pytest

from .bases import BasePathConvertibleFileHandlerTest
from ..routines_for_tests import create_pbs_test_writer
from ...handlers import PBSFile
from ...handlers.file_approvers.exceptions import MPIError
from ...linux_tools import touch


class TestPBSFile(BasePathConvertibleFileHandlerTest):
    _file_handler_class = PBSFile

    @pytest.fixture(autouse=True)
    async def setup_pbs_handler(self, setup_writable_handler):
        create_pbs_test_writer(self.handler)
        # create artificial script file
        self._command_file = os.path.join(
            self.tempdir, "abinit")
        await touch(self._command_file)
        self.handler.command = self._command_file
        self.handler.lines_before = ["cd $PBS_O_WORKDIR"]

    async def test_validating_errors(self):
        await self.handler.write()
        self.handler.nodes = 1
        self.handler.ppn = 1
        self.handler.queuing_system = "torque"
        self.handler.mpi_command = "mpirun -np 2"
        with pytest.raises(MPIError):
            await self.handler.validate()

    @pytest.mark.parametrize(
            "cmd, args, cmd_attr, arg_attr",
            [("/cmd",
             {"-arg1": 1, "--arg2": 2, "-arg3": "", "-arg4=": 9}.copy(),
             "command", "command_arguments"),
             ("/mpi_cmd",
              {"-arg1": 1, "--arg2": 2, "-arg3": "", "-arg4=": 9}.copy(),
              "mpi_command", "mpi_command_arguments"),
             ],
            )
    async def test_commands_and_arguments(self, cmd, args, cmd_attr, arg_attr):
        # test the command and arguments properties and setters
        await self.handler.write()
        tojoin = [cmd]
        for arg, value in args.items():
            value = str(value)
            if "=" in arg:
                tojoin.append(arg + str(value))
                continue
            if not len(value):
                tojoin.append(arg)
                continue
            else:
                tojoin.append(arg + " " + str(value))
                continue
        full_cmd = " ".join(tojoin)
        # test command
        setattr(self.handler, cmd_attr, full_cmd)
        assert getattr(self.handler, cmd_attr) == full_cmd
        assert getattr(self.handler, arg_attr) == args
        # test string handling for args
        # pop an element and check if command is respected
        args.pop(list(args.keys())[0])
        setattr(self.handler, arg_attr, args)
        assert getattr(self.handler, arg_attr) == args
        argsstr = []
        for arg, value in args.items():
            if "=" in arg:
                argsstr.append(arg + str(value))
                continue
            if not len(str(value)):
                argsstr.append(arg)
                continue
            else:
                argsstr.append(arg + " " + str(value))
                continue
        argsstr = " ".join(argsstr)
        setattr(self.handler, arg_attr, argsstr)
        assert getattr(self.handler, arg_attr) == args
        assert getattr(self.handler, cmd_attr) == cmd + " " + argsstr

    async def _change_random_property(self, handler):
        # change the command as it is a property shared by any pbs file
        # generate random jobname. Taken from:
        # https://stackoverflow.com/a/2257449/6362595
        allchars = string.ascii_letters + string.digits
        handler.command = ''.join(random.sample(allchars, 10))
