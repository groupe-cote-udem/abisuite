import pytest

from .bases import BaseFileHandlerTest
from ...handlers import GenericFile


class TestGenericFileHandler(BaseFileHandlerTest):
    _file_handler_class = GenericFile

    @pytest.fixture
    def pre_setup_generic_handler(self, named_tempfile):
        self._example_file = named_tempfile.name

    @pytest.fixture(autouse=True)
    def setup_generic_handler(self, pre_setup_generic_handler, setup_handler):
        pass
