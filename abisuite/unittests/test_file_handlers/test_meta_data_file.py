import json
import os
import random
import string

import aiofiles

import pytest

from .bases import BasePathConvertibleFileHandlerTest
from ...handlers import MetaDataFile
from ...handlers.file_structures import MetaDataStructure
from ...launchers import __ALL_LAUNCHERS__
from ...linux_tools import mkdir


class TestMetaDataFile(BasePathConvertibleFileHandlerTest):
    """Test case for the meta data file handler.
    """
    _file_handler_class = MetaDataFile

    @pytest.fixture(autouse=True)
    async def setup_meta_data_handler(self, setup_writable_handler):
        for prop in MetaDataStructure.all_attributes:
            if prop in MetaDataStructure.set_attributes:
                continue
            if prop in MetaDataStructure.list_attributes:
                continue
            setattr(self.handler, prop, prop)

    async def test_path_reconstruction(self):
        await self.handler.write()
        # check path reconstruction by changing the calc_workdir
        self.handler.calc_workdir = os.path.join(self.tempdir,
                                                 "new_workdir")
        for attr in self.handler.structure.convertible_path_attributes:
            handler_attr = getattr(self.handler, attr)
            assert os.path.commonpath(
                    (self.handler.calc_workdir, handler_attr)) == (
                    self.handler.calc_workdir), f"{attr}: {handler_attr}"

    async def test_last_modified_property(self):
        # check that last_modified property changes when rewritting file
        await self.handler.write()
        async with await self._file_handler_class.from_file(
                self.handler.path) as meta:
            await meta.read()
            last_modified = meta.last_modified
            await self._change_random_property(meta)
        async with await self._file_handler_class.from_file(
                self.handler.path) as meta:
            await meta.read()
            assert meta.last_modified != last_modified

    @pytest.mark.parametrize(
            "calctype, launcher_class",
            [(calc, lcls)
             for calc, lcls in __ALL_LAUNCHERS__.items()
             if calc != "abinit_cut3d"],
            )
    async def test_init_from_launcher_or_calculation(
            self, calctype, launcher_class):
        workdir = os.path.join(
            self.tempdir,
            str(launcher_class).replace(
                " ", "_").replace(
                "'", "").replace("<", "").replace(">", ""))
        jobname = "test"
        launcher = launcher_class(jobname)
        # create fake script
        suffix = launcher.calctype
        if "abinit" not in suffix:
            suffix += ".x"
        async with aiofiles.tempfile.NamedTemporaryFile(
                suffix=suffix) as cmd_file:
            await launcher.set_workdir(workdir)
            launcher.command = cmd_file.name
            meta = self._file_handler_class.from_launcher(launcher)
            assert meta.calc_workdir == launcher.workdir
            assert meta.path == os.path.join(
                    launcher.workdir, "." + launcher.jobname +
                    meta._parser_class._expected_ending)
            del meta
            # check that trying to get meta file from non existent calculation
            # raises an error
            with pytest.raises(NotADirectoryError):
                await self._file_handler_class.from_calculation(
                        launcher.workdir)
            # just create the top directory
            await mkdir(launcher.workdir)
            with pytest.raises(FileNotFoundError):
                await self._file_handler_class.from_calculation(
                        launcher.workdir)
            launcher.meta_data_file.submit_time = "now"
            launcher.meta_data_file.input_file_path = launcher.input_file.path
            launcher.meta_data_file.pbs_file_path = launcher.pbs_file.path
            sifp = launcher.pbs_file.input_file_path
            launcher.meta_data_file.script_input_file_path = sifp
            await launcher.meta_data_file.write()
            wd = launcher.workdir
            async with await self._file_handler_class.from_calculation(
                    wd) as meta:
                assert meta.calctype == launcher.calctype

    async def test_data_integrity_checker(self):
        # test not a dict error in meta file
        async with aiofiles.open(self.handler.path, "w") as f:
            await f.write(json.dumps(1))
        with pytest.raises(TypeError):
            await self.handler.read()
        # test that an unknown variable raises an error
        async with aiofiles.open(self.handler.path, "w") as f:
            json.dump({"unexpected key": "wtf"}, f)
        with pytest.raises(ValueError):
            await self.handler.read()

    async def _change_random_property(self, handler):
        # change the jobname only
        # generate random jobname. Taken from:
        # https://stackoverflow.com/a/2257449/6362595
        allchars = string.ascii_letters + string.digits
        handler.jobname = ''.join(random.sample(allchars, 10))
