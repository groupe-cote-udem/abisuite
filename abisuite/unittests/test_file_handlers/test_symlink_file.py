import aiofiles

import pytest

from .bases import BasePathConvertibleFileHandlerTest
from ...handlers import SymLinkFile


class TestSymLinkFile(BasePathConvertibleFileHandlerTest):
    """Test case for a symlink file handler.
    """
    _file_handler_class = SymLinkFile

    @pytest.fixture(autouse=True)
    async def setup_symlink_handler(self, setup_writable_handler):
        # create source file
        async with aiofiles.tempfile.NamedTemporaryFile(
                dir=self.tempdir, delete=False) as source:
            self.handler.source = source.name
            yield

    async def _change_random_property(self, handler):
        # only one property to change is source
        async with aiofiles.tempfile.NamedTemporaryFile(
                dir=self.tempdir,
                delete=False) as source:
            handler.source = source.name
