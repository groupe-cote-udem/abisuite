import os

import pytest

from .bases import (
        BaseFileHandlerTest,
        BaseInputFileHandlerTest,
        BasePathConvertibleFileHandlerTest,
        )
from ..variables_for_tests import (
        abinit_anaddb_log_example,
        abinit_anaddb_phfrq_example,
        abinit_anaddb_vars,
        abinit_dmft_hamiltonian_example,
        abinit_dmft_projectors_example,
        abinit_gs_input_variables,
        abinit_gs_output_file,
        abinit_mrgddb_log_example,
        abinit_optic_input_variables,
        )
from ...handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile, AbinitAnaddbLogFile,
        AbinitAnaddbPhfrqFile, AbinitDMFTEigFile, AbinitDMFTProjectorsFile,
        AbinitInputFile, AbinitMrgddbInputFile,
        AbinitMrgddbLogFile, AbinitOpticFilesFile, AbinitOpticInputFile,
        AbinitOutputFile,
        )


class BaseFilesHandlerTest(BasePathConvertibleFileHandlerTest):
    """Base test case class for all files file handler classes.
    """

    @pytest.fixture(autouse=True)
    def setup_files_handler(self, setup_writable_handler):
        self.handler.input_file_path = os.path.join(self.tempdir,
                                                    "input")
        self.handler.output_file_path = os.path.join(self.tempdir,
                                                     "output")

    async def _change_random_property(self, handler):
        handler.output_file_path += "_new"


class TestAbinitAnaddbFilesFile(BaseFilesHandlerTest):
    """Test case for an anaddb files file handler.
    """
    _file_handler_class = AbinitAnaddbFilesFile

    @pytest.fixture(autouse=True)
    def setup(self, setup_files_handler):
        self.handler.ddb_file_path = "test_ddb"
        self.handler.band_structure_file_path = "test_band_structure"
        self.handler.gkk_file_path = "test_gkk"
        self.handler.eph_data_prefix = "test_eph_data_prexif"
        self.handler.ddk_file_path = "test_ddk"


class TestAbinitAnaddbInputFile(BaseInputFileHandlerTest):
    """Test case for the anaddb input file handler.
    """
    _file_handler_class = AbinitAnaddbInputFile
    _input_vars = abinit_anaddb_vars.copy()

    async def _change_random_property(self, handler):
        handler.input_variables["ifcout"] += 1


class TestAbinitAnaddbLogFile(BaseFileHandlerTest):
    """Test case for an anaddb log file handler.
    """
    _file_handler_class = AbinitAnaddbLogFile
    _example_file = abinit_anaddb_log_example


class TestAbinitAnaddbPhfrqFile(BaseFileHandlerTest):
    """Test case for an anaddb PHFRQ file handler.
    """
    _file_handler_class = AbinitAnaddbPhfrqFile
    _example_file = abinit_anaddb_phfrq_example


class TestAbinitInputFile(BaseInputFileHandlerTest):
    _file_handler_class = AbinitInputFile
    _input_vars = abinit_gs_input_variables.copy()

    async def _change_random_property(self, handler):
        handler.input_variables["ecut"] += 1.0


class TestAbinitMrgddbInputFile(
        BasePathConvertibleFileHandlerTest):
    _file_handler_class = AbinitMrgddbInputFile

    @pytest.fixture(autouse=True)
    def setup_abinit_mrgddb_input_handler(self, setup_writable_handler):
        self.handler.output_file_path = "test_output"
        self.handler.title = "test title"
        self.handler.ddb_paths = ["ddb1_path", "ddb2_path", "ddb3_path"]

    async def _change_random_property(self, handler):
        handler.title += " new"


class TestAbinitMrgddbLogFile(BaseFileHandlerTest):
    _file_handler_class = AbinitMrgddbLogFile
    _example_file = abinit_mrgddb_log_example


class TestAbinitOutputFile(BaseFileHandlerTest):
    _file_handler_class = AbinitOutputFile
    _example_file = abinit_gs_output_file


class TestAbinitOpticInputFile(BaseInputFileHandlerTest):
    _file_handler_class = AbinitOpticInputFile
    _input_vars = abinit_optic_input_variables.copy()

    async def _change_random_property(self, handler):
        handler.input_variables["broadening"] += 0.01


class TestAbinitOpticFilesFile(BaseFilesHandlerTest):
    """Test case for the abinit optic files file handler.
    """
    _file_handler_class = AbinitOpticFilesFile

    @pytest.fixture(autouse=True)
    def setup_abinit_optic_files_handler(self, setup_files_handler):
        self.handler.output_data_prefix = "data"
        self.handler.output_data_dir = self.tempdir


class TestAbinitDMFTEigFile(BaseFileHandlerTest):
    _file_handler_class = AbinitDMFTEigFile
    _example_file = abinit_dmft_hamiltonian_example


class TestAbinitDMFTProjectorsFile(BaseFileHandlerTest):
    _file_handler_class = AbinitDMFTProjectorsFile
    _example_file = abinit_dmft_projectors_example
