# from ..variables_for_tests import (
#         abinit_vars, optic_vars, qe_dosx_vars, qe_dynmatx_vars,
#         qe_epsilonx_vars, qe_fsx_vars, qe_matdynx_vars, qe_phx_vars,
#         qe_ppx_vars, qe_projwfcx_vars, qe_pwx_vars, qe_q2rx_vars,
#         PWCalc, PHCalc, Q2RCalc
#         )
# from ...launchers import ALL_LAUNCHERS
import abc
import os

import aiofiles.os

import aioshutil

import pytest

from ...exceptions import DevError
from ...routines import is_list_like

# # order match the list in launchers/__init__.py
# ALL_TEST_VARS = [abinit_vars, optic_vars, qe_dosx_vars, qe_dynmatx_vars,
#                  qe_epsilonx_vars, qe_fsx_vars, qe_matdynx_vars, qe_phx_vars,
#                  qe_ppx_vars, qe_projwfcx_vars, qe_pwx_vars, qe_q2rx_vars]
# ALL_TEST_LINKS = [None, None, PWCalc, PHCalc, PWCalc, PWCalc, Q2RCalc,
#                   PWCalc, PWCalc, PWCalc, None, PHCalc]


class ManualTestException(Exception):
    """Custom exception to be raised during tests.

    Defined this to be a really specific exception that is raised
    manually in a try except statement. Use it to make sure other exceptions
    are raised in the appropriate way.
    """

    pass


class BaseFileHandlerTest(abc.ABC):
    """Base file handler test class."""

    _file_handler_class = None
    _example_file = None

    @pytest.fixture(autouse=True)
    async def setup_handler(self, tempdir):
        """Setup fixture for handlers."""
        if self._file_handler_class is None:
            raise DevError("Need to set '_file_handler_class'.")
        self.tempdir = tempdir
        self.handler = self._file_handler_class()
        if self._example_file is not None:
            newpath = os.path.join(self.tempdir,
                                   os.path.basename(self._example_file))
            await aioshutil.copy2(self._example_file, newpath)
            await self.handler.set_path(newpath)
            await self.handler.read()

    async def test_dirname_property(self):
        """Test that the dirname property returns the right dirname."""
        assert self.handler.dirname == os.path.dirname(self.handler.path)
        await self.handler.rename("new_file_rename")
        assert self.handler.dirname == os.path.dirname(self.handler.path)
        await self.handler.move(
                os.path.join(self.handler.dirname, "new_file_after_moving"))
        assert self.handler.dirname == os.path.dirname(self.handler.path)
        await self.handler.copy(
                os.path.join(self.handler.dirname, "new_copied"))
        assert self.handler.dirname == os.path.dirname(self.handler.path)
        await self.handler.bump_copy()
        assert self.handler.dirname == os.path.dirname(self.handler.path)

    async def test_bump_copy(self):
        """Checks that the bump_rename method works as intended."""
        oldpath = self.handler.path
        await self.handler.bump_copy()
        assert await aiofiles.os.path.isfile(oldpath + "_0001")
        await self.handler.bump_copy()
        assert await aiofiles.os.path.isfile(oldpath + "_0002")

    async def test_cat(self):
        """Test the cat method of file handlers."""
        if self._example_file is None:
            return
        # make sure this does not raise an error
        assert await self.handler.cat() is None

    async def test_copy(self):
        """Test the copy method of file handlers."""
        newfilepath = os.path.join(self.tempdir, "test_copy_file")
        await self.handler.copy(newfilepath, overwrite=True)
        assert await aiofiles.os.path.isfile(newfilepath)

    async def test_copy_handler(self):
        """Test the copy_handler method of file handlers."""
        copy = await self.handler.copy_handler()
        assert copy == self.handler

    async def test_delete(self):
        """Test the delete method of file_handlers."""
        await self.handler.delete()
        assert not await aiofiles.os.path.isfile(self.handler.path)

    async def test_move(self):
        """Test the move method of file handlers."""
        newfilepath = os.path.join(self.tempdir, "test_move_file")
        oldpath = self.handler.path
        await self.handler.move(newfilepath, overwrite=True)
        assert await aiofiles.os.path.isfile(newfilepath)
        assert not await aiofiles.os.path.isfile(oldpath)
        assert newfilepath == self.handler.path

    async def test_read(self):
        """Test the read method of file handlers."""
        # we expect that this should not raise an error
        assert await self.handler.read() is None

    async def test_rename(self):
        """Checks that the rename method works as intended."""
        new_name = "test_new_name"
        dirname = self.handler.dirname
        oldpath = self.handler.path
        newpath = os.path.join(dirname, new_name)
        await self.handler.rename(new_name)
        assert not await aiofiles.os.path.isfile(oldpath)
        assert await aiofiles.os.path.isfile(newpath)
        assert self.handler.path == newpath

    # this test does not work on all file handlers....
    # def test_init_from_calculation(self):
    #     for (launcher_class, input_vars, links) in zip(ALL_LAUNCHERS,
    #                                                    ALL_TEST_VARS,
    #                                                    ALL_TEST_LINKS):
    #         classname = str(launcher_class).split(".")[-1].split("'")[0]
    #         workdir = os.path.join(self.tempdir.name,
    #                                classname)
    #         jobname = "test"
    #         launcher = launcher_class(jobname, loglevel=1)
    #         # create fake script
    #         suffix = launcher.calctype
    #         if "qe_" in suffix:
    #             suffix += ".x"
    #         script = tempfile.NamedTemporaryFile(suffix=suffix)
    #         launcher.workdir = workdir
    #         launcher.command = script.name
    #         # check that trying to get meta file from
    #         # non existent calculation
    #         # raises an error
    #         with self.assertRaises(NotADirectoryError):
    #             self._file_handler_class.from_calculation(launcher.workdir)
    #         launcher.init_calculation_tree()
    #         with self.assertRaises(FileNotFoundError):
    #             self._file_handler_class.from_calculation(launcher.workdir)
    #         # write calculation
    #         launcher.input_variables = input_vars.copy()
    #         if links is not None:
    #             launcher.link_calculation(links)
    #         launcher.queuing_system = "local"
    #         launcher.write(bypass_validation=True)
    #         wd = launcher.workdir
    #         with self._file_handler_class.from_calculation(wd) as handler:
    #             self.assertTrue(isinstance(handler,
    #                             self._file_handler_class))


class BaseWritableFileHandlerTest(BaseFileHandlerTest, abc.ABC):
    """Base test case class for handlers that can be written."""

    @pytest.fixture(autouse=True)
    async def setup_writable_handler(self, setup_handler):
        """Setup fixture for writable file handler tests."""
        self.path = os.path.join(self.tempdir, "test")
        await self.handler.set_path(self.path)

    async def test_bump_copy(self):
        """Test for the bump_copy method of writable file handlers."""
        await self.handler.write()
        await super().test_bump_copy()

    async def test_copy(self):
        """Test for the copy method of writable file handlers."""
        await self.handler.write()
        await super().test_copy()

    async def test_copy_handler(self):
        """Test copy handler method of file handlers."""
        await self.handler.write()
        await super().test_copy_handler()
        prev = await self.handler.copy_handler()
        await self._change_random_property(self.handler)
        assert self.handler != prev

    async def test_context_manager(self):
        """Test the context manager of file_handlers."""
        # test that nothing changed if nothing changed
        await self.handler.write()
        previous = await self.handler.copy_handler()
        async with await self._file_handler_class.from_file(
                self.handler.path) as handler:
            await handler.read()
            assert previous == handler
        # test something changed
        async with self.handler as handler:
            await self._change_random_property(handler)
        # read new
        newhandler = await self._file_handler_class.from_file(
                self.handler.path)
        await newhandler.read()
        assert previous != newhandler, f"{previous} ==\n\n {newhandler}"
        # test nothing changed if an error is raised inside the context manager
        previous = await self.handler.copy_handler()
        try:
            async with self.handler as handler:
                await self._change_random_property(handler)
                raise ManualTestException
        except ManualTestException:
            pass
        newhandler = await self._file_handler_class.from_file(
                self.handler.path)
        await newhandler.read()
        assert previous == newhandler, f"{previous}\n!=\n{newhandler}"

    async def test_dirname_property(self):
        """Test the dirname property of writable file handlers."""
        await self.handler.write()
        await super().test_dirname_property()

    async def test_move(self):
        """Test the move method of writable file handlers."""
        await self.handler.write()
        await super().test_move()

    async def test_read(self):
        """Test the read method of writable file handlers."""
        await self.handler.write()
        await super().test_read()

    async def test_rename(self):
        """Test the rename method of writable file handlers."""
        await self.handler.write()
        await super().test_rename()

    async def test_write(self):
        """Test the write method of file handlers."""
        await self.handler.write()
        async with await self._file_handler_class.from_file(
                self.handler.path) as handler:
            await handler.read()
            assert handler == self.handler, f"{handler}\n!=\n{self.handler}"

    @abc.abstractmethod
    async def _change_random_property(self, handler):  # pragma: no cover
        pass


class BasePathConvertibleFileHandlerTest(BaseWritableFileHandlerTest):
    """Base test case class for path convertible file handlers."""

    async def test_path_conversion(self):
        """Checks that path conversion works as expected."""
        await self.handler.write()

        def check_conversion(absolute):
            for attr in self.handler.structure.convertible_path_attributes:
                value = getattr(self.handler, attr)
                if is_list_like(value):
                    for path in value:
                        if absolute:
                            assert os.path.isabs(path), path
                        else:
                            assert os.path.isabs(path), path
                else:
                    if absolute:
                        assert os.path.isabs(value), value
                    else:
                        assert not os.path.isabs(value), value
        # test that paths conversion works
        self.handler.convert_to_relative_paths()
        check_conversion(False)
        relcopy = await self.handler.copy_handler()
        self.handler.convert_to_absolute_paths()
        check_conversion(True)
        # convert to relative path and compare
        # convert back and check everything is the same
        self.handler.convert_to_relative_paths()
        assert relcopy == self.handler


class BaseInputFileHandlerTest(BaseWritableFileHandlerTest):
    """Base class for input file handlers tests."""

    _inputs_vars = None

    @pytest.fixture(autouse=True)
    def setup_input_handler(self, setup_writable_handler):
        """Setup fixture for input file handlers tests."""
        self.handler.input_variables = self._input_vars
