import os

import aiofiles.os

import pytest

from .bases import BaseWriterTest
from ...handlers.file_parsers import SymLinkParser
from ...handlers.file_writers import SymLinkWriter
from ...linux_tools import touch


class TestSymLinkWriter(BaseWriterTest):
    """Test case for the symlink file writer.
    """
    _parser_class = SymLinkParser
    _writer_class = SymLinkWriter

    @pytest.fixture(autouse=True)
    async def setup_symlink_writer(self, setup_writer):
        # need to create file to link (source)
        self.source = os.path.join(self.tempdir, "test_symlink")
        await touch(self.source)
        self.writer.source = self.source

    async def test_writing(self):
        await super().test_writing()
        assert await aiofiles.os.path.islink(self.writer.path)
        assert await aiofiles.os.path.samefile(
                await aiofiles.os.readlink(self.writer.path), self.source)

    async def test_dirlink(self):
        # check if source is a directory instead of a file
        async with aiofiles.tempfile.TemporaryDirectory(
                dir=self.tempdir) as temp:
            self.writer.source = temp
            # check that a direct write creates a symlink dir
            await self.writer.write(overwrite=True)
            assert await aiofiles.os.path.islink(self.writer.path)
            assert await aiofiles.os.path.samefile(
                await aiofiles.os.readlink(self.writer.path), temp)
