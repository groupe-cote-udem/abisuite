import os

import aiofiles

import pytest

from ...exceptions import DevError
from ...routines import is_list_like

# TODO: make a global test with corresponding parser to make sure everything
# has been well written


class BaseWriterTest:
    """Base class for any writer test case."""

    _parser_class = None
    _writer_class = None

    @pytest.fixture
    async def setup_writer(self, tempdir):
        """Setup writer instance."""
        if self._parser_class is None:
            raise DevError(
                    f"Need to set '_parser_class' for '{self.__class__}'.")
        if self._writer_class is None:
            raise DevError(
                    f"Need to set '_writer_class' for '{self.__class__}'.")
        self.tempdir = tempdir
        self.writer = self._writer_class(loglevel=1)
        if is_list_like(self._parser_class._expected_ending):
            # take first one only
            ext = self._parser_class._expected_ending[0]
        else:
            ext = self._parser_class._expected_ending
        self.writer.path = os.path.join(
                        self.tempdir,
                        "test" + ext)

    async def test_writing(self):
        """Simple test writer to write file."""
        await self.writer.write()
        # check that file exists
        assert await aiofiles.os.path.isfile(self.writer.path)
        # check that it has written everything it had to
        parser = await self._parser_class.from_file(self.writer.path)
        await parser.read()
        assert parser.structure == self.writer.structure, (
               f"{parser.structure} != {self.writer.structure}")

    async def test_error_prevent_overwrite(self):
        """Test that an error is raised when file already exists."""
        # check that trying to overwrite it raises an error
        await self.writer.write(overwrite=True)  # just do it in case it wasnt
        # done already
        with pytest.raises(FileExistsError):
            await self.writer.write()

    async def test_path(self):
        """Test the path property."""
        # # test that suffix is added if not set
        # self.writer.path = os.path.join(self.tempdir.name, "test")
        # suffix = self._writer_class._file_suffix
        # self.assertTrue(self.writer.path.endswith(suffix))
        # reset path
        await self.writer.write()
        self.writer._path = None
        # test that not setting path raises an error
        with pytest.raises(ValueError):
            self.writer.path

    async def test_workdir(self):
        """Test the workdir property."""
        # test that even if workdir not set, it is automatically set
        # from path
        await self.writer.write()
        assert self.writer.workdir == self.tempdir


class BaseInputWriterTest(BaseWriterTest):
    """Base input file writer tests for an input file with input vars."""

    _input_vars = None

    @pytest.fixture(autouse=True)
    def setup_input_writer(self, setup_writer):
        """Setup input file writer fixture."""
        if self._input_vars is None:
            raise DevError(
                    f"Need to set '_input_vars' in '{self.__class__}'.")
        self.writer.input_variables = self._input_vars
