import os

import pytest

from .bases import (
        BaseInputWriterTest,
        BaseWriterTest,
        )
from ..variables_for_tests import (
        abinit_anaddb_vars,
        abinit_gs_input_variables,
        abinit_optic_input_variables,
        )
from ...handlers.file_parsers import (
        AbinitAnaddbFilesParser, AbinitAnaddbInputParser,
        AbinitInputParser,
        AbinitMrgddbInputParser,
        AbinitOpticFilesParser,
        AbinitOpticInputParser,
        )
from ...handlers.file_writers import (
        AbinitAnaddbFilesWriter, AbinitAnaddbInputWriter,
        AbinitInputWriter,
        AbinitMrgddbInputWriter,
        AbinitOpticFilesWriter,
        AbinitOpticInputWriter,
        )


class BaseFilesWriterTest(BaseWriterTest):
    """Base class for Test cases of abinit files file writer classes.
    """

    @pytest.fixture(autouse=True)
    def setup_files_writer(self, setup_writer):
        self.writer.path = os.path.join(self.tempdir, "test.files")
        self.writer.input_file_path = "test.in"
        self.writer.output_file_path = "test.out"


class TestAbinitAnaddbFilesWriter(BaseFilesWriterTest):
    """Test case for the anaddb files file writer.
    """
    _parser_class = AbinitAnaddbFilesParser
    _writer_class = AbinitAnaddbFilesWriter

    @pytest.fixture(autouse=True)
    def setup_abinit_anaddb_files_writer(self, setup_files_writer):
        self.writer.ddb_file_path = "test_ddb"
        self.writer.band_structure_file_path = "test_band_structure"
        self.writer.gkk_file_path = "test_gkk"
        self.writer.eph_data_prefix = "test_eph_data_prefix"
        self.writer.ddk_file_path = "test_ddk"


class TestAbinitAnaddbInputWriter(BaseInputWriterTest):
    _input_vars = abinit_anaddb_vars
    _parser_class = AbinitAnaddbInputParser
    _writer_class = AbinitAnaddbInputWriter


class TestAbinitMrgddbInputWriter(BaseWriterTest):
    """Test case for the mrgddb input file writer.
    """
    # NOTE: we use BaseWriterTest instead of BaseInputWriterTest because mrgddb
    # input files don't have input variables.
    _parser_class = AbinitMrgddbInputParser
    _writer_class = AbinitMrgddbInputWriter

    @pytest.fixture(autouse=True)
    def setup_abinit_mrgddb_input_writer(self, setup_writer):
        self._properties = {
                "output_file_path": "test_output",
                "title": "test_title",
                "ddb_paths": ["test_ddb_path1", ],
                }
        for attr, value in self._properties.items():
            setattr(self.writer, attr, value)


class TestAbinitOpticFilesWriter(BaseFilesWriterTest):
    """Test case for the optic files file writer.
    """
    _parser_class = AbinitOpticFilesParser
    _writer_class = AbinitOpticFilesWriter

    @pytest.fixture(autouse=True)
    async def setup_abinit_optic_files_writer(self, setup_files_writer):
        self.writer.output_data_dir = self.tempdir
        self.writer.output_data_prefix = "odat_test"


class TestAbinitInputWriter(BaseInputWriterTest):
    _input_vars = abinit_gs_input_variables
    _parser_class = AbinitInputParser
    _writer_class = AbinitInputWriter


class TestAbinitOpticInputWriter(BaseInputWriterTest):
    _input_vars = abinit_optic_input_variables
    _parser_class = AbinitOpticInputParser
    _writer_class = AbinitOpticInputWriter
