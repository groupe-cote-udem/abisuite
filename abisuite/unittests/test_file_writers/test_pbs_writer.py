import pytest

from .bases import BaseWriterTest
from ..routines_for_tests import create_pbs_test_writer
from ... import __IMPLEMENTED_QUEUING_SYSTEMS__
from ...handlers.file_parsers import PBSParser
from ...handlers.file_writers import PBSWriter
from ...linux_tools import cat


class TestPBSWriter(BaseWriterTest):
    """Test case for the PBS file writer.
    """
    _parser_class = PBSParser
    _writer_class = PBSWriter

    @pytest.fixture(autouse=True)
    def setup_pbs_writer(self, setup_writer):
        create_pbs_test_writer(self.writer)

    async def test_writing(self):
        await super().test_writing()
        # try writing it using all possible queuing systems
        for queuing_system in __IMPLEMENTED_QUEUING_SYSTEMS__:
            self.writer.queuing_system = queuing_system
            await self.writer.write(overwrite=True, recompute_lines=True)
            # compare result
            newpbs = await PBSParser.from_file(self.writer.path)
            await newpbs.read()
            assert self.writer.structure == newpbs.structure, (
                        f"{queuing_system}:\n{self.writer.structure}\n!="
                        f"\n{newpbs.structure}\n{await cat(self.writer.path)}"
                    )

    async def test_wrong_queuing_system(self):
        await self.writer.write()
        with pytest.raises(ValueError):
            self.writer.queuing_system = "wrong queuing system"
            await self.writer.write(overwrite=True, recompute_lines=True)

    async def test_grid_engine_raise_error_when_no_queue(self):
        await self.writer.write()
        with pytest.raises(ValueError):
            self.writer.queue = None
            self.writer.queuing_system = "grid_engine"
            await self.writer.write(overwrite=True, recompute_lines=True)
