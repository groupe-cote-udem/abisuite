import pytest

from ...exceptions import DevError
from ...handlers.file_approvers.exceptions import InputFileError


class BaseApproverTest:
    """Base approver test class."""

    _approver_class = None
    _exception = None

    @pytest.fixture(autouse=True)
    def setup_approver(self):
        """Setup fixture for approvers."""
        if self._approver_class is None:
            raise DevError(
                    f"Need to set '_approver_class' in '{self.__class__}'.")
        self.approver = self._approver_class()

    async def test_approver_works(self):
        """Test that the approver works."""
        # check that an error is raised looking of approver is valid before
        # being validated
        with pytest.raises(self._approver_class._exception):
            self.approver.is_valid
        # check that it works as expected using the variables
        await self.approver.validate()
        assert self.approver.is_valid, self.approver.errors
        assert len(self.approver.errors) == 0, self.approver.errors
        # raising errors should not raise anything
        self.approver.raise_errors()
        # check that if we manually append an error, it acts as expected
        self.approver.errors.append("error")
        assert not self.approver.is_valid
        with pytest.raises(self._approver_class._exception):
            self.approver.raise_errors()


class BaseInputApproverTest(BaseApproverTest):
    """Base input file approver test class."""

    _specific_mandatory_vars = None
    _variables = None
    _variables_db = None
    _pseudos = None

    @pytest.fixture(autouse=True)
    def setup_input_approver(self, setup_approver):
        """Setup fixture for input file approvers."""
        if self._variables is None:
            raise DevError(
                    f"Need to set '_variables' for '{self.__class__}'.")
        if self._variables_db is None:
            raise DevError(
                    f"Need to set '_variables_db' for '{self.__class__}'.")
        self.variables = self._variables.copy()
        # get mandatory variables
        man = []
        for var, params in self._variables_db.items():
            if params["mandatory"]:
                man.append(var)
        self._mandatory_vars = man
        self.approver.input_variables = self.variables
        if self._pseudos is not None:
            self.approver.pseudos = self._pseudos

    async def test_reset(self) -> None:
        """Test the reset method."""
        assert not self.approver._has_been_validated
        await self.approver.validate()
        assert self.approver._has_been_validated
        self.approver.reset()
        assert not self.approver._has_been_validated

    def test_no_dict_raise(self):
        """Test that an error is raised when input vars is not a dict."""
        with pytest.raises(TypeError):
            app = self._approver_class()
            app.input_variables = 1

    async def test_missing_mandatory_variable(self):
        """Test that approver spots missing variables."""
        manvars = list(self._mandatory_vars)
        if self._specific_mandatory_vars is not None:
            manvars += list(self._specific_mandatory_vars)
        for var in self._mandatory_vars:
            inputs = self._variables.copy()
            if "*" in var:
                # a wild card mandatory variable.
                splitted = var.split("*")
                start = splitted[0]
                end = splitted[1]
                topop = []
                for varname in inputs:
                    # pop them all!
                    if varname.startswith(start) and varname.endswith(end):
                        topop.append(varname)
                for varname in topop:
                    inputs.pop(varname, None)
            else:
                inputs.pop(var, None)
            approver = self._approver_class()
            approver.input_variables = inputs
            if self._pseudos is not None:
                approver.pseudos = self._pseudos
            await approver.validate()
            assert not approver.is_valid, (
                    f"missing {var} should raise an error.")
            assert len(approver.errors) >= 1
            with pytest.raises(InputFileError):
                approver.raise_errors()
            del approver
