import pytest

from .bases import BaseInputApproverTest
from ..variables_for_tests import (
        qe_dos_input_variables,
        qe_dynmatx_vars, qe_epsilonx_vars, qe_epwx_vars,
        qe_fsx_vars,
        qe_ld1x_vars,
        qe_matdynx_vars, qe_phx_vars, qe_ppx_vars, qe_projwfcx_vars,
        qe_pw2wannier90x_vars,
        qe_pwx_vars,
        qe_q2rx_vars,
        )
from ...handlers.file_approvers import (
        QEDOSInputApprover, QEDynmatInputApprover,
        QEEPWInputApprover,
        QEEpsilonInputApprover,
        QEFSInputApprover, QELD1InputApprover, QEMatdynInputApprover,
        QEPHInputApprover, QEPPInputApprover,
        QEPW2Wannier90InputApprover,
        QEPWInputApprover,
        QEProjwfcInputApprover,
        QEQ2RInputApprover,
        )
from ...variables.qe_variables import (
        ALL_QEDOS_VARIABLES, ALL_QEDYNMAT_VARIABLES, ALL_QEEPSILON_VARIABLES,
        ALL_QEEPW_VARIABLES,
        ALL_QEFS_VARIABLES, ALL_QELD1_VARIABLES, ALL_QEMATDYN_VARIABLES,
        ALL_QEPH_VARIABLES, ALL_QEPP_VARIABLES, ALL_QEPROJWFC_VARIABLES,
        ALL_QEPW2WANNIER90_VARIABLES,
        ALL_QEPW_VARIABLES,
        ALL_QEQ2R_VARIABLES,
        )


class TestQEDOSInputApprover(BaseInputApproverTest):
    _approver_class = QEDOSInputApprover
    _variables = qe_dos_input_variables.copy()
    _variables_db = ALL_QEDOS_VARIABLES


class TestQEDynmatInputApprover(BaseInputApproverTest):
    _approver_class = QEDynmatInputApprover
    _variables = qe_dynmatx_vars.copy()
    _variables_db = ALL_QEDYNMAT_VARIABLES


class TestQEEpsilonInputApprover(BaseInputApproverTest):
    _approver_class = QEEpsilonInputApprover
    _variables = qe_epsilonx_vars.copy()
    _variables_db = ALL_QEEPSILON_VARIABLES


class TestQEEPWInputApprover(BaseInputApproverTest):
    _approver_class = QEEPWInputApprover
    _variables = qe_epwx_vars.copy()
    _variables_db = ALL_QEEPW_VARIABLES


class TestQEFSInputApprover(BaseInputApproverTest):
    _approver_class = QEFSInputApprover
    _variables = qe_fsx_vars.copy()
    _variables_db = ALL_QEFS_VARIABLES


class TestQELD1InputApprover(BaseInputApproverTest):
    _approver_class = QELD1InputApprover
    _variables = qe_ld1x_vars.copy()
    _variables_db = ALL_QELD1_VARIABLES


class TestQEMatdynInputApprover(BaseInputApproverTest):
    _approver_class = QEMatdynInputApprover
    _variables = qe_matdynx_vars.copy()
    _variables_db = ALL_QEMATDYN_VARIABLES


class TestQEPHInputApprover(BaseInputApproverTest):
    _approver_class = QEPHInputApprover
    _variables = qe_phx_vars
    _variables_db = ALL_QEPH_VARIABLES


class TestQEPPInputApprover(BaseInputApproverTest):
    _approver_class = QEPPInputApprover
    _variables = qe_ppx_vars.copy()
    _variables_db = ALL_QEPP_VARIABLES


class TestQEProjwfcInputApprover(BaseInputApproverTest):
    _approver_class = QEProjwfcInputApprover
    _variables = qe_projwfcx_vars.copy()
    _variables_db = ALL_QEPROJWFC_VARIABLES


class TestQEPWInputApprover(BaseInputApproverTest):
    _approver_class = QEPWInputApprover
    _variables = qe_pwx_vars.copy()
    _variables_db = ALL_QEPW_VARIABLES

    async def test_verbosity_check_for_band_structure(self):
        self.approver.input_variables["calculation"] = "bands"
        self.approver.input_variables["verbosity"] = "high"
        await self.approver.validate()
        assert self.approver.is_valid, self.approver.errors
        # huge grid
        ks = [100, 100, 100, 1, 1, 1]
        self.approver.input_variables["k_points"].value["k_points"] = ks
        self.approver.input_variables["verbosity"] = "low"
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1

    async def test_lspinorb(self):
        # check good setting
        newvars = self._variables.copy()
        newvars["lspinorb"] = True
        newvars["noncolin"] = True
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert approver.is_valid, approver.errors
        # check noncolin absent
        newvars = self._variables.copy()
        newvars["lspinorb"] = True
        newvars.pop("noncolin", None)
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert not approver.is_valid
        # check noncolin false
        newvars = self._variables.copy()
        newvars["lspinorb"] = True
        newvars["noncolin"] = False
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert not approver.is_valid

    async def test_wrong_geometry(self):
        # check too much atoms for what is defined
        newvars = self._variables.copy()
        newvars["nat"] = 4  # too much
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert not approver.is_valid

        # check positions wrong
        newvars["nat"] = 1
        # vector not right
        with pytest.raises(ValueError):
            newvars["atomic_positions"] = {"parameter": "crystal",
                                           "positions": {"Si": [0.0, 0.0]}}
            approver = self._approver_class()
            approver.input_variables = newvars
        newvars["nat"] = 2
        # vector not right
        with pytest.raises(ValueError):
            newvars["atomic_positions"] = {"parameter": "crystal",
                                           "positions": {"Si": [[0.0, 0.0],
                                                                [0.0, 0.1, 0.1]
                                                                ]}}
            approver = self._approver_class()
            approver.input_variables = newvars

        # check wrong species
        newvars = self._variables.copy()
        newvars["ntyp"] = 2  # too much
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert not approver.is_valid

    async def test_ibrav_checks(self):
        newvars = self._variables.copy()
        newvars["ibrav"] = 0
        # pop it just to be sure
        newvars.pop("cell_parameters", None)
        approver = self._approver_class()
        approver.input_variables = newvars
        await approver.validate()
        assert not approver.is_valid


class TestQEPW2Wannier90InputApprover(
        BaseInputApproverTest):
    _approver_class = QEPW2Wannier90InputApprover
    _variables = qe_pw2wannier90x_vars.copy()
    _variables_db = ALL_QEPW2WANNIER90_VARIABLES


class TestQEQ2RInputApprover(BaseInputApproverTest):
    _approver_class = QEQ2RInputApprover
    _variables = qe_q2rx_vars.copy()
    _variables_db = ALL_QEQ2R_VARIABLES
