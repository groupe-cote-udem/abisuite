from unittest.mock import patch

import pytest

from .bases import BaseApproverTest
from ...handlers.file_approvers import PBSApprover
from ...handlers.file_approvers.exceptions import PBSError


class TestPBSApprover(BaseApproverTest):
    _approver_class = PBSApprover
    _exception = PBSError

    @pytest.fixture(autouse=True)
    def setup_pbs_approver(self, setup_approver):
        self.approver.queuing_system = "local"
        self.approver.command = "command"

    @patch("abisuite.handlers.file_approvers.pbs_approver.which")
    async def test_approver_works(self, whichmock):
        whichmock.return_value = "not None"
        await super().test_approver_works()

    @patch("abisuite.handlers.file_approvers.pbs_approver.which")
    async def test_command_approval(self, whichmock):
        whichmock.return_value = "not None"
        # check that a command exists in env but is not directly a file works
        self.approver.command = "some_command_without_a_direct_file"
        # check that if force local is True, nothing happens
        self.approver.queuing_system = "local"
        await self.approver.validate()
        assert self.approver.is_valid, self.approver.errors
        # check that nothing happens if it's not the case either
        self.approver.queuing_system = "torque"
        await self.approver.validate()
        assert self.approver.is_valid
        # check that an error is raised if which returns None
        whichmock.return_value = None
        await self.approver.validate()
        assert not self.approver.is_valid
