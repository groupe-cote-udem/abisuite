import os

import pytest

from .bases import BaseApproverTest, BaseInputApproverTest
from ..routines_for_tests import copy_calculation
from ..variables_for_tests import (
        abinit_anaddb_vars,
        abinit_gs_input_variables,
        abinit_gs_pseudos,
        abinit_gw_example,
        abinit_optic_input_variables,
        )
from ...handlers.file_approvers import (
        AbinitAnaddbInputApprover, AbinitInputApprover,
        AbinitMrgddbInputApprover, AbinitOpticInputApprover,
        )
from ...handlers.file_approvers.abinit_approvers.abinit_input_approver import (
        ATOMIC_POSITION_VARS,
        TOLERANCES_VARS,
        )
from ...handlers.file_approvers.exceptions import InputFileError
from ...variables.abinit_variables import (
        ALL_ABINITOPTIC_VARIABLES,
        ALL_ABINIT_ANADDB_VARIABLES,
        ALL_ABINIT_VARIABLES,
        )


class TestAbinitAnaddbInputApprover(BaseInputApproverTest):
    _approver_class = AbinitAnaddbInputApprover
    _variables = abinit_anaddb_vars.copy()
    _variables_db = ALL_ABINIT_ANADDB_VARIABLES


class TestAbinitInputApprover(BaseInputApproverTest):
    _approver_class = AbinitInputApprover
    _variables = abinit_gs_input_variables
    _variables_db = ALL_ABINIT_VARIABLES
    _pseudos = abinit_gs_pseudos

    async def test_kptgw_not_in_scf(self, tempdir):
        # check that asking to compute self energy at a kpt not computed
        # in the original (n)scf run yields an approver error
        # first copy GW workflow
        here = os.path.dirname(os.path.abspath(__file__))
        new_pseudos = [
                os.path.join(
                    here, "..", "test_sequencers",
                    "files", "pseudos", "Si.psp8"),
                ]
        await copy_calculation(
            os.path.join(abinit_gw_example, "scf_run"),
            os.path.join(tempdir, "scf_run"),
            new_pseudos=new_pseudos,
            )
        await copy_calculation(
            os.path.join(abinit_gw_example, "nscf_run"),
            os.path.join(tempdir, "nscf_run"),
            new_pseudos=new_pseudos,
            new_parents=[os.path.join(tempdir, "scf_run")],
            )
        await copy_calculation(
            os.path.join(abinit_gw_example, "screening_run"),
            os.path.join(tempdir, "screening_run"),
            new_pseudos=new_pseudos,
            new_parents=[os.path.join(tempdir, "nscf_run")],
            )
        # now define approver correct self energy calculation
        self.approver.structure.input_variables.update(
                {"nband": 100,
                 "ecutsigx": 8.0,
                 "nkptgw": 1,
                 "kptgw": [0.0, 0.0, 0.0],
                 "bdgw": [4, 5],
                 "optdriver": 4,
                 }
                )
        self.approver.parents = [
                os.path.join(tempdir, "screening_run"),
                os.path.join(tempdir, "nscf_run"),
                ]
        await self.approver.validate()
        assert self.approver.is_valid
        self.approver.reset()
        # insert invalid kpt
        self.approver.structure.input_variables.update({"kptgw": [0.1, 0, 0]})
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1

    async def test_tolwfr_if_nscf(self):
        self.approver.structure.add_input_variable("iscf", -2)
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1, self.approver.errors
        with pytest.raises(InputFileError):
            self.approver.raise_errors()

    async def test_one_tolerances(self):
        """Check that if more than one tolerance var is there, an error is
        raised.

        Also check that if no tolerance is there, an error is raised.
        """
        for tolerance in TOLERANCES_VARS:
            if tolerance in self.variables:
                continue
            inputs = self.variables.copy()
            inputs[tolerance] = 10e-6
            approver = self._approver_class()
            approver.input_variables = inputs
            approver.pseudos = abinit_gs_pseudos
            await approver.validate()
            assert not approver.is_valid, tolerance
            assert len(approver.errors) == 1, approver.errors
            with pytest.raises(InputFileError):
                approver.raise_errors()

        # check at least one tolerance is there
        for tolerance in TOLERANCES_VARS:
            if tolerance in self.approver.input_variables:
                self.approver.input_variables.pop(tolerance)
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1
        with pytest.raises(InputFileError):
            self.approver.raise_errors()

    async def test_one_atomic_position(self):
        # test that when there are more than 1 atomic position variable present
        # in input file, the approver raises an error.
        for atomic in ATOMIC_POSITION_VARS:
            if atomic in self.variables:
                continue
            inputs = self.variables.copy()
            inputs[atomic] = [[0.0, 0.0, 0.0], [1.0, 1.0, 1.0]]
            approver = self._approver_class()
            approver.input_variables = inputs
            approver.pseudos = abinit_gs_pseudos
            await approver.validate()
            assert not approver.is_valid
            assert len(approver.errors) == 1, approver.errors
            with pytest.raises(InputFileError):
                approver.raise_errors()

        # check that if no atomic position is present, an error is also raised
        for atomic in ATOMIC_POSITION_VARS:
            if atomic in self.variables:
                self.variables.pop(atomic)
                break
        self.approver.input_variables = inputs
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1
        with pytest.raises(InputFileError):
            self.approver.raise_errors()

    async def test_wrong_znucl_vs_pseudo(self):
        """For AlAs, check that setting a wrong znucl compared to pseudo
        raises an error.

        Actually 2 errors: one for each znucl
        """
        self.variables["znucl"] = [1, 2]
        self.approver.input_variables = self.variables
        await self.approver.validate()
        assert len(self.approver.errors) == 2
        with pytest.raises(InputFileError):
            self.approver.raise_errors()

    async def test_not_enough_bands(self):
        self.variables["nspinor"] = 2
        self.variables["nband"] = 1
        self.approver.input_variables = self.variables
        await self.approver.validate()
        assert not self.approver.is_valid
        with pytest.raises(InputFileError):
            self.approver.raise_errors()

    async def test_shiftk_not_consistent(self):
        # test nshiftk not well set
        self.variables["nshiftk"] = 0
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_gs_pseudos
        await approver.validate()
        assert not approver.is_valid
        with pytest.raises(InputFileError):
            approver.raise_errors()
        # test len(shiftk) != nshift
        self.variables["nshiftk"] = 2
        self.variables["shiftk"] = [0.5, 0.5, 0.5]
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_gs_pseudos
        await approver.validate()
        assert not approver.is_valid
        with pytest.raises(InputFileError):
            approver.raise_errors()
        self.variables["shiftk"] = [[0.5, 0.5, 0.5], [0.6, 0.6, 0.6],
                                    [0.7, 0.7, 0.7]]
        approver = self._approver_class()
        approver.input_variables = self.variables
        approver.pseudos = abinit_gs_pseudos
        await approver.validate()
        assert not approver.is_valid
        with pytest.raises(InputFileError):
            approver.raise_errors()


class TestAbinitMrgddbInputApprover(BaseApproverTest):
    """Test case for the abinit mrgddb input approver class.
    """
    _approver_class = AbinitMrgddbInputApprover
    _exception = InputFileError

    @pytest.fixture(autouse=True)
    def setup_abinit_mrgddb_input_approver(self, setup_approver):
        self.approver.output_file_path = "test_output_file"
        self.approver.ddb_paths = ["test_file1", "test_file2", ]

    async def test_not_enough_ddb_paths(self):
        """Test that when there is less than 2 ddb paths, the approver raises
        an error.
        """
        self.approver.ddb_paths = []
        await self.approver.validate()
        assert len(self.approver.errors) == 1
        with pytest.raises(self._exception):
            self.approver.raise_errors()


class TestAbinitOpticInputApprover(BaseInputApproverTest):
    _approver_class = AbinitOpticInputApprover
    _variables = abinit_optic_input_variables.copy()
    _variables_db = ALL_ABINITOPTIC_VARIABLES
