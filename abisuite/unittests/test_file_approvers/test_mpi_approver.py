import pytest

from .bases import BaseApproverTest
from ..variables_for_tests import BASIC_MPI_VARIABLES
from ...handlers.file_approvers import MPIApprover
from ...handlers.file_approvers.exceptions import MPIError


class TestMPIApprover(BaseApproverTest):
    _approver_class = MPIApprover
    _exception = MPIError

    @pytest.fixture(autouse=True)
    def setup_mpi_approver(self, setup_approver):
        self.approver.mpi_command = BASIC_MPI_VARIABLES["mpi_command"]
        self.approver.ppn = BASIC_MPI_VARIABLES["ppn"]
        self.approver.nodes = BASIC_MPI_VARIABLES["nodes"]
        self.approver.queuing_system = "torque"

    async def test_nothing_happens_if_no_mpi_command(self):
        approver = MPIApprover()
        await approver.validate()
        assert approver.is_valid

    async def test_nothing_happens_if_no_npernode_specified(self):
        self.approver.mpi_command = "mpirun"
        await self.approver.validate()
        assert self.approver.is_valid

        # test with -np instead of -npernode
        self.approver.mpi_command = "mpirun -np 4"
        await self.approver.validate()
        assert self.approver.is_valid
        # test that an empty command returns None as npernodes
        assert self.approver.extract_npernode(None) is None

    async def test_nodes(self):
        # test that an integer works instead of a str
        self.approver.nodes = 1
        self.approver.mpi_command = "mpirun"
        await self.approver.validate()
        assert self.approver.is_valid, self.approver.errors
        # test that a nodes string works too
        self.approver.nodes = "1"
        await self.approver.validate()
        assert self.approver.is_valid

    async def test_ask_too_much_npernode_for_ppn(self):
        self.approver.mpi_command = f"mpirun -npernode {self.approver.ppn + 1}"
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1
        with pytest.raises(MPIError):
            self.approver.raise_errors()
