import pytest

from .bases import BaseApproverTest
from ...handlers.file_approvers import SymLinkApprover
from ...handlers.file_approvers.exceptions import SymLinkError


class TestSymLinkApprover(BaseApproverTest):
    _approver_class = SymLinkApprover
    _exception = SymLinkError

    @pytest.fixture(autouse=True)
    def setup_symlink_approver(self, setup_approver, named_tempfile):
        # create file
        self.source = named_tempfile
        self.approver.source = self.source.name

    async def test_non_existant_source(self):
        self.approver.source = "non_existant_source"
        await self.approver.validate()
        assert not self.approver.is_valid
        assert len(self.approver.errors) == 1
        with pytest.raises(SymLinkError):
            self.approver.raise_errors()
