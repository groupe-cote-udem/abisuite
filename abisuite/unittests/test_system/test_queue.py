import pytest

from ...system import Queue


output_examples = {
    "slurm": (
        "=             JOBID     NAME     USER ST       TIME  NODES "
        "NODELIST(REASON) WORK_DIR\n=           2107431      epw oums1149 PD  "
        "0:00      8 (QOSGrpNodeLimit) /scratch/oums1149/epw/run\n=           "
        "2107430 kgrid12_ oums1149 PD       0:00     20 (Priority) "
        "/data/fgiustino/oums1149/Sr2RuO4/PBE_NC/noSOC/gs/nscf/ecutwfc90/kgrid"
        "12_12_12_20nodes/run\n"
        )
    }


# FIXME: for now only test the slurm queuing system
class TestQueue:
    """Test case for the different Queue parsers.
    """

    @pytest.fixture(autouse=True)
    def setup_queue(self, named_tempfile):
        self.queue = Queue()
        self.queue_cmd_tempfile = named_tempfile
        self.queue_cmd = self.queue_cmd_tempfile.name

    # TODO: use mock instead of artificially setting temporary files...
    @pytest.mark.parametrize(
            "queuing_system", [
                "slurm",
                ],
            )
    def test_queue_parsing(self, queuing_system):
        # NOTE: see fixme above for this loop
        # manual setting
        self.queue.queuing_system = queuing_system
        parser = self.queue.queue_parser  # get parser cls
        parser.queue_command = self.queue_cmd  # artificially set cmd
        parser = parser()  # initialize parser obj
        # test the analyse output method
        parser.analyse_queue_output(output_examples[queuing_system])
        # data is manually extracted from example
        assert parser.njobs == 2
        paths = (
            "/data/fgiustino/oums1149/Sr2RuO4/PBE_NC/noSOC/gs/nscf/ecutwfc90/kgrid12_12_12_20nodes/run",  # noqa
            "/scratch/oums1149/epw/run")
        for job in paths:
            assert job in [x.workdir for x in parser.jobs]
