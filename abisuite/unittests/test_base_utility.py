import logging

import pytest

from ..bases import BaseUtility
from ..exceptions import DevError


class TestBaseUtility:
    def test_base_utility(self):
        # test forgot loggername raises an error
        class TestBaseClass(BaseUtility):
            pass
        with pytest.raises(DevError):
            TestBaseClass()
        loggername = "test_loggername"
        loglevel = 17
        # test loglevel is set at the right level

        class TestBaseClass(BaseUtility):
            _loggername = loggername
        instance = TestBaseClass(loglevel=loglevel)
        assert instance._logger.level == loglevel
        assert instance._logger.name == loggername

    def test_loglevel_property(self):
        class TestBaseClass(BaseUtility):
            _loggername = "test"
        loglevel = logging.INFO
        instance = TestBaseClass(loglevel=loglevel)
        assert instance._loglevel == loglevel
        # check that changing the loglevel works
        newloglevel = logging.ERROR
        instance._loglevel = newloglevel
        assert instance._loglevel == newloglevel
