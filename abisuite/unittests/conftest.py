import asyncio

import aiofiles

try:
    from peewee import SqliteDatabase
    __TEST_DB__ = SqliteDatabase(':memory:')
except ImportError:
    __TEST_DB__ = None
import pytest

from .. import __USER_CONFIG__
from ..databases import DBCalculation


@pytest.fixture(scope="session")
def event_loop():
    """Event loop fixture that can run session scoped async fixtures."""
    policy = asyncio.get_event_loop_policy()
    loop = policy.new_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="function")
async def tempdir():
    """Fixture for a temporary directory."""
    async with aiofiles.tempfile.TemporaryDirectory() as temp:
        yield temp


@pytest.fixture(scope="function")
async def tempdir2():
    """Fixture for a 2nd temporary directory."""
    async with aiofiles.tempfile.TemporaryDirectory() as temp:
        yield temp


@pytest.fixture(scope="function")
async def tempdir3():
    """Fixture for a 3rd temporary directory."""
    async with aiofiles.tempfile.TemporaryDirectory() as temp:
        yield temp


@pytest.fixture(scope="function")
async def named_tempfile():
    """Fixture for a temporary file."""
    async with aiofiles.tempfile.NamedTemporaryFile() as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_abinit_script():
    """Fixture for a temporary abinit script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="abinit") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_anaddb_script():
    """Fixture for a temporary abinit anaddb script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="anaddb") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_mrgddb_script():
    """Fixture for a temporary abinit mrgddb script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="mrgddb") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_optic_script():
    """Fixture for a temporary abinit mrgddb script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="optic") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_pwx_script():
    """Fixture for a temporary pw.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="pw.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_dosx_script():
    """Fixture for a temporary dos.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="dos.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_q2rx_script():
    """Fixture for a temporary q2r.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="q2r.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_matdynx_script():
    """Fixture for a temporary matdyn.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(
            suffix="matdyn.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_epwx_script():
    """Fixture for a temporary epw.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(suffix="epw.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_wannier90x_script():
    """Fixture for a temporary wannier90.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(
            suffix="wannier90.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_pw2wannier90x_script():
    """Fixture for a temporary pw2wannier.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(
            suffix="pw2wannier90.x") as file_:
        yield file_


@pytest.fixture(scope="session")
async def temp_kpointsx_script():
    """Fixture for a temporary kpoints.x script."""
    async with aiofiles.tempfile.NamedTemporaryFile(
            suffix="kpoints.x") as file_:
        yield file_


# @pytest.fixture(scope="session")
# def testdb():
#     """Fixture for a test db."""
#     with tempfile.TemporaryDirectory() as temp:
#         path = os.path.join(temp, "testdb.sqlite")
#         with sqlite3.connect(path) as con:
#             pass
#         con.close()
#         connect_to_database(database=path)
#         yield


@pytest.fixture(scope="session", autouse=True)
def testdb():
    """Pytest fixture for a testing calculation database."""
    if __TEST_DB__ is not None:
        __TEST_DB__.bind([DBCalculation], bind_refs=False, bind_backrefs=False)
        __TEST_DB__.connect()
        __TEST_DB__.create_tables([DBCalculation])
    __USER_CONFIG__["DATABASE"]["path"] = "__TEST__"
    yield
    # cleanup if necessary
    if __TEST_DB__ is not None:
        __TEST_DB__.drop_tables([DBCalculation])
        __TEST_DB__.close()
