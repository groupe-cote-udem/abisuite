import os

import pytest

from ...exceptions import DevError


class BaseParserTest:
    """Base class for parser classes for non-writable files."""

    _example_file = None
    _parser_class = None

    @pytest.fixture(autouse=True)
    async def setup_parser(self):
        """Setup parser."""
        if self._example_file is None:
            raise DevError(
                    f"Need to set '_example_file' in '{self.__class__}'.")
        if self._parser_class is None:
            raise DevError(
                    f"Need to set '_parser_class' in '{self.__class__}'.")
        self.parser = await self._parser_class.from_file(self._example_file)

    async def test_parsing(self):
        """Test that parsing raises no errors."""
        await self.parser.read()
        # nothing to do just make sure there is no errors
        assert self._example_file == self.parser.path


class BaseWritableParserTest:
    """Base test for file parsers which can be written."""

    _file_suffix = None
    _parser_class = None
    _writer_class = None
    _initialize_attributes_upon_setup = True

    @pytest.fixture(autouse=True)
    def setup_parser(self, tempdir):
        """Setup parser fixture."""
        if self._parser_class is None:
            raise DevError("Need to setup '_parser_class'")
        if self._writer_class is None:
            raise DevError("Need to setup '_writer_class'")
        self.tempdir = tempdir
        self.path = os.path.join(self.tempdir, "test")
        if self._file_suffix is not None:
            self.path += self._file_suffix
        self.writer = self._writer_class()
        if self._initialize_attributes_upon_setup:
            for prop in self.writer.structure.all_attributes:
                if prop in self.writer.structure.set_attributes:
                    setattr(self.writer, prop, set())
                    continue
                if prop in self.writer.structure.list_attributes:
                    setattr(self.writer, prop, [])
                    continue
                setattr(self.writer, prop, prop)
        self.writer.path = self.path

    async def test_parsing(self):
        """Test parsing a writable file."""
        await self.writer.write()
        self.parser = await self._parser_class.from_file(self.path)
        await self.parser.read()
        assert self.parser.structure == self.writer.structure, (
                f"{self.parser.structure} != {self.writer.structure}")


class BaseInputParserTestNoInputVariables(BaseWritableParserTest):
    """Base tests for input file parser with no input vars."""

    _file_ending = ".in"
    _initialize_attributes_upon_setup = False


class BaseInputParserTest(BaseInputParserTestNoInputVariables):
    """Base tests class for input file parsers."""

    _input_vars = None

    @pytest.fixture(autouse=True)
    def setup_input_parser(self, setup_parser):
        """Setup fixture for input file parsers tests."""
        if self._input_vars is None:
            raise DevError(
                    f"Need to set '_input_vars' in '{self.__class__}'.")
        self.writer.input_variables = self._input_vars
