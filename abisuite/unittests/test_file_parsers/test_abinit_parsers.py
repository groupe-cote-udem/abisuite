import pytest

from .bases import (
        BaseInputParserTest,
        BaseInputParserTestNoInputVariables,
        BaseParserTest,
        BaseWritableParserTest,
        )
from ..variables_for_tests import (
        abinit_anaddb_log_example, abinit_anaddb_phfrq_example,
        abinit_anaddb_vars,
        abinit_dmft_hamiltonian_example, abinit_dmft_projectors_example,
        abinit_gs_input_variables,
        abinit_gs_output_file,
        abinit_mrgddb_log_example,
        abinit_optic_input_variables,
        abinit_optic_lincomp_example,
        )
from ...handlers.file_parsers import (
        AbinitAnaddbFilesParser, AbinitAnaddbInputParser,
        AbinitAnaddbLogParser, AbinitAnaddbPhfrqParser, AbinitDMFTEigParser,
        AbinitDMFTProjectorsParser,
        AbinitInputParser,
        AbinitMrgddbInputParser, AbinitMrgddbLogParser,
        AbinitOpticFilesParser,
        AbinitOpticInputParser,
        AbinitOpticLincompParser,
        AbinitOutputParser,
        )
from ...handlers.file_writers import (
        AbinitAnaddbFilesWriter, AbinitAnaddbInputWriter,
        AbinitInputWriter,
        AbinitMrgddbInputWriter,
        AbinitOpticFilesWriter,
        AbinitOpticInputWriter,
        )


class BaseFilesParserTest(BaseWritableParserTest):
    """Base test case for files file parsers.
    """
    _file_suffix = ".files"

    @pytest.fixture(autouse=True)
    def setup_files_parser(self, setup_parser):
        self.writer.input_file_path = "test_files_file_parser.in"
        self.writer.output_file_path = "test_files_file_parser.out"


class TestAbinitAnaddbFilesParser(BaseFilesParserTest):
    """Test Case for abinit anaddb files file parser class.
    """
    _parser_class = AbinitAnaddbFilesParser
    _writer_class = AbinitAnaddbFilesWriter

    @pytest.fixture(autouse=True)
    def setup_abinit_anaddb_files_parser(self, setup_files_parser):
        self.writer.ddb_file_path = "test_ddb"
        self.writer.band_structure_file_path = "test_band_structure"
        self.writer.gkk_file_path = "test_gkk"
        self.writer.eph_data_prefix = "test_eph_prefix"
        self.writer.ddk_file_path = "test_ddk"


class TestAbinitAnaddbInputParser(BaseInputParserTest):
    _input_vars = abinit_anaddb_vars
    _parser_class = AbinitAnaddbInputParser
    _writer_class = AbinitAnaddbInputWriter


class TestAbinitAnaddbLogParser(BaseParserTest):
    _parser_class = AbinitAnaddbLogParser
    _example_file = abinit_anaddb_log_example


class TestAbinitAnaddbPhfrqParser(BaseParserTest):
    """Test case for the anaddb PHFRQ file parser.
    """
    _parser_class = AbinitAnaddbPhfrqParser
    _example_file = abinit_anaddb_phfrq_example

    async def test_parsing(self):
        await super().test_parsing()
        # test that number of branches and qpts is good
        nqpts = self.parser.frequencies.shape[0]
        nbranches = self.parser.frequencies.shape[1]
        assert nqpts == self.parser.nqpts
        assert nbranches == self.parser.nbranches


class TestAbinitInputParser(BaseInputParserTest):
    _input_vars = abinit_gs_input_variables
    _parser_class = AbinitInputParser
    _writer_class = AbinitInputWriter


class TestAbinitOutputParser(BaseParserTest):
    _example_file = abinit_gs_output_file
    _parser_class = AbinitOutputParser

    async def test_output_parser(self):
        await self.parser.read()
        # in this example file, it is just a GS calculation and all the input
        # variables are fixed => they do not change after the calculation
        # they should be the same as the abinit_vars dict
        for name, value in abinit_gs_input_variables.items():
            if name not in self.parser.input_variables and (
                    name not in self.parser.output_variables):
                # this var is not echoed
                continue
            if name == "shiftk":
                # this looks like the printing is bugged in newer versions
                # of abinit...
                continue
            assert name in self.parser.input_variables
            assert value == self.parser.input_variables[name], name
            assert name in self.parser.output_variables
            assert value == self.parser.output_variables[name]


class TestAbinitOpticInputParser(BaseInputParserTest):
    _input_vars = abinit_optic_input_variables.copy()
    _parser_class = AbinitOpticInputParser
    _writer_class = AbinitOpticInputWriter


class TestAbinitOpticLincompParser(BaseParserTest):
    _example_file = abinit_optic_lincomp_example
    _parser_class = AbinitOpticLincompParser


class TestAbinitOpticFilesParser(BaseFilesParserTest):
    """Test case for the abinit optic files file parser class.
    """
    _parser_class = AbinitOpticFilesParser
    _writer_class = AbinitOpticFilesWriter

    @pytest.fixture(autouse=True)
    def setup_abinit_optic_files_parser(self, setup_files_parser):
        self.writer.output_data_prefix = "odat_test_files_file_parser"
        self.writer.output_data_dir = self.tempdir


class TestAbinitDMFTProjectorsParser(BaseParserTest):
    _parser_class = AbinitDMFTProjectorsParser
    _example_file = abinit_dmft_projectors_example


class TestAbinitDMFTEigParser(BaseParserTest):
    _parser_class = AbinitDMFTEigParser
    _example_file = abinit_dmft_hamiltonian_example


class TestAbinitMrgddbInputParser(
        BaseInputParserTestNoInputVariables):
    """Test case for the mrgddb input parser class.
    """
    _parser_class = AbinitMrgddbInputParser
    _writer_class = AbinitMrgddbInputWriter

    @pytest.fixture(autouse=True)
    def setup_abinit_mrgddb_input_parser(self, setup_parser):
        self.writer.output_file_path = "test_output"
        self.writer.title = "test title"
        self.writer.ddb_paths = ["ddb1", "ddb2"]


class TestAbinitMrgddbLogParser(BaseParserTest):
    """Test case for a the mrgddb log parser class.
    """
    _example_file = abinit_mrgddb_log_example
    _parser_class = AbinitMrgddbLogParser
