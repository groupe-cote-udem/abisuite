import os

import pytest

from .bases import BaseWritableParserTest
from ...handlers.file_parsers import SymLinkParser
from ...handlers.file_writers import SymLinkWriter
from ...linux_tools import touch


class TestSymLinkParser(BaseWritableParserTest):
    _parser_class = SymLinkParser
    _writer_class = SymLinkWriter

    @pytest.fixture(autouse=True)
    async def setup_parser(self, tempdir):
        self.tempdir = tempdir
        self.path = os.path.join(self.tempdir, "test")
        self.writer = self._writer_class()
        self.writer.path = self.path
        self.source = os.path.join(self.tempdir, "source_file")
        await touch(self.source)
        self.writer.source = self.source
