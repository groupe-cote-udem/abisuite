import aiofiles

import pytest

from .bases import BaseParserTest
from ...handlers.file_parsers import GenericParser


class TestGenericParser(BaseParserTest):
    _parser_class = GenericParser

    @pytest.fixture
    async def pre_setup(self):
        async with aiofiles.tempfile.NamedTemporaryFile(
                delete=False) as tempfile:
            self.tempfile = tempfile
            self._example_file = self.tempfile.name
            yield

    @pytest.fixture(autouse=True)
    async def setup_generic_parser(self, pre_setup, setup_parser):
        pass
