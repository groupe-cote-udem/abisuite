from .bases import (
        BaseInputParserTest,
        BaseParserTest,
        )
from ..variables_for_tests import (
        qe_dos_input_variables,
        qe_dynmatx_vars, qe_epsilonx_vars, qe_epwx_vars,
        qe_fsx_vars,
        qe_ld1x_vars,
        qe_matdynx_vars,
        qe_phx_dyn0_example,
        qe_phx_vars,
        qe_phxlog_example,
        qe_ppx_vars, qe_projwfcx_vars,
        qe_pw2wannier90x_vars,
        qe_pwx_vars,
        qe_pwxlog_example,
        qe_q2rx_vars,
        )
from ...handlers.file_parsers import (
        QEDOSInputParser, QEDynmatInputParser,
        QEEPWInputParser,
        QEEpsilonInputParser,
        QEFSInputParser, QELD1InputParser, QEMatdynInputParser,
        QEPHDyn0Parser, QEPHInputParser, QEPHLogParser,
        QEPPInputParser,
        QEPW2Wannier90InputParser,
        QEPWInputParser, QEPWLogParser,
        QEProjwfcInputParser,
        QEQ2RInputParser,
        )
from ...handlers.file_writers import (
        QEDOSInputWriter, QEDynmatInputWriter,
        QEEPWInputWriter,
        QEEpsilonInputWriter,
        QEFSInputWriter, QELD1InputWriter, QEMatdynInputWriter,
        QEPHInputWriter, QEPPInputWriter,
        QEPW2Wannier90InputWriter,
        QEPWInputWriter,
        QEProjwfcInputWriter,
        QEQ2RInputWriter,
        )


class TestQEPHDyn0Parser(BaseParserTest):
    _example_file = qe_phx_dyn0_example
    _parser_class = QEPHDyn0Parser

    async def test_parsing(self):
        await super().test_parsing()
        # confirm data extracted
        qpts = self.parser.qpts
        assert len(qpts) == self.parser.nqpt
        assert self.parser.qgrid == [2, 2, 2]
        assert qpts == [[0.0, 0.0, 0.0],
                        [0.5, -0.5, 0.5],
                        [0.0, -1.0, 0.0]]


class TestQEPHLogParser(BaseParserTest):
    _example_file = qe_phxlog_example
    _parser_class = QEPHLogParser

    async def test_parsing(self):
        await super().test_parsing()
        # data directly taken from file
        freqs = self.parser.phonon_frequencies
        # 3 modes are computed
        assert len(self.parser.phonon_frequencies) == 3
        # silicon has 6 modes
        for freq in freqs:
            assert len(freq["frequencies_THz"]) == 6
            assert len(freq["frequencies_cm-1"]) == 6
            assert len(freq["q_point"]) == 3


class TestQEPWLogParser(BaseParserTest):
    _example_file = qe_pwxlog_example
    _parser_class = QEPWLogParser


class TestQEDOSInputParser(BaseInputParserTest):
    _input_vars = qe_dos_input_variables.copy()
    _parser_class = QEDOSInputParser
    _writer_class = QEDOSInputWriter


class TestQEDynmatInputParser(BaseInputParserTest):
    _input_vars = qe_dynmatx_vars.copy()
    _parser_class = QEDynmatInputParser
    _writer_class = QEDynmatInputWriter


class TestQEEpsilonInputParser(BaseInputParserTest):
    _input_vars = qe_epsilonx_vars.copy()
    _parser_class = QEEpsilonInputParser
    _writer_class = QEEpsilonInputWriter


class TestQEEPWInputParser(BaseInputParserTest):
    _input_vars = qe_epwx_vars.copy()
    _parser_class = QEEPWInputParser
    _writer_class = QEEPWInputWriter


class TestQEFSInputParser(BaseInputParserTest):
    _input_vars = qe_fsx_vars.copy()
    _parser_class = QEFSInputParser
    _writer_class = QEFSInputWriter


class TestQELD1InputParser(BaseInputParserTest):
    _input_vars = qe_ld1x_vars.copy()
    _parser_class = QELD1InputParser
    _writer_class = QELD1InputWriter


class TestQEMatdynInputParser(BaseInputParserTest):
    _input_vars = qe_matdynx_vars.copy()
    _parser_class = QEMatdynInputParser
    _writer_class = QEMatdynInputWriter


class TestQEPHInputParser(BaseInputParserTest):
    _input_vars = qe_phx_vars.copy()
    _parser_class = QEPHInputParser
    _writer_class = QEPHInputWriter


class TestQEPPInputParser(BaseInputParserTest):
    _input_vars = qe_ppx_vars.copy()
    _parser_class = QEPPInputParser
    _writer_class = QEPPInputWriter


class TestQEProjwfcInputParser(BaseInputParserTest):
    _input_vars = qe_projwfcx_vars.copy()
    _parser_class = QEProjwfcInputParser
    _writer_class = QEProjwfcInputWriter


class TestQEPWInputParser(BaseInputParserTest):
    _input_vars = qe_pwx_vars.copy()
    _parser_class = QEPWInputParser
    _writer_class = QEPWInputWriter


class TestQEPW2Wannier90InputParser(BaseInputParserTest):
    _input_vars = qe_pw2wannier90x_vars.copy()
    _parser_class = QEPW2Wannier90InputParser
    _writer_class = QEPW2Wannier90InputWriter


class TestQEQ2RInputParser(BaseInputParserTest):
    _input_vars = qe_q2rx_vars.copy()
    _parser_class = QEQ2RInputParser
    _writer_class = QEQ2RInputWriter
