import pytest

from .bases import BaseWritableParserTest
from ...handlers.file_parsers import MetaDataParser
from ...handlers.file_writers import MetaDataWriter


class TestMetaDataParser(BaseWritableParserTest):
    _file_suffix = MetaDataParser._expected_ending
    _parser_class = MetaDataParser
    _writer_class = MetaDataWriter

    @pytest.mark.parametrize(
            "calctype", ["abinit", "abinit_optic", "qe_pw", "qe_ph"])
    async def test_from_calculation(self, calctype):
        self.writer.calctype = calctype
        await self.writer.write()
        self.parser = await self._parser_class.from_calculation(self.tempdir)
        await self.parser.read()
        assert self.parser.structure == self.writer.structure, (
                f"{self.parser.structure}\n !=\n {self.writer.structure}")
