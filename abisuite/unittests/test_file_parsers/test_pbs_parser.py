import os

import pytest

from .bases import BaseWritableParserTest
from ..routines_for_tests import create_pbs_test_writer
from ...handlers.file_parsers import PBSParser
from ...handlers.file_writers import PBSWriter


class TestPBSParser(BaseWritableParserTest):
    _parser_class = PBSParser
    _writer_class = PBSWriter
    _file_suffix = ".sh"

    @pytest.fixture(autouse=True)
    def setup_parser(self, tempdir):
        self.tempdir = tempdir
        self.path = os.path.join(self.tempdir, "test")
        if self._file_suffix is not None:
            self.path += self._file_suffix
        self.writer = self._writer_class()
        self.writer.path = self.path
        create_pbs_test_writer(self.writer)
