import pytest

from ..utils import TaggedList


class TestTaggedList:
    _item = [1, 2, 3]
    _tag = "normal list"

    @pytest.fixture(autouse=True)
    def setup_tagged_list(self):
        self.tagged_list = TaggedList()
        self.tagged_list.append(self._item, tag=self._tag)
        yield
        del self.tagged_list

    def test_append(self):
        # check the item is accessible both by its tag and index
        assert self.tagged_list[self._tag] == self._item
        assert self.tagged_list[0] == self._item

    def test_remove_by_tag(self):
        self.tagged_list.remove(self._tag)
        with pytest.raises(KeyError):
            self.tagged_list.remove(self._tag)

    def test_remove_by_index(self):
        self.tagged_list.remove(0)
        with pytest.raises(KeyError):
            self.tagged_list.remove(0)

    def test_set_item(self):
        newitem = self._item + [4]
        # set by tag
        self.tagged_list[self._tag] = newitem
        assert self.tagged_list[self._tag] == newitem
        with pytest.raises(KeyError):
            self.tagged_list["wrong key"] = newitem
        # set by index
        newitem += [5]
        self.tagged_list[0] = newitem
        assert self.tagged_list[0] == newitem
        with pytest.raises(KeyError):
            # wrong index
            self.tagged_list[3] = newitem

    def test_change_tag(self):
        newtag = "new tag"
        self.tagged_list.change_tag(newtag, self._tag)
        assert self.tagged_list.get_tag_from_index(0) == newtag
        newtag = "new tag2"
        self.tagged_list.change_tag(newtag, 0)
        assert self.tagged_list.get_tag_from_index(0) == newtag

        with pytest.raises(KeyError):
            self.tagged_list.change_tag(newtag, "wrong tag")
        with pytest.raises(KeyError):
            self.tagged_list.change_tag(newtag, 12)

    def test_get_tag_from_index(self):
        assert self.tagged_list.get_tag_from_index(0) == self._tag
        with pytest.raises(KeyError):
            self.tagged_list.get_tag_from_index(12)

    def test_tolist(self):
        # test that the tolist method returns a copied list
        list_ = self.tagged_list.tolist()
        assert isinstance(list_, list)
        assert list_ is not self.tagged_list.items
