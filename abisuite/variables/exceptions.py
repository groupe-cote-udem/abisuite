import difflib

from ..colors import Colors


class InvalidInputVariableError(Exception):
    """Custom error class for invalid input variables."""

    def __init__(self, input_var, all_db_vars):
        """Init method.

        Parameters
        ----------
        input_var: str
            The name of the input variable.
        all_db_vars: list-like
            The list of all db input variable names.
        """
        closest = difflib.get_close_matches(
                input_var, all_db_vars, 5)
        msg = (Colors.color_text("ERROR", "bold", "red") +
               f": Invalid input variable: '{input_var}'. Not present in the "
               "input variables DB.")
        if len(closest) == 1:
            msg += f" Did you mean '{closest[0]}'?"
        elif len(closest) > 1:
            msg += f" Did you something in ({', '.join(closest)})?"
        super().__init__(msg)
