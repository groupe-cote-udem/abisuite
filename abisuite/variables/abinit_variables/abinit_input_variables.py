from .bases import BaseAbinitInputVariable
from ..bases import (
        MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
        variable_db_integrity_checker,
        )
from ...routines import is_str


ALLOWED_ABINIT_BLOCKS = (
        "bands", "basis set", "Bethe-Salpeter Equation", "checks",
        "files", "software parameter / driver / algorithm", "fft grid", "GW",
        "internal storage", "k-points", "model", "parallelization",
        "paw parameters", "printing / reading", "response function",
        "scf procedure", "structural optimization", "unit cell",
        "van der waals"
        )


# These variables should be written as strings but without
# quotation marks (and also read as strings)
__ABINIT_VARIABLES_STR_BUT_NO_QUOTES__ = (
        "bs_coulomb_term", "gwmem", "fftgw", "istwfk",
        )


class AbinitInputVariable(BaseAbinitInputVariable):
    """Class that represent an abinit input variable."""

    def __str__(self):
        string = f" {self.name}"
        if self.name in __ABINIT_VARIABLES_STR_BUT_NO_QUOTES__ and (
                self.is_value_a_str()):
            # exceptions that can be strings but don't accept quotation marks
            # e.g.: gwmem can be 00 where both 0 must be present because each
            # digit means something. User can set this as a string but in the
            # end we don't want quotation marks.
            return string + f" {str(self.value)}\n"
        return super().__str__()

    @BaseAbinitInputVariable.value.setter
    def value(self, val):
        """Sets the variable's value."""
        if self.name in __ABINIT_VARIABLES_STR_BUT_NO_QUOTES__:
            # convert to str in case ints are also allowed
            # this is for simplicity when comparing input files
            # since when we parse input files with such variables
            # we just keep it as a string
            val = str(val)
        BaseAbinitInputVariable.value.fset(self, val)


class AbinitPseudosVariable(BaseAbinitInputVariable):
    """Special case for 'pseudos' variable which is a list of strings."""

    def __str__(self):
        # compute string representation of the variable
        string = f" {self.name}"
        if is_str(self.value):
            self.value = [self.value]
        return (string + self._spaces +
                '"' + self.convert_vector_to_str(self.value, sep=", ") + '"'
                + "\n")


RAW_ABINIT_VARIABLES = {
        "acell": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "amu": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "angdeg": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "autoparal": {
            "allowed": (0, 1, 2, 3, 4),
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "awtr": {
            "allowed": (0, 1),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "bdgw": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "boxcutmin": {
            "block": "basis set",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1.0,  # artificial minimal value
            "type": "scalar",
            },
        "bs_algorithm": {
            "allowed": (1, 2, 3),
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "bs_calctype": {
            "allowed": (1, 2),
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "bs_coulomb_term": {
            "allowed": (
                "00", "01", "01", "10", 10, "11", 11,
                "20", 20, "21", 21,
                ),
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": (str, int),
            },
        "bs_coupling": {
            "allowed": (0, 1),
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "bs_exchange_term": {
            "allowed": (0, 1),
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "bs_freq_mesh": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "len": 3,
            "mandatory": False,
            "type": "vector",
            },
        "bs_haydock_niter": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "bs_haydock_tol": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "len": 2,
            "type": "vector",
            },
        "bs_loband": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "bs_nstates": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "chkdilatmx": {
            "allowed": (0, 1),
            "block": "checks",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "chksymbreak": {
            "block": "checks",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "chksymtnons": {
            "block": "checks",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "diemac": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "scalar",
            },
        "dilatmx": {
            "block": "structural optimization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "ecut": {
            "block": "basis set",
            "class": AbinitInputVariable,
            "mandatory": True,
            "min": 0.0,
            "type": "scalar",
            },
        "ecuteps": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "ecutsigx": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "ecutsm": {
            "block": "basis set",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "ecutwfn": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "fftgw": {
            "allowed": (
                "00", "01", "10", 10, "11", 11, "20", 20, "21", 21,
                "30", 30, "31", 31,
                ),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": (str, int),
            },
        "freqspmax": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "freqremax": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "freqremin": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "gw_icutcoul": {
            "allowed": (0, 1, 2, 3, 4, 5, 6, 7, 14, 15, 16),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "gwcalctyp": {
            "allowed": (
                0, 1, 2, 5, 6, 7, 8, 9, 10, 12, 15, 16, 17, 18, 19, 20,
                22, 25, 26, 27, 28, 29,
                ),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "gwmem": {
            "allowed": ("00", "01", "10", 10, "11", 11),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": (str, int),
            },
        "gwpara": {
            "allowed": (1, 2),
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "inclvkb": {
            "allowed": (0, 1, 2),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "indata_prefix": {
            "block": "files",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": str,
            },
        "ionmov": {
            "block": "structural optimization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "iomode": {
            "allowed": (0, 1, 3),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "irdbscoup": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int
            },
        "irdbsreso": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int
            },
        "irdddk": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int
            },
        "irdden": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "irdhaydock": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int
            },
        "irdscr": {
            "allowed": (0, 1),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "irdwfk": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "iscf": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "istwfk": {
            "block": "internal storage",
            "class": AbinitInputVariable,
            "mandatory": False,
            },
        "ixc": {
            "block": "model",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "kpt": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "kptgw": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "kptbounds": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "kptnrm": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": float,
            "min": 1.0,
            },
        "kptopt": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "kptrlatt": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "type": "2darr",
            "mandatory": False,
            },
        "kptrlen": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "type": "scalar",
            "mandatory": False,
            },
        "mbpt_sciss": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "type": float,
            "mandatory": False,
            "min": 0.0,
            },
        "natom": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": True,
            "min": 1,
            "type": int,
            },
        "natrd": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nband": {
            "block": "bands",
            "class": AbinitInputVariable,
            "min": 1,
            "mandatory": False,
            "type": ("vector", int),
            },
        "nbdbuf": {
            "block": "bands",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "ndivk": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "ndivsm": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ndtset": {
            "block": "software parameter / driver / algorithm",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nfreqim": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nfreqsp": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nfreqre": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "ngfft": {
            "block": "fft grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "ngfftdg": {
            "block": "fft grid",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "ngkpt": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "len": 3,
            "mandatory": False,
            "type": "vector",
            },
        "nkpt": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nkptgw": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nline": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "npband": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "npfft": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nphf": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "npimage": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "npkpt": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nppert": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "npulayit": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nnsclo": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "npspinor": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nqpt": {
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nshiftk": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nspden": {
            "block": "model",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nspinor": {
            "allowed": (1, 2),
            "block": "model",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nsppol": {
            "block": "model",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nstep": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "nsym": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ntime": {
            "block": "structural optimization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "ntypat": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": True,
            "min": 1,
            "type": int,
            },
        "occ": {
            "block": "bands",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "occopt": {
            "block": "bands",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "optcell": {
            "block": "structural optimization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "optdriver": {
            "block": "software parameter / driver / algorithm",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int
            },
        "optforces": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "outdata_prefix": {
            "block": "files",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": str,
            },
        "paral_kgb": {
            "allowed": (0, 1),
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "paral_rf": {
            "block": "parallelization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "pawecutdg": {
            "block": "basis set",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "pawfatbnd": {
            "allowed": (0, 1, 2),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "pawprtdos": {
            "allowed": (0, 1, 2),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "pawxcdev": {
            "allowed": (0, 1, 2),
            "block": "paw parameters",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "pp_dirpath": {
            "block": "files",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": str,
            },
        "ppmfrq": {
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "prtden": {
            "allowed": (0, 1, 2, 3, 4, 5, 6, 7),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtdos": {
            "allowed": (0, 1, 2, 3, 4, 5),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtdosm": {
            "allowed": (0, 1, 2),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prteig": {
            "allowed": (0, 1),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtfsurf": {
            "allowed": (0, 1),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtgsr": {
            "allowed": (0, 1),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtprocar": {
            # actually a value of 2 is allowed but it makes the procar file
            # unreadable by pyprocar unless we manually remove some lines...
            "allowed": (0, 1),  # , 2),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtvol": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "prtwf": {
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "pseudos": {
            "block": "files",
            "class": AbinitPseudosVariable,
            "mandatory": True,
            "type": "vector",
            },
        "qpt": {
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "rfatpol": {
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "rfdir": {
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "rfelfd": {
            "allowed": [0, 1, 2, 3],
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "rfphon": {
            "allowed": [0, 1],
            "block": "response function",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "rprim": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr"
            },
        "rprimd": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr"
            },
        "scalecart": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "shiftk": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "spgroup": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "max": 230,
            "type": int,
            },
        "spinat": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "symchi": {
            "allowed": (0, 1),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "symrel": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "symsigma": {
            "allowed": (0, 1),
            "block": "GW",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "timopt": {
            "allowed": (-4, -3, -2, -1, 0, 1, 2, 3, 4, 10),
            "block": "printing / reading",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "toldfe": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "toldff": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tolmxf": {
            "block": "structural optimization",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tolrff": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tolvrs": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tolwfk": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tolwfr": {
            "block": "scf procedure",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "tmpdata_prefix": {
            "block": "files",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": str,
            },
        "tnons": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "tsmear": {
            "block": "bands",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "typat": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": "vector"
            },
        "useylm": {
            "allowed": (0, 1),
            "block": "software parameter / driver / algorithm",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "vdw_df_threshold": {
            "block": "van der waals",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "vdw_tol": {
            "block": "van der waals",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": "scalar",
            },
        "vdw_xc": {
            # values of 1 and 2 are not fully operational yet
            # (see abinit source code)
            "allowed": (0, 1, 2, 5, 6, 7, 10, 11, 14),
            "block": "van der waals",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "wfk_task": {
            "allowed": (
                "wfk_ddk", "wfk_einterp", "wfk_fullbz", "wfk_kpts_erange",
                ),
            "block": "software parameter / driver / algorithm",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": str,
            },
        "wfoptalg": {
            "allowed": (0, 1, 2, 3, 4, 10, 14, 114),
            "block": "software parameter / driver / algorithm",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "wtk": {
            "block": "k-points",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "xcart": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "xred": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "zcut": {
            "block": "Bethe-Salpeter Equation",
            "class": AbinitInputVariable,
            "mandatory": False,
            "min": 0.0,
            "type": float,
            },
        "znucl": {
            "block": "unit cell",
            "class": AbinitInputVariable,
            "mandatory": True,
            "type": "vector",
            },
        }


ALL_ABINIT_VARIABLES = variable_db_integrity_checker(
        RAW_ABINIT_VARIABLES,
        ALLOWED_ABINIT_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        [AbinitInputVariable, AbinitPseudosVariable],
        optional_keys=OPTIONAL_VARIABLE_KEYS)
