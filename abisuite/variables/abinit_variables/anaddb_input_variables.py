from .bases import BaseAbinitInputVariable
from ..bases import (
        MANDATORY_VARIABLE_KEYS, OPTIONAL_VARIABLE_KEYS,
        variable_db_integrity_checker,
        )


ALLOWED_ABINIT_ANADDB_BLOCKS = (
        "flags", "Wavevector grid", "Effective charges",
        "Interatomic force constants", "Phonon band structure",
        )

RAW_ABINIT_ANADDB_INPUT_VARIABLES = {
        "asr": {
            "allowed": (0, 1, 2),
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "atifc": {
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "brav": {
            "allowed": (-1, 1, 2, 3, 4),
            "block": "Wavevector grid",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "chneut": {
            "allowed": (0, 1, 2),
            "block": "Effective charges",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "dipdip": {
            "allowed": (0, 1),
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "eivec": {
            "allowed": (0, 1, 2, 4),
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "freeze_displ": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": float,
            },
        "ifcana": {
            "allowed": (0, 1),
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ifcflag": {
            "allowed": (0, 1),
            "block": "flags",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": int,
            },
        "ifcout": {
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "natifc": {
            "block": "Interatomic force constants",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "ngqpt": {
            "block": "Wavevector grid",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "nph1l": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nph2l": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "nqpath": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 1,
            "type": int,
            },
        "nqshft": {
            "block": "Wavevector grid",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "min": 0,
            "type": int,
            },
        "q1shft": {
            "block": "Wavevector grid",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "vector",
            },
        "qpath": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "qph1l": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        "qph2l": {
            "block": "Phonon band structure",
            "class": BaseAbinitInputVariable,
            "mandatory": False,
            "type": "2darr",
            },
        }


ALL_ABINIT_ANADDB_VARIABLES = variable_db_integrity_checker(
        RAW_ABINIT_ANADDB_INPUT_VARIABLES,
        ALLOWED_ABINIT_ANADDB_BLOCKS,
        MANDATORY_VARIABLE_KEYS,
        [BaseAbinitInputVariable],
        optional_keys=OPTIONAL_VARIABLE_KEYS)
