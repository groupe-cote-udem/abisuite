import abc
import os
import sys
from typing import Any, Sequence

from .exceptions import InvalidInputVariableError
from ..bases import BaseUtility
from ..routines import (
        is_2d_arr,
        is_list_like,
        is_scalar,
        is_scalar_or_str,
        is_vector,
        )

# mandatory parameters for input variables
MANDATORY_VARIABLE_KEYS = ("block", "class", "mandatory")
# optional parameters
OPTIONAL_VARIABLE_KEYS = (
        "allowed", "max", "min", "type", "mandatory_keys",
        "path_convertible", "len",
        )
# allowed types for input variables
ALLOWED_TYPES = (
        str, list, dict, float, int, bool, "vector", "2darr", "scalar")


class BaseInputVariable(BaseUtility, abc.ABC):
    """Base class for input variables."""

    _spaces = " " * 2  # spaces between elements of an array

    def __init__(self, name, value, block, **kwargs):
        """Base input variable init method.

        Parameters
        ----------
        name : str
               The name of the variable.
        value : The value of the input variable.
        block : str
                The variable block (or category).
        """
        super().__init__(**kwargs)
        self.name = name
        self._value = None
        self.value = value
        self.block = block
        # self._logger.debug(f"Created Input Variable:"
        #                    f" {self.name}={self.value}")

    def __abs__(self):
        return self.__class__(
                self.name, abs(self.value), self.block,
                loglevel=self._loglevel)

    def __add__(self, obj):
        if isinstance(obj, BaseInputVariable):
            if self.name != obj.name:
                raise ValueError("Adding two different variables...")
            obj = obj.value
        return self.__class__(self.name, self.value + obj, self.block,
                              loglevel=self._loglevel)

    def __pow__(self, obj):
        if isinstance(obj, BaseInputVariable):
            if self.name != obj.name:
                # raise ValueError("Cannot multiply two different variables")
                # if name not the same, just return the number
                return self.value ** obj.value
            obj = obj.value
        # otherwise return a new InputVariable object
        return self.__class__(self.name, self.value ** obj, self.block,
                              loglevel=self._loglevel)

    def __bool__(self):
        return bool(self.value)

    def __truediv__(self, obj):
        if isinstance(obj, BaseInputVariable):
            if self.name != obj.name:
                raise ValueError("Adding two different variables...")
            obj = obj.value
        return self.__class__(self.name, self.value / obj, self.block,
                              loglevel=self._loglevel)

    def __mul__(self, obj):
        if isinstance(obj, BaseInputVariable):
            if self.name != obj.name:
                # raise ValueError("Cannot multiply two different variables")
                # if name not the same, just return the number
                return self.value * obj.value
            obj = obj.value
        # otherwise return a new InputVariable object
        return self.__class__(self.name, self.value * obj, self.block,
                              loglevel=self._loglevel)

    def __neg__(self):
        if type(self.value) not in (int, float):
            raise TypeError(f"Cannot 'negatify' '{self.value}'.")
        return self.__class__(
                self.name,
                -self.value,
                self.block,
                loglevel=self._loglevel)

    def __rmul__(self, obj):
        return self.__mul__(obj)

    def __eq__(self, obj):
        if is_list_like(self.value):
            if is_2d_arr(self.value):
                if not is_2d_arr(obj.value) and is_list_like(obj.value):
                    # convert into a 2d arr
                    obj.value = [obj.value]
                for row1, row2 in zip(self, obj):
                    for item1, item2 in zip(row1, row2):
                        if item1 != item2:
                            return False
                return True
            if not is_list_like(obj.value):
                # compare only first element
                # since sometimes, a single scalar (which should be interpreted
                # as a list of 1 element)
                if len(self.value) == 1:
                    return self.value[0] == obj.value
            # a simple list
            if len(self) != len(obj):
                return False
            # compare each value
            for item1, item2 in zip(self, obj):
                if item1 != item2:
                    return False
            return True
        if isinstance(obj, BaseInputVariable):
            if is_list_like(obj.value):
                # the other is a list but not self
                if len(obj.value) == 1:
                    return obj.value[0] == self.value
        if isinstance(obj, BaseInputVariable):
            if isinstance(self.value, str) and sys.platform == "win32":
                # on windows, if a value is a Path, use normpath to normalize
                # the value. otherwise two equal paths might compare False
                return (
                    os.path.normpath(self.value) ==
                    os.path.normpath(obj.value))
            return self.value == obj.value
        return self.value == obj

    def __gt__(self, obj):
        return not self < obj and self != obj

    def __ge__(self, obj):
        return not self < obj

    def __getitem__(self, item):
        if not is_list_like(self.value) and not isinstance(self.value, dict):
            raise TypeError(f"{self.__class__} does not support item indexing")
        return self.value[item]

    def __le__(self, obj):
        return self < obj or self == obj

    def __lt__(self, obj):
        # only compare similar obj
        if is_list_like(self.value):
            raise TypeError("Cannot compare list using this operator.")
        if isinstance(self, str):
            raise TypeError("Cannot compare strings using this operator.")
        if not isinstance(obj, BaseInputVariable):
            return self.value < obj
        return self.value < obj.value

    def __neq__(self, obj):
        return not self == obj

    def __iter__(self):
        try:
            for item in self.value:
                yield item
        except TypeError:
            raise TypeError(f"Input var {self.name}={self.value}"
                            " is not iterable.")

    def __len__(self):
        if not is_list_like(self.value):
            raise TypeError(f"Input variable {self.name}={self.value}"
                            " has no len() defined.")
        return len(self.value)

    def __repr__(self):
        return str(self.value)

    @property
    def value(self):
        """Return the input variable value."""
        return self._value

    @value.setter
    def value(self, value):
        self._value = value
        # if is_list_like(self.value):
        #     # put all arrays into numpy arrays
        #     self._value = np.array(self._value)

    def copy(self):
        """Copy the input variable object."""
        try:
            value = self.value.copy()
        except AttributeError:
            # no copy method
            value = self.value
        return self.__class__(self.name, value, self.block,
                              loglevel=self._loglevel)

    def is_value_a_vector(self, value=None):
        """Return True if the value is a vector."""
        if value is None:
            value = self.value
        is_a_vector = is_vector(value)
        if not is_a_vector:
            return False
        if self.is_value_a_2d_array(value):
            return False
        return True

    def is_value_a_2d_array(self, value=None):
        """Return True if the value is a 2D array."""
        if value is None:
            value = self.value
        return is_2d_arr(value)

    def is_value_a_scalar(self):
        """Return True if the value is a scalar."""
        return (self.is_value_a_scalar_or_str() and not
                self.is_value_a_str())

    def is_value_a_str(self):
        """Return True if the value is a str."""
        return isinstance(self.value, str)

    def is_value_a_scalar_or_str(self, value=None):
        """Return True if the value is a scalar or a str."""
        if value is None:
            value = self.value
        return is_scalar_or_str(value)

    def convert_2d_arr_to_str(self, array, startspace=0,
                              add_common_spaces=True,
                              separator=2):
        """Convert the 2D array into a str."""
        # return a string of the 2d array
        # first find maximal element length in each column
        # TODO: clean this ugly method... only god now knows what how it works
        maxlen = []
        # taken from
        # https://stackoverflow.com/a/6473724/6362595
        transpose = list(map(list, zip(*array)))
        for column in transpose:
            maxlencol = 0
            for element in column:
                length = len(str(element))
                if length > maxlencol:
                    maxlencol = length
            maxlen.append(maxlencol)
        # create string
        string = ""
        for row in array:
            string += " " * startspace
            if add_common_spaces:
                string += self._spaces  # beginning spaces
            str_row = []
            for i, element in enumerate(row):
                str_element = str(element)
                add_space = maxlen[i] - len(str_element)
                if i != 0:
                    add_space += separator
                elif i == 0 and add_common_spaces:
                    add_space += separator
                str_row.append(" " * add_space + str_element)
            string += "".join(str_row) + "\n"
        # if last row, remove newline to not create unnecessary newline
        # string = string[:-1]
        return string

    def convert_vector_to_str(
            self, vector: Sequence[Any], sep: str = None) -> str:
        """Convert the vector into a string.

        Parameters
        ----------
        vector: list-like
            The list of things to convert to a single string.
        sep: str, optional
            The separator between list items. If None, the default
            spaces are taken.
        """
        if sep is None:
            sep = self._spaces
        return sep.join([str(x) for x in vector])

    def endswith(self, end):
        """Returns if variable ends with a given ending.

        Parameters
        ----------
        end: str
            The ending to check.

        Raises
        ------
        AttributeError:
            If variable value is not a string.

        Returns
        -------
        True: If value endswith ending.
        False: Otherwise
        """
        if not isinstance(self.value, str):
            raise AttributeError(
                    f"'{self.name}' variable does not support 'endswith()'"
                    " method.")
        return self.value.endswith(end)

    def startswith(self, start):
        """Returns if variable starts with a given starting.

        Parameters
        ----------
        start: str
            The starting to check.

        Raises
        ------
        AttributeError:
            If variable value is not a string.

        Returns
        -------
        True: If value starts with starting.
        False: Otherwise
        """
        if not isinstance(self.value, str):
            raise AttributeError(
                    f"'{self.name}' variable does not support 'startswith()'"
                    " method.")
        return self.value.startswith(start)


class InputVariableDict(BaseUtility):
    """Container for input variables based on a specific variable database."""

    _loggername = "InputVariableDict"

    def __init__(self, variables_db, **kwargs):
        """Init method for the InputVariableDict.

        Parameters
        ----------
        variables_db : The variables database the dictionary is based upon.
        """
        super().__init__(**kwargs)
        self._variables_db = variables_db
        self._container = {}
        # self._logger.debug(f"Created Input variable dict for"
        #                    f" {self._variable_class}")

    def __contains__(self, key):
        return key in self._container

    def __eq__(self, dic):
        # two variable dict will be equal if the content of their
        # containers are equal
        if not isinstance(dic, InputVariableDict):
            raise TypeError(f"Cannot compare two different obj: {self}, {dic}")
        for k, v in self.items():
            if k not in dic:
                return False
            if v != dic[k]:
                return False
        for k in dic:
            if k not in self:
                return False
        return True

    def __getitem__(self, key):
        return self._container[key]

    def __iter__(self):
        for item in self._container:
            yield item

    def __setitem__(self, key, value):
        self.add_variable(key, value)

    def __len__(self):
        return len(self._container)

    def __str__(self):
        return str(self._container)

    def __repr__(self):
        return repr(self._container)

    def add_variable(self, name, value):
        """Adds one variable to the container.

        Parameters
        ----------
        name : str
               The name of the variable.
        value : The value of the variable.
        """
        # try to append to the dict
        try:
            self._container[name] = self._get_var_from_name_value(name, value)
        except InvalidInputVariableError as err:
            self._logger.error("Input variable error: exiting now.")
            raise SystemExit(err)

    def clear(self):
        """Clear the input variables dict."""
        # erase everything
        del self._container
        self._container = {}

    def copy(self):
        """Copy the input variables dict."""
        self._logger.debug("Copying input variable dict.")
        new = InputVariableDict(self._variables_db,
                                loglevel=self._logger.level)
        for k, v in self.items():
            new.add_variable(k, v.copy())
        return new

    def get(self, name, *args, **kwargs):
        """Get the value of a variable inside the dictionary.

        Parameters
        ----------
        name : str
               The variable name.
        default : optional
                  The default value if 'name' is not inside dict. If it is None
                  and 'name' is not inside the dict, a KeyError is raised.
        """
        if name in self._container:
            return self._container[name]
        return self._return_default_item(name, *args, **kwargs)

    def pop(self, name, *args, **kwargs):
        """Pop the value of a variable inside the dictionary.

        Parameters
        ----------
        name : str
               The variable name.
        default : optional
                  The default value if 'name' is not inside dict. If it is None
                  and 'name' is not inside the dict, a KeyError is raised.
        """
        if name in self._container:
            return self._container.pop(name)
        return self._return_default_item(name, *args, **kwargs)

    def update(self, dictio):
        """Adds multiple variables from a dict to the container.

        Parameters
        ----------
        dictio : dict
                 The dictionary to add values from.
        """
        for k, v in dictio.items():
            self.add_variable(k, v)

    def todict(self):
        """Convert the Input variables dict object into a python dict."""
        dic = {}
        for k, var in self._container.items():
            dic[k] = var.value
        return dic

    def items(self):
        """Iterate over (key, value) pairs."""
        for k, v in self._container.items():
            yield k, v

    def values(self):
        """Iterate over values."""
        for v in self._container.values():
            yield v

    def validate(self):
        """Validate the consistency of input variables.

        Done with respect to the
        corresponding variables DB.
        """
        for name, value in self.items():
            var = value.value
            # self._check_var_in_db(name)  # already done
            self._check_var_allowed(name, var)
            self._check_var_type(name, var)

    def keys(self):
        """Iterate over the keys."""
        for k in self._container.keys():
            yield k

    def _check_var_allowed(self, name, var):
        if "len" in self._variables_db[name]:
            length = self._variables_db[name]["len"]
            if not is_list_like(var) and length != 1:
                raise ValueError(
                        f"{name}={var} should have length of '{length}'.")
            if len(var) != length:
                raise ValueError(
                        f"{name}={var} should have length of '{length}'.")
        if "min" in self._variables_db[name]:
            min_ = self._variables_db[name]["min"]
            if is_list_like(var):
                # each values must be higher than the min
                for val in var:
                    if val >= min_:
                        continue
                    raise ValueError(f"{name}={var} < min allowed ({min_})")
            else:
                if var < min_:
                    raise ValueError(f"{name}={var} < min allowed ({min_})")
        if "max" in self._variables_db[name]:
            max_ = self._variables_db[name]["max"]
            types = self._variables_db[name]["type"]
            if not is_list_like(types):
                types = [types]
            if len(types) == 1 and types[0] is not str:
                if var > max_:
                    raise ValueError(f"{name}={var} > max allowed ({max_})")
            else:
                # a string with a 'max' attribute is for its length
                if len(var) > max_:
                    raise ValueError(
                            f"{name}='{var}' is longer than allowed"
                            f" ({len(var)} > {max_}).")
        if "allowed" not in self._variables_db[name]:
            # nothing else to check
            return
        allowed = self._variables_db[name]["allowed"]
        if not isinstance(allowed, dict):
            # assume its a list
            if var not in allowed:
                raise ValueError(f"{name}={var} is not in allowed values:"
                                 f" {allowed}")
            return
        # if we are here, allowed is a dict. This means the variables is given
        # as a dict and each keys can have its allowed values
        for key, val in var.items():
            if key in allowed:
                # there is allowed value for this key
                if val not in allowed[key]:
                    raise ValueError(f"'{name}['{key}']={val} not in allowed "
                                     f"values: '{allowed[key]}'")
        # check that all mandatory keys are there if some are defined
        if "mandatory_keys" in self._variables_db[name]:
            mankeys = self._variables_db[name]["mandatory_keys"]
            for mankey in mankeys:
                if mankey not in var:
                    raise ValueError(f"'{mankey}' should be present in"
                                     f" '{name}'")

    def _check_var_type(self, name, var):
        if "type" not in self._variables_db[name]:
            return
        types = self._variables_db[name]["type"]
        if not is_list_like(types):
            types = (types, )
        for typ in types:
            if typ == "vector":
                if is_scalar_or_str(var):
                    # variable is a single element => transorm into a vector.
                    var = (var, )
                if is_vector(var):
                    return
            elif typ == "2darr":
                if is_vector(var):
                    # only a 1D arr, create a 2d arr from it
                    var = [list(var)]
                if is_2d_arr(var):
                    return
            elif typ == "scalar" and is_scalar(var):
                return
            elif typ is int and isinstance(var, int):
                return
            elif typ is list and is_list_like(var):
                return
            elif typ is float:
                if is_scalar(var):
                    return
                if isinstance(var, str):
                    if var.isdigit():
                        return
            elif typ is str and isinstance(var, str):
                return
            elif typ is dict and isinstance(var, dict):
                return
            elif typ is bool:
                if var is True or var is False:
                    return
                if isinstance(var, str):
                    # try to convert into boolean value
                    if "true" in var.lower() or "false" in var.lower():
                        return
        # if we're here then var type is not good
        raise TypeError(
                f"Expected variable '{name}'='{var}' to be of a type among "
                f"one of the following allowed types: '{types}'.")

    def _check_var_in_db(self, var):
        # do this complicated loop because we allow flexibility in the var_db
        # for special '*' characters to allow multiple values at once.
        # e.g.: amass(1), amass(2), ... amass(ntyp)
        all_db_vars = tuple(self._variables_db.keys())
        for dbvar in all_db_vars:
            if var == dbvar:
                return
            if "*" in dbvar:
                # variable can have many values
                split = dbvar.split("*")
                if len(split) != 2:
                    raise NotImplementedError("More than 2 star split...")
                if not (var.startswith(split[0]) and var.endswith(split[1])):
                    continue
                # '*' should be replaced by an int
                star = var[len(split[0]):-len(split[1])]
                if not star.isdigit():
                    continue
                # ok var in db
                # add it to the db temporarily
                self._variables_db[var] = self._variables_db[dbvar].copy()
                return
        raise InvalidInputVariableError(var, all_db_vars)

    def _get_var_from_name_value(self, name, value):
        # check variable before creating it
        # check if variable is defined in the DB
        self._check_var_in_db(name)
        # lookup in the database for the var's class and block
        block = self._variables_db[name]["block"]
        cls = self._variables_db[name]["class"]
        # if value is already the good class, append it directly
        if isinstance(value, cls):
            return value
        # the following checks are now only called when calling the 'validate'
        # method
        # # otherwise, make more checks and return the new class
        # # Check if its type is good
        # self._check_var_type(name, value)
        # # Check if the value is allowed
        # self._check_var_allowed(name, value)
        value = cls(name, value, block,
                    loglevel=self._logger.level)
        return value

    def _return_default_item(self, name, *args, **kwargs):
        if len(args) > 1 or len(kwargs) > 1 or len(args) + len(kwargs) > 1:
            raise ValueError("Only one default at a time.")
        if len(kwargs):
            if "default" not in kwargs:
                raise ValueError(
                        "Need to have 'default' keyword if using kwargs in "
                        "get() method.")
            return self._get_var_from_name_value(name, kwargs["default"])
        if len(args):
            return self._get_var_from_name_value(name, args[0])
        else:
            raise KeyError(name)


def variable_db_integrity_checker(raw_vars, allowed_blocks, mandatory_keys,
                                  allowed_classes, optional_keys=None):
    """Checks the integrity of a variable db.

    This function exists
    to make sure devs didn't make any mistakes while writing
    the database [don't trust humans!!].

    Parameters
    ----------
    raw_vars : dict
               The raw variable database.
    allowed_blocks : list
                     The list of allowed blocks.
    mandatory_keys : list
                     The list of mandatory keys for each variable.
    allowed_classes : list
                      The list of allowed Variable classes.
    optional_keys : list, optional
                    The list of optional keys if relevant.

    Returns
    -------
    The raw_vars if everything is ok.
    """
    if optional_keys is None:
        optional_keys = []
    for varname, params in raw_vars.items():
        # check all mandatory keys are present
        for key in mandatory_keys:
            if key not in params:
                raise ValueError(f"'{varname}' should contain '{key}'")
        # check that all keys are either in mandatory or optional
        for key in params:
            if key not in mandatory_keys and key not in optional_keys:
                raise ValueError(f"'{varname}' has unknown parameter: '{key}'")
        # check that the var class is allowed
        if params["class"] not in allowed_classes:
            raise ValueError(f"'{varname}' class is not allowed.")
        # check that the var block is allowed
        block = params["block"]
        if block not in allowed_blocks:
            raise ValueError(f"'{block}' block of '{varname}' is not allowed.")
        if "allowed" in params:
            # if allowed is present and it is a dict, type should be a dict
            if isinstance(params["allowed"], dict):
                if "type" not in params:
                    raise ValueError(f"'allowed' is dict for {varname} but "
                                     "'type' is not set to dict...")
                if params["type"] is not dict:
                    raise ValueError(f"'allowed' is dict for {varname} but "
                                     "'type' is not set to dict...")
        if "type" in params:
            if not is_list_like(params["type"]):
                types = (params["type"], )
            else:
                types = params["type"]
            for typ in types:
                if typ not in ALLOWED_TYPES:
                    raise TypeError(f"'{typ}' not allowed...")
        if "len" in params:
            # check that length is an integer
            if not isinstance(params["len"], int):
                raise TypeError(f"'len' should be an integer for {varname}...")
    return raw_vars
