from .bases import BaseQERestarter


class QEEpsilonRestarter(BaseQERestarter):
    """Restarter class for a qe_epsilon calculation."""

    _calctype = "qe_epsilon"
    _loggername = "QEEpsilonRestarter"
