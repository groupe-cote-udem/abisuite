from .bases import BaseQERestarter


class QEQ2RRestarter(BaseQERestarter):
    """Restarter class for a qe_q2r calculation."""

    _calctype = "qe_q2r"
    _loggername = "QEQ2RRestarter"
