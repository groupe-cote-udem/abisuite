from .bases import BaseQERestarter


class QEPPRestarter(BaseQERestarter):
    """Restarter class for a qe_pp calculation."""

    _calctype = "qe_pp"
    _loggername = "QEPPRestarter"
