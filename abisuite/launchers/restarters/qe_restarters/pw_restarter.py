import math

from .bases import BaseQERestarter
from ....handlers import CalculationDirectory, QEPWInputFile, QEPWLogFile


async def get_qe_pw_final_geometry(
        workdir: CalculationDirectory | str) -> None:
    """Returns a dict of variables containing the optimized input variables.

    Parameters
    ----------
    workdir: str, CalculationDirectory
        The workdir where to pull the final geometry from.

    Raises
    ------
    LookupError: If could not get final positions.
    ValueError: If previous calculation was not 'vc-relax'.
    NotImplementedError: If ibrav is not implemented.
    """
    async with await QEPWInputFile.from_calculation(workdir) as inp:
        ibrav = inp.input_variables["ibrav"]
        atomic_positions = inp.input_variables["atomic_positions"].copy()
        atomic_species = inp.input_variables["atomic_species"]
        prev_calculation = inp.input_variables["calculation"]
        ntyp = inp.input_variables["ntyp"]
        nat = inp.input_variables["nat"]
        pseudo_dir = inp.input_variables["pseudo_dir"]
    to_rtn = {"atomic_positions": atomic_positions,
              "atomic_species": atomic_species,
              "ibrav": ibrav, "pseudo_dir": pseudo_dir,
              "nat": nat, "ntyp": ntyp,
              }
    async with await QEPWLogFile.from_calculation(workdir) as log:
        final_positions = log.final_atomic_positions
        if final_positions is None:
            raise LookupError(
                    f"Could not get final positions from {log.path}.")
        atomic_positions["positions"].update(final_positions)
        if prev_calculation == "relax":
            return to_rtn
        # also return new celldm(*)s the computation will depend of the
        # type of bravais lattice since it is rather comfusing how QE
        # prints the final results combined with the way they are input...
        # they basically prints the optimized lattice vectors
        if prev_calculation != "vc-relax":
            raise ValueError(
                    "Previous calculation should have been of type "
                    "'vc-relax'.")
        vectors = log.final_geometry["primitive_vectors"]
        # documentation for ibrav:
        # https://www.quantum-espresso.org/Doc/INPUT_PW.html#idm200
        celldm1 = inp.input_variables["celldm(1)"]
        if ibrav == 2:
            # FCC. new celldm(1) is 2 * any non zero element * previous
            celldm1 *= 2 * abs(vectors[0][0])
            to_rtn["celldm(1)"] = celldm1
            return to_rtn
        if ibrav == 4:
            # hexagonal and trigonal lattices
            # celldm1 = a
            # celldm3 = c/a
            to_rtn["celldm(1)"] = celldm1 * vectors[0][0]
            to_rtn["celldm(3)"] = vectors[2][2]
            return to_rtn
        if ibrav == 7:
            # body centered tetragonal
            # celldm(1) = a
            # celldm(2) = c/a
            to_rtn["celldm(1)"] = celldm1 * vectors[0][0] * 2
            to_rtn["celldm(3)"] = vectors[2][2] * 2
            return to_rtn
        if ibrav == -13:
            # base-centered monoclinic with unique axis = b
            # celldm1 = a
            # celldm2 = b/a
            # celldm3 = c/a
            # celldm5 = cos(beta)
            # vector00 = a/2
            # vector01 = b/2
            # vector20 = c * cos(beta)
            # vector22 = c * sin(beta)
            # celldm2 = inp.input_variables["celldm(2)"]
            # celldm3 = inp.input_variables["celldm(3)"]
            # celldm5 = inp.input_variables["celldm(5)"]
            to_rtn["celldm(1)"] = celldm1 * vectors[0][0] * 2
            to_rtn["celldm(2)"] = vectors[0][1] * 2
            to_rtn["celldm(3)"] = math.sqrt(
                    vectors[2][0] ** 2 + vectors[2][2] ** 2)
            to_rtn["celldm(5)"] = vectors[2][0] / to_rtn["celldm(3)"]
            return to_rtn
        raise NotImplementedError(f"ibrav = {ibrav.value}")


class QEPWRestarter(BaseQERestarter):
    """Restarter class for a qe_pw calculation."""

    _calctype = "qe_pw"
    _loggername = "QEPWRestarter"

    async def recover_calculation(
            self, *args, use_last_geometry=False, **kwargs):
        """Recover a calculation.

        Parameters
        ----------
        use_last_geometry : bool, optional
            If True, the last atomic geometry is used in the
            case of a 'relax' calculation.
        """
        await super().recover_calculation(*args, **kwargs)
        if not use_last_geometry:
            # nothing else to do
            return
        # need to dig for the last geometry
        # for now only works for the 'relax' case.
        updated_vars = await get_qe_pw_final_geometry(
                self.calculation_to_recover.path)
        self.launcher.input_variables.update(updated_vars)
