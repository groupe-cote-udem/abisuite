from .bases import BaseQERestarter


class QEProjwfcRestarter(BaseQERestarter):
    """Restarter class for a qe_projwfc calculation."""

    _calctype = "qe_projwfc"
    _loggername = "QEProjwfcRestarter"
