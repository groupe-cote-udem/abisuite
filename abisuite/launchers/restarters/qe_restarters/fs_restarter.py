from .bases import BaseQERestarter


class QEFSRestarter(BaseQERestarter):
    """Restarter class for a qe_fs calculation."""

    _calctype = "qe_fs"
    _loggername = "QEFSRestarter"
