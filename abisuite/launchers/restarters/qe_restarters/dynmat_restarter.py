from .bases import BaseQERestarter


class QEDynmatRestarter(BaseQERestarter):
    """Restarter class for a qe_dynmat calculation."""

    _calctype = "qe_dynmat"
    _loggername = "QEDynmatRestarter"
