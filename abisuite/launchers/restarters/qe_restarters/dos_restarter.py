from .bases import BaseQERestarter


class QEDOSRestarter(BaseQERestarter):
    """Restarter class for a qe_dos calculation."""

    _calctype = "qe_dos"
    _loggername = "QEDOSRestarter"
