from .bases import BaseQERestarter


class QEMatdynRestarter(BaseQERestarter):
    """Restarter class for a qe_matdyn calculation."""

    _calctype = "qe_matdyn"
    _loggername = "QEMatdynRestarter"
