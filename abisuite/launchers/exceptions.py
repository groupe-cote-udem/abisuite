class TooManyCalculationsInQueueError(Exception):
    """Error class when there are too many calculation in the system queue."""

    pass
