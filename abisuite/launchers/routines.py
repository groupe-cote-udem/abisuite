import os
import subprocess

from .exceptions import TooManyCalculationsInQueueError
from .. import __USER_CONFIG__
from ..handlers import PBSFile
from ..linux_tools import cd


__SUBMIT_SCRIPT_FROM_QUEUING_SYSTEM__ = {
        "local": "bash",
        "slurm": "sbatch",
        "torque": "qsub",
        "grid_engine": "qsub",
        "pbs_professional": "qsub",
        }


async def submit_or_launch(
        pbs_file: PBSFile,
        background: bool = False,
        ignore_calculation_limit: bool = False,
        ) -> None:
    """Submits or launch a calculation.

    Parameters
    ----------
    pbs_file: PBSFile object
        The PBS file that we want to submit or launch.
    background : bool, optional
                 If True, all jobs will be submitted in the background
                 (with subprocess.Popen instead of subprocess.run).
                 This parameter is not considered for jobs that need
                 to be submitted through a queuing system.
    ignore_calculation_limit: bool, optional
        If True, we ignore the calculation limit set in the config file.
    """
    runfunc = subprocess.run if not background else subprocess.Popen
    exe = __SUBMIT_SCRIPT_FROM_QUEUING_SYSTEM__[pbs_file.queuing_system]
    command = [exe, pbs_file.path]
    if pbs_file.queuing_system != "local" and not ignore_calculation_limit:
        try:
            await check_queue()
        except TooManyCalculationsInQueueError as e:
            pbs_file._logger.error(
                f"Cannot launch '{pbs_file.jobname}' ('{pbs_file.path}')"
                )
            raise e
    with cd(os.path.dirname(pbs_file.path)):
        runfunc(command)
    if not background and pbs_file.queuing_system != "local":
        from abisuite import __SYSTEM_QUEUE__
        # adjust system queue in case it does not read queue immediately
        # in case another calculation is quickly submitted after this one
        async with __SYSTEM_QUEUE__ as queue:
            queue.njobs += 1


async def check_queue(max_jobs_in_queue=None):
    """Checks if the number of jobs in queue exceeds a user given threshold.

    Parameters
    ----------
    max_jobs_in_queue: int, optional
        Gives the maximum amount of jobs allowed in queue. If maximum is
        reached, an error is raised. If None, takes the attribute set in
        the config file. If this one is None as well, no limit is set.
    """
    if max_jobs_in_queue is None:
        if __USER_CONFIG__.SYSTEM.max_jobs_in_queue is not None:
            max_jobs_in_queue = __USER_CONFIG__.SYSTEM.max_jobs_in_queue
    if max_jobs_in_queue is None:
        return
    from abisuite import __SYSTEM_QUEUE__
    async with __SYSTEM_QUEUE__ as queue:
        if queue.njobs >= max_jobs_in_queue:
            # maximum jobs reached. raise an error
            raise TooManyCalculationsInQueueError(
                    f"There are {queue.njobs} running jobs already"
                    f", which is >= than the maximum number of "
                    f"jobs allowed ("
                    f"{max_jobs_in_queue}).")


def find_cmd_from_config_entries(software, calctype):
    """Return the command path from the config file for a given program."""
    # try to locate the script name in the config file
    script = calctype.split("_")[-1]
    return __USER_CONFIG__[software.upper()][script + "_path"]
