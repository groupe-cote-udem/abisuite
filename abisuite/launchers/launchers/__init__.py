from .abinit_launchers import (
        AbinitAnaddbLauncher, AbinitCut3DLauncher, AbinitLauncher,
        AbinitMrgddbLauncher, AbinitOpticLauncher,
        )
from .qe_launchers import (
        QEDOSLauncher,
        QEDynmatLauncher,
        QEEpsilonLauncher,
        QEEPWLauncher,
        QEFSLauncher,
        QEKpointsLauncher,
        QELD1Launcher,
        QEMatdynLauncher,
        QEPHLauncher,
        QEPPLauncher, 
        QEProjwfcLauncher,
        QEPWLauncher,
        QEPW2Wannier90Launcher,
        QEQ2RLauncher,
        )
from .wannier90_launchers import (
        Wannier90Launcher,
        )


# list to keep order (useful for testing)
__ALL_LAUNCHERS__ = {
        "abinit": AbinitLauncher,
        "abinit_anaddb": AbinitAnaddbLauncher,
        "abinit_cut3d": AbinitCut3DLauncher,
        "abinit_mrgddb": AbinitMrgddbLauncher,
        "abinit_optic": AbinitOpticLauncher,
        "qe_dos": QEDOSLauncher,
        "qe_dynmat": QEDynmatLauncher,
        "qe_epsilon": QEEpsilonLauncher,
        "qe_epw": QEEPWLauncher,
        "qe_fs": QEFSLauncher,
        "qe_kpoints": QEKpointsLauncher,
        "qe_ld1": QELD1Launcher,
        "qe_matdyn": QEMatdynLauncher,
        "qe_ph": QEPHLauncher,
        "qe_pp": QEPPLauncher,
        "qe_projwfc": QEProjwfcLauncher,
        "qe_pw": QEPWLauncher,
        "qe_pw2wannier90": QEPW2Wannier90Launcher,
        "qe_q2r": QEQ2RLauncher,
        "wannier90": Wannier90Launcher,
        }
