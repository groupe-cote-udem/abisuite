from .bases import BaseQELauncher
from ....handlers import QEKpointsInputFile


class QEKpointsLauncher(BaseQELauncher):
    """Launcher for the kpoints.x utility of Quantum Espresso."""

    _calctype = "qe_kpoints"
    _loggername = "QEKpointsLauncher"
    _input_file_handler_class = QEKpointsInputFile
    _override_pbs_command_line = False

    async def set_input_variables(self, inputs):
        """Sets input variables."""
        inputs = await self._preprocess_inputs(inputs)
        for attr, val in inputs.items():
            setattr(self.input_file, attr, val)

    async def _preprocess_inputs(self, inputs):
        inputs = self._overwrite_input_var(
                inputs, "filout",
                # limited string size so path must be short
                self.jobname + ".mesh"
                )
        return inputs
