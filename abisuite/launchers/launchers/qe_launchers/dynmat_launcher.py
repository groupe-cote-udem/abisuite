from .bases import BaseQELauncherForFildyn
from ....handlers import QEDynmatInputFile


class QEDynmatLauncher(BaseQELauncherForFildyn):
    """Launcher class for a QE calculation using the dynmat.x script."""

    _loggername = "QEDynmatLauncher"
    _input_file_handler_class = QEDynmatInputFile
    _calctype = "qe_dynmat"

    async def validate(self, *args, **kwargs):
        """Validate the calculation."""
        await super().validate(*args, **kwargs)
        # check that the '.dyn' file is there or has been put in the files
        # to link
        await self._check_for_input_file(".dyn")
