from .bases import (
        BasePostPWLauncher,
        BaseQELauncherWithOutdirPrefix,
        )
from ....handlers import QEEpsilonInputFile


class QEEpsilonLauncher(BaseQELauncherWithOutdirPrefix, BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using epsilon.x."""

    _loggername = "QEEpsilonLauncher"
    _input_file_handler_class = QEEpsilonInputFile
    _calctype = "qe_epsilon"
