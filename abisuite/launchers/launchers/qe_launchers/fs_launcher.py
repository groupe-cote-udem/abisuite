from .bases import (
        BasePostPWLauncher,
        BaseQELauncherWithOutdirPrefix,
        )
from ....handlers import QEFSInputFile


# Watchout deadly diamond of death with the class below!!
class QEFSLauncher(BaseQELauncherWithOutdirPrefix,
                   BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using the fs.x."""

    _loggername = "QEFSLauncher"
    _input_file_handler_class = QEFSInputFile
    _calctype = "qe_fs"
