import os

from .bases import (
        BasePostPWLauncher,
        BaseQELauncherWithOutdirPrefix,
        )
from ....handlers import QEProjwfcInputFile


class QEProjwfcLauncher(BaseQELauncherWithOutdirPrefix,
                        BasePostPWLauncher):
    """Launcher class for a QE Calculation using projwfc.x script."""

    _loggername = "QEProjwfcLauncher"
    _input_file_handler_class = QEProjwfcInputFile
    _calctype = "qe_projwfc"

    async def _preprocess_inputs(self, inputs):
        inputs = await BaseQELauncherWithOutdirPrefix._preprocess_inputs(
                self, inputs)
        filpdos = os.path.join(self.output_data_dir.path, self.jobname)
        inputs = self._overwrite_input_var(inputs, "filpdos", filpdos)
        filproj = os.path.join(self.output_data_dir.path,
                               self.jobname + ".proj")
        inputs = self._overwrite_input_var(inputs, "filproj", filproj)
        return inputs
