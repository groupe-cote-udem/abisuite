import os

from .bases import (
        BasePostPWLauncher,
        BaseQELauncherWithOutdirPrefix,
        )
from ....handlers import QEDOSInputFile
from ....handlers.file_approvers import QEDOSInputParalApprover


class QEDOSLauncher(BaseQELauncherWithOutdirPrefix,
                    BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using dos.x."""

    _calctype = "qe_dos"
    _loggername = "QEDOSLauncher"
    _input_file_handler_class = QEDOSInputFile
    _paral_approver_class = QEDOSInputParalApprover

    async def _preprocess_inputs(self, inputs):
        inputs = await BaseQELauncherWithOutdirPrefix._preprocess_inputs(
                self, inputs)
        fildos = os.path.join(self.output_data_dir.path, self.jobname + ".dos")
        inputs = self._overwrite_input_var(inputs, "fildos", fildos)
        return inputs
