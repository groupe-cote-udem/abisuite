import os

from .bases import (
        BasePostPWLauncher,
        BaseQELauncherWithOutdirPrefix,
        )
from ....handlers import QEPPInputFile


class QEPPLauncher(BaseQELauncherWithOutdirPrefix, BasePostPWLauncher):
    """Launcher class for a Quantum Espresso Calculation using pp.x script."""

    _loggername = "QEPPLauncher"
    _input_file_handler_class = QEPPInputFile
    _calctype = "qe_pp"

    async def _preprocess_inputs(self, inputs):
        inputs = await super()._preprocess_inputs(inputs)
        # setup the output file filplot
        fil = os.path.join(self.output_data_dir.path, self.jobname + ".wavefc")
        inputs = self._overwrite_input_var(inputs, "filplot", fil)
        # also the xsf output file
        xsf = os.path.join(self.output_data_dir.path, self.jobname + ".xsf")
        inputs = self._overwrite_input_var(inputs, "fileout", xsf)
        return inputs

    async def validate(self, *args, **kwargs):
        """Validate the calculation."""
        await super().validate(*args, **kwargs)
        # check for a ".save" file has been linked to the calculation
        await self._check_for_input_file(".save")
