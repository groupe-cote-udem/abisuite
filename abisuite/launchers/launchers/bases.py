import abc
import asyncio
import datetime
import glob
import os

import aiofiles.os

from ..linkers import __ALL_LINKERS__
from ..restarters import __ALL_RESTARTERS__
from ..routines import find_cmd_from_config_entries, submit_or_launch
from ... import __USER_CONFIG__
from ...bases import BaseCalctypedUtility
from ...databases import DBCalculation
from ...exceptions import DevError
from ...handlers import CalculationDirectory, MetaDataFile, PBSFile
from ...handlers.file_handlers.bases import BaseFileHandler
from ...routines import full_abspath, is_int, is_list_like


class BaseLauncherPBSOnly(BaseCalctypedUtility, abc.ABC):
    """Base class for Launchers that only implements the PBS file.

    Useful for scripts like 'cut3d' that only process a single file.
    An input file is also implemented.

    Basically this does the same as the BaseLauncher class except
    that there is less structure (does not use the CalculationDirectory
    class).
    """

    _input_file_handler_class = None

    def __init__(self, *args, **kwargs):
        """Init method for BaseLauncherPBSOnly."""
        if self._input_file_handler_class is None:
            raise DevError("Need to set input file handler class.")
        self._executables_directory = None
        self._build = None
        self._pbs_file = None
        self._input_file = None
        super().__init__(*args, **kwargs)

    # set these as we want to have DRY code between all possible
    # parameters we can set the pbs file and setting them through launcher
    def __getattr__(self, attr):
        # NOTE: if an attr is defined in this class => it will take it before
        # going through this method
        if attr in self.pbs_file.structure.all_attributes or attr in dir(
                self.pbs_file.structure):
            return getattr(self.pbs_file, attr)
        raise AttributeError(
                f"{self.__class__} does not have attribute: '{attr}'.")

    def __setattr__(self, attr, value):
        if attr in dir(self) or attr.startswith("_"):
            BaseCalctypedUtility.__setattr__(self, attr, value)
            return
        if attr in self.pbs_file.structure.all_attributes or attr in dir(
                self.pbs_file.structure):
            setattr(self.pbs_file, attr, value)
            return
        raise AttributeError(
                f"{self.__class__} does not have attribute: '{attr}'.")

    @property
    def build(self):
        """Build directory entry name as specified in the config file."""
        if self._build is not None:
            return self._build
        # set the default build
        self._logger.debug("Setting the default build for the calculation.")
        self.build = getattr(
                __USER_CONFIG__, self.software.upper()).default_build
        return self.build

    @build.setter
    def build(self, build):
        """Set a build instead of a command + executables directory."""
        if build not in __USER_CONFIG__:
            raise ValueError(f"{build} is not defined in config file.")
        entry = __USER_CONFIG__[build]
        # set executables directory
        self.executables_directory = entry.build_path
        # set modules
        for module_type in ("to_use", "to_load", "to_unload", "to_swap"):
            attr_name = f"modules_{module_type}"
            setattr(self, attr_name, getattr(entry, attr_name))
        self.command = find_cmd_from_config_entries(
                self.software, self.calctype)
        self._build = build
        # set default lines before if needed
        if not len(self.lines_before):
            self.lines_before = entry.default_lines_before
        # do nothing if there are already lines before to not overwrite them
        # or to conflict them

    @property
    def command(self):
        """Return the command line."""
        try:
            return self.pbs_file.command
        except ValueError:
            # need to set command using default command
            cmd = find_cmd_from_config_entries(self.software, self.calctype)
            self._process_command(cmd)
        return self.pbs_file.command

    @command.setter
    def command(self, command):
        # check that script exist and is coherant with Launcher
        if self._expected_script not in command:
            raise ValueError(f"Expected {self._expected_script} in command")
        self._process_command(command)

    # @property
    # def command_arguments(self):
    #     return self.pbs_file.command_arguments

    # @command_arguments.setter
    # def command_arguments(self, command_arguments):
    #     self.pbs_file.command_arguments = command_arguments

    # @property
    # def cpus_per_task(self):
    #     return self.pbs_file.cpus_per_task

    # @cpus_per_task.setter
    # def cpus_per_task(self, c):
    #     self.pbs_file.cpus_per_task = c

    @property
    def executables_directory(self):
        """Return the executables directory."""
        if self._executables_directory is not None:
            return self._executables_directory
        # if None, return the default dir stated in CONFIG
        self._logger.debug("Setting default executables directory "
                           "from config file.")
        self.build = __USER_CONFIG__[self.software.upper()]["default_build"]
        return self.executables_directory

    @executables_directory.setter
    def executables_directory(self, directory):
        directory = full_abspath(directory)
        if not os.path.isdir(directory):
            raise NotADirectoryError(f"{directory}")
        self._executables_directory = directory

    @property
    def _expected_script(self):
        return self.calctype.split("_")[-1]

    @property
    def input_file(self):
        """Return the input file handler."""
        if self._input_file is not None:
            return self._input_file
        self._input_file = self._input_file_handler_class(
                loglevel=self._loglevel)
        return self.input_file

    @property
    def jobname(self):
        """Return the jobname."""
        try:
            return self.pbs_file.jobname
        except ValueError as e:
            self._logger.exception("An error occured while getting jobname.")
            raise e

    # just an alias
    @property
    def modules(self):
        """Return the list of modules to load."""
        return self.modules_to_load

    @modules.setter
    def modules(self, modules):
        self.modules_to_load = modules

    @property
    def pbs_file(self):
        """Return the pbs file handler."""
        if self._pbs_file is not None:
            return self._pbs_file
        self._pbs_file = PBSFile(loglevel=self._loglevel)
        return self.pbs_file

    @property
    def queuing_system(self):
        """Return the queuing system."""
        try:
            return self.pbs_file.queuing_system
        except ValueError:
            # set default
            self.queuing_system = __USER_CONFIG__.SYSTEM.queuing_system
        return self.queuing_system

    @queuing_system.setter
    def queuing_system(self, queuing_system):
        self.pbs_file.queuing_system = queuing_system

    async def launch(self, *args, **kwargs):
        """Alias for the 'run' method."""
        await self.run(*args, **kwargs)

    @abc.abstractmethod
    async def run(self, *args, **kwargs):
        """Run the calculation."""
        pass

    def _process_command(self, command):
        # process the command to make sure it is well set
        # copy arguments to make sure they are still set after processing cmd
        args_already_set = self.command_arguments.copy()
        split = command.split(" ")
        cmd = split[0]
        if not os.path.isfile(cmd):
            # try appending the executables directory
            cmd = os.path.join(self.executables_directory, cmd)
        if len(split) - 1:
            # reset arguments based on new cmd string
            cmd = " ".join([cmd] + split[1:])
            args_already_set = {}
        self.pbs_file.command = cmd
        if len(args_already_set):
            self.command_arguments = args_already_set


class BaseLauncher(BaseLauncherPBSOnly, abc.ABC):
    """Base class for any launchers.

    User should not use this class directly.
    User should either use the already implemented Launcher classes or
    subclass this class.

    A launcher object will collect file
    handlers to generate a calculation. It will take input variables and
    make some checks using Input Approvers objects to make sure input
    variables are consistent and make sense.

    workdir/ -> input file, log file, out file, stderr goes there
    ----input_data/ -> all input data from another calculation goes there
    ----run/ -> files file, pbs file some output files goes there
    -------output_data/ -> other outputs will go there
    """

    _override_pbs_command_line = False
    _paral_approver_class = None
    _parent_approver_class = None

    def __init__(self, jobname, *args, **kwargs):
        """Base launcher class.

        Parameters
        ----------
        jobname : str
                  This is the jobname of the calculation. All files will
                  have this name.
        """
        super().__init__(*args, **kwargs)
        self._calculation_directory = CalculationDirectory(
                self._input_file_handler_class,
                loglevel=self._loglevel
                )
        # set default queuing system from the start
        self._set_defaults()
        self.meta_data_file.calctype = self._calctype
        # set jobname at the end (at least after calculation directory created)
        self.jobname = jobname
        # project name for database
        self._project = ""
        self._allow_connection_to_database = True
        # create a list of files to delete after writing the job
        # (for temporary files)
        self._files_to_delete = []
        if self._override_pbs_command_line:
            self.pbs_file.command_line = self._override_pbs_command_line

    @property
    def approver(self):
        """Return the input file's approver object."""
        return self.input_file.approver

    @property
    def allow_connection_to_database(self):
        """Set to True to allow connection to database."""
        return self._allow_connection_to_database

    @allow_connection_to_database.setter
    def allow_connection_to_database(self, allow):
        if allow is not True and allow is not False:
            raise TypeError("'allow_connection_to_database' must be a bool.")
        self._allow_connection_to_database = allow

    @property
    def calculation_directory(self):
        """Return the calculation directory handler."""
        return self._calculation_directory

    async def set_calculation_directory(
            self, calc: CalculationDirectory) -> None:
        """Set the calculation directory instance."""
        if not isinstance(calc, CalculationDirectory):
            raise TypeError(
                    "Expected a CalculationDirectory instance but got "
                    f"'{calc}' instead.")
        # make sure jobname is synced
        await calc.set_jobname(self.jobname)
        self._calculation_directory = calc

    @property
    def calctype(self):
        """Return the calculation's type."""
        return self.meta_data_file.calctype

    @property
    def files_to_copy(self):
        """Return the list of files to copy."""
        return self.meta_data_file.copied_files

    @property
    def files_to_link(self):
        """Return the list of files to link."""
        return self.meta_data_file.linked_files

    @property
    def input_data_dir(self):
        """Return the input data directory handler."""
        return self.calculation_directory.input_data_directory

    @property
    def input_file(self):
        """Return the input file handler."""
        return self.calculation_directory.input_file

    @property
    def input_variables(self):
        """Return the input variables dictionary."""
        try:
            return self.input_file.input_variables
        except ValueError:
            raise ValueError("Need to set the input variables!")

    async def set_input_variables(self, input_variables):
        """Sets the input variables."""
        self.input_file.input_variables = input_variables

    @input_variables.deleter
    def input_variables(self):
        del self.input_file.input_variables

    @property
    def jobname(self):
        """Return the jobname."""
        try:
            return self.meta_data_file.jobname
        except Exception:
            return BaseLauncherPBSOnly.jobname.fget(self)

    @jobname.setter
    def jobname(self, jobname):
        # BaseLauncherPBSOnly.jobname.fset(self, jobname)
        self.pbs_file.jobname = jobname
        self.meta_data_file.jobname = jobname

    @property
    def output_data_dir(self):
        """Return the output data directory handler."""
        # output dir in run dir
        return self.calculation_directory.run_directory.output_data_directory

    @property
    def parents(self):
        """The list of parent calculations.

        This info is stored in the Meta Data File.
        """
        return self.meta_data_file.parents

    @property
    def pbs_file(self):
        """Return the pbs file handler."""
        return self.calculation_directory.pbs_file

    @property
    def project(self):
        """Return the project tag."""
        return self._project

    @project.setter
    def project(self, project):
        if not isinstance(project, str):
            raise TypeError("'project' must be a string.")
        self._project = project

    # the pseudos property is not relevant to all launchers and thus it may
    # be just None
    @property
    def pseudos(self):
        """Return the list of pseudopotential files."""
        return self.input_file.pseudos

    async def set_pseudos(self, pseudos):
        """Sets pseudos."""
        await self.input_file.set_pseudos(pseudos)

    @property
    def rundir(self):
        """Return the run directory handler."""
        return self.calculation_directory.run_directory

    @property
    def workdir(self):
        """Return the workdir of the calculation."""
        return self.calculation_directory.path

    async def set_workdir(self, workdir):
        """Sets workdir asynchronously."""
        workdir = full_abspath(workdir)
        await self.calculation_directory.set_path(workdir)
        await self.meta_data_file.set_path(
                os.path.join(self.workdir, "." + self.jobname + ".meta"))
        if is_list_like(self.input_file._parser_class._expected_ending):
            # many possible endings for input file extension
            await self.input_file.set_path(self._get_input_file_path())
        else:
            await self.input_file.set_path(
                    os.path.join(
                        self.workdir,
                        self.jobname +
                        self.input_file._parser_class._expected_ending)
                    )
        await self._set_pbs_file_paths()

    @property
    def meta_data_file(self):
        """Return the meta data file handler."""
        return self.calculation_directory.meta_data_file

    async def add_input_directory(self, dirpath, *args, **kwargs):
        """Add a whole directory to link in the input data.

        This methods just recursively calls 'add_input_file'.
        """
        if is_list_like(dirpath):
            for path in dirpath:
                await self.add_input_directory(path)
            return
        dirpath = full_abspath(dirpath)
        if "*" in dirpath:
            # stardef, call glob and add recursively
            self._logger.debug(f"Stardef found in: {dirpath}")
            alldirs = glob.glob(dirpath)
            await self.add_input_directory(
                    alldirs, *args, **kwargs)
            return
        if not await aiofiles.os.path.isdir(dirpath):
            raise NotADirectoryError(dirpath)
        await self._do_add_input_directory(dirpath, *args, **kwargs)

    async def _do_add_input_directory(
            self, dirpath, *args, subdir=None,
            keep_original_dirname=False, **kwargs):
        # at this point, dirpath SHOULD be a directory
        newsubdir = os.path.basename(dirpath)
        if not keep_original_dirname:
            # new subdirname is also changed
            newsubdir = self._get_target_name(newsubdir)
        if subdir is not None:
            subdir = os.path.join(subdir, newsubdir)
        else:
            subdir = newsubdir
        for name in await aiofiles.os.listdir(dirpath):
            subpath = os.path.join(dirpath, name)
            await self.add_input_file(
                    subpath, *args, keep_original_dirname=False,
                    subdir=subdir, **kwargs)

    async def add_input_file(
            self, filepath, *args, copy_file_with_suffix=None,
            keep_original_dirname=False, **kwargs):
        """Add an input file to link to the present calculation.

        A symbolic link will be created to this file once the files
        are written (unless copy is True in which case the files are copied).

        Supports '*' notation.
        E.g.: 'file.in*' will add each file that starts with 'file.in'

        Parameters
        ----------
        filepath : str, list
                   The path to the file to link. Can be a list.
        copy : bool, optional
               If True, the file will be copied instead of linked via
               a symlink.
        copy_file_with_suffix : str, list-like, optional
                                If not None, states the file suffix to copy
                                instead of linking. Useful for softwares
                                who overwrite certain files to preserve
                                previous calculations.
        keep_original_filename : bool, optional
                                 If False, a target name for input file is
                                 computed from the 'Launcher's jobname'.
                                 Else the same filename is used.
        keep_original_dirname : bool, optional
                                Same as 'keep_original_filename but for
                                directories in case subdirectories happen.
        subdir : str, optional
                 If not None, this is a prefix to the filename inside the
                 input data directory that will be added to the target.
        """
        if is_list_like(filepath):
            for path in filepath:
                await self.add_input_file(
                        full_abspath(path), *args,
                        copy_file_with_suffix=copy_file_with_suffix,
                        keep_original_dirname=keep_original_dirname,
                        **kwargs,
                        )
            return
        if isinstance(filepath, BaseFileHandler):
            path = filepath.path
        else:
            path = full_abspath(filepath)
        if "*" in path:
            self._logger.debug(f"Stardef found in: {path}")
            allfiles = glob.glob(path)
            await self.add_input_file(
                    allfiles, *args,
                    copy_file_with_suffix=copy_file_with_suffix,
                    keep_original_dirname=keep_original_dirname,
                    **kwargs)
            return
        # if a directory, append recursively and force subdir
        if await aiofiles.os.path.isdir(path):
            await self.add_input_directory(
                            path, *args,
                            copy_file_with_suffix=copy_file_with_suffix,
                            keep_original_dirname=keep_original_dirname,
                            **kwargs)
            return
        if copy_file_with_suffix is not None:
            if not is_list_like(copy_file_with_suffix):
                copy_file_with_suffix = (copy_file_with_suffix, )
        else:
            copy_file_with_suffix = []
        if not isinstance(copy_file_with_suffix, list):
            copy_file_with_suffix = list(copy_file_with_suffix)
        # from here we know its a file to link/copy
        await self._do_add_input_file(
                        path,
                        copy_file_with_suffix, *args,
                        **kwargs)

    async def _do_add_input_file(
            self, path,
            copy_file_with_suffix,
            copy=False,
            keep_original_filename=False,
            subdir=None):
        # From here, path SHOULD be a file and copy_file_with_suffix SHOULD
        # be a list
        for fileend in copy_file_with_suffix:
            if path.endswith(fileend):
                copy = True
                break
        if keep_original_filename:
            targetname = os.path.basename(path)
        else:
            targetname = self._get_target_name(path)
        target_path = self._get_target_path(targetname)
        if subdir is not None:
            target_path = os.path.join(os.path.dirname(target_path),
                                       subdir,
                                       os.path.basename(target_path))
        if copy:
            await self._add_copied_file_to_calcdir(target_path, path)
        else:
            await self._add_symlink_file_to_calcdir(target_path, path)

    def add_input_variables(self, new_variables):
        """Adds input variables to the input variables dictionary.

        Also updates variables if they were already there.
        """
        self.input_file.add_input_variables(new_variables)

    async def clean_workdir(self):
        """Delete everything in the working directory."""
        await self.calculation_directory.delete()

    def clear_linked_calculations(self):
        """Remove all traces of a linked calculation."""
        self.meta_data_file.parents = []  # reset parents in meta file
        # remove all input files
        self.clear_input_files()

    def clear_input_files(self):
        """Remove all input files to link and to copy."""
        self.input_data_dir.clear_content()
        self.meta_data_file.copied_files = []
        self.meta_data_file.linked_files = []

    def clear_input_variables(self):
        """Remove all input variables."""
        self.input_file.clear_input_variables()

    def clear_workdir(self):
        """Delete all files in workdir."""
        self.calculation_directory.delete()

    async def link_calculation(
            self, directory,
            allow_calculation_not_started=False,
            allow_calculation_failed=False):
        """Link a calculation to the Launcher.

        Parameters
        ----------
        allow_calculation_failed : bool, optional
            If True, allows the calculation to link to have failed.
            Otherwise an error is raised.
        allow_calculation_not_started : bool, optional
            If True, the linked calculation is allowed to not been started.
            Useful for parallel calculations which don't rely on a result
            but on a specific input.
        directory : str or int
                    The path to the calculation directory.
                    (Must contain a '.meta' file).
                    Can also be an integer. In that case, it is the ID of a
                    calculation in the database given as a kwarg.
        """
        if self.calctype not in __ALL_LINKERS__:
            self._logger.error(
                    f"Cannot link a calculation because no linker have been "
                    f"defined for this calctype: '{self.calctype}'.")
            raise NotImplementedError(self.calctype)
        if is_int(directory, int):
            # link agains a calculation in a SQLite database
            directory = DBCalculation.get(
                    DBCalculation.id == directory).calculation
        directory = full_abspath(directory)
        if "*" in directory:
            self._logger.debug(f"Stardef found in: {directory}")
            alldirs = glob.glob(directory)
            for path in alldirs:
                await self.link_calculation(path)
            return
        self._logger.info(f"Linking calculation with {directory}")
        if not await CalculationDirectory.is_calculation_directory(directory):
            raise NotADirectoryError(
                    f"Not a calculation directory: {directory}")
        # if already linked, just don't care
        if directory in self.meta_data_file.parents:
            return
        # from this point we need to link using a linker
        # first get linker
        async with await CalculationDirectory.from_calculation(
                directory, loglevel=self._loglevel) as calc:
            if await calc.calctype not in __ALL_LINKERS__[self.calctype]:
                self._logger.error(
                        f"Cannot link calculation of type "
                        f"'{await calc.calctype}' "
                        f"to a calculation of type '{self.calctype}'. "
                        f"Either because it is not allower or either it is not"
                        f" implemented.")
                raise NotImplementedError(await calc.calctype)
            linker_cls = __ALL_LINKERS__[self.calctype][await calc.calctype]
            # instanciate linker class and add what we need
            linker = linker_cls(self, loglevel=self._loglevel)
            acns = allow_calculation_not_started
            linker.allow_calculation_not_started = acns
            linker.allow_calculation_failed = allow_calculation_failed
            await linker.set_calculation_to_link(calc.path)
            await linker.link_calculation()

    # TODO: rebase this method with the 'link_calculation' one.
    async def recover_from(self, directory, *args, database=None, **kwargs):
        """Recover a (possibly failed) calculation.

        Parameters
        ----------
        directory : str or int
            The path (or database id) to the calculation to recover from.
        database : str, optional
            If not None, it is the path to the sqlite database file to use tio
            get the recovered calculation path from.

        Other args and kwargs are passed to he Restarter objects when calling
        their 'recover_calculation' method.
        """
        if self.calctype not in __ALL_RESTARTERS__:
            self._logger.error(
                    f"Cannot recover a calculation because no restarter"
                    " have been "
                    f"defined for this calctype: '{self.calctype}'.")
            raise NotImplementedError(self.calctype)
        if is_int(directory):
            # link agains a calculation in a SQLite database
            directory = DBCalculation.get(
                    DBCalculation.id == directory).calculation
        directory = full_abspath(directory)
        self._logger.info(f"Recover from calculation: {directory}")
        if not await CalculationDirectory.is_calculation_directory(directory):
            raise NotADirectoryError(
                    f"Not a calculation directory: {directory}")
        # from this point we need to invoke the restarted object.
        async with await CalculationDirectory.from_calculation(
                directory, loglevel=self._loglevel) as calc:
            if await calc.calctype != self.calctype:
                raise ValueError(
                        "Cannot recover from a different calctype: "
                        f"'{calc.calctype}'.")
            restarter_cls = __ALL_RESTARTERS__[self.calctype]
            # instanciate restarter class and add what we need
            restarter = restarter_cls(self, loglevel=self._loglevel)
            await restarter.set_calculation_to_recover(calc)
            await restarter.recover_calculation(*args, **kwargs)

    async def validate(
            self,
            bypass_validation: bool = False,
            bypass_pbs_validation: bool = False,
            only_validate_input_file: bool = False,
            ) -> None:
        """Validates the input variables with the appropriate Approver class.

        Parameters
        ----------
        bypass_validation : bool, optional
            If True, the input file is not validated.
        bypass_pbs_validation : bool, optional
            If True, the pbs file is not validated.
        only_validate_input_file: bool, optional
            Only validate input file that's it.
        """
        # call self.command here to atomatically set command if needed
        self.command
        if not bypass_pbs_validation or only_validate_input_file:
            await self.pbs_file.validate()
        if bypass_validation:
            return
        # check jobname length
        maxlen = __USER_CONFIG__.SYSTEM.max_jobname_len
        if maxlen is not None and self.queuing_system != "local":
            if len(self.jobname) > maxlen:
                raise ValueError(
                        f"Jobname '{self.jobname}' too long according to "
                        f"user config file (max {maxlen} chars.)")
        await self.input_file.validate()
        if self._paral_approver_class is not None:
            self._logger.debug("Cross validating input with mpi parameters.")
            cls = self._paral_approver_class
            inst = cls.from_approvers(self.input_file.approver,
                                      self.pbs_file.mpi_approver,
                                      loglevel=self._logger.level)
            input_paral_approver = inst
            await input_paral_approver.validate()
            if not input_paral_approver.is_valid:
                input_paral_approver.raise_errors()
        if only_validate_input_file:
            return
        if self._parent_approver_class is not None:
            self._logger.debug("Cross validating input variables with parent"
                               " calculation.")
            cls = self._parent_approver_class
            inst = await cls.from_approver(
                    self.input_file.approver,
                    self.meta_data_file.parents,
                    loglevel=self._loglevel)
            await inst.validate()
            if not inst.is_valid:
                inst.raise_errors()
        # check that all input files to link exist
        for path in self.meta_data_file.linked_files:
            if not await aiofiles.os.path.exists(path):
                raise FileNotFoundError(f"File to link not found: {path}.")
        self._logger.info("Validation: OK -> ready for launching!")

    async def run(self, **kwargs):
        """Run or submit the calculation (if on a supercluster)."""
        # check that batch file is there
        if not await aiofiles.os.path.isfile(self.pbs_file.path):
            raise FileNotFoundError(f"Cannot run calculation without batch"
                                    f" file: {self.pbs_file.path} use write()")
        if self.queuing_system not in (None, "local"):
            self._logger.info(f"Submitting {self.jobname}")
        else:
            self._logger.info(f"Launching {self.jobname}")
        await submit_or_launch(self.pbs_file, **kwargs)
        async with self.meta_data_file as meta:
            meta.submit_time = str(datetime.datetime.now())
        # NOTE: we link to database here mostly because of unittests.
        # If a database is defined in config file (path not None below) we
        # don't want to put test calculations in it automatically. Thus, since
        # unittest never call 'launch()' (for now 10/05/2019) we link here.
        # this goes the same for Sequencers.
        allow = self.allow_connection_to_database
        if __USER_CONFIG__.DATABASE.path is not None and allow:
            self.calculation_directory.connect_to_database()
            if self.calculation_directory.is_in_database:
                # reset status and monitored (used in restarts)
                self._logger.info(
                        "Calculation already in database. "
                        "Updating status and monitored attribute.")
                await self.calculation_directory.reset_status()
                self.calculation_directory.update_database_monitored(True)
            else:
                # add calculation to db
                if self.project:
                    self._logger.info(
                            "Adding calculation to database under project: "
                            f"'{self.project}'.")
                else:
                    self._logger.info("Adding calculation to database.")
                self.calculation_directory.add_to_database()
                self.calculation_directory.update_database_project(
                    self.project)

    async def write(
            self, force_queuing_system=None,
            bypass_validation=False, bypass_pbs_validation=False,
            overwrite=False, **kwargs):
        """Writes the files fot the calculation.

        Validates input parameters before writing files.

        Parameters
        ----------
        force_queuing_system : str, optional
            Used to force the queuing system for the pbs
            file. If None, the default system is used as
            defined in the config file. Possible values are the ones
            defined in the main __init__.py file of abisuite.
        bypass_validation : bool, optional
                            If True, the validation step for the input file
                            is skipped.
                            (Mainly used for testing purposes)
        bypass_pbs_validation : bool, optional
            Same as 'bypass_validation' but for pbs file only.
        overwrite : bool, optional
                    If False, an error is raised when trying to write the
                    files in an already existing calculation directory.

        Other kwargs are passed directly to all the writers write methods
        """
        # set the following before validating
        if force_queuing_system is not None:
            self.pbs_file.queuing_system = force_queuing_system
        # call self.command here to atomatically set command if needed
        self.command
        await self.validate(
                bypass_validation=bypass_validation,
                bypass_pbs_validation=bypass_pbs_validation)
        # give submit time even if we don't use the 'run' method afterwards
        # (it will be updated anyways)
        self.meta_data_file.submit_time = str(datetime.datetime.now())
        if await CalculationDirectory.is_calculation_directory(
                self.workdir) and not overwrite:
            raise IsADirectoryError(f"Is already a calculation directory: "
                                    f"{self.workdir}")
        await self.calculation_directory.write(overwrite=overwrite, **kwargs)
        # append children to all parents
        await asyncio.gather(
                *(self._add_child_to_meta(parent)
                    for parent in self.meta_data_file.parents))
        self._delete_temporary_files()

    async def _add_child_to_meta(self, parent):
        async with await MetaDataFile.from_calculation(parent) as meta:
            meta.add_child(self.workdir)

    @classmethod
    async def restart_from(
            cls, path, *args, clear_output=False, relink=False,
            run=True, new_workdir=None, **kwargs):
        """Set the meta data file and the pbs file from 'path'.

        Parameters
        ----------
        path : str
               Which calculation needs to be restarted.
        clear_output : bool, optional
                       If True, all the content of the directory is cleared.
                       Then everything is rewritten.
        relink : bool, optional
                 If True, all parents are relinked.
        run : bool, optional
              If True, the calculation is immediately launched.
        new_workdir : str, optional
                      If not None, specifies a new workdir for the restarted
                      calculation. Everything is relinked after that.
        All other args and kwargs:
            passed to the new Launcher instance.
        """
        new = cls(*args, **kwargs)
        await new.set_workdir(path)
        await new.meta_data_file.read()
        await new.pbs_file.read()
        await new.input_file.read()
        if new_workdir is not None:
            await new.set_workdir(new_workdir)
            # reset input variables if needed
            await new.set_input_variables(new.input_variables)
        if clear_output:
            new.clear_workdir()
            new.input_data_dir.clear_content()
            new.rundir.clear_content()
        # relink parents if needed
        if clear_output or relink or new_workdir is not None:
            for parent in new.meta_data_file.parents:
                await new.link_calculation(parent)
        if run:
            await new.run()
        return new

    async def _add_copied_file_to_calcdir(self, target_path, path):
        if await aiofiles.os.path.islink(path):
            path = await aiofiles.os.readlink(path)
        if os.path.basename(
                self.input_data_dir.path
                ) in target_path:
            # add file in input data directory
            await self.input_data_dir.add_copied_file(
                    target_path, path)
            # need to add manually to meta data file
            if path not in self.meta_data_file.copied_files:
                self.meta_data_file.copied_files.append(path)
        else:
            await self.calculation_directory.add_copied_file(target_path, path)

    async def _add_symlink_file_to_calcdir(self, target_path, path):
        """Adds a symlink that points to a certain input file.

        Parameters
        ----------
        target_path: str
            Where the symlinked will be written (it's final path).
        path: str
            The symlink source (where it points to).
        """
        if target_path.startswith(self.input_data_dir.path):
            # the symlink will be located into the input data dir
            if not await self.input_data_dir.contains(target_path):
                # only add if it does not exist already
                await self.input_data_dir.add_symlink(
                    target_path, path)
            # need to add manually to meta data file
            if path not in self.meta_data_file.linked_files:
                self.meta_data_file.linked_files.append(path)
        else:
            await self.calculation_directory.add_symlink(target_path, path)

    def _delete_temporary_files(self):
        # delete temporary files if necessary
        for file_to_delete in self._files_to_delete:
            if not isinstance(file_to_delete, str):
                # it's a file object
                name = file_to_delete.name
                file_to_delete.close()
                del file_to_delete
                file_to_delete = name
            if os.path.exists(file_to_delete):
                # else remove manually
                os.remove(file_to_delete)
            with self.meta_data_file as meta:
                # also delete from meta data file if they are present
                if file_to_delete in meta.copied_files:
                    meta.copied_files.remove(file_to_delete)
                if file_to_delete in meta.linked_files:
                    meta.linked_files.remove(file_to_delete)
        del self._files_to_delete
        self._files_to_delete = []  # reset

    def _get_input_file_path(self):
        # not a mandatory method for subclasses but if needed, it needs
        # to be implemented
        # this it is not an abstract method.
        raise NotImplementedError("_get_input_file_path")

    def _get_target_name(self, target):
        return os.path.basename(target)

    def _get_target_path(self, targetname):
        return os.path.join(self.input_data_dir.path, targetname)

    def _set_defaults(self):
        # set default attributes according to config file
        self.queuing_system = __USER_CONFIG__.SYSTEM.queuing_system
        # set default parameters if relevant
        if __USER_CONFIG__.SYSTEM.default_memory not in (0, None):
            self.memory = __USER_CONFIG__.SYSTEM.default_memory
        if __USER_CONFIG__.SYSTEM.default_project_account is not None:
            self.project_account = (
                    __USER_CONFIG__.SYSTEM.default_project_account)
        if __USER_CONFIG__.SYSTEM.default_project_code is not None:
            self.project_code = __USER_CONFIG__.SYSTEM.default_project_code
        if __USER_CONFIG__.SYSTEM.default_mpi_command is not None:
            self.mpi_command = __USER_CONFIG__.SYSTEM.default_mpi_command
        if __USER_CONFIG__.SYSTEM.default_lines_before:
            self.lines_before += __USER_CONFIG__.SYSTEM.default_lines_before

    async def _set_pbs_file_paths(self):
        # set the pbs file when 'workdir' is set/modified
        await self.pbs_file.set_path(
                os.path.join(self.rundir.path, self.jobname + ".sh"))
        self.pbs_file.input_file_path = self.input_file.path
        self.pbs_file.log_path = os.path.join(self.workdir,
                                              self.jobname + ".log")
        self.pbs_file.stderr_path = os.path.join(self.workdir,
                                                 self.jobname + ".stderr")
