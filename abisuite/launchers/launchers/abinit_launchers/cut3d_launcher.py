import os

import aiofiles.os

from ..bases import BaseLauncherPBSOnly
from ...routines import submit_or_launch
from ....handlers import (
        AbinitCut3DInputFile,
        CalculationDirectory,
        SymLinkFile,
        is_calculation_directory,
        )
from ....routines import full_abspath
from ....status_checkers.exceptions import CalculationNotFinishedError


class AbinitCut3DLauncher(BaseLauncherPBSOnly):
    """Launcher for the cut3d utility of Abinit."""

    _calctype = "abinit_cut3d"
    _loggername = "AbinitCut3DLauncher"
    _input_file_handler_class = AbinitCut3DInputFile

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._workdir = None
        self._calculation_directory = None
        # set pbs file
        self.pbs_file.queuing_system = "local"
        self.pbs_file.command = self.command

    @property
    def calculation_directory(self) -> CalculationDirectory:
        """The calculation directory."""
        return self._calculation_directory

    @calculation_directory.setter
    def calculation_directory(self, calc):
        self._calculation_directory = calc

    @property
    def workdir(self):
        """Return the calculation's workdir."""
        if self.calculation_directory is None:
            raise ValueError("Need to set 'workdir'.")
        return self.calculation_directory.path

    async def set_workdir(self, workdir):
        """Sets the working directory."""
        workdir = full_abspath(workdir)
        if await is_calculation_directory(workdir):
            self._calculation_directory = (
                    await CalculationDirectory.from_calculation(
                        workdir, loglevel=self._loglevel))
        else:
            self._calculation_directory = (
                    CalculationDirectory(
                        AbinitCut3DInputFile, loglevel=self._loglevel))
            await self.calculation_directory.set_path(workdir)
        # write next to the DEN file
        await self.pbs_file.set_path(os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.sh"))
        await self.input_file.set_path(os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.in"))
        self.pbs_file.input_file_path = self.input_file.path
        self.pbs_file.log_path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.log")
        self.pbs_file.stderr_path = os.path.join(
                self.calculation_directory.output_data_directory.path,
                "cut3d.stderr")
        self.input_file.output_file_path = "density.xsf"
        self.input_file.option = 9
        self.input_file.post_option = ["n", 0]

    async def link_den_file(self, den_path):
        """Link a DEN file to use with cur3D.

        Parameters
        ----------
        den_path: str
            The path towards the DEN file.
        """
        # add a symlink inside the output data dir that points towards it
        symlink = SymLinkFile(loglevel=self._loglevel)
        await symlink.set_path(os.path.join(
                self.calculation_directory.output_data_directory.path,
                os.path.basename(den_path)))
        symlink.source = den_path
        await symlink.write()
        self.input_file.data_file_path = symlink.path

    async def validate(self, *args, **kwargs):
        """Validate the calculation."""
        # get DEN filename
        for path in await aiofiles.os.listdir(
                self.calculation_directory.output_data_directory.path):
            if "DEN" in path:
                self.input_file.data_file_path = path
                break
        else:
            raise FileNotFoundError("Could not find DEN file.")
        await super().validate(*args, **kwargs)

    async def write(self, overwrite: bool = False) -> None:
        """Write the calculation's files."""
        await self.pbs_file.write(overwrite=overwrite)
        await self.input_file.write(overwrite=overwrite)

    async def run(self):
        """Run the calculation."""
        # only local run for now
        self._logger.info(f"Launching cut3d on calculation: '{self.workdir}'.")
        # if calculation not finished raise error
        if await is_calculation_directory(self.workdir):
            async with self.calculation_directory as calc:
                if (await calc.status)["calculation_finished"] is False:
                    raise CalculationNotFinishedError(self.workdir)
        await submit_or_launch(self.pbs_file, background=False)
