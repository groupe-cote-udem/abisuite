import os

from .bases import BaseAbinitLauncher
from ....handlers import (
        AbinitAnaddbFilesFile, AbinitAnaddbInputFile,
        )


class AbinitAnaddbLauncher(BaseAbinitLauncher):
    """Launcher class for an anaddb calculation."""

    _calctype = "abinit_anaddb"
    _files_file_handler_class = AbinitAnaddbFilesFile
    _input_file_handler_class = AbinitAnaddbInputFile
    _loggername = "AbinitAnaddbLauncher"

    @property
    def band_structure_file_path(self):
        """Return the band structure file path."""
        return self.files_file.band_structure_file_path

    @band_structure_file_path.setter
    def band_structure_file_path(self, band_structure_file_path):
        self.files_file.band_structure_file_path = band_structure_file_path

    @property
    def ddb_file_path(self):
        """Return the ddb file path."""
        return self.files_file.ddb_file_path

    @ddb_file_path.setter
    def ddb_file_path(self, ddb_file_path):
        self.files_file.ddb_file_path = ddb_file_path

    @property
    def ddk_file_path(self):
        """Return the ddk file path."""
        return self.files_file.ddk_file_path

    @ddk_file_path.setter
    def ddk_file_path(self, ddk_file_path):
        self.files_file.ddk_file_path = ddk_file_path

    @property
    def eph_data_prefix(self):
        """Return the eph data prefix."""
        return self.files_file.eph_data_prefix

    @eph_data_prefix.setter
    def eph_data_prefix(self, eph_data_prefix):
        self.files_file.eph_data_prefix = eph_data_prefix

    @property
    def gkk_file_path(self):
        """Return the gkk file path."""
        return self.files_file.gkk_file_path

    @gkk_file_path.setter
    def gkk_file_path(self, gkk_file_path):
        self.files_file.gkk_file_path = gkk_file_path

    async def set_workdir(self, workdir):
        """Set the calculation's workdir."""
        await super().set_workdir(workdir)
        # put input DDB into input data directory
        self.ddb_file_path = os.path.join(
                self.input_data_dir.path,
                self._add_prefixes_to_target("DDB"))
        self.band_structure_file_path = os.path.join(
                self.output_data_dir.path,
                "phonon_band_structure.dat")
        self.gkk_file_path = os.path.join(
                self.input_data_dir.path,
                self.jobname + "_gkk")
        self.eph_data_prefix = os.path.join(
                self.output_data_dir.path,
                self.jobname + ".eph")
        self.ddk_file_path = os.path.join(
                self.input_data_dir.path,
                self.jobname + "_ddk")

    def link_ddb_from(self, calculation):
        """Link a DDB file to this anaddb calculation."""
        self._link_output_file_from_calculation(calculation, contains="_DDB")

    async def validate(
            self, *args, only_validate_input_file: bool = False,
            **kwargs) -> None:
        """Validate the calculation.

        Parameters
        ----------
        only_validate_input_file: bool, optional
            If True, we only validate the input file.
        """
        await super().validate(
                *args, only_validate_input_file=only_validate_input_file,
                **kwargs)
        if only_validate_input_file:
            return
        # check that a DDB file was linked
        async for file_ in self.input_data_dir:
            if file_.path.endswith("DDB"):
                break
        else:
            raise FileNotFoundError(
                    "Need to link anaddb calculation against a mrgddb "
                    "calculation or to a DDB file!"
                    )

    def _add_prefixes_to_target(self, end):
        return "idat_" + self.jobname + "_" + end
