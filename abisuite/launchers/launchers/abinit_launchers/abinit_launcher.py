import asyncio
import os

from .bases import BaseAbinitLauncherNoFilesFile
from ..scf_launchers import BaseSCFLauncher
from .... import USER_CONFIG
from ....exceptions import DevError
from ....handlers import AbinitInputFile, CalculationDirectory
from ....handlers.file_approvers import AbinitInputParalApprover
from ....routines import (
        get_calculation_directory_from_calculation_file as gcdfcf,
        )


# variables to load when loading kpts variables
__ALL_ABINIT_KPTS_VARIABLES__ = (
        "ngkpt", "nshiftk", "shiftk", "kptrlatt", "kptbounds",
        "ndivk",
        )
# variables to keep when linking density
__ALL_ABINIT_DEN_KPTS_VARIABLES__ = [
        "ecut", "pawecutdg", "occopt", "occ", "ixc", "tsmear",
        ]
# variables to keep when linking WFK file.
__ALL_ABINIT_WFK_VARIABLES__ = (
        __ALL_ABINIT_DEN_KPTS_VARIABLES__.copy() + [
            "ngkpt", "nshiftk", "shiftk",  "nband",
            "nbdbuf", "kptrlatt", "nbdbuf", "ndivk",
            "kptbounds",
            ]
        )

__ABINIT_ONE_FROM_THESE_KPTS_VARIABLES__ = (
        ("ngkpt", "kptrlatt", "kptbounds"),
        # not mandatory variables
        ("shiftk", ),
        ("nshiftk", ),
        ("ndivk", ),
        )
__ALL_ABINIT_GEOMETRY_VARIABLES__ = (
        "acell", "rprim", "ntypat", "znucl", "natom", "typat",
        "xcart", "xred", "pp_dirpath", "pseudos",
        )
__ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__ = (
        ("xred", "xcart"),
        )
__ABINIT_ONE_FROM_THESE_DEN_KPTS_VARIABLES__ = (
        ("pawecutdg", ),  # not mandatory
        )


class AbinitLauncher(BaseAbinitLauncherNoFilesFile, BaseSCFLauncher):
    """Launcher class for an abinit calculation."""

    _calctype = "abinit"
    _input_file_handler_class = AbinitInputFile
    _loggername = "AbinitLauncher"
    _paral_approver_class = AbinitInputParalApprover
    _geometry_variables = __ALL_ABINIT_GEOMETRY_VARIABLES__
    _one_from_these_geometry_variables = (
            __ABINIT_ONE_FROM_THESE_GEOMETRY_VARIABLES__)
    _kpts_variables = __ALL_ABINIT_KPTS_VARIABLES__
    _one_from_these_kpts_variables = __ABINIT_ONE_FROM_THESE_KPTS_VARIABLES__
    # (
    #         "ngkpt", "nshiftk", "shiftk",
    #         )
    # change the input file redirection in pbs files to a more stable one
    _override_pbs_command_line = (
                "$MPIRUN $EXECUTABLE $INPUT > $LOG 2> $STDERR"
                )

    def __init__(self, *args, **kwargs):
        BaseAbinitLauncherNoFilesFile.__init__(self, *args, **kwargs)
        BaseSCFLauncher.__init__(self, *args, **kwargs)

    async def set_workdir(self, workdir):
        """Set the launcher's workdir."""
        await super().set_workdir(workdir)
        try:
            self._set_prefix_variables()
        except ValueError:
            pass

    async def set_input_variables(self, ivars, **kwargs):
        """Sets the input variables."""
        await super().set_input_variables(ivars, **kwargs)
        self._set_prefix_variables()
        if "pp_dirpath" in ivars:
            return
        if USER_CONFIG.DEFAULTS.default_pseudos_dir is None:
            self._logger.error("No default pseudo dir defined in config.")
            raise ValueError("No default pseudo dir defined in config"
                             " and it is not given in inputs.")
        else:
            self.input_variables.update(
                    {"pp_dirpath": USER_CONFIG.DEFAULTS.default_pseudos_dir})

    def _set_prefix_variables(self):
        # try setting input variables if not set
        if "tmpdata_prefix" not in self.input_variables:
            self.input_variables["tmpdata_prefix"] = os.path.join(
                self.output_data_dir.path, "tmp_" + self.jobname)
        if "outdata_prefix" not in self.input_variables:
            self.input_variables["outdata_prefix"] = os.path.join(
                self.output_data_dir.path, "odat_" + self.jobname)
        if "indata_prefix" not in self.input_variables:
            self.input_variables["indata_prefix"] = os.path.join(
                self.input_data_dir.path, "idat_" + self.jobname)

    async def link_bsr_from(self, calc):
        """Link BSR file from a previous calculation.

        Calling this will also link the same WFK and SCR files
        from the run that generated the BSR file.

        Parameters
        ----------
        calc : path to calculation.
        """
        await self._link_output_file_from_calculation(calc, contains="_BSR")
        irdbsreso = self.input_variables.get("irdbsreso", 0)
        if irdbsreso != 1:
            self._logger.info(
                    "Automatically setting 'irdbsreso' to 1 since we link a "
                    "BSR file.")
            self.input_variables["irdbsreso"] = 1
        wfk_linked = False
        scr_linked = False
        async with await CalculationDirectory.from_calculation(
                calc, loglevel=self._loglevel) as calc:
            async with calc.input_data_directory as idd:
                for handler in await idd.walk(paths_only=False):
                    if "_WFK" in handler.basename:
                        await self.link_wfk_from(gcdfcf(handler, max_layers=2))
                        wfk_linked = True
                    if "_SCR" in handler.basename:
                        await self.link_scr_from(gcdfcf(handler, max_layers=2))
                        scr_linked = True
                    if wfk_linked and scr_linked:
                        return
        if not wfk_linked:
            raise FileNotFoundError("Could not find WFK file to link.")
        if not scr_linked:
            raise FileNotFoundError("Could not find SCR file to link.")

    async def link_den_from(self, calc):
        """Link DEN file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        # use contains instead of endswith since the file could be in netcdf
        # format and thus endswith .nc
        await self._link_output_file_from_calculation(calc, contains="_DEN")
        # manually set irdwfk to 1 if not already
        irdden = self.input_variables.get("irdden", 0)
        if irdden != 1:
            self._logger.info(
                    "Automatically setting 'irdden' to 1 since we link a "
                    "DEN file.")
            self.input_variables["irdden"] = 1

        # if "irdden" not in self.input_variables:
        #     raise ValueError("Need to set 'irdden' to 1.")
        # else:
        #     if self.input_variables["irdden"] != 1:
        #         raise ValueError("Need to set 'irdden' to 1.")
        async with await CalculationDirectory.from_calculation(
                calc) as calcdir:
            async with calcdir.input_file as input_file:
                for varname in __ALL_ABINIT_DEN_KPTS_VARIABLES__:
                    if varname in input_file.input_variables:
                        # update input variables with this one
                        # if it is not equal or
                        # it is is not there, just set it but warn user
                        value = input_file.input_variables[varname]
                        if varname not in self.input_variables:
                            self._logger.info(
                                    f"Setting '{varname}' to '{value.value}'. "
                                    "Because of input DEN file."
                                    )
                            self.input_variables[varname] = value.value
                        else:
                            if value != self.input_variables[varname]:
                                self._logger.warning(
                                        f"Overwriting input variable "
                                        f"'{varname}' to '{value.value}' "
                                        "because of input DEN file.")
                                self.input_variables[varname] = value.value

    async def link_haydock_from(self, calc):
        """Link HAYDR_SAVE file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        await self._link_output_file_from_calculation(calc, contains="_HAYDR")
        irdhaydock = self.input_variables.get("irdhaydock", 0)
        if irdhaydock != 1:
            self._logger.info(
                    "Automatically setting 'irdhaydock' to 1 since we link a "
                    "HAYDOCK_SAVE file.")
            self.input_variables["irdhaydock"] = 1

    async def link_scr_from(self, calc):
        """Link SCR file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        # use contains instead of endswith since the file could be in netcdf
        # format and thus endswith .nc
        await self._link_output_file_from_calculation(calc, contains="_SCR")
        # manually set irdwfk to 1 if not already
        irdscr = self.input_variables.get("irdscr", 0)
        if irdscr != 1:
            self._logger.info(
                    "Automatically setting 'irdscr' to 1 since we link a "
                    "SCR file.")
            self.input_variables["irdscr"] = 1

    async def link_bs_reso_from(self, calc: str) -> None:
        """Link a BSR file from a previous calculation.

        Parameters
        ----------
        calc: str
            Path of the calculation that produced the BSR file.
        """
        await self._link_output_file_from_calculation(calc, endswith="_BSR")
        # manually set irdbsreso to 1 if needed
        irdbsreso = self.input_variables.get("irdbsreso", 0)
        if irdbsreso != 1:
            self._logger.info(
                    "Automatically setting 'irdbsreso' to 1 since we link a"
                    " BSR file.")
            self.input_variables["irdbsreso"] = 1

    async def link_wfk_from(self, calc):
        """Link a WFK file from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        await self._link_output_file_from_calculation(calc, contains="_WFK")
        # Input file must have same kpt parameters than the one used to compute
        # the WFK.
        # unless it is a nscf calculation or BSE or GW
        # then number of bands can't change
        # for now I just implemented for iscf = -2 but I think it's safe for
        # any value of iscf. I am lazy to change all the unittests....
        # FIXME: (above)
        iscf = self.input_variables.get("iscf", 7)
        async with await CalculationDirectory.from_calculation(
                calc) as calcdir:
            async with calcdir.input_file as input_file:
                for varname in __ALL_ABINIT_WFK_VARIABLES__:
                    if (iscf == -2 or await self.approver.is_gw_run or
                            await self.approver.is_bse_run) and varname in (
                            "nband",
                            "nbdbuf", "ngkpt", "kptrlatt"):
                        # skip this as this can change
                        continue
                    if varname in input_file.input_variables:
                        # update input variables with this one
                        # if it is not equal raise an error
                        # it is is not there, just set it but warn user
                        value = input_file.input_variables[varname]
                        if varname not in self.input_variables:
                            self._logger.info(
                                    f"Setting '{varname}' to '{value.value}'. "
                                    "Because of input WFK file."
                                    )
                            self.input_variables[varname] = value.value
                        else:
                            if value != self.input_variables[varname]:
                                self._logger.warning(
                                        f"Overwriting input variable "
                                        f"'{varname}' to '{value.value}' "
                                        "because of input WFK file.")
                                self.input_variables[varname] = value.value
        # manually set irdwfk to 1 if not already
        irdwfk = self.input_variables.get("irdwfk", 0)
        if irdwfk != 1:
            self._logger.info(
                    "Automatically setting 'irdwfk' to 1 since a "
                    "WFK file is linked.")
            self.input_variables["irdwfk"] = 1

        # if "irdwfk" not in self.input_variables:
        #     raise ValueError("Need to set 'irdwfk' to 1.")
        # else:
        #     if self.input_variables["irdwfk"] != 1:
        #         raise ValueError("Need to set 'irdwfk' to 1.")
        #     # else nothing to do

    async def link_ddk_from(self, calc):
        """Link 1WF* files from a previous calculation.

        Parameters
        ----------
        calc : path to calculation.
        """
        await self._link_output_file_from_calculation(calc, contains="_1WF")
        if "irdddk" not in self.input_variables:
            raise ValueError("Need to set 'irdddk' to 1.")
        else:
            if self.input_variables["irdddk"] != 1:
                raise ValueError("Need to set 'irdddk' to 1.")

    async def _link_output_file_from_calculation(
            self, calc, endswith=None, contains=None):
        if endswith is None and contains is None:
            raise DevError("Need to specify either 'endswith' or 'contains'.")
        if not await CalculationDirectory.is_calculation_directory(calc):
            raise NotADirectoryError(calc)
        async with await CalculationDirectory.from_calculation(
                calc) as calcdir:
            async with calcdir.output_data_directory as out_dir:
                coros = []
                async for filename in out_dir:
                    coros.append(
                            self._link_output_file(
                                filename, endswith, contains))
                await asyncio.gather(*coros)
        # add calculation to parents
        self.meta_data_file.add_parent(calc)

    async def _link_output_file(self, filename, endswith, contains):
        if endswith is not None:
            if not filename.path.endswith(endswith):
                return
        if contains is not None:
            if contains not in filename.path:
                return
        already_copied = filename.path in self.files_to_copy
        already_linked = filename.path in self.files_to_link
        if already_copied or already_linked:
            return
        await self.add_input_file(filename.path)

    async def validate(
            self, *args,
            only_validate_input_file: bool = False,
            **kwargs,
            ) -> None:
        """Validate the calculation."""
        self.input_file.approver.parents = self.parents
        await super().validate(
                *args,
                only_validate_input_file=only_validate_input_file,
                **kwargs)
        # check that all input files have been added
        if not only_validate_input_file:
            await self._cross_check_input_files_and_ird_variables()
            if self.input_variables.get("irdwfk", 0).value != 0:
                await self._check_input_file_added(contains="_WFK")

    async def _check_input_file_added(self, endswith=None, contains=None):
        if endswith is None and contains is None:
            raise DevError("endswith and contains cannot be None at same time")
        if endswith is not None and contains is not None:
            raise DevError(
                    "endswith and contains cannot be not None at same time.")
        # check that an input file has been linked
        async for fil in self.input_data_dir:
            if endswith is not None:
                if fil.path.endswith(endswith):
                    return
            if contains is not None:
                if contains in fil.path:
                    return
        # nothing found raise an error
        raise FileNotFoundError(
                f"'*{endswith or contains}* in {self.input_data_dir}'")

    async def _cross_check_input_files_and_ird_variables(self):
        irdwfk = self.input_variables.get("irdwfk", 0).value
        if irdwfk != 0:
            # check that a "WFK" file has been linked
            try:
                await self._check_input_file_added(contains="_WFK")
            except FileNotFoundError:
                raise FileNotFoundError(
                        "irdwfk is not 0 but no WFK file linked.")
        irdddk = self.input_variables.get("irdddk", 0).value
        if irdddk != 0:
            # check that _1WF* files have been linked
            try:
                await self._check_input_file_added(contains="_1WF")
            except FileNotFoundError:
                raise FileNotFoundError(
                        "irdddk is not 0 but no 1WF files linked.")
        irdscr = self.input_variables.get("irdscr", 0).value
        if irdscr != 0:
            # check that _1WF* files have been linked
            try:
                await self._check_input_file_added(contains="_SCR")
            except FileNotFoundError:
                raise FileNotFoundError(
                        "irdscr is not 0 but no SCR files linked.")

    def _add_prefixes_to_target(self, end):
        return (os.path.basename(self.input_variables["indata_prefix"].value) +
                "_" + end)
