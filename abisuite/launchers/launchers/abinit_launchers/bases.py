import abc
import os

from ..bases import BaseLauncher
from ....exceptions import DevError


DATA_FILE_ENDINGS = (
        "1WF",
        "BSR",
        "DDB",
        "DEN",
        "DOS",
        "EIG",
        "EVK",
        "GEO",
        "HAYDR_SAVE",
        "KSS",
        "POT", "SCR",
        "WFK",
        "WFQ",
        )


class BaseAbinitLauncherNoFilesFile(BaseLauncher):
    """Base class for any Abinit launchers that don't require a files file."""

    def _get_target_name(self, path):
        # path is the source file to point to. return the link destination
        filename = os.path.basename(path)
        if filename.endswith("HAYDR_SAVE"):
            end = "HAYDR_SAVE"
        else:
            end = filename.split("_")[-1]
        # loop over all possible endings (instead of just if) to take into
        # account _1WFX files where X is a number.
        for ending in DATA_FILE_ENDINGS:
            if ending in end:
                break
        else:
            # executed only if no ending are in the end of the filename.
            raise NotImplementedError(f"file ending {end} not supported.")
        if ending == "EVK":
            # splitoff EVK files because they have a different format
            # the idir is stored inside the filename
            # name_X_EVK(.nc) for X in (1, 2, 3)
            newname = self._add_prefixes_to_target(filename)
        else:
            newname = self._add_prefixes_to_target(end)
        return newname

    @abc.abstractmethod
    def _add_prefixes_to_target(self, *args, **kwargs):
        pass

    def _get_input_file_path(self):
        # since abinit v9, input file paths have the .abi extension
        return os.path.join(self.workdir, self.jobname + ".abi")


class BaseAbinitLauncher(BaseAbinitLauncherNoFilesFile, abc.ABC):
    """Base class for Abinit launchers that requires a files file."""

    _files_file_handler_class = None

    def __init__(self, *args, **kwargs):
        """Base Launcher class init method.

        Created the directories and various files needed for the calculation.

        Parameters
        ----------
        kwargs : other kwargs goes to the PBS file.
        """
        super().__init__(*args, **kwargs)
        if self._files_file_handler_class is None:
            raise DevError("Need to set FilesFile handler class.")
        lvl = self._loglevel
        self._files_file = self._files_file_handler_class(loglevel=lvl)

    @property
    def files_file(self):
        """Return the files file object."""
        return self._files_file

    @property
    def output_file_path(self):
        """Return the output file path."""
        return self.files_file.output_file_path

    @output_file_path.setter
    def output_file_path(self, output_file_path):
        self.files_file.output_file_path = output_file_path

    async def set_workdir(self, workdir):
        """Set the calculation's workdir."""
        # override this method because abinit pbs files need files file
        # call base class setter:
        # https://stackoverflow.com/questions/39133072/override-abstract-setter-of-property-in-python-3
        await super().set_workdir(workdir)
        await self.files_file.set_path(
                os.path.join(self.rundir.path, self.jobname + ".files"))
        self.output_file_path = os.path.join(self.workdir,
                                             self.jobname + ".abo")
        self.files_file.input_file_path = self.input_file.path
        self.pbs_file.input_file_path = self.files_file.path
        # for meta data file: reset script file path as it is the files file
        self.meta_data_file.script_input_file_path = self.files_file.path

    async def write(self, *args, **kwargs):
        """Write the files for the calculation."""
        await super().write(
                *args,
                bypass_validation=kwargs.pop("bypass_validation", False),
                bypass_pbs_validation=kwargs.pop(
                    "bypass_pbs_validation", False),
                **kwargs)
        await self.files_file.write(**kwargs)
