from ..bases import BaseLinker


class AbinitOpticfromAbinitLinker(BaseLinker):
    """Linker class to link an abinit calculation to an optic calculation."""

    _calctype = "abinit_optic"
    _link_calctype = "abinit"
    _loggername = "AbinitOpticfromAbinitLinker"

    async def _link_files_from_calculation(self, *args, **kwargs):
        # need to link 1WF* files from previous abinit calculation in case
        # it is a respfn calc. otherwise link the WFK only
        async with self.calculation_to_link.input_file as inf:
            if "rfelfd" in inf.input_variables:
                await self._link_1wf_files_from_calculation()
                return
            elif "wfk_task" in inf.input_variables:
                if inf.input_variables["wfk_task"] == "wfk_ddk":
                    await self._link_evk_files_from_calculation()
                else:
                    await self._link_wfk_file_from_calculation()
                return
            else:
                await self._link_wfk_file_from_calculation()

    async def _link_1wf_files_from_calculation(self):
        # link 1wf files. these should be in the output data directory
        # there should be 3 of them but the file suffix depends on the number
        # of atoms in the system. thus just list them all and link what
        # we find
        # Actually, all the linking is done in the Launcher from the
        # corresponding input variables. so we just need to set the
        # input variables accordingly
        async with self.calculation_to_link.output_data_directory as out:
            wf1_files = []
            async for filehandler in out:
                path = filehandler.path
                # there is also a _1WF_EIG file that we don't want
                if "_1WF" in path and "_EIG" not in path:
                    wf1_files.append(filehandler.path)
        if not wf1_files:
            # nothing was found!
            raise FileNotFoundError(
                   "Could't find any 1WF* files in "
                   f"'{self.calculation_to_link.path}'.")
        # sort files by last number (in case they were not sorted)
        # and set input variables accordingly
        if len(wf1_files) != 3:
            # wtf? there should be 1 file per dimension
            # are we in another universe now?!?
            raise LookupError(wf1_files)
        for ifile, filepath in enumerate(sorted(wf1_files)):
            await self.add_input_file(filepath, ddk_idx=ifile + 1)

    async def _link_evk_files_from_calculation(self):
        # link EVK files. these should be in the output data directory
        # there should be 3 of them but the file suffix depend on the rfdir
        # thus just list them all and link what we find
        # Actually, all the linking is done in the Launcher from the
        # corresponding input variables. so we just need to set the
        # input variables accordingly
        async with self.calculation_to_link.output_data_directory as out:
            evk_files = []
            async for filehandler in out:
                path = filehandler.path
                if "_EVK" in path:
                    evk_files.append(filehandler.path)
        if not evk_files:
            # nothing was found!
            raise FileNotFoundError(
                   "Could't find any EVK files in "
                   f"'{self.calculation_to_link.path}'.")
        # sort files by last number (in case they were not sorted)
        # and set input variables accordingly
        if len(evk_files) != 3:
            # wtf? there should be 1 file per dimension
            # are we in another universe now?!?
            raise LookupError(evk_files)
        for ifile, filepath in enumerate(sorted(evk_files)):
            await self.add_input_file(filepath, ddk_idx=ifile + 1)

    async def _link_wfk_file_from_calculation(self):
        # samething as linking 1wf files but for WFK file
        async with self.calculation_to_link.output_data_directory as out:
            async for filehandler in out:
                if "_WFK" in filehandler.path:
                    await self.add_input_file(filehandler.path)
                    return
        # nothing was found!
        raise FileNotFoundError(
                "Could't find any WFK files in: "
                f"'{self.calculation_to_link.path}'.")
