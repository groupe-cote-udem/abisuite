import os

from .bases import BaseQELinker


class QEMatdynfromQEQ2RLinker(BaseQELinker):
    """Linker class to link a qe_q2r calculation to a qe_matdyn calc."""

    _calctype = "qe_matdyn"
    _link_calctype = "qe_q2r"
    _loggername = "QEMatdynfromQEQ2RLinker"

    async def _link_files_from_calculation(self, metadata):
        outdir = metadata.output_data_dir
        await self.launcher.add_input_file(os.path.join(outdir, "*.fc"))
