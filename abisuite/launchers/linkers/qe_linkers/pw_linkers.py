import os

import aiofiles.os

from .bases import BaseQELinker


class QEPWfromQEPWLinker(BaseQELinker):
    """Linker class to link a pw.x calculation to another pw.x calculation."""

    _calctype = "qe_pw"
    _link_calctype = "qe_pw"
    _loggername = "QEPWfromQEPWLinker"

    async def _link_files_from_calculation(self, metadata):
        """Link a previous pw calculation to this launcher.

        The relevant files will be linked.
        """
        """Only need to link xml data file, charge density and pseudos."""
        outdir = metadata.output_data_dir
        # find the name of the 'save' directory
        savedirpath = self._search_for_save_dir(outdir)
        tolink = []
        for filename in await aiofiles.os.listdir(savedirpath):
            if filename.startswith("wfc"):
                continue
            tolink.append(os.path.join(savedirpath, filename))
        await self.launcher.add_input_file(
                tolink, keep_original_filename=True,
                copy_file_with_suffix=["data-file-schema.xml"],
                subdir=self.launcher.jobname + ".save",
                )
        # # TODO: probably a better way to do this
        # # need to link everything except for the 'wfc*.dat' files.
        # # these files are located in the ".save" directory in output_data_dir
        # # copy xml files though to prevent overwriting
        # # (=> done in add_input_file method)
        # outdir = metadata.output_data_dir
        # # to achieve this, we need to create a temporary directory which will
        # # contain the only links we need
        # # we do this in order to keep the input in a .save subdir
        # # NOTE: create temp to launcher attribute as we want the tempdir to b
        # # destroyed only when launcher is destroyed.
        # self.launcher._temp = tempfile.TemporaryDirectory()
        # tempdir = self.launcher._temp.name
        # # find the name of the 'save' directory
        # savedirpath = self._search_for_save_dir(outdir)
        # savename = os.path.basename(savedirpath)
        # # make the same dir in the temp dir
        # savepath = os.path.join(tempdir, savename)
        # await mkdir(savepath)
        # # now symlink all non ".dat" files except for charge-density
        # for subfile in await aiofiles.os.listdir(savedirpath):
        #     if subfile.endswith(".dat") and subfile.startswith("wfc"):
        #         # don't symlink it
        #         continue
        #     symlinkfile = SymLinkFile(loglevel=self._loglevel)
        #     await symlinkfile.set_path(os.path.join(savepath, subfile))
        #     symlinkfile.source = os.path.join(outdir, savename, subfile)
        #     await symlinkfile.validate()
        #     await symlinkfile.write()
        # # add the temporary directory to the list of input files
        # # copy data-file-scheme.xml since it is modified by new pw run.
        # await self.launcher.add_input_file(
        #         savepath, copy_file_with_suffix=["data-file-schema.xml"],
        #         keep_original_filename=True)
