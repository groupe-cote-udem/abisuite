from .bases import BasePostQEPWLinker


class QEPHfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_ph calc."""

    _calctype = "qe_ph"
    _link_calctype = "qe_pw"
    _loggername = "QEPHfromQEPWLinker"
