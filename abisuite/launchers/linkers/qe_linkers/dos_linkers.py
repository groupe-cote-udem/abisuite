import os

import aiofiles.os

from .bases import BasePostQEPWLinker


class QEDOSfromQEPWLinker(BasePostQEPWLinker):
    """Linker class to link a qe_pw calculation to a qe_dos calculation."""

    _calctype = "qe_dos"
    _link_calctype = "qe_pw"
    _loggername = "QEDOSfromQEPWLinker"

    async def _link_files_from_calculation(self, metadata):
        """Only need to link xml data file, charge density and pseudos."""
        outdir = metadata.output_data_dir
        # find the name of the 'save' directory
        savedirpath = self._search_for_save_dir(outdir)
        tolink = []
        for filename in await aiofiles.os.listdir(savedirpath):
            if filename.startswith("wfc"):
                continue
            tolink.append(os.path.join(savedirpath, filename))
        await self.launcher.add_input_file(
                tolink, keep_original_filename=True,
                subdir=self.launcher.jobname + ".save",
                )
