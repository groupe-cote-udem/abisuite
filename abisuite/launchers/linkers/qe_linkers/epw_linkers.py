import os

import aiofiles
import aiofiles.os

import numpy as np

from .bases import BasePostQEPHLinker
from ....handlers import (
        QEMatdynInputFile, QEPWInputFile, QEQ2RInputFile,
        )
from ....linux_tools import mkdir


class BaseQEEPWLinker(BasePostQEPHLinker):
    """Base class for all qe_epw linkers."""

    _calctype = "qe_epw"

    def _generate_fine_k_or_q_path(self, coords, pts_between):
        qpts_to_write = []
        for i, (qpt, nqpt) in enumerate(zip(coords[:-1], pts_between[:-1])):
            start = qpt
            end = coords[i + 1]
            q0s = np.linspace(start[0], end[0], nqpt)
            q1s = np.linspace(start[1], end[1], nqpt)
            q2s = np.linspace(start[2], end[2], nqpt)
            for q0, q1, q2 in zip(q0s, q1s, q2s):
                qpts_to_write.append([q0, q1, q2])
        return qpts_to_write

    async def _write_and_link_fine_path(self, varname, pts_to_write):
        """Writes a file for 'fil{k,q}f.txt' and links it to the launcher.

        The file is created in the output directory of the linked calculation.
        """
        weight = 1 / len(pts_to_write)
        dirname = self.calculation_to_link.output_data_directory.path
        if not await aiofiles.os.path.isdir(dirname):
            await mkdir(dirname)
        temp = os.path.join(dirname, varname + ".txt")
        # if file exists, overwrite
        if await aiofiles.os.path.exists(temp):
            await aiofiles.os.remove(temp)
        self._logger.debug(
                f"Linking a temporary file for fine dispersion path "
                f"'{temp}'.")
        async with aiofiles.open(temp, "w") as f:
            # write header
            await f.write(f"{len(pts_to_write)} crystal\n")
            for qpt in pts_to_write:
                await f.write(f"{qpt[0]} {qpt[1]} {qpt[2]} {weight}\n")
        await self.launcher.add_input_file(
            temp, copy=True, keep_original_filename=True)
        # add input variable to input vars (relative path to run dir!)
        self.launcher.input_variables[varname] = os.path.relpath(
                os.path.join(
                    self.launcher.input_data_dir.path, varname + ".txt"),
                self.launcher.calculation_directory.run_directory.path
                )


class QEEPWfromQEEPWLinker(BaseQEEPWLinker):
    """Linker class to link a qe_epw calc to another qe_epw calculation."""

    _link_calctype = "qe_epw"
    _loggername = "QEEPWfromQEEPWLinker"

    async def _link_files_from_calculation(self, metadata):
        # link the '.epb' files if needed
        if self.launcher.input_variables.get("epbread", False):
            await self._link_epb_files()
        # link the '.fmt' files if needed
        if self.launcher.input_variables.get("epwread", False):
            await self._link_coarse_matrix_elements()
        await self._link_kmaps()
        # check if we need to link wannier90 files
        if self.launcher.input_variables.get(
                "wannierize", False).value is False:
            await self._link_wannier90_files()
        # link filqf and filkf if necessary
        for fil in ("filqf", "filkf"):
            if fil not in self.launcher.input_variables:
                continue
            filpath = self.launcher.input_variables[fil]
            # only link file if not linked already since it might
            # happend that it is already done when
            # linking another child calc
            linked = (os.path.basename(
                        filpath.value) in self.launcher.input_data_dir)
            if not linked:
                await self.add_input_file(
                        filpath.value, keep_original_filename=True)
        # self._link_epmatwp_file()

    # def _link_epmatwp_file(self):
    #     # links the PREFIX.epmatwp file if needed
    #     if self.launcher.input_variables.get("etf_mem", 1).value != 0:
    #         return
    #     # need to link it
    #     outdir = self.calculation_to_link.output_data_directory.path
    #     self.add_input_file(os.path.join(outdir, "*.epmatwp"))

    async def _link_wannier90_files(self):
        run = self.calculation_to_link.run_directory.path
        for path in ("*.mmn", "*.nnkp", "*.ukk", ):
            try:
                await self.add_input_file(os.path.join(run, path))
            except FileExistsError:
                pass
        # link crystal.fmt and keep the name
        try:
            await self.add_input_file(
                os.path.join(run, "crystal.fmt"), keep_original_filename=True)
        except FileExistsError:
            pass

    async def _link_kmaps(self):
        # link them if they exist.
        run = self.calculation_to_link.run_directory.path
        for end in ("*.kmap", "*.kgmap"):
            try:
                await self.add_input_file(os.path.join(run, end))
            except FileExistsError:
                pass

    async def _link_coarse_matrix_elements(self):
        # check if calculation to link actually wrote the matrix elements on
        # a coarse mesh
        async with self.calculation_to_link.input_file as inf:
            epwwrite = inf.input_variables.get("epwwrite", False)
            if not epwwrite:
                raise ValueError(
                        "Calculation to link "
                        f"('{self.calculation_to_link.path}') "
                        "didn't write the electron-phonon "
                        "matrix elements on the coarse grid and they are "
                        "required for the new calculation (use 'epwwrite=True'"
                        ").")
        # need to link the 'epwdata.fmt' files in run directory
        for path in ("epwdata.fmt", ):
            try:
                await self.add_input_file(
                        os.path.join(
                            self.calculation_to_link.run_directory.path,
                            path),
                        keep_original_filename=True)
            except FileExistsError:
                pass
        # need to link velocity matrix elements. The file to link depends of
        # the input variables
        vme = self.input_variables.get("vme", False)
        try:
            if vme == "dipole":
                await self.add_input_file(
                    os.path.join(
                        self.calculation_to_link.run_directory.path,
                        "dmedata.fmt"),
                    keep_original_filename=True)
            elif vme == "wannier":
                await self.add_input_file(
                        os.path.join(
                            self.calculation_to_link.run_directory.path,
                            "vmedata.fmt"),
                        keep_original_filename=True)
            else:
                raise ValueError(vme)
        except FileExistsError:
            pass
        # also link the 'XX.epmatwpX' files in output data dir
        calc = self.calculation_to_link
        await self.add_input_file(
                os.path.join(
                    calc.run_directory.output_data_directory.path,
                    "*.epmatwp*"))

    async def _link_epb_files(self):
        # check if calculation to link wrote the epb files
        async with self.calculation_to_link.input_file as inf:
            epbwrite = inf.input_variables.get("epbwrite", False)
            if not epbwrite:
                raise ValueError(
                        "Calculation to link didn't write '.epb' files "
                        "and they are required for new calc.")
            # check if the '.epb' files were actually written
        calc = self.calculation_to_link
        async with calc.run_directory.output_data_directory as out:
            async for item in out:
                path = item.path
                if ".epb" in path:
                    # at least one has been written conitinue
                    break
            else:
                raise FileNotFoundError(
                        "No '.epb' files were written.")
        # link the 'epb' files.
        await self.add_input_file(
                os.path.join(
                    calc.run_directory.output_data_directory.path,
                    "*.epb*"))


class QEEPWfromQEMatdynLinker(BaseQEEPWLinker):
    """Linker class to link a qe_matdyn calculation to a qe_epw calc."""

    _link_calctype = "qe_matdyn"
    _loggername = "QEEPWfromQEMatdynLinker"

    async def _link_files_from_calculation(self, metadata):
        # if we require band_plot => link path in filqf.txt file
        if not self.launcher.input_variables.get("band_plot", False):
            # nothing to do
            self._logger.warning(
                    "Tried to link a qe_matdyn calc but 'band_plot' is set to"
                    " False => nothing to do...")
            return
        # we want to link a matdyn calculaton to compare (eventually) the
        # DFPT phonon dispersion vs the interpolated one by epw.
        # we don't actually link anything from the QEmatdyn calculaton in
        # itself but we rather create a 'filqf' file containing the same
        # qpoints as the matdyn calculation.
        async with await QEMatdynInputFile.from_calculation(
                metadata.calc_workdir) as mat_in:
            qpts = [[round(x, 10) for x in qpt]
                    for qpt in mat_in.input_variables[
                        "q_points"].value["qpts"]]
            nqpts = mat_in.input_variables["q_points"].value["nqpts"]
            if not mat_in.input_variables.get("q_in_cryst_coord", False).value:
                raise ValueError(
                        "Cannot compute qpoint path from "
                        f"{mat_in.path} because coordinates are "
                        "not in crystal coordinates.")
        qpts_to_write = self._generate_fine_k_or_q_path(qpts, nqpts)
        await self._write_and_link_fine_path("filqf", qpts_to_write)


class QEEPWfromQEPHLinker(BaseQEEPWLinker):
    """Linker class to link a qe_ph calculation to a qe_epw calc."""

    _link_calctype = "qe_ph"
    _loggername = "QEEPWfromQEPHLinker"

    async def _link_files_from_calculation(self, metadata):
        # need to link the 'dyn' files (done in mother class)
        await super()._link_files_from_calculation(metadata)
        # link the dvscf1 files
        await self.launcher.add_input_file(
                os.path.join(
                    metadata.output_data_dir, "_ph0", "*dvscf1",
                    )
                )
        await self.launcher.add_input_file(
                os.path.join(
                    metadata.output_data_dir, "_ph0", "*.q_*", "*.dvscf1"
                    )
                )
        # need also to link the _ph0/*.phsave directory
        await self.launcher.add_input_file(
                os.path.join(
                    metadata.output_data_dir, "_ph0", "*.phsave"
                    )
                )
        # set qgrid in inputs if inputs are already set
        inputs = self.launcher.input_variables
        # reset input will set qgrid
        await self.launcher.set_input_variables(inputs.todict())


class QEEPWfromQEPWLinker(BaseQEEPWLinker):
    """Linker for an EPW calculation that links a PW calculation."""

    _link_calctype = "qe_pw"
    _loggername = "QEEPWfromQEPWLinker"

    async def _link_files_from_calculation(self, metadata):
        # check that the calculation was nscf
        async with await QEPWInputFile.from_meta_data_file(
                metadata, loglevel=self._loglevel) as infile:
            if infile.input_variables["calculation"] == "nscf":
                await self._link_qenscf_calc_from_meta(metadata)
            elif infile.input_variables["calculation"] == "bands":
                await self._link_qebands_calc_from_meta(metadata)
            else:
                raise TypeError(
                        "Need to link a nscf or band structure calc.")

    async def _link_qenscf_calc_from_meta(self, meta):
        await self.launcher.add_input_file(
                os.path.join(meta.output_data_dir, "*.save"))

    async def _link_qebands_calc_from_meta(self, meta):
        # if we require band_plot => link path in filqf.txt file
        if not self.launcher.input_variables.get("band_plot", False):
            # don't need to do anything
            self._logger.warning(
                    "Tried to link a qe_pw band calc but 'band_plot' is set "
                    "to False => nothing to do...")
            return
        # create a filkf.txt file with the band structure kpt path.
        async with await QEPWInputFile.from_meta_data_file(
                meta, loglevel=self._loglevel) as infile:
            kpts = infile.input_variables["k_points"].value
        if kpts["parameter"] != "crystal_b":
            raise ValueError(
                    "Cannot link qe band structure calc since kpts are not "
                    "defined in crystal coordinates.")
        nkps = kpts["weights"]
        coords = kpts["k_points"]
        # create a temporary file which is linked to the calculation
        kpts_to_write = self._generate_fine_k_or_q_path(coords, nkps)
        varname = "filkf"
        # unless we compute a nesting function
        if self.launcher.input_variables.get("nest_fn", False):
            varname = "filqf"
        await self._write_and_link_fine_path(varname, kpts_to_write)


class QEEPWfromQEQ2RLinker(BaseQEEPWLinker):
    """Linker class for a qe_epw calc thas is linked to a qe_q2r calc."""

    _link_calctype = "qe_q2r"
    _loggername = "QEEPWfromQEQ2RLinker"

    async def _link_files_from_calculation(self, meta):
        # link the force constant file and set 'lifc' to true
        # the fc path is written in input file
        async with await QEQ2RInputFile.from_calculation(
                meta.calc_workdir, loglevel=self._loglevel) as infile:
            if "flfrc" not in infile.input_variables:
                raise FileNotFoundError(
                        "Cannot link a 'q2r' calculation if no force constant "
                        "file to link.")
            fcfil = infile.input_variables["flfrc"].value
        await self.launcher.add_input_file(fcfil)
        self.launcher.input_variables["lifc"] = True
