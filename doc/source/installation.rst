Installation
============

To install the abisuite package just issue the following::

 $ git clone git@gitlab.com:groupe-cote-udem/abisuite.git
 $ cd abisuite

Execute setup script and install requirements::
  
 $ pip install -r requirements.txt
 $ pip install .

For a development installation (to modify the code without having to execute
the setup script each time). Use the `-e` flag for the `pip install` command
and install the (recommanded) optional packages::
 
 $ pip install -r requirements.txt
 $ pip install -r docs-requirements.txt
 $ pip install -r test-requirements.txt
 $ pip install -e .
