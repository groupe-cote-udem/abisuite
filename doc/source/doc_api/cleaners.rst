.. _cleaners_api:

Cleaners Module
===============

.. contents::
    :local:

This module is not meant to be used direclty. It contains Cleaners classes
that are used to clean calculation directories to make room on disk.
These classes are used notably when invoking the ``abisuite clean`` command.

``Bases``
---------

.. automodule:: abisuite.cleaners.bases
    :members:
    :show-inheritance:

:mod:`Abinit Cleaners <~abisuite.cleaners.abinit_cleaners>`
-----------------------------------------------------------

.. automodule:: abisuite.cleaners.abinit_cleaners.abinit_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.abinit_cleaners.anaddb_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.abinit_cleaners.mrgddb_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.abinit_cleaners.optic_cleaner
    :members:
    :show-inheritance:


:mod:`Quantum Espresso Cleaners <~abisuite.cleaners.qe_cleaners>`
-----------------------------------------------------------------

.. automodule:: abisuite.cleaners.qe_cleaners.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.qe_cleaners.epw_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.qe_cleaners.matdyn_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.qe_cleaners.ph_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.qe_cleaners.pw_cleaner
    :members:
    :show-inheritance:

.. automodule:: abisuite.cleaners.qe_cleaners.q2r_cleaner
    :members:
    :show-inheritance:
