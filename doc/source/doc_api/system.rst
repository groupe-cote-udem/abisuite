.. _system_api:


System Module
=============

.. contents::
    :local:

This module contains utilities that help to solve any questions related
to queuing systems.

.. automodule:: abisuite.system.job
    :members:
    :show-inheritance:

.. automodule:: abisuite.system.queue
    :members:
    :show-inheritance:

.. automodule:: abisuite.system.queue_parsers
    :members:
    :show-inheritance:
