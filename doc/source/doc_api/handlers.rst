.. _handlers_api:

###############
Handlers Module
###############

.. contents::
    :local:

The handlers module contains classes that handles files and directories.
There are common base classes for file handlers and directory handlers
but then, each file type and directory type have their own sub classes.

Bases Classes
=============

.. automodule:: abisuite.handlers.bases
    :members:
    :show-inheritance:

Directory Handlers
==================

The tree of a prototypical calculation directory in abisuite is made up
of various sub directories. The main one is the
:class:`~abisuite.handlers.directory_handlers.calculation_directory.CalculationDirectory`.
It contains an instance of various files: a :class:`~abisuite.handlers.file_handlers.meta_data_file.MetaDataFile`,
an ``Input File`` (the class depends of the ``calctype``), a :class:`~abisuite.handlers.file_handlers.stderr_file.StderrFile`
(if the file exists), a ``Log File`` (the class depends on the ``calctype`` and it will be present
if the file exists on disk) and all the other files which is contained in the folder and sub folders.

If also contains various subdirectories: an
:class:`~abisuite.handlers.directory_handlers.input_data_directory.InputDataDirectory` and
a :class:`~abisuite.handlers.directory_handlers.run_directory.RunDirectory` which
itself contains an instance of an
:class:`~abisuite.handlers.directory_handlers.output_data_directory.OutputDataDirectory`.


.. automodule:: abisuite.handlers.directory_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.calculation_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.generic_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.input_data_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.output_data_directory
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.directory_handlers.run_directory
    :members:
    :show-inheritance:

File Handlers
=============

.. contents::
    :local:

The file handlers manages files directly. They are not built the same but they share common
classes (see :mod:`~abisuite.handlers.file_handlers.bases`). All file handlers have a
:ref:`File Structure <file_structures_api>` which states which properties a given file possess. They all have
a corresponding :ref:`File Parser <file_parsers_api>` which parses the file. Writable files possess an 
:ref:`File Writer <file_writers_api>` write them. Finally, some approvable files (like
input files) also possess :ref:`File Approvers <file_approvers_api>` that can validate the content
of a file is good before writing it.

:mod:`Bases <abisuite.handlers.file_handlers.bases>`
----------------------------------------------------

The file handlers main base classes are stored in this module.

.. automodule:: abisuite.handlers.file_handlers.bases
    :members:
    :show-inheritance:

:mod:`Generic File Handlers <abisuite.handlers.file_handlers.generic_files>`
----------------------------------------------------------------------------

Generic file handlers. They can be used for any file but won't be able to parse
them nor write them. They should only use for direct file manipulation like
copying, moving or deleting when the user is not concerned in the actual file.

.. automodule:: abisuite.handlers.file_handlers.generic_files
    :members:
    :show-inheritance:

:mod:`Meta Data File Handler <abisuite.handlers.file_handlers.meta_data_file>`
------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.meta_data_file
    :members:
    :show-inheritance:

:mod:`PBS File <abisuite.handlers.file_handlers.pbs_file>`
----------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.pbs_file
    :members:
    :show-inheritance:

:mod:`Pseudo Potential File Handlers <abisuite.handlers.file_handlers.pseudo_file>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.pseudo_file
    :members:
    :show-inheritance:

:mod:`Stderr File Handler <abisuite.handlers.file_handlers.stderr_file>`
------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.stderr_file
    :members:
    :show-inheritance:

:mod:`Sym Link File Handler <abisuite.handlers.file_handlers.symlink_file>`
---------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.symlink_file
    :members:
    :show-inheritance:

:mod:`Abinit File Handlers <abisuite.handlers.file_handlers.abinit_handlers>`
-----------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dmft_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dmft_projectors_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_fatband_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_gsr_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_output_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_procar_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.abinit_projected_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_files_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.anaddb_phfrq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.cut3d_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.mrgddb_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.mrgddb_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_files_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_lincomp_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.abinit_handlers.optic_log_file
    :members:
    :show-inheritance:

:mod:`Quantum Espresso File Handlers <abisuite.handlers.file_handlers.qe_handlers>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dos_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dynmat_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.dynmat_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epsilon_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epsilon_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_a2f_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_band_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_conductivity_tensor_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_phbandfreq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_phonon_self_energy_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_resistivity_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.epw_spec_fun_phon_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.fs_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.fs_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ld1_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ld1_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_dos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_eig_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_freq_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.matdyn_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_dyn0_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.ph_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pp_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pp_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.projwfc_pdos_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw2wannier90_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw2wannier90_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.pw_log_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.q2r_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.qe_handlers.q2r_log_file
    :members:
    :show-inheritance:

:mod:`Wannier90 File Handlers <abisuite.handlers.file_handlers.wannier90_handlers>`
-----------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_banddat_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_bandkpt_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_input_file
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_handlers.wannier90_handlers.wannier90_output_file
    :members:
    :show-inheritance:


File Structures
===============
.. _file_structures_api:

The file structures dictates what is the content of a file. Each file handler
have one. The way they work is that, each time we try to access an attribute
of a file handler (or a writer, approver or parser), we will check in the structure
file to see if the property is defined. File structures handles this.

``Structure Bases and General Structures``
------------------------------------------

.. automodule:: abisuite.handlers.file_structures.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.generic_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.meta_data_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.mpi_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.pbs_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.pseudo_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.slurm_files_to_delete_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.stderr_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.symlink_structure
    :members:
    :show-inheritance:

:mod:`Abinit Structures <~abisuite.handlers.file_structures.abinit_structures>`
-------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_dmft_eig_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_dmft_projectors_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_dos_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_eig_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_fatband_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_gsr_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_output_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_procar_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.abinit_projected_dos_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.anaddb_files_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.anaddb_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.anaddb_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.anaddb_phfrq_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.cut3d_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.mrgddb_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.mrgddb_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.optic_files_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.optic_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.optic_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.abinit_structures.optic_lincomp_structure
    :members:
    :show-inheritance:

:mod:`Quantum Espresso File Structures <~abisuite.handlers.file_structures.qe_structures>`
------------------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_structures.qe_structures.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.dos_dos_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.dos_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.dos_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.dynmat_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.dynmat_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epsilon_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epsilon_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_a2f_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_band_eig_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_conductivity_tensor_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_phbandfreq_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_phonon_self_energy_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_resistivity_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.epw_spec_fun_phon_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.fs_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.fs_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.ld1_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.ld1_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.matdyn_dos_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.matdyn_eig_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.matdyn_freq_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.matdyn_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.matdyn_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.ph_dyn0_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.ph_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.ph_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pp_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pp_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.projwfc_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.projwfc_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.projwfc_pdos_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pw2wannier90_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pw2wannier90_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pw_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.pw_log_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.q2r_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.qe_structures.q2r_log_structure
    :members:
    :show-inheritance:

:mod:`Wannier90 Structures <~abisuite.handlers.file_structures.wannier90_structures>`
-------------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_structures.wannier90_structures.wannier90_banddat_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.wannier90_structures.wannier90_bandkpt_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.wannier90_structures.wannier90_input_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_structures.wannier90_structures.wannier90_output_structure
    :members:
    :show-inheritance:


File Writers
============
.. _file_writers_api:

TODO: write me!


File Parsers
============

.. _file_parsers_api:

TODO: write me!

File Approvers
==============
.. _file_approvers_api:

This module contains the `Approvers` which are classes that cross checks
input variables between them to make sure everything is consistent
prior launching or submitting a calculation using a `Launcher object<launchers_api>`.


:mod:`Bases <~abisuite.handlers.file_approvers.bases>`
------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.bases
    :members:
    :show-inheritance:

:mod:`Approvers Exceptions<~abisuite.handlers.file_approvers.exceptions>`
-------------------------------------------------------------------------

These are exceptions classes used by the `Approvers` module.

.. automodule:: abisuite.handlers.file_approvers.exceptions
    :members:
    :show-inheritance:

:mod:`Generic Approvers <~abisuite.handlers.file_approvers.generic_approvers>`
------------------------------------------------------------------------------

These are dummy approvers that don't do anything. Contrary to base
classes, these can be instanciated directly.

.. automodule:: abisuite.handlers.file_approvers.generic_approvers
    :members:
    :show-inheritance:

:mod:`Meta Data File Approver <~abisuite.handlers.file_approvers.meta_data_approver>`
-------------------------------------------------------------------------------------

Specific approver for a meta data file.

.. automodule:: abisuite.handlers.file_approvers.meta_data_approver
    :members:
    :show-inheritance:

:mod:`Mpi Approver<~abisuite.handlers.file_approvers.mpi_approver>`
-------------------------------------------------------------------

Approver classes for mpi commands.

.. automodule:: abisuite.handlers.file_approvers.mpi_approver
    :members:
    :show-inheritance:

:mod:`PBS File Approver<~abisuite.handlers.file_approvers.pbs_approver>`
------------------------------------------------------------------------

Specific approver class for a batch PBS file.

.. automodule:: abisuite.handlers.file_approvers.pbs_approver
    :members:
    :show-inheritance:

:mod:`SymLink Approver<~abisuite.handlers.file_approvers.symlink_approver>`
---------------------------------------------------------------------------

Specific approver class for a symlink file.

.. automodule:: abisuite.handlers.file_approvers.symlink_approver
    :members:
    :show-inheritance:

:mod:`Abinit Approvers<~abisuite.handlers.file_approvers.abinit_appovers>`
--------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.abinit_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.anaddb_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.mrgddb_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.abinit_approvers.optic_input_approver
    :members:
    :show-inheritance:


:mod:`Quantum Espresso Approvers<~abisuite.handlers.file_approvers.qe_approvers>`
---------------------------------------------------------------------------------

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.dos_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.dynmat_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.epsilon_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.epw_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.fs_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.ld1_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.matdyn_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.ph_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pp_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.projwfc_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pw2wannier90_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.pw_input_approver
    :members:
    :show-inheritance:

.. automodule:: abisuite.handlers.file_approvers.qe_approvers.q2r_input_approver
    :members:
    :show-inheritance:
