Post Processors Module
======================

.. contents::
    :local:

This module contains utilities to post process data produced by the ab initio
softwares.

.. automodule:: abisuite.post_processors.bases
    :members:
    :show-inheritance:

Eliashberg a2F function
-----------------------

.. automodule:: abisuite.post_processors.a2f
    :members:
    :show-inheritance:

Band Structure
--------------
.. automodule:: abisuite.post_processors.band_structure
    :members:
    :show-inheritance:

.. automodule:: abisuite.post_processors.fat_band_structure
    :members:
    :show-inheritance:

DOS
---
.. automodule:: abisuite.post_processors.dos
    :members:
    :show-inheritance:

Phonon Dispersion
-----------------
.. automodule:: abisuite.post_processors.phonon_dispersion
    :members:
    :show-inheritance:

Phonon Spectral Function
------------------------
.. automodule:: abisuite.post_processors.phonon_spectral_function
    :members:
    :show-inheritance:

Conductivity/Resistivity Tensor
-------------------------------
.. automodule:: abisuite.post_processors.conductivity_tensor
    :members:
    :show-inheritance:

.. automodule:: abisuite.post_processors.resistivity
    :members:
    :show-inheritance:

Dielectric Tensor
-----------------
.. automodule:: abisuite.post_processors.dielectric_tensor
    :members:
    :show-inheritance:

Miscellaneous
-------------
.. automodule:: abisuite.post_processors.fits
    :members:
    :show-inheritance:
