.. _scripts_api:


Scripts Module
==============

.. contents::
    :local:

This module defines the abisuite scripts. There are a few scripts that are quite usefull
for numerous tasks like:

  - checking the status of one or more calculations,
  - debugging a calculation,
  - relaunching calculations,
  - deleting, cleaning or moving calculations,
  - etc.

``Bases, Main Script and Common stuff``
---------------------------------------

# .. automodule:: abisuite.scripts.abisuite_script
#     :members:
#     :show-inheritance:

.. automodule:: abisuite.scripts.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.routines
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.utils
    :members:
    :show-inheritance:


``Abisuite Subscripts``
-----------------------

.. automodule:: abisuite.scripts.input_file_repair
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.clean
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.config
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.db
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.debug
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.grep
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.meta_data_file_repair
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.package
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.queue
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.relaunch
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.rm
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.status
    :members:
    :show-inheritance:

.. automodule:: abisuite.scripts.visualize_structure
    :members:
    :show-inheritance:
