.. _plotters_api:


Plotters Module
===============

.. contents::
    :local:

This module contains utilities to easily plot anything.


:mod:`Curves <abisuite.plotters.curves>`
----------------------------------------

This module contains the curves objects. They are used by the
:class:`Plot <abisuite.plotters.plot.Plot>` objects to generate
the curves on the plot using matplotlib.

.. automodule:: abisuite.plotters.curves
    :members:
    :show-inheritance:
    :special-members:

:mod:`Plot <abisuite.plotters.plot>`
------------------------------------

This contains the infamous :class:`Plot <abisuite.plotters.plot.Plot>`
object which can be used to generate plots in an straightforward way.

.. automodule:: abisuite.plotters.plot
    :members:
    :show-inheritance:
    :special-members:

:mod:`Multiplot <abisuite.plotters.multiplot>`
-----------------------------------------------

This module contains the :class:`MultiPlot <abisuite.plotters.multiplot.MultiPlot>`
which can be used to generate plots with subplots by combining
:class:`Plot <abisuite.plotters.plot.Plot>` objects.

.. automodule:: abisuite.plotters.multiplot
    :members:
    :show-inheritance:

:mod:`Plot Tools <abisuite.plotters.plot_tools>`
------------------------------------------------

This module defines many plot tools that are added to matplotlib windows.
These tools serve manyh purposes when a matplotlib figure is drawn.

.. automodule:: abisuite.plotters.plot_tools
    :members:
    :show-inheritance:
