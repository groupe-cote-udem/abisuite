.. _sequencers_api:

Sequencers Module
=================

.. contents::
    :local:

This module contains the sequencers objects which are used to easily produce
series of coherent calculations. Contrary to the :ref:`Workflows <workflows_api>`,
the ``Sequencers`` are smaller in terms of calculations. Their purpose is to
define a given *sequence* of calculations for a given purpose. There are many
of these and a ``Workflow`` object groups many of such sequences.

See the :ref:`Tutorials on sequencers <sequencers_tutorial>` for detailed
information on how to use them.

``Common Sequencers``
---------------------

These common sequencers are usually not meant to be used direclty. They are rather
base classes for sequencers that are specific to each softwares.

.. automodule:: abisuite.sequencers.band_structure_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.ecut_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.exceptions
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.individual_phonon_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.kgrid_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.nscf_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.phonon_dispersion_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.relaxation_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.scf_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.smearing_convergence_sequencers
    :members:
    :show-inheritance:

:mod:`Abinit Sequencers <~abisuite.sequencers.abinit_sequencers>`
-----------------------------------------------------------------

.. automodule:: abisuite.sequencers.abinit_sequencers.band_structure_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.ddk_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.dos_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.ecut_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.individual_phonon_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.kgrid_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.nscf_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.optic_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.phonon_dispersion_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.relaxation_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.scf_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.abinit_sequencers.smearing_convergence_sequencers
    :members:
    :show-inheritance:

:mod:`Quantum Espresso Sequencers <~abisuite.sequencers.qe_sequencers>`
-----------------------------------------------------------------------

.. automodule:: abisuite.sequencers.qe_sequencers.band_structure_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.ecut_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.epw_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.fermi_surface_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.individual_phonon_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.kgrid_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.phonon_dispersion_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.relaxation_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.scf_sequencer
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.smearing_convergence_sequencers
    :members:
    :show-inheritance:

.. automodule:: abisuite.sequencers.qe_sequencers.thermal_expansion_sequencer
    :members:
    :show-inheritance:
