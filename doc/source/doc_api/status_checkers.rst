.. _status_checkers_api:


Status Checkers Module
======================

.. contents::
    :local:


This module is not meant to be used directly. It's only purpose is to provide
tools to determine calculation's statuses. It is invoked when using the
``abisuite status`` command for instance.


``Base Classes and Common Utilities``
-------------------------------------

.. automodule:: abisuite.status_checkers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.exceptions
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.status_tree
    :members:
    :show-inheritance:

:mod:`Abinit Status Checkers <~abisuite.status_checkers.abinit_status_checkers>`
--------------------------------------------------------------------------------

.. automodule:: abisuite.status_checkers.abinit_status_checkers.anaddb_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.abinit_status_checkers.abinit_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.abinit_status_checkers.mrgddb_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.abinit_status_checkers.optic_status_checker
    :members:
    :show-inheritance:


:mod:`Quantum Espresso Status Checkers <~abisuite.status_checkers.qe_status_checkers>`
--------------------------------------------------------------------------------------

.. automodule:: abisuite.status_checkers.qe_status_checkers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.dos_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.dynmat_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.epsilon_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.epw_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.fs_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.ld1_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.matdyn_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.ph_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.pp_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.projwfc_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.pw2wannier90_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.pw_status_checker
    :members:
    :show-inheritance:

.. automodule:: abisuite.status_checkers.qe_status_checkers.q2r_status_checker
    :members:
    :show-inheritance:
