.. _launchers_api:

Launchers Module
================

.. contents::
    :local:

This module contains all the necessary Launcher classes to execute ab initio
tasks.

:mod:`Bases <~abisuite.launchers.launchers>`
--------------------------------------------

.. automodule:: abisuite.launchers.launchers.bases
    :members:
    :show-inheritance:


:mod:`Abinit Launchers <~abisuite.launchers.launchers.abinit_launchers>`
------------------------------------------------------------------------

.. automodule:: abisuite.launchers.launchers.abinit_launchers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.abinit_launchers.abinit_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.abinit_launchers.anaddb_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.abinit_launchers.cut3d_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.abinit_launchers.mrgddb_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.abinit_launchers.optic_launcher
    :members:
    :show-inheritance:

:mod:`Quantum Espresso Launchers<~abisuite.launchers.launchers.qe_launchers>`
-----------------------------------------------------------------------------

.. automodule:: abisuite.launchers.launchers.qe_launchers.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.dos_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.dynmat_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.epsilon_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.epw_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.fs_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.ld1_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.matdyn_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.ph_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.pp_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.projwfc_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.pw2wannier90_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.pw_launcher
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.launchers.qe_launchers.q2r_launcher
    :members:
    :show-inheritance:
