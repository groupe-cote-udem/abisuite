.. _variables_api:

Variables Module
================

.. contents::
    :local:

This module contains the databases of input variables for each codes.
These databases are not necessarily 100% complete as there are
very often new variables added or modified. It thus need to be
updated quite often. If you ever want to use a variable that is
not stored inside a given database, don't hesitate to send me
a pull request!

There a one DB for each codes that required input variables (e.g.:
there are none for mrgddb of abinit). Each DB is a list of dict.
Each dict is for one variable and contains some entries that describe it.
It has at least the name of the variable, the type (int, float, str, etc.), etc.
If one intialize a Launcher and sets the input variables, an
:class:`~abisuite.variables.bases.InputVariableDict` will be created. It is
a container of custom classes that holds the input variables information.
Already at this point there are some checks that occured wether the input
variables given exists and if wether or not they respect the properties defined
in the databases. It prevents some malicious errors when one tried to launch a calculation!

In the DB, there are 'class' entries for each variables in a DB. It specifies which
object will be used to represent the given input variable inside the
:class:`~abisuite.variables.bases.InputVariableDict`.

``Common``
----------

.. automodule:: abisuite.variables.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.exceptions
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.generic_variables
    :members:
    :show-inheritance:


:mod:`Abinit Input Variables <~abisuite.variables.abinit_variables>`
--------------------------------------------------------------------

.. automodule:: abisuite.variables.abinit_variables.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.abinit_variables.abinit_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.abinit_variables.anaddb_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.abinit_variables.optic_input_variables
    :members:
    :show-inheritance:


:mod:`Quantum Espresso Input Variables <~abisuite.variables.qe_variables>`
--------------------------------------------------------------------------

.. automodule:: abisuite.variables.qe_variables.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.dos_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.dynmat_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.epsilon_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.epw_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.fs_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.ld1_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.matdyn_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.ph_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.pp_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.projwfc_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.pw_input_variables
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.qe_variables.q2r_input_variables
    :members:
    :show-inheritance:


:mod:`Wannier90 Input Variables <~abisuite.variables.wannier90_variables>`
--------------------------------------------------------------------------

.. automodule:: abisuite.variables.wannier90_variables.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.variables.wannier90_variables.wannier90_input_variables
    :members:
    :show-inheritance:
