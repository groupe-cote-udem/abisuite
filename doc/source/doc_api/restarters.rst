.. _restarters_api:

Restarters Module
=================

.. contents::
    :local:

The Restarters are objects which can recover a calculation, wether or not
the calculation has failed or succeeded. These utilities are not
meant to be used directly but rather within a Launcher object.

:mod:`Bases <~abisuite.launchers.restarters>`
---------------------------------------------

.. automodule:: abisuite.launchers.restarters.bases
    :members:
    :show-inheritance:

:mod:`Quantum Espresso Restarters <~abisuite.restarters.qe_restarters>`
-----------------------------------------------------------------------

.. automodule:: abisuite.launchers.restarters.qe_restarters.bases
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.dos_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.dynmat_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.epsilon_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.epw_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.fs_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.ld1_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.matdyn_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.ph_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.pp_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.projwfc_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.pw_restarter
    :members:
    :show-inheritance:

.. automodule:: abisuite.launchers.restarters.qe_restarters.q2r_restarter
    :members:
    :show-inheritance:



:mod:`Abinit Restarters <~abisuite.launchers.restarters.abinit_restarters>`
---------------------------------------------------------------------------

.. automodule:: abisuite.launchers.restarters.abinit_restarters.abinit_restarter
    :members:
    :show-inheritance:
