.. _config_file:

Config File
===========

abisuite works using user-defined config options. This file is automatically created
in `~/.config/abisuite`. If any entry disappears or is added during developpement,
the new (or deleted) entry will be automatically (re)added to the config file with
a default value.
