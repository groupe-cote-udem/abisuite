.. _launchers_tutorial:

Launchers Tutorial
==================

.. contents::
    :local:

:ref:`Launchers <launchers_api>` are the building blocks of abisuite.
Each `Launcher` class has a many purposes: write, submit, execute,
approve (verify inputs) and store calculations in a database.
They also handle the different calculation parameters (like mpi commands,
module loadings, etc.) for that the calculation can be launched on an HPC.

Each Launcher
manages only one calculation at a time and each class is associated with a
single executable. For instance, for Quantum Espresso, the way this software
is built means that there are many Launcher classes since this software is
divided in many sub softwares. For abinit, there are one main launcher
for the abinit executable but there are other launchers (e.g.: the optic Launcher)
for the other scripts as well.

Most Launchers work basically the same, the main differences are in the way
they link to other calculations and the way they approve the inputs.
Actually, other objects are responsible for linking / recovering calculations
for more modularity (see :ref:`Restarters <restarters_api>` and :ref:`Linkers <linkers_api>`
for more details).

Abinit tbase1_1 example
-----------------------

Here is an example usage of the
:class:`AbinitLauncher <abisuite.launchers.launchers.abinit_launchers.abinit_launcher.AbinitLauncher>`
for doing the
`tbase1_1 abinit tutorial <https://docs.abinit.org/tutorial/base1/index.html>`__.
We start by importing and initiating the launcher.
It requires one argument only which is the *jobname* of the calculation::

  from abisuite import AbinitLauncher as Launcher

  launcher = Launcher("tbase1_1")

Then we decide where the calculation will be run::

  launcher.workdir = "work/tbase1_1"

For the *abinit* software, we need to specify the pseudo potential files
outside of the input variables. (Warning: this will probably be deprecated
soon as the new way pseudos are handled within abinit are now using the
input file which was not the case prior to abinit v9)::

  launcher.pseudos = ["path/to/pseudo_file/H.psp8"]

You have the choice to either set it to absolute full paths or to just put the
filename and configure a *default_pseudo_dir* inside the
:ref:`Config File <config_file>`. The pseudos will be then taken from this default
pseudo directory. Note that an excellent place to download pseudo potential
files is the `pseudo-dojo <http://www.pseudo-dojo.org/>`__ where the pseudo
are tested and different XC functionals are available along with the choice
of SR or FR pseudos::

  # other way to define pseudos using the 'default_pseudo_dir' in config file
  launcher.pseudos = ["H.psp8"]

Then, set the input variables by using a dictionary (note however that
the *pseudos* and *pp_dirpath* have not yet be implemented and thus are not
added here)::

  launcher.input_variables = {
          "acell": [10, 10, 10],
          "ntypat": 1,
          "znucl": 1,
          "natom": 2,
          "typat": [1, 1],
          "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
          "ecut": 10,
          "kptopt": 0,
          "nkpt": 1,
          "nstep": 10,
          "toldfe": 1.0e-6,
          "diemac": 2.0,
          }

Then, we define the calculation parameters. If you are on your personnal computer,
you might just want to define the `mpi_command` property and the `build` property::

  launcher.mpi_command = "mpirun -np 4"
  launcher.build = "abinit_default_build"  # optional

The `mpi_command` property do exactly what is says: it will use this mpi command
to run the abinit script. `build` however states the abinit build to use. This build
is specifically defined in the :ref:`Config File<config_file>`. One can define other
builds and reference them in the launcher. The launcher object will then set the
`command` attribute from the build to the correct path towards the abinit script.
Also, if you are on a HPC, it will add `modules` properties to either load/unload/use
for the build to work. But all these properties can also be fine tuned manually in the
launcher::

  # OPTIONAL
  launcher.command = "path/to/abinit_executable

If on an HPC, one needs to define the `queuing_system` in the Config file and specify
job attributes::

  launcher.ntasks = 4  # or launcher.nodes = 1, launcher.ppn = 4
  launcher.walltime = "1:00:00"
  launcher.memory_per_cpu = "1G"  # or launcher.memory = "4G" (if using nodes and ppn)
  launcher.modules_to_load = ["libxc/5.0.1"]  # example
  # ...
  launcher.queuing_system = "local"  # if you're on a HPC and want to run on front node

See all available properties in the API documentation for the :ref:`Launchers <launchers_api>`.
Once everything is set, one needs to write the files and one can then submit (or run)::

  launcher.write()
  launcher.run()

When writing the files, a bunch of other functions will be called to cross-check input
variables and pseudo potential files such that everything is consistent and no errors
have slipped in. That being said, it is not 100% foolproofed but it can help a lot
(e.g.: for checking out typos).

Abinit tbase1_2 example
-----------------------

The **base1_2** tutorial of Abinit makes a computation of the total energy in function
of the interatomic distance. It does this using the infamous *Multidataset mode* of
Abinit. Unfortunately, and in general, I personnally never use this feature. The reason
is mostly based on the fact that I think it is better to make individual calculations
for each dataset instead of doing a single calculation with multiple datasets.
For instance, if an error occur at some dataset(s), this will break abinit and make
it leave. Doing so, perhaps you're gonna have to restart the whole thing...
If you had split the work into many single calculations, then, if one errors out,
then only this one breaks but the others might be good. That way you only need to
debug a single calculation and don't have to restart everything. For this reason,
**abisuite does not support multi dataset mode!**

The easiest way to use Launchers in order to make multiple datasets
in different calculations is straightforward: just make a python
loop over the parameters you want to test. Just make sure to change to working
directory for each calculations. For the remainder of the tutorial,
I will assume you don't work on a HPC. Otherwise, you would need to add
calculation parameters (e.g.: walltime, memory, ntasks, etc.) for each
individual calculations. Here's the code::

  import numpy as np
  from abisuite import AbinitLauncher as Launcher

  xcarts_x = np.linspace(0.5, 1.0, 21)  # we have 21 dtsets total

  for xcart_x in xcarts_x:
      launcher = Launcher(f"tbase1_2_xcart_{xcart_x}")
      launcher.workdir = f"work/tbase1_2/xcart_{xcart_x}"
      # don't need to add 'ndtset', 'xcart+' and 'getwfk'
      # since each calculation only has 1 dtset
      launcher.input_variables = {
          "xcart": [[xcart_x, 0.0, 0.0], [-xcart_x, 0.0, 0.0]],
          "nband": 1,
          "acell": [10, 10, 10],
          "ntypat": 1,
          "znucl": [1],
          "natom": 2,
          "typat": [1, 1],
          "ecut": 10.0,
          "kptopt": 0,
          "nkpt": 1,
          "nstep": 10,
          "toldfe": 1e-6,
          "diemac": 2.0,
          }
      launcher.pseudos = ["H.psp8"]
      launcher.write()
      launcher.run()

Now each calculation will be run one after the other in the loop. A side note here
worth noting: when writing the files using the `write()` function, there are
a few checkups made on the input variables and the pseudo potential files
to make sure everything is consistent and no typos were made. This is quite useful
when launching big calculations on HPC clusters to make sure everything is well
set before launching. This is to prevent colossal deceptions when you wait, for example,
a few days for a calculation to run before it crashes after 20 seconds because you
wrote `ecutt` instead of `ecut`.

This *approval* of input files is done by the :ref:`Approvers classes <file_approvers_api>`.
There is one approver class for each launcher class. There are also general approver
classes that checks the mpi commands and cross check it with variables if needed.
The approvers also checks agains :ref:`Variables databases <variables_api>` for allowed
values of the input variables. That's how it checks for typos: if a variable is not
in the given database (because of a typo), then an error is raised before launching/submitting
the calculation. You can try it now if you want: make a typo in the above dict of input variables
and you'll see how the approver responds!

Now comes the plotting part, abisuite comes with a bunch of tools to post process calculations.
One of them is the infamous :class:`Plot <abisuite.plotters.plot.Plot>` object which can make plots.
But in order to make plots, one needs to retrieve data from abinit output files.
This can be done by using the different :ref:`File Handlers <handlers_api>`. For instance,
this script can make the plot of `etotal` vs `interatomic distance` by using the
:class:`AbinitOutputFile Handler <abisuite.handlers.file_handlers.abinit_handlers.abinit_output_file.AbinitOutputFile>`::

  import os

  import numpy as np

  from abisuite import Plot
  from abisuite.handlers import AbinitOutputFile

  etotals = []
  interatomic_distances = []

  # gather data
  # iterate over all dirname in the main working directory
  for dirname in os.listdir("work/tbase1_2"):
      # compute the full path of a given individual calculation
      workdir = os.path.join("work/tbase1_2", dirname)
      # use a with statement to make sure the output file is read
      with AbinitOutputFile.from_calculation(workdir) as out:
          etotals.append(out.output_variables["etotal"])
          xcarts = out.output_variables["xcart"]
          interatomic_distances.append(abs(xcarts[0][0] - xcarts[1][0]))

  # sort data by interatomic distances
  sorted_indices = np.argsort(interatomic_distances)
  interatomic_distances = np.array(interatomic_distances)[sorted_indices]
  etotals = np.array(etotals)[sorted_indices]

  # make the plot
  plot = Plot()
  # use the 'add_curve' method to make a single curve. All attributes will be
  # added to the matplotlib figure in the end.
  plot.add_curve(interatomic_distances, etotals, linewidth=2, color="b",
                 marker="s", markerfacecolor="b", markersize=5)
  # add plot attributes
  plot.xlabel = "Interatomic distance [bohrs]"
  plot.ylabel = "Total energy [Ha]"
  plot.title = "Total energy vs Interatomic Distance"
  # generate the plot
  plot.plot()
  # save plot if needed
  # plot.save( ... )
