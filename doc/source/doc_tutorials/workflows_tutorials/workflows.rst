.. _workflows_tutorial:


Workflows Tutorial
==================


The Workflows object are the **ultimate** powers of the abisuite package!
These objects harness the power of all the other tools inside the abisuite
package (or most of them) and combine them in a single utility.
They can: write & launch calculations, clean them, link them coherently
and post-process them in order to get useful data.

Here are a few examples on how to use them. I reproduced some of the abinit
tutorials for the :class:`~abisuite.workflows.abinit_workflow.AbinitWorkflow`
while one can check for all workflows functionalities in the corresponding
API documentation: :ref:`Workflows API <workflows_api>`.

In a nutshell, the workflows objects are a bunch of organized
:ref:`Sequencers <sequencers_api>`
objects that can be coherently linked together. Their purpose is
to study a specific material using a *single* python script that
is reused and modified on the way.

I replicated some abinit tutorials with workflows and other
abisuite object in the following pages.

.. toctree::

   abinit_base1
   abinit_base2
   abinit_gw1
