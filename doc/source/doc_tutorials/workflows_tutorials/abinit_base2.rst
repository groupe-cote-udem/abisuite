=======================
Abinit base 2 tutorial
=======================

This tutorial assumes you've completed the
:ref:`base 1 <workflows_tutorial_abinit_base1>` tutorial.

Computing bond length and atomisation energy of the H2 molecule
---------------------------------------------------------------

The first part of this tutorials aims at computing the optimal bond length
for the H2 molecule and the corresponding atomisation energy.
Even though the actual abinit tutorial uses the multidtset mode, we will
have to use 2 different workflow objects because multidtset is not supported
unfortunately::

  import asyncio
  from abisuite.workflows import AbinitWorkflow


  async def main():
      common_variables = {
          "acell": [10, 10, 10],
          "ntypat": 1, "znucl": 1,
          "pseudos": ["H.psp8"],
          "kptopt": 0, "nkpt": 1, "diemac": 2.0,
          "nstep": 10, "ecut": 10,
          }

      # H2 molecule relaxation
      workflowH2 = AbinitWorkflow(
          relaxation=True,
          )
      scf_variables = common_variables.copy()
      scf_variables.update({
          "typat": [1, 1], "natom": 2,
          "ntime": 10, "tolmxf": 5e-4,
          "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
          "nband": 1, "toldff": 5e-5, "ionmov": 2,
          })
      await workflowH2.set_relaxation(
          root_workdir="H2/relaxation",
          scf_input_variables=scf_variables,
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          relax_atoms=True,
          relax_cell=False,
          )
      # H molecule (simple GS)
      workflowH = AbinitWorkflow(
          gs=True,
          )
      scf_variables = common_variables.copy()
      scf_variables.update({
          "natom": 1, "nsppol": 2, "occopt": 2,
          "nband": [1, 1], "occ": [1.0, 0.0],
          "toldfe": 1e-6,
          "xcart": [[0.0, 0.0, 0.0]],
          "spinat": [[0.0, 0.0, 1.0]],
          "typat": [1],
          })
      await workflowH.set_gs(
          root_workdir="H/gs",
          scf_input_variables=scf_variables,
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          )
      # run both workflows asynchronously
      await asyncio.gather(workflowH2.run(), workflowH.run())
          

  if __name__ == "__main__":
      asyncio.run(main())


GS ecut convergence
-------------------

Workflows are equipped with ecut convergence sequencers. You can use it like so::

  import asyncio
  from abisuite.workflows import AbinitWorkflow


  async def main():
      common_variables = {
          "acell": [10, 10, 10],
          "ntypat": 1, "znucl": 1,
          "pseudos": ["H.psp8"],
          "kptopt": 0, "nkpt": 1, "diemac": 2.0,
          "nstep": 10,
          }

      # H2 molecule relaxation
      workflowH2 = AbinitWorkflow(
          gs_ecut_convergence=True,  # Set this to true to make gs ecut conv.
          )
      scf_variables = common_variables.copy()
      scf_variables.update({
          "typat": [1, 1], "natom": 2,
          "ntime": 10, "tolmxf": 5e-4,
          "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
          "nband": 1, "toldff": 5e-5, "ionmov": 2,
          })
      await workflowH2.set_gs_ecut_convergence(
          root_workdir="H2/gs_ecut_convergence_bond_length",
          scf_input_variables=scf_variables,
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          scf_ecuts=[10, 15, 20, 25, 30, 35],  # list of ecuts to test
          scf_convergence_criterion=10,  # need to set criterion to determine converged value (meV/atom)
          )
      # H molecule (simple GS)
      workflowH = AbinitWorkflow(
          gs_ecut_convergence=True,
          )
      scf_variables = common_variables.copy()
      scf_variables.update({
          "natom": 1, "nsppol": 2, "occopt": 2,
          "nband": [1, 1], "occ": [1.0, 0.0],
          "toldfe": 1e-6,
          "xcart": [[0.0, 0.0, 0.0]],
          "spinat": [[0.0, 0.0, 1.0]],
          "typat": [1],
          })
      await workflowH.set_gs_ecut_convergence(
          root_workdir="H/gs",
          scf_input_variables=scf_variables,
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          scf_ecuts=[10, 15, 20, 25, 30, 35],  # list of ecuts to test
          scf_convergence_criterion=10,  # need to set criterion to determine converged value
          )
      # run both workflows asynchronously
      await asyncio.gather(workflowH2.run(), workflowH.run())
          

  if __name__ == "__main__":
      asyncio.run(main())


Once the workflows are done you should see graphs like the ones below pop out.
This shows the total energy convergence and the variation with respect to the ecut
parameter. I did not implement an automatic graph for the bond length convergence
I'll leave it as an exercise.

.. image:: images/workflow_abinit_base2_gs_ecut_convergence.png
   :alt: GS ecut convergence plot.

Acell convergence
-----------------

Now, for the acell convergence, as this is not an usual thing to do except for 
converging emptiness, there are no too to converge acell parameters. However it
is easy to create multiple workflows and manually change the acell values for
each run. You can do it like so::

  import asyncio
  from abisuite.workflows import AbinitWorkflow


  async def main():
      # gather all coroutines in a single list
      # so that we can execute them all asynchronously
      coros = []
      for a in [8, 10, 12, 14, 16, 18]:
          common_variables = {
              "acell": [a, a, a],
              "ntypat": 1, "znucl": 1,
              "pseudos": ["H.psp8"],
              "kptopt": 0, "nkpt": 1, "diemac": 2.0,
              "nstep": 10,  "ecut": 10,
              }

          # H2 molecule relaxation
          workflowH2 = AbinitWorkflow(
              gs=True,
              )
          scf_variables = common_variables.copy()
          scf_variables.update({
              "typat": [1, 1], "natom": 2,
              "ntime": 10, "tolmxf": 5e-4,
              "xcart": [[-0.7, 0.0, 0.0], [0.7, 0.0, 0.0]],
              "nband": 1, "toldff": 5e-5, "ionmov": 2,
              })
          await workflowH2.set_gs(
              root_workdir=f"H2/gs_vs_acell/acell{a}",
              scf_input_variables=scf_variables,
              scf_calculation_parameters={
                  "mpi_command": "mpirun -np 4",
                  },
              )
          # H molecule (simple GS)
          workflowH = AbinitWorkflow(
              gs=True,
              )
          scf_variables = common_variables.copy()
          scf_variables.update({
              "natom": 1, "nsppol": 2, "occopt": 2,
              "nband": [1, 1], "occ": [1.0, 0.0],
              "toldfe": 1e-6,
              "xcart": [[0.0, 0.0, 0.0]],
              "spinat": [[0.0, 0.0, 1.0]],
              "typat": [1],
              })
          await workflowH.set_gs(
              root_workdir=f"H/gs_vs_acell/acell{a}",
              scf_input_variables=scf_variables,
              scf_calculation_parameters={
                  "mpi_command": "mpirun -np 4",
                  },
              )
          coros.append(workflowH2.run())
          coros.append(workflowH.run())
      # run all workflows asynchronously
      await asyncio.gather(*coros)
              

  if __name__ == "__main__":
      asyncio.run(main())

Again, since this is manual stuff there's no too to automatically plot
the energy or atomisation energy vs acell. I'll leave this as an exercise.

The remainder of the abinit tutorial is a simple GS calculation for both system
using converged values and another XC functional which requires a different
pseudopotential (just need to change the ``pseudos`` variable accordingly).
