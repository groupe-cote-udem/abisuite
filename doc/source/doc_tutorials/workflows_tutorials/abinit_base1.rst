.. _workflows_tutorial_abinit_base1:

======================
Abinit base 1 tutorial
======================

Simple Ground State Calculation
-------------------------------

This is a replica of the first base tutorial of abinit.
Basically, its purpose is to make a single ground state calculation
of the hydrogen molecule. It is not particularly complex but here are
how you could do it using abisuite's API::

  import asyncio
  from abisuite.workflows import AbinitWorkflow
  
  async def main():
      workflow = AbinitWorkflow(
          gs=True,
          )
      await workflow.set_gs(
          root_workdir="tbase1_1",
          scf_input_variables={
              "acell": [10, 10, 10],
              "rprim": [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
              "ntypat": 1,
              "znucl": 1,
              "natom": 2,
              "typat": [1, 1],
              "xcart": [[-0.7, 0, 0], [0.7, 0, 0]],
              "ecut": 10.0,
              "kptopt": 0,
              "nkpt": 1,
              "kpt": [[0, 0, 0]],
              "nstep": 10,
              "toldfe": 1e-6,
              "diemac": 2.0,
              "pseudos": ["H.psp8"],
              },
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          )
      await workflow.run()

  if __name__ == "__main__":
      asyncio.run(main())

Notice the keywords ``async`` and ``await`` as well as the import of ``asyncio``.
If you are not familiar with asyncio, I strongly encourage you
to `this tutorial <https://realpython.com/async-io-python/>`_. I decided in 2023
to implement ``asyncio`` throughout the codebase. While it can be cumbersome
on a local computer, it can prove useful when the super cluster's filesystem is
slow as this allows a kind of parallelism when handling files. Also, if you
are not familiar with the ``if __name__ == "__main__"`` syntax and why it is
there, again, take a look at `this tutorial in order to understand why <https://realpython.com/if-name-main-python/>`_.

Back to the tutorial, if you execute the script above and everything runs correctly,
nothing else will happen apart from some logging output to the terminal that should look like this:

.. image:: images/workflow_abinit_base1_terminal_output_example.png
   :alt: What should be output on the terminal when executing the script.

You can look at the calculation status by executing the ``abisuite status`` script like so::

  $ abisuite status *

which should print something like this:


.. image:: images/abisuite_status_script_output_example.png
   :alt: Abisuite status script output example.

This script can be useful to quickly look at a glance the status
of multiple calculations. Now, my aim is not to teach you how to use abinit so
I'll not explain what is the meaning of all the variables.
There is a post processor utility within abinit that serves the purpose
to display SCF convergence plot. You can just use this to look at convergences!
Here is an example (just add these lines after the ``await workflow.run()`` line::

  ...  # other imports
  from abisuite.post_processors import SCFConvergence

  async def main():
      ...
      # generate SCF convergence plots
      scf_convergence = await SCFConvergence.from_calculation(
              workflow.gs_sequencer.scf_workdir)
      plot = scf_convergence.create_plot()
      plot.plot()

   ...  # rest of code


You should see a graph like the following:

.. image:: images/workflow_abinit_base1_scf_convergence.png
   :alt: SCF convergence plots

Determining Relaxed Bound Length
--------------------------------

The next part of the tutorial involves multiple ground state calculations in
order to minimize the energy with respect to the bond length.
Thr original abinit tutorial makes use of the famous *multidtset* mode
which allows to launch an arbitrary number of dtsets with a single use of the
abinit executable. While this might prove useful, ``abisuite`` **DOES NOT** support
this. You could still launch one using the usual ``Launchers`` but workflows
or sequencers won't work properly. This is because I strongly discourage the
use of multidtsets for multiple reasons. The main one is that if something
happens during one dtset (like out-of-memory errors for instance), the rest
of the calculations won't happen even though they might not be affected at all by this.
Furthermore, the computational ressources needed by each dtset might
differ greatly between them which might result in a huge waste of ressources since
for the whole calculation to work you will need to satisfy the most hungry dtset.
Instead, it is preferable to work with multiple single dtset calculations.
Here's how you could to it within ``abisuite``::


  import asyncio
  from abisuite.workflows import AbinitWorkflow
  
  async def main():
      # 1 workflow / gs calculation
      ndtsets = 21
      base_xcart = 0.5
      delta_xcart = 0.025
      for i in range(ndtsets):
          x = base_xcart + i * delta_xcart
          workflow = AbinitWorkflow(
              gs=True,
              )
          await workflow.set_gs(
              root_workdir=f"tbase1_2/xcart_{x}",  # need to change directories for each calc
              scf_input_variables={
                  "acell": [10, 10, 10],
                  "rprim": [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
                  "ntypat": 1,
                  "znucl": 1,
                  "natom": 2,
                  "typat": [1, 1],
                  "xcart": [[-x, 0, 0], [x, 0, 0]],
                  "ecut": 10.0,
                  "kptopt": 0,
                  "nkpt": 1,
                  "kpt": [[0, 0, 0]],
                  "nstep": 10,
                  "toldfe": 1e-6,
                  "diemac": 2.0,
                  "pseudos": ["H.psp8"],
                  },
              scf_calculation_parameters={
                  "mpi_command": "mpirun -np 4",
                  },
              )
          await workflow.run()

  if __name__ == "__main__":
      asyncio.run(main())

Executing this script will run all gs calculations one at a time and, most importantly,
**INDEPENDENTLY OF EACH OTHER**. Therefore, if by any chance one would crash, then
the rest of them will execute anyway. You can use the ``abisuite status tbase1_2`` command
to check out the status of all calculations. Now, to make the energy vs bond length plot,
we need to extract the total energy from all calculations. Since all
information is stored in the log files, we just need to parse them.
There are many ways to do it and here's one::

  import asyncio
  import os
  import numpy as np
  from abisuite.handlers import AbinitLogFile
  from abisuite.plotters import Plot

  async def main():
      etots = []
      bond_lengths = []
      root = "tbase1_2"
      for calcdir in os.listdir(root):
         calcpath = os.path.join(root, calcdir)
         loghandler = await AbinitLogFile.from_calculation(calcpath)
         # to read the file either call read or enter an async with stmt 
         # await loghandler.read()
         # the with stmt prevents reading in case file was already read by the same handler
         async with loghandler:
             etot = loghandler.output_variables["etotal"]
             xcarts = loghandler.input_variables["xcart"]
             bond_length = 2 * xcarts[1][0]
             etots.append(etot)
             bond_lengths.append(bond_length)
      # not make the plot using a Plot object
      plot = Plot()
      # need to sort data first
      sorting_order = np.argsort(bond_lengths)
      xdata = np.array(bond_lengths)[sorting_order]
      ydata = np.array(etots)[sorting_order]
      plot.add_curve(
          xdata, ydata, marker="o", markersize=5,
          markerfacecolor="C0", color="C0", linewidth=2,
          )
      plot.plot()

  if __name__ == "__main__":
      asyncio.run(main())


One can then find the minimum value using any method.


Determining Relaxed Bond Length Using Molecular Dynamics
--------------------------------------------------------

Even though one could use the ``set_gs`` feature of the workflows
and set the ``ionmov & optcell`` and other relevent abinit input
variables in order to do atomic and cell relaxation using the molecular
dynamic features of abinit, one can use the ``set_relaxation`` method
of the workflows instead. This allows to use the relaxed geometry
throughout the worklow::

  import asyncio
  from abisuite.workflows import AbinitWorkflow

  async def main():
      workflow = AbinitWorkflow(
          pseudos=["H.psp8"],
          relaxation=True,
          gs=True,
          )
      await workflow.set_relaxation(
          root_workdir="tbase1_3/relaxation",
          scf_input_variables={
              "acell": [10, 10, 10],
              "rprim": [[1, 0, 0], [0, 1, 0], [0, 0, 1]],
              "ntypat": 1,
              "znucl": 1,
              "natom": 2,
              "typat": [1, 1],
              "xcart": [[-0.7, 0, 0], [0.7, 0, 0]],
              "ecut": 10.0,
              "kptopt": 0,
              "nkpt": 1,
              "kpt": [[0, 0, 0]],
              "nstep": 10,
              "tolrff": 0.02,
              "diemac": 2.0,
              "ecutsm": 0.5,
              "tolmxf": 5e-4,
              "ntime": 10,
              "nband": 1,
              "ionmov": 2,  # set this here so that we can chose the algorithm we want
              "pseudos": ["H.psp8"],
              },
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 4",
              },
          )
      await workflow.set_gs(
          root_workdir="tbase1_3/gs",
          scf_input_variables={
              "ecut": 10.0,
              "kptopt": 0,
              "nkpt": 1,
              "kpt": [[0, 0, 0]],
              "nstep": 10,
              "toldfe": 1e-6,
              "diemac": 2.0,
              },
          scf_calculation_parameters={
              "mpi_command": "mpirun -np 6",
              },
          use_relaxed_geometry=True,  # will use the relaxed geometry
          relax_atoms=True,  
          relax_cell=False,  # will not relax cell parameters
          )
      await workflow.run()
      if await workflow.workflow_completed:
          print(workflow.gs_sequencer.scf_input_variables["xcart"])

  if __name__ == "__main__":
      asyncio.run(main())

The above script will need to be executed twice. The first time will execute
the atomic relaxation and then exit. Then, upon reexecution, the code
will automatically get the relaxed atomic positions and use them for
the ground state calculation. The last ``if`` block checks if the
workflow is completed and prints out the ``xcart`` variable used
for the GS calculation. It should be close to the one in the abinit tutorial.

You can show the same plots as abipy by calling the appropriate post processing
class::

  from abisuite.post_processors import RelaxationConvergence

  async def main():
    ...
    relax = await RelaxationConvergence.from_calculation(
                "tbase1_3/relaxation/relax_atoms_only",
                )
    plot = relax.create_plot()
    plot.plot()

.. image:: images/workflow_abinit_base1_relaxation_convergence.png
   :alt: Relaxation convergence plot.


Visualize Charge Density
------------------------

If the last script successfully completed, the GS charge density
should have been printed out on file. You can use the ``abisuite visualizecharge``
script to visualize the charge density. This script will automatically
call the abinit ``cut3d`` utility in order to visualize them::

  $ abisuite visualizecharge tbase1_3/gs


Atomization Energy
------------------

The final part of the tutorial compares the energy of a single H atom
to the Hydrogen molecule. With all the above you should be able to
launch the relevent new ground state calculation for the single H atom.
Now, to get the total energy, either you could dive in the log file by
yourself using any means necessary. Or, to automate it, you could use
the ``AbinitLogFile`` handler class as described in the previous section.
