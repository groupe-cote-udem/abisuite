Abinit GW1 tutorial
-------------------

The tutorial starts with a simple complete GW workflow that starts from
a single GS calculation. One can start by doing this simple one::

  import asyncio
  from abisuite import AbinitWorkflow

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    pseudos=["path/to/Si/pseudo.psp8"])
      workflow.set_gs(
            root_workdir="work_gw1/gs_run",
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-10,
                "acell": [10.26] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
                "ngkpt": [2, 2, 2],
                "diemac": 12.0,
                "nstep": 20,
                "ecut": 8.0,
                "istwfk": "*1",
                "nshiftk": 4,
                "shiftk": [[0., 0.0, 0.0], [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
  
  if __name__ == "__main__":
      asyncio.run(main())

Creating a script with only these instruction will execute a single GS calculation.
Once it's done, we can start doing the GW part by **amending** the same script
and **reexecuting it** all the time. This allows to use only one python script
for the whole pipeline of calculations without having to keep track of many
calculations::

  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA  # new here

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw=True,  # NEW
                    pseudos=["path/to/Si/pseudo.psp8"])
      workflow.set_gs(
            # ... nothing's changed here
            )

      # new part
      workflow.set_gw(
            root_workdir="work_gw1/gw",
            use_gs=True,  # this enables linking with previous GS calculation
            nscf_input_variables={
                "nband": 100,
                "nbdbuf": 20,
                "iscf": -2,  # optional here since default iscf for nscf calcs is -2 in abisuite
                "tolwfr": 1e-12,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "nband": 60,
                "ecuteps": 3.6,
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "nband": 80,
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
    if __name__ == "__main__":
        asyncio.run(main())

From this point, executing the script multiple times will yield each dtset of the
tutorial into separate runs linked together. After everything is done,
one can look at the output file of the self energy run to get the GW data.

Then, the abinit tutorial goes on to the various convergence studies which can
be cumbersome. Luckily, I've implemented all convergence studies into the
workflow. These would be fun *prior* to the actual *official* GW run.
Starting with the number of bands for the self energy operator::
   
  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw_self_energy_nband_convergence=True,  # NEW
                    gw=False,  # MODIFIED
                    pseudos=["path/to/Si/pseudo.psp8"])
      workflow.set_gs(
            # ... nothing's changed here
            )

      # new part
      workflow.set_gw_self_energy_nband_convergence(
            root_workdir="work_gw1/gw_self_energy_nband_convergence",
            use_gs=True,
            nscf_input_variables={
                "nband": 180,  # need to increase nbands because screening and self energy have nband maxed by this value
                "nbdbuf": 20,  # to make convergence quicker
                "iscf": -2,  # optional here since default iscf for nscf calcs is -2 in abisuite
                "tolwfr": 1e-12,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "nband": 60,
                "ecuteps": 3.6,
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_nbands=[25, 50, 100, 150, 180],  # nbands to check
            selfenergy_nband_convergence_criterion=3,   # in meV
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
       
      # this part will do nothing since the 'gw' part was set to False upon init
      workflow.set_gw(...)
  
  if __name__ == "__main__":
      asyncio.run(main())

Once all self energy calculations are done, the following graph should appear and
be saved into the ``work_gw/gw_self_energy_nband_convergence/results`` directory:

.. image:: images/workflow_abinit_gw_self_energy_nband_convergence.png
   :alt: GW self energy nband convergence

Like in the tutorial, we can see that ``nband`` = 100 is converged within 3 meV for the
QP band gap renormalization. Once we determined this, we want to converge the
number of bands for the screening matrix. Using a single keyword, we can
use the self energy converged nband for the self energy part. Again,
we can only **ammend** the same workflow script::

  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw_self_energy_nband_convergence=True,
                    gw_screening_nband_convergence=True,  # NEW
                    gw=False,
                    pseudos=["path/to/Si/pseudo.psp8"])
      await workflow.set_gs(
            # ... nothing's changed here
            )

      await workflow.set_gw_self_energy_nband_convergence(
            # .. nothing's changed here
            )
      # new part
      await workflow.set_gw_screening_nband_convergence(
            root_workdir="work_gw1/gw_screening_nband_convergence",
            use_gs=True,
            use_nscf_from="gw_self_energy_nband_convergence",  # use the nscf calc from this part of the workflow
            use_converged_gw_self_energy_nband=True,  # enables the use of converged self energy nband
            screening_input_variables={
                "ecuteps": 3.6,
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_nbands=[25, 50, 100, 150, 180],  # nbands to check
            screening_nband_convergence_criterion=5,   # in meV
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
      # this part will do nothing since the 'gw' part was set to False upon init
      await workflow.set_gw(...)
  
  if __name__ == "__main__":
      asyncio.run(main())

Once all the calculations are done, you should get the following graph:

.. image:: images/workflow_abinit_gw_screening_nband_convergence.png
   :alt: GW screening nband convergence

From the graph we see that ``nband`` = 100 yields converged results within 5 meV.
Then, we can make the ``ecuteps`` convergence for the screening matrix.
Again we can **ammend** the same workflow script with the following::

  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw_self_energy_nband_convergence=True,
                    gw_screening_nband_convergence=True,
                    gw_ecuteps_convergence=True,  # NEW
                    gw=False,
                    pseudos=["path/to/Si/pseudo.psp8"])
      await workflow.set_gs(
            # ... nothing's changed here
            )
      await workflow.set_gw_self_energy_nband_convergence(
            # .. nothing's changed here
            )
      await workflow.set_gw_screening_nband_convergence(
            # .. nothing's changed here
            )
      # new part
      await workflow.set_gw_ecuteps_convergence(
            root_workdir="work_gw1/gw_ecuteps_convergence",
            use_gs=True,
            use_nscf_from="gw_self_energy_nband_convergence",  # use the nscf calc from this part of the workflow
            use_converged_gw_self_energy_nband=True,  # enables the use of converged self energy nband
            use_converged_gw_screening_nband=True,    # enables the use of converged screening nband
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_ecutepss=[2, 4, 6, 8],             # ecuteps(s) to check
            screening_ecuteps_convergence_criterion=30,  # in meV
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
      # this part will do nothing since the 'gw' part was set to False upon init
      await workflow.set_gw(...)

  if __name__ == "__main__":
      asyncio.run(main())

Once everything's over you should get the following plot:

.. image:: images/workflow_abinit_gw_ecuteps_convergence.png
   :alt: GW screening ecuteps convergence

We see that ``ecuteps`` = 4 is converged within 30 meV. The last convergence study
(which is not actually done in the abinit tutorial) is the kpoint grid one.
This one is longer since the number of kpoints increases quickly when one
increases the size of the grid. Here's how you could do it using the workflow,
again only by **ammending** the same script::

  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw_self_energy_nband_convergence=True,
                    gw_screening_nband_convergence=True,
                    gw_ecuteps_convergence=True,
                    gw_ngkpt_convergence=True,  # NEW
                    gw=False,
                    pseudos=["path/to/Si/pseudo.psp8"])
      await workflow.set_gs(
            # ... nothing's changed here
            )
      await workflow.set_gw_self_energy_nband_convergence(
            # .. nothing's changed here
            )
      await workflow.set_gw_screening_nband_convergence(
            # .. nothing's changed here
            )
      await workflow.set_gw_ecuteps_convergence(
            # .. nothing's changed here
            )
      # new part
      await workflow.set_gw_kgrid_convergence(
            root_workdir="work_gw1/gw_ngkpt_convergence",
            use_converged_gw_self_energy_nband=True,  # enables the use of converged self energy nband
            use_converged_gw_screening_nband=True,    # enables the use of converged screening nband
            use_converged_gw_ecuteps=True,            # enables the use of converged ecuteps
            scf_kgrids=[[x, x, x] for x in [2, 4, 8]],
            scf_kgrid_input_variable_name="ngkpt",
            scf_ngkpt_convergence_criterion=5,  # in meV
            # we need to define SCF input parameters since we redo a SCF calc
            # for each kgrid test.
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-10,
                "acell": [10.26] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
                "diemac": 12.0,
                "nstep": 20,
                "ecut": 8.0,
                "istwfk": "*1",
                "nshiftk": 4,
                "shiftk": [[0., 0.0, 0.0], [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 100,  # this value should be > converged screening/self-energy nband
                "nbdbuf": 20,
                "iscf": -2,  # optional here since default iscf for nscf calcs is -2 in abisuite
                "tolwfr": 1e-12,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )
       # this part will do nothing since the 'gw' part was set to False upon init
       await workflow.set_gw(...)

   if __name__ == "__main__":
       asyncio.run(main())

Once everything's over you should get the following plot:

.. image:: images/workflow_abinit_gw_ngkpt_convergence.png
   :alt: GW ngkpt convergence

From now on, we see that ngkpt = [4, 4, 4] yields converged band gap renormalization
within 5 meV. We can now officially do the final converged GW calculation with
all converged parameters, again, by **amending** the same good ol' workflow
script::

  import asyncio
  from abisuite import AbinitWorkflow
  from abisuite.constants import EV_TO_HA

  async def main():
      workflow = AbinitWorkflow(
                    gs=True,
                    gw_self_energy_nband_convergence=True,
                    gw_screening_nband_convergence=True,
                    gw_ecuteps_convergence=True,
                    gw_ngkpt_convergence=True,
                    gw=True,  # MODIFIED AGAIN
                    pseudos=["path/to/Si/pseudo.psp8"])
      await workflow.set_gs(
            # ... nothing's changed here
            )
      await workflow.set_gw_self_energy_nband_convergence(
            # ... nothing's changed here
            )
      await workflow.set_gw_screening_nband_convergence(
            # ... nothing's changed here
            )
      await workflow.set_gw_ecuteps_convergence(
            # ... nothing's changed here
            )
      await workflow.set_gw_ngkpt_convergence(
            # ... nothing's changed here
            )
      # modified part
      # you might want to delete previous calculation that was stored at the
      # same root
      await workflow.set_gw(
            root_workdir="work_gw1/gw",
            use_converged_gw_self_energy_nband=True,  # enables the use of converged self energy nband
            use_converged_gw_screening_nband=True,    # enables the use of converged screening nband
            use_converged_gw_ecuteps=True,            # enables the use of converged ecuteps
            use_converged_gw_ngkpt=True,              # enables the use of converged ngkpt
            # we need to redefine the SCF parameters since we
            # changed the kgrid compared to the original GS run
            scf_input_variables={
                "nband": 6,
                "tolvrs": 1e-10,
                "acell": [10.26] * 3,
                "rprim": [[0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                "ntypat": 1,
                "znucl": 14,
                "natom": 2,
                "typat": [1, 1],
                "xred": [[0.0, 0.0, 0.0], [0.25, 0.25, 0.25]],
                "diemac": 12.0,
                "nstep": 20,
                "ecut": 8.0,
                "istwfk": "*1",
                "nshiftk": 4,
                "shiftk": [[0., 0.0, 0.0], [0.0, 0.5, 0.5], [0.5, 0.0, 0.5], [0.5, 0.5, 0.0]],
                },
            scf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            nscf_input_variables={
                "nband": 100,
                "nbdbuf": 20,
                "iscf": -2,  # optional here since default iscf for nscf calcs is -2 in abisuite
                "tolwfr": 1e-12,
                },
            nscf_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            screening_input_variables={
                "ppmfrq": 16.7 * EV_TO_HA,
                },
            screening_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            selfenergy_input_variables={
                "ecutsigx": 8.0,
                "nkptgw": 1,
                "kptgw": [0.0, 0.0, 0.0],
                "bdgw": [4, 5],
                },
            selfenergy_calculation_parameters={
                "mpi_command": "mpirun -np 4",
                },
            )

  if __name__ == "__main__":
      asyncio.run(main())
